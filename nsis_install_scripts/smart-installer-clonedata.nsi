; ----------------------------------------------------------------------------------
; **************************** SET PARAMETERS **************************************
; ----------------------------------------------------------------------------------
; Name of application
Name "SMART(Stack Monitoring & Assessment for Reliable daTa) v0.90"

; Icon of application
Icon "smart.ico"

; The file to write
OutFile "setup-smart-clone-0.907.exe"

; Set the default Installation Directory
InstallDir "$PROGRAMFILES\SMART"

; Set the text which prompts the user to enter the installation directory
DirText "SMART(Stack Monitoring & Assessment for Reliable daTa) 프로그램 설치 디렉토리를 설정하여 주시기 바랍니다."

; define JRE download url
!define JRE_URL "http://javadl.oracle.com/webapps/download/AutoDL?BundleId=210185"

; set NSIS include files
!include      "FileFunc.nsh"
!insertmacro  GetFileVersion
!insertmacro  GetParameters
!include      "WordFunc.nsh"
!insertmacro  VersionCompare
!include      "UAC.nsh"

; ----------------------------------------------------------------------------------
; *********************** SECTION FOR JRE INSTALL CHECK ****************************
; ----------------------------------------------------------------------------------
Section ""

  ; check JRE installed
  Call GetJRE

  ; copy application files
  SetOutPath "$INSTDIR\"
  File    "tms_qa_v0.907.jar"
  File    "smart.ico"

  SetOutPath "$INSTDIR\image"
  File /r "image\*"

  SetOutPath "$INSTDIR\clone_data"
  File /r "clone_data\*"

  SetOutPath "$INSTDIR\conf"
  File /r "conf\*"
  
  SetOutPath "$INSTDIR\"

  ; write uninstaller
  WriteUninstaller $INSTDIR\Uninstall.exe

  ; set java and application classpath
  !define JAVAEXE    "C:\ProgramData\Oracle\Java\javapath\javaw.exe"
  !define CLASSPATH  "tms_qa_v0.907.jar"

  ; create shortcut in start menu
  CreateDirectory "$SMPROGRAMS\SMART"
  CreateShortCut "$SMPROGRAMS\SMART\SMART.lnk" "${JAVAEXE} -Xms2G -Xmx4G " "-jar ${CLASSPATH}" "$INSTDIR\smart.ico" 0
  CreateShortCut "$SMPROGRAMS\SMART\Uninstall SMART.lnk" "$INSTDIR\Uninstall.exe"

  ; create shortcut in desktop
  CreateShortCut "$DESKTOP\SMART.lnk" "${JAVAEXE} " "-jar ${CLASSPATH}" "$INSTDIR\smart.ico" 0

  ; create registry keys for add/remove programs in control panel
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SMART" "DisplayName" "SMART (remove only)"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SMART" "UninstallString" "$INSTDIR\Uninstall.exe"

SectionEnd

; ----------------------------------------------------------------------------------
; ************************** SECTION FOR UNINSTALLING ******************************
; ---------------------------------------------------------------------------------- 
Section "Uninstall"

  ; remove all the files and folders
  Delete $INSTDIR\Uninstall.exe ; delete self
  Delete $INSTDIR\tms_qa_v0.907.jar
  Delete $INSTDIR\smart.ico
  Delete $INSTDIR\clone_data\*
  Delete $INSTDIR\conf\*
  Delete $INSTDIR\logs\*
  Delete $INSTDIR\image\*
  
  RMDir $INSTDIR\clone_data
  RMDir $INSTDIR\conf
  RMDir $INSTDIR\logs
  RMDir $INSTDIR\image
  RMDir $INSTDIR

  ; remove all the startmenu links
  Delete "$SMPROGRAMS\SMART\SMART.lnk"
  Delete "$SMPROGRAMS\SMART\Uninstall SMART.lnk"
  
  RMDIR "$SMPROGRAMS\SMART"

  ; remove all the desktop links
  Delete "$DESKTOP\SMART.lnk"

  ; delete registry keys
  DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\SMART"
  DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\SMART"

SectionEnd
 
; ----------------------------------------------------------------------------------
; *********************** FUNCTION FOR JRE INSTALL CHECK ****************************
; ----------------------------------------------------------------------------------
Function GetJRE

  Push $R0
 
  ; 1) Check local JRE
  CheckLocal:
    ClearErrors
    StrCpy $R0 "C:\ProgramData\Oracle\Java\javapath\javaw.exe"
    IfFileExists $R0 JreFound DownloadJRE

  ; 2) Download JRE
  DownloadJRE:
    MessageBox MB_ICONINFORMATION "자바(Java Runtime Environment)의 설치가 필요합니다. JRE를 다운로드하고 설치를 시작합니다."
    StrCpy $2 "$TEMP\Java Runtime Environment.exe"
    nsisdl::download /TIMEOUT=30000 ${JRE_URL} $2
    Pop $R0
    StrCmp $R0 "success" +3
      MessageBox MB_ICONSTOP "Download failed: $R0"
      Abort
    ExecWait $2
    Delete $2
 
  ; 3) JRE founded. return
  JreFound:
    
FunctionEnd