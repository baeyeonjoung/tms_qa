// Tibero
create table QA.ruleset_mgmt (

  ruleset_id    INT           NOT NULL,
  fact_code     VARCHAR(6)    NOT NULL,
  stack_code    VARCHAR(3)    NOT NULL,
  ruleset_state VARCHAR(1)    DEFAULT '0'  NOT NULL,
  ruleset_name  VARCHAR(50),
  ruleset_descr VARCHAR(200),
  user_id       VARCHAR(20),
  create_time   VARCHAR(20),
  start_time    VARCHAR(20),
  end_time      VARCHAR(20),
  CONSTRAINT  ruleset_mgmt_pk  PRIMARY KEY (ruleset_id)
);

CREATE  SEQUENCE  QA.seq_ruleset  START  WITH  1;

CREATE INDEX QA.ruleset_mgmt_idx1  ON QA.ruleset_mgmt(fact_code, stack_code);
CREATE INDEX QA.ruleset_mgmt_idx2  ON QA.ruleset_mgmt(fact_code, stack_code, ruleset_state);


// MySQL
create table tms_common.ruleset_mgmt (

ruleset_id    INT           NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
fact_code     INT(11)       NOT NULL,
stck_code     TINYINT(4)    NOT NULL,
ruleset_state VARCHAR(1)    NOT NULL  DEFAULT '0',
ruleset_name  VARCHAR(50),
ruleset_descr VARCHAR(200),
user_id       VARCHAR(20),
create_time   VARCHAR(20),
start_time    VARCHAR(20),
end_time      VARCHAR(20),
INDEX (fact_code, stck_code),
INDEX (fact_code, stck_code, ruleset_state)
);
