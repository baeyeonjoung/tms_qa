// Tibero
create table QA.ruleset_corr_mgmt (

  ruleset_id         INT            NOT NULL,
  pattern_id         INT            NOT NULL,
  ruleset_enable     VARCHAR(1)     DEFAULT '0'  NOT NULL,
  threshold_lower    DECIMAL(10,3)  DEFAULT  0   NOT NULL,
  threshold_upper    DECIMAL(10,3)  DEFAULT  0   NOT NULL,
  items              VARCHAR(50),
  rsquare            DECIMAL(10,3)  DEFAULT  0   NOT NULL,
  cov_matrix         VARCHAR(2000),
  imv_cov_matrix     VARCHAR(2000),
  mean_value         VARCHAR(200),
  sigma_value        VARCHAR(200),
  confi_level        DECIMAL(10,3)  DEFAULT  0   NOT NULL  
);

CREATE INDEX QA.ruleset_corr_mgmt_idx1  ON QA.ruleset_corr_mgmt(ruleset_id);
CREATE INDEX QA.ruleset_corr_mgmt_idx2  ON QA.ruleset_corr_mgmt(ruleset_id, pattern_id);

// MySQL
create table tms_common.ruleset_corr_mgmt (

ruleset_id         INT           NOT NULL,
pattern_id         INT           NOT NULL,
ruleset_enable     VARCHAR(1)    NOT NULL  DEFAULT '0',
threshold_lower    DECIMAL(10,3) NOT NULL  DEFAULT 0,
threshold_upper    DECIMAL(10,3) NOT NULL  DEFAULT 0,
items              VARCHAR(50),
rsquare            DECIMAL(10,3) NOT NULL  DEFAULT 0,
cov_matrix         VARCHAR(1000),
imv_cov_matrix     VARCHAR(1000),
mean_value         VARCHAR(100),
sigma_value        VARCHAR(100),
confi_level        DECIMAL(10,3) NOT NULL  DEFAULT 0,
INDEX (ruleset_id),
INDEX (ruleset_id, pattern_id)
);
