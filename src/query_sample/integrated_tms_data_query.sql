		SELECT  
		    a.min_time  as  timestr
		  , max(case when a.item_code=1  then a.min_valu end)  as  tsp
		  , max(case when a.item_code=2  then a.min_valu end)  as  so2
		  , max(case when a.item_code=3  then a.min_valu end)  as  nox
		  , max(case when a.item_code=4  then a.min_valu end)  as  hcl
		  , max(case when a.item_code=5  then a.min_valu end)  as  hf
		  , max(case when a.item_code=6  then a.min_valu end)  as  nh3
		  , max(case when a.item_code=7  then a.min_valu end)  as  co
		  , max(case when a.item_code=8  then a.min_valu end)  as  o2
		  , max(case when a.item_code=9  then a.min_valu end)  as  flw
		  , max(case when a.item_code=10 then a.min_valu end)  as  tmp
		  , max(case when a.item_code=11 then a.min_valu end)  as  tm1
		  , max(case when a.item_code=12 then a.min_valu end)  as  tm2
		  , max(case when a.item_code=13 then a.min_valu end)  as  tm3
		FROM    cleansys_c3.tmsf_min  as  a
		WHERE   fact_code = 30154  and  stck_code = 13  and  min_time >= '20130701'  and  min_time < '20140701'
		group  by  a.min_time
		order by a.min_time asc