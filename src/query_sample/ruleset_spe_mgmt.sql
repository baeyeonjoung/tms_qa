// Tibero
create table QA.ruleset_spe_mgmt (

  ruleset_id         INT            NOT NULL,
  pattern_id         INT            NOT NULL,
  ruleset_enable     VARCHAR(1)     DEFAULT '0' NOT NULL,
  threshold_lower    NUMBER(10,3)   DEFAULT  0  NOT NULL,
  threshold_upper    NUMBER(10,3)   DEFAULT  0  NOT NULL,
  items              VARCHAR(50),
  pc_count           INT            DEFAULT 0   NOT NULL,
  eigen_vector       VARCHAR(2000),
  eigen_value        VARCHAR(200),
  neigen_value       VARCHAR(200),
  mean_value         VARCHAR(200),
  sigma_value        VARCHAR(200),
  confi_level        NUMBER(10,3)   DEFAULT 0   NOT NULL
);

CREATE INDEX QA.ruleset_spe_mgmt_idx1  ON QA.ruleset_spe_mgmt(ruleset_id);
CREATE INDEX QA.ruleset_spe_mgmt_idx2  ON QA.ruleset_spe_mgmt(ruleset_id, pattern_id);


// MySQL
create table tms_common.ruleset_spe_mgmt (

ruleset_id         INT           NOT NULL,
pattern_id         INT           NOT NULL,
ruleset_enable     VARCHAR(1)    NOT NULL  DEFAULT '0',
threshold_lower    DECIMAL(10,3) NOT NULL  DEFAULT 0,
threshold_upper    DECIMAL(10,3) NOT NULL  DEFAULT 0,
items              VARCHAR(50),
pc_count           INT           NOT NULL  DEFAULT 0,
eigen_vector       VARCHAR(1000),
eigen_value        VARCHAR(100),
neigen_value       VARCHAR(100),
mean_value         VARCHAR(100),
sigma_value        VARCHAR(100),
confi_level        DECIMAL(10,3) NOT NULL  DEFAULT 0,
INDEX (ruleset_id),
INDEX (ruleset_id, pattern_id)
);
