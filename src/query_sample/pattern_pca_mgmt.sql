// Tibero
create table QA.pattern_pca_mgmt (

  ruleset_id         INT            NOT NULL,
  pattern_id         INT            NOT NULL,
  items              VARCHAR(50),
  pc_count           INT            DEFAULT 0 NOT NULL,
  eigen_vector       VARCHAR(2000),
  eigen_value        VARCHAR(200),
  neigen_value       VARCHAR(200)
);

CREATE INDEX QA.pattern_pca_mgmt_idx  ON QA.pattern_pca_mgmt(ruleset_id);
CREATE INDEX QA.pattern_pca_mgmt1_idx  ON QA.pattern_pca_mgmt(ruleset_id, pattern_id);

// MySQL
create table tms_common.pattern_pca_mgmt (

ruleset_id         INT           NOT NULL,
pattern_id         INT           NOT NULL,
items              VARCHAR(50),
pc_count           INT           NOT NULL  DEFAULT 0,
eigen_vector       VARCHAR(1000),
eigen_value        VARCHAR(100),
neigen_value       VARCHAR(100),
INDEX (ruleset_id),
INDEX (ruleset_id, pattern_id)
);
