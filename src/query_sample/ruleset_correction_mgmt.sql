// Tibero
create table QA.ruleset_correction_mgmt (

  ruleset_id     INT           NOT NULL,
  item_code      INT           NOT NULL,
  std_oxgy       DECIMAL(10,3) DEFAULT 0.0   NOT NULL,
  std_mois       DECIMAL(10,3) DEFAULT 0.0   NOT NULL,
  oxyg_corr      VARCHAR(1)    DEFAULT '0'   NOT NULL,
  temp_corr      VARCHAR(1)    DEFAULT '0'   NOT NULL,
  mois_corr      VARCHAR(1)    DEFAULT '0'   NOT NULL
);

CREATE INDEX QA.ruleset_correction_mgmt_idx  ON QA.ruleset_correction_mgmt(ruleset_id);
