// Tibero
create table QA.pattern_mgmt (

  ruleset_id         INT           NOT NULL,
  pattern_id         INT           NOT NULL,
  pattern_name       VARCHAR(50),
  pattern_enable     VARCHAR(1)    DEFAULT '0'   NOT NULL,
  ruleset_enable     VARCHAR(1)    DEFAULT '0'   NOT NULL,
  operate_stop_state VARCHAR(1)    DEFAULT '0'   NOT NULL,
  item_count         INT           DEFAULT  0    NOT NULL,
  items              VARCHAR(100)
);

CREATE INDEX QA.pattern_mgmt_idx  ON QA.pattern_mgmt(ruleset_id);

// MySQL
create table tms_common.pattern_mgmt (

ruleset_id         INT           NOT NULL,
pattern_id         INT           NOT NULL,
pattern_name       VARCHAR(50),
pattern_enable     VARCHAR(1)    NOT NULL  DEFAULT '0',
ruleset_enable     VARCHAR(1)    NOT NULL  DEFAULT '0',
operate_stop_state VARCHAR(1)    NOT NULL  DEFAULT '0',
item_count         INT           NOT NULL  DEFAULT  0,
items              VARCHAR(100),
INDEX (ruleset_id)
);
