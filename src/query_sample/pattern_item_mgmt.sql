// Tibero
create table QA.pattern_item_mgmt (

  ruleset_id         INT             NOT NULL,
  pattern_id         INT             NOT NULL,
  item_code          INT             NOT NULL,
  item_name          VARCHAR(20)     NOT NULL,
  min_range          NUMBER(10, 3)   DEFAULT 0 NOT NULL,
  max_range          NUMBER(10, 3)   DEFAULT 0 NOT NULL,
  bin_count          INT             DEFAULT 0 NOT NULL,
  bin_interval       INT             DEFAULT 0 NOT NULL,
  density            VARCHAR(3000)
);

CREATE INDEX QA.pattern_item_mgmt_idx1  ON QA.pattern_item_mgmt(ruleset_id);
CREATE INDEX QA.pattern_item_mgmt_idx2  ON QA.pattern_item_mgmt(ruleset_id, pattern_id);
CREATE INDEX QA.pattern_item_mgmt_idx3  ON QA.pattern_item_mgmt(ruleset_id, pattern_id, item_code);

// MySQL
create table QA.tms_common.pattern_item_mgmt (

  ruleset_id         INT             NOT NULL,
  pattern_id         INT             NOT NULL,
  item_code          INT             NOT NULL,
  item_name          VARCHAR(20)     NOT NULL,
  min_range          DECIMAL(10, 3)  NOT NULL  DEFAULT '0',
  max_range          DECIMAL(10, 3)  NOT NULL  DEFAULT '0',
  bin_count          INT             NOT NULL  DEFAULT '0',
  bin_interval       INT             NOT NULL  DEFAULT '0',
  density            VARCHAR(3000),
  INDEX (ruleset_id),
  INDEX (ruleset_id, pattern_id),
  INDEX (ruleset_id, pattern_id, item_code)
);
