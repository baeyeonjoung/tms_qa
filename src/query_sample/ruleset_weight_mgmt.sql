// Tibero
create table QA.ruleset_weight_mgmt (

  ruleset_id         INT        NOT NULL,
  multi_weight       INT        DEFAULT 1  NOT NULL,
  bi_weight          INT        DEFAULT 1  NOT NULL,
  uni_weight         INT        DEFAULT 1  NOT NULL,
  corr_weight        INT        DEFAULT 1  NOT NULL,
  warn_threshold     INT        DEFAULT 2  NOT NULL,
  minor_threshold    INT        DEFAULT 4  NOT NULL,
  major_threshold    INT        DEFAULT 6  NOT NULL,
  critical_threshold INT        DEFAULT 8  NOT NULL
);

CREATE INDEX QA.ruleset_weight_mgmt_idx  ON QA.ruleset_weight_mgmt(ruleset_id);

// MySQL
create table tms_common.ruleset_weight_mgmt (

ruleset_id         INT           NOT NULL,
multi_weight       INT           NOT NULL  DEFAULT 1,
bi_weight          INT           NOT NULL  DEFAULT 1,
uni_weight         INT           NOT NULL  DEFAULT 1,
corr_weight        INT           NOT NULL  DEFAULT 1,
warn_threshold     INT           NOT NULL  DEFAULT 2,
minor_threshold    INT           NOT NULL  DEFAULT 4,
major_threshold    INT           NOT NULL  DEFAULT 6,
critical_threshold INT           NOT NULL  DEFAULT 8,
INDEX (ruleset_id)
);
