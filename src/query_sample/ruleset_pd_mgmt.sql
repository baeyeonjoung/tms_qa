// Tibero
create table QA.ruleset_pd_mgmt (

  ruleset_id         INT           NOT NULL,
  pattern_id         INT           NOT NULL,
  ruleset_enable     VARCHAR(1)    DEFAULT '0'  NOT NULL,
  threshold_lower    NUMBER(10,3)  DEFAULT  0   NOT NULL,
  threshold_upper    NUMBER(10,3)  DEFAULT  0   NOT NULL,
  item               INT,
  mean_value         NUMBER(10,3),
  sigma_value        NUMBER(10,3),
  confi_level        NUMBER(10,3)  DEFAULT  0   NOT NULL
);

CREATE INDEX QA.ruleset_pd_mgmt_idx1  ON QA.ruleset_pd_mgmt(ruleset_id);
CREATE INDEX QA.ruleset_pd_mgmt_idx2  ON QA.ruleset_pd_mgmt(ruleset_id, pattern_id);

// MySQL
create table tms_common.ruleset_pd_mgmt (

ruleset_id         INT           NOT NULL,
pattern_id         INT           NOT NULL,
ruleset_enable     VARCHAR(1)    NOT NULL  DEFAULT '0',
threshold_lower    DECIMAL(10,3) NOT NULL  DEFAULT 0,
threshold_upper    DECIMAL(10,3) NOT NULL  DEFAULT 0,
item               INT,
mean_value         DECIMAL(10,3),
sigma_value        DECIMAL(10,3),
confi_level        DECIMAL(10,3) NOT NULL  DEFAULT 0,
INDEX (ruleset_id),
INDEX (ruleset_id, pattern_id)
);
