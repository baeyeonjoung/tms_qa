/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.nexacro;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.database.QueryPatternInfo;
import com.snu.tms.qa.database.QueryPatternItemInfo;
import com.snu.tms.qa.database.QueryPatternPcaInfo;
import com.snu.tms.qa.database.QueryPatternRulesetCorr;
import com.snu.tms.qa.database.QueryPatternRulesetPd;
import com.snu.tms.qa.database.QueryPatternRulesetSpc;
import com.snu.tms.qa.database.QueryPatternRulesetSpe;
import com.snu.tms.qa.database.QueryPatternRulesetTss;
import com.snu.tms.qa.database.QueryRulesetCorrectionInfo;
import com.snu.tms.qa.database.QueryRulesetInfo;
import com.snu.tms.qa.database.QueryRulesetWeightingInfo;
import com.snu.tms.qa.database.SqlMapClientManager;
import com.snu.tms.qa.model.db.Tms30minDataCellDB;
import com.snu.tms.qa.model.db.Tms30minDataRecordDB;
import com.snu.tms.qa.model.db.TmsDataCellDB;
import com.snu.tms.qa.model.db.TmsDataRecordDB;
import com.snu.tms.qa.util.DateUtil;

/**
 * Setting the program startup process.
 * 
 */
public class StackApiTest {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(StackApiTest.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Setup logging and other required library
		try {
			org.apache.log4j.PropertyConfigurator.configure("./conf/log.properties");
			logger.info("TMS Quality Assessment process is stating .............");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("./conf/log.properties file can't found.\n"
					+ "Process halted and system will be exit.");
			System.exit(1);
		}
		
		// Initialize database connection
		try {
			SqlMapClientManager.getSqlMapClient();
			logger.info( "Database for TMS connection established." );
			
		} catch (Exception e) {
			logger.fatal( 
					"Database connection can't be established.");
			logger.fatal( "Process is halted and system will be exit." );
			System.exit(-1);
		}
		
		// Start Test
		new StackApiTest();
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  StackApiDataset  dataset = null;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Test
	 */
	public StackApiTest() {
		
		// create dataset model
		this.dataset = new StackApiDataset();
		
		// set data condition
		dataset.setDataCondition("150061", 11, "2014-01-01 00:00:00", "2014-02-01 00:00:00");
		
		// set 5-min data
		dataset.setTms5minRecords(queryTms5minData());
		
		// set 30-min data
		dataset.setTms30minRecords(queryTms30minData());
		
		// set ruleset_mgmt
		dataset.setRulesetInfo(
				QueryRulesetInfo.queryDefaultRulesetInfo(
						dataset.getFactCode(), 
						dataset.getStackCode()));
		if (dataset.getRulesetModel().getRulesetId() == -1) {
			System.out.println("RulesetModel doesn't settled.");
			return;
		}
		int rulesetId = dataset.getRulesetModel().getRulesetId();
		
		/**
		// 특정 Rulset ID를 선택한 경우
		dataset.setRulesetInfo(
				QueryRulesetInfo.queryTargetRulesetInfo( 201 ));
		*/
		
		// set ruleset_pattern_mgmt
		dataset.setRulesetPatternInfo(QueryPatternInfo.queryAllPatternInfo(rulesetId));
		
		// set ruleset_pattern_item_mgmt
		dataset.setRulesetPatternItemInfo(QueryPatternItemInfo.queryAllPatternInfo(rulesetId));
		
		// set ruleset_pattern_pca_mgmt
		dataset.setRulesetPatternPcaInfo(QueryPatternPcaInfo.queryAllPatternPcaInfo(rulesetId));
		
		// set ruleset_pattern_tss_mgmt
		dataset.setRulesetPatternTssInfo(QueryPatternRulesetTss.queryAllRuleset(rulesetId));
		
		// set ruleset_pattern_spe_mgmt
		dataset.setRulesetPatternSpeInfo(QueryPatternRulesetSpe.queryAllRuleset(rulesetId));

		// set ruleset_pattern_corr_mgmt
		dataset.setRulesetPatternCorrInfo(QueryPatternRulesetCorr.queryAllRuleset(rulesetId));

		// set ruleset_pattern_spc_mgmt
		dataset.setRulesetPatternSpcInfo(QueryPatternRulesetSpc.queryAllRuleset(rulesetId));

		// set ruleset_pattern_pd_mgmt
		dataset.setRulesetPatternPdInfo(QueryPatternRulesetPd.queryAllRuleset(rulesetId));
		
		//set ruleset_correction_mgmt
		dataset.setRulesetCorrectionInfo(QueryRulesetCorrectionInfo.queryRulesetCorrectionInfo(rulesetId));
		
		// set ruleset_weighting_mgmt
		dataset.setRulesetWeightingInfo(QueryRulesetWeightingInfo.queryRulesetWeightingInfo(rulesetId));
		
		// apply ruleset_model
		dataset.applyRulesetModel();
		
	} // end of constructor
	
	/**
	 * set tms 5min data
	 */
	private List<TmsDataCellDB> queryTms5minData() {

		// set rulset_mgmt
		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			TmsDataRecordDB tmsDataRecordDB = new TmsDataRecordDB();
			tmsDataRecordDB.setFact_code(dataset.getFactCode());
			tmsDataRecordDB.setStck_code(dataset.getStackCode());
			tmsDataRecordDB.setStart_time_str(DateUtil.getTmsDateStr(dataset.getRulsetBaseStartTime()));
			tmsDataRecordDB.setEnd_time_str(DateUtil.getTmsDateStr(dataset.getRulsetBaseEndTime()));
			
			String queryName = "QueryTmsRecord.SelectAllTmsRecord";

			long queryStartTime = System.currentTimeMillis();
			logger.debug(String.format(
					"QueryStart (db_name: %s, fact_code: %s, stck_code: %d, start_time: %s, end_time: %s)", 
					"", dataset.getFactCode(), dataset.getStackCode(), 
					DateUtil.getTmsDateStr(dataset.getRulsetBaseStartTime()), 
					DateUtil.getTmsDateStr(dataset.getRulsetBaseEndTime())));
			
			@SuppressWarnings("unchecked")
			List<TmsDataCellDB> results = (List<TmsDataCellDB>) 
				sqlMapClient.queryForList(queryName, tmsDataRecordDB);
			
			long queryFinishTime = System.currentTimeMillis();
			logger.debug(String.format(
					"Query Finished. (query_time: %d seconds)", 
					((int) (queryFinishTime - queryStartTime)/1000L)));
			
			return results;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error( e.toString() );
			return null;
		}
		
	} // end of method
	
	/**
	 * set tms 30min data
	 */
	private List<Tms30minDataCellDB> queryTms30minData() {

		// set rulset_mgmt
		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			Tms30minDataRecordDB tmsDataRecordDB = new Tms30minDataRecordDB();
			tmsDataRecordDB.setFact_code(dataset.getFactCode());
			tmsDataRecordDB.setStck_code(dataset.getStackCode());
			tmsDataRecordDB.setStart_time_str(DateUtil.getTmsDateStr(dataset.getRulsetBaseStartTime()));
			tmsDataRecordDB.setEnd_time_str(DateUtil.getTmsDateStr(dataset.getRulsetBaseEndTime()));
			
			String queryName = "QueryTms30minRecord.SelectAllTmsRecord";

			long queryStartTime = System.currentTimeMillis();
			logger.debug(String.format(
					"QueryStart (db_name: %s, fact_code: %s, stck_code: %d, start_time: %s, end_time: %s)", 
					"", dataset.getFactCode(), dataset.getStackCode(), 
					DateUtil.getTmsDateStr(dataset.getRulsetBaseStartTime()), 
					DateUtil.getTmsDateStr(dataset.getRulsetBaseEndTime())));
			
			@SuppressWarnings("unchecked")
			List<Tms30minDataCellDB> results = (List<Tms30minDataCellDB>) 
				sqlMapClient.queryForList(queryName, tmsDataRecordDB);
			
			long queryFinishTime = System.currentTimeMillis();
			logger.debug(String.format(
					"Query Finished. (query_time: %d seconds)", 
					((int) (queryFinishTime - queryStartTime)/1000L)));

			return results;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error( e.toString() );
			return null;
		}
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
