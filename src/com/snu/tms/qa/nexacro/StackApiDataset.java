/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.nexacro;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.snu.tms.qa.analysis.outlier.DoAnalysisReverseCorrection;
import com.snu.tms.qa.model.PatternDataHistogram;
import com.snu.tms.qa.model.Tms30minDataRecord;
import com.snu.tms.qa.model.Tms30minDataset;
import com.snu.tms.qa.model.TmsDataCell;
import com.snu.tms.qa.model.TmsDataHeader;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.db.PatternItemRecord;
import com.snu.tms.qa.model.db.PatternPcaRecord;
import com.snu.tms.qa.model.db.PatternRecord;
import com.snu.tms.qa.model.db.PatternRulesetCorrRecord;
import com.snu.tms.qa.model.db.PatternRulesetPdRecord;
import com.snu.tms.qa.model.db.PatternRulesetSpcRecord;
import com.snu.tms.qa.model.db.PatternRulesetSpeRecord;
import com.snu.tms.qa.model.db.PatternRulesetTssRecord;
import com.snu.tms.qa.model.db.RulesetCorrectionRecord;
import com.snu.tms.qa.model.db.RulesetRecord;
import com.snu.tms.qa.model.db.RulesetWeightingRecord;
import com.snu.tms.qa.model.db.Tms30minDataCellDB;
import com.snu.tms.qa.model.db.TmsDataCellDB;
import com.snu.tms.qa.model.ruleset.DetectionDataCell;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.DetectionRuleset;
import com.snu.tms.qa.model.ruleset.OdrRecord;
import com.snu.tms.qa.model.ruleset.PcaResultModel;
import com.snu.tms.qa.model.ruleset.RawDataCell;
import com.snu.tms.qa.model.ruleset.RulesetCorr;
import com.snu.tms.qa.model.ruleset.RulesetCorrectionModel;
import com.snu.tms.qa.model.ruleset.RulesetModel;
import com.snu.tms.qa.model.ruleset.RulesetPatternModel;
import com.snu.tms.qa.model.ruleset.RulesetPcaSpe;
import com.snu.tms.qa.model.ruleset.RulesetPcaTss;
import com.snu.tms.qa.model.ruleset.RulesetPd;
import com.snu.tms.qa.model.ruleset.RulesetSpc;
import com.snu.tms.qa.model.ruleset.RulesetWeightingModel;
import com.snu.tms.qa.util.DateUtil;

/**
 * Data Element for TMS Data
 * 
 */
public class StackApiDataset {

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	// Set Fact_Code and Stack_Code
	private  String     factCode  = "";
	private  int        stackCode = 0;
	
	// TMS raw data
	private  TmsDataset  tmsDataset = null;
	
	// items
	private  int[]       items = null;
	
	// TMS 30min Data
	private  Tms30minDataset  tms30minDataset = null;
	
	private Vector<DetectionPatternDataset> detectionPatternDataset = 
			new Vector<DetectionPatternDataset>();
	
	// weighting factor model
	private RulesetWeightingModel rulesetWeighingModel = null;
	
	private RulesetModel  rulesetModel = new RulesetModel();
	
	// ruleset base date 
	private  long      rulsetBaseStartTime = 0L;
	private  long      rulsetBaseEndTime   = 0L;

	private Hashtable<Integer, Vector<TmsDataRecord>> patternTmsDataRecordHash = 
			new Hashtable<Integer, Vector<TmsDataRecord>>();
	
	private  DetectionRecord[]       detectionRecords;
	private  OdrRecord[]             odrRecords;

	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public StackApiDataset() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * set fact_code, stack_code, start_time, end_time
	 * @param start_time_str: time format (yyyy-MM-dd HH:mm:ss)
	 * @param end_time_str: time format (yyyy-MM-dd HH:mm:ss)
	 */
	public void setDataCondition(
			String  fact_code,
			int     stack_code,
			String  start_time_str, 
			String  end_time_str) {
		
		// set parameters
		this.factCode  = fact_code;
		this.stackCode = stack_code;
		this.rulsetBaseStartTime = DateUtil.getDate(start_time_str);
		this.rulsetBaseEndTime   = DateUtil.getDate(end_time_str);
		
		// Create TmsDataSet
		TmsDataHeader tmsDataHeader = new TmsDataHeader();
		tmsDataHeader.setStart_time_str(start_time_str);
		tmsDataHeader.setEnd_time_str(end_time_str);
		this.tmsDataset = new TmsDataset();
		this.tmsDataset.setTmsDataHeader(tmsDataHeader);
		
	} // end of method
	
	/**
	 * Set 5min-data
	 */
	public void setTms5minRecords(List<TmsDataCellDB> results) {
		
		// check data
		if (results == null) {
			return;
		}
		
		// set data
		// "this.tmsDataset.setTmsDataHeader(tmsDataHeader);" 부분에서 이미 5분 간격으로 자료가 생성됨.
		long targetTime = 0L;
		TmsDataRecord dataRecord = null;
		for (TmsDataCellDB result : results) {
			
			targetTime = DateUtil.getTmsDate(result.getMin_time());
			dataRecord = this.tmsDataset.getTmsDataRecord(targetTime);
			if (dataRecord == null) {
				continue;
			}
			dataRecord.setValue(
					result.getItem_code(), 
					result.getMin_valu(), 
					result.getMin_valu_bf());
		
		} // end of for-loop
		
		// 측정기기 상태정보가 교정중(1), 동잗불량(2), 전원단절(4), 보수중(8) 등에 대해서
		// 일부 항목이 다른 항목에 비해 유효한 데이터가 50% 미만일 경우에는
		// 해당 Item을 자료에서 삭제한다.
		this.tmsDataset.doDataValidationCheck();
		
	} // end of method
	
	/**
	 * Set 30min-data
	 */
	public void setTms30minRecords(List<Tms30minDataCellDB> results) {
		
		// check data
		if (results == null) {
			return;
		}
		
		// set Tms30Min dataset
		this.tms30minDataset = new Tms30minDataset();
		this.tms30minDataset.init(this.rulsetBaseStartTime, this.rulsetBaseEndTime);

		// set data
		long targetTime = 0L;
		Tms30minDataRecord dataRecord = null;
		for (Tms30minDataCellDB result : results) {
			
			targetTime = DateUtil.getTmsDate(result.getHalf_time());
			dataRecord = this.tms30minDataset.getTmsDataRecord(targetTime);
			if (dataRecord == null) {
				continue;
			}
			dataRecord.setValue(
					result.getItem_code(), 
					result.getHalf_valu(),
					result.getHalf_over(),
					result.getHalf_stat(),
					result.getHalf_dlst(),
					result.getStat_code(),
					result.getStat_valu());
		
		} // end of for-loop
		
	} // end of method
	
	/**
	 * set ruleset_mgmt data
	 */
	public void setRulesetInfo(RulesetRecord rulesetRecord) {
		
		// check data
		if (rulesetRecord == null) {
			return;
		}
		
		this.rulesetModel.setRulesetId(rulesetRecord.getRuleset_id());
		this.rulesetModel.setFactCode(rulesetRecord.getFact_code());
		this.rulesetModel.setStckCode(rulesetRecord.getStck_code());
		this.rulesetModel.setRulesetState(rulesetRecord.getRuleset_state());
		this.rulesetModel.setRulesetName(rulesetRecord.getRuleset_name());
		this.rulesetModel.setRulesetDescr(rulesetRecord.getRuleset_descr());
		this.rulesetModel.setRulesetStartTime(DateUtil.getDate(rulesetRecord.getStart_time()));
		this.rulesetModel.setRulesetEndTime(DateUtil.getDate(rulesetRecord.getEnd_time()));
		this.rulesetModel.setUserId(rulesetRecord.getUser_id());
		this.rulesetModel.setCreateTime(rulesetRecord.getCreate_time());
		
	} // end of method
	
	/**
	 * set ruleset_pattern_mgmt
	 */
	public void setRulesetPatternInfo(List<PatternRecord> results) {
		
		// check data
		if (results == null) {
			return;
		}
		
		// convert from database record type to system model type
		for (PatternRecord record : results) {
			
			// set parameters
			//int      rulesetId        = record.getRuleset_id();
			int      patternId        = record.getPattern_id();
			String   patternName      = record.getPattern_name();
			boolean  isEnabled        = (record.getPattern_enable() == 1     ? true : false);
			boolean  isRulesetCreated = (record.getRuleset_enable() == 1     ? true : false);
			boolean  isOperateStop    = (record.getOperate_stop_state() == 1 ? true : false);
			int      itemCount        = record.getItem_count();
			String   items            = record.getItems();
			
			// set model
			RulesetPatternModel patternModel = new RulesetPatternModel(
					patternId,
					patternName,
					isEnabled,
					isRulesetCreated,
					isOperateStop, 
					itemCount,
					items,
					rulesetModel.getRulesetStartTime(),
					rulesetModel.getRulesetEndTime());
			
			// add model
			this.rulesetModel.addRulesetPatternModel(patternModel);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * set ruleset_pattern_item_mgmt
	 */
	public void setRulesetPatternItemInfo(List<PatternItemRecord> results) {
		
		// check data
		if (results == null) {
			return;
		}
		
		// convert from database record type to system model type
		for (PatternItemRecord record : results) {
			
			// set parameters
			//int      rulesetId     = record.getRuleset_id();
			int      patternId     = record.getPattern_id();
			int      item_code     = record.getItem_code();
			double   min_range     = record.getMin_range();
			double   max_range     = record.getMax_range();
			int      bin_count     = record.getBin_count();
			int      bin_interval  = record.getBin_interval();
			String   density       = record.getDensity();
			
			// get ruleset histogram
			RulesetPatternModel  rulesetPatternModel = this.rulesetModel.getRulesetPatternModel(patternId);
			if (rulesetPatternModel == null) {
				continue;
			}
			
			// add model
			PatternDataHistogram patternDataHistogram = 
					rulesetPatternModel.getPatternDataHistogram(item_code);
			if (patternDataHistogram == null) {
				continue;
			}
			patternDataHistogram.setBinNo(bin_count);
			patternDataHistogram.setInterval(bin_interval);
			patternDataHistogram.setMaxRange(max_range);
			patternDataHistogram.setMinRange(min_range);
			patternDataHistogram.setDensities(density);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * set ruleset_pattern_pca_mgmt
	 */
	public void setRulesetPatternPcaInfo(List<PatternPcaRecord> results) {
		
		// check data
		if (results == null) {
			return;
		}
		
		// convert from database record type to system model type
		for (PatternPcaRecord record : results) {
			
			// set parameters
			//int      rulesetId     = record.getRuleset_id();
			int      patternId     = record.getPattern_id();
			String   items         = record.getItems();
			int      pc_count      = record.getPc_count();
			String   eigen_vector  = record.getEigen_vector();
			String   eigen_value   = record.getEigen_value();
			String   neigen_value  = record.getNeigen_value();
			
			// get ruleset histogram
			RulesetPatternModel  rulesetPatternModel = this.rulesetModel.getRulesetPatternModel(patternId);
			if (rulesetPatternModel == null) {
				continue;
			}
			
			// add model
			PcaResultModel pcaResultModel = new PcaResultModel();
			pcaResultModel.setItems(items);
			pcaResultModel.setPcCount(pc_count);
			pcaResultModel.setEigenVector(eigen_vector);
			pcaResultModel.setEigenValues(eigen_value);
			pcaResultModel.setNeigenValues(neigen_value);
			
			rulesetPatternModel.setPcaResultModel(pcaResultModel);
			
		} // end of for-loop
		
	} // end of method	
	
	/**
	 * set ruleset_pattern_tss_mgmt
	 */
	public void setRulesetPatternTssInfo(List<PatternRulesetTssRecord> results) {
		
		// check data
		if (results == null) {
			return;
		}
		
		// convert from database record type to system model type
		for (PatternRulesetTssRecord record : results) {
			
			// set parameters
			int      ruleset_id      = record.getRuleset_id();
			int      pattern_id      = record.getPattern_id();
			int      ruleset_enable  = record.getRuleset_enable();
			double   threshold_lower = record.getThreshold_lower();
			double   threshold_upper = record.getThreshold_upper();
			String   items           = record.getItems();
			int      pc_count        = record.getPc_count();
			String   eigen_vector    = record.getEigen_vector();
			String   eigen_value     = record.getEigen_value();
			String   neigen_value    = record.getNeigen_value();
			String   cov_matrix      = record.getCov_matrix();
			String   imv_cov_matrix  = record.getImv_cov_matrix();
			String   mean_value      = record.getMean_value();
			String   sigma_value     = record.getSigma_value();
			double   confi_level     = record.getConfi_level();
			
			// get ruleset histogram
			RulesetPatternModel  rulesetPatternModel = this.rulesetModel.getRulesetPatternModel(pattern_id);
			if (rulesetPatternModel == null) {
				continue;
			}
			
			// add model
			RulesetPcaTss model = new RulesetPcaTss();
			model.setRulesetId(ruleset_id);
			model.setPatternId(pattern_id);
			model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
			model.setThresholds(new double[] {threshold_upper, threshold_lower});
			model.setItems(items);
			model.setPcCount(pc_count);
			model.setEigenVector(eigen_vector);
			model.setEigenValues(eigen_value);
			model.setNeigenValues(neigen_value);
			model.setCovMatrix(cov_matrix);
			model.setImvCovMatrix(imv_cov_matrix);
			model.setMean(mean_value);
			model.setSigma(sigma_value);
			model.setConfidenceLevel(confi_level);
			
			rulesetPatternModel.addDetectionRuleset(model);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * set ruleset_pattern_spe_mgmt
	 */
	public void setRulesetPatternSpeInfo(List<PatternRulesetSpeRecord> results) {
		
		// check data
		if (results == null) {
			return;
		}
		
		// convert from database record type to system model type
		for (PatternRulesetSpeRecord record : results) {
			
			// set parameters
			int      ruleset_id      = record.getRuleset_id();
			int      pattern_id      = record.getPattern_id();
			int      ruleset_enable  = record.getRuleset_enable();
			double   threshold_lower = record.getThreshold_lower();
			double   threshold_upper = record.getThreshold_upper();
			String   items           = record.getItems();
			int      pc_count        = record.getPc_count();
			String   eigen_vector    = record.getEigen_vector();
			String   eigen_value     = record.getEigen_value();
			String   neigen_value    = record.getNeigen_value();
			String   mean_value      = record.getMean_value();
			String   sigma_value     = record.getSigma_value();
			double   confi_level     = record.getConfi_level();
			
			// get ruleset histogram
			RulesetPatternModel  rulesetPatternModel = this.rulesetModel.getRulesetPatternModel(pattern_id);
			if (rulesetPatternModel == null) {
				continue;
			}
			
			// add model
			RulesetPcaSpe model = new RulesetPcaSpe();
			model.setRulesetId(ruleset_id);
			model.setPatternId(pattern_id);
			model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
			model.setThresholds(new double[] {threshold_upper, threshold_lower});
			model.setItems(items);
			model.setPcCount(pc_count);
			model.setEigenVector(eigen_vector);
			model.setEigenValues(eigen_value);
			model.setNeigenValues(neigen_value);
			model.setMean(mean_value);
			model.setSigma(sigma_value);
			model.setConfidenceLevel(confi_level);
			
			rulesetPatternModel.addDetectionRuleset(model);
			
		} // end of for-loop
		
	} // end of method	
	
	/**
	 * set ruleset_pattern_corr_mgmt
	 */
	public void setRulesetPatternCorrInfo(List<PatternRulesetCorrRecord> results) {
		
		// check data
		if (results == null) {
			return;
		}
		
		// convert from database record type to system model type
		for (PatternRulesetCorrRecord record : results) {
			
			// set parameters
			int      ruleset_id      = record.getRuleset_id();
			int      pattern_id      = record.getPattern_id();
			int      ruleset_enable  = record.getRuleset_enable();
			double   threshold_lower = record.getThreshold_lower();
			double   threshold_upper = record.getThreshold_upper();
			String   items           = record.getItems();
			double   rsquare         = record.getRsquare();
			String   cov_matrix      = record.getCov_matrix();
			String   imv_cov_matrix  = record.getImv_cov_matrix();
			String   mean_value      = record.getMean_value();
			String   sigma_value     = record.getSigma_value();
			double   confi_level     = record.getConfi_level();
			
			// get ruleset histogram
			RulesetPatternModel  rulesetPatternModel = this.rulesetModel.getRulesetPatternModel(pattern_id);
			if (rulesetPatternModel == null) {
				continue;
			}
			
			// add model
			RulesetCorr model = new RulesetCorr();
			model.setRulesetId(ruleset_id);
			model.setPatternId(pattern_id);
			model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
			model.setThresholds(new double[] {threshold_upper, threshold_lower});
			model.setItems(items);
			model.setrSquare(rsquare);
			model.setCovMatrix(cov_matrix);
			model.setImvCovMatrix(imv_cov_matrix);
			model.setMean(mean_value);
			model.setSigma(sigma_value);
			model.setConfidenceLevel(confi_level);
			
			rulesetPatternModel.addDetectionRuleset(model);
			
		} // end of for-loop
		
	} // end of method	
	
	/**
	 * set ruleset_pattern_spc_mgmt
	 */
	public void setRulesetPatternSpcInfo(List<PatternRulesetSpcRecord> results) {
		
		// check data
		if (results == null) {
			return;
		}
		
		// convert from database record type to system model type
		for (PatternRulesetSpcRecord record : results) {
			
			// set parameters
			int      ruleset_id      = record.getRuleset_id();
			int      pattern_id      = record.getPattern_id();
			int      ruleset_enable  = record.getRuleset_enable();
			double   threshold_lower = record.getThreshold_lower();
			double   threshold_upper = record.getThreshold_upper();
			int      item            = record.getItem();
			double   mean_value      = record.getMean_value();
			double   sigma_value     = record.getSigma_value();
			double   confi_level     = record.getConfi_level();
			
			// get ruleset histogram
			RulesetPatternModel  rulesetPatternModel = this.rulesetModel.getRulesetPatternModel(pattern_id);
			if (rulesetPatternModel == null) {
				continue;
			}
			
			// add model
			RulesetSpc model = new RulesetSpc();
			model.setRulesetId(ruleset_id);
			model.setPatternId(pattern_id);
			model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
			model.setThresholds(new double[] {threshold_upper, threshold_lower});
			model.setItem(item);
			model.setMean(mean_value);
			model.setSigma(sigma_value);
			model.setConfidenceLevel(confi_level);
			
			rulesetPatternModel.addDetectionRuleset(model);
			
		} // end of for-loop
		
	} // end of method		
	
	/**
	 * set ruleset_pattern_pd_mgmt
	 */
	public void setRulesetPatternPdInfo(List<PatternRulesetPdRecord> results) {
		
		// check data
		if (results == null) {
			return;
		}
		
		// convert from database record type to system model type
		for (PatternRulesetPdRecord record : results) {
			
			// set parameters
			int      ruleset_id      = record.getRuleset_id();
			int      pattern_id      = record.getPattern_id();
			int      ruleset_enable  = record.getRuleset_enable();
			double   threshold_lower = record.getThreshold_lower();
			double   threshold_upper = record.getThreshold_upper();
			int      item            = record.getItem();
			double   mean_value      = record.getMean_value();
			double   sigma_value     = record.getSigma_value();
			double   confi_level     = record.getConfi_level();
			
			// get ruleset histogram
			RulesetPatternModel  rulesetPatternModel = this.rulesetModel.getRulesetPatternModel(pattern_id);
			if (rulesetPatternModel == null) {
				continue;
			}
			
			// add model
			RulesetPd model = new RulesetPd();
			model.setRulesetId(ruleset_id);
			model.setPatternId(pattern_id);
			model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
			model.setThresholds(new double[] {threshold_upper, threshold_lower});
			model.setItem(item);
			model.setMean(mean_value);
			model.setSigma(sigma_value);
			model.setConfidenceLevel(confi_level);
			
			rulesetPatternModel.addDetectionRuleset(model);
			
		} // end of for-loop
		
	} // end of method		
	
	/**
	 * set ruleset_correction_mgmt
	 */
	public void setRulesetCorrectionInfo(List<RulesetCorrectionRecord> results) {
		
		// check data
		if (results == null) {
			return;
		}
		
		// convert from database record type to system model type
		for (RulesetCorrectionRecord record : results) {
			
			// set parameters
			//int       ruleset_id  = record.getRuleset_id();
			int       item_code   = record.getItem_code();
			double    std_oxgy    = record.getStd_oxgy();
			double    std_mois    = record.getStd_mois();
			int       oxyg_corr   = record.getOxyg_corr();
			int       temp_corr   = record.getTemp_corr();
			int       mois_corr   = record.getMois_corr();
			
			// add model
			this.rulesetModel.addRulesetCorrectionModel(new RulesetCorrectionModel(
					item_code, std_oxgy, std_mois,
					oxyg_corr, temp_corr, mois_corr));
			
		} // end of for-loop
		
	} // end of method			
	
	/**
	 * set ruleset_weighting_mgmt
	 */
	public void setRulesetWeightingInfo(RulesetWeightingRecord result) {
		
		// check data
		if (result == null) {
			return;
		}
		
		// set parameters
		//int  ruleset_id         = result.getRuleset_id();
		int  multi_weight       = result.getMulti_weight();
		int  bi_weight          = result.getBi_weight();
		int  uni_weight         = result.getUni_weight();
		int  corr_weight        = result.getCorr_weight();
		int  warn_threshold     = result.getWarn_threshold();
		int  minor_threshold    = result.getMinor_threshold();
		int  major_threshold    = result.getMajor_threshold();
		int  critical_threshold = result.getCritical_threshold();

		// add model
		this.rulesetModel.setRulesetWeightingFactors(
				multi_weight, bi_weight, uni_weight, corr_weight, 
				warn_threshold, minor_threshold, major_threshold, critical_threshold);
		
	} // end of method			
	
	/**
	 * Set RulesetModel
	 */
	public void applyRulesetModel() {
		
		// clear all previous patternData
		clearPatternData();
		
		// set weighting factor
		this.rulesetWeighingModel = this.rulesetModel.getRulesetWeightingModel();
		
		// get all pattern model
		RulesetPatternModel[] rulesetPatternModels = this.rulesetModel.getAllRulesetPatternModel();
		if (rulesetPatternModels == null) {
			return;
		}
		
		// assign pattern for all TmsDataRecords
		for (RulesetPatternModel model : rulesetPatternModels) {
			patternTmsDataRecordHash.put(model.getPatternId(), new Vector<TmsDataRecord>());
		} // end of for-loop
		
		TmsDataRecord[] tmsDataRecords = this.tmsDataset.getTmsDataRecords();
		this.items = rulesetPatternModels[0].getItems();
		for (TmsDataRecord tmsDataRecord : tmsDataRecords) {
			
			// 보정전 자료가 입력되지 않은 경우에는 Ruleset에 설정되어 있는 CorrectionInfo로 값을 치환
			DoAnalysisReverseCorrection.doReverseCorrectionForEmptyCell(tmsDataRecord, this.rulesetModel);
			
			// get conformity value for each Pattern
			double[] values = tmsDataRecord.getValues(items);
			PatternConformity[] patternConformities = new PatternConformity[rulesetPatternModels.length];
			for (int i=0 ; i<rulesetPatternModels.length ; i++) {
				
				patternConformities[i] = new PatternConformity(
						rulesetPatternModels[i].getPatternId(),
						rulesetPatternModels[i].getRulesetPatternConformity(items, values));
				//System.out.println(String.format(
				//		"PatternID[%3d] - Conformity[%6.30f", 
				//		patternConformities[i].patternId, 
				//		patternConformities[i].conformityValue));
			
			} // end of for-loop
			
			// sorting (descending)
			Arrays.sort(patternConformities);
			
			// add record
			patternTmsDataRecordHash.get(patternConformities[0].patternId).add(tmsDataRecord);
			
		} // end of for-loop
		
		
		// set detection pattern dataset
		for (RulesetPatternModel model : rulesetPatternModels) {
			
			// create DetectionPatternDataset
			int     patternId   = model.getPatternId();
			String  patternName = model.getPatternName();
			PatternDataHistogram[] patternDataHistograms = model.getPatternDataHistograms();
			double[][] itemDataRange = new double[items.length][2];
			for (int i=0 ; i<items.length ; i++) {
				itemDataRange[i][0] = patternDataHistograms[i].getMinRange();
				itemDataRange[i][1] = patternDataHistograms[i].getMaxRange();
			}
			
			DetectionPatternDataset dataset = new DetectionPatternDataset(
					this.factCode,
					this.stackCode,
					patternId, 
					patternName,
					items,
					rulsetBaseStartTime,
					rulsetBaseEndTime,
					itemDataRange,
					patternDataHistograms);
			dataset.setEnabled(model.isEnabled());
			dataset.setRulesetCreated(model.isRulesetCreated());
			dataset.setOperateStop(model.isOperateStop());
			dataset.setPcaResultModel(model.getPcaResultModel());
			
			// add data records
			Vector<TmsDataRecord> tmsDataRecordArray = patternTmsDataRecordHash.get(patternId);
			long time;
			TmsDataRecord tmsDataRecord;
			for (int j=0 ; j<tmsDataRecordArray.size() ; j++) {
				
				tmsDataRecord = tmsDataRecordArray.get(j);
				time = tmsDataRecord.getTime();
				TmsDataCell tmsDataCell;
				RawDataCell[] rawDataCells = new RawDataCell[items.length];
				
				for (int k=0 ; k<items.length ; k++) {
					
					tmsDataCell = tmsDataRecord.getDataCell(items[k]);
					rawDataCells[k] = new RawDataCell(
							tmsDataCell.getData_type(), 
							tmsDataCell.getData_value(), 
							tmsDataCell.getOrigin_value());
				}
				
				// add dataRecord
				dataset.addDetectionRecord(
						new DetectionRecord(dataset, time, patternId, patternName, rawDataCells));
			}
			
			// add ruleset
			DetectionRuleset[] rulesets = model.getDetectionRulesets();
			for (DetectionRuleset ruleset : rulesets) {
				dataset.addRuleset(ruleset);
			}
			
			// add pattern dataset
			this.detectionPatternDataset.add(dataset);
			
		} // end of for-loop
		
		// Set odrModel and detectionRecords Model
		this.odrRecords = this.createOdrModel();
		this.detectionRecords = this.createDetectionModel();
		
	} // end of method
	
	/**
	 * Initialize data
	 */
	public void clear() {
		
		this.tmsDataset = null;
		this.tms30minDataset = null;
		this.detectionPatternDataset.clear();
		this.rulesetWeighingModel = null;
		
		System.gc();
		
	} // end of method
	
	/**
	 * clear pattern data
	 */
	public void clearPatternData() {
		this.detectionPatternDataset.clear();
	} // end of method
	

	/**
	 * @return the factCode
	 */
	public String getFactCode() {
		return factCode;
	}

	/**
	 * @param factCode the factCode to set
	 */
	public void setFactCode(String factCode) {
		this.factCode = factCode;
	}

	/**
	 * @return the stackCode
	 */
	public int getStackCode() {
		return stackCode;
	}

	/**
	 * @param stackCode the stackCode to set
	 */
	public void setStackCode(int stackCode) {
		this.stackCode = stackCode;
	}

	/**
	 * @return the tmsDataSet
	 */
	public TmsDataset getTmsDataset() {
		return tmsDataset;
	} // end of method
	
	/**
	 * @return the tmsDataSet
	 */
	public Tms30minDataset get30minTmsDataset() {
		return tms30minDataset;
	} // end of method

	/**
	 * Add patternData
	 */
	public void addPatternData(DetectionPatternDataset patternData) {
		this.detectionPatternDataset.add(patternData);
	} // end of method
	
	/**
	 * @return the items
	 */
	public int[] getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(int[] items) {
		this.items = items;
	}

	/**
	 * Get Pattern Count
	 */
	public int getPatternCount() {
		return this.detectionPatternDataset.size();
	} // end of method
	
	/**
	
	/**
	 * Get patternData
	 */
	public DetectionPatternDataset[] getAllPatternData() {
		return this.detectionPatternDataset.toArray(
				new DetectionPatternDataset[this.detectionPatternDataset.size()]);
	} // end of method
	
	/**
	 * Get PatternData
	 */
	public DetectionPatternDataset getPatternData(int patternId) {
		
		// search patternData and return
		DetectionPatternDataset patternData;
		for (Enumeration<DetectionPatternDataset> e=this.detectionPatternDataset.elements(); e.hasMoreElements();) {
			patternData = e.nextElement();
			if (patternData.getPatternId() == patternId) {
				return patternData;
			}
		}
		
		// if matched patternData is not exist,
		return null;

	} // end of method

	/**
	 * @return the rulesetWeighingModel
	 */
	public RulesetWeightingModel getRulesetWeighingModel() {
		return rulesetWeighingModel;
	}
	
	/**
	 * get RulesetModel
	 */
	public RulesetModel getRulesetModel() {
		return this.rulesetModel;
	}

	/**
	 * @param rulesetWeighingModel the rulesetWeighingModel to set
	 */
	public void setRulesetWeighingModel(RulesetWeightingModel rulesetWeighingModel) {
		this.rulesetWeighingModel = rulesetWeighingModel;
	}

	/**
	 * @return the rulsetBaseStartTime
	 */
	public long getRulsetBaseStartTime() {
		return rulsetBaseStartTime;
	}

	/**
	 * @param rulsetBaseStartTime the rulsetBaseStartTime to set
	 */
	public void setRulsetBaseStartTime(long rulsetBaseStartTime) {
		this.rulsetBaseStartTime = rulsetBaseStartTime;
	}

	/**
	 * @return the rulsetBaseEndTime
	 */
	public long getRulsetBaseEndTime() {
		return rulsetBaseEndTime;
	}

	/**
	 * @param rulsetBaseEndTime the rulsetBaseEndTime to set
	 */
	public void setRulsetBaseEndTime(long rulsetBaseEndTime) {
		this.rulsetBaseEndTime = rulsetBaseEndTime;
	}
	
	/**
	 * get tms data records
	 */
	public TmsDataRecord[] getPatternTmsDataRecords(int patternId) {
		
		Vector<TmsDataRecord> patternTmsDataRecords = this.patternTmsDataRecordHash.get(patternId);
		if (patternTmsDataRecords == null) {
			return null;
		}
		return patternTmsDataRecords.toArray(new TmsDataRecord[patternTmsDataRecords.size()]);
		
	} // end of method
	
	/**
	 * get tms data records
	 */
	public int getPatternTmsDataRecordCount(int patternId) {
		
		Vector<TmsDataRecord> patternTmsDataRecords = this.patternTmsDataRecordHash.get(patternId);
		if (patternTmsDataRecords == null) {
			return 0;
		}
		return patternTmsDataRecords.size();
		
	} // end of method
	
	/**
	 * create OdrModel
	 */
	private OdrRecord[] createOdrModel() {
		
		// get Pattern Data model
		HashSet<String> multiRuleHashSet = new HashSet<String>();
		HashSet<String> biRuleHashSet    = new HashSet<String>();
		HashSet<String> uniRuleHashSet   = new HashSet<String>();
		HashSet<String> corrRuleHashSet  = new HashSet<String>();

		DetectionRuleset[] rulesets;
		for (int i=0 ; i<this.detectionPatternDataset.size() ; i++) {
			
			rulesets = this.detectionPatternDataset.get(i).getAllRuleset();
			for (int j=0 ; j<rulesets.length ; j++) {
				
				switch(rulesets[j].getRulesetType()) {
				case DetectionRuleset.RULESET_PCA_TSS:
					multiRuleHashSet.add(rulesets[j].getKeyName());
					break;
				case DetectionRuleset.RULESET_PCA_SPE:
					multiRuleHashSet.add(rulesets[j].getKeyName());
					break;
				case DetectionRuleset.RULESET_CORR:
					biRuleHashSet.add(rulesets[j].getKeyName());
					break;
				case DetectionRuleset.RULESET_SPC:
					uniRuleHashSet.add(rulesets[j].getKeyName());
					break;
				case DetectionRuleset.RULESET_PD:
					corrRuleHashSet.add(rulesets[j].getKeyName());
					break;
				
				}
			}
		}
		
		Hashtable<String, Integer> rulesetOrderHash = new Hashtable<String, Integer>();
		int index = 0;
		String key = "";
		for (Iterator<String> e=multiRuleHashSet.iterator() ; e.hasNext() ; ) {
			key = e.next();
			rulesetOrderHash.put(key, index++);
		}
		for (Iterator<String> e=biRuleHashSet.iterator() ; e.hasNext() ; ) {
			key = e.next();
			rulesetOrderHash.put(key, index++);
		}
		for (Iterator<String> e=uniRuleHashSet.iterator() ; e.hasNext() ; ) {
			key = e.next();
			rulesetOrderHash.put(key, index++);
		}
		for (Iterator<String> e=corrRuleHashSet.iterator() ; e.hasNext() ; ) {
			key = e.next();
			rulesetOrderHash.put(key, index++);
		}
		// add record
		Vector<OdrRecord> odrRecordArray = new Vector<OdrRecord>();
		DetectionRecord[] detectionRecords;
		String factCode, factName, patternName;
		int    stckCode, patternId;
		long   time;
		OdrRecord  odrRecord;
		DetectionPatternDataset detectionPatternDataset;
		for (int i=0 ; i<this.detectionPatternDataset.size() ; i++) {
			
			detectionPatternDataset = this.detectionPatternDataset.get(i);
			factCode    = detectionPatternDataset.getFactoryId();
			factName    = detectionPatternDataset.getFactoryName();
			stckCode    = detectionPatternDataset.getStackId();
			patternName = detectionPatternDataset.getPatternName();
			patternId   = detectionPatternDataset.getPatternId();
			detectionRecords = detectionPatternDataset.getAllDetectionRecords();
			for (int j=0 ; j<detectionRecords.length ; j++) {
				
				// create odrRecord
				time = detectionRecords[j].getTime();
				odrRecord = new OdrRecord(
						factCode, factName, stckCode, 
						time, patternId, patternName, rulesetOrderHash,
						rulesetWeighingModel);
				odrRecordArray.add(odrRecord);
				
				// set odrCell
				if (!detectionPatternDataset.isEnabled()) {
					continue;
				}
				DetectionDataCell[] outlierCells = detectionRecords[j].getAllDetectionDataCell();
				int  odrCellIndex;
				for (int k=0 ; k<outlierCells.length ; k++) {
					if (!outlierCells[k].getRuleset().isRulesetEnabled()) {
						continue;
					}
					odrCellIndex = rulesetOrderHash.get(outlierCells[k].getRuleset().getKeyName());
					odrRecord.odrCells[odrCellIndex].setIsOutlier(outlierCells[k].isOutlier());
				}
			}
		}
		
		// create return value
		OdrRecord[] odrRecords = odrRecordArray.toArray(new OdrRecord[odrRecordArray.size()]);
		Arrays.sort(odrRecords);
		
		// Test Print
		//for (OdrRecord record : odrRecords) {
		//	System.out.println(record.toString());
		//}
		
		return odrRecords;
		
	} // end of method
	
	/**
	 * create detection decords
	 */
	private DetectionRecord[] createDetectionModel() {
		
		// set data
		Vector<DetectionRecord> detectionRecordArray = new Vector<DetectionRecord>();
		DetectionRecord[] subRecords;
		for (int i=0 ; i<this.detectionPatternDataset.size() ; i++) {
			subRecords = this.detectionPatternDataset.get(i).getAllDetectionRecords();
			for (int j=0 ; j<subRecords.length ; j++) {
				detectionRecordArray.add(subRecords[j]);
			}
		}
		
		DetectionRecord[] detectionRecords = detectionRecordArray.toArray(
				new DetectionRecord[detectionRecordArray.size()]);
		Arrays.sort(detectionRecords);
		
		return detectionRecords;
		
	} // end of method
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class PatternConformity implements Comparable<PatternConformity> {
		
		//----------------------------------------------------------------------
		// Fields
		//----------------------------------------------------------------------
		public  int     patternId = 0;
		public  double  conformityValue = 0.0D;
		
		//----------------------------------------------------------------------
		// Constructor
		//----------------------------------------------------------------------
		public PatternConformity(int patternId, double conformityValue) {
			
			this.patternId = patternId;
			this.conformityValue = conformityValue;
			
		} // end of method
		
		//----------------------------------------------------------------------
		// Implemented method
		//----------------------------------------------------------------------
		/**
		 * compareTo
		 */
		public int compareTo(PatternConformity compare) {
			
			if (this.conformityValue > compare.conformityValue) {
				return -1;
			} else if (this.conformityValue < compare.conformityValue) {
				return 1;
			} else {
				return 0;
			}
			
		} // end of method
		
	} // end of inner class

	
} // end of class
