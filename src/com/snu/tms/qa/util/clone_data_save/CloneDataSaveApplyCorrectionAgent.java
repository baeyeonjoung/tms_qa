/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.util.clone_data_save;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.soap.QueryTmsRecord;
import com.snu.tms.qa.fileio.TmsDataWriter;
import com.snu.tms.qa.model.FactoryInfo;
import com.snu.tms.qa.model.InventoryDataManager;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.TmsDataCell;
import com.snu.tms.qa.model.TmsDataHeader;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;


/**
 * Analysis agent for General statistic
 * 
 */
public class CloneDataSaveApplyCorrectionAgent 
{
	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(CloneDataSaveApplyCorrectionAgent.class);
	
	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * 
	 */
	public static void doCloneDataSave(
			String  factoryCode,
			String  startTimeStr,
			String  endTimeStr) {
		
		// Get InventoryManager
		InventoryDataManager inventoryDataManager = InventoryDataManager.getInstance();
		FactoryInfo factInfo = inventoryDataManager.getFactoryInfo(factoryCode);
		
		if (factInfo == null) {
			logger.debug(String.format(
					"target factory can't find. factoryCode[%s]", 
					factoryCode));
			return;
		}
		
		logger.debug(String.format(
				"Clone data save process started. factoryCode[%s], factoryName[%s]", 
				factInfo.getCode(),
				factInfo.getFullName()));
		
		// Get all stackInfo
		StackInfo[] stacks = inventoryDataManager.getStackInfo(factoryCode);
		if (stacks == null || stacks.length == 0) {
			logger.debug(String.format(
					"There is no stack. factoryCode[%s]", 
					factoryCode));
			return;
		}
		
		// query stack data and save to a file
		for (int i=0 ; i<stacks.length ; i++) {
			
			logger.debug(String.format(
					"stack data quering and file save process started. stackNo[%d], stackName[%s]", 
					stacks[i].getStackNumber(),
					stacks[i].getStackName()));
			
			// query data
			TmsDataset dataset = QueryTmsRecord.queryTmsRecord(
					factInfo.getCode(), 
					stacks[i].getStackNumber(), 
					startTimeStr, 
					endTimeStr);
			
			// set TMS header
			TmsDataHeader tmsDataHeader = new  TmsDataHeader();
			tmsDataHeader.setFactoryInfo(factInfo);
			tmsDataHeader.setStackInfo(stacks[i]);
			dataset.setTmsDataHeader(tmsDataHeader);
			
			// check null
			if (dataset.getTmsDataRecordCount() == 0) {
				return;
			}
			
			// do reverse correction
			doReverseCorrection(dataset);
			
			// change value with reverse_correction value
			doReplaceValue(dataset);
			
			// file save
			String fileName = "./clone_data/reverse_correction/tms_data_" + 
					factInfo.getCode() + "_" + 
					stacks[i].getStackNumber() + "_" + 
					startTimeStr.substring(0, 10) + "_" + endTimeStr.substring(0, 10) + ".txt";
			
			TmsDataWriter.writeDataFile(fileName, dataset);
			
		} // end of for-loop
		
		
	} // end of method
	
	/**
	 * do replace value from revered correction value to value
	 */
	public static void doReplaceValue(TmsDataset tmsDataSet) {
		
		TmsDataRecord[] tmsDataRecords = tmsDataSet.getTmsDataRecords();
		TmsDataCell dataCell = null;
		for (TmsDataRecord tmsDataRecord : tmsDataRecords) {
			
			// change itemCode for NOX
			dataCell = tmsDataRecord.getDataCell(ItemCodeDefine.NOX);
			dataCell.setData_value(dataCell.getOrigin_value());

			// change itemCode for SO2
			dataCell = tmsDataRecord.getDataCell(ItemCodeDefine.SOX);
			dataCell.setData_value(dataCell.getOrigin_value());

			// change itemCode for FLW
			dataCell = tmsDataRecord.getDataCell(ItemCodeDefine.FL1);
			dataCell.setData_value(dataCell.getOrigin_value());

		}		
		
	} // end of method
	
	
	/**
	 * Do reverse correction
	 */
	public static void doReverseCorrection(TmsDataset tmsDataSet) {
		
		// STCK에 대한 표준산소농도가 정의되는게 맞지 않으므로 우선 Comment 처리 (2016-10-15)
		/**
		double stdOxyg = tmsDataSet.getTmsDataHeader().getStackInfo().getStandardOxygen();
		double tmsOxyg, tmsTmp;
		
		if (stdOxyg <= 0.0D) {
			return;
		}
		
		// do reverse correction
		TmsDataRecord[] tmsDataRecords = tmsDataSet.getTmsDataRecords();
		TmsDataCell dataCell = null;
		for (TmsDataRecord tmsDataRecord : tmsDataRecords) {
			
			// prepare correction value
			tmsOxyg = tmsDataRecord.getDataCell(ItemCodeDefine.O2).getData_value();
			tmsTmp  = tmsDataRecord.getDataCell(ItemCodeDefine.TMP).getData_value();
			
			// do reverse correction for each item
			for (int item=ItemCodeDefine.TSP ; item<=ItemCodeDefine.FL1 ; item++) {
				
				// apply only NOX, SOX, FLW
				if (item != ItemCodeDefine.NOX && item != ItemCodeDefine.SOX && item != ItemCodeDefine.FL1) {
					continue;
				}
				// Check data is valid
				dataCell = tmsDataRecord.getDataCell(item);
				if (dataCell.getData_value() == -1.0D) {
					continue;
				}
				
				// copy dataValue to originValue
				dataCell.setOrigin_value(dataCell.getData_value());
				
				// Do oxygen correction
				dataCell.setOrigin_value(DoAnalysisReverseCorrection.doOxygenReverseCorrection(
						dataCell.getOrigin_value(), stdOxyg, tmsOxyg));
				dataCell.setOxyg_correction(true);
				
				// Do temperature correction
				if (item == ItemCodeDefine.FL1) {
					dataCell.setOrigin_value(DoAnalysisReverseCorrection.doTemperatureReverseCorrection(
							dataCell.getOrigin_value(), tmsTmp));
					dataCell.setTemp_correction(true);
				}
			
			}
		}
		*/
		
	} // end of method
			
	/**
	 * Do temperature reverse correction
	 */
	public  static  double  doTemperatureReverseCorrection(
			double  afterValue,
			double  gauge_temp) {
		
		double  beforeValue = 0.0D;
		beforeValue = afterValue * (273.0D)/(273.0D + gauge_temp);
		if (beforeValue < 0.0D) {
			beforeValue = 0.0D;
		}
		
		return beforeValue;
	}
	
	/**
	 * Do Moisture reverse correction
	 */
	public  static  double  doMoistureReverseCorrection(
			double  afterValue,
			double  stck_mois) {
		
		double  beforeValue = 0.0D;
		if (stck_mois == 100.0D) {
			beforeValue = afterValue;
		} else {
			beforeValue = afterValue * (100.0D - stck_mois)/(100.0D);
		}
		if (beforeValue < 0.0D) {
			beforeValue = 0.0D;
		}
		
		return beforeValue;
	}
	
	/**
	 * Do Moisture reverse correction (flow)
	 */
	public  static  double  doMoistureReverseCorrectionFlow(
			double  afterValue,
			double  stck_mois) {
		
		double  beforeValue = 0.0D;
		if (stck_mois == 100.0D) {
			beforeValue = afterValue;
		} else {
			beforeValue = afterValue * (100.0D)/(100.0D - stck_mois);
		}
		if (beforeValue < 0.0D) {
			beforeValue = 0.0D;
		}
		
		return beforeValue;
	}
	
	/**
	 * Do oxygen reverse correction
	 */
	public  static  double  doOxygenReverseCorrection(
			double  afterValue,
			double  std_oxyg, 
			double  gauge_oxyg) {
		
		double  beforeValue = 0.0D;
		if (gauge_oxyg == 21.0D || std_oxyg == 21.0D) {
			beforeValue = afterValue;
		} else {
			beforeValue = afterValue * (21.0D - gauge_oxyg)/(21.0D - std_oxyg);
		}
		if (beforeValue < 0.0D) {
			beforeValue = 0.0D;
		}
		
		return beforeValue;
	}



	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================

	
	
	

} // end of class
