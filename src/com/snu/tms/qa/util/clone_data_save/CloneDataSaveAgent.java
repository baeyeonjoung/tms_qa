/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.util.clone_data_save;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.soap.QueryTmsRecord;
import com.snu.tms.qa.fileio.TmsDataWriter;
import com.snu.tms.qa.model.FactoryInfo;
import com.snu.tms.qa.model.InventoryDataManager;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.TmsDataset;


/**
 * Analysis agent for General statistic
 * 
 */
public class CloneDataSaveAgent 
{
	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(CloneDataSaveAgent.class);
	
	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * 
	 */
	public static void doCloneDataSave(
			String  factoryCode,
			String  startTimeStr,
			String  endTimeStr) {
		
		// Get InventoryManager
		InventoryDataManager inventoryDataManager = InventoryDataManager.getInstance();
		FactoryInfo factInfo = inventoryDataManager.getFactoryInfo(factoryCode);
		
		if (factInfo == null) {
			logger.debug(String.format(
					"target factory can't find. factoryCode[%s]", 
					factoryCode));
			return;
		}
		
		logger.debug(String.format(
				"Clone data save process started. factoryCode[%s], factoryName[%s]", 
				factInfo.getCode(),
				factInfo.getFullName()));
		
		// Get all stackInfo
		StackInfo[] stacks = inventoryDataManager.getStackInfo(factoryCode);
		if (stacks == null || stacks.length == 0) {
			logger.debug(String.format(
					"There is no stack. factoryCode[%s]", 
					factoryCode));
			return;
		}
		
		// query stack data and save to a file
		for (int i=0 ; i<stacks.length ; i++) {
			
			logger.debug(String.format(
					"stack data quering and file save process started. stackNo[%d], stackName[%s]", 
					stacks[i].getStackNumber(),
					stacks[i].getStackName()));
			
			// query data
			TmsDataset dataset = QueryTmsRecord.queryTmsRecord(
					factInfo.getCode(), 
					stacks[i].getStackNumber(), 
					startTimeStr, 
					endTimeStr);
			if (dataset.getTmsDataRecordCount() == 0) {
				return;
			}
			
			// file save
			String fileName = "./clone_data/tms_data_" + 
					factInfo.getCode() + "_" + 
					stacks[i].getStackNumber() + "_" + 
					startTimeStr.substring(0, 10) + "_" + endTimeStr.substring(0, 10) + ".txt";
			
			TmsDataWriter.writeDataFile(fileName, dataset);
			
		} // end of for-loop
		
		
	} // end of method
	



	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================

	
	
	

} // end of class
