package com.snu.tms.qa.util;

/*
 *  S-Series BSR (Bulk Statistics Report) Proxy
 *  (Convert BSR ==> Netflow or other flow format)
 *  Copyright (C) 2010 MobileConvergence
 *
 *  Written By Yeonjoung-Bae
 *  Written At 2010-03-17
 */

import java.io.*;

/**
 * File Copy, Delete, ...
 * 
 */
public class FileUtils 
{
	//=============================================================================================
	// Static Fields
	//=============================================================================================

	// ========================================================================
	//  Static Method
	// ========================================================================
	//---------------------------------------------------------------------------------------------
	// Copy File
	public static void copyFile(String srcFilePath, String dstFilePath)
	throws Exception {

		// Make File Object
		FileInputStream  inStream  = null;
		FileOutputStream outStream = null;
		try {
			inStream  = new FileInputStream(srcFilePath);
			outStream = new FileOutputStream(dstFilePath);
			int len;
			byte[] buf=new byte[2048];
			while ((len=inStream.read(buf))!=-1) {
				outStream.write(buf,0,len);
			}
			outStream.flush();
			inStream.close();
			outStream.close();
		} catch (Exception e) {
			try {
				inStream.close();
				outStream.close();
			} catch (Exception ee) {}
			throw new Exception("Can't copy file " + srcFilePath + " -> " + dstFilePath+"\n", e);
		}

	} // End of Method

	
	//---------------------------------------------------------------------------------------------
	// Create Folder
	public static void createFolder(String folderPath) {
		
		// Check if file is exist
		boolean isFileExist = false;
		File targetFile = new File(folderPath);
		isFileExist = targetFile.exists();
		
		// Create Folder
		if (isFileExist) {
			// Check this is folder
			boolean isFolder = targetFile.isDirectory();
			if (isFolder) {
				return;
			} else {
				deleteFile(folderPath);
				targetFile.mkdir();
			}
		} else {
			targetFile.mkdir();
		}
		
	} // End of Method
	
	
	//---------------------------------------------------------------------------------------------
	// Delete File
	public static void deleteFile(String filePath) {
		
		// Check if file is exist
		boolean isFileExist = false;
		File targetFile = new File(filePath);
		isFileExist = targetFile.exists();
		
		// File Delete
		if (isFileExist) {
			targetFile.delete();
		}
		
	} // End of Method
	
	//---------------------------------------------------------------------------------------------
	// Delete File
	public static void deleteAllFile(String filePath) {
		
		// Check if file is exist
		boolean isFileExist = false;
		File targetFile = new File(filePath);
		isFileExist = targetFile.exists();
		// File Delete
		if (isFileExist) {
			iterateDeleteFile(targetFile);
		}
		
	} // End of Method	
	
	//---------------------------------------------------------------------------------------------
	// Delete Old Files
	public static void deleteOldFiles(String filePath, long timeThreshold) {
		
		// Check File Exist
		File targetFile = new File(filePath);
		if (!targetFile.exists()) {
			return;
		}
		
		// Is File
		if (targetFile.isFile()) {
			if (targetFile.lastModified() < timeThreshold) {
				try {targetFile.delete(); } catch (Exception e) {}
			}
		}
		
		// Is Directory
		else {
			File[] children = targetFile.listFiles();
			if (children == null || children.length == 0) {
				return;
			} else {
				for (int i=0 ; i<children.length ; i++) {
					if (children[i].lastModified() < timeThreshold) {
						try {children[i].delete(); } catch (Exception e) {}
					}
				}
			}
		}
		
	} // End of Method
	
	//---------------------------------------------------------------------------------------------
	// Iterate Delete
	public static void iterateDeleteFile(File parentFile) {
		
		File[] children = parentFile.listFiles();
		if (children == null || children.length == 0) {
			//try {parentFile.delete(); } catch (Exception e) {}
		} else {
			for (int i=0 ; i<children.length ; i++) {
				if (children[i].isDirectory())
					iterateDeleteFile(children[i]);
				else
					try {children[i].delete(); } catch (Exception e) {}
			}
			//try {parentFile.delete(); } catch (Exception e) {}
		}
		
	} // End of Method
	

	// ========================================================================
	//  Static Private Method
	// ========================================================================
	//---------------------------------------------------------------------------------------------

	

} // END OF CLASS

