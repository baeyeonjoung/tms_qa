package com.snu.tms.qa.util;

/*
 *  S-Series BSR (Bulk Statistics Report) Proxy
 *  (Convert BSR ==> Netflow or other flow format)
 *  Copyright (C) 2010 MobileConvergence
 *
 *  Written By Yeonjoung-Bae
 *  Written At 2010-03-17
 */

import java.text.DecimalFormat;

/**
 * Utility for converting Date format
 */
public class NumberUtil 
{

	// Static Fields
	//==========================================================================
	public static DecimalFormat simpleFormatter = 
		new DecimalFormat("#,###,###,###,##0.000");
	
	public static DecimalFormat simpleIntegerFormatter = 
			new DecimalFormat("#,###,###,###,##0");

	public static DecimalFormat simpleNonCommaFormatter = 
			new DecimalFormat("############0.000");
		
		public static DecimalFormat simpleNonCommaIntegerFormatter = 
				new DecimalFormat("############0");

	//==========================================================================
	// Utility Methods
	//==========================================================================
	/** Return this formatted number  */
	public static String getNumberStr(double number) {
		
//System.out.println(number);
		
		if (number == Double.NaN) {
//System.out.println("===============");
			return " ";
		}
		try {
			return simpleFormatter.format(number);
		} catch (Exception e) {
			return " ";
		}
		
	} // End of Method
	
	
	/** Return this formatted number  */
	public static String getNumberStr(int number) {
		
		try {
			return simpleIntegerFormatter.format(number);
		} catch (Exception e) {
			return " ";
		}
		
	} // End of Method
	
	
	/** Return this formatted number  */
	public static String getNonCommaNumberStr(double number) {
		
//System.out.println(number);
		
		if (number == Double.NaN) {
//System.out.println("===============");
			return "";
		}
		try {
			return simpleNonCommaFormatter.format(number);
		} catch (Exception e) {
			return "";
		}
		
	} // End of Method
	
	
	/** Return this formatted number  */
	public static String getNonCommaNumberStr(int number) {
		
		try {
			return simpleNonCommaIntegerFormatter.format(number);
		} catch (Exception e) {
			return "";
		}
		
	} // End of Method

	

} // End of Method