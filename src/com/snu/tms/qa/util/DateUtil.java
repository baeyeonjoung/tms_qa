package com.snu.tms.qa.util;

/*
 *  S-Series BSR (Bulk Statistics Report) Proxy
 *  (Convert BSR ==> Netflow or other flow format)
 *  Copyright (C) 2010 MobileConvergence
 *
 *  Written By Yeonjoung-Bae
 *  Written At 2010-03-17
 */

import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * Utility for converting Date format
 */
public class DateUtil 
{

	// Static Fields
	//==========================================================================
	public static SimpleDateFormat basicDateFormatter = 
		new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static SimpleDateFormat tmsDateFormatter = 
			new SimpleDateFormat("yyyyMMddHHmm");

	public static SimpleDateFormat fileDateFormatter  = 
		new SimpleDateFormat("yyMMddHHmmss");
	
	public static SimpleDateFormat simpleDateFormatter  = 
			new SimpleDateFormat("yy/MM/dd HH");
	
	public static SimpleDateFormat simpleMinFormatter  = 
			new SimpleDateFormat("yy/MM/dd HH:mm");

	//==========================================================================
	// Utility Methods
	//==========================================================================
	/** Return this time dateString in "yyyy-MM-dd HH:mm:ss"  */
	public static String getCurrDateStr() {
		
		try {
			return basicDateFormatter.format(new Date(System.currentTimeMillis()));
		} catch (Exception e) {
			return "";
		}
		
	} // End of Method
	
	/** Return dateString in "yyyy-MM-dd HH:mm:ss"  */
	public static String getDateStr(long timestamp) {

		try {
			return basicDateFormatter.format(new Date(timestamp));
		} catch (Exception e) {
			return "";
		}
		
	} // End of Method
	
	/** Return dateString in "yyyy-MM-dd HH:mm:ss"  */
	public static String getSimpleDateStr(long timestamp) {

		try {
			return simpleDateFormatter.format(new Date(timestamp));
		} catch (Exception e) {
			return "";
		}
		
	} // End of Method
	
	/** Return dateString in "yyyy-MM-dd HH:mm:ss"  */
	public static String getSimpleMinStr(long timestamp) {

		try {
			return simpleMinFormatter.format(new Date(timestamp));
		} catch (Exception e) {
			return "";
		}
		
	} // End of Method
	
	/** Return dateString in "yyyy-MM-dd HH:mm:ss"  */
	public static String getDateStr(int year, int month, int day) {

		try {
			Calendar calendar = Calendar.getInstance();
			calendar.set(year, month, day, 0, 0, 0);
			return basicDateFormatter.format(calendar.getTime());
		} catch (Exception e) {
			return "";
		}
		
	} // End of Method


	/**
	 * parse time string
	 */
	public static long getDate(String timeStr) {
		try {
			return basicDateFormatter.parse(timeStr).getTime();
		} catch (Exception e) {
			return 0L;
		}
	}
	

	/** Return dateString in "yyyyMMddHHmm"  */
	public static String getTmsDateStr(long timestamp) {

		try {
			return tmsDateFormatter.format(new Date(timestamp));
		} catch (Exception e) {
			return "";
		}
		
	} // End of Method

	
	/** Return dateString in "yyyyMMddHHmm"  */
	public static String getTmsDateStr(int year, int month, int day) {

		try {
			Calendar calendar = Calendar.getInstance();
			calendar.set(year, month, day, 0, 0, 0);
			return tmsDateFormatter.format(calendar.getTime());
		} catch (Exception e) {
			return "";
		}
		
	} // End of Method

	/**
	 * parse time string
	 */
	public static long getTmsDate(String timeStr) {
		try {
			return tmsDateFormatter.parse(timeStr).getTime();
		} catch (Exception e) {
			return 0L;
		}
	}
	
	
	
	/** Return time-based file name in "yyMMddHHmmss" */
	public static String getFileDateStr(long timestamp) {
		
		try {
			return fileDateFormatter.format(new Date(timestamp));
		} catch (Exception e) {
			return "";
		}
		
	} // End of Method
	
	/** Return time value by parsing the time-based file name in "yyMMddHHmmss" */
	public static long getFileDate(String fileDateStr) {
		
		try {
			return fileDateFormatter.parse(fileDateStr).getTime();
		} catch (Exception e) {
			return 0L;
		}
		
	} // End of Method
	

} // End of Method