/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.startup;

import java.awt.EventQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.gui.Login_TmsQA_GUI;
import com.snu.tms.qa.gui.Startup_TmsQA_GUI;
import com.snu.tms.qa.model.InventoryDataManager;
import com.snu.tms.qa.model.LoginUserManager;
import com.snu.tms.qa.soap.SoapClientManager;

/**
 * Setting the program startup process.
 * 
 */
public class Startup_TmsQA {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(Startup_TmsQA.class);

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Setup logging and other required library
		try {
			org.apache.log4j.PropertyConfigurator.configure("./conf/log.properties");
			logger.info("TMS Quality Assessment process is stating .............");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("./conf/log.properties file can't found.\n"
					+ "Process halted and system will be exit.");
			System.exit(1);
		}
		
		/**
		 * 네트워크 보안 상의 이유로 직접 데이터베이스를 접속하지 않고,
		 * WAS 서버에서 제송하는 SOAP을 이용한다.

		// Initialize database connection
		try {
			SqlMapClientManager.getSqlMapClient();
			logger.info( "Database for TMS connection established." );
			
		} catch (Exception e) {
			logger.fatal( 
					"Database connection can't be established.");
			logger.fatal( "Process is halted and system will be exit." );
			System.exit(-1);
		}
		*/
		
		// Initialize soap client context
		try {
			SoapClientManager.getContext();
			logger.info( "SOAP client connection estabilished." );
		} catch (Exception e) {
			logger.error( "SOAP client connection failed." );
		}
		
		// show login window
		new Login_TmsQA_GUI();
		
		// check user is authorized
		LoginUserManager loginUserManager = LoginUserManager.getInstance();
		if (loginUserManager.getUserId().equals("")) {
			logger.fatal( "Login failed. System will be halted." );
			System.exit(-1);
		}

		// Initialize InventoryDataManager
		InventoryDataManager.getInstance();
		
		// show window
		final Startup_TmsQA_GUI window = Startup_TmsQA_GUI.getInstance();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window.mainFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
