/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.startup;

import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.database.QueryCorrectionInfo;
import com.snu.tms.qa.database.QueryFactInfo;
import com.snu.tms.qa.database.QueryStckInfo;
import com.snu.tms.qa.soap.QueryTmsRecord;
import com.snu.tms.qa.database.SqlMapClientManager;
import com.snu.tms.qa.model.FactoryInfo;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.db.CorrectionRecord;
import com.snu.tms.qa.model.db.FactRecord;
import com.snu.tms.qa.model.db.StckRecord;
import com.snu.tms.qa.util.DateUtil;

/**
 * Setting the program startup process.
 * 
 */
public class Startup_TmsQA_Test_Tibero {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(Startup_TmsQA_Test_Tibero.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Setup logging and other required library
		try {
			org.apache.log4j.PropertyConfigurator.configure("./conf/log.properties");
			logger.info("TMS Quality Assessment process is stating .............");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("./conf/log.properties file can't found.\n"
					+ "Process halted and system will be exit.");
			System.exit(1);
		}
		
		// Initialize database connection
		try {
			SqlMapClientManager.getSqlMapClient();
			logger.info( "Database connection established." );
			
		} catch (Exception e) {
			logger.fatal( 
					"Database connection can't be established.");
			logger.fatal( "Process is halted and system will be exit." );
			System.exit(-1);
		}
		
		// Initialize InventoryDataManager
		//InventoryDataManager.getInstance();
		TestTiberoDatabase();
		

	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructors
	//==========================================================================
	
	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Test Tibero Database query
	 */
	public static void TestTiberoDatabase() {
		
		// Test FactInfo query
		FactoryInfo factoryInfo = null;
		Vector<FactRecord> factories = QueryFactInfo.queryAllFactInfo();
		for (FactRecord factRecord : factories) {
			factoryInfo = new FactoryInfo(
							factRecord.getFact_code(),
							factRecord.getFact_nams(),
							factRecord.getFact_naml(),
							factRecord.getFact_namt(),
							factRecord.getFact_addr(),
							factRecord.getFact_prod(),
							factRecord.getFact_numb(),
							factRecord.getArea_cod2());
			System.out.println(factoryInfo + "\n");
		}
		
		// Test StckInfo query
		Vector<StckRecord> stacks = QueryStckInfo.queryAllStckInfo();
		for (StckRecord stckRecord : stacks) {
			System.out.println(stckRecord + "\n");
		}
		
		// Test CorrectionInfo query
		Vector<CorrectionRecord> correctionFactors = QueryCorrectionInfo.queryAllCorrectionInfo();
		for (CorrectionRecord correctionFactor : correctionFactors) {
			System.out.println(correctionFactor.toString() + "\n");
		}
		
		//QueryTmsRecord.queryTmsRecord("", "150061", 11, "2014-01-01 00:00:00", "2015-01-01 00:00:00");
		QueryTmsRecord.queryTms30minRecord("150061", 11, 
				DateUtil.getDate("2014-01-01 00:00:00"), DateUtil.getDate("2015-01-01 00:00:00"));
		
	} // end of method

} // end of class
