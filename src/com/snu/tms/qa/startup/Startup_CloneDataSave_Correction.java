/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.startup;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



//import com.snu.tms.qa.analysis.Agent4GeneralStatsAnalysis;
import com.snu.tms.qa.database.SqlMapClientManager;
import com.snu.tms.qa.model.InventoryDataManager;
import com.snu.tms.qa.util.clone_data_save.CloneDataSaveApplyCorrectionAgent;



/**
 * Setting the program startup process.
 * 
 */
public class Startup_CloneDataSave_Correction {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(Startup_CloneDataSave_Correction.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Setup logging and other required library
		try {
			org.apache.log4j.PropertyConfigurator.configure("./conf/log.properties");
			logger.info("TMS Quality Assessment process is stating .............");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("./conf/log.properties file can't found.\n"
					+ "Process halted and system will be exit.");
			System.exit(1);
		}
		
		// Initialize database connection
		try {
			SqlMapClientManager.getSqlMapClient();
			logger.info( "Database for TMS(CleanSYS and nSKY) connection established." );
			
		} catch (Exception e) {
			logger.fatal( 
					"Database connection can't be established.");
			logger.fatal( "Process is halted and system will be exit." );
			System.exit(-1);
		}
		
		// Initialize InventoryDataManager
		InventoryDataManager.getInstance();
		
		// data query and save
		CloneDataSaveApplyCorrectionAgent.doCloneDataSave("150123", "2013-01-01 00:00:00", "2014-01-01 00:00:00");
		CloneDataSaveApplyCorrectionAgent.doCloneDataSave("150123", "2014-01-01 00:00:00", "2015-01-01 00:00:00");
		CloneDataSaveApplyCorrectionAgent.doCloneDataSave("150067", "2013-01-01 00:00:00", "2014-01-01 00:00:00");
		CloneDataSaveApplyCorrectionAgent.doCloneDataSave("150067", "2014-01-01 00:00:00", "2015-01-01 00:00:00");
		
		// clone data save
		

	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
