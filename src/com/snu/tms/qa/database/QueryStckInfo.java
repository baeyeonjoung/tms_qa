/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.db.StckRecord;

/**
 * Query Agent
 * 
 */
public class QueryStckInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryStckInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	
	/**
	 * For MySQL database
	 */
	public static Vector<StckRecord> queryAllStckInfo(String  db_name) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			String queryName = "QueryStckInfo.SelectAllStckInfo";
			@SuppressWarnings("unchecked")
			List<StckRecord> results = (List<StckRecord>) 
				sqlMapClient.queryForList(queryName);

			//for (StckRecord result : results) {
			//	System.out.println(result.toString() + "\n");
			//}

			// return
			Vector<StckRecord> resultArray = new Vector<StckRecord>();
			for (StckRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<StckRecord>();
		}

	} // end of method
	
	
	/**
	 * For MySQL database
	 */
	public static StckRecord queryStckInfo(String  db_name, String fact_code, int stck_code) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			StckRecord stckRecord = new StckRecord();
			stckRecord.setFact_code(fact_code);
			stckRecord.setStck_code(stck_code);
			
			String queryName = "QueryStckInfo.SelectStckInfo";
			StckRecord result = (StckRecord) 
				sqlMapClient.queryForObject(queryName, stckRecord);

			System.out.println(result.toString() + "\n");

			// return
			return result;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return null;
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static Vector<StckRecord> queryAllStckInfo() {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			String queryName = "QueryStckInfo.SelectAllStckInfo";
			@SuppressWarnings("unchecked")
			List<StckRecord> results = (List<StckRecord>) 
				sqlMapClient.queryForList(queryName);

			// return
			Vector<StckRecord> resultArray = new Vector<StckRecord>();
			for (StckRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<StckRecord>();
		}

	} // end of method
	
	
	/**
	 * @param args
	 */
	public static StckRecord queryStckInfo(String fact_code, int stck_code) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			StckRecord stckRecord = new StckRecord();
			stckRecord.setFact_code(fact_code);
			stckRecord.setStck_code(stck_code);
			
			String queryName = "QueryStckInfo.SelectStckInfo";
			StckRecord result = (StckRecord) 
				sqlMapClient.queryForObject(queryName, stckRecord);

			// return
			return result;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return null;
		}

	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
