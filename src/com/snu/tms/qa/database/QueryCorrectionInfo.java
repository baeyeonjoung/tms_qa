/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.db.CorrectionRecord;

/**
 * Query Agent
 * 
 */
public class QueryCorrectionInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryCorrectionInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	
	/**
	 * @param args
	 */
	public static Vector<CorrectionRecord> queryAllCorrectionInfo() {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			Vector<CorrectionRecord> resultArray = new Vector<CorrectionRecord>();
			String queryName = "QueryCorrectionInfo.SelectAllCorrectionInfo";
			@SuppressWarnings("unchecked")
			List<CorrectionRecord> results = (List<CorrectionRecord>)sqlMapClient.queryForList(queryName);

			for (CorrectionRecord result : results) {
				resultArray.add(result);
			}

			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<CorrectionRecord>();
		}

	} // end of method
	
	public static Vector<CorrectionRecord> queryAllCorrectionInfoMysql() {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			Vector<CorrectionRecord> resultArray = new Vector<CorrectionRecord>();
			String queryName = "QueryCorrectionInfo.SelectAllCorrectionInfo";
			@SuppressWarnings("unchecked")
			List<CorrectionRecord> results = (List<CorrectionRecord>)sqlMapClient.queryForList(queryName);

			for (CorrectionRecord result : results) {
				resultArray.add(result);
			}

			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<CorrectionRecord>();
		}

	} // end of method


	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
