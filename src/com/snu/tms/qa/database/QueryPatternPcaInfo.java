/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.db.PatternPcaRecord;
import com.snu.tms.qa.model.db.StackKeyRecord;

/**
 * Query Agent
 * 
 */
public class QueryPatternPcaInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryPatternPcaInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static Vector<PatternPcaRecord> queryAllPatternPcaInfo(String factCode, int stckCode) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// set stck key
			StackKeyRecord keyRecord = new StackKeyRecord(factCode, stckCode);
			
			// Get all tableName
			String queryName = "QueryPatternPcaInfo.SelectAllPatternPcaInfo";
			@SuppressWarnings("unchecked")
			List<PatternPcaRecord> results = (List<PatternPcaRecord>) 
				sqlMapClient.queryForList(queryName, keyRecord);

			// return
			Vector<PatternPcaRecord> resultArray = new Vector<PatternPcaRecord>();
			for (PatternPcaRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<PatternPcaRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static Vector<PatternPcaRecord> queryAllPatternPcaInfo(int rulsetId) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			String queryName = "QueryPatternPcaInfo.SelectPatternPcaInfo";
			@SuppressWarnings("unchecked")
			List<PatternPcaRecord> results = (List<PatternPcaRecord>) 
				sqlMapClient.queryForList(queryName, rulsetId);

			// return
			Vector<PatternPcaRecord> resultArray = new Vector<PatternPcaRecord>();
			for (PatternPcaRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<PatternPcaRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertPatternPcaInfo(PatternPcaRecord patternRecord) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do insert query
			String queryName = "QueryPatternPcaInfo.InsertPatternPcaInfo";
			sqlMapClient.insert(queryName, patternRecord);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}

	} // end of method
	
	/**
	 * delete all pattern related with ruleset id
	 */
	public static void deletePatternPcaInfo(int rulesetId) {
		
		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do delete query
			String queryName = "QueryPatternPcaInfo.DeletePatternPcaInfo";
			sqlMapClient.update(queryName, rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
