/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.db.ExhaustRecord;

/**
 * Query Agent
 * 
 */
public class QueryExhaustInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryExhaustInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static Vector<ExhaustRecord> queryAllExhaustInfo() {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			String queryName = "QueryExhaustInfo.SelectAllExhaustInfo";
			@SuppressWarnings("unchecked")
			List<ExhaustRecord> results = (List<ExhaustRecord>) 
				sqlMapClient.queryForList(queryName);

			// return
			Vector<ExhaustRecord> resultArray = new Vector<ExhaustRecord>();
			for (ExhaustRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<ExhaustRecord>();
		}

	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
