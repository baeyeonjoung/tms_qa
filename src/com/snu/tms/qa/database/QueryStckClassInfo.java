/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.db.StckClassRecord;

/**
 * Query Agent
 * 
 */
public class QueryStckClassInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryStckClassInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	
	/**
	 * @param args
	 */
	public static Vector<StckClassRecord> queryAllStckClassInfo() {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			String queryName = "QueryStckClassInfo.SelectAllStckClassInfo";
			@SuppressWarnings("unchecked")
			List<StckClassRecord> results = (List<StckClassRecord>) 
				sqlMapClient.queryForList(queryName);

			// return
			Vector<StckClassRecord> resultArray = new Vector<StckClassRecord>();
			for (StckClassRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<StckClassRecord>();
		}

	} // end of method
	

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
