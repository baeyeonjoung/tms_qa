/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.db.PatternRulesetCorrRecord;
import com.snu.tms.qa.model.db.StackKeyRecord;

/**
 * Query Agent
 * 
 */
public class QueryPatternRulesetCorr {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryPatternRulesetCorr.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static Vector<PatternRulesetCorrRecord> queryAllRuleset(String factCode, int stckCode) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// set stck key
			StackKeyRecord keyRecord = new StackKeyRecord(factCode, stckCode);
			
			// Get all tableName
			String queryName = "QueryPatternRulesetCorr.SelectAllRecord";
			@SuppressWarnings("unchecked")
			List<PatternRulesetCorrRecord> results = (List<PatternRulesetCorrRecord>) 
				sqlMapClient.queryForList(queryName, keyRecord);

			// return
			Vector<PatternRulesetCorrRecord> resultArray = new Vector<PatternRulesetCorrRecord>();
			for (PatternRulesetCorrRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<PatternRulesetCorrRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static Vector<PatternRulesetCorrRecord> queryAllRuleset(int rulsetId) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			String queryName = "QueryPatternRulesetCorr.SelectRecord";
			@SuppressWarnings("unchecked")
			List<PatternRulesetCorrRecord> results = (List<PatternRulesetCorrRecord>) 
				sqlMapClient.queryForList(queryName, rulsetId);

			// return
			Vector<PatternRulesetCorrRecord> resultArray = new Vector<PatternRulesetCorrRecord>();
			for (PatternRulesetCorrRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<PatternRulesetCorrRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertRuleset(PatternRulesetCorrRecord rulesetRecord) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do insert query
			String queryName = "QueryPatternRulesetCorr.InsertRecord";
			sqlMapClient.insert(queryName, rulesetRecord);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}

	} // end of method
	
	/**
	 * delete all pattern related with ruleset id
	 */
	public static void deleteRuleset(int rulesetId) {
		
		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do delete query
			String queryName = "QueryPatternRulesetCorr.DeleteRecord";
			sqlMapClient.update(queryName, rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
