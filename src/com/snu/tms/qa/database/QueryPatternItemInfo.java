/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.db.PatternItemRecord;
import com.snu.tms.qa.model.db.StackKeyRecord;

/**
 * Query Agent
 * 
 */
public class QueryPatternItemInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryPatternItemInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static Vector<PatternItemRecord> queryAllPatternItemInfo(String factCode, int stckCode) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// set stck key
			StackKeyRecord keyRecord = new StackKeyRecord(factCode, stckCode);
			
			// Get all tableName
			String queryName = "QueryPatternItemInfo.SelectAllPatternItemInfo";
			@SuppressWarnings("unchecked")
			List<PatternItemRecord> results = (List<PatternItemRecord>) 
				sqlMapClient.queryForList(queryName, keyRecord);

			// return
			Vector<PatternItemRecord> resultArray = new Vector<PatternItemRecord>();
			for (PatternItemRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<PatternItemRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static Vector<PatternItemRecord> queryAllPatternInfo(int rulsetId) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			String queryName = "QueryPatternItemInfo.SelectPatternItemInfo";
			@SuppressWarnings("unchecked")
			List<PatternItemRecord> results = (List<PatternItemRecord>) 
				sqlMapClient.queryForList(queryName, rulsetId);

			// return
			Vector<PatternItemRecord> resultArray = new Vector<PatternItemRecord>();
			for (PatternItemRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<PatternItemRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertPatternItemInfo(PatternItemRecord patternRecord) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do insert query
			String queryName = "QueryPatternItemInfo.InsertPatternItemInfo";
			sqlMapClient.insert(queryName, patternRecord);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}

	} // end of method
	
	/**
	 * delete all pattern related with ruleset id
	 */
	public static void deletePatternItemInfo(int rulesetId) {
		
		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do delete query
			String queryName = "QueryPatternItemInfo.DeletePatternItemInfo";
			sqlMapClient.update(queryName, rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
