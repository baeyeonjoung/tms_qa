/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;


import java.io.File;
import java.io.FileInputStream;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

/**
 * Using iBATIS Framework as a Database Connection
 * This class managing the SqlMap Connection via iBATIS Framework
 *
 */
public class SqlMapClientManager 
{
	//==========================================================================
	// Fields
	//==========================================================================
    private static final SqlMapClient sqlMap;

	static {
    	try {
    		String resource = new File("conf/dbconfig.xml").getAbsolutePath();

    		FileInputStream reader=new FileInputStream(resource);
    		sqlMap = SqlMapClientBuilder.buildSqlMapClient(reader);

    	} catch (Exception e) {
    		e.printStackTrace();
    		throw new RuntimeException(e);
    	}
	}

	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor for SqlMapClientManager
	 * (Using Singleton Pattern)
	 * 
	 */
	private SqlMapClientManager(){}

	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Get SqlMapClient
	 * (Singleton Pattern)
	 */
	public static SqlMapClient getSqlMapClient() {
		return sqlMap;  
	} // end of method

} // end of class
