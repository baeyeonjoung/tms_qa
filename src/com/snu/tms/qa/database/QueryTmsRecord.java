/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.Tms30minDataset;
import com.snu.tms.qa.model.TmsDataHeader;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.db.Tms30minDataCellDB;
import com.snu.tms.qa.model.db.Tms30minDataRecordDB;
import com.snu.tms.qa.model.db.TmsDataCellDB;
import com.snu.tms.qa.model.db.TmsDataRecordDB;
import com.snu.tms.qa.util.DateUtil;

/**
 * Query Agent
 * 
 */
public class QueryTmsRecord {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryTmsRecord.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	
	/**
	 * @param start_time_str: time format (yyyy-MM-dd HH:mm:ss)
	 * @param end_time_str: time format (yyyy-MM-dd HH:mm:ss)
	 */
	@SuppressWarnings("unchecked")
	public static TmsDataset queryTmsRecord(
			String  db_name, String fact_code, int stck_code, 
			String start_time_str, String end_time_str) {
		
		// Query FactInfo
		//FactRecord fact_record = QueryFactInfo.queryFactInfo(db_name, fact_code);
		//StckRecord stck_record = QueryStckInfo.queryStckInfo(db_name, fact_code, stck_code);
		
		// Create TmsDataSet
		TmsDataset tmsDataSet = new TmsDataset();
		TmsDataHeader tmsDataHeader = new TmsDataHeader();
		tmsDataHeader.setStart_time_str(start_time_str);
		tmsDataHeader.setEnd_time_str(end_time_str);
		tmsDataSet.setTmsDataHeader(tmsDataHeader);
		
		// Query TmsData
		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			TmsDataRecordDB tmsDataRecordDB = new TmsDataRecordDB();
			tmsDataRecordDB.setFact_code(fact_code);
			tmsDataRecordDB.setStck_code(stck_code);
			tmsDataRecordDB.setStart_time_str(DateUtil.getTmsDateStr(tmsDataHeader.getStart_time()));
			tmsDataRecordDB.setEnd_time_str(DateUtil.getTmsDateStr(tmsDataHeader.getEnd_time()));
			
			String queryName = "QueryTmsRecord.SelectAllTmsRecord";

			long queryStartTime = System.currentTimeMillis();
			logger.debug(String.format(
					"QueryStart (db_name: %s, fact_code: %s, stck_code: %d, start_time: %s, end_time: %s)", 
					db_name, fact_code, stck_code, start_time_str, end_time_str));
			
			long targetTime = 0L;
			List<TmsDataCellDB> results = (List<TmsDataCellDB>) 
				sqlMapClient.queryForList(queryName, tmsDataRecordDB);
			for (TmsDataCellDB result : results) {
				targetTime = DateUtil.getTmsDate(result.getMin_time());
				tmsDataSet.getTmsDataRecord(targetTime).setValue(
						result.getItem_code(), result.getMin_valu(), result.getMin_valu_bf());
			}
			
			long queryFinishTime = System.currentTimeMillis();
			logger.debug(String.format(
					"Query Finished. (query_time: %d seconds, query_records: %d\ndb_name: %s, fact_code: %s, stck_code: %d, start_time :%s, end_time: %s)", 
					((int) (queryFinishTime - queryStartTime)/1000L), results.size(),
					db_name, fact_code, stck_code, start_time_str, end_time_str));
			//System.out.println(tmsDataSet.toString());
			
			return tmsDataSet;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error( e.toString() );
			return tmsDataSet;
		}

	} // end of method
	
	/**
	 * @param start_time_str: time format (yyyy-MM-dd HH:mm:ss)
	 * @param end_time_str: time format (yyyy-MM-dd HH:mm:ss)
	 */
	@SuppressWarnings("unchecked")
	public static Tms30minDataset queryTms30minRecord(
			String  db_name, String fact_code, int stck_code, 
			long start_time, long end_time) {
		
		// Create TmsDataSet
		Tms30minDataset tmsDataSet = new Tms30minDataset();
		tmsDataSet.init(start_time, end_time);
		
		// Query TmsData
		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			Tms30minDataRecordDB tmsDataRecordDB = new Tms30minDataRecordDB();
			tmsDataRecordDB.setFact_code(fact_code);
			tmsDataRecordDB.setStck_code(stck_code);
			tmsDataRecordDB.setStart_time_str(DateUtil.getTmsDateStr(start_time));
			tmsDataRecordDB.setEnd_time_str(DateUtil.getTmsDateStr(end_time));
			
			String queryName = "QueryTms30minRecord.SelectAllTmsRecord";

			long queryStartTime = System.currentTimeMillis();
			logger.debug(String.format(
					"QueryStart (db_name: %s, fact_code: %s, stck_code: %d, start_time: %s, end_time: %s)", 
					db_name, fact_code, stck_code, DateUtil.getTmsDateStr(start_time), DateUtil.getTmsDateStr(end_time)));
			
			long targetTime = 0L;
			List<Tms30minDataCellDB> results = (List<Tms30minDataCellDB>) 
				sqlMapClient.queryForList(queryName, tmsDataRecordDB);
			for (Tms30minDataCellDB result : results) {
				
				targetTime = DateUtil.getTmsDate(result.getHalf_time());
				tmsDataSet.getTmsDataRecord(targetTime).setValue(
						result.getItem_code(), 
						result.getHalf_valu(),
						result.getHalf_over(),
						result.getHalf_stat(),
						result.getHalf_dlst(),
						result.getStat_code(),
						result.getStat_valu());
			}
			
			long queryFinishTime = System.currentTimeMillis();
			logger.debug(String.format(
					"Query Finished. (query_time: %d seconds, query_records: %d\n 30min db_name: %s, fact_code: %s, stck_code: %d, start_time :%s, end_time: %s)", 
					((int) (queryFinishTime - queryStartTime)/1000L), results.size(),
					db_name, fact_code, stck_code, 
					DateUtil.getTmsDateStr(start_time), DateUtil.getTmsDateStr(end_time)));

			
			System.out.println(tmsDataSet.toString());
			
			return tmsDataSet;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error( e.toString() );
			return tmsDataSet;
		}

	} // end of method
	

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
