/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.PatternDataHistogram;
import com.snu.tms.qa.model.db.PatternItemRecord;
import com.snu.tms.qa.model.db.PatternPcaRecord;
import com.snu.tms.qa.model.db.PatternRecord;
import com.snu.tms.qa.model.db.PatternRulesetCorrRecord;
import com.snu.tms.qa.model.db.PatternRulesetPdRecord;
import com.snu.tms.qa.model.db.PatternRulesetSpcRecord;
import com.snu.tms.qa.model.db.PatternRulesetSpeRecord;
import com.snu.tms.qa.model.db.PatternRulesetTssRecord;
import com.snu.tms.qa.model.db.RulesetCorrectionRecord;
import com.snu.tms.qa.model.db.RulesetRecord;
import com.snu.tms.qa.model.db.RulesetWeightingRecord;
import com.snu.tms.qa.model.ruleset.DetectionRuleset;
import com.snu.tms.qa.model.ruleset.PcaResultModel;
import com.snu.tms.qa.model.ruleset.RulesetCorr;
import com.snu.tms.qa.model.ruleset.RulesetCorrectionModel;
import com.snu.tms.qa.model.ruleset.RulesetModel;
import com.snu.tms.qa.model.ruleset.RulesetPatternModel;
import com.snu.tms.qa.model.ruleset.RulesetPcaSpe;
import com.snu.tms.qa.model.ruleset.RulesetPcaTss;
import com.snu.tms.qa.model.ruleset.RulesetPd;
import com.snu.tms.qa.model.ruleset.RulesetSpc;
import com.snu.tms.qa.model.ruleset.RulesetWeightingModel;
import com.snu.tms.qa.util.DateUtil;

/**
 * Query Agent
 * 
 */
public class RulesetModelUpdateAgent {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(RulesetModelUpdateAgent.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * update ruleset staet
	 */
	public static void updateRulesetState(RulesetModel rulesetModel) {
		
		// update table
		queryUpdate_ruleset_mgmt(rulesetModel);
		
		// logging
		logger.debug("ruleset_mgmt table updated.\n" + rulesetModel.toString());
		
	} // end of method
	
	/**
	 * update ruleset model
	 */
	public static int updateRulesetModel(RulesetModel rulesetModel) {
		
		// logging
		logger.debug("Start update ruleset model.\n" + rulesetModel.toString());
		
		// ruleset_mgmt update
		// if RulesetModel ID is "-1" ==> insert
		int rulesetId = -1;
		if (rulesetModel.getRulesetId() == -1) {
			rulesetId = doInsertRulesetModel(rulesetModel);
		}
		
		// if rulesetModel is alreay exist
		else {
			doUpdateRulesetModel(rulesetModel);
			rulesetId = rulesetModel.getRulesetId();
		}

		// return
		return rulesetId;
		
	} // end of method
	
	/**
	 * Insert Ruleset model
	 */
	private static int doInsertRulesetModel(RulesetModel rulesetModel) {
		
		// insert ruleset_mgmt
		int rulesetId = queryInsert_ruleset_mgmt(rulesetModel);
		rulesetModel.setRulesetId(rulesetId);
		logger.debug("ruleset_mgmt table updated.");
		
		// insert pattern
		queryInsert_pattern_mgmt(rulesetModel);
		logger.debug("pattern_mgmt table updated.");
		
		// insert pattern_item
		queryInsert_pattern_item_mgmt(rulesetModel);
		logger.debug("pattern_item_mgmt table updated.");
		
		// insert pattern_pca
		queryInsert_pattern_pca_mgmt(rulesetModel);
		logger.debug("pattern_pca_mgmt table updated.");
		
		// insert pattern_ruleset
		queryInsert_pattern_ruleset_mgmt(rulesetModel);
		logger.debug("ruleset_pattern_mgmt table updated.");
		
		// insert correction_model
		queryInsert_ruleset_correction_mgmt(rulesetModel);
		logger.debug("ruleset_correction_mgmt table updated.");

		// insert weighting_model
		queryInsert_ruleset_weighting_mgmt(rulesetModel);
		logger.debug("ruleset_weight_mgmt table updated.");

		return rulesetId;
		
	} // end of method
	
	/**
	 * update Ruleset model
	 */
	private static void doUpdateRulesetModel(RulesetModel rulesetModel) {
		
		// update ruleset_mgmt
		queryUpdate_ruleset_mgmt(rulesetModel);
		logger.debug("ruleset_mgmt table updated.");
		
		// delete all pattern
		queryDelete_pattern_mgmt(rulesetModel.getRulesetId());
		logger.debug("pattern_mgmt table deleted. (" + rulesetModel.getRulesetId() + ")");
		
		// insert pattern
		queryInsert_pattern_mgmt(rulesetModel);
		logger.debug("pattern_mgmt table updated.");

		// delete all pattern_item
		queryDelete_pattern_item_mgmt(rulesetModel.getRulesetId());
		logger.debug("pattern_item_mgmt table deleted. (" + rulesetModel.getRulesetId() + ")");
		
		// insert pattern_item
		queryInsert_pattern_item_mgmt(rulesetModel);
		logger.debug("pattern_item_mgmt table updated.");

		// delete all pattern_pca
		queryDelete_pattern_pca_mgmt(rulesetModel.getRulesetId());
		logger.debug("pattern_pca_mgmt table deleted. (" + rulesetModel.getRulesetId() + ")");

		// insert pattern_pca
		queryInsert_pattern_pca_mgmt(rulesetModel);
		logger.debug("pattern_pca_mgmt table updated.");

		// delete all pattern_pca
		queryDelete_pattern_ruleset_mgmt(rulesetModel.getRulesetId());
		logger.debug("ruleset_xx_mgmt table deleted. (" + rulesetModel.getRulesetId() + ")");

		// insert pattern_pca
		queryInsert_pattern_ruleset_mgmt(rulesetModel);
		logger.debug("ruleset_xx_mgmt table updated.");
		
		// delete all ruleset_correction
		queryDelete_ruleset_correction_mgmt(rulesetModel.getRulesetId());
		logger.debug("ruleset_correction_mgmt table deleted. (" + rulesetModel.getRulesetId() + ")");
		
		// insert correction_model
		queryInsert_ruleset_correction_mgmt(rulesetModel);
		logger.debug("ruleset_correction_mgmt table updated.");

		// delete all ruleset_weight
		queryDelete_ruleset_weighting_mgmt(rulesetModel.getRulesetId());
		logger.debug("ruleset_weight_mgmt table deleted. (" + rulesetModel.getRulesetId() + ")");

		// insert weighting_model
		queryInsert_ruleset_weighting_mgmt(rulesetModel);
		logger.debug("ruleset_weight_mgmt table updated.");

		
	} // end of method
	
	/**
	 * delete Ruleset model
	 */
	public static void doDeleteRulesetModel(RulesetModel rulesetModel) {
		
		// update ruleset_mgmt
		queryDelete_ruleset_mgmt(rulesetModel.getRulesetId());
		
		// delete all pattern
		queryDelete_pattern_mgmt(rulesetModel.getRulesetId());

		// delete all pattern_item
		queryDelete_pattern_item_mgmt(rulesetModel.getRulesetId());

		// delete all pattern_pca
		queryDelete_pattern_pca_mgmt(rulesetModel.getRulesetId());

		// delete all pattern_pca
		queryDelete_pattern_ruleset_mgmt(rulesetModel.getRulesetId());
		
		// delete all ruleset_correction
		queryDelete_ruleset_correction_mgmt(rulesetModel.getRulesetId());
		
		// delete all ruleset_weight
		queryDelete_ruleset_weighting_mgmt(rulesetModel.getRulesetId());

		// logging
		logger.debug("rulesetModel deleted.\n" + rulesetModel.toString());

	} // end of method
	
	/**
	 * Insert query for ruleset_mgmt
	 */
	private static int queryInsert_ruleset_mgmt(RulesetModel rulesetModel) {
		
		// set ruleset record and update query
		RulesetRecord rulesetRecord = new RulesetRecord();
		
		rulesetRecord.setFact_code(rulesetModel.getFactCode());
		rulesetRecord.setStck_code(rulesetModel.getStckCode());
		rulesetRecord.setRuleset_name(rulesetModel.getRulesetName());
		rulesetRecord.setRuleset_descr(rulesetModel.getRulesetDescr());
		rulesetRecord.setRuleset_state(rulesetModel.getRulesetState());
		rulesetRecord.setStart_time(DateUtil.getDateStr(rulesetModel.getRulesetStartTime()));
		rulesetRecord.setEnd_time(DateUtil.getDateStr(rulesetModel.getRulesetEndTime()));
		rulesetRecord.setUser_id(rulesetModel.getUserId());
		rulesetRecord.setCreate_time(DateUtil.getCurrDateStr());
		
		int rulesetId = QueryRulesetInfo.insertRulesetInfo(rulesetRecord);
		return rulesetId;
		
	} // end of method
	
	/**
	 * Update query for ruleset_mgmt
	 */
	private static void queryUpdate_ruleset_mgmt(RulesetModel rulesetModel) {
		
		// set ruleset record and update query
		RulesetRecord rulesetRecord = new RulesetRecord();
		
		rulesetRecord.setRuleset_id(rulesetModel.getRulesetId());
		rulesetRecord.setFact_code(rulesetModel.getFactCode());
		rulesetRecord.setStck_code(rulesetModel.getStckCode());
		rulesetRecord.setRuleset_name(rulesetModel.getRulesetName());
		rulesetRecord.setRuleset_descr(rulesetModel.getRulesetDescr());
		rulesetRecord.setRuleset_state(rulesetModel.getRulesetState());
		rulesetRecord.setStart_time(DateUtil.getDateStr(rulesetModel.getRulesetStartTime()));
		rulesetRecord.setEnd_time(DateUtil.getDateStr(rulesetModel.getRulesetEndTime()));
		rulesetRecord.setUser_id(rulesetModel.getUserId());
		rulesetRecord.setCreate_time(DateUtil.getCurrDateStr());
		
		QueryRulesetInfo.updateRulesetInfo(rulesetRecord);
		
	} // end of method
	
	/**
	 * delete ruleset_mgmt
	 */
	private static void queryDelete_ruleset_mgmt(int rulesetId) {
		QueryRulesetInfo.deleteRulesetInfo(rulesetId);
	} // end of method
	
	/**
	 * delete pattern_mgmt
	 */
	private static void queryDelete_pattern_mgmt(int rulesetId) {
		QueryPatternInfo.deletePatternInfo(rulesetId);
	} // end of method
	
	/**
	 * insert pattern_mgmt
	 */
	private static void queryInsert_pattern_mgmt(RulesetModel rulesetModel) {
		
		// Get PatternInfo
		RulesetPatternModel[] patternModels = rulesetModel.getAllRulesetPatternModel();
		for (RulesetPatternModel patternModel : patternModels) {
			
			// insert pattern record
			PatternRecord patternRecord = new PatternRecord(
					rulesetModel.getRulesetId(),
					patternModel.getPatternId(),
					patternModel.getPatternName(),
					(patternModel.isEnabled() ? 1 : 0),
					(patternModel.isRulesetCreated() ? 1 : 0),
					(patternModel.isOperateStop() ? 1 : 0),
					patternModel.getItemCount(),
					patternModel.getItemStr());
			
			// insert query
			QueryPatternInfo.insertPatternInfo(patternRecord);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * delete pattern_item_mgmt
	 */
	private static void queryDelete_pattern_item_mgmt(int rulesetId) {
		QueryPatternItemInfo.deletePatternItemInfo(rulesetId);
	} // end of method
	
	/**
	 * insert pattern_item_mgmt
	 */
	private static void queryInsert_pattern_item_mgmt(RulesetModel rulesetModel) {
		
		// Get PatternInfo
		RulesetPatternModel[] patternModels = rulesetModel.getAllRulesetPatternModel();
		for (RulesetPatternModel patternModel : patternModels) {
			
			// get all item info
			PatternDataHistogram[] patternDataHistograms = patternModel.getPatternDataHistograms();

			// insert pattern record
			for (PatternDataHistogram patternDataHistogram : patternDataHistograms) {
				
				PatternItemRecord patternItemRecord = new PatternItemRecord(
						rulesetModel.getRulesetId(),
						patternModel.getPatternId(),
						patternDataHistogram.getItem(),
						ItemCodeDefine.getItemName(patternDataHistogram.getItem()),
						patternDataHistogram.getMinRange(),
						patternDataHistogram.getMaxRange(),
						patternDataHistogram.getBinNo(),
						patternDataHistogram.getInterval(),
						patternDataHistogram.getDensitiesString());
				
				// insert query
				QueryPatternItemInfo.insertPatternItemInfo(patternItemRecord);
			
			} // end of for-loop
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * delete pattern_pca_mgmt
	 */
	private static void queryDelete_pattern_pca_mgmt(int rulesetId) {
		QueryPatternPcaInfo.deletePatternPcaInfo(rulesetId);
	} // end of method
	
	/**
	 * insert pattern_pca_mgmt
	 */
	private static void queryInsert_pattern_pca_mgmt(RulesetModel rulesetModel) {
		
		// Get PatternInfo
		RulesetPatternModel[] patternModels = rulesetModel.getAllRulesetPatternModel();
		for (RulesetPatternModel patternModel : patternModels) {
			
			// get pca result model
			PcaResultModel pcaResultModel = patternModel.getPcaResultModel();

			// insert pattern pca record
			PatternPcaRecord patternPcaRecord = new PatternPcaRecord(
					rulesetModel.getRulesetId(),
					patternModel.getPatternId(),
					pcaResultModel.getItemStr(),
					pcaResultModel.getPcCount(),
					pcaResultModel.getEigenVectorStr(),
					pcaResultModel.getEigenValueStr(),
					pcaResultModel.getNeigenValueStr());
			
			// insert query
			QueryPatternPcaInfo.insertPatternPcaInfo(patternPcaRecord);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * delete ruleset_xx_mgmt
	 */
	private static void queryDelete_pattern_ruleset_mgmt(int rulesetId) {
		
		// delete ruleset_tss_mgmt
		QueryPatternRulesetTss.deleteRuleset(rulesetId);

		// delete ruleset_spe_mgmt
		QueryPatternRulesetSpe.deleteRuleset(rulesetId);

		// delete ruleset_corr_mgmt
		QueryPatternRulesetCorr.deleteRuleset(rulesetId);

		// delete ruleset_spc_mgmt
		QueryPatternRulesetSpc.deleteRuleset(rulesetId);

		// delete ruleset_pd_mgmt
		QueryPatternRulesetPd.deleteRuleset(rulesetId);
		
	} // end of method
	
	/**
	 * delete ruleset_correction_mgmt
	 */
	private static void queryDelete_ruleset_correction_mgmt(int rulesetId) {
		
		// delete ruleset_correction_mgmt
		QueryRulesetCorrectionInfo.deleteRulesetCorrectionInfo(rulesetId);

	} // end of method
	
	/**
	 * delete ruleset_weight_mgmt
	 */
	private static void queryDelete_ruleset_weighting_mgmt(int rulesetId) {
		
		// delete ruleset_weight_mgmt
		QueryRulesetWeightingInfo.deleteRulesetWeightingInfo(rulesetId);

	} // end of method
	
	/**
	 * insert pattern_pca_mgmt
	 */
	private static void queryInsert_pattern_ruleset_mgmt(RulesetModel rulesetModel) {
		
		// Get PatternInfo
		RulesetPatternModel[] patternModels = rulesetModel.getAllRulesetPatternModel();
		for (RulesetPatternModel patternModel : patternModels) {
			
			// get ruleset
			DetectionRuleset[] detectionRulesets = patternModel.getDetectionRulesets();
			
			// insert ruleset
			for (DetectionRuleset detectionRuleset : detectionRulesets) {
				
				if (detectionRuleset instanceof RulesetPcaTss) {
					
					RulesetPcaTss ruleset = (RulesetPcaTss)detectionRuleset;
					PatternRulesetTssRecord rulesetRecord = new PatternRulesetTssRecord(
							rulesetModel.getRulesetId(),
							patternModel.getPatternId(),
							(ruleset.isRulesetEnabled() ? 1 : 0),
							ruleset.getThresholds()[1],
							ruleset.getThresholds()[0],
							ruleset.getItemStr(),
							ruleset.getPcCount(),
							ruleset.getEigenVectorStr(),
							ruleset.getEigenValueStr(),
							ruleset.getNeigenValueStr(),
							ruleset.getCovMatrixStr(),
							ruleset.getImvCovMatrixStr(),
							ruleset.getMeanStr(),
							ruleset.getSigmaStr(),
							ruleset.getConfidenceLevel());
					QueryPatternRulesetTss.insertRuleset(rulesetRecord);
					
				} else if (detectionRuleset instanceof RulesetPcaSpe) {
					
					RulesetPcaSpe ruleset = (RulesetPcaSpe)detectionRuleset;
					PatternRulesetSpeRecord rulesetRecord = new PatternRulesetSpeRecord(
							rulesetModel.getRulesetId(),
							patternModel.getPatternId(),
							(ruleset.isRulesetEnabled() ? 1 : 0),
							ruleset.getThresholds()[1],
							ruleset.getThresholds()[0],
							ruleset.getItemStr(),
							ruleset.getPcCount(),
							ruleset.getEigenVectorStr(),
							ruleset.getEigenValueStr(),
							ruleset.getNeigenValueStr(),
							ruleset.getMeanStr(),
							ruleset.getSigmaStr(),
							ruleset.getConfidenceLevel());
					QueryPatternRulesetSpe.insertRuleset(rulesetRecord);
					
				} else if (detectionRuleset instanceof RulesetCorr) {
					
					RulesetCorr ruleset = (RulesetCorr)detectionRuleset;
					PatternRulesetCorrRecord rulesetRecord = new PatternRulesetCorrRecord(
							rulesetModel.getRulesetId(),
							patternModel.getPatternId(),
							(ruleset.isRulesetEnabled() ? 1 : 0),
							ruleset.getThresholds()[1],
							ruleset.getThresholds()[0],
							ruleset.getItemStr(),
							ruleset.getrSquare(),
							ruleset.getCovMatrixStr(),
							ruleset.getImvCovMatrixStr(),
							ruleset.getMeanStr(),
							ruleset.getSigmaStr(),
							ruleset.getConfidenceLevel());
					QueryPatternRulesetCorr.insertRuleset(rulesetRecord);
					
				} else if (detectionRuleset instanceof RulesetSpc) {
				
					RulesetSpc ruleset = (RulesetSpc)detectionRuleset;
					PatternRulesetSpcRecord rulesetRecord = new PatternRulesetSpcRecord(
							rulesetModel.getRulesetId(),
							patternModel.getPatternId(),
							(ruleset.isRulesetEnabled() ? 1 : 0),
							ruleset.getThresholds()[1],
							ruleset.getThresholds()[0],
							ruleset.getItem(),
							ruleset.getMean(),
							ruleset.getSigma(),
							ruleset.getConfidenceLevel());
					QueryPatternRulesetSpc.insertRuleset(rulesetRecord);
					
				} else if (detectionRuleset instanceof RulesetPd) {
					
					RulesetPd ruleset = (RulesetPd)detectionRuleset;
					PatternRulesetPdRecord rulesetRecord = new PatternRulesetPdRecord(
							rulesetModel.getRulesetId(),
							patternModel.getPatternId(),
							(ruleset.isRulesetEnabled() ? 1 : 0),
							ruleset.getThresholds()[1],
							ruleset.getThresholds()[0],
							ruleset.getItem(),
							ruleset.getMean(),
							ruleset.getSigma(),
							ruleset.getConfidenceLevel());
					QueryPatternRulesetPd.insertRuleset(rulesetRecord);
					
				}
			} // end of for-loop
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * insert ruleset_correction_mgmt
	 */
	private static void queryInsert_ruleset_correction_mgmt(RulesetModel rulesetModel) {
		
		// Get CorrectionInfo
		RulesetCorrectionModel[] correctionModels = rulesetModel.getAllRulesetCorrectionModel();
		for (RulesetCorrectionModel correctionModel : correctionModels) {
			
			// insert correction record
			RulesetCorrectionRecord correctionRecord = new RulesetCorrectionRecord(
					rulesetModel.getRulesetId(),
					correctionModel.getItemCode(),
					correctionModel.getStdOxygen(),
					correctionModel.getStdMoisture(),
					correctionModel.isOxygenApplied(),
					correctionModel.isTemperatureApplied(),
					correctionModel.isMoistureApplied());
			
			// insert query
			QueryRulesetCorrectionInfo.insertRulesetCorrectionInfo(correctionRecord);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * insert ruleset_weighting_mgmt
	 */
	private static void queryInsert_ruleset_weighting_mgmt(RulesetModel rulesetModel) {
		
		// insert weighting record
		RulesetWeightingModel weightingModel = rulesetModel.getRulesetWeightingModel();
		RulesetWeightingRecord weightingRecord = new RulesetWeightingRecord(
				rulesetModel.getRulesetId(),
				weightingModel.getMultivariateRulesetWeighting(),
				weightingModel.getBivariateRulesetWeighting(),
				weightingModel.getUnivariateRulesetWeighting(),
				weightingModel.getCorrectionRulesetWeighting(),
				weightingModel.getWarningLevel(),
				weightingModel.getMinorLevel(),
				weightingModel.getMajorLevel(),
				weightingModel.getCriticalLevel());
		
			// insert query
			QueryRulesetWeightingInfo.insertRulesetWeightingInfo(weightingRecord);
			
	} // end of method


	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
