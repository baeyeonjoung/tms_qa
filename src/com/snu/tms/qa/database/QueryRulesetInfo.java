/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.db.RulesetRecord;

/**
 * Query Agent
 * 
 */
public class QueryRulesetInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryRulesetInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static Vector<RulesetRecord> queryAllRulesetInfo() {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			String queryName = "QueryRulesetInfo.SelectAllRulesetInfo";
			@SuppressWarnings("unchecked")
			List<RulesetRecord> results = (List<RulesetRecord>) 
				sqlMapClient.queryForList(queryName);

			// return
			Vector<RulesetRecord> resultArray = new Vector<RulesetRecord>();
			for (RulesetRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<RulesetRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static Vector<RulesetRecord> queryAllRulesetInfo(String factCode, int stckCode) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			RulesetRecord record = new RulesetRecord();
			record.setFact_code(factCode);
			record.setStck_code(stckCode);
			
			// Get all tableName
			String queryName = "QueryRulesetInfo.SelectRulesetInfo";
			@SuppressWarnings("unchecked")
			List<RulesetRecord> results = (List<RulesetRecord>) 
				sqlMapClient.queryForList(queryName, record);

			// return
			Vector<RulesetRecord> resultArray = new Vector<RulesetRecord>();
			for (RulesetRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<RulesetRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static RulesetRecord queryDefaultRulesetInfo(String factCode, int stckCode) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			RulesetRecord record = new RulesetRecord();
			record.setFact_code(factCode);
			record.setStck_code(stckCode);
			
			// Get all tableName
			String queryName = "QueryRulesetInfo.SelectDefaultRulesetInfo";
			RulesetRecord result = (RulesetRecord) sqlMapClient.queryForObject(queryName, record);

			return result;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return null;
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static RulesetRecord queryTargetRulesetInfo(int rulesetId) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			RulesetRecord record = new RulesetRecord();
			record.setRuleset_id(rulesetId);
			
			// Get all tableName
			String queryName = "QueryRulesetInfo.SelectTargetRulesetInfo";
			RulesetRecord result = (RulesetRecord) sqlMapClient.queryForObject(queryName, record);

			return result;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return null;
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static int insertRulesetInfo(RulesetRecord rulesetRecord) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do insert query
			String queryName = "QueryRulesetInfo.InsertRulesetInfo";
			sqlMapClient.insert(queryName, rulesetRecord);
			
			// do query ruleset id
			queryName = "QueryRulesetInfo.SelectLastRulesetId";
			Integer rulesetId = (Integer)sqlMapClient.queryForObject(queryName, rulesetRecord);
			
			return rulesetId.intValue();

		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
			return -1;
		}

	} // end of method
	
	
	/**
	 * @param args
	 */
	public static void updateRulesetInfo(RulesetRecord rulesetRecord) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do update query
			String queryName = "QueryRulesetInfo.UpdateRulesetInfo";
			sqlMapClient.insert(queryName, rulesetRecord);

		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}

	} // end of method
	
	/**
	 * delete ruleset recrod
	 */
	public static void deleteRulesetInfo(int rulesetId) {
		
		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do delete query
			String queryName = "QueryRulesetInfo.DeleteRulesetInfo";
			sqlMapClient.update(queryName, rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
