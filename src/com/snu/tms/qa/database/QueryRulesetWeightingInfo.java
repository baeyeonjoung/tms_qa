/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.db.RulesetCorrectionRecord;
import com.snu.tms.qa.model.db.RulesetWeightingRecord;
import com.snu.tms.qa.model.db.StackKeyRecord;

/**
 * Query Agent
 * 
 */
public class QueryRulesetWeightingInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryRulesetWeightingInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static Vector<RulesetWeightingRecord> queryAllRulesetWeightingInfo() {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			String queryName = "QueryRulesetWeightingInfo.SelectAllRulesetWeightingInfo";
			@SuppressWarnings("unchecked")
			List<RulesetWeightingRecord> results = (List<RulesetWeightingRecord>) 
				sqlMapClient.queryForList(queryName);

			// return
			Vector<RulesetWeightingRecord> resultArray = new Vector<RulesetWeightingRecord>();
			for (RulesetWeightingRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<RulesetWeightingRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static Vector<RulesetWeightingRecord> queryAllRulesetWeightingInfo(String factCode, int stckCode) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// set stck key
			StackKeyRecord keyRecord = new StackKeyRecord(factCode, stckCode);
			
			// Get all tableName
			String queryName = "QueryRulesetWeightingInfo.SelectAllRecord";
			@SuppressWarnings("unchecked")
			List<RulesetWeightingRecord> results = (List<RulesetWeightingRecord>) 
				sqlMapClient.queryForList(queryName, keyRecord);

			// return
			Vector<RulesetWeightingRecord> resultArray = new Vector<RulesetWeightingRecord>();
			for (RulesetWeightingRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<RulesetWeightingRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static RulesetWeightingRecord queryRulesetWeightingInfo(int rulesetId) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			String queryName = "QueryRulesetWeightingInfo.SelectRulesetWeightingInfo";
			RulesetWeightingRecord result = (RulesetWeightingRecord) 
				sqlMapClient.queryForObject(queryName, rulesetId);

			// return
			return result;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new RulesetWeightingRecord();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertRulesetWeightingInfo(RulesetWeightingRecord RulesetWeightingRecord) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do insert query
			String queryName = "QueryRulesetWeightingInfo.InsertRulesetWeightingInfo";
			sqlMapClient.insert(queryName, RulesetWeightingRecord);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}

	} // end of method

	/**
	 * delete ruleset recrod
	 */
	public static void deleteRulesetWeightingInfo(int rulesetId) {
		
		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do delete query
			String queryName = "QueryRulesetWeightingInfo.DeleteRulesetWeightingInfo";
			sqlMapClient.update(queryName, rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
