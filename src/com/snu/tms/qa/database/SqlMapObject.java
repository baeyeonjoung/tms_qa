/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import com.ibatis.sqlmap.client.SqlMapClient;

/**
 * Using iBATIS Framework as a Database Connection
 * This Class is SqlMapObject for mapping iBATIS Framework with real Database Connection
 *
 */
public class SqlMapObject 
{
	//==========================================================================
	// Fields
	//==========================================================================
	protected static SqlMapClient 	sqlMap 	= SqlMapClientManager.getSqlMapClient();

} // end of class
