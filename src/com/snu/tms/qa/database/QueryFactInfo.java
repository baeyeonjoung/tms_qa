/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.db.FactRecord;

/**
 * Query Agent
 * 
 */
public class QueryFactInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryFactInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static Vector<FactRecord> queryAllFactInfo() {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			String queryName = "QueryFactInfo.SelectAllFactInfo";
			@SuppressWarnings("unchecked")
			List<FactRecord> results = (List<FactRecord>) 
				sqlMapClient.queryForList(queryName);

			// return
			Vector<FactRecord> resultArray = new Vector<FactRecord>();
			for (FactRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<FactRecord>();
		}

	} // end of method
	
	
	/**
	 * For Tibero database
	 */
	public static FactRecord queryFactInfo(String fact_code) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			FactRecord factRecord = new FactRecord();
			factRecord.setFact_code(fact_code);
			
			String queryName = "QueryFactInfo.SelectFactInfo";
			FactRecord result = (FactRecord) 
				sqlMapClient.queryForObject(queryName, factRecord);

			return result;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return null;
		}

	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
