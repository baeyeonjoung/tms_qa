/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.db.PatternRulesetSpcRecord;
import com.snu.tms.qa.model.db.StackKeyRecord;

/**
 * Query Agent
 * 
 */
public class QueryPatternRulesetSpc {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryPatternRulesetSpc.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static Vector<PatternRulesetSpcRecord> queryAllRuleset(String factCode, int stckCode) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// set stck key
			StackKeyRecord keyRecord = new StackKeyRecord(factCode, stckCode);
			
			// Get all tableName
			String queryName = "QueryPatternRulesetSpc.SelectAllRecord";
			@SuppressWarnings("unchecked")
			List<PatternRulesetSpcRecord> results = (List<PatternRulesetSpcRecord>) 
				sqlMapClient.queryForList(queryName, keyRecord);

			// return
			Vector<PatternRulesetSpcRecord> resultArray = new Vector<PatternRulesetSpcRecord>();
			for (PatternRulesetSpcRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<PatternRulesetSpcRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static Vector<PatternRulesetSpcRecord> queryAllRuleset(int rulsetId) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			String queryName = "QueryPatternRulesetSpc.SelectRecord";
			@SuppressWarnings("unchecked")
			List<PatternRulesetSpcRecord> results = (List<PatternRulesetSpcRecord>) 
				sqlMapClient.queryForList(queryName, rulsetId);

			// return
			Vector<PatternRulesetSpcRecord> resultArray = new Vector<PatternRulesetSpcRecord>();
			for (PatternRulesetSpcRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<PatternRulesetSpcRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertRuleset(PatternRulesetSpcRecord rulesetRecord) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do insert query
			String queryName = "QueryPatternRulesetSpc.InsertRecord";
			sqlMapClient.insert(queryName, rulesetRecord);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}

	} // end of method
	
	/**
	 * delete all pattern related with ruleset id
	 */
	public static void deleteRuleset(int rulesetId) {
		
		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do delete query
			String queryName = "QueryPatternRulesetSpc.DeleteRecord";
			sqlMapClient.update(queryName, rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
