/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.db.PatternRulesetPdRecord;
import com.snu.tms.qa.model.db.RulesetCorrectionRecord;
import com.snu.tms.qa.model.db.StackKeyRecord;

/**
 * Query Agent
 * 
 */
public class QueryRulesetCorrectionInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryRulesetCorrectionInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static Vector<RulesetCorrectionRecord> queryAllRulesetCorrectionInfo() {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			String queryName = "QueryRulesetCorrectionInfo.SelectAllRulesetCorrectionInfo";
			@SuppressWarnings("unchecked")
			List<RulesetCorrectionRecord> results = (List<RulesetCorrectionRecord>) 
				sqlMapClient.queryForList(queryName);

			// return
			Vector<RulesetCorrectionRecord> resultArray = new Vector<RulesetCorrectionRecord>();
			for (RulesetCorrectionRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<RulesetCorrectionRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static Vector<RulesetCorrectionRecord> queryAllRulesetCorrectionInfo(String factCode, int stckCode) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// set stck key
			StackKeyRecord keyRecord = new StackKeyRecord(factCode, stckCode);
			
			// Get all tableName
			String queryName = "QueryRulesetCorrectionInfo.SelectAllRecord";
			@SuppressWarnings("unchecked")
			List<RulesetCorrectionRecord> results = (List<RulesetCorrectionRecord>) 
				sqlMapClient.queryForList(queryName, keyRecord);

			// return
			Vector<RulesetCorrectionRecord> resultArray = new Vector<RulesetCorrectionRecord>();
			for (RulesetCorrectionRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<RulesetCorrectionRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static Vector<RulesetCorrectionRecord> queryRulesetCorrectionInfo(int rulesetId) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			String queryName = "QueryRulesetCorrectionInfo.SelectRulesetCorrectionInfo";
			@SuppressWarnings("unchecked")
			List<RulesetCorrectionRecord> results = (List<RulesetCorrectionRecord>) 
				sqlMapClient.queryForList(queryName, rulesetId);

			// return
			Vector<RulesetCorrectionRecord> resultArray = new Vector<RulesetCorrectionRecord>();
			for (RulesetCorrectionRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<RulesetCorrectionRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertRulesetCorrectionInfo(RulesetCorrectionRecord rulesetCorrectionRecord) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do insert query
			String queryName = "QueryRulesetCorrectionInfo.InsertRulesetCorrectionInfo";
			sqlMapClient.insert(queryName, rulesetCorrectionRecord);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}

	} // end of method

	/**
	 * delete ruleset recrod
	 */
	public static void deleteRulesetCorrectionInfo(int rulesetId) {
		
		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do delete query
			String queryName = "QueryRulesetCorrectionInfo.DeleteRulesetCorrectionInfo";
			sqlMapClient.update(queryName, rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
