/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.db.PatternRulesetPdRecord;
import com.snu.tms.qa.model.db.StackKeyRecord;

/**
 * Query Agent
 * 
 */
public class QueryPatternRulesetPd {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryPatternRulesetPd.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static Vector<PatternRulesetPdRecord> queryAllRuleset(String factCode, int stckCode) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// set stck key
			StackKeyRecord keyRecord = new StackKeyRecord(factCode, stckCode);
			
			// Get all tableName
			String queryName = "QueryPatternRulesetPd.SelectAllRecord";
			@SuppressWarnings("unchecked")
			List<PatternRulesetPdRecord> results = (List<PatternRulesetPdRecord>) 
				sqlMapClient.queryForList(queryName, keyRecord);

			// return
			Vector<PatternRulesetPdRecord> resultArray = new Vector<PatternRulesetPdRecord>();
			for (PatternRulesetPdRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<PatternRulesetPdRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static Vector<PatternRulesetPdRecord> queryAllRuleset(int rulsetId) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// Get all tableName
			String queryName = "QueryPatternRulesetPd.SelectRecord";
			@SuppressWarnings("unchecked")
			List<PatternRulesetPdRecord> results = (List<PatternRulesetPdRecord>) 
				sqlMapClient.queryForList(queryName, rulsetId);

			// return
			Vector<PatternRulesetPdRecord> resultArray = new Vector<PatternRulesetPdRecord>();
			for (PatternRulesetPdRecord result : results) {
				resultArray.add(result);
			}
			
			return resultArray;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new Vector<PatternRulesetPdRecord>();
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertRuleset(PatternRulesetPdRecord rulesetRecord) {

		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do insert query
			String queryName = "QueryPatternRulesetPd.InsertRecord";
			sqlMapClient.insert(queryName, rulesetRecord);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}

	} // end of method
	
	/**
	 * delete all pattern related with ruleset id
	 */
	public static void deleteRuleset(int rulesetId) {
		
		try {
			// Make SqlMapClient
			SqlMapClient sqlMapClient = SqlMapClientManager.getSqlMapClient();
			
			// do delete query
			String queryName = "QueryPatternRulesetPd.DeleteRecord";
			sqlMapClient.update(queryName, rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
