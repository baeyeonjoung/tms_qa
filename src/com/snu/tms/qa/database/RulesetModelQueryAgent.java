/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.database;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.PatternDataHistogram;
import com.snu.tms.qa.model.db.PatternItemRecord;
import com.snu.tms.qa.model.db.PatternPcaRecord;
import com.snu.tms.qa.model.db.PatternRecord;
import com.snu.tms.qa.model.db.PatternRulesetCorrRecord;
import com.snu.tms.qa.model.db.PatternRulesetPdRecord;
import com.snu.tms.qa.model.db.PatternRulesetSpcRecord;
import com.snu.tms.qa.model.db.PatternRulesetSpeRecord;
import com.snu.tms.qa.model.db.PatternRulesetTssRecord;
import com.snu.tms.qa.model.db.RulesetCorrectionRecord;
import com.snu.tms.qa.model.db.RulesetRecord;
import com.snu.tms.qa.model.db.RulesetWeightingRecord;
import com.snu.tms.qa.model.ruleset.PcaResultModel;
import com.snu.tms.qa.model.ruleset.RulesetCorr;
import com.snu.tms.qa.model.ruleset.RulesetCorrectionModel;
import com.snu.tms.qa.model.ruleset.RulesetModel;
import com.snu.tms.qa.model.ruleset.RulesetPatternModel;
import com.snu.tms.qa.model.ruleset.RulesetPcaSpe;
import com.snu.tms.qa.model.ruleset.RulesetPcaTss;
import com.snu.tms.qa.model.ruleset.RulesetPd;
import com.snu.tms.qa.model.ruleset.RulesetSpc;
import com.snu.tms.qa.util.DateUtil;

/**
 * Query Agent
 * 
 */
public class RulesetModelQueryAgent {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(RulesetModelQueryAgent.class);

	//==========================================================================
	// Static Method
	//==========================================================================
	
	/**
	 * query ruleset model
	 */
	public static RulesetModel[] queryRulesetModel(String factCode, int stckCode) {
		
		// create RulesetModel Hashtable
		Hashtable<Integer, RulesetModel> rulesetModelHash = new Hashtable<Integer, RulesetModel>();
		
		// query all ruleset_mgmt
		RulesetModel[] rulesetModels = querySelect_ruleset_mgmt(factCode, stckCode);
		for (RulesetModel model : rulesetModels) {
			rulesetModelHash.put(model.getRulesetId(), model);
		}
		
		// query all 
		querySelect_ruleset_pattern_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_pattern_item_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_pattern_pca_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_pattern_tss_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_pattern_spe_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_pattern_corr_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_pattern_spc_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_pattern_pd_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_correction_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_weighting_mgmt(factCode, stckCode, rulesetModelHash);

		// return
		Arrays.sort(rulesetModels);
		return rulesetModels;
		
	} // end of method
	
	/**
	 * query for ruleset_mgmt
	 */
	private static RulesetModel[] querySelect_ruleset_mgmt(String factCode, int stckCode) {
		
		// query from database
		Vector<RulesetRecord> records = QueryRulesetInfo.queryAllRulesetInfo(factCode, stckCode);
		
		// convert from database record type to system model type
		RulesetModel[] models = new RulesetModel[records.size()];
		for (int i=0 ; i<records.size() ; i++) {
			
			// get database record
			RulesetRecord record = records.get(i);
			
			// set system model
			models[i] = new RulesetModel();
			models[i].setRulesetId(record.getRuleset_id());
			models[i].setFactCode(factCode);
			models[i].setStckCode(stckCode);
			models[i].setRulesetState(record.getRuleset_state());
			models[i].setRulesetName(record.getRuleset_name());
			models[i].setRulesetDescr(record.getRuleset_descr());
			models[i].setRulesetStartTime(DateUtil.getDate(record.getStart_time()));
			models[i].setRulesetEndTime(DateUtil.getDate(record.getEnd_time()));
			models[i].setUserId(record.getUser_id());
			models[i].setCreateTime(record.getCreate_time());
			
		} // end of for-loop
		
		// return
		return models;
		
	} // end of method
	
	/**
	 * query pattern_mgmt
	 */
	private static void querySelect_ruleset_pattern_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		Vector<PatternRecord> records = QueryPatternInfo.queryAllPatternInfo(factCode, stckCode);
				
		// convert from database record type to system model type
		for (PatternRecord record : records) {
			
			// set parameters
			int      rulesetId        = record.getRuleset_id();
			int      patternId        = record.getPattern_id();
			String   patternName      = record.getPattern_name();
			boolean  isEnabled        = (record.getPattern_enable() == 1 ? true : false);
			boolean  isRulesetCreated = (record.getRuleset_enable() == 1 ? true : false);
			boolean  isOperateStop    = (record.getOperate_stop_state() == 1 ? true : false);
			int      itemCount        = record.getItem_count();
			String   items            = record.getItems();
			
			// get ruleset model
			RulesetModel rulesetModel = rulesetModelHash.get(rulesetId);
			if (rulesetModel == null) {
				continue;
			}
			
			// set model
			RulesetPatternModel model = new RulesetPatternModel(
					patternId,
					patternName,
					isEnabled,
					isRulesetCreated,
					isOperateStop, 
					itemCount,
					items,
					rulesetModel.getRulesetStartTime(),
					rulesetModel.getRulesetEndTime());
			
			// add model
			rulesetModel.addRulesetPatternModel(model);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query pattern_item_mgmt
	 */
	private static void querySelect_ruleset_pattern_item_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		Vector<PatternItemRecord> records = QueryPatternItemInfo.queryAllPatternItemInfo(factCode, stckCode);
				
		// convert from database record type to system model type
		for (PatternItemRecord record : records) {
			
			// set parameters
			int      rulesetId     = record.getRuleset_id();
			int      patternId     = record.getPattern_id();
			int      item_code     = record.getItem_code();
			double   min_range     = record.getMin_range();
			double   max_range     = record.getMax_range();
			int      bin_count     = record.getBin_count();
			int      bin_interval  = record.getBin_interval();
			String   density       = record.getDensity();
			
			// get ruleset model
			RulesetModel rulesetModel = rulesetModelHash.get(rulesetId);
			if (rulesetModel == null) {
				continue;
			}
			
			// get ruleset histogram
			RulesetPatternModel  rulesetPatternModel = rulesetModel.getRulesetPatternModel(patternId);
			if (rulesetPatternModel == null) {
				continue;
			}
			
			// add model
			PatternDataHistogram patternDataHistogram = 
					rulesetPatternModel.getPatternDataHistogram(item_code);
			if (patternDataHistogram == null) {
				continue;
			}
			patternDataHistogram.setBinNo(bin_count);
			patternDataHistogram.setInterval(bin_interval);
			patternDataHistogram.setMaxRange(max_range);
			patternDataHistogram.setMinRange(min_range);
			patternDataHistogram.setDensities(density);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query pattern_item_mgmt
	 */
	private static void querySelect_ruleset_pattern_pca_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		Vector<PatternPcaRecord> records = QueryPatternPcaInfo.queryAllPatternPcaInfo(factCode, stckCode);
				
		// convert from database record type to system model type
		for (PatternPcaRecord record : records) {
			
			// set parameters
			int      rulesetId     = record.getRuleset_id();
			int      patternId     = record.getPattern_id();
			String   items         = record.getItems();
			int      pc_count      = record.getPc_count();
			String   eigen_vector  = record.getEigen_vector();
			String   eigen_value   = record.getEigen_value();
			String   neigen_value  = record.getNeigen_value();
			
			// get ruleset model
			RulesetModel rulesetModel = rulesetModelHash.get(rulesetId);
			if (rulesetModel == null) {
				continue;
			}
			
			// get ruleset histogram
			RulesetPatternModel  rulesetPatternModel = rulesetModel.getRulesetPatternModel(patternId);
			if (rulesetPatternModel == null) {
				continue;
			}
			
			// add model
			PcaResultModel pcaResultModel = new PcaResultModel();
			pcaResultModel.setItems(items);
			pcaResultModel.setPcCount(pc_count);
			pcaResultModel.setEigenVector(eigen_vector);
			pcaResultModel.setEigenValues(eigen_value);
			pcaResultModel.setNeigenValues(neigen_value);
			
			rulesetPatternModel.setPcaResultModel(pcaResultModel);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query ruleset_tss_mgmt
	 */
	private static void querySelect_ruleset_pattern_tss_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		Vector<PatternRulesetTssRecord> records = QueryPatternRulesetTss.queryAllRuleset(factCode, stckCode);
				
		// convert from database record type to system model type
		for (PatternRulesetTssRecord record : records) {
			
			// set parameters
			int      ruleset_id      = record.getRuleset_id();
			int      pattern_id      = record.getPattern_id();
			int      ruleset_enable  = record.getRuleset_enable();
			double   threshold_lower = record.getThreshold_lower();
			double   threshold_upper = record.getThreshold_upper();
			String   items           = record.getItems();
			int      pc_count        = record.getPc_count();
			String   eigen_vector    = record.getEigen_vector();
			String   eigen_value     = record.getEigen_value();
			String   neigen_value    = record.getNeigen_value();
			String   cov_matrix      = record.getCov_matrix();
			String   imv_cov_matrix  = record.getImv_cov_matrix();
			String   mean_value      = record.getMean_value();
			String   sigma_value     = record.getSigma_value();
			double   confi_level     = record.getConfi_level();
			
			// get ruleset model
			RulesetModel rulesetModel = rulesetModelHash.get(ruleset_id);
			if (rulesetModel == null) {
				continue;
			}
			
			// get ruleset histogram
			RulesetPatternModel  rulesetPatternModel = rulesetModel.getRulesetPatternModel(pattern_id);
			if (rulesetPatternModel == null) {
				continue;
			}
			
			// add model
			RulesetPcaTss model = new RulesetPcaTss();
			model.setRulesetId(ruleset_id);
			model.setPatternId(pattern_id);
			model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
			model.setThresholds(new double[] {threshold_upper, threshold_lower});
			model.setItems(items);
			model.setPcCount(pc_count);
			model.setEigenVector(eigen_vector);
			model.setEigenValues(eigen_value);
			model.setNeigenValues(neigen_value);
			model.setCovMatrix(cov_matrix);
			model.setImvCovMatrix(imv_cov_matrix);
			model.setMean(mean_value);
			model.setSigma(sigma_value);
			model.setConfidenceLevel(confi_level);
			
			rulesetPatternModel.addDetectionRuleset(model);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query ruleset_tss_mgmt
	 */
	private static void querySelect_ruleset_pattern_spe_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		Vector<PatternRulesetSpeRecord> records = QueryPatternRulesetSpe.queryAllRuleset(factCode, stckCode);
				
		// convert from database record type to system model type
		for (PatternRulesetSpeRecord record : records) {
			
			// set parameters
			int      ruleset_id      = record.getRuleset_id();
			int      pattern_id      = record.getPattern_id();
			int      ruleset_enable  = record.getRuleset_enable();
			double   threshold_lower = record.getThreshold_lower();
			double   threshold_upper = record.getThreshold_upper();
			String   items           = record.getItems();
			int      pc_count        = record.getPc_count();
			String   eigen_vector    = record.getEigen_vector();
			String   eigen_value     = record.getEigen_value();
			String   neigen_value    = record.getNeigen_value();
			String   mean_value      = record.getMean_value();
			String   sigma_value     = record.getSigma_value();
			double   confi_level     = record.getConfi_level();
			
			// get ruleset model
			RulesetModel rulesetModel = rulesetModelHash.get(ruleset_id);
			if (rulesetModel == null) {
				continue;
			}
			
			// get ruleset histogram
			RulesetPatternModel  rulesetPatternModel = rulesetModel.getRulesetPatternModel(pattern_id);
			if (rulesetPatternModel == null) {
				continue;
			}
			
			// add model
			RulesetPcaSpe model = new RulesetPcaSpe();
			model.setRulesetId(ruleset_id);
			model.setPatternId(pattern_id);
			model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
			model.setThresholds(new double[] {threshold_upper, threshold_lower});
			model.setItems(items);
			model.setPcCount(pc_count);
			model.setEigenVector(eigen_vector);
			model.setEigenValues(eigen_value);
			model.setNeigenValues(neigen_value);
			model.setMean(mean_value);
			model.setSigma(sigma_value);
			model.setConfidenceLevel(confi_level);
			
			rulesetPatternModel.addDetectionRuleset(model);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query ruleset_tss_mgmt
	 */
	private static void querySelect_ruleset_pattern_corr_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		Vector<PatternRulesetCorrRecord> records = QueryPatternRulesetCorr.queryAllRuleset(factCode, stckCode);
				
		// convert from database record type to system model type
		for (PatternRulesetCorrRecord record : records) {
			
			// set parameters
			int      ruleset_id      = record.getRuleset_id();
			int      pattern_id      = record.getPattern_id();
			int      ruleset_enable  = record.getRuleset_enable();
			double   threshold_lower = record.getThreshold_lower();
			double   threshold_upper = record.getThreshold_upper();
			String   items           = record.getItems();
			double   rsquare         = record.getRsquare();
			String   cov_matrix      = record.getCov_matrix();
			String   imv_cov_matrix  = record.getImv_cov_matrix();
			String   mean_value      = record.getMean_value();
			String   sigma_value     = record.getSigma_value();
			double   confi_level     = record.getConfi_level();
			
			// get ruleset model
			RulesetModel rulesetModel = rulesetModelHash.get(ruleset_id);
			if (rulesetModel == null) {
				continue;
			}
			
			// get ruleset histogram
			RulesetPatternModel  rulesetPatternModel = rulesetModel.getRulesetPatternModel(pattern_id);
			if (rulesetPatternModel == null) {
				continue;
			}
			
			// add model
			RulesetCorr model = new RulesetCorr();
			model.setRulesetId(ruleset_id);
			model.setPatternId(pattern_id);
			model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
			model.setThresholds(new double[] {threshold_upper, threshold_lower});
			model.setItems(items);
			model.setrSquare(rsquare);
			model.setCovMatrix(cov_matrix);
			model.setImvCovMatrix(imv_cov_matrix);
			model.setMean(mean_value);
			model.setSigma(sigma_value);
			model.setConfidenceLevel(confi_level);
			
			rulesetPatternModel.addDetectionRuleset(model);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query ruleset_tss_mgmt
	 */
	private static void querySelect_ruleset_pattern_spc_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		Vector<PatternRulesetSpcRecord> records = QueryPatternRulesetSpc.queryAllRuleset(factCode, stckCode);
				
		// convert from database record type to system model type
		for (PatternRulesetSpcRecord record : records) {
			
			// set parameters
			int      ruleset_id      = record.getRuleset_id();
			int      pattern_id      = record.getPattern_id();
			int      ruleset_enable  = record.getRuleset_enable();
			double   threshold_lower = record.getThreshold_lower();
			double   threshold_upper = record.getThreshold_upper();
			int      item            = record.getItem();
			double   mean_value      = record.getMean_value();
			double   sigma_value     = record.getSigma_value();
			double   confi_level     = record.getConfi_level();
			
			// get ruleset model
			RulesetModel rulesetModel = rulesetModelHash.get(ruleset_id);
			if (rulesetModel == null) {
				continue;
			}
			
			// get ruleset histogram
			RulesetPatternModel  rulesetPatternModel = rulesetModel.getRulesetPatternModel(pattern_id);
			if (rulesetPatternModel == null) {
				continue;
			}
			
			// add model
			RulesetSpc model = new RulesetSpc();
			model.setRulesetId(ruleset_id);
			model.setPatternId(pattern_id);
			model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
			model.setThresholds(new double[] {threshold_upper, threshold_lower});
			model.setItem(item);
			model.setMean(mean_value);
			model.setSigma(sigma_value);
			model.setConfidenceLevel(confi_level);
			
			rulesetPatternModel.addDetectionRuleset(model);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query ruleset_pd_mgmt
	 */
	private static void querySelect_ruleset_pattern_pd_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		Vector<PatternRulesetPdRecord> records = QueryPatternRulesetPd.queryAllRuleset(factCode, stckCode);
				
		// convert from database record type to system model type
		for (PatternRulesetPdRecord record : records) {
			
			// set parameters
			int      ruleset_id      = record.getRuleset_id();
			int      pattern_id      = record.getPattern_id();
			int      ruleset_enable  = record.getRuleset_enable();
			double   threshold_lower = record.getThreshold_lower();
			double   threshold_upper = record.getThreshold_upper();
			int      item            = record.getItem();
			double   mean_value      = record.getMean_value();
			double   sigma_value     = record.getSigma_value();
			double   confi_level     = record.getConfi_level();
			
			// get ruleset model
			RulesetModel rulesetModel = rulesetModelHash.get(ruleset_id);
			if (rulesetModel == null) {
				continue;
			}
			
			// get ruleset histogram
			RulesetPatternModel  rulesetPatternModel = rulesetModel.getRulesetPatternModel(pattern_id);
			if (rulesetPatternModel == null) {
				continue;
			}
			
			// add model
			RulesetPd model = new RulesetPd();
			model.setRulesetId(ruleset_id);
			model.setPatternId(pattern_id);
			model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
			model.setThresholds(new double[] {threshold_upper, threshold_lower});
			model.setItem(item);
			model.setMean(mean_value);
			model.setSigma(sigma_value);
			model.setConfidenceLevel(confi_level);
			
			rulesetPatternModel.addDetectionRuleset(model);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query ruleset_correction_mgmt
	 */
	private static void querySelect_ruleset_correction_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		Vector<RulesetCorrectionRecord> records = 
				QueryRulesetCorrectionInfo.queryAllRulesetCorrectionInfo(factCode, stckCode);
				
		// convert from database record type to system model type
		for (RulesetCorrectionRecord record : records) {
			
			// set parameters
			int       ruleset_id  = record.getRuleset_id();
			int       item_code   = record.getItem_code();
			double    std_oxgy    = record.getStd_oxgy();
			double    std_mois    = record.getStd_mois();
			int       oxyg_corr   = record.getOxyg_corr();
			int       temp_corr   = record.getTemp_corr();
			int       mois_corr   = record.getMois_corr();
			
			// get ruleset model
			RulesetModel rulesetModel = rulesetModelHash.get(ruleset_id);
			if (rulesetModel == null) {
				continue;
			}
			
			// add model
			rulesetModel.addRulesetCorrectionModel(new RulesetCorrectionModel(
					item_code, std_oxgy, std_mois,
					oxyg_corr, temp_corr, mois_corr));
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query ruleset_weighting_mgmt
	 */
	private static void querySelect_ruleset_weighting_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		Vector<RulesetWeightingRecord> records = 
				QueryRulesetWeightingInfo.queryAllRulesetWeightingInfo(factCode, stckCode);
				
		// convert from database record type to system model type
		for (RulesetWeightingRecord record : records) {
			
			// set parameters
			int  ruleset_id         = record.getRuleset_id();
			int  multi_weight       = record.getMulti_weight();
			int  bi_weight          = record.getBi_weight();
			int  uni_weight         = record.getUni_weight();
			int  corr_weight        = record.getCorr_weight();
			int  warn_threshold     = record.getWarn_threshold();
			int  minor_threshold    = record.getMinor_threshold();
			int  major_threshold    = record.getMajor_threshold();
			int  critical_threshold = record.getCritical_threshold();

			// get ruleset model
			RulesetModel rulesetModel = rulesetModelHash.get(ruleset_id);
			if (rulesetModel == null) {
				continue;
			}
			
			// add model
			rulesetModel.setRulesetWeightingFactors(
					multi_weight, bi_weight, uni_weight, corr_weight, 
					warn_threshold, minor_threshold, major_threshold, critical_threshold);
			
		} // end of for-loop
		
	} // end of method


	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
