/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.ruleset;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.*;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.statistics.HistogramDataset;

import com.snu.tms.qa.soap.RulesetModelQueryAgent;
import com.snu.tms.qa.soap.RulesetModelUpdateAgent;
import com.snu.tms.qa.model.InventoryDataManager;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.PatternDataHistogram;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.ruleset.DetectionRuleset;
import com.snu.tms.qa.model.ruleset.RulesetCorrectionModel;
import com.snu.tms.qa.model.ruleset.RulesetModel;
import com.snu.tms.qa.model.ruleset.RulesetPatternModel;
import com.snu.tms.qa.model.ruleset.RulesetWeightingModel;
import com.snu.tms.qa.util.DateUtil;


/**
 * Outlier detection panel
 * 
 */
public class UI_RulesetManagerBasePanel extends JPanel
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;

	private static Log logger = LogFactory.getLog(UI_RulesetManagerBasePanel.class);

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	private  JTable               rulesetTable        = null;
	private  JTabbedPane          rulesetBasePanel    = null;
	
	private  Vector<RulesetModel> rulesetModels       = new Vector<RulesetModel>();
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public UI_RulesetManagerBasePanel() {
		
		// add data loading control panel
		JPanel      dataControlBase  = createDataControlPanel();
		JSplitPane  rulesetBase      = createRulesetBasePanel();
		
		// set Layout
		this.setLayout(new BorderLayout());
		this.add(dataControlBase, BorderLayout.NORTH);
		this.add(rulesetBase, BorderLayout.CENTER);
		
	} // end of constructor
	
	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create data loading control panel
	 */
	private JPanel createDataControlPanel() {

		// Set target date
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(600, 55));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("  ruleset control  "),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// Set Apply Button
		JButton refreshButton = new JButton("refresh all");
		refreshButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateRulesetTable();
			}
		});
		refreshButton.setBounds(20, 20, 150, 25);
		basePanel.add(refreshButton);

		// set default ruleset
		JButton setDefaultRulesetButton = new JButton("set default ruleset");
		setDefaultRulesetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeDefaultRuleset();
			}
		});
		setDefaultRulesetButton.setBounds(190, 20, 190, 25);
		basePanel.add(setDefaultRulesetButton);
		
		/**
		// apply ruleset button
		JButton applyButton = new JButton("apply");
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				applyChange();
			}
		});
		applyButton.setBounds(400, 20, 80, 25);
		basePanel.add(applyButton);
		*/

		// delete ruleset button
		JButton deleteButton = new JButton("delete");
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteRuleset();
			}
		});
		deleteButton.setBounds(400, 20, 80, 25);
		basePanel.add(deleteButton);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * create ruleset base panel
	 */
	private JSplitPane createRulesetBasePanel() {
		
		// create base panel
		JSplitPane basePanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		basePanel.setDividerLocation(230);
		basePanel.setDividerSize(5);
		
		// create ruleset table
		RulesetTableModel rulesetTableModel = new RulesetTableModel();
		this.rulesetTable = new JTable(rulesetTableModel);
		
		// set row height and column width
		this.rulesetTable.setRowHeight(25);
		this.rulesetTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.rulesetTable.getColumnModel().getColumn(0).setPreferredWidth(60);
		this.rulesetTable.getColumnModel().getColumn(1).setPreferredWidth(150);
		this.rulesetTable.getColumnModel().getColumn(2).setPreferredWidth(50);
		this.rulesetTable.getColumnModel().getColumn(3).setPreferredWidth(100);
		this.rulesetTable.getColumnModel().getColumn(4).setPreferredWidth(200);
		this.rulesetTable.getColumnModel().getColumn(5).setPreferredWidth(60);
		this.rulesetTable.getColumnModel().getColumn(6).setPreferredWidth(140);
		this.rulesetTable.getColumnModel().getColumn(7).setPreferredWidth(100);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
		leftRenderer.setHorizontalAlignment( JLabel.LEFT );
		
		this.rulesetTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.rulesetTable.getColumnModel().getColumn(1).setCellRenderer( leftRenderer );
		this.rulesetTable.getColumnModel().getColumn(3).setCellRenderer( leftRenderer );
		this.rulesetTable.getColumnModel().getColumn(4).setCellRenderer( centerRenderer );
		this.rulesetTable.getColumnModel().getColumn(5).setCellRenderer( centerRenderer );
		this.rulesetTable.getColumnModel().getColumn(6).setCellRenderer( centerRenderer );
		this.rulesetTable.getColumnModel().getColumn(7).setCellRenderer( centerRenderer );
		
		// add List selection Model
		this.rulesetTable.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent event) {
						
						int  selectedRow = rulesetTable.getSelectedRow();
						RulesetModel model = rulesetModels.get(selectedRow);
						if (model != null) {
							updateRulesetBasePanel(model.getRulesetId());
						}
					}
				});		
		
		// create ruleset info panel
		this.rulesetBasePanel = new JTabbedPane();
		
		// set layout
		basePanel.add(new JScrollPane(this.rulesetTable));
		basePanel.add(this.rulesetBasePanel);

		return basePanel;
		
	} // end of method
	
	/**
	 * update ruleset table
	 */
	private void updateRulesetTable() {

		// Get Selected StackInfo
		InventoryDataManager inventoryDataManager = InventoryDataManager.getInstance();
		StackInfo stackInfo = inventoryDataManager.getSelectedStackInfo();
		
		// Check StackInfo and TimeSet
		if (stackInfo == null) {
			JOptionPane.showMessageDialog(
					SwingUtilities.getWindowAncestor(this), 
					"Ruleset target stack selecting is required in left window.");
		}
		
		// update ruleset table
		setRulesetModel();
		this.rulesetTable.updateUI();

	} // end of method
	
	/**
	 * update ruleset base panel
	 */
	private void updateRulesetBasePanel(int rulesetId) {
		
		// clear panel
		this.rulesetBasePanel.removeAll();
		
		// get parameters
		RulesetModel rulesetModel = getRulesetModel(rulesetId);
		if (rulesetModel == null) {
			return;
		}
		
		// get ruleset pattern model
		RulesetPatternModel[] patternModels = rulesetModel.getAllRulesetPatternModel();
		Arrays.sort(patternModels);
		for (RulesetPatternModel patternModel : patternModels) {
			
			this.rulesetBasePanel.addTab(
					String.format("[%d] %s", patternModel.getPatternId(), patternModel.getPatternName()), 
					createPatternBasePanel(rulesetModel, patternModel));
			
		} // end of for-loop
		
		// update
		this.rulesetBasePanel.updateUI();
		
	} // end of method
	
	/**
	 * create pattern base panel
	 */
	private JSplitPane createPatternBasePanel(
			RulesetModel rulesetModel, 
			RulesetPatternModel patternModel) {
		
		// create pattern info table
		PatternInfoTableModel patternInfoTableModel = new PatternInfoTableModel(patternModel);
		JTable patternInfoTable = new JTable(patternInfoTableModel);
		
		// set row height and column width
		patternInfoTable.setRowHeight(25);
		patternInfoTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		patternInfoTable.getColumnModel().getColumn(0).setPreferredWidth(110);
		patternInfoTable.getColumnModel().getColumn(1).setPreferredWidth(140);

		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		patternInfoTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		patternInfoTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );

		// set histogram and ruleset table panel
		JTabbedPane rightBasePanel = new JTabbedPane();
		rightBasePanel.add("pattern_histogram", createHistogramPanel(patternModel));
		rightBasePanel.add("ruleset", createRulesetTablePanel(patternModel));
		rightBasePanel.add("item_correction", createItemCorrectionPanel(rulesetModel));
		rightBasePanel.add("ruleset_weighting", createRulesetWeightingPanel(rulesetModel));
		
		// set Layout
		JSplitPane basePanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		basePanel.setDividerLocation(250);
		basePanel.setDividerSize(5);
		
		basePanel.add(new JScrollPane(patternInfoTable));
		basePanel.add(rightBasePanel);
		
		// return
		return basePanel;
		
	} // end of method
	
	/**
	 * create histogram base panel
	 */
	private JScrollPane createHistogramPanel(RulesetPatternModel patternModel) {
		
		// get selected pattern and histogram data
		PatternDataHistogram[] patternHistograms = 
				patternModel.getPatternDataHistograms();
		if (patternHistograms == null) {
			return new JScrollPane();
		}
		int size = (int) Math.ceil( ((double)patternHistograms.length) / 2.0D);
		
		// create basePanel
		JPanel basePanel = new JPanel(new GridLayout(0, 2));
		basePanel.setPreferredSize(new Dimension(450, 150 * size));
		
		// create histogram and add basePanel
		JPanel chartPanel;
		for (int i=0 ; i<patternHistograms.length ; i++) {
			chartPanel = createPatternHistogramGraph(patternHistograms[i]);
			chartPanel.setPreferredSize(new Dimension(200, 150));
			basePanel.add(chartPanel);
		}
		return new JScrollPane(basePanel);		
		
	} // end of method
	
	/**
	 * create PatternHistogram graph
	 */
	private JPanel createPatternHistogramGraph(PatternDataHistogram histogramData) {
		
		// get parameters
		double[][] densities = histogramData.getDensities();
		int        item      = histogramData.getItem();
		String     itemStr   = ItemCodeDefine.getItemName(item);
		
		// prepare HistogramDataset
		HistogramDataset dataset = new HistogramDataset();
		int dataCount = 2000;
		double densitySum = 0.0D;
		for (int i=0 ; i<densities.length ; i++) {
			densitySum += densities[i][1];
		}

		double densityMultiplier = 0.0D;
		if (densitySum != 0.0D) {
			densityMultiplier = ((double)dataCount) / densitySum;
		}
		
		double[] values = new double[dataCount];
		int count = 0;
		int N     = 0;
		for (int i=0 ; i<densities.length ; i++) {
			N = (int) (densities[i][1] * densityMultiplier);
			for (int j=0 ; j<N ; j++) {
				if (count >= dataCount) {
					break;
				}
				values[count++] = densities[i][0];
			}
		}

		values = Arrays.copyOf(values, count);
		if (histogramData.getBinNo() <= 1) {
			dataset.addSeries(itemStr, values, 1);
		} else {
			dataset.addSeries(itemStr, values, histogramData.getBinNo()-1);
		}
		
		// create histogram chart
		JFreeChart chart = ChartFactory.createHistogram(
				"",
				null,
				null,
				dataset,
				PlotOrientation.VERTICAL,
				true,
				true,
				false);
		
		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setDomainPannable(true);
		plot.setRangePannable(true);
		plot.setForegroundAlpha(0.85f);
		//NumberAxis xAxis = (NumberAxis) plot.getDomainAxis();
		//xAxis.setRange(histogramData.getMinRange(), histogramData.getMaxRange());
		NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
		yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
		renderer.setDrawBarOutline(false);
		renderer.setBarPainter(new StandardXYBarPainter());
		renderer.setShadowVisible(false);
		
		// return
		return new ChartPanel(chart);

	} // end of method
	
	/**
	 * create ruleset table panel
	 */
	private JScrollPane createRulesetTablePanel(RulesetPatternModel patternModel) {

		// create ruleset table
		RulesetRecordTableModel rulesetTableModel = new RulesetRecordTableModel(patternModel);
		JTable rulesetRecordTable = new JTable(rulesetTableModel);
		
		// set row height and column width
		rulesetRecordTable.setRowHeight(25);
		rulesetRecordTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		rulesetRecordTable.getColumnModel().getColumn(0).setPreferredWidth(50);
		rulesetRecordTable.getColumnModel().getColumn(1).setPreferredWidth(50);
		rulesetRecordTable.getColumnModel().getColumn(2).setPreferredWidth(120);
		rulesetRecordTable.getColumnModel().getColumn(3).setPreferredWidth(120);
		rulesetRecordTable.getColumnModel().getColumn(4).setPreferredWidth(60);
		rulesetRecordTable.getColumnModel().getColumn(5).setPreferredWidth(70);
		rulesetRecordTable.getColumnModel().getColumn(6).setPreferredWidth(120);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
		leftRenderer.setHorizontalAlignment( JLabel.LEFT );
		
		rulesetRecordTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		rulesetRecordTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		rulesetRecordTable.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		rulesetRecordTable.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
		rulesetRecordTable.getColumnModel().getColumn(5).setCellRenderer( centerRenderer );
		rulesetRecordTable.getColumnModel().getColumn(6).setCellRenderer( centerRenderer );

		// set layout
		return new JScrollPane(rulesetRecordTable);
		
	} // end of method
	
	/**
	 * create ruleset table panel
	 */
	private JScrollPane createItemCorrectionPanel(RulesetModel rulesetModel) {

		// create ruleset table
		ItemCorrectionTableModel itemCorrectionTableModel = new ItemCorrectionTableModel(rulesetModel);
		JTable itemCorrectionTable = new JTable(itemCorrectionTableModel);
		
		itemCorrectionTable.setRowHeight(25);
		itemCorrectionTable.getColumnModel().getColumn(0).setPreferredWidth(80);
		itemCorrectionTable.getColumnModel().getColumn(1).setPreferredWidth(100);
		itemCorrectionTable.getColumnModel().getColumn(2).setPreferredWidth(100);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		itemCorrectionTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		itemCorrectionTable.getColumnModel().getColumn(1).setCellRenderer( rightRenderer );
		itemCorrectionTable.getColumnModel().getColumn(2).setCellRenderer( rightRenderer );
		
		// set layout
		return new JScrollPane(itemCorrectionTable);
		
	} // end of method
	
	/**
	 * create ruleset table panel
	 */
	private JScrollPane createRulesetWeightingPanel(RulesetModel rulesetModel) {

		// create ruleset table
		RulesetWeightingTableModel rulesetWeightingTableModel = new RulesetWeightingTableModel(rulesetModel);
		JTable rulesetWeightingTable = new JTable(rulesetWeightingTableModel);
		
		rulesetWeightingTable.setRowHeight(25);
		rulesetWeightingTable.getColumnModel().getColumn(0).setPreferredWidth(200);
		rulesetWeightingTable.getColumnModel().getColumn(1).setPreferredWidth(100);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		rulesetWeightingTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		rulesetWeightingTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		
		// set layout
		return new JScrollPane(rulesetWeightingTable);
		
	} // end of method
	
	/**
	 * apply change
	 */
	//private void applyChange() {} // end of method

	/**
	 * get ruleset model
	 */
	private RulesetModel getRulesetModel(int rulesetId) {
		
		// check null
		for (RulesetModel model : this.rulesetModels) {
			if (model.getRulesetId() == rulesetId) {
				return model;
			}
		} // end of for-loop
		return null;
		
	} // end of method
	
	/**
	 * delete ruleset
	 */
	private void deleteRuleset() {

		// get selected row
		int[] selectedRows = this.rulesetTable.getSelectedRows();
		if (selectedRows == null || selectedRows.length == 0) {
			JOptionPane.showMessageDialog(
					SwingUtilities.getWindowAncestor(this), 
					"There is no selected ruleset for delete.");
			return;
		}
		
		// user confirm
		int isConfirmed = JOptionPane.showConfirmDialog(
				SwingUtilities.getWindowAncestor(this), 
				"[ " + selectedRows.length + " ] ruleset will be removed. "
						+ "This delete process is irreversible. Are you sure remove ruleset?");
		if (isConfirmed != JOptionPane.OK_OPTION) {
			return;
		} 
		
		// delete query
		if (this.rulesetModels == null) {
			return;
		}
		for (int i=0 ; i<selectedRows.length ; i++) {
			
			if (i >= this.rulesetModels.size()) {
				break;
			}
			// delete query
			RulesetModelUpdateAgent.doDeleteRulesetModel(this.rulesetModels.get(selectedRows[i]));
		}
		
		// Ruleset model update
		Arrays.sort(selectedRows);
		for (int i=selectedRows.length-1 ; i>= 0 ; i--) {
			this.rulesetModels.remove(selectedRows[i]);
		}
		
		// UI update
		this.rulesetTable.updateUI();

	} // end of method

	/**
	 * set ruleset model
	 */
	private void setRulesetModel() {
		
		// Get Selected StackInfo
		InventoryDataManager inventoryDataManager = InventoryDataManager.getInstance();
		StackInfo stackInfo = inventoryDataManager.getSelectedStackInfo();

		if (stackInfo == null) {
			return;
		}
		
		// clear ruleset models
		this.rulesetModels.clear();
		
		// query ruleset model
		RulesetModel[] rulesetModels = RulesetModelQueryAgent.queryRulesetModel(
				stackInfo.getFactoryInfo().getCode(), 
				stackInfo.getStackNumber());
		if (rulesetModels != null) {
			for (RulesetModel model : rulesetModels) {
				this.rulesetModels.add(model);
			}
		}
		
	} // end of method
	
	/**
	 * change default ruleset
	 */
	private void changeDefaultRuleset() {
		
		// get selected ruleset
		int selectedRow = this.rulesetTable.getSelectedRow();
		RulesetModel selectedModel = this.rulesetModels.get(selectedRow);
		if (selectedModel == null) {
			return;
		}
		
		if (selectedModel.getRulesetState() == 1) {
			return;
		}
		
		// set default ruleset
		selectedModel.setRulesetState(1);
		
		// db update
		RulesetModel model;
		for (int i=0 ; i<this.rulesetModels.size() ; i++) {
			
			model = this.rulesetModels.get(i);
			if (i == selectedRow) {
				model.setRulesetState(1);
				RulesetModelUpdateAgent.updateRulesetState(model);
			} else if (model.getRulesetState() == 1) {
				model.setRulesetState(0);
				RulesetModelUpdateAgent.updateRulesetState(model);
			}
			
		} // end of for-loop
		this.rulesetTable.updateUI();
		
	} // end of method
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class RulesetTableModel extends AbstractTableModel {
		
		//----------------------------------------------------------------------
		// Fields
		//----------------------------------------------------------------------
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames = {
			"ID", "name", "apply", "desc", "ruleset base-time", "patterns", "create-time", "user-id"
		};
		
		//----------------------------------------------------------------------
		// Constructor
		//----------------------------------------------------------------------
		public RulesetTableModel() {}
		
		//----------------------------------------------------------------------
		// Implemented method
		//----------------------------------------------------------------------
		public String getColumnName(int col) {
			return columnNames[col];
		} // end of method
		
		public int getRowCount() {
			if (rulesetModels == null) {
				return 0;
			}
			return rulesetModels.size();
			}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			RulesetModel rulesetModel = rulesetModels.get(row);
			if (col == 0) {
				return rulesetModel.getRulesetId();
			} else if (col == 1) {
				return rulesetModel.getRulesetName();
			} else if (col == 2) {
				return (rulesetModel.getRulesetState() == 1 ? Boolean.TRUE : Boolean.FALSE);
			} else if (col == 3) {
				return (rulesetModel.getRulesetDescr());
			} else if  (col == 4) {
				return String.format("%s ~ %s", 
						DateUtil.getSimpleMinStr(rulesetModel.getRulesetStartTime()),
						DateUtil.getSimpleMinStr(rulesetModel.getRulesetEndTime()));
			} else if  (col == 5) {
				return rulesetModel.getRulesetPatternModelCount();
			} else if  (col == 6) {
				return rulesetModel.getCreateTime();
			} else if (col == 7) {
				return rulesetModel.getUserId();
			} else {
				return "";
			}
		}
		
		public boolean isCellEditable(int row, int col) {
			if (col == 2) {
				return false;
			}
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return Integer.class;
			} else if (col == 1) {
				return String.class;
			} else if (col == 2) {
				return Boolean.class;
			} else if (col == 3) {
				return String.class;
			} else if (col == 4) {
				return String.class;
			} else if (col == 5) {
				return String.class;
			} else if (col == 6) {
				return String.class;
			} else if (col == 7) {
				return String.class;
			} else {
				return String.class;
			}
			
		}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class PatternInfoTableModel extends AbstractTableModel {
		
		//----------------------------------------------------------------------
		// Fields
		//----------------------------------------------------------------------
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames = {
			"property", "value"
		};
		
		private final  String[] rowNames = {
				"pattern id", "pattern name", "start time", "end time", 
				"enable detection", "ruleset created", "operate stop", "item count", "itmes"
			};
		
		private  RulesetPatternModel  rulesetPatternModel = null;
		
		//----------------------------------------------------------------------
		// Constructor
		//----------------------------------------------------------------------
		public PatternInfoTableModel(RulesetPatternModel rulesetPatternModel) {
			this.rulesetPatternModel = rulesetPatternModel;
		} // end of constructor
		
		//----------------------------------------------------------------------
		// Implemented method
		//----------------------------------------------------------------------
		public String getColumnName(int col) {
			return columnNames[col];
		} // end of method
		
		public int getRowCount() {
			return 9;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return rowNames[row];
			} else {
				if (row == 0) {
					return rulesetPatternModel.getPatternId();
				} else if (row == 1) {
					return rulesetPatternModel.getPatternName();
				} else if (row == 2) {
					return DateUtil.getDateStr(rulesetPatternModel.getPatternBaseStartTime());
				} else if (row == 3) {
					return DateUtil.getDateStr(rulesetPatternModel.getPatternBaseEndTime());
				} else if (row == 4) {
					return (rulesetPatternModel.isEnabled() ? "TRUE" : "FALSE");
				} else if (row == 5) {
					return (rulesetPatternModel.isRulesetCreated() ? "TRUE" : "FALSE");
				} else if (row == 6) {
					return (rulesetPatternModel.isOperateStop() ? "TRUE" : "FALSE");
				} else if (row == 7) {
					return rulesetPatternModel.getItemCount();
				} else if (row == 8) {
					String itemInfo = "";
					int[] items = rulesetPatternModel.getItems();
					for (int i=0 ; i<items.length ; i++) {
						if (i != 0) {
							itemInfo += ", ";
						}
						itemInfo += ItemCodeDefine.getItemName(items[i]);
					}
					return itemInfo;
				} else {
					return "";
				}
			}
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class

	//==========================================================================
	// Inner Class
	//==========================================================================
	class RulesetRecordTableModel extends AbstractTableModel {
		
		//----------------------------------------------------------------------
		// Fields
		//----------------------------------------------------------------------
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames = {
			"ruleset", "pattern", "type", "items", "enabled", "confidence level", "threshold"
		};

		private  RulesetPatternModel  rulesetPatternModel = null;
		private  DetectionRuleset[]   rulesets = null;

		//----------------------------------------------------------------------
		// Constructor
		//----------------------------------------------------------------------
		public RulesetRecordTableModel(RulesetPatternModel rulesetPatternModel) {

			this.rulesetPatternModel = rulesetPatternModel;
			this.rulesets = this.rulesetPatternModel.getDetectionRulesets();

		} // end of constructor
		
		//----------------------------------------------------------------------
		// Implemented method
		//----------------------------------------------------------------------
		public String getColumnName(int col) {
			return columnNames[col];
		} // end of method
		
		public int getRowCount() {
			if (this.rulesets == null) {
				return 0;
			}
			return rulesets.length;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			DetectionRuleset ruleset = this.rulesets[row];
			if (col == 0) {
				return ruleset.getRulesetId();
			} else if (col == 1) {
				return rulesetPatternModel.getPatternId();
			} else if (col == 2) {
				return DetectionRuleset.RULETSET_NAME[ruleset.getRulesetType()];
			} else if (col == 3) {
				String itemInfo = "";
				int[] items = ruleset.getItems();
				for (int i=0 ; i<items.length ; i++) {
					if (i != 0) {
						itemInfo += ", ";
					}
					itemInfo += ItemCodeDefine.getItemName(items[i]);
				}
				return itemInfo;
			} else if  (col == 4) {
				return (ruleset.isRulesetEnabled() ? Boolean.TRUE : Boolean.FALSE);
			} else if  (col == 5) {
				return String.format("%.3f", ruleset.getConfidenceLevel() * 100.0D) + " %";
			} else if  (col == 6) {
				double[] threholds = ruleset.getThresholds();
				return String.format("%.3f ~ %.3f", 
						threholds[1],
						threholds[0]);
			} else {
				return "";
			}
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return Integer.class;
			} else if (col == 1) {
				return Integer.class;
			} else if (col == 2) {
				return String.class;
			} else if (col == 3) {
				return String.class;
			} else if (col == 4) {
				return Boolean.class;
			} else if (col == 5) {
				return String.class;
			} else if (col == 6) {
				return String.class;
			} else {
				return String.class;
			}
			
		}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class ItemCorrectionTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		public  RulesetCorrectionModel[]  correctionModels = null;
		public  int[]                     items     = null;

		public ItemCorrectionTableModel(RulesetModel rulesetModel) {
			
			// get CorrectionModel
			this.correctionModels = rulesetModel.getAllRulesetCorrectionModel();
			Arrays.sort(this.correctionModels);
			
			// get items
			this.items = new int[correctionModels.length];
			int i = 0;
			for (RulesetCorrectionModel correctionModel : correctionModels) {
				this.items[i++] = correctionModel.getItemCode();
			}
		
		} // end of constructor
		
		public String getColumnName(int col) {
			
			if (col == 0) {
				return "Item";
			} else if (col == 1) {
				return "Std.Oxgn";
			} else if (col == 2) {
				return "Std.Moi";
			} else if (col == 3) {
				return "Oxgn Corr.";
			} else if (col == 4) {
				return "Temp Corr.";
			} else if (col == 5) {
				return "Mois Corr.";
			} else {
				return "";
			}
		}
		
		public int getRowCount() { 
			return this.items.length;
		}
		
		public int getColumnCount() { 
			return 6;
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return ItemCodeDefine.getItemName(this.items[row]);
			} else if (col == 1) {
				if (this.correctionModels[row].getStdOxygen() == -1.0D) {
					return "";
				} else {
					return String.format("%3.0f", this.correctionModels[row].getStdOxygen());
				}
			} else if (col == 2) {
				if (this.correctionModels[row].getStdMoisture() == -1.0D) {
					return "";
				} else {
					return String.format("%3.2f", this.correctionModels[row].getStdMoisture());
				}
			} else if (col == 3) {
				return new Boolean(this.correctionModels[row].isOxygenApplied());
			} else if (col == 4) {
				return new Boolean(this.correctionModels[row].isTemperatureApplied());
			} else if (col == 5) {
				return new Boolean(this.correctionModels[row].isMoistureApplied());
			} else {
				return "";
			}
		
		} // end of method
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return String.class;
			} else if (col == 1) {
				return String.class;
			} else if (col == 2) {
				return String.class;
			} else {
				return Boolean.class;
			}
			
		}

		public boolean isCellEditable(int row, int col) {
			return true;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class RulesetWeightingTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		public  RulesetWeightingModel  weightingModel = null;

		public RulesetWeightingTableModel(RulesetModel rulesetModel) {
			this.weightingModel = rulesetModel.getRulesetWeightingModel();
		} // end of constructor
		
		public String getColumnName(int col) {
			
			if (col == 0) {
				return "Type & Level";
			} else if (col == 1) {
				return "Value";
			} else {
				return "";
			}
		}
		
		public int getRowCount() { 
			return 8;
		}
		
		public int getColumnCount() { 
			return 2;
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				if (row == 0) {
					return "Multivariate Ruleset Weighting";
				} else if (row == 1) {
					return "Bivariate Ruleset Weighting";
				} else if (row == 2) {
					return "Univariate Ruleset Weighting";
				} else if (row == 3) {
					return "Correction Ruleset Weighting";
				} else if (row == 4) {
					return "Warning Count Threshold";
				} else if (row == 5) {
					return "Minor Count Threshold";
				} else if (row == 6) {
					return "Major Count Threshold";
				} else if (row == 7) {
					return "Critical Count Threshold";
				} else {
					return "";
				}
			} else if (col == 1) {
				if (row == 0) {
					return weightingModel.getMultivariateRulesetWeighting();
				} else if (row == 1) {
					return weightingModel.getBivariateRulesetWeighting();
				} else if (row == 2) {
					return weightingModel.getUnivariateRulesetWeighting();
				} else if (row == 3) {
					return weightingModel.getCorrectionRulesetWeighting();
				} else if (row == 4) {
					return weightingModel.getWarningLevel();
				} else if (row == 5) {
					return weightingModel.getMinorLevel();
				} else if (row == 6) {
					return weightingModel.getMajorLevel();
				} else if (row == 7) {
					return weightingModel.getCriticalLevel();
				} else {
					return "";
				}
			} else {
				return "";
			}
		
		} // end of method

		public boolean isCellEditable(int row, int col) {
			return true;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
} // end of class
