/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.snu.tms.qa.gui.evaluation.UI_RulesetEvalution;
import com.snu.tms.qa.gui.outlier.UI_OutlierDetection;
import com.snu.tms.qa.gui.ruleset.UI_RulesetManagerBasePanel;


/**
 * Left Panel for stack selection
 * 
 */
public class AnalysisBasePanel extends JTabbedPane
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	private JPanel  outlierContainer    = null;
	private JPanel  rulesetContainer    = null;
	private JPanel  evaluationContainer = null;
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * 
	 */
	public AnalysisBasePanel() {
		
		// add sub panel
		this.outlierContainer    = new JPanel(new BorderLayout());
		this.rulesetContainer    = new JPanel(new BorderLayout());
		this.evaluationContainer = new JPanel(new BorderLayout());
		
		this.addTab("Outlier Detection",     this.outlierContainer);
		this.addTab("Ruleset Management",    this.rulesetContainer);
		this.addTab("Ruleset Evaluation",    this.evaluationContainer);
		
		// set ourlier detection panel
		outlierContainer.add(new UI_OutlierDetection(), BorderLayout.CENTER);
		rulesetContainer.add(new UI_RulesetManagerBasePanel(), BorderLayout.CENTER);
		evaluationContainer.add(new UI_RulesetEvalution(), BorderLayout.CENTER);
		
		// set default tab
		this.setSelectedIndex(0);
		
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Init. UI
	 * This method doesn't use anywhere. (2016-07-21)
	 */
	public void initUI() {
		
		/**
		// cleanup
		this.outlierContainer.removeAll();
		this.rulesetContainer.removeAll();
		this.evaluationContainer.removeAll();
		*/
		
		// update UI
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// set outlier detection panel
					outlierContainer.add(new UI_OutlierDetection(), BorderLayout.CENTER);
					
					// set outlier ruleset panel
					
					// set evalution panel
					
					// updateUI
					setSelectedIndex(0);
					updateUI();
					
				} catch (Exception e) {	
					e.printStackTrace(); 
				}
			}
		});
		
	} // end of method



} // end of class
