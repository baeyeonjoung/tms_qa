/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JSplitPane;

import com.snu.tms.qa.gui.stack.UI_StackSelection;

import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;


/**
 * Setting the program startup process.
 * 
 */
public class Startup_TmsQA_GUI {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Startup_TmsQA_GUI instance = new Startup_TmsQA_GUI();
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	public JFrame  mainFrame;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Main window
	 */
	private Startup_TmsQA_GUI() {
		
		// initialize windows
		initialize();
		
	} // end of method
	
	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * Get Windows instance
	 */
	public static Startup_TmsQA_GUI getInstance() {
		return instance;
	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		// Set Frame
		this.mainFrame = new JFrame();
		this.mainFrame.setBounds(100, 100, 1200, 900);
		this.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.mainFrame.setTitle("Air-DAP(Data quality Analysis Program) (v1.0)");
		
		// set icon
		try {
			this.mainFrame.setIconImage(ImageIO.read(new File("image/smart.png")));
		} catch (IOException exc) {}

		// Create Left Stack Selection Panel and Right Analysis Panel
		AnalysisBasePanel analysisBasePanel = new AnalysisBasePanel();
		UI_StackSelection stackSelectionPanel = new UI_StackSelection();
		
		// Set Layout
		JSplitPane basePanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		basePanel.setDividerLocation(400);
		basePanel.setDividerSize(5);
		
		basePanel.add(stackSelectionPanel);
		basePanel.add(analysisBasePanel);
		
		this.mainFrame.getContentPane().setLayout(new BorderLayout());
		this.mainFrame.getContentPane().add(basePanel, BorderLayout.CENTER);
		
	} // end of method

} // end of class
