/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.evaluation.dialog;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.util.DateUtil;

import java.awt.Dimension;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * Left Panel for stack selection
 * 
 */
public class DialogRawDataViewer extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	private static final DecimalFormat indexFormatter = new DecimalFormat("#,###,###,##0");
	private static final DecimalFormat valueFormatter = new DecimalFormat("#,###,###,##0.00");
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  TmsDataset dataSet;
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogRawDataViewer(
			JFrame parent, 
			String stackInfoStr,
			TmsDataset dataSet) {
		
		// set dialog
		super(parent, "Raw data viewer : " + stackInfoStr, ModalityType.MODELESS);

		// set icon
		try {
			setIconImage(ImageIO.read(new File("image/smart.png")));
		} catch (IOException exc) {}

		// Set Raw Data
		this.dataSet = dataSet;
		
		// Set JTable
		TableModel tableModel = new TableModel();
		JTable dataTable = new JTable(tableModel);
		dataTable.setRowHeight(20);
		dataTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		dataTable.getColumnModel().getColumn(0).setPreferredWidth(60);
		dataTable.getColumnModel().getColumn(1).setPreferredWidth(150);
		for (int i=2 ; i<tableModel.getColumnCount() ; i++) {
			dataTable.getColumnModel().getColumn(i).setPreferredWidth(80);
		}
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );

		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		dataTable.getColumnModel().getColumn(0).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		for (int i=2 ; i<tableModel.getColumnCount() ; i++) {
			dataTable.getColumnModel().getColumn(i).setCellRenderer( rightRenderer );
		}
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(dataTable);
		basePane.setPreferredSize(new Dimension(900, 700));
		this.add(basePane);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================

	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class TableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		public int[][]     itemValueCount = null;
		public double[][]  values         = null;
		public double[][]  bfValues       = null;
		public long[]      times          = null;

		public TableModel() {
			this.itemValueCount = dataSet.getItemValueCount();
			this.values         = dataSet.getTmsDataRecordValues();
			this.bfValues       = dataSet.getTmsDataRecordBeforeValues();
			this.times          = dataSet.getTmsDataRecordTimes();
		}
		
		public String getColumnName(int col) {
			
			if (col == 0) {
				return "No.";
			} else if (col ==1) {
				return "time";
			} else {
				String itemName = ItemCodeDefine.getItemName(itemValueCount[(col-2)/2][0]);
				if ( (col-2)%2 == 0) {
					return itemName + "(after)";
				} else {
					return itemName + "(before)";
				}
			}
		}
		
		public int getRowCount() { 
			if (dataSet == null) {
				return 0;
			}
			return dataSet.getTmsDataRecordCount();
		}
		
		public int getColumnCount() { 
			return itemValueCount.length*2 + 2; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return indexFormatter.format(row+1);
			} else if (col == 1) {
				return DateUtil.getDateStr(times[row]);
			} else {
				if ( (col-2)%2 == 0) {
					return valueFormatter.format(values[row][(col-2)/2]);
				} else {
					return valueFormatter.format(bfValues[row][(col-2)/2]);
				}
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	
} // end of class

