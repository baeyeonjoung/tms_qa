/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.evaluation.dialog;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.fileio.TmsDataWriterAsCsvFormat;
import com.snu.tms.qa.model.TmsDataset;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 * Raw data exporter to csv format
 * 
 */
public class DialogRawDataCsvExporter 
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(DialogRawDataCsvExporter.class);
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================

	//==========================================================================
	// Static Method
	//==========================================================================

	/**
	 * Export file(csv)
	 */
	public static void exportRawDataAsCsvFormat(
			JFrame parent, 
			String fileName,
			TmsDataset dataSet) {
		
		// Show JFileChooser
		JFileChooser fc = new JFileChooser();
		fc.setDialogType(JFileChooser.SAVE_DIALOG);
		fc.setSelectedFile(new File(fileName + ".csv"));
		fc.setFileFilter(new FileNameExtensionFilter("csv file","csv"));
		
		int returnVal = fc.showSaveDialog(parent);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			TmsDataWriterAsCsvFormat.writeDataFile(file.getAbsolutePath(), dataSet);
		}
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================

	
	
} // end of class
