/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.evaluation.dialog;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 * Show Graph
 * 
 */
public class DialogRawDataGraph extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long  serialVersionUID = 1L;
	
	private static final int[]  leftAxisCandidate  = 
		{ItemCodeDefine.FL1, ItemCodeDefine.TMP, ItemCodeDefine.O2};
	private static final int[]  rightAxisCandidate = 
		{ItemCodeDefine.NOX, ItemCodeDefine.SOX, ItemCodeDefine.TSP};
	
	private static final Color[] itemColors = {
		Color.DARK_GRAY, Color.RED, Color.BLUE, Color.GREEN,
		Color.CYAN, Color.MAGENTA, Color.YELLOW};
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  TmsDataset  dataSet;
	private  boolean[]   itemSelectionCheck;
	private  JFreeChart  chart;
	private  JPanel      chartBasePanel;
	
	private  int[]       items = null;
	@SuppressWarnings("rawtypes")
	private  JComboBox[] itemSelectionCombo = new JComboBox[6];
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogRawDataGraph(
			JFrame parent, 
			String stackInfoStr,
			TmsDataset dataSet) {
		
		// set dialog
		super(parent, "Raw data graph : " + stackInfoStr, ModalityType.MODELESS);
		
		// set icon
		try {
			setIconImage(ImageIO.read(new File("image/smart.png")));
		} catch (IOException exc) {}
		
		// Set Raw Data
		this.dataSet = dataSet;
		
		// create controlPane
		JPanel controlPanel = createControlPanel();
		
		// create graphPanel
		this.chartBasePanel = new JPanel(new BorderLayout());
		updateChart();
		
		// Add basePanel
		this.setPreferredSize(new Dimension(960, 600));
		this.setLayout(new BorderLayout());
		this.add(controlPanel, BorderLayout.NORTH);
		this.add(this.chartBasePanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create control Panel
	 */
	private JPanel createControlPanel() {
		
		// Set basePanel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(960, 80));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("Item selection control"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// set items
		int[][] itemRecordCounts = this.dataSet.getItemValueCount();
		this.items = new int[itemRecordCounts.length];
		for (int i=0 ; i<itemRecordCounts.length ; i++) {
			this.items[i] = itemRecordCounts[i][0];
		}
		
		// create CheckBox
		this.itemSelectionCombo[0] = createItemSelectionCombo(leftAxisCandidate);
		this.itemSelectionCombo[1] = createItemSelectionCombo(null);
		this.itemSelectionCombo[2] = createItemSelectionCombo(null);
		this.itemSelectionCombo[3] = createItemSelectionCombo(rightAxisCandidate);
		this.itemSelectionCombo[4] = createItemSelectionCombo(null);
		this.itemSelectionCombo[5] = createItemSelectionCombo(null);
		
		JLabel label = new JLabel("Left Axis");
		label.setBounds(20, 25, 100, 20);
		basePanel.add(label);
		
		this.itemSelectionCombo[0].setBounds(100, 25, 80, 20);
		basePanel.add(this.itemSelectionCombo[0]);
		
		this.itemSelectionCombo[1].setBounds(200, 25, 80, 20);
		basePanel.add(this.itemSelectionCombo[1]);

		this.itemSelectionCombo[2].setBounds(300, 25, 80, 20);
		basePanel.add(this.itemSelectionCombo[2]);

		label = new JLabel("Right Axis");
		label.setBounds(20, 50, 100, 20);
		basePanel.add(label);
		
		this.itemSelectionCombo[3].setBounds(100, 50, 80, 20);
		basePanel.add(this.itemSelectionCombo[3]);
		
		this.itemSelectionCombo[4].setBounds(200, 50, 80, 20);
		basePanel.add(this.itemSelectionCombo[4]);

		this.itemSelectionCombo[5].setBounds(300, 50, 80, 20);
		basePanel.add(this.itemSelectionCombo[5]);
		
		JButton applyButton = new JButton("apply selection");
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateItemSelection();
			}
		});
		applyButton.setBounds(400, 50, 120, 20);
		basePanel.add(applyButton);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * change item selection check
	 */
	public void setItemSelectionCheck(int i, boolean isCheck) {
		this.itemSelectionCheck[i] = isCheck;
	} // end of method
	
	/**
	 * update chart
	 */
	private void updateChart() {
		
		this.chartBasePanel.removeAll();
		this.chartBasePanel.add(createChartPanel(), BorderLayout.CENTER);
		this.chartBasePanel.updateUI();
		
	} // end of method
	
	
	/**
	 * create chart Panel
	 */
	private JPanel createChartPanel() {
		
		// create XYDataSet
		TimeSeriesCollection chartDataSet = createDataSet(0, 0);
		String seriesName = "";
		if (chartDataSet.getSeriesCount() != 0) {
			seriesName = (String)chartDataSet.getSeriesKey(0);
		}
		
		// create chart
		chart = ChartFactory.createTimeSeriesChart(
				"",  // title
				"date",             // x-axis label
				seriesName,   // y-axis label
				chartDataSet,            // data
				true,               // create legend?
				true,               // generate tooltips?
				false               // generate URLs?
		);

		// set Font
		chart.getTitle().setFont(new Font("����", Font.BOLD, 14));
		chart.getLegend().setItemFont(new Font("����", Font.BOLD, 12));

		// set series count
		int  seriesIndex = 0;
		
		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setOrientation(PlotOrientation.VERTICAL);
		plot.setDomainPannable(true);
		plot.setRangePannable(false);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		// set x-axis properties
		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));

		// set range axis
		XYItemRenderer renderer = new StandardXYItemRenderer();
		renderer.setSeriesPaint(seriesIndex,  itemColors[seriesIndex]);
		plot.setRenderer(renderer);
		
		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setLabelPaint(itemColors[seriesIndex]);
		rangeAxis.setTickLabelPaint(itemColors[seriesIndex]);
		
		// AXIS 2
		chartDataSet = createDataSet(0, 1);
		if (chartDataSet.getSeriesCount() != 0) {
			seriesIndex++;
			NumberAxis axis2 = new NumberAxis((String)chartDataSet.getSeriesKey(0));
			axis2.setAutoRangeIncludesZero(false);
			plot.setRangeAxis(seriesIndex, axis2);
			plot.setRangeAxisLocation(seriesIndex, AxisLocation.BOTTOM_OR_LEFT);
			plot.setDataset(seriesIndex, chartDataSet);
			plot.mapDatasetToRangeAxis(seriesIndex, seriesIndex);
			
			renderer = new StandardXYItemRenderer();
			renderer.setSeriesPaint(seriesIndex,  itemColors[seriesIndex]);
			plot.setRenderer(seriesIndex, renderer);
			axis2.setLabelPaint(itemColors[seriesIndex]);
			axis2.setTickLabelPaint(itemColors[seriesIndex]);
			
		}

		// AXIS 3
		chartDataSet = createDataSet(0, 2);
		if (chartDataSet.getSeriesCount() != 0) {
			seriesIndex++;
			NumberAxis axis2 = new NumberAxis((String)chartDataSet.getSeriesKey(0));
			axis2.setAutoRangeIncludesZero(false);
			plot.setRangeAxis(seriesIndex, axis2);
			plot.setRangeAxisLocation(seriesIndex, AxisLocation.BOTTOM_OR_LEFT);
			plot.setDataset(seriesIndex, chartDataSet);
			plot.mapDatasetToRangeAxis(seriesIndex, seriesIndex);

			renderer = new StandardXYItemRenderer();
			renderer.setSeriesPaint(seriesIndex,  itemColors[seriesIndex]);
			plot.setRenderer(seriesIndex, renderer);
			axis2.setLabelPaint(itemColors[seriesIndex]);
			axis2.setTickLabelPaint(itemColors[seriesIndex]);

		}
		
		// AXIS 4
		chartDataSet = createDataSet(1, 0);
		if (chartDataSet.getSeriesCount() != 0) {
			seriesIndex++;
			NumberAxis axis2 = new NumberAxis((String)chartDataSet.getSeriesKey(0));
			axis2.setAutoRangeIncludesZero(false);
			plot.setRangeAxis(seriesIndex, axis2);
			plot.setDataset(seriesIndex, chartDataSet);
			plot.mapDatasetToRangeAxis(seriesIndex, seriesIndex);

			renderer = new StandardXYItemRenderer();
			renderer.setSeriesPaint(seriesIndex,  itemColors[seriesIndex]);
			plot.setRenderer(seriesIndex, renderer);
			axis2.setLabelPaint(itemColors[seriesIndex]);
			axis2.setTickLabelPaint(itemColors[seriesIndex]);

		}

		// AXIS 5
		chartDataSet = createDataSet(1, 1);
		if (chartDataSet.getSeriesCount() != 0) {
			seriesIndex++;
			NumberAxis axis2 = new NumberAxis((String)chartDataSet.getSeriesKey(0));
			axis2.setAutoRangeIncludesZero(false);
			plot.setRangeAxis(seriesIndex, axis2);
			plot.setDataset(seriesIndex, chartDataSet);
			plot.mapDatasetToRangeAxis(seriesIndex, seriesIndex);

			renderer = new StandardXYItemRenderer();
			renderer.setSeriesPaint(seriesIndex,  itemColors[seriesIndex]);
			plot.setRenderer(seriesIndex, renderer);
			axis2.setLabelPaint(itemColors[seriesIndex]);
			axis2.setTickLabelPaint(itemColors[seriesIndex]);

		}

		// AXIS 6
		chartDataSet = createDataSet(1, 2);
		if (chartDataSet.getSeriesCount() != 0) {
			seriesIndex++;
			NumberAxis axis2 = new NumberAxis((String)chartDataSet.getSeriesKey(0));
			axis2.setAutoRangeIncludesZero(false);
			plot.setRangeAxis(seriesIndex, axis2);
			plot.setDataset(seriesIndex, chartDataSet);
			plot.mapDatasetToRangeAxis(seriesIndex, seriesIndex);

			renderer = new StandardXYItemRenderer();
			renderer.setSeriesPaint(seriesIndex,  itemColors[seriesIndex]);
			plot.setRenderer(seriesIndex, renderer);
			axis2.setLabelPaint(itemColors[seriesIndex]);
			axis2.setTickLabelPaint(itemColors[seriesIndex]);

		}
		
		// return
		return new ChartPanel(chart);
		
	} // end of method
	
	
	/**
	 * 
	 */
	private void updateItemSelection() {
		updateChart();
	} // end of method
	
	
	/**
	 * Create graph data set
	 */
	private TimeSeriesCollection createDataSet(int axis, int index) {
		
		// selected combo
		@SuppressWarnings("rawtypes")
		JComboBox selectedCombo = this.itemSelectionCombo[axis*3 + index];
		String selectedItem = (String)selectedCombo.getSelectedItem();
		
		// check selecte item
		if (selectedItem.equals("NON")) {
			return new TimeSeriesCollection();
		}
		
		int selectedItemCode = ItemCodeDefine.getItemCode(selectedItem);
		if (selectedItemCode == ItemCodeDefine.UNDEFINED) {
			return new TimeSeriesCollection();
		}
		
		// create timeseries data
		TimeSeries series = new TimeSeries(selectedItem);
		Minute time = null;
		double value = 0.0D;
		TmsDataRecord[] records = this.dataSet.getTmsDataRecords();
		for (TmsDataRecord record : records) {
			
			time = new Minute(new java.util.Date(record.getTime()));
			value = record.getDataCell(selectedItemCode).getData_value();
			if (value != -1.0D) {
				series.add(time, value);
			}
		}
		
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(series);
		
		return dataset;
		
	} // end of method
	
	
	/**
	 * create Item selection combobox
	 */
	private JComboBox<String> createItemSelectionCombo(int[] itemSelectionCandidates) {
		
		// set item
		String[] candidates = null;
		if (itemSelectionCandidates != null) {
			candidates = new String[this.items.length];
			for (int i=0 ; i<this.items.length ; i++) {
				candidates[i] = ItemCodeDefine.getItemName(this.items[i]);
			}
		} else {
			candidates = new String[this.items.length+1];
			candidates[0] = "NON";
			for (int i=0 ; i<this.items.length ; i++) {
				candidates[i+1] = ItemCodeDefine.getItemName(this.items[i]);
			}
		}
		
		// set comboBox
		JComboBox<String> comboBox = new JComboBox<String>(candidates);
		if (itemSelectionCandidates != null) {
			for (int i=0 ; i<itemSelectionCandidates.length ; i++) {
				for (int j=0 ; j<this.items.length ; j++) {
					if (itemSelectionCandidates[i] == this.items[j]) {
						comboBox.setSelectedItem(ItemCodeDefine.getItemName(itemSelectionCandidates[i]));
						return comboBox;
					}
				}
			}
			comboBox.setSelectedIndex(0);
			return comboBox;
		} else {
			comboBox.setSelectedIndex(0);
			return comboBox;
		}
		
		
	} // end of method
	
	//==========================================================================
	// Inner Class
	//==========================================================================

	
	
} // end of class
