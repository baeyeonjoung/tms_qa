/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.evaluation.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import com.snu.tms.qa.model.EvalDatasetManager;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.ruleset.DetectionDataCell;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.DetectionRuleset;
import com.snu.tms.qa.model.ruleset.OdrCell;
import com.snu.tms.qa.model.ruleset.OdrRecord;
import com.snu.tms.qa.model.ruleset.RawDataCell;
import com.snu.tms.qa.model.ruleset.RulesetResultFilterModel;
import com.snu.tms.qa.model.ruleset.RulesetWeightingModel;
import com.snu.tms.qa.util.DateUtil;
import com.snu.tms.qa.util.FileUtils;
import com.snu.tms.qa.util.NumberUtil;


/**
 * set detection weighting factor and threshold
 * 
 */
public class DialogOutlierDetectionThreshold extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  EvalDatasetManager      datasetManager;
	private  RulesetWeightingModel  rulesetWeighingModel;
	
	private  OdrRecord[]             odrModel;
	private  OdrRecord[]             filteredOdrModel;
	
	private  int[]  rulesetWeightingValues = new int[4];
	private  int[]  rulesetLevelValues     = new int[4];
	
	private  JTable  outlierCheckTable;
	private  JTable  rawDataTable;
	
	private  JTable  detectionResultTable;
	
	private Hashtable<Long, Integer>    timeHash;
	
	private RulesetResultFilterModel filterModel;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogOutlierDetectionThreshold(JFrame parent) {
		
		// set dialog
		super(parent, "", ModalityType.MODELESS);
		this.setTitle("Set outlier detection threshold and weighting factors");
		
		// set parameters
		this.datasetManager = EvalDatasetManager.getInstance();
		this.rulesetWeighingModel = this.datasetManager.getRulesetWeighingModel();
		
		this.rulesetWeightingValues[0] = this.rulesetWeighingModel.getMultivariateRulesetWeighting();
		this.rulesetWeightingValues[1] = this.rulesetWeighingModel.getBivariateRulesetWeighting();
		this.rulesetWeightingValues[2] = this.rulesetWeighingModel.getUnivariateRulesetWeighting();
		this.rulesetWeightingValues[3] = this.rulesetWeighingModel.getCorrectionRulesetWeighting();

		this.rulesetLevelValues[0] = this.rulesetWeighingModel.getWarningLevel();
		this.rulesetLevelValues[1] = this.rulesetWeighingModel.getMinorLevel();
		this.rulesetLevelValues[2] = this.rulesetWeighingModel.getMajorLevel();
		this.rulesetLevelValues[3] = this.rulesetWeighingModel.getCriticalLevel();
		
		// create OdrModel
		this.odrModel = createOdrModel();
		
		// create filter model
		if (this.odrModel.length != 0) {
			OdrCell[] odrCells = this.odrModel[0].odrCells;
			String[] rulesetNames = new String[odrCells.length+1];
			rulesetNames[0] = "Non";
			for (int i=0 ; i<odrCells.length ; i++) {
				rulesetNames[i+1] = odrCells[i].ruleTypeName;
			}
			this.filterModel = new RulesetResultFilterModel(
					this.datasetManager.getAllPatternData(), 
					rulesetNames);
		} else {
			this.filterModel = new RulesetResultFilterModel(
					this.datasetManager.getAllPatternData(), 
					new String[0]);
		}
		
		// create FilteredOdrModel
		setFilteredOdrModel();
		
		// set layout
		JPanel       controlPanel      = createControlPanel();
		JSplitPane   weightingPanel    = createThresholdPanel();
		JScrollPane  outlierTablePanel = createOutlierTablePanel();
		JScrollPane  rawDataTablePanel = createRawDataTablePanel();
		
		JPanel upperBasePanel = new JPanel(new BorderLayout());
		controlPanel.setPreferredSize(new Dimension(1000, 50));
		weightingPanel.setPreferredSize(new Dimension(1000, 180));
		upperBasePanel.add(controlPanel, BorderLayout.NORTH);
		upperBasePanel.add(weightingPanel, BorderLayout.CENTER);
		
		JSplitPane lowerBasePanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		lowerBasePanel.setDividerLocation(300);
		lowerBasePanel.setDividerSize(5);
		
		lowerBasePanel.add(outlierTablePanel);
		lowerBasePanel.add(rawDataTablePanel);
		
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(1000, 800));
		this.add(upperBasePanel, BorderLayout.NORTH);
		this.add(lowerBasePanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create control panel
	 */
	private JPanel createControlPanel() {
		
		// create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("  outlier detection threshold control  "),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));

		// add save threshold button
		JButton applyButton = new JButton("save threshold");
		applyButton.setEnabled(true);
		applyButton.setBounds(20, 20, 150, 20);
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				applyWeightingValues();
				closeWindow();
			}
		});
		basePanel.add(applyButton);
		
		JButton filteringButton = new JButton("filter condition");
		filteringButton.setBounds(200, 20, 200, 20);
		filteringButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showResultFiltering();
				outlierCheckTable.updateUI();
			}
		});
		basePanel.add(filteringButton);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * create edit threshold panel
	 */
	private JSplitPane createThresholdPanel() {
		
		// create pannel
		JPanel       editPanel           = createEditThresholdPanel();
		JScrollPane  detectionTablePanel = createDetectionResultTable();

		JSplitPane basePanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		basePanel.setDividerLocation(650);
		basePanel.setDividerSize(5);
		
		basePanel.add(editPanel);
		basePanel.add(detectionTablePanel);
		
		basePanel.setPreferredSize(new Dimension(1000, 180));
		
		return basePanel;
		
	} // end of method
	
	/**
	 * create control panel
	 */
	private JPanel createEditThresholdPanel() {
		
		// create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("  weighting factor for outlier detection ruleset  "),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// set layout
		JLabel weightingLabel = null;
		weightingLabel = new JLabel("Weighting value of multivariate ruleset");
		weightingLabel.setBounds(20, 25, 250, 25);
		basePanel.add(weightingLabel);
		
		weightingLabel = new JLabel("Weighting value of bivariate ruleset");
		weightingLabel.setBounds(20, 55, 250, 25);
		basePanel.add(weightingLabel);

		weightingLabel = new JLabel("Weighting value of univariate ruleset");
		weightingLabel.setBounds(20, 85, 250, 25);
		basePanel.add(weightingLabel);

		weightingLabel = new JLabel("Weighting value of correction ruleset");
		weightingLabel.setBounds(20, 115, 250, 25);
		basePanel.add(weightingLabel);
		
		JComboBox<String> weightingValueCombo = null;
		String[] weightingValues = {"0", "1", "2", "3", "4" ,"5"};
		weightingValueCombo = new JComboBox<String>(weightingValues);
		weightingValueCombo.setBounds(270, 25, 80, 25);
		weightingValueCombo.setSelectedIndex(this.rulesetWeightingValues[0]);
		weightingValueCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("rawtypes")
				int selectedIndex = ((JComboBox)e.getSource()).getSelectedIndex();
				rulesetWeightingValues[0] = selectedIndex;
			}
		});
		basePanel.add(weightingValueCombo);
		
		weightingValueCombo = new JComboBox<String>(weightingValues);
		weightingValueCombo.setBounds(270, 55, 80, 25);
		weightingValueCombo.setSelectedIndex(this.rulesetWeightingValues[1]);
		weightingValueCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("rawtypes")
				int selectedIndex = ((JComboBox)e.getSource()).getSelectedIndex();
				rulesetWeightingValues[1] = selectedIndex;
			}
		});
		basePanel.add(weightingValueCombo);

				weightingValueCombo = new JComboBox<String>(weightingValues);
		weightingValueCombo.setBounds(270, 85, 80, 25);
		weightingValueCombo.setSelectedIndex(this.rulesetWeightingValues[2]);
		weightingValueCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("rawtypes")
				int selectedIndex = ((JComboBox)e.getSource()).getSelectedIndex();
				rulesetWeightingValues[2] = selectedIndex;
			}
		});
		basePanel.add(weightingValueCombo);

				weightingValueCombo = new JComboBox<String>(weightingValues);
		weightingValueCombo.setBounds(270, 115, 80, 25);
		weightingValueCombo.setSelectedIndex(this.rulesetWeightingValues[3]);
		weightingValueCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("rawtypes")
				int selectedIndex = ((JComboBox)e.getSource()).getSelectedIndex();
				rulesetWeightingValues[3] = selectedIndex;
			}
		});
		basePanel.add(weightingValueCombo);

		JLabel levelLabel = null;
		levelLabel = new JLabel("WARNING  level threshold");
		levelLabel.setBounds(390, 25, 160, 25);
		basePanel.add(levelLabel);
		
		levelLabel = new JLabel("MINOR    level threshold");
		levelLabel.setBounds(390, 55, 160, 25);
		basePanel.add(levelLabel);

		levelLabel = new JLabel("MAJOR    level threshold");
		levelLabel.setBounds(390, 85, 160, 25);
		basePanel.add(levelLabel);

		levelLabel = new JLabel("CRITICAL level threshold");
		levelLabel.setBounds(390, 115, 160, 25);
		basePanel.add(levelLabel);
		
		JComboBox<String> levelValueCombo = null;
		String[] levelValues = {
				" 1", " 2", " 3", " 4" ," 5", " 6", " 7", " 8", " 9", "10", 
				"11", "12", "13", "14" ,"15", "16", "17", "18", "19", "20", 
				"21", "22", "23", "24" ,"25", "26", "27", "28", "29", "30", 
				"31", "32", "33", "34" ,"35", "36", "37", "38", "39", "40", 
				"41", "42", "43", "44" ,"45", "46", "47", "48", "49", "50"};
		levelValueCombo = new JComboBox<String>(levelValues);
		levelValueCombo.setBounds(550, 25, 80, 25);
		levelValueCombo.setSelectedIndex(this.rulesetLevelValues[0] - 1);
		levelValueCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("rawtypes")
				int selectedIndex = ((JComboBox)e.getSource()).getSelectedIndex();
				rulesetLevelValues[0] = selectedIndex + 1;
			}
		});
		basePanel.add(levelValueCombo);
		
		levelValueCombo = new JComboBox<String>(levelValues);
		levelValueCombo.setBounds(550, 55, 80, 25);
		levelValueCombo.setSelectedIndex(this.rulesetLevelValues[1] - 1);
		levelValueCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("rawtypes")
				int selectedIndex = ((JComboBox)e.getSource()).getSelectedIndex();
				rulesetLevelValues[1] = selectedIndex + 1;
			}
		});
		basePanel.add(levelValueCombo);

		levelValueCombo = new JComboBox<String>(levelValues);
		levelValueCombo.setBounds(550, 85, 80, 25);
		levelValueCombo.setSelectedIndex(this.rulesetLevelValues[2] - 1);
		levelValueCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("rawtypes")
				int selectedIndex = ((JComboBox)e.getSource()).getSelectedIndex();
				rulesetLevelValues[2] = selectedIndex + 1;
			}
		});
		basePanel.add(levelValueCombo);

		levelValueCombo = new JComboBox<String>(levelValues);
		levelValueCombo.setBounds(550, 115, 80, 25);
		levelValueCombo.setSelectedIndex(this.rulesetLevelValues[3] - 1);
		levelValueCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("rawtypes")
				int selectedIndex = ((JComboBox)e.getSource()).getSelectedIndex();
				rulesetLevelValues[3] = selectedIndex + 1;
			}
		});
		basePanel.add(levelValueCombo);
		
		JButton applyButton = new JButton("update detection table");
		applyButton.setBounds(20, 145, 200, 20);
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				applyWeightingValues();
				outlierCheckTable.updateUI();
				detectionResultTable.updateUI();
			}
		});
		basePanel.add(applyButton);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * create detection reuslt table
	 */
	private JScrollPane createDetectionResultTable() {
		
		// Create Table
		DecteionResultTableModel detectionResultTableModel = new DecteionResultTableModel();
		this.detectionResultTable = new JTable(detectionResultTableModel);
		this.detectionResultTable.setRowHeight(20);
		//this.detectionResultTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		this.detectionResultTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.detectionResultTable.getColumnModel().getColumn(0).setPreferredWidth(50);
		
		int patternCount = this.datasetManager.getPatternCount();
		for (int i=0 ; i<patternCount ; i++) {
			this.detectionResultTable.getColumnModel().getColumn(i+1).setCellRenderer( rightRenderer );
			this.detectionResultTable.getColumnModel().getColumn(i+1).setPreferredWidth(50);
		}
				
		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.detectionResultTable);
		return basePane;
		
	} // end of method
	
	/**
	 * create control panel
	 */
	private JScrollPane createOutlierTablePanel() {
		
		// Create Table
		OutlierCheckTableModel outlierCheckTableModel = new OutlierCheckTableModel();
		this.outlierCheckTable = new JTable(outlierCheckTableModel);
		this.outlierCheckTable.setRowHeight(20);
		this.outlierCheckTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.outlierCheckTable.getColumnModel().getColumn(0).setPreferredWidth(50);
		this.outlierCheckTable.getColumnModel().getColumn(1).setPreferredWidth(150);
		this.outlierCheckTable.getColumnModel().getColumn(2).setPreferredWidth(50);
		this.outlierCheckTable.getColumnModel().getColumn(3).setPreferredWidth(120);
		this.outlierCheckTable.getColumnModel().getColumn(4).setPreferredWidth(60);
		this.outlierCheckTable.getColumnModel().getColumn(5).setPreferredWidth(60);
		this.outlierCheckTable.getColumnModel().getColumn(6).setPreferredWidth(80);
		for (int i=7 ; i<outlierCheckTableModel.getColumnCount() ; i++) {
			this.outlierCheckTable.getColumnModel().getColumn(i).setPreferredWidth(100);
		}
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		this.outlierCheckTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.outlierCheckTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.outlierCheckTable.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		this.outlierCheckTable.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
		this.outlierCheckTable.getColumnModel().getColumn(4).setCellRenderer( centerRenderer );
		this.outlierCheckTable.getColumnModel().getColumn(5).setCellRenderer( centerRenderer );
		this.outlierCheckTable.getColumnModel().getColumn(6).setCellRenderer( centerRenderer );
		
		// add List selection Model
		this.outlierCheckTable.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent event) {
						
						int  selectedRow = outlierCheckTable.getSelectedRow();
						String timeStr = (String)outlierCheckTable.getValueAt(selectedRow, 1);
						long selectedTime = DateUtil.getDate(timeStr);
						changeTableRowSelection(selectedTime);
						
					}
				});
				
		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.outlierCheckTable);
		return basePane;
		
	} // end of method
	
	/**
	 * create control panel
	 */
	private JScrollPane createRawDataTablePanel() {
		
		// Create Table
		RawDataTableModel rawDataTableModel = new RawDataTableModel();
		this.rawDataTable = new JTable(rawDataTableModel);
		this.rawDataTable.setRowHeight(20);
		this.rawDataTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.rawDataTable.getColumnModel().getColumn(0).setPreferredWidth(150);
		this.rawDataTable.getColumnModel().getColumn(1).setPreferredWidth(50);
		this.rawDataTable.getColumnModel().getColumn(2).setPreferredWidth(120);
		for (int i=3 ; i<rawDataTableModel.getColumnCount() ; i++) {
			this.rawDataTable.getColumnModel().getColumn(i).setPreferredWidth(100);
		}
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		this.rawDataTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.rawDataTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.rawDataTable.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		for (int i=3 ; i<rawDataTableModel.getColumnCount() ; i++) {
			this.rawDataTable.getColumnModel().getColumn(i).setCellRenderer( rightRenderer );
		}
				
		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.rawDataTable);
		return basePane;
		
	} // end of method
	
	/**
	 * export result as a csv file format
	 */
	private void exportAsCsv() {
		
		// set file name
		String name = String.format("%s-%d.csv", this.odrModel[0].factCode, this.odrModel[0].stckCode);
		
		// Show JFileChooser
		JFileChooser fc = new JFileChooser();
		fc.setDialogType(JFileChooser.SAVE_DIALOG);
		fc.setSelectedFile(new File(name));
		fc.setFileFilter(new FileNameExtensionFilter("csv file","csv"));
		
		int returnVal = fc.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			writeDataFile(file.getAbsolutePath());
		}
		
	} // end of method
	
	/**
	 * apply user selected weighting values
	 */
	private void applyWeightingValues() {
		
		// set Parameters
		this.rulesetWeighingModel.setMultivariateRulesetWeighting(rulesetWeightingValues[0]);
		this.rulesetWeighingModel.setBivariateRulesetWeighting(rulesetWeightingValues[1]);
		this.rulesetWeighingModel.setUnivariateRulesetWeighting(rulesetWeightingValues[2]);
		this.rulesetWeighingModel.setCorrectionRulesetWeighting(rulesetWeightingValues[3]);
		
		this.rulesetWeighingModel.setWarningLevel(rulesetLevelValues[0]);
		this.rulesetWeighingModel.setMinorLevel(rulesetLevelValues[1]);
		this.rulesetWeighingModel.setMajorLevel(rulesetLevelValues[2]);
		this.rulesetWeighingModel.setCriticalLevel(rulesetLevelValues[3]);
		
	} // end of method
	
	/**
	 * Show result filtering dialog
	 */
	private void showResultFiltering() {
		
		// set filter
		JDialog dialog = new DialogOutlierDetectionThresholdFilter(this, this.filterModel);
		
		// change filteredOdrRecord
		setFilteredOdrModel();
		
		// close window
		dialog.dispose();
		
	} // end of method
	
	/**
	 * create OdrModel
	 */
	private OdrRecord[] createOdrModel() {
		
		// get Pattern Data model
		HashSet<String> multiRuleHashSet = new HashSet<String>();
		HashSet<String> biRuleHashSet    = new HashSet<String>();
		HashSet<String> uniRuleHashSet   = new HashSet<String>();
		HashSet<String> corrRuleHashSet  = new HashSet<String>();

		DetectionPatternDataset[] patternDatasetArray = this.datasetManager.getAllPatternData();
		DetectionRuleset[] rulesets;
		for (int i=0 ; i<patternDatasetArray.length ; i++) {
			
			rulesets = patternDatasetArray[i].getAllRuleset();
			for (int j=0 ; j<rulesets.length ; j++) {
				
				switch(rulesets[j].getRulesetType()) {
				case DetectionRuleset.RULESET_PCA_TSS:
					multiRuleHashSet.add(rulesets[j].getKeyName());
					break;
				case DetectionRuleset.RULESET_PCA_SPE:
					multiRuleHashSet.add(rulesets[j].getKeyName());
					break;
				case DetectionRuleset.RULESET_CORR:
					biRuleHashSet.add(rulesets[j].getKeyName());
					break;
				case DetectionRuleset.RULESET_SPC:
					uniRuleHashSet.add(rulesets[j].getKeyName());
					break;
				case DetectionRuleset.RULESET_PD:
					corrRuleHashSet.add(rulesets[j].getKeyName());
					break;
				
				}
			}
		}
		
		Hashtable<String, Integer> rulesetOrderHash = new Hashtable<String, Integer>();
		int index = 0;
		String key = "";
		for (Iterator<String> e=multiRuleHashSet.iterator() ; e.hasNext() ; ) {
			key = e.next();
			rulesetOrderHash.put(key, index++);
		}
		for (Iterator<String> e=biRuleHashSet.iterator() ; e.hasNext() ; ) {
			key = e.next();
			rulesetOrderHash.put(key, index++);
		}
		for (Iterator<String> e=uniRuleHashSet.iterator() ; e.hasNext() ; ) {
			key = e.next();
			rulesetOrderHash.put(key, index++);
		}
		for (Iterator<String> e=corrRuleHashSet.iterator() ; e.hasNext() ; ) {
			key = e.next();
			rulesetOrderHash.put(key, index++);
		}
		// add record
		Vector<OdrRecord> odrRecordArray = new Vector<OdrRecord>();
		DetectionRecord[] detectionRecords;
		String factCode, factName, patternName;
		int    stckCode, patternId;
		long   time;
		OdrRecord  odrRecord;
		for (int i=0 ; i<patternDatasetArray.length ; i++) {
			
			factCode    = patternDatasetArray[i].getFactoryId();
			factName    = patternDatasetArray[i].getFactoryName();
			stckCode    = patternDatasetArray[i].getStackId();
			patternName = patternDatasetArray[i].getPatternName();
			patternId   = patternDatasetArray[i].getPatternId();
			detectionRecords = patternDatasetArray[i].getAllDetectionRecords();
			for (int j=0 ; j<detectionRecords.length ; j++) {
				
				// create odrRecord
				time = detectionRecords[j].getTime();
				odrRecord = new OdrRecord(
						factCode, factName, stckCode, 
						time, patternId, patternName, rulesetOrderHash,
						rulesetWeighingModel);
				odrRecordArray.add(odrRecord);
				
				// set odrCell
				if (!patternDatasetArray[i].isEnabled()) {
					continue;
				}
				DetectionDataCell[] outlierCells = detectionRecords[j].getAllDetectionDataCell();
				int  odrCellIndex;
				for (int k=0 ; k<outlierCells.length ; k++) {
					if (!outlierCells[k].getRuleset().isRulesetEnabled()) {
						continue;
					}
					odrCellIndex = rulesetOrderHash.get(outlierCells[k].getRuleset().getKeyName());
					odrRecord.odrCells[odrCellIndex].setIsOutlier(outlierCells[k].isOutlier());
				}
			}
		}
		
		// return
		OdrRecord[] odrRecords = odrRecordArray.toArray(new OdrRecord[odrRecordArray.size()]);
		Arrays.sort(odrRecords);
		
		return odrRecords;
		
	} // end of method
	
	/**
	 * Set Filtered OdrModel
	 */
	private void setFilteredOdrModel() {
		
		// create temporary Array
		Vector<OdrRecord> odrRecordArray = new Vector<OdrRecord>();
		
		// search odrRecord to show
		for (OdrRecord odrRecord : this.odrModel) {
			
			// check Pattern
			if (!this.filterModel.getPatternFilter(odrRecord.patternId)) {
				continue;
			}
			
			// check Level Filter
			int level = rulesetWeighingModel.getLevel(odrRecord.getOutlierScore());
			if (!this.filterModel.getLevelFilter(level)) {
				continue;
			}
			
			// check ruleset filter
			boolean isShow = false;
			for (OdrCell odrCell : odrRecord.odrCells) {
				if (odrCell.isOutlier == 1 && this.filterModel.getRulesetFilter(odrCell.ruleTypeName)) {
					isShow = true;
				}
			}
			
			if (this.filterModel.getRulesetFilter("Non")) {
				if (odrRecord.getOutlierScore() == 0) {
					isShow = true;
				}
			}
			
			if (isShow) {
				odrRecordArray.add(odrRecord);
			}
			
		}
		
		// Set filteredOdrRecord
		this.filteredOdrModel = odrRecordArray.toArray(new OdrRecord[odrRecordArray.size()]);
		
	} // end of method
	
	/**
	 * file save
	 */
	private void writeDataFile(String outputFilePath) {
		
		// Prepare file writer
		BufferedWriter writer = null;
		try {
			// Prepare file
			File resultFile = new File(outputFilePath);
			if (resultFile.exists()) {
				FileUtils.deleteFile(outputFilePath);
			}
			
			// Prepare writer
			Charset charset = Charset.forName("KSC5601");
			Path path = FileSystems.getDefault().getPath(outputFilePath);
			writer = Files.newBufferedWriter(path, charset);
			
			// prepare Raw Data
			DetectionPatternDataset[] patternDatasets = datasetManager.getAllPatternData();
			Hashtable<Long, DetectionRecord> detectionRecordHash = new Hashtable<Long, DetectionRecord>();
			DetectionRecord[] subRecords;
			for (int i=0 ; i<patternDatasets.length ; i++) {
				subRecords = patternDatasets[i].getAllDetectionRecords();
				for (int j=0 ; j<subRecords.length ; j++) {
					detectionRecordHash.put(subRecords[j].getTime(), subRecords[j]);
				}
			}
			int[] items = patternDatasets[0].getItems();
			
			// write column name
			String columnName = "";
			columnName += "Time, ID, Pattern Name, Count, Score, Level";
			for (int i=0 ; i<odrModel[0].odrCells.length ; i++) {
				columnName += ", " + odrModel[0].odrCells[i].ruleTypeName;
			}
			for (int i=0 ; i<items.length ; i++) {
				columnName += ", " + ItemCodeDefine.getItemName(items[i]);
			}
			writer.write(columnName);
			writer.write("\n");
			
			// write records
			String levelStr = "";
			int    levelScore = 0;
			DetectionRecord record;
			for (int i=0 ; i<odrModel.length ; i++) {
				
				writer.write(DateUtil.getDateStr(odrModel[i].time));
				writer.write(", " + odrModel[i].patternId);
				writer.write(", " + odrModel[i].patternName);
				writer.write(", " + odrModel[i].getOutlierCount());
				levelScore = odrModel[i].getOutlierScore();
				writer.write(", " + levelScore);
				
				levelStr = "";
				if (levelScore < rulesetWeighingModel.getWarningLevel()) {
					levelStr = " ";
				} else if (levelScore < rulesetWeighingModel.getMinorLevel()) {
					levelStr = "WARNING";
				} else if (levelScore < rulesetWeighingModel.getMajorLevel()) {
					levelStr = "MINOR";
				} else if (levelScore < rulesetWeighingModel.getCriticalLevel()) {
					levelStr = "MAJOR";
				} else {
					levelStr = "CRITICAL";
				}
				writer.write(", " + levelStr);
				
				for (int j=0 ; j<odrModel[i].odrCells.length ; j++) {
					writer.write(", " + odrModel[i].odrCells[j].isOutlier);
				}
				
				record = detectionRecordHash.get(odrModel[i].time);
				RawDataCell[] rawDataCells = record.getRawDataCell(items);
				for (int j=0 ; j<rawDataCells.length ; j++) {
					writer.write(", " + NumberUtil.getNonCommaNumberStr(rawDataCells[j].getData_value()));
				}
				writer.write("\n");

			}
			
		} catch (Exception e) {
			System.out.println(e.toString());
		} finally {
			try {
				writer.close();
			} catch (Exception ee) {}
		}
		
	} // end of method
	
	/**
	 * Create time hashtable
	 */
	private void createTimeHashtable(DetectionRecord[] records) {
		
		// create time hash
		this.timeHash = new Hashtable<Long, Integer>();
		
		// set data
		for (int i=0 ; i<records.length ; i++) {
			this.timeHash.put(records[i].getTime(), i);
		}
		
	} // end of method
	
	/**
	 * Table Selection
	 */
	private void changeTableRowSelection(long selectedTime) {
		
		// get table row index from time hash
		Integer row = this.timeHash.get(selectedTime);
		if (row == null) {
			return;
		}
		
		// table selection
		this.rawDataTable.setRowSelectionInterval(row, row);
		JViewport viewport = (JViewport)this.rawDataTable.getParent();
		Rectangle rect = this.rawDataTable.getCellRect(row, 0, true);
		Rectangle viewRect = viewport.getViewRect();
		
		int y = viewRect.y;
		if (rect.y >= viewRect.y && rect.y <= (viewRect.y + viewRect.height - rect.height)) {
			
		} else if (rect.y < viewRect.y) {
			y = rect.y;
		} else if (rect.y > (viewRect.y + viewRect.height - rect.height)) {
			 y = rect.y - viewRect.height + rect.height;
		}
		viewport.setViewPosition(new Point(0, y));
		
	} // end of method
	
	/**
	 * Close this window
	 */
	public void closeWindow() {
		this.dispose();
	} // end of method

	
	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * Outlier Check Table Model
	 */
	class OutlierCheckTableModel extends AbstractTableModel {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		private static final long serialVersionUID = 1L;
		
		//==========================================================================
		// Fields
		//==========================================================================
		private String[] columnNames;
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public OutlierCheckTableModel() {
			
			this.columnNames = new String[odrModel[0].odrCells.length + 7];
			this.columnNames[0] = "No.";
			this.columnNames[1] = "Time";
			this.columnNames[2] = "ID";
			this.columnNames[3] = "Pattern Name";
			this.columnNames[4] = "Count";
			this.columnNames[5] = "Score";
			this.columnNames[6] = "Level";
			for (int i=0 ; i<odrModel[0].odrCells.length ; i++) {
				this.columnNames[i+7] = odrModel[0].odrCells[i].ruleTypeName;
			}
		}
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			return filteredOdrModel.length;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return row+1;
			} else if (col == 1) {
				return DateUtil.getDateStr(filteredOdrModel[row].time);
			} else if (col == 2) {
				return filteredOdrModel[row].patternId;
			} else if (col == 3) {
				return filteredOdrModel[row].patternName;
			} else if (col == 4) {
				return filteredOdrModel[row].getOutlierCount();
			} else if (col == 5) {
				return filteredOdrModel[row].getOutlierScore();
			} else if (col == 6) {
				return filteredOdrModel[row].getOutlierLevelStr();
			} else {
				return new Boolean(filteredOdrModel[row].odrCells[col-7].isOutlier == 1);
			}
		
		} // end of method
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return String.class;
			} else if (col == 1) {
				return Integer.class;
			} else if (col == 2) {
				return String.class;
			} else if (col == 3) {
				return Integer.class;
			} else if (col == 4) {
				return Integer.class;
			} else if (col == 5) {
				return String.class;
			} else {
				return Boolean.class;
			}
			
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * RawData Table Model
	 */
	class RawDataTableModel extends AbstractTableModel {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		private static final long serialVersionUID = 1L;
		
		//==========================================================================
		// Fields
		//==========================================================================
		private String[] columnNames;
		private int[]    items;
		private DetectionRecord[] records;
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public RawDataTableModel() {
			
			// get Raw Data
			DetectionPatternDataset[] patternDatasets = datasetManager.getAllPatternData();
			this.items = patternDatasets[0].getItems();
			
			// set column names
			this.columnNames = new String[items.length + 3];
			this.columnNames[0] = "Time";
			this.columnNames[1] = "ID";
			this.columnNames[2] = "Pattern Name";
			for (int i=0 ; i<items.length ; i++) {
				this.columnNames[i+3] = ItemCodeDefine.getItemName(items[i]);
			}
			
			// set data
			Vector<DetectionRecord> detectionRecordArray = new Vector<DetectionRecord>();
			DetectionRecord[] subRecords;
			for (int i=0 ; i<patternDatasets.length ; i++) {
				subRecords = patternDatasets[i].getAllDetectionRecords();
				for (int j=0 ; j<subRecords.length ; j++) {
					detectionRecordArray.add(subRecords[j]);
				}
			}
			
			this.records = detectionRecordArray.toArray(new DetectionRecord[detectionRecordArray.size()]);
			Arrays.sort(this.records);
			createTimeHashtable(this.records);
			
		} // end of constructor
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			return records.length;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return DateUtil.getDateStr(records[row].getTime());
			} else if (col == 1) {
				return records[row].getPatternId();
			} else if (col == 2) {
				return records[row].getPatternName();
			} else {
				return this.records[row].getRawDataCell(this.items[col-3]).getData_value();
			}
		
		} // end of method
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return String.class;
			} else if (col == 1) {
				return Integer.class;
			} else if (col == 2) {
				return String.class;
			} else {
				return Double.class;
			}
			
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * DetectionResultTable Model
	 */
	class DecteionResultTableModel extends AbstractTableModel {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		private static final long serialVersionUID = 1L;
		
		//==========================================================================
		// Fields
		//==========================================================================
		private  String[]     columnNames;
		private  OdrRecord[]  odrRecords;
		private  int[]        patternIndices;
		
		private  String[]     rowName = {"normal", "warning", "minor", "major", "critical"};
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public DecteionResultTableModel() {
			
			// get OdrRecrod
			DetectionPatternDataset[] patternDatasets = datasetManager.getAllPatternData();
			this.patternIndices = new int[patternDatasets.length];
			for (int i=0 ; i<patternDatasets.length ; i++) {
				patternIndices[i] = patternDatasets[i].getPatternId();
			}
			this.odrRecords = odrModel;
			
			// set column names
			this.columnNames = new String[patternDatasets.length + 1];
			this.columnNames[0] = "level";
			for (int i=0 ; i<patternDatasets.length ; i++) {
				this.columnNames[i+1] = String.format("[ %d ] %s", 
						patternDatasets[i].getPatternId(),
						patternDatasets[i].getPatternName());
			}
			
		} // end of constructor
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			return rowName.length;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return this.rowName[row];
			} else {
				if (row == 0) {
					return NumberUtil.getNumberStr(
							getOdrRecord(this.patternIndices[col-1], OdrRecord.OUTLIER_LEVEL_NON));
				} else if (row == 1) {
					return NumberUtil.getNumberStr(
							getOdrRecord(this.patternIndices[col-1], OdrRecord.OUTLIER_LEVEL_WARNING));
				} else if (row == 2) {
					return NumberUtil.getNumberStr(
							getOdrRecord(this.patternIndices[col-1], OdrRecord.OUTLIER_LEVEL_MINOR));
				} else if (row == 3) {
					return NumberUtil.getNumberStr(
							getOdrRecord(this.patternIndices[col-1], OdrRecord.OUTLIER_LEVEL_MAJOR));
				} else if (row == 4) {
					return NumberUtil.getNumberStr(
							getOdrRecord(this.patternIndices[col-1], OdrRecord.OUTLIER_LEVEL_CRITICAL));
				} else {
					return 0;
				}
			}
		
		} // end of method
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return String.class;
			} else if (col == 1) {
				return Integer.class;
			} else if (col == 2) {
				return String.class;
			} else {
				return Double.class;
			}
			
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
		/**
		 * get OdrCount
		 */
		public int getOdrRecord(int patternIndex, int odrLevel) {
			
			int count = 0;
			for (int i=0 ; i<this.odrRecords.length ; i++) {
				
				if (odrRecords[i].patternId == patternIndex) {
					if (odrRecords[i].getOutlierLevel() == odrLevel) {
						count++;
					}
				}
				
			}
			return count;
			
		} // end of method
		
	} // end of class
	
} // end of class
