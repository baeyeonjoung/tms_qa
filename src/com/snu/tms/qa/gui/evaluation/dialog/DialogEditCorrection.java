/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.evaluation.dialog;

import com.snu.tms.qa.analysis.outlier.DoAnalysisReverseCorrection;
import com.snu.tms.qa.model.CorrectionItemInfo;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.TmsDataset;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * Edit correction factor
 * 
 */
public class DialogEditCorrection extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  TmsDataset dataSet;
	
	private  StackInfo  stackInfo;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogEditCorrection(
			JFrame parent, 
			String stackInfoStr,
			TmsDataset dataSet) {
		
		// set dialog
		super(parent, "Raw data viewer : " + stackInfoStr, ModalityType.MODELESS);

		// set icon
		try {
			setIconImage(ImageIO.read(new File("image/smart.png")));
		} catch (IOException exc) {}
		
		// Set Raw Data
		this.dataSet = dataSet;
		this.stackInfo = new StackInfo(dataSet.getTmsDataHeader().getStackInfo());
		
		// create controlPane
		JPanel controlPanel = createControlPanel();
		
		// create Table base Panel
		JScrollPane tablePanel = createTablePanel();
		
		// add basePanel
		JPanel basePanel = new JPanel(new BorderLayout());
		basePanel.add(controlPanel, BorderLayout.NORTH);
		basePanel.add(tablePanel, BorderLayout.CENTER);
		this.add(basePanel);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create control panel
	 */
	private JPanel createControlPanel() {
		
		// Set basePanel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(600, 40));
		
		// create button
		JButton applyButton = new JButton("apply");
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				applyChange();
				closeWindow();
			}
		});
		applyButton.setBounds(20, 10, 120, 20);
		basePanel.add(applyButton);
		
		return basePanel;
		
	} // end of method
	
	
	/**
	 * create table panel
	 */
	private JScrollPane createTablePanel() {
		
		// Set JTable
		TableModel tableModel = new TableModel();
		JTable dataTable = new JTable(tableModel);

		dataTable.setRowHeight(30);
		dataTable.getColumnModel().getColumn(0).setPreferredWidth(100);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		dataTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		
		// Add basePanel
		JScrollPane tablePanel = new JScrollPane(dataTable);
		tablePanel.setPreferredSize(new Dimension(600, 300));
		
		return tablePanel;
		
	} // end of method
	
	/**
	 * Close this window
	 */
	public void closeWindow() {
		this.dispose();
	} // end of method
	
	/**
	 * Apply Pattern Analysis Result
	 */
	public void applyChange() {
		
		// set correction factor
		this.dataSet.getTmsDataHeader().getStackInfo().setCorrectionInfo(this.stackInfo);
		
		// do reverse correction
		DoAnalysisReverseCorrection.doReverseCorrection(this.dataSet);
		
	} // end of method

	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class TableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		public int[]      items     = null;

		public TableModel() {

			// get items
			this.items = dataSet.getItems();
		
		} // end of constructor
		
		public String getColumnName(int col) {
			
			if (col == 0) {
				return "Item";
			} else if (col ==1) {
				return "Oxygon corr.";
			} else if (col ==2) {
				return "Temperature corr.";
			} else if (col ==3) {
				return "Moisture corr.";
			} else {
				return "";
			}
		}
		
		public int getRowCount() { 
			if (dataSet == null) {
				return 0;
			}
			return this.items.length;
		}
		
		public int getColumnCount() { 
			return 4;
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return ItemCodeDefine.getItemName(this.items[row]);
			} else if (col == 1) {
				return new Boolean(stackInfo.getCorrectionInfo(this.items[row]).isOxygenApplied());
			} else if (col == 2) {
				return new Boolean(stackInfo.getCorrectionInfo(this.items[row]).isTemperatureApplied());
			} else if (col == 3) {
				return new Boolean(stackInfo.getCorrectionInfo(this.items[row]).isMoistureApplied());
			} else {
				return "";
			}
		
		} // end of method
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return String.class;
			} else {
				return Boolean.class;
			}
			
		}

		public boolean isCellEditable(int row, int col) {
			
			if (col == 0) {
				return false;
			} else {
				return true;
			}
		}
		
		public void setValueAt(Object value, int row, int col) {
			
			if (col == 0) {
				return;
			} 
			
			// get target item
			int item = this.items[row];
			CorrectionItemInfo corrInfo = stackInfo.getCorrectionInfo(item);
			boolean isChecked = ((Boolean) value).booleanValue();
			if (col == 1) {
				corrInfo.setOxygenApplied(isChecked);
			} else if (col == 2) {
				corrInfo.setTemperatureApplied(isChecked);
			} else {
				corrInfo.setMoistureApplied(isChecked);
			}
		}
		
		
	} // end of class
	
	
} // end of class
