/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.evaluation.dialog;

import com.snu.tms.qa.model.FactoryInfo;
import com.snu.tms.qa.model.InventoryDataManager;
import com.snu.tms.qa.util.DateUtil;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * managing clone data file
 * 
 */
public class DialogCloneDataFileManager extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  CloneFileModel[]  cloneDataFileModels = null;
	private  JTable            cloneDataTable = null;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogCloneDataFileManager(JFrame parent) {
		
		// set dialog
		super(parent, "Clone data file manager", ModalityType.MODELESS);

		// set icon
		try {
			setIconImage(ImageIO.read(new File("image/smart.png")));
		} catch (IOException exc) {}
		
		// get file names
		updateCloneDataFileModel();
		
		// create controlPane
		JPanel controlPanel = createControlPanel();
		
		// create Table base Panel
		JScrollPane tablePanel = createTablePanel();
		
		// add basePanel
		JPanel basePanel = new JPanel(new BorderLayout());
		basePanel.add(controlPanel, BorderLayout.NORTH);
		basePanel.add(tablePanel, BorderLayout.CENTER);
		this.add(basePanel);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create control panel
	 */
	private JPanel createControlPanel() {
		
		// Set basePanel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(800, 40));
		
		// create cancel button
		JButton deleteButton = new JButton("delete file");
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doCloneFileDelete();
				updateCloneDataFileModel();
				if (cloneDataTable != null) {
					cloneDataTable.updateUI();
				}
			}
		});
		deleteButton.setBounds(20, 10, 150, 20);
		basePanel.add(deleteButton);

		// create cancel button
		JButton cancelButton = new JButton("cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeWindow();
			}
		});
		cancelButton.setBounds(180, 10, 100, 20);
		basePanel.add(cancelButton);
		
		return basePanel;
		
	} // end of method
	
	
	/**
	 * create table panel
	 */
	private JScrollPane createTablePanel() {
		
		// Set JTable
		TableModel tableModel = new TableModel();
		this.cloneDataTable = new JTable(tableModel);

		this.cloneDataTable.setRowHeight(25);
		this.cloneDataTable.getColumnModel().getColumn(0).setPreferredWidth(50);
		this.cloneDataTable.getColumnModel().getColumn(1).setPreferredWidth(80);
		this.cloneDataTable.getColumnModel().getColumn(2).setPreferredWidth(170);
		this.cloneDataTable.getColumnModel().getColumn(3).setPreferredWidth(70);
		this.cloneDataTable.getColumnModel().getColumn(4).setPreferredWidth(80);
		this.cloneDataTable.getColumnModel().getColumn(5).setPreferredWidth(100);
		this.cloneDataTable.getColumnModel().getColumn(6).setPreferredWidth(100);
		this.cloneDataTable.getColumnModel().getColumn(7).setPreferredWidth(150);
		this.cloneDataTable.getColumnModel().getColumn(8).setPreferredWidth(100);
		
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
		leftRenderer.setHorizontalAlignment( JLabel.LEFT );
		
		this.cloneDataTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.cloneDataTable.getColumnModel().getColumn(2).setCellRenderer( leftRenderer );
		this.cloneDataTable.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
		this.cloneDataTable.getColumnModel().getColumn(4).setCellRenderer( centerRenderer );
		this.cloneDataTable.getColumnModel().getColumn(5).setCellRenderer( centerRenderer );
		this.cloneDataTable.getColumnModel().getColumn(6).setCellRenderer( centerRenderer );
		this.cloneDataTable.getColumnModel().getColumn(7).setCellRenderer( centerRenderer );
		this.cloneDataTable.getColumnModel().getColumn(8).setCellRenderer( rightRenderer );
		
		// Add basePanel
		JScrollPane tablePanel = new JScrollPane(this.cloneDataTable);
		tablePanel.setPreferredSize(new Dimension(800, 400));
		
		return tablePanel;
		
	} // end of method
	
	/**
	 * Close this window
	 */
	public void closeWindow() {
		this.dispose();
	} // end of method
	
	/**
	 * delete selected clone data files
	 */
	public void doCloneFileDelete() {
		
		// check data
		if (this.cloneDataFileModels == null) {
			return;
		}
		
		// file delete
		for (CloneFileModel fileModel : this.cloneDataFileModels) {
			
			if (fileModel.target != null && fileModel.isSelected) {
				try { fileModel.target.delete(); } catch (Exception e) {e.printStackTrace();}
			}
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * update clone data model
	 */
	public void updateCloneDataFileModel() {
		
		// init clone data file models
		Vector<CloneFileModel>  cloneDataFileModels = new Vector<CloneFileModel>();
		InventoryDataManager inventoryDataManager = InventoryDataManager.getInstance();
		
		// get file names from "./clone_data"
		File cloneFolder = new File("./clone_data");
		if (cloneFolder == null || !cloneFolder.exists() || !cloneFolder.isDirectory()) {
			return;
		}
		File[] cloneDataFiles = cloneFolder.listFiles();
		for (File cloneDataFile : cloneDataFiles) {
			
			// get file name
			String fileName = cloneDataFile.getName().toLowerCase();
			if (!fileName.startsWith("tms") || !fileName.endsWith(".txt")) {
				continue;
			}
			
			// parse file name
			String subFileName = "";
			int dataFileType = 0;
			if (fileName.contains("30min")) {
				subFileName = fileName.substring(15, fileName.length()-4);
				dataFileType = 1;
			} else {
				subFileName = fileName.substring(9, fileName.length()-4);
				dataFileType = 0;
			}
			String[] fileItems = subFileName.split("_");
			if (fileItems.length < 4) {
				continue;
			}
			String factCode = fileItems[0];
			FactoryInfo factoryInfo = inventoryDataManager.getFactoryInfo(factCode);
			if (factoryInfo == null) {
				continue;
			}
			String stackCode = fileItems[1];
			String sTime = fileItems[2];
			String eTime = fileItems[3];
			long   modifiedTime = cloneDataFile.lastModified();
			long   fileSize = cloneDataFile.length();
			
			cloneDataFileModels.add(new CloneFileModel(
					cloneDataFile,
					false,
					factCode,
					factoryInfo.getFullName(),
					Integer.parseInt(stackCode),
					dataFileType,
					sTime,
					eTime,
					DateUtil.getDateStr(modifiedTime),
					String.format("%.3f Mbytes", ((double)fileSize)/(1048576D))));
			
		} // end of for-loop
		
		// convert array and sorting
		CloneFileModel[] cloneFiles = cloneDataFileModels.toArray(new CloneFileModel[cloneDataFileModels.size()]);
		Arrays.sort(cloneFiles);
		
		// set
		this.cloneDataFileModels = cloneFiles;
		
	} // end of method

	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class TableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		public TableModel() {} // end of constructor
		
		public String getColumnName(int col) {
			
			if (col == 0) {
				return "";
			} else if (col == 1) {
				return "fact_code";
			} else if (col == 2) {
				return "fact_name";
			} else if (col == 3) {
				return "stack";
			} else if (col == 4) {
				return "type";
			} else if (col == 5) {
				return "start_time";
			} else if (col == 6) {
				return "end_time";
			} else if (col == 7) {
				return "create_time";
			} else {
				return "file_size";
			}
		}
		
		public int getRowCount() { 
			if (cloneDataFileModels == null) {
				return 0;
			}
			return cloneDataFileModels.length;
		}
		
		public int getColumnCount() { 
			return 9;
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return new Boolean(cloneDataFileModels[row].isSelected);
			} else if (col == 1) {
				return cloneDataFileModels[row].factCode;
			} else if (col == 2) {
				return cloneDataFileModels[row].factName;
			} else if (col == 3) {
				return cloneDataFileModels[row].stackCode;
			} else if (col == 4) {
				return (cloneDataFileModels[row].dataType == 0 ? "5-min" : "30-min");
			} else if (col == 5) {
				return cloneDataFileModels[row].sTime;
			} else if (col == 6) {
				return cloneDataFileModels[row].eTime;
			} else if (col == 7) {
				return cloneDataFileModels[row].saveTime;
			} else if (col == 8) {
				return cloneDataFileModels[row].fileSize;
			} else {
				return "";
			}
		
		} // end of method
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return Boolean.class;
			} else {
				return String.class;
			}
			
		}

		public boolean isCellEditable(int row, int col) {
			
			if (col == 0) {
				return true;
			} else {
				return false;
			}
		}
		
		public void setValueAt(Object value, int row, int col) {
			
			if (col == 0) {
				boolean isChecked = ((Boolean) value).booleanValue();
				cloneDataFileModels[row].isSelected = isChecked;
			} 
		}
		
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class CloneFileModel implements Comparable<CloneFileModel> {
		
		public  File    target     = null; 
		public  boolean isSelected = false;
		public  String  factCode   = "";
		public  String  factName   = "";
		public  int     stackCode  = 0;
		public  int     dataType   = 0;   // dataType[0] : 5-min data, dataType[1] : 30-min data
		public  String  sTime      = "";
		public  String  eTime      = "";
		public  String  saveTime   = "";
		public  String  fileSize   = "";
		
		public CloneFileModel(
				File     target,
				boolean  isSelected,
				String  factCode,
				String  factName,
				int     stackCode,
				int     dataType,
				String  sTime,
				String  eTime,
				String  saveTime,
				String  fileSize) {
			this.target     = target;
			this.isSelected = isSelected;
			this.factCode   = factCode;
			this.factName   = factName;
			this.stackCode  = stackCode;
			this.dataType   = dataType;
			this.sTime      = sTime;
			this.eTime      = eTime;
			this.saveTime   = saveTime;
			this.fileSize   = fileSize;
			
		} // end of constructor
		
		/**
		 * compare
		 */
		public int compareTo(CloneFileModel compare) {
			
			// compare factCode
			int compareValue = 0;
			compareValue = this.factCode.compareTo(compare.factCode);
			if (compareValue != 0) {
				return compareValue;
			}
			
			// compare stackCode
			if (this.stackCode != compare.stackCode) {
				return (this.stackCode - compare.stackCode);
			}
			
			// compare startTme and endTime
			compareValue = this.sTime.compareTo(compare.sTime);
			if (compareValue != 0) {
				return compareValue;
			}
			compareValue = this.eTime.compareTo(compare.eTime);
			if (compareValue != 0) {
				return compareValue;
			}
			
			// return
			return 0;
			
		} // end of method
		
	} // end of inner class
	
} // end of class
