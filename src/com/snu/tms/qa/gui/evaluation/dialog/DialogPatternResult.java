/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.evaluation.dialog;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.event.ChartProgressEvent;
import org.jfree.chart.event.ChartProgressListener;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import com.snu.tms.qa.model.EvalDatasetManager;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.PatternDataHistogram;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.util.NumberUtil;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Hashtable;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * Show Graph
 * 
 */
public class DialogPatternResult extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  EvalDatasetManager  datasetManager = EvalDatasetManager.getInstance();
	
	private  TmsDataRecord[]     tmsDataRecords = null;

	private  JTabbedPane    patternBasePanel = null;
	
	private  int            selectedItemIndex = 0;
	private  int            selectedItem = ItemCodeDefine.UNDEFINED;
	
	private  JPanel         chartBasePanel;
	
	private  Hashtable<Long, Integer>    timeHash;
	private  Hashtable<Long, Integer>    patternHash;
	
	private  JTable         rawDataTable;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogPatternResult(
			final JFrame   parent, 
			final String   stackInfoStr) {
		
		// set dialog
		super(parent, "Ruleset Evalution (Pattern Identification) : " + stackInfoStr, ModalityType.MODELESS);

		// set icon
		try {
			setIconImage(ImageIO.read(new File("image/smart.png")));
		} catch (IOException exc) {}
		
		// create timeHash
		createTimeHashtable();
		
		// Pattern analysis panel
		this.patternBasePanel = new JTabbedPane();
		setPatternBasePanel();
		
		// set ui
		this.setPreferredSize(new Dimension(1100, 800));
		this.setLayout(new BorderLayout());
		this.add(this.patternBasePanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * set Pattern BasePanel
	 */
	private void setPatternBasePanel() {
		
		// Set Pattern analysis result
		this.patternBasePanel.addTab("Item value histogram", createPatternAnalysisResultPanel());
		
		// set pattern data timeseries graph
		this.patternBasePanel.addTab("Timeseries graph", createPatternTimeseriesPanel());
		
	} // end of method
	
	
	/**
	 * Set Pattern analysis result
	 */
	private JPanel createPatternAnalysisResultPanel() {
		
		// make basePanel
		JPanel basePanel = new JPanel(new BorderLayout());
		
		// create Pattern Analysis result table
		JScrollPane tablePanel = createPatternAnalysisResultTablePanel();
		int patternCount = this.datasetManager.getPatternCount();
		tablePanel.setPreferredSize(new Dimension(600, 5+20*(patternCount+1)));
		basePanel.add(tablePanel, BorderLayout.NORTH);
		
		// create data scattor plot
		JPanel chartBasePanel = new JPanel(new GridLayout(0, 1));
		DetectionPatternDataset[] detectionPatternDatasets = this.datasetManager.getAllPatternData();
		for (DetectionPatternDataset detectionPatternData : detectionPatternDatasets) {
			JPanel chartPanel = createHistogramPanel(detectionPatternData);
			chartBasePanel.add(chartPanel);
		}
		JScrollPane lowerBasePanel = new JScrollPane(chartBasePanel);
		lowerBasePanel.setPreferredSize(new Dimension(1100, 600));
		
		// return
		basePanel.add(lowerBasePanel, BorderLayout.CENTER);
		return basePanel;
		
	} // end of method
	
	
	/**
	 * createPatternTimeseriesPanel
	 */
	private JPanel createPatternTimeseriesPanel() {
		
		// make basePanel
		JPanel basePanel = new JPanel(new BorderLayout());
		
		// make control panel
		JPanel upperBasePanel = createControlPanel();
		upperBasePanel.setPreferredSize(new Dimension(1100, 35));
		
		// make lower base panel
		JSplitPane lowerBasePanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		lowerBasePanel.setDividerLocation(300);
		lowerBasePanel.setDividerSize(5);
		lowerBasePanel.setPreferredSize(new Dimension(1100, 550));
		
		this.chartBasePanel = new JPanel(new BorderLayout());
		this.chartBasePanel.add(createTimeseriesPanel(), BorderLayout.CENTER);
		lowerBasePanel.add(this.chartBasePanel);

		JScrollPane tablePanel = createRawDataTablePanel();
		lowerBasePanel.add(tablePanel);
		
		// Base Panel
		basePanel.add(upperBasePanel, BorderLayout.NORTH);
		basePanel.add(lowerBasePanel, BorderLayout.CENTER);
		
		// return 
		return basePanel;
		
	} // end of method
	
	/**
	 * create Control panel
	 */
	private JPanel createControlPanel() {
		
		// create base panel
		JPanel basePanel = new JPanel(null);
		
		// create JCombo
		int[] items = this.datasetManager.getItems();
		String[] itemNames = new String[items.length];
		for (int i=0 ; i<items.length ; i++) {
			itemNames[i] = ItemCodeDefine.getItemName(items[i]);
		}
		final JComboBox<String> itemSelectionCombo = new JComboBox<String>(itemNames);
		itemSelectionCombo.setBounds(20, 5, 100, 20);
		basePanel.add(itemSelectionCombo);
		
		// set selected item
		this.selectedItemIndex = 0;
		this.selectedItem = items[0];
		itemSelectionCombo.setSelectedIndex(0);
		
		itemSelectionCombo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				
				// get item seleteion
				int selectedIndex = itemSelectionCombo.getSelectedIndex();
				
				// update timeseries graph
				updateChart(selectedIndex);
				
			}
		});
		
		// return
		return basePanel;
		
	} // end of method
	
	
	/**
	 * create timeseries graph
	 */
	private JPanel createTimeseriesPanel() {
		
		// create XYDataSet
		XYDataset chartDataSet = createTimeseriesDataSet();
		
		// create chart
		final JFreeChart chart = ChartFactory.createTimeSeriesChart(
				"",           // title
				"",           // x-axis label
				"",           // y-axis label
				chartDataSet, // data
				true,        // create legend?
				true,         // generate tooltips?
				false         // generate URLs?
		);

		// set Font
		chart.getTitle().setFont(new Font("����", Font.BOLD, 14));
		chart.getLegend().setItemFont(new Font("����", Font.BOLD, 12));

		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setNoDataMessage("NO DATA");
		plot.setDomainPannable(true);
		plot.setRangePannable(false);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);
		
		// set chart shape properties
		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		plot.setRangeCrosshairVisible(true);
		plot.setRangeCrosshairLockedOnData(true);
		plot.setDomainZeroBaselineVisible(true);
		plot.setRangeZeroBaselineVisible(true);
		XYDotRenderer renderer = new XYDotRenderer();
		renderer.setDotWidth(3);
		renderer.setDotHeight(3);
		plot.setRenderer(renderer);
		
		// set x-axis properties
		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("yy/MM/dd"));
		
		// Add chart listener
		chart.addProgressListener( new ChartProgressListener() {
			public void chartProgress(ChartProgressEvent event) {
				
				if (event.getType() != ChartProgressEvent.DRAWING_FINISHED) {
					return;
				}
				
				// get mouse click domain axis point
				XYPlot plot = (XYPlot) chart.getPlot();
				double xx = plot.getDomainCrosshairValue();
				
				// change table selection
				changeTableRowSelection((long)xx);
				
			} // end of method
		});

		// return
		return new ChartPanel(chart);	
		
	} // end of method
	
	
	/**
	 * create table panel
	 */
	private JScrollPane createRawDataTablePanel() {

		// Create Table
		this.rawDataTable = new JTable(new RawDataTableModel());
		this.rawDataTable.setRowHeight(20);
		this.rawDataTable.getColumnModel().getColumn(0).setPreferredWidth(170);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		this.rawDataTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.rawDataTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.rawDataTable.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		for (int i=0 ; i<this.datasetManager.getItems().length ; i++) {
			this.rawDataTable.getColumnModel().getColumn(i+3).setCellRenderer( rightRenderer );
		}
				
		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.rawDataTable);
		basePane.setPreferredSize(new Dimension(1100, 2500));
		return basePane;
		
	} // end of method
	
	
	/**
	 * Create graph data set
	 */
	private XYDataset createTimeseriesDataSet() {
		
		// create TimeSeries
		DetectionPatternDataset[] detectionPatternDatasets = this.datasetManager.getAllPatternData();
		TimeSeries[] timeseries = new TimeSeries[detectionPatternDatasets.length];
		for (int i=0 ; i<timeseries.length ; i++) {
			timeseries[i] = new TimeSeries(String.format(" %d [ %s ]", 
					detectionPatternDatasets[i].getPatternId(),
					detectionPatternDatasets[i].getPatternName()));
		}
		
		// prepare parameters
		Minute time = null;
		double value;
		for (int i=0 ; i<detectionPatternDatasets.length ; i++) {
			
			TmsDataRecord[] records = this.datasetManager.getPatternTmsDataRecords(detectionPatternDatasets[i].getPatternId());
			for (int j=0 ; j<records.length ; j++) {
				
				value = records[j].getDataCell(this.selectedItem).getData_value();
				time  = new Minute(new java.util.Date(records[j].getTime()));
				timeseries[i].add(time, value);
			}
		
		} // end of for-loop
		
		// Create TimeSeriesCollection
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		for (int i=0 ; i<timeseries.length ; i++) {
			dataset.addSeries(timeseries[i]);
		}
		
		// return
		return dataset;
		
	} // end of method
	
	
	/**
	 * Create time hashtable
	 */
	private void createTimeHashtable() {
		
		// set TmsDataRecords
		this.tmsDataRecords = this.datasetManager.getTmsDataset().getTmsDataRecords();
		Arrays.sort(this.tmsDataRecords);
		
		// create time hash
		this.timeHash = new Hashtable<Long, Integer>();
		this.patternHash = new Hashtable<Long, Integer>();
		
		// set timeHash
		for (int i=0 ; i<tmsDataRecords.length ; i++) {
			this.timeHash.put(tmsDataRecords[i].getTime(), i);
		}
		
		// set patternHash
		DetectionPatternDataset[] datasets = this.datasetManager.getAllPatternData();
		for (DetectionPatternDataset dataset : datasets) {
			
			TmsDataRecord[] records = this.datasetManager.getPatternTmsDataRecords(dataset.getPatternId());
			if (records != null) {
				for (TmsDataRecord record : records) {
					this.patternHash.put(record.getTime(), dataset.getPatternId());
				}
			}
		}
		
	} // end of method
	
	/**
	 * Table Selection
	 */
	private void changeTableRowSelection(long selectedTime) {
		
		// get table row index from time hash
		Integer row = this.timeHash.get(selectedTime);
		if (row == null) {
			return;
		}
		
		// table selection
		this.rawDataTable.setRowSelectionInterval(row, row);
		JViewport viewport = (JViewport)this.rawDataTable.getParent();
		Rectangle rect = this.rawDataTable.getCellRect(row, 0, true);
		Rectangle viewRect = viewport.getViewRect();
		
		int y = viewRect.y;
		if (rect.y >= viewRect.y && rect.y <= (viewRect.y + viewRect.height - rect.height)) {
			
		} else if (rect.y < viewRect.y) {
			y = rect.y;
		} else if (rect.y > (viewRect.y + viewRect.height - rect.height)) {
			 y = rect.y - viewRect.height + rect.height;
		}
		viewport.setViewPosition(new Point(0, y));
		
	} // end of method
	

	/**
	 * Set data table
	 */
	private JPanel createHistogramPanel(DetectionPatternDataset detectionPatternData) {
		
		// Get items
		int[] items = detectionPatternData.getItems();
		
		// create chart
		JPanel chartBasePanel = new JPanel(new GridLayout(0, 3));
		chartBasePanel.setPreferredSize(new Dimension(1000, 150*(items.length / 3)));
		
		JPanel chartPanel;
		for (int i=0 ; i<items.length ; i++) {
			chartPanel = createHistogram(detectionPatternData.getPatternDataHistogram(items[i]));
			chartPanel.setPreferredSize(new Dimension(200, 150));
			chartBasePanel.add(chartPanel);
		}
		
		// set border
		String titile = String.format("Pattern_%d [ %s ]", 
				detectionPatternData.getPatternId(), 
				detectionPatternData.getPatternName());
		chartBasePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder(titile),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								chartBasePanel.getBorder()));

		
		// return
		return chartBasePanel;
		
	} // end of method
	
	
	/**
	 * create Histogram
	 */
	private JPanel createHistogram(PatternDataHistogram patternDataHistogram) {
		
		// get parameters
		double[][] densities = patternDataHistogram.getDensities();
		int        item      = patternDataHistogram.getItem();
		String     itemStr   = ItemCodeDefine.getItemName(item);
		
		// prepare HistogramDataset
		HistogramDataset dataset = new HistogramDataset();
		int dataCount = 2000;
		double densitySum = 0.0D;
		for (int i=0 ; i<densities.length ; i++) {
			densitySum += densities[i][1];
		}

		double densityMultiplier = 0.0D;
		if (densitySum != 0.0D) {
			densityMultiplier = ((double)dataCount) / densitySum;
		}
		
		double[] values = new double[dataCount];
		int count = 0;
		int N     = 0;
		for (int i=0 ; i<densities.length ; i++) {
			N = (int) (densities[i][1] * densityMultiplier);
			for (int j=0 ; j<N ; j++) {
				if (count >= dataCount) {
					break;
				}
				values[count++] = densities[i][0];
			}
		}

		values = Arrays.copyOf(values, count);
		dataset.addSeries(itemStr, values, patternDataHistogram.getBinNo()-1);
		
		// create histogram chart
		JFreeChart chart = ChartFactory.createHistogram(
				"",
				null,
				null,
				dataset,
				PlotOrientation.VERTICAL,
				true,
				true,
				false);
		
		// set Font
		//chart.getTitle().setFont(new Font("����", Font.BOLD, 14));
		//chart.getLegend().setItemFont(new Font("����", Font.BOLD, 12));

		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setDomainPannable(true);
		plot.setRangePannable(true);
		plot.setForegroundAlpha(0.85f);
		//NumberAxis xAxis = (NumberAxis) plot.getDomainAxis();
		//xAxis.setRange(histogramData.getMinRange(), histogramData.getMaxRange());
		NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
		yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
		renderer.setDrawBarOutline(false);
		renderer.setBarPainter(new StandardXYBarPainter());
		renderer.setShadowVisible(false);
		
		// return
		return new ChartPanel(chart);
		
	} // end of method
	
	

	/**
	 * Set Pattern Analysis Result Table Panel
	 */
	private JScrollPane createPatternAnalysisResultTablePanel() {
		
		JTable patternResultTable = new JTable(new PatternAnalysisResultTableModel());
		patternResultTable.setRowHeight(20);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );

		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		patternResultTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		patternResultTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		patternResultTable.getColumnModel().getColumn(2).setCellRenderer( rightRenderer );
		patternResultTable.getColumnModel().getColumn(3).setCellRenderer( rightRenderer );
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(patternResultTable);
		return basePane;
		
	} // end of method
	
	/**
	 * update chart panel
	 */
	private void updateChart(int selectedItemIndex) {
		
		// set parameters
		int[] items = this.datasetManager.getItems();
		this.selectedItemIndex = selectedItemIndex;
		this.selectedItem = items[this.selectedItemIndex];
		
		// chage chart panel
		this.chartBasePanel.removeAll();
		this.chartBasePanel.add(createTimeseriesPanel(), BorderLayout.CENTER);
		this.chartBasePanel.updateUI();
		
	} // end of method
	
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class PatternAnalysisResultTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		private  String[] columnNames = {"Pattern Index", "Pattern Name", "Data Count", "% Data Count"};
		private  int      patternCount = 0;
		private  DetectionPatternDataset[] detectionPatternDatasets = null;
		
		
		public PatternAnalysisResultTableModel() {
			
			this.patternCount = datasetManager.getPatternCount();
			this.detectionPatternDatasets = datasetManager.getAllPatternData();
			
		} // end of constructor
			
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() {
			return patternCount;
		} // end of method
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return "Pattern[" + detectionPatternDatasets[row].getPatternId() + "]";
			} else if (col == 1) {
				return detectionPatternDatasets[row].getPatternName();
			} else if (col == 2) {
				return NumberUtil.getNumberStr(datasetManager.getPatternTmsDataRecordCount(detectionPatternDatasets[row].getPatternId()));
			} else if (col == 3) {
				int allDataCount = tmsDataRecords.length;
				int targetDataCount = datasetManager.getPatternTmsDataRecordCount(detectionPatternDatasets[row].getPatternId());
				if (allDataCount == 0) {
					return "0.00 %";
				} else {
					return String.format("%.2f", ((double)targetDataCount / (double)allDataCount) * 100.0D) + " %";
				}
			} else {
				return "";
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class RawDataTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		private String[] columnNames;
		private int[]    items;
		private  DetectionPatternDataset[] detectionPatternDatasets = null;
		
		public RawDataTableModel() {
			
			// set dataset
			this.detectionPatternDatasets = datasetManager.getAllPatternData();
			this.items   = datasetManager.getItems();
			
			// set column names
			this.columnNames = new String[items.length + 3];
			columnNames[0] = "Time";
			columnNames[1] = "Id";
			columnNames[2] = "Name";
			for (int i=0 ; i<items.length ; i++) {
				this.columnNames[i+3] = ItemCodeDefine.getItemName(items[i]);
			}
		}
		
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			return tmsDataRecords.length;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return tmsDataRecords[row].getTimestr();
			} else if (col == 1) {
				return patternHash.get(tmsDataRecords[row].getTime());
			} else if (col == 2) {
				int patternId = patternHash.get(tmsDataRecords[row].getTime());
				String patternName = datasetManager.getPatternData(patternId).getPatternName();
				return patternName;
			} else {
				return NumberUtil.getNumberStr(tmsDataRecords[row].getDataCell(this.items[col-3]).getData_value());
			}
		
		} // end of method
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 1) {
				return Integer.class;
			} else {
				return String.class;
			}
			
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	
} // end of class
