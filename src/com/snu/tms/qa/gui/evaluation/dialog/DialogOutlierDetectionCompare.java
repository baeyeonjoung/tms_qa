/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.evaluation.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.event.ChartProgressEvent;
import org.jfree.chart.event.ChartProgressListener;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import com.snu.tms.qa.soap.QueryTmsRecord;
import com.snu.tms.qa.fileio.OutlierDetectionResultCompareWriterAsCsvFormat;
import com.snu.tms.qa.fileio.QueryTms30minRecordFromFile;
import com.snu.tms.qa.fileio.Tms30minDataWriter;
import com.snu.tms.qa.model.EvalDatasetManager;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.Tms30minDataCell;
import com.snu.tms.qa.model.Tms30minDataRecord;
import com.snu.tms.qa.model.Tms30minDataset;
import com.snu.tms.qa.model.TmsDataCell;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.ruleset.OdrCell;
import com.snu.tms.qa.model.ruleset.OdrRecord;
import com.snu.tms.qa.model.ruleset.RulesetModel;
import com.snu.tms.qa.util.DateUtil;


/**
 * Show outlier detection reuslts
 * 
 */
public class DialogOutlierDetectionCompare extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	private static final String[] OUTLIER_CODE = 
		{"", "WARNING", "MINOR", "MAJOR", "CRITICAL"};
	private static final String[] OUTLIER_CODE_STR = 
		{"정상", "WARNING", "MINOR", "MAJOR", "CRITICAL"};
	private static final String[] HALF_OVER_CODE = 
		{"0", "1", "2"};
	private static final String[] HALF_OVER_CODE_STR = 
		{"0 (정상)", "1 (초과)", "2 (비율초과)"};
	private static final String[] HALF_STAT_CODE = 
		{"0", "1", "2", "4", "8"};
	private static final String[] HALF_STAT_CODE_STR = 
		{"0 (정상)", "1 (교정중)", "2 (동작불량)", "4 (전원단절)", "8 (보정중)"};
	private static final String[] HALF_DLST_CODE = 
		{"0", "1", "4"};
	private static final String[] HALF_DLST_CODE_STR = 
		{"0 (정상)", "1 (비정상)", "4 (전원단절)"};
	private static final String[] HALF_EXCH_CODE = 
		{"0", 
		"10", "11", "12", "13", "14",
		"15", "16", "18", "19", "20", 
		"21", "22", "24", "30", "40", 
		"50", "60", "61", "62", "70", 
		"90", "91", "R1", "R2", "R3"};
	private static final String[] HALF_EXCH_CODE_STR = 
		{"정상", 
		"10", "11", "12", "13", "14",
		"15", "16", "18", "19", "20", 
		"21", "22", "24", "30", "40", 
		"50", "60", "61", "62", "70", 
		"90", "91", "R1", "R2", "R3"};
	private static final String[] HALF_EXCH_DESC = {
		"정상",
		"[10] 미수신 자료 : 정상 마감된 자료 중 최근 30분 평균자료)",
		"[11] 조치명령 받은 측정기기 개선(개선계획서 미제출) : 정상 마감된 전월의 최근 3개월간 30분 평균자료 중 최대값",
		"[12] 조치명령 받은 측정기기 개선(개선계획서 제출) : 정상 마감된 전월의 최근 3개월간 30분 평균자료",
		"[13] 조치명령 받지 않은 측정기기 개선 (자체 개선계획) : 정상 마감된 전월의 최근 3개월간 30분 평균자료",
		"[14] 정도검사 기간 및 불합격 또는 미수검 : 정상 마감된 전월의 최근 1개월간 30분 평균자료",
		"[15] 비정상 측정자료 : 정상 마감된 자료 중 최근 30분 평균자료",
		"[16] 장비점검(통합시험 등) : 정상 마감된 자료 중 최근 30분 평균자료",
		"[18] 관할 행정기관 인정(24시간 미만) : 정상 자료 중 최근 30분 평균자료",
		"[19] 관할 행정기관 인정(24시간 이상) : 정상 마감된 전월의 최근 1개월간 30분 평균자료",
		"[20] 배출시설 기준초과 인정시간 : 해당 30분 평균자료를 그대로 적용",
		"[21] 배출시설 가동중지 : 해당기간의 자료는 0으로 처리",
		"[22] 대체자료 : 사용자가 직접 입력한 값",
		"[24] 행정기관 인정 미전송(최초 설치 후 가동개시 전) : 해당기간 자료는 0으로 처리",
		"[30] 배출및방지시설(개선계획서제출) : 해당 30분 평균자료를 그대로 적용",
		"[40] 배출및방지시설(행정처분후개선) : 해당 30분 평균자료를 그대로 적용",
		"[50] 상대정확도 부적합 : 정상 마감된 전월의 최근 1개월간 30분 평균자료",
		"[60] 공기비 3배 이상인 기간 : 해당 30분 평균자료를 그대로 적용",
		"[61] 산소항목 비정상 : 해당 30분 평균자료를 그대로 사용",
		"[62] 공기비 3배 기간 중 상태표시 발생 : 공기비 3배 이상인 자료 중 최근 30분 평균자료",
		"[70] 상태표시 발생 : 정상 마감된 자료 중 최근 30분 평균자료",
		"[90] 유량계 정도검사 : 정상 가동된 최근 1일간 평균자료",
		"[91] 소각시설 폐기물 투입이전 CO초과 : 해당 30분 평균자료를 그대로 사용",
		"[R1] 오염항목 상태표시, 산소정상 ,온도정상 : 최근 오염항목,산소,온도가 모두 정상측정된 30분 자료",
		"[R2] 오염항목 정상, 산소 상태표시 ,온도 상태표시 : 최근 오염항목,산소,온도가 모두 정상측정된 30분 자료",
		"[R3] 오염항목 상태표시, 산소 상태표시 ,온도 상태표시 : 최근 오염항목,산소,온도가 모두 정상측정된 30분 자료"
	};
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  EvalDatasetManager         datasetManager;
	private  Tms30minDataRecord[]       dataRecords30min;
	private  Tms30minDataRecord[]       dataRecordsFiltered30min;
	private  OdrRecord[]                odrRecords;
	
	private  Hashtable<Long, OdrRecord>          odrRecordHash;
	private  Hashtable<Long, TmsDataRecord>      tmsRecordHash;
	private  Hashtable<Long, Tms30minDataRecord> tms30minRecordHash;
	
	private  DataRecordFilter        filter = new DataRecordFilter();
	
	private  JCheckBox[]   halfOverFilter = new JCheckBox[HALF_OVER_CODE_STR.length];
	private  JCheckBox[]   halfStatFilter = new JCheckBox[HALF_STAT_CODE_STR.length];
	private  JCheckBox[]   halfDlstFilter = new JCheckBox[HALF_DLST_CODE_STR.length];
	private  JCheckBox[]   halfExchFilter = new JCheckBox[HALF_EXCH_CODE_STR.length];
	
	private  JTable  dataTable30min;
	private  JTable  dataTable5min;
	private  JTable  dataTable5minOutlier;

	private  JPanel  timeseriesGraphBasePanel;
	private  JPanel  lowerBase;
	
	private  long    selectedTime = 0L;
	
	private  int     selectedItem = 0;
	
	private  JButton exportCsvFormatButton = null;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogOutlierDetectionCompare(
			JDialog parent,
			OdrRecord[] odrRecords) {
		
		// set dialog
		super(parent, "", ModalityType.MODELESS);
		this.setTitle("Outlier detection result (compare with 30-min data)");
		
		// set parameters
		this.datasetManager = EvalDatasetManager.getInstance();
		this.dataRecords30min = new Tms30minDataRecord[0];
		this.dataRecordsFiltered30min = new Tms30minDataRecord[0];
		this.odrRecords = odrRecords;
		
		// set OdrRecord Hashtable
		this.odrRecordHash = new Hashtable<Long, OdrRecord>();
		for (OdrRecord odrRecord : this.odrRecords) {
			this.odrRecordHash.put(odrRecord.time, odrRecord);
		}
		
		// set Tms30minRecord hash
		this.tms30minRecordHash = new Hashtable<Long, Tms30minDataRecord>();
		this.tmsRecordHash = new Hashtable<Long, TmsDataRecord>();
		
		// create components
		JPanel      controlPanel     = createControlPanel();
		JPanel      filterPanel      = createFilterPanel();
		this.timeseriesGraphBasePanel = new JPanel(new BorderLayout());
		
		filterPanel.setPreferredSize(new Dimension(1000, 180));
		this.timeseriesGraphBasePanel.setPreferredSize(new Dimension(1000, 280));
		
		// set layout
		JPanel   upperBase  = new JPanel(new BorderLayout());
		JPanel   middleBase = new JPanel(new BorderLayout());
		this.lowerBase  = new JPanel(new BorderLayout());
		
		upperBase.setPreferredSize(new Dimension(1000, 50));
		middleBase.setPreferredSize(new Dimension(1000, 480));
		this.lowerBase.setPreferredSize(new Dimension(1000, 300));
		
		// set upper base panel
		upperBase.add(controlPanel, BorderLayout.CENTER);
		
		// set middle base panel
		middleBase.add(filterPanel, BorderLayout.NORTH);
		middleBase.add(this.timeseriesGraphBasePanel, BorderLayout.CENTER);
		
		// set all layout
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(1000, 800));
		this.add(upperBase,  BorderLayout.NORTH);
		this.add(middleBase, BorderLayout.CENTER);
		this.add(this.lowerBase,  BorderLayout.SOUTH);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Set DataRecords
	 */
	private void init() {
		
		// get all records
		this.dataRecords30min = this.datasetManager.get30minTmsDataset().getTmsDataRecords();
		
		// get all TmsDataRecord
		TmsDataRecord[] tmsDataRecords = this.datasetManager.getTmsDataset().getTmsDataRecords();
		for (TmsDataRecord record : tmsDataRecords) {
			this.tmsRecordHash.put(record.getTime(), record);
		}
		
		// set timeHash
		for (Tms30minDataRecord record : this.dataRecords30min) {
			this.tms30minRecordHash.put(record.getTime(), record);
		}
		
		// set outlier level
		long timeInterval = 5L*60L*1000L;
		long baseTime = 0L;
		int score;
		for (Tms30minDataRecord record : this.dataRecords30min) {
			
			baseTime = record.getTime();
			OdrRecord odrRecord;
			int outlierScore = 0;
			for (int i=0 ; i<6 ; i++) {
				odrRecord = this.odrRecordHash.get(baseTime + timeInterval * (long)i);
				if (odrRecord != null) {
					score = odrRecord.getOutlierLevel();
					if (outlierScore < score) {
						outlierScore = score;
					}
				}
			}
			record.setOutlierLevel(outlierScore);
			
		}
		
		// GUI update
		initTablePanel();
		updateTimeseriesGraphPanel();
		
		// set "export CSV format button" enable
		this.exportCsvFormatButton.setEnabled(true);

	} // end of method
	
	/**
	 * create time series graph panel
	 */
	private void updateTimeseriesGraphPanel() {
		
		if (this.dataRecords30min.length == 0) {
			return;
		}
		
		// create XYDataSet
		XYDataset chartDataSet = createTimeseriesDataSet();
		
		// create chart
		final JFreeChart chart = ChartFactory.createTimeSeriesChart(
				"",           // title
				"",           // x-axis label
				ItemCodeDefine.getItemName(this.selectedItem),           // y-axis label
				chartDataSet, // data
				true,        // create legend?
				true,         // generate tooltips?
				false         // generate URLs?
		);

		// set Font
		//chart.getTitle().setFont(new Font("돋움", Font.BOLD, 14));
		//chart.getLegend().setItemFont(new Font("돋움", Font.BOLD, 12));

		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setNoDataMessage("NO DATA");
		plot.setDomainPannable(true);
		plot.setRangePannable(false);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);
		
		// set chart shape properties
		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		plot.setRangeCrosshairVisible(true);
		plot.setRangeCrosshairLockedOnData(true);
		plot.setDomainZeroBaselineVisible(true);
		plot.setRangeZeroBaselineVisible(true);
		
		XYDotRenderer renderer = new XYDotRenderer();
		renderer.setDotWidth(3);
		renderer.setDotHeight(3);
		plot.setRenderer(renderer);
		Color[] paintColors = {Color.GRAY, Color.BLACK, Color.YELLOW, Color.GREEN, Color.BLUE, Color.RED};
		for (int i=0 ; i<6 ; i++) {
			renderer.setSeriesPaint(i, paintColors[i]);
		}
		
		// set x-axis properties
		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("yy/MM/dd HH:mm"));
		
		// Add chart listener
		chart.addProgressListener( new ChartProgressListener() {
			public void chartProgress(ChartProgressEvent event) {
				
				if (event.getType() != ChartProgressEvent.DRAWING_FINISHED) {
					return;
				}
				
				// get mouse click domain axis point
				XYPlot plot = (XYPlot) chart.getPlot();
				double xx = plot.getDomainCrosshairValue();
				
				// change table selection
				changeTableSelection((long)xx);
				
			} // end of method
		});

		// return
		this.timeseriesGraphBasePanel.removeAll();
		this.timeseriesGraphBasePanel.add(new ChartPanel(chart), BorderLayout.CENTER);
		this.timeseriesGraphBasePanel.updateUI();
		
	} // end of method
	
	/**
	 * Create graph data set
	 */
	private XYDataset createTimeseriesDataSet() {
		
		// create TimeSeries
		String[] levelStr = 
			{"filtered", "set state code", "30min(X)-5min(X)", "30min(X)-5min(O)", "30min(O)-5min(X)", "30min(O)-5min(O)"};
		TimeSeries[] timeseries = new TimeSeries[levelStr.length];
		for (int i=0 ; i<timeseries.length ; i++) {
			timeseries[i] = new TimeSeries(levelStr[i]);
		}
		
		// Set data
		Minute time = null;
		double value;
		boolean filteredCheck, min5Check, min30Check, stateCodeCheck;
		for (Tms30minDataRecord record : this.dataRecords30min) {
			
			// init parameters
			filteredCheck = true;
			min5Check  = false;
			min30Check = false;
			stateCodeCheck = false;
			
			
			// check filtered
			filteredCheck = applyFilter4Records30min(record, this.selectedItem);
			
			// check 5min
			if (record.getOutlierLevel() > OdrRecord.OUTLIER_LEVEL_NON) {
				min5Check = true;
			}
			
			// check state code
			Tms30minDataCell cell = record.getDataCell(this.selectedItem);
			if (cell == null) {
				continue;
			}
			if (!cell.getHalf_stat().equals("0") || !cell.getHalf_dlst().equals("0")) {
				stateCodeCheck = true;
			} else {
				stateCodeCheck = false;
			}
			
			//check 30-min
			if (!cell.getStat_code().equals("0")) {
				min30Check = true;
			} else {
				min30Check = false;
			}
			
			// determin series type
			int seriesIndex = 0;
			if (!filteredCheck) {
				seriesIndex = 0;
			} else if (stateCodeCheck) {
				seriesIndex = 1;
			} else if (!min5Check && !min30Check) {
				seriesIndex = 2;
			} else if (min5Check && !min30Check) {
				seriesIndex = 3;
			} else if (!min5Check && min30Check) {
				seriesIndex = 4;
			} else {
				seriesIndex = 5;
			}
			
			value = cell.getHalf_valu();
			time  = new Minute(new java.util.Date(record.getTime()));
			timeseries[seriesIndex].add(time, value);
		
		} // end of for-loop
		
		// Create TimeSeriesCollection
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		for (int i=0 ; i<timeseries.length ; i++) {
			dataset.addSeries(timeseries[i]);
		}
		
		// return
		return dataset;
		
	} // end of method
	
	/**
	 * update Table Panel
	 */
	private void initTablePanel() {
		
		// clear all
		this.lowerBase.removeAll();
		
		// set lower base Tables
		JPanel  lowerLeftBase = new JPanel(new BorderLayout());
		JPanel  lowerRightBase = new JPanel(new BorderLayout());
		
		lowerLeftBase.add(create30minDataTable(), BorderLayout.CENTER);
		lowerLeftBase.setPreferredSize(new Dimension(450, 300));
		
		lowerRightBase = new JPanel(new BorderLayout());
		lowerRightBase.add(create5minDataTable(), BorderLayout.NORTH);
		lowerRightBase.add(create5minOutlierTablePanel(), BorderLayout.CENTER);
		lowerRightBase.setPreferredSize(new Dimension(550, 300));

		this.lowerBase.add(lowerLeftBase, BorderLayout.WEST);
		this.lowerBase.add(lowerRightBase, BorderLayout.CENTER);
		
		// update
		this.lowerBase.updateUI();
		
	} // end of method
	
	/**
	 * Table Selection
	 */
	private void changeTableSelection(long selectedTime) {
		
		// set parameters
		this.selectedTime = selectedTime;
		
		// update table
		if (this.dataTable30min != null) {
			this.dataTable30min.updateUI();
		}
		
		if (this.dataTable5min != null) {
			this.dataTable5min.updateUI();
		}
		
		if (this.dataTable5minOutlier != null) {
			this.dataTable5minOutlier.updateUI();
		}
		
	} // end of method
	
	/**
	 * set filtered data records 30min
	 */
	private boolean applyFilter4Records30min(Tms30minDataRecord record, int selectedItem) {
		
		// set parameters
		Tms30minDataCell cell = record.getDataCell(selectedItem);
		if (cell == null) {
			return false;
		}
		
		// search half_over
		if (filter.overFilter.get(cell.getHalf_over()) != Boolean.TRUE) {
			return false;
		}
		
		// search half_stat
		if (filter.sensorFilter.get(cell.getHalf_stat()) != Boolean.TRUE) {
			return false;
		}
			
		// search half_dlst
		if (filter.loggerFilter.get(cell.getHalf_dlst()) != Boolean.TRUE) {
			return false;
		}
			
		// search stat_code
		if (filter.statFilter.get(cell.getStat_code()) != Boolean.TRUE) {
			return false;
		}
		
		return true;
		
	} // end of method
	
	/**
	 * create control panel
	 */
	private JPanel createControlPanel() {
		
		// create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("  outlier detection result control  "),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// Add Control Button
		JButton queryDataButton = new JButton("query 30-min data");
		queryDataButton.setBounds(20, 20, 200, 20);
		queryDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showWaitMessageDialogAndDoQueryWork();
			}
		});
		basePanel.add(queryDataButton);
		
		// Add Control Button
		exportCsvFormatButton = new JButton("export csv format");
		exportCsvFormatButton.setBounds(250, 20, 200, 20);
		exportCsvFormatButton.setEnabled(false);
		exportCsvFormatButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exportCsvFormat();
			}
		});
		basePanel.add(exportCsvFormatButton);		
		return basePanel;
		
	} // end of method
	
	/**
	 * Create Filter Panel
	 */
	private JPanel createFilterPanel() {
		
		// create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("  30-min data filter configuration  "),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		/**
		// Set outlier filter
		JCheckBox outlierFilterAllCheck = new JCheckBox("OUTLIER");
		outlierFilterAllCheck.setSelected(false);
		outlierFilterAllCheck.setBounds(20, 20, 120, 25);
		outlierFilterAllCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i=0 ; i<outlierFilter.length ; i++) {
					outlierFilter[i].setSelected(((JCheckBox)e.getSource()).isSelected());
				}
			}
		});
		basePanel.add(outlierFilterAllCheck);
		
		int x = 160;
		int y = 20;
		for (int i=0 ; i<this.outlierFilter.length ; i++) {
		
			this.outlierFilter[i] = new JCheckBox(OUTLIER_CODE_STR[i]);
			this.outlierFilter[i].setSelected(true);
			this.outlierFilter[i].setBounds(x+100*i, y, 100, 25);
			final int codeIndex = i;
			this.outlierFilter[i].addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					filter.outlierFilter.put(OUTLIER_CODE[codeIndex], 
							((JCheckBox)e.getSource()).isSelected());
				}
			});
			basePanel.add(this.outlierFilter[i]);
		}
		*/
		
		// create JCombo
		int[] items = this.datasetManager.getItems();
		String[] itemNames = new String[items.length];
		for (int i=0 ; i<items.length ; i++) {
			itemNames[i] = ItemCodeDefine.getItemName(items[i]);
		}
		
		final JComboBox<String> itemSelectionCombo = new JComboBox<String>(itemNames);
		itemSelectionCombo.setBounds(20, 20, 100, 20);
		basePanel.add(itemSelectionCombo);
		
		// set selected item
		this.selectedItem = items[0];
		itemSelectionCombo.setSelectedIndex(0);
		
		itemSelectionCombo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				
				@SuppressWarnings("rawtypes")
				int selectedIndex = ((JComboBox)e.getSource()).getSelectedIndex();
				selectedItem = datasetManager.getItems()[selectedIndex];
				updateTimeseriesGraphPanel();
				
			}
		});

		
		// Add Control Button
		JButton applyButton = new JButton("apply filter");
		applyButton.setBounds(140, 20, 150, 20);
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateTimeseriesGraphPanel();
			}
		});
		basePanel.add(applyButton);
		
		// set components set boudns parameters
		int x = 160;
		int y = 45;
		
		// Set half_over filter
		JCheckBox halfOverFilterAllCheck = new JCheckBox("HALF_OVER");
		halfOverFilterAllCheck.setSelected(false);
		halfOverFilterAllCheck.setBounds(20, 45, 120, 25);
		halfOverFilterAllCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i=0 ; i<halfOverFilter.length ; i++) {
					halfOverFilter[i].setSelected(((JCheckBox)e.getSource()).isSelected());
				}
			}
		});
		basePanel.add(halfOverFilterAllCheck);
		
		for (int i=0 ; i<this.halfOverFilter.length ; i++) {
		
			this.halfOverFilter[i] = new JCheckBox(HALF_OVER_CODE_STR[i]);
			this.halfOverFilter[i].setSelected(true);
			this.halfOverFilter[i].setBounds(x+100*i, y, 100, 25);
			final int codeIndex = i;
			this.halfOverFilter[i].addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					filter.overFilter.put(HALF_OVER_CODE[codeIndex], 
							((JCheckBox)e.getSource()).isSelected());
				}
			});
			basePanel.add(this.halfOverFilter[i]);
		}
		
		// Set half_over filter
		JCheckBox halfStatFilterAllCheck = new JCheckBox("HALF_STAT");
		halfStatFilterAllCheck.setSelected(false);
		halfStatFilterAllCheck.setBounds(20, 70, 120, 25);
		halfStatFilterAllCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i=0 ; i<halfStatFilter.length ; i++) {
					halfStatFilter[i].setSelected(((JCheckBox)e.getSource()).isSelected());
				}
			}
		});
		basePanel.add(halfStatFilterAllCheck);
		
		y = 70;
		for (int i=0 ; i<this.halfStatFilter.length ; i++) {
			
			this.halfStatFilter[i] = new JCheckBox(HALF_STAT_CODE_STR[i]);
			this.halfStatFilter[i].setSelected(true);
			this.halfStatFilter[i].setBounds(x+100*i, y, 100, 25);
			final int codeIndex = i;
			this.halfStatFilter[i].addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					filter.sensorFilter.put(HALF_STAT_CODE[codeIndex], 
							((JCheckBox)e.getSource()).isSelected());
				}
			});
			basePanel.add(this.halfStatFilter[i]);
		}

		// Set half_over filter
		JCheckBox halfDlstFilterAllCheck = new JCheckBox("HALF_DLST");
		halfDlstFilterAllCheck.setSelected(false);
		halfDlstFilterAllCheck.setBounds(20, 95, 120, 25);
		halfDlstFilterAllCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i=0 ; i<halfDlstFilter.length ; i++) {
					halfDlstFilter[i].setSelected(((JCheckBox)e.getSource()).isSelected());
				}
			}
		});
		basePanel.add(halfDlstFilterAllCheck);
		
		y = 95;
		for (int i=0 ; i<this.halfDlstFilter.length ; i++) {
			
			this.halfDlstFilter[i] = new JCheckBox(HALF_DLST_CODE_STR[i]);
			this.halfDlstFilter[i].setSelected(true);
			this.halfDlstFilter[i].setBounds(x+100*i, y, 100, 25);
			final int codeIndex = i;
			this.halfDlstFilter[i].addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					filter.loggerFilter.put(HALF_DLST_CODE[codeIndex], 
							((JCheckBox)e.getSource()).isSelected());
				}
			});
			basePanel.add(this.halfDlstFilter[i]);
		}

		// Set half_over filter
		JCheckBox halfExchFilterAllCheck = new JCheckBox("STAT_CODE");
		halfExchFilterAllCheck.setSelected(false);
		halfExchFilterAllCheck.setBounds(20, 120, 120, 25);
		halfExchFilterAllCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i=0 ; i<halfExchFilter.length ; i++) {
					halfExchFilter[i].setSelected(((JCheckBox)e.getSource()).isSelected());
				}
			}
		});
		basePanel.add(halfExchFilterAllCheck);
		
		y = 120;
		int index = 0;
		for (int i=0 ; i<this.halfExchFilter.length ; i++) {
			
			if (i == 13) {
				index = 0;
				y += 25;
			}
			this.halfExchFilter[i] = new JCheckBox(HALF_EXCH_CODE_STR[i]);
			this.halfExchFilter[i].setSelected(true);
			this.halfExchFilter[i].setBounds(x+62*index, y, 62, 25);
			this.halfExchFilter[i].setToolTipText(HALF_EXCH_DESC[i]);
			final int codeIndex = i;
			this.halfExchFilter[i].addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					filter.statFilter.put(HALF_EXCH_CODE[codeIndex], 
							((JCheckBox)e.getSource()).isSelected());
				}
			});
			basePanel.add(this.halfExchFilter[i]);
			index++;
		}
		
		return basePanel;
		
	} // end of method
	
	/**
	 * create 30min viewer table
	 */
	private JScrollPane create30minDataTable() {
		
		// Create Table
		TableModel30min model = new TableModel30min();
		this.dataTable30min = new JTable(model);
		this.dataTable30min.setRowHeight(20);
		//this.dataTable30min.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.dataTable30min.getColumnModel().getColumn(0).setPreferredWidth(90);
		this.dataTable30min.getColumnModel().getColumn(1).setPreferredWidth(50);
		this.dataTable30min.getColumnModel().getColumn(2).setPreferredWidth(55);
		for (int i=3 ; i<7 ; i++) {
			this.dataTable30min.getColumnModel().getColumn(i).setPreferredWidth(40);
		}
		this.dataTable30min.getColumnModel().getColumn(7).setPreferredWidth(55);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		this.dataTable30min.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.dataTable30min.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.dataTable30min.getColumnModel().getColumn(2).setCellRenderer( rightRenderer );
		this.dataTable30min.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
		this.dataTable30min.getColumnModel().getColumn(4).setCellRenderer( centerRenderer );
		this.dataTable30min.getColumnModel().getColumn(5).setCellRenderer( centerRenderer );
		this.dataTable30min.getColumnModel().getColumn(6).setCellRenderer( centerRenderer );
		this.dataTable30min.getColumnModel().getColumn(7).setCellRenderer( rightRenderer );
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.dataTable30min);
		basePane.setPreferredSize(new Dimension(450, 300));
		return basePane;
		
	} // end of method
	
	private JScrollPane create5minDataTable() {

		// Create Table
		TableModel5min model = new TableModel5min();
		this.dataTable5min = new JTable(model);
		this.dataTable5min.setRowHeight(20);
		this.dataTable5min.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.dataTable5min.getColumnModel().getColumn(0).setPreferredWidth(90);
		this.dataTable5min.getColumnModel().getColumn(1).setPreferredWidth(50);
		this.dataTable5min.getColumnModel().getColumn(2).setPreferredWidth(100);
		this.dataTable5min.getColumnModel().getColumn(3).setPreferredWidth(70);
		for (int i=4 ; i<model.getColumnCount() ; i++) {
			this.dataTable5min.getColumnModel().getColumn(i).setPreferredWidth(60);
		}

		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		this.dataTable5min.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.dataTable5min.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.dataTable5min.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		this.dataTable5min.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
		for (int i=4 ; i<model.getColumnCount() ; i++) {
			this.dataTable5min.getColumnModel().getColumn(i).setCellRenderer(rightRenderer);
		}
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.dataTable5min);
		basePane.setPreferredSize(new Dimension(550, 150));
		return basePane;
		
	} // end of method
	
	private JScrollPane create5minOutlierTablePanel() {

		// Create Table
		TableModel5minOutlier model = new TableModel5minOutlier();
		this.dataTable5minOutlier = new JTable(model);
		this.dataTable5minOutlier.setRowHeight(20);
		this.dataTable5minOutlier.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.dataTable5minOutlier.getColumnModel().getColumn(0).setPreferredWidth(90);
		this.dataTable5minOutlier.getColumnModel().getColumn(1).setPreferredWidth(50);
		this.dataTable5minOutlier.getColumnModel().getColumn(2).setPreferredWidth(100);
		this.dataTable5minOutlier.getColumnModel().getColumn(3).setPreferredWidth(70);
		for (int i=4 ; i<model.getColumnCount() ; i++) {
			this.dataTable5minOutlier.getColumnModel().getColumn(i).setPreferredWidth(80);
		}

		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		this.dataTable5minOutlier.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.dataTable5minOutlier.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.dataTable5minOutlier.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		this.dataTable5minOutlier.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.dataTable5minOutlier);
		basePane.setPreferredSize(new Dimension(550, 150));
		return basePane;
		
	} // end of method
	
	/**
	 * Show Query Data
	 */
	private void showWaitMessageDialogAndDoQueryWork() {
		
		// Get selected StackInfo
		final EvalDatasetManager datasetManager = EvalDatasetManager.getInstance();
		final StackInfo stackInfo = datasetManager.getTmsDataset().getTmsDataHeader().getStackInfo();
		if (stackInfo == null) {
			JOptionPane.showMessageDialog(
					SwingUtilities.getWindowAncestor(this), 
					"Analysis target stack is not valid.");
		}
		final long sTime = datasetManager.getTmsDataset().getTmsDataHeader().getStart_time();
		final long eTime = datasetManager.getTmsDataset().getTmsDataHeader().getEnd_time();
		
		// Make SwingWorker
		final Window win = SwingUtilities.getWindowAncestor(this);
		SwingWorker<Void, Void> threadSwingWorker = new SwingWorker<Void, Void>() {

			@Override
			protected Void doInBackground() throws Exception {

				// set Tms30minDataset
				Tms30minDataset tms30minDataset = null;
				
				// Firstly, Search data file from "./clone_data/"
				String sTimeStr = DateUtil.getDateStr(sTime);
				String eTimeStr = DateUtil.getDateStr(eTime);
				String fileName = "./clone_data/tms_30min_data_" + 
						stackInfo.getFactoryInfo().getCode() + "_" + 
						stackInfo.getStackNumber() + "_" + 
						sTimeStr.substring(0, 10) + "_" + eTimeStr.substring(0, 10) + ".txt";
				File dataFile = new File(fileName);
				if (dataFile.exists()) {
					tms30minDataset = QueryTms30minRecordFromFile.readTmsRecord(
							fileName, 
							stackInfo.getFactoryInfo().getCode(), 
							stackInfo.getStackNumber(), 
							sTime, 
							eTime);
				} 
				// Secondly, query from database
				else {
					tms30minDataset= QueryTmsRecord.queryTms30minRecord(
						stackInfo.getFactoryInfo().getCode(), 
						stackInfo.getStackNumber(), 
						sTime, 
						eTime);
				}
			
				// Set Data
				datasetManager.setTms30minDataset(tms30minDataset);
				
				// File Save
				if (!dataFile.exists()) {
					int answer = JOptionPane.showConfirmDialog(
							win, 
							"Do you want to save the data to a file for use next time analysis?");
					if (answer == JOptionPane.YES_OPTION) {
						Tms30minDataWriter.writeDataFile(fileName, tms30minDataset);
					}
				}
				
				// set Data
				init();
				// return
				return null;
			}
		};
		
		// Create JDialog for show please wait message
		final JDialog dialog = new JDialog(win, "Information message", ModalityType.APPLICATION_MODAL);
		
		// Add SwingWorker Listener
		threadSwingWorker.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state")) {
					if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
						dialog.dispose();
					}
				}
			}
		});
		
		// Execute SwingWorker
		threadSwingWorker.execute();
		
		// Show ProgressBar
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		JPanel panel = new JPanel(new BorderLayout());
		panel.setPreferredSize(new Dimension(400, 80));
		panel.add(progressBar, BorderLayout.CENTER);
		JLabel messageLabel = new JLabel("  Database query job take serveral minutes.");
		messageLabel.setPreferredSize(new Dimension(300, 40));
		panel.add(messageLabel, BorderLayout.PAGE_START);
		dialog.add(panel);
		dialog.pack();
		dialog.setLocationRelativeTo(win);
		dialog.setVisible(true);
		
	} // end of method
	
	/**
	 * export data as a CSV format
	 */
	private void exportCsvFormat() {
		
		// get fact_code and stck_code
		RulesetModel rulesetModel = this.datasetManager.getRulesetModel();
		if (rulesetModel == null ) {
			JOptionPane.showMessageDialog(
					SwingUtilities.getWindowAncestor(this), 
					"Outlier detection model doesn't estabilished yet.");
		}
		String fact_code = rulesetModel.getFactCode();
		int    stck_code = rulesetModel.getStckCode();
		String fileName = String.format("OutlierDetectionEvaluationCompareResult-%s-%d", fact_code, stck_code);
		
		// Show JFileChooser
		JFileChooser fc = new JFileChooser();
		fc.setDialogType(JFileChooser.SAVE_DIALOG);
		fc.setSelectedFile(new File(fileName + ".csv"));
		fc.setFileFilter(new FileNameExtensionFilter("csv file","csv"));
		
		int returnVal = fc.showSaveDialog(SwingUtilities.getWindowAncestor(this));
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			OutlierDetectionResultCompareWriterAsCsvFormat.writeDataFile(
					file.getAbsolutePath(), 
					this.datasetManager.getItems(), 
					this.datasetManager.getRulsetBaseStartTime(),
					this.datasetManager.getRulsetBaseEndTime(),
					this.odrRecordHash,
					this.tmsRecordHash,
					this.tms30minRecordHash);
		}

	} // end of method

	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * 30min data table model
	 */
	class TableModel30min extends AbstractTableModel {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		private static final long serialVersionUID = 1L;
		
		//==========================================================================
		// Fields
		//==========================================================================
		private String[] columnNames = 
			{"Time", "Item", "Value", "Over", "State", "DLSate", "Code", "SetValue"};
		
		private Tms30minDataCell[] selectedCells = null;
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public TableModel30min() {
			
		} // end of constructor
		
		//==========================================================================
		// Methods
		//==========================================================================
		/**
		 * set selected Record cell
		 */
		public Tms30minDataCell[] getSelectedDataCell() {
			
			// get selected record
			Tms30minDataRecord selectedRecord = tms30minRecordHash.get(selectedTime);
			if (selectedRecord == null) {
				return null;
			} else {
				int[] items = selectedRecord.getValidItems();
				Tms30minDataCell[] selectedCells = new Tms30minDataCell[items.length];
				for (int i=0 ; i<items.length ; i++) {
					selectedCells[i] = selectedRecord.getDataCell(items[i]);
				}
				return selectedCells;
			}
			
		} // end of method
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			
			this.selectedCells = getSelectedDataCell();
			if (this.selectedCells == null) {
				return 0;
			} else {
				return this.selectedCells.length;
			}
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (this.selectedCells == null) {
				return "";
			}
			
			if (col == 0) {
				return DateUtil.getSimpleMinStr(selectedTime);
			} else if (col == 1) {
				return ItemCodeDefine.getItemName(this.selectedCells[row].getData_type());
			} else if (col == 2) {
				return this.selectedCells[row].getHalf_valu();
			} else if (col == 3) {
				return this.selectedCells[row].getHalf_over();
			} else if (col == 4) {
				return this.selectedCells[row].getHalf_stat();
			} else if (col == 5) {
				return this.selectedCells[row].getHalf_dlst();
			} else if (col == 6) {
				return this.selectedCells[row].getStat_code();
			} else {
				double stat_value = this.selectedCells[row].getStat_valu();
				if (stat_value == -1.0D) {
					return "";
				} else {
					return stat_value;
				}
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * 5min data table model
	 */
	class TableModel5min extends AbstractTableModel {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		private static final long serialVersionUID = 1L;
		
		//==========================================================================
		// Fields
		//==========================================================================
		private String[] columnNames = null;
		private int[]    items       = null;
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public TableModel5min() {
			
			// get parameters
			items = datasetManager.getTmsDataset().getItems();
			
			// Set ColumnName
			int columnCount = 4 + items.length;
			this.columnNames = new String[columnCount];
			
			int index = 0;
			this.columnNames[index++] = "Time";
			this.columnNames[index++] = "P.ID";
			this.columnNames[index++] = "P.Name";
			this.columnNames[index++] = "Level";
			for (int i=0 ; i<items.length ; i++) {
				this.columnNames[index++] = ItemCodeDefine.getItemName(this.items[i]);
			}
			
		} // end of constructor
		
		//==========================================================================
		// Methods
		//==========================================================================
		public long[] getSelectedTimes() {
			
			// set base time
			long baseTime = selectedTime;
			long timeInterval = 5L*60L*1000L;
			long[] times = new long[6];
			
			// create times
			for (int i=0 ; i<times.length ; i++) {
				times[i] = baseTime + (long)i * timeInterval;
			}
			return times;
			
		} // end of method
		
		public long getTime(int row) {
			return selectedTime + ((long)row) * (5L*60L*1000L);
		} // end of method
		
		public OdrRecord getOdrRecord(long time) {
			return odrRecordHash.get(time);
		} // end of method
		
		public TmsDataRecord getTmsDataRecord(long time) {
			return tmsRecordHash.get(time);
		} // end of method
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			return 6;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			// set target time
			long targetTime = getTime(row);
			
			// get value
			if (col == 0) {
				return DateUtil.getSimpleMinStr(targetTime);
			} else if (col == 1) {
				
				OdrRecord targetOdrRecord = getOdrRecord(targetTime);
				if (targetOdrRecord == null) {
					return "";
				} else {
					return targetOdrRecord.patternId + "";
				}
				
			} else if (col == 2) {
				
				OdrRecord targetOdrRecord = getOdrRecord(targetTime);
				if (targetOdrRecord == null) {
					return "";
				} else {
					return targetOdrRecord.patternName + "";
				}
				
			} else if (col == 3) {
				
				OdrRecord targetOdrRecord = getOdrRecord(targetTime);
				if (targetOdrRecord == null) {
					return "";
				} else {
					return targetOdrRecord.getOutlierLevelStr();
				}
			
			} else {
				
				TmsDataRecord targetTmsRecord = getTmsDataRecord(targetTime);
				if (targetTmsRecord == null) {
					return "";
				} else {
					TmsDataCell dataCell = targetTmsRecord.getDataCell(items[col-4]);
					if (dataCell == null) {
						return "";
					} else {
						return String.format("%.1f", dataCell.getData_value());
					}
					
				}
				
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * 5min data table model
	 */
	class TableModel5minOutlier extends AbstractTableModel {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		private static final long serialVersionUID = 1L;
		
		//==========================================================================
		// Fields
		//==========================================================================
		private String[]  columnNames = null;
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public TableModel5minOutlier() {
			
			// get first OdrCell
			OdrRecord firstOdrRecord = odrRecords[0];
			
			if (firstOdrRecord == null) {
				this.columnNames = new String[4];
				return;
			}
			
			// Set ColumnName
			OdrCell[] odrCells = firstOdrRecord.odrCells;
			this.columnNames = new String[4 + odrCells.length];
			
			int index = 0;
			this.columnNames[index++] = "Time";
			this.columnNames[index++] = "P.ID";
			this.columnNames[index++] = "P.Name";
			this.columnNames[index++] = "Level";
			for (int i=0 ; i<odrCells.length ; i++) {
				this.columnNames[index++] = odrCells[i].ruleTypeName;
			}
			
		} // end of constructor
		
		//==========================================================================
		// Methods
		//==========================================================================
		public long[] getSelectedTimes() {
			
			// set base time
			long baseTime = selectedTime;
			long timeInterval = 5L*60L*1000L;
			long[] times = new long[6];
			
			// create times
			for (int i=0 ; i<times.length ; i++) {
				times[i] = baseTime + (long)i * timeInterval;
			}
			return times;
			
		} // end of method
		
		public long getTime(int row) {
			return selectedTime + ((long)row) * (5L*60L*1000L);
		} // end of method
		
		public OdrRecord getOdrRecord(long time) {
			return odrRecordHash.get(time);
		} // end of method
		
		public TmsDataRecord getTmsDataRecord(long time) {
			return tmsRecordHash.get(time);
		} // end of method
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			return 6;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			// set target time
			long targetTime = getTime(row);
			
			// get value
			if (col == 0) {
				return DateUtil.getSimpleMinStr(targetTime);
			} else if (col == 1) {
				
				OdrRecord targetOdrRecord = getOdrRecord(targetTime);
				if (targetOdrRecord == null) {
					return "";
				} else {
					return targetOdrRecord.patternId + "";
				}
				
			} else if (col == 2) {
				
				OdrRecord targetOdrRecord = getOdrRecord(targetTime);
				if (targetOdrRecord == null) {
					return "";
				} else {
					return targetOdrRecord.patternName + "";
				}
				
			} else if (col == 3) {
				
				OdrRecord targetOdrRecord = getOdrRecord(targetTime);
				if (targetOdrRecord == null) {
					return "";
				} else {
					return targetOdrRecord.getOutlierLevelStr();
				}
			
			} else {
				
				OdrRecord targetOdrRecord = getOdrRecord(targetTime);
				if (targetOdrRecord == null) {
					return new Boolean(false);
				} else {
					OdrCell[] odrCells = targetOdrRecord.odrCells;
					return new Boolean(odrCells[col-4].isOutlier != 0);
				}
				
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return String.class;
			} else if (col == 1) {
				return String.class;
			} else if (col == 2) {
				return String.class;
			} else if (col == 3) {
				return String.class;
			} else {
				return Boolean.class;
			}
			
		} // end of method
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * 30min data filter
	 */
	class DataRecordFilter {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		
		//==========================================================================
		// Fields
		//==========================================================================
		public Hashtable<String, Boolean> outlierFilter= new Hashtable<String, Boolean>();
		public Hashtable<String, Boolean> overFilter   = new Hashtable<String, Boolean>();
		public Hashtable<String, Boolean> sensorFilter = new Hashtable<String, Boolean>();
		public Hashtable<String, Boolean> loggerFilter = new Hashtable<String, Boolean>();
		public Hashtable<String, Boolean> statFilter   = new Hashtable<String, Boolean>();
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public DataRecordFilter() {
			
			// outlier
			outlierFilter.put("",         true);
			outlierFilter.put("WARNING",  true);
			outlierFilter.put("MINOR",    true);
			outlierFilter.put("MAJOR",    true);
			outlierFilter.put("CRITICAL", true);

			// half_over
			overFilter.put("0", true);
			overFilter.put("1", true);
			overFilter.put("2", true);
			
			// half_stat
			sensorFilter.put("0", true);
			sensorFilter.put("1", true);
			sensorFilter.put("2", true);
			sensorFilter.put("4", true);
			sensorFilter.put("8", true);
			
			// half_dlst
			loggerFilter.put("0", true);
			loggerFilter.put("1", true);
			loggerFilter.put("4", true);
			
			// stat_code
			statFilter.put("0", true);
			statFilter.put("10", true);
			statFilter.put("11", true);
			statFilter.put("12", true);
			statFilter.put("13", true);
			statFilter.put("14", true);
			statFilter.put("15", true);
			statFilter.put("16", true);
			statFilter.put("18", true);
			statFilter.put("19", true);
			statFilter.put("20", true);
			statFilter.put("21", true);
			statFilter.put("22", true);
			statFilter.put("24", true);
			statFilter.put("30", true);
			statFilter.put("40", true);
			statFilter.put("50", true);
			statFilter.put("60", true);
			statFilter.put("61", true);
			statFilter.put("62", true);
			statFilter.put("70", true);
			statFilter.put("90", true);
			statFilter.put("91", true);
			statFilter.put("R1", true);
			statFilter.put("R2", true);
			statFilter.put("R3", true);
			
		} // end of constructor
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================

		
	} // end of class
	
} // end of class
