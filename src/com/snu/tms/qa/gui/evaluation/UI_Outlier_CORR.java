/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.evaluation;

import javax.swing.JTabbedPane;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRuleset;


/**
 * Panel for outlier(Correlation analysis) detection
 * 
 */
public class UI_Outlier_CORR extends JTabbedPane
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Show analysis result for outlier detection using Correction Analysis
	 */
	public UI_Outlier_CORR(DetectionPatternDataset patternData) {
		
		DetectionRuleset[] corrRulesets = patternData.getAllRuleset(DetectionRuleset.RULESET_CORR);
		
		// filter with r-sqaure and p-value
		for (int i=0 ; i<corrRulesets.length ; i++) {
			
			int[] items = corrRulesets[i].getItems();
			String name = String.format(
					"CORR [%s - %s]", 
					ItemCodeDefine.getItemName(items[0]),
					ItemCodeDefine.getItemName(items[1]));
				this.add(name, new UI_Outlier_CORR_ITEM(patternData, items[0], items[1]));
		}
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
