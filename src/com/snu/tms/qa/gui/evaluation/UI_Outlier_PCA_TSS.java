/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.evaluation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.text.SimpleDateFormat;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.event.ChartProgressEvent;
import org.jfree.chart.event.ChartProgressListener;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.DetectionRuleset;
import com.snu.tms.qa.model.ruleset.RulesetPcaTss;
import com.snu.tms.qa.util.NumberUtil;


/**
 * Panel for outlier(PCA) detection
 * 
 */
public class UI_Outlier_PCA_TSS extends JPanel
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private DetectionPatternDataset     patternData;
	private RulesetPcaTss               ruleset;
	
	private JTable                      outlierCheckTable;
	
	private JPanel                      chartBasePanel;
	
	private Hashtable<Long, Integer>    timeHash;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Show analysis result for outlier detection using PCA(Principle Component Analysis)
	 */
	public UI_Outlier_PCA_TSS(DetectionPatternDataset patternData) {
		
		// set data
		this.patternData = patternData;
		
		// set ruleset
		DetectionRuleset registeredRuleset = patternData.getRuleset(new RulesetPcaTss());
		if (registeredRuleset == null) {
			return;
		} else {
			this.ruleset = (RulesetPcaTss)registeredRuleset;
		}
		
		// create timeHash
		createTimeHashtable();
		
		// create control panel
		JPanel controlBase = createControlPanel();
		
		// Set upper components
		JPanel chartBase = createOutlierChartPanel();
		
		// Set lower components
		JScrollPane outlierCheckBase = createOutlierTablePanel();
		
		// setLayout
		this.setLayout(new BorderLayout());
		// add controlBase
		this.add(controlBase, BorderLayout.NORTH);
		// create basePanel
		JPanel basePanel = new JPanel(new BorderLayout());
		basePanel.add(chartBase, BorderLayout.NORTH);
		basePanel.add(outlierCheckBase, BorderLayout.CENTER);
		this.add(basePanel, BorderLayout.CENTER);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create Control Panel
	 */
	public JPanel createControlPanel() {

		// Set Base Panel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(400, 30));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder(""),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
				
		// set ruleset enable
		JCheckBox rulesetEnableCheck = new JCheckBox("enable ruleset");
		rulesetEnableCheck.setSelected(this.ruleset.isRulesetEnabled());
		rulesetEnableCheck.setEnabled(false);
		rulesetEnableCheck.setBounds(10, 5, 140, 20);
		basePanel.add(rulesetEnableCheck);
		
		// Set Threshold button
		JLabel label = new JLabel("confidence level");
		label.setBounds(150, 5, 110, 20);
		basePanel.add(label);
		
		JTextField confidenceLevel = new JTextField(String.format("%.2f", ruleset.getConfidenceLevel() * 100.0D) + " %");
		confidenceLevel.setEditable(false);
		confidenceLevel.setBounds(260, 5, 100, 20);
		basePanel.add(confidenceLevel);
		
		label = new JLabel("threshold");
		label.setBounds(380, 5, 80, 20);
		basePanel.add(label);
		
		JTextField thresholdValue = new JTextField(String.format("%.3f", ruleset.getThresholds()[0]));
		thresholdValue.setEditable(false);
		thresholdValue.setBounds(460, 5, 100, 20);
		basePanel.add(thresholdValue);

		// return
		return basePanel;
		
	} // end of method
	
	/**
	 * create T-Square statistics and SPE panel
	 */
	public JPanel createOutlierChartPanel() {

		JPanel basePanel = new JPanel(new BorderLayout());
		basePanel.setPreferredSize(new Dimension(400, 200));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("T-square statistics plot"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		this.chartBasePanel = new JPanel(new BorderLayout());
		this.chartBasePanel.add(createChartPanel(), BorderLayout.CENTER);
		basePanel.add(this.chartBasePanel, BorderLayout.CENTER);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * create outlier check table panel
	 */
	private JScrollPane createOutlierTablePanel() {

		// Create Table
		this.outlierCheckTable = new JTable(new TableModel());
		this.outlierCheckTable.setRowHeight(20);
		//dataTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.outlierCheckTable.getColumnModel().getColumn(0).setPreferredWidth(170);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		this.outlierCheckTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.outlierCheckTable.getColumnModel().getColumn(2).setCellRenderer( rightRenderer );
		for (int i=0 ; i<patternData.getItems().length ; i++) {
			this.outlierCheckTable.getColumnModel().getColumn(i+3).setCellRenderer( rightRenderer );
		}
				
		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.outlierCheckTable);
		basePane.setPreferredSize(new Dimension(400, 200));
		return basePane;
		
	} // end of method
	
	/**
	 * create chart Panel
	 */
	private JPanel createChartPanel() {
		
		// create XYDataSet
		XYDataset chartDataSet = createDataSet();
		
		// create chart
		final JFreeChart chart = ChartFactory.createTimeSeriesChart(
				"",  // title
				"",             // x-axis label
				"",   // y-axis label
				chartDataSet,            // data
				false,               // create legend?
				true,               // generate tooltips?
				false               // generate URLs?
		);
		
		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setNoDataMessage("NO DATA");
		plot.setDomainPannable(true);
		plot.setRangePannable(false);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);
		
		// set chart shape properties
		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		plot.setRangeCrosshairVisible(true);
		plot.setRangeCrosshairLockedOnData(true);
		plot.setDomainZeroBaselineVisible(true);
		plot.setRangeZeroBaselineVisible(true);
		XYDotRenderer renderer = new XYDotRenderer();
		renderer.setSeriesPaint(0, Color.blue);
		renderer.setSeriesPaint(1, Color.red);
		renderer.setDotWidth(3);
		renderer.setDotHeight(3);
		plot.setRenderer(renderer);
		
		// set x-axis properties
		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("yyMMdd"));
		
		// Add chart listener
		chart.addProgressListener( new ChartProgressListener() {
			public void chartProgress(ChartProgressEvent event) {
				
				if (event.getType() != ChartProgressEvent.DRAWING_FINISHED) {
					return;
				}
				
				// get mouse click domain axis point
				XYPlot plot = (XYPlot) chart.getPlot();
				double xx = plot.getDomainCrosshairValue();
				
				// change table selection
				changeTableRowSelection((long)xx);
				
				
				
			} // end of method
		});

		// return
		return new ChartPanel(chart);
		
	} // end of method

	/**
	 * Create graph data set
	 */
	private XYDataset createDataSet() {
		
		// create TimeSeries
		String[] seriesName = {"normal data", "outlier data"};
		TimeSeries[] tmsDatasets = new TimeSeries[2];
		for (int i=0 ; i<2 ; i++) {
			tmsDatasets[i] = new TimeSeries(seriesName[i]);
		}
		
		// prepare parameters
		Minute time = null;
		DetectionRecord[] dataRecord = this.patternData.getAllDetectionRecords();
		double value = 0.0D;
		double threshold = this.ruleset.getThresholds()[0];
		
		// Set data
		int[] items = this.ruleset.getItems();
		for (int i=0 ; i<dataRecord.length ; i++) {
			value = this.ruleset.applyRuleset(dataRecord[i].getRawDataCellValue(items));
			time = new Minute(new java.util.Date(dataRecord[i].getTime()));
			if (value > threshold) {
				if (value > (threshold * 3.0D)) {
					value = threshold * 3.0D;
				}
				tmsDatasets[1].add(time, value);
			} else {
				tmsDatasets[0].add(time, value);
			}
		}
		
		// Create TimeSeriesCollection
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(tmsDatasets[0]);
		dataset.addSeries(tmsDatasets[1]);
		
		// return
		return dataset;
		
	} // end of method
	
	/**
	 * Create time hashtable
	 */
	private void createTimeHashtable() {
		
		// create time hash
		this.timeHash = new Hashtable<Long, Integer>();
		
		// set data
		DetectionRecord[] records = patternData.getAllDetectionRecords();
		for (int i=0 ; i<records.length ; i++) {
			this.timeHash.put(records[i].getTime(), i);
		}
		
	} // end of method
	
	/**
	 * Table Selection
	 */
	private void changeTableRowSelection(long selectedTime) {
		
		// get table row index from time hash
		Integer row = this.timeHash.get(selectedTime);
		if (row == null) {
			return;
		}
		
		// table selection
		this.outlierCheckTable.setRowSelectionInterval(row, row);
		JViewport viewport = (JViewport)this.outlierCheckTable.getParent();
		Rectangle rect = this.outlierCheckTable.getCellRect(row, 0, true);
		Rectangle viewRect = viewport.getViewRect();
		
		int y = viewRect.y;
		if (rect.y >= viewRect.y && rect.y <= (viewRect.y + viewRect.height - rect.height)) {
			
		} else if (rect.y < viewRect.y) {
			y = rect.y;
		} else if (rect.y > (viewRect.y + viewRect.height - rect.height)) {
			 y = rect.y - viewRect.height + rect.height;
		}
		viewport.setViewPosition(new Point(0, y));
		
	} // end of method



	//==========================================================================
	// Inner Class
	//==========================================================================
	class TableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		private String[] columnNames;
		private int[]    items;
		
		private DetectionRecord[] records;
		
		public TableModel() {
			
			// set dataset
			this.records = patternData.getAllDetectionRecords();
			this.items   = patternData.getItems();
			
			// set column names
			this.columnNames = new String[items.length + 3];
			columnNames[0] = "Time";
			columnNames[1] = "Outlier";
			columnNames[2] = "Error";
			for (int i=0 ; i<items.length ; i++) {
				this.columnNames[i+3] = ItemCodeDefine.getItemName(items[i]);
			}
		}
		
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			if (records == null) {
				return 0;
			}
			return records.length;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return records[row].getTimeStr();
			} else if (col == 1) {
				return new Boolean(records[row].getDetectionDataCell(ruleset).isOutlier());
			} else if (col == 2) {
				return NumberUtil.getNumberStr(records[row].getDetectionDataCell(ruleset).getResultValue());
			} else {
				return NumberUtil.getNumberStr(records[row].getRawDataCell(items[col-3]).getData_value());
			}
		
		} // end of method
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 1) {
				return Boolean.class;
			} else {
				return String.class;
			}
			
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	


} // end of class
