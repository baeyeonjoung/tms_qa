/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.evaluation;

import javax.swing.JTabbedPane;

import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;


/**
 * Panel for outlier(PCA) detection
 * 
 */
public class UI_Outlier_PCA extends JTabbedPane
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Show analysis result for outlier detection using PCA(Principle Component Analysis)
	 */
	public UI_Outlier_PCA(DetectionPatternDataset patternData) {
		
		// Add PCA - T-Square Statistics analysis
		this.add("PCA(TSS)", new UI_Outlier_PCA_TSS(patternData));

		// Add PCA - Statistic Prediction Error analysis
		this.add("PCA(SPE)", new UI_Outlier_PCA_SPE(patternData));
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
