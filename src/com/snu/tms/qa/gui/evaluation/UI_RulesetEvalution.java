/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.evaluation;

import com.snu.tms.qa.soap.QueryTmsRecord;
import com.snu.tms.qa.soap.RulesetModelQueryAgent;
import com.snu.tms.qa.fileio.QueryTmsRecordFromFile;
import com.snu.tms.qa.fileio.TmsDataReader;
import com.snu.tms.qa.fileio.TmsDataWriter;
import com.snu.tms.qa.gui.evaluation.UI_Outlier_CORR;
import com.snu.tms.qa.gui.evaluation.UI_Outlier_PCA;
import com.snu.tms.qa.gui.evaluation.UI_Outlier_PD;
import com.snu.tms.qa.gui.evaluation.UI_Outlier_SPC;
//import com.snu.tms.qa.gui.evaluation.dialog.DialogEditCorrection;
import com.snu.tms.qa.gui.evaluation.dialog.DialogOutlierDetectionResult;
import com.snu.tms.qa.gui.evaluation.dialog.DialogPatternResult;
import com.snu.tms.qa.gui.evaluation.dialog.DialogRawDataCsvExporter;
import com.snu.tms.qa.gui.evaluation.dialog.DialogRawDataGraph;
import com.snu.tms.qa.gui.evaluation.dialog.DialogRawDataViewer;
import com.snu.tms.qa.gui.evaluation.dialog.DialogOutlierDetectionThreshold;
import com.snu.tms.qa.model.EvalDatasetManager;
import com.snu.tms.qa.model.InventoryDataManager;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.RulesetModel;
import com.snu.tms.qa.util.DateUtil;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.Dialog.ModalityType;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Properties;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;


/**
 * Outlier detection panel
 * 
 */
public class UI_RulesetEvalution extends JPanel
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;

	private static Log logger = LogFactory.getLog(UI_RulesetEvalution.class);

	//==========================================================================
	// Local Fields
	//==========================================================================
	private EvalDatasetManager    datasetManager = EvalDatasetManager.getInstance();
	
	private  JPanel               overallBasePanel      = null;
	private  JPanel               contentsBasePanel     = null;
	
	private  JLabel               selectedPatternLabel  = null;
	private  JTabbedPane          outlierDetectionPanel = null;
	
	private  JDatePickerImpl      startDatePicker;
	private  JDatePickerImpl      endDatePicker;
	
	private  JTable               rulesetTable  = null;
	private  Vector<RulesetModel> rulesetModels = new Vector<RulesetModel>();
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public UI_RulesetEvalution() {
		
		// add data loading control panel
		JPanel dataControlBase = createDataControlPanel();
		this.overallBasePanel = new JPanel(new BorderLayout());
		
		// set Layout
		this.setLayout(new BorderLayout());
		this.add(dataControlBase, BorderLayout.NORTH);
		this.add(this.overallBasePanel, BorderLayout.CENTER);
		
	} // end of constructor
	
	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create data loading control panel
	 */
	private JPanel createDataControlPanel() {

		// Set target date
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(600, 60));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("  data loading control  "),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));

		// Set start Date
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year",  "Year");

		UtilDateModel startDateModel = new UtilDateModel();
		startDateModel.setDate(2014, 0, 1);
		startDateModel.setSelected(true);
		JDatePanelImpl startDatePanel = new JDatePanelImpl(startDateModel, p);
		startDatePanel.updateUI();
		startDatePanel.setShowYearButtons(true);
		this.startDatePicker = new JDatePickerImpl(startDatePanel, new DateLabelFormatter());
		this.startDatePicker.setBounds(20, 20, 170, 30);
		basePanel.add(this.startDatePicker);

		// Set end Date
		UtilDateModel endDateModel = new UtilDateModel();
		endDateModel.setDate(2014, 1, 1);
		endDateModel.setSelected(true);
		JDatePanelImpl endDatePanel = new JDatePanelImpl(endDateModel, p);
		endDatePanel.setShowYearButtons(true);
		this.endDatePicker = new JDatePickerImpl(endDatePanel, new DateLabelFormatter());
		this.endDatePicker.setBounds(210, 20, 170, 30);
		basePanel.add(this.endDatePicker);
		
		// Set Apply Button
		JButton dataLoadingButton = new JButton("apply");
		dataLoadingButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadData();
			}
		});
		dataLoadingButton.setBounds(400, 20, 80, 25);
		basePanel.add(dataLoadingButton);

		// return
		return basePanel;
		
	} // end of method
	
	/**
	 * load (from db query or file loading) file
	 */
	private void loadData() {
		
		// Get Selected StackInfo
		InventoryDataManager inventoryDataManager = InventoryDataManager.getInstance();
		StackInfo stackInfo = inventoryDataManager.getSelectedStackInfo();
		
		// Get Start Time
		int sYear  = this.startDatePicker.getModel().getYear();
		int sMonth = this.startDatePicker.getModel().getMonth();
		int sDay   = this.startDatePicker.getModel().getDay();
		String sDateStr = DateUtil.getDateStr(sYear, sMonth, sDay);
		
		// Get Start Time
		int eYear  = this.endDatePicker.getModel().getYear();
		int eMonth = this.endDatePicker.getModel().getMonth();
		int eDay   = this.endDatePicker.getModel().getDay();
		String eDateStr = DateUtil.getDateStr(eYear, eMonth, eDay);
		
		// Check StackInfo and TimeSet
		if (stackInfo == null) {
			JOptionPane.showMessageDialog(
					SwingUtilities.getWindowAncestor(this), 
					"Analysis target stack is not valid.");
		} else if (eDateStr.compareTo(sDateStr) <= 0) {
			JOptionPane.showMessageDialog(
					SwingUtilities.getWindowAncestor(this), 
					"Analysis target time is not valid.");
		} else {
			// Show Dialog and query start
			showWaitMessageDialogAndDoQueryWork(stackInfo, sDateStr, eDateStr, true);
		}
		
		// db query for ruleset
		setRulesetModel();
		
		// update panel
		showGeneralControlPanel();
		
	} // end of method
	
	/**
	 * Data loading waiting dialog
	 */
	private void showWaitMessageDialogAndDoQueryWork(
			final StackInfo stackInfo, 
			final String sTimeStr, 
			final String eTimeStr, 
			final boolean fileSave) {
		
		// Make SwingWorker
		SwingWorker<Void, Void> threadSwingWorker = new SwingWorker<Void, Void>() {

			@Override
			protected Void doInBackground() throws Exception {
				
				// Prepare DataSetInstnace
				EvalDatasetManager datasetManager = EvalDatasetManager.getInstance();
				datasetManager.clear();
				
				// prepare TmsDataSet
				TmsDataset tmsDataSet = null;
				
				// search file
				String targetFilePath = TmsDataReader.searchFile(
						"./clone_data", 
						stackInfo.getFactoryInfo().getCode(), 
						stackInfo.getStackNumber(), 
						sTimeStr.substring(0, 10), 
						eTimeStr.substring(0, 10));
				if (targetFilePath == null) {
					targetFilePath = "*";
				}
				
				// Firstly, Search data file from "./clone_data/"
				File dataFile = new File(targetFilePath);
				if (dataFile.exists()) {
					logger.debug("Target file exist. [" + targetFilePath + "]");
					tmsDataSet = QueryTmsRecordFromFile.readTmsRecord(
							targetFilePath, 
							stackInfo.getFactoryInfo().getCode(), 
							stackInfo.getStackNumber(), 
							sTimeStr, 
							eTimeStr);
					tmsDataSet.getTmsDataHeader().setFactoryInfo(stackInfo.getFactoryInfo());
					tmsDataSet.getTmsDataHeader().setStackInfo(stackInfo);
				} 
				// Secondly, query from database
				else {
					logger.debug("Target file doesn't exist. So try db query.");
					tmsDataSet = QueryTmsRecord.queryTmsRecord(
							stackInfo.getFactoryInfo().getCode(), 
							stackInfo.getStackNumber(), 
							sTimeStr, 
							eTimeStr);
					tmsDataSet.getTmsDataHeader().setFactoryInfo(stackInfo.getFactoryInfo());
					tmsDataSet.getTmsDataHeader().setStackInfo(stackInfo);
				}

				// Set Data
				datasetManager.setTmsDataset(tmsDataSet);
				
				// File Save
				if (!dataFile.exists() && fileSave) {
					String fileName = "./clone_data/tms_data_" + 
							stackInfo.getFactoryInfo().getCode() + "_" + 
							stackInfo.getStackNumber() + "_" + 
							sTimeStr.substring(0, 10) + "_" + eTimeStr.substring(0, 10) + ".txt";
					TmsDataWriter.writeDataFile(fileName, tmsDataSet);
				}

				// return
				return null;
			}
		};
		
		// Create JDialog for show please wait message
		Window win = SwingUtilities.getWindowAncestor(this);
		final JDialog dialog = new JDialog(win, "Information message", ModalityType.APPLICATION_MODAL);
		
		// Add SwingWorker Listener
		threadSwingWorker.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state")) {
					if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
						dialog.dispose();
					}
				}
			}
		});
		
		// Execute SwingWorker
		threadSwingWorker.execute();
		
		// Show ProgressBar
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		JPanel panel = new JPanel(new BorderLayout());
		panel.setPreferredSize(new Dimension(400, 80));
		panel.add(progressBar, BorderLayout.CENTER);
		JLabel messageLabel = new JLabel("  Database query job take serveral minutes.");
		messageLabel.setPreferredSize(new Dimension(300, 40));
		panel.add(messageLabel, BorderLayout.PAGE_START);
		dialog.add(panel);
		dialog.pack();
		dialog.setLocationRelativeTo(win);
		dialog.setVisible(true);
		
	} // end of method
	
	/**
	 * update ruleset table
	 */
	private void updateRulesetTable() {

		// Get Selected StackInfo
		InventoryDataManager inventoryDataManager = InventoryDataManager.getInstance();
		StackInfo stackInfo = inventoryDataManager.getSelectedStackInfo();
		
		// Check StackInfo and TimeSet
		if (stackInfo == null) {
			JOptionPane.showMessageDialog(
					SwingUtilities.getWindowAncestor(this), 
					"Ruleset target stack selecting is required in left window.");
		}
		
		// update ruleset table
		setRulesetModel();
		this.rulesetTable.updateUI();

	} // end of method
	
	/**
	 * set ruleset model
	 */
	private void setRulesetModel() {
		
		// Get Selected StackInfo
		InventoryDataManager inventoryDataManager = InventoryDataManager.getInstance();
		StackInfo stackInfo = inventoryDataManager.getSelectedStackInfo();

		if (stackInfo == null) {
			return;
		}
		
		// clear ruleset models
		this.rulesetModels.clear();
		
		// query ruleset model
		RulesetModel[] rulesetModels = RulesetModelQueryAgent.queryRulesetModel(
				stackInfo.getFactoryInfo().getCode(), 
				stackInfo.getStackNumber());
		if (rulesetModels != null) {
			for (RulesetModel model : rulesetModels) {
				this.rulesetModels.add(model);
			}
		}
		
	} // end of method
	

	/**
	 * Set general statistical analysis
	 */
	private void showGeneralControlPanel() {
		
		// remove all sub-contents components
		if (this.overallBasePanel != null) {
			this.overallBasePanel.removeAll();
		}
		
		if (this.datasetManager.getTmsDataset() == null) {
			return;
		}
		
		// create contents panel
		JPanel       controlPanel = createControlPanel();
		JScrollPane  rulesetPanel = createRulesetControlPanel();
		this.contentsBasePanel = new JPanel(new BorderLayout());
		
		JPanel basePanel = new JPanel(new BorderLayout());
		basePanel.setPreferredSize(new Dimension(600, 200));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("  select apply ruleset  "),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));

		basePanel.add(controlPanel, BorderLayout.NORTH);
		basePanel.add(rulesetPanel, BorderLayout.CENTER);
		
		// add panel
		this.overallBasePanel.add(basePanel, BorderLayout.NORTH);
		this.overallBasePanel.add(this.contentsBasePanel, BorderLayout.CENTER);
		
		// update UI
		this.overallBasePanel.updateUI();
		
	} // end of method
		
	/**
	 * Create general statisics control panel
	 */
	private JPanel createControlPanel() {
		
		// Set ancestor
		final JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		
		// Set Base Panel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(600, 35));
		
		// Set Apply Button
		JButton refreshButton = new JButton("refresh ruleset");
		refreshButton.setBounds(10, 0, 120, 25);
		refreshButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateRulesetTable();
			}
		});
		basePanel.add(refreshButton);

		// apply ruleset button
		JButton applyButton = new JButton("apply ruleset");
		applyButton.setBounds(140, 0, 120, 25);
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				applyRuleset();
			}
		});
		basePanel.add(applyButton);

		// Set ControlButton
		JButton viewDataButton = new JButton("data viewer");
		viewDataButton.setBounds(290, 0, 100, 25);
		viewDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = EvalDatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogRawDataViewer(
						rootFrame, 
						stackInfoStr,
						dataSet);
			}
		});
		basePanel.add(viewDataButton);

		JButton exportDataButton = new JButton("data export");
		exportDataButton.setBounds(400, 0, 100, 25);
		exportDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = EvalDatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("%s_%d",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber());
				DialogRawDataCsvExporter.exportRawDataAsCsvFormat(rootFrame, stackInfoStr, dataSet);
			}
		});
		basePanel.add(exportDataButton);
			
		JButton showGraphButton = new JButton("graph");
		showGraphButton.setBounds(510, 0, 100, 25);
		showGraphButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = EvalDatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogRawDataGraph(
						rootFrame, 
						stackInfoStr,
						dataSet);
			}
		});
		basePanel.add(showGraphButton);
		
		/**
		JButton showEditCorrectionButton = new JButton("edit correction");
		showEditCorrectionButton.setBounds(620, 0, 120, 25);
		showEditCorrectionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = EvalDatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogEditCorrection(
						rootFrame, 
						stackInfoStr,
						dataSet);
			}
		});
		basePanel.add(showEditCorrectionButton);
		*/

		return basePanel;
		
	} // end of method
	
	/**
	 * set Pattern analysis
	 */
	private JScrollPane createRulesetControlPanel() {
		
		// create ruleset table
		RulesetTableModel rulesetTableModel = new RulesetTableModel();
		rulesetTable = new JTable(rulesetTableModel);
		
		// set row height and column width
		rulesetTable.setRowHeight(25);
		rulesetTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		rulesetTable.getColumnModel().getColumn(0).setPreferredWidth(60);
		rulesetTable.getColumnModel().getColumn(1).setPreferredWidth(150);
		rulesetTable.getColumnModel().getColumn(2).setPreferredWidth(50);
		rulesetTable.getColumnModel().getColumn(3).setPreferredWidth(100);
		rulesetTable.getColumnModel().getColumn(4).setPreferredWidth(200);
		rulesetTable.getColumnModel().getColumn(5).setPreferredWidth(60);
		rulesetTable.getColumnModel().getColumn(6).setPreferredWidth(140);
		rulesetTable.getColumnModel().getColumn(7).setPreferredWidth(100);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
		leftRenderer.setHorizontalAlignment( JLabel.LEFT );
		
		rulesetTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		rulesetTable.getColumnModel().getColumn(1).setCellRenderer( leftRenderer );
		rulesetTable.getColumnModel().getColumn(3).setCellRenderer( leftRenderer );
		rulesetTable.getColumnModel().getColumn(4).setCellRenderer( centerRenderer );
		rulesetTable.getColumnModel().getColumn(5).setCellRenderer( centerRenderer );
		rulesetTable.getColumnModel().getColumn(6).setCellRenderer( centerRenderer );
		rulesetTable.getColumnModel().getColumn(7).setCellRenderer( centerRenderer );
		
		// set table panel
		JScrollPane tablePanel = new JScrollPane(rulesetTable);
		tablePanel.setPreferredSize(new Dimension(600, 130));
		return tablePanel;
		
	} // end of method
	
	/**
	 * apply selected ruleset
	 */
	private void applyRuleset() {
		
		// get selected ruleset
		int selectedRow = this.rulesetTable.getSelectedRow();
		if (selectedRow < 0) {
			JOptionPane.showMessageDialog(
					SwingUtilities.getWindowAncestor(this), 
					"Ruleset target doesn't selected.");
			return;
		}
		RulesetModel selectedRulesetModel = this.rulesetModels.get(selectedRow);
		
		// set EvalDatasetInstance
		EvalDatasetManager datasetManager = EvalDatasetManager.getInstance();
		datasetManager.initDataModel(selectedRulesetModel);
		
		// set outlier panel
		setOutlierPanel();
		
	} // end of method

	/**
	 * Outlier Detection Panel
	 */
	private void setOutlierPanel() {
		
		// clear basePanel
		this.contentsBasePanel.removeAll();
		if (this.datasetManager.getPatternCount() == 0) {
			this.contentsBasePanel.updateUI();
			return;
		}
		
		// create Button control panel
		JPanel controlBasePanel = setOutlierDetectionControlPanel();
		
		// create pattern selection panel
		JScrollPane patternSelectionBasePanel = 
				new JScrollPane(setOutlierDetectionPatternSelectionPanel());
		
		// create detection Panel
		JPanel patternLabelPanel = new JPanel(null);
		patternLabelPanel.setPreferredSize(new Dimension(400, 30));
		
		JLabel patternLabel = new JLabel("Selected Pattern : ");
		patternLabel.setBounds(10, 5, 120, 20);
		patternLabelPanel.add(patternLabel);
		
		this.selectedPatternLabel = new JLabel();
		//this.selectedPatternLabel.setHorizontalAlignment(SwingConstants.LEFT);
		this.selectedPatternLabel.setBounds(130, 5, 300, 20);
		patternLabelPanel.add(this.selectedPatternLabel);
		
		this.outlierDetectionPanel = new JTabbedPane();

		JPanel detectionBasePanel = new JPanel(new BorderLayout());
		detectionBasePanel.add(patternLabelPanel, BorderLayout.NORTH);
		detectionBasePanel.add(this.outlierDetectionPanel, BorderLayout.CENTER);
		
		// Create basePanel
		JSplitPane outlierDetectionBasePanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		outlierDetectionBasePanel.setDividerLocation(180);
		outlierDetectionBasePanel.setDividerSize(5);
		
		outlierDetectionBasePanel.add(patternSelectionBasePanel);
		outlierDetectionBasePanel.add(detectionBasePanel);

		// Set Layout
		this.contentsBasePanel.add(controlBasePanel, BorderLayout.NORTH);
		this.contentsBasePanel.add(outlierDetectionBasePanel, BorderLayout.CENTER);
		this.contentsBasePanel.updateUI();
		
	} // end of method
	
	/**
	 * Set Outlier Detection Control Panel
	 */
	private JPanel setOutlierDetectionControlPanel() {
		
		// create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(600, 60));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("outlier ruleset control"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));

		
		// Add Control Button
		JButton showPatternButton = new JButton("show Pattern result");
		showPatternButton.setBounds(20, 25, 150, 25);
		showPatternButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showPatternResult();
			}
		});
		basePanel.add(showPatternButton);
		
		JButton setDetionThresholdButton = new JButton("set weighting factor");
		setDetionThresholdButton.setBounds(190, 25, 150, 25);
		setDetionThresholdButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setWeightingFactor();
			}
		});
		basePanel.add(setDetionThresholdButton);

		JButton showResultButton = new JButton("show outlier result");
		showResultButton.setBounds(360, 25, 150, 25);
		showResultButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showOutlierDetectionResult();
			}
		});
		basePanel.add(showResultButton);

		//return
		return basePanel;
		
		
	} // end of method
	
	/**
	 * set Outlier Detection Pattern Selection Panel
	 */
	private void setOutlierDetectionPanel(DetectionPatternDataset patternData) {
		
		// remove all previous component
		this.outlierDetectionPanel.removeAll();
		
		// set Pattern Label
		String patternName = String.format(
				"Pattern ID [ %d ]  Pattern Name [ %s ]", 
				patternData.getPatternId(), 
				patternData.getPatternName());
		this.selectedPatternLabel.setText(patternName);
		
		// Add outlier detection panel
		this.outlierDetectionPanel.add("Multivariate checking",  new UI_Outlier_PCA(patternData));
		this.outlierDetectionPanel.add("Bivariate checking",     new UI_Outlier_CORR(patternData));
		this.outlierDetectionPanel.add("Univariate checking",    new UI_Outlier_SPC(patternData));
		this.outlierDetectionPanel.add("Correction checking",    new UI_Outlier_PD(patternData));
		//this.outlierDetectionPanel.add("Undulation",           new AnalysisPanel_Outlier_UD(patternData));
		//this.outlierDetectionPanel.add("Constant",             new AnalysisPanel_Outlier_CONST(patternData));
		
	} // end of method
	

	/**
	 * set Outliser Detection Panel
	 */
	private JPanel setOutlierDetectionPatternSelectionPanel() {
		
		// Get PatternData
		DetectionPatternDataset[] patternDataArray = this.datasetManager.getAllPatternData();
		Arrays.sort(patternDataArray);
		
		// Get Pattern name
		if (patternDataArray.length == 0) {
			return new JPanel();
		}
		
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(160, 155*patternDataArray.length));
		
		// Add PatternSelection Box
		JPanel patternSelectionBox;
		for (int i=0 ; i<patternDataArray.length ; i++) {
			patternSelectionBox = setPatternSelectionBox(patternDataArray[i]);
			patternSelectionBox.setBounds(0,  155*i, 160, 155);
			basePanel.add(patternSelectionBox);
		}
		
		// return
		return basePanel;
		
	} // end of method
	
	/**
	 * Create Pattern Selection Box
	 */
	private JPanel setPatternSelectionBox(final DetectionPatternDataset patternData) {
		
		// Create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(155, 155));
		basePanel.setBorder(BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("Pattern ID [" + patternData.getPatternId() + " ]"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// create PatternIndex Label
		JLabel label = new JLabel("Pattern Name");
		label.setBounds(10, 20, 140, 20);
		basePanel.add(label);
		
		JTextField textField = new JTextField(patternData.getPatternName());
		textField.setBounds(10, 40, 140, 20);
		basePanel.add(textField);
		
		JCheckBox enableDetectionCheck = new JCheckBox("enable detection");
		enableDetectionCheck.setSelected(patternData.isEnabled());
		enableDetectionCheck.setEnabled(false);
		enableDetectionCheck.setBounds(10, 60, 140, 20);
		basePanel.add(enableDetectionCheck);
		
		enableDetectionCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				patternData.setEnabled(((JCheckBox)e.getSource()).isSelected());
			}
		});
		
		final JCheckBox rulesetCheck = new JCheckBox("ruleset created");
		rulesetCheck.setSelected(patternData.isRulesetCreated());
		rulesetCheck.setEnabled(false);
		rulesetCheck.setBounds(10, 80, 140, 20);
		basePanel.add(rulesetCheck);
		
		rulesetCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rulesetCheck.setSelected(patternData.isRulesetCreated());
			}
		});

		final JCheckBox operateStopCheck = new JCheckBox("operate stop state");
		operateStopCheck.setSelected(patternData.isOperateStop());
		operateStopCheck.setEnabled(false);
		operateStopCheck.setBounds(10, 100, 140, 20);
		basePanel.add(operateStopCheck);
		
		operateStopCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				patternData.setOperateStop(((JCheckBox)e.getSource()).isSelected());
			}
		});

		JButton button = new JButton("execute detection");
		button.setBounds(10, 125, 140, 20);
		basePanel.add(button);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOutlierDetectionPanel(patternData);
			}
		});
		
		//this.patternNameInputHash.put(patternData.getPatternIndex(), textField);
		//this.patternEnableInputHash.put(patternData.getPatternIndex(), enableDetectionCheck);
		//this.patternRulesetInputHash.put(patternData.getPatternIndex(), rulesetCheck);
		
		// return
		return basePanel;
		
	} // end of method
	
	/**
	 * show Pattern analysis result of outlier detection
	 */
	private void showPatternResult() {
		
		JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		TmsDataset dataSet = EvalDatasetManager.getInstance().getTmsDataset();
		String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
				dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
				dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
				dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
				dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
		new DialogPatternResult(rootFrame, stackInfoStr);
		
	} // end of method
	
	/**
	 * set outlier detection weighting factor and threshold
	 */
	private void setWeightingFactor() {
		
		JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		new DialogOutlierDetectionThreshold(rootFrame);
		
	} // end of method

	/**
	 * show result of outlier detection
	 */
	private void showOutlierDetectionResult() {
		
		JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		new DialogOutlierDetectionResult(rootFrame);
		
	} // end of method
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	public class DateLabelFormatter extends AbstractFormatter {

		private static final long serialVersionUID = 1L;
		private String datePattern = "yyyy-MM-dd HH:mm:ss";
		private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

		public Object stringToValue(String text) throws ParseException {
			return dateFormatter.parseObject(text);
		}

		public String valueToString(Object value) throws ParseException {
			if (value != null) {
				Calendar cal = (Calendar) value;
				return dateFormatter.format(cal.getTime());
			}
			return "";
		}
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class RulesetTableModel extends AbstractTableModel {
		
		//----------------------------------------------------------------------
		// Fields
		//----------------------------------------------------------------------
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames = {
			"ID", "name", "apply", "desc", "ruleset base-time", "patterns", "create-time", "user-id"
		};
		
		//----------------------------------------------------------------------
		// Constructor
		//----------------------------------------------------------------------
		public RulesetTableModel() {}
		
		//----------------------------------------------------------------------
		// Implemented method
		//----------------------------------------------------------------------
		public String getColumnName(int col) {
			return columnNames[col];
		} // end of method
		
		public int getRowCount() {
			if (rulesetModels == null) {
				return 0;
			}
			return rulesetModels.size();
			}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			RulesetModel rulesetModel = rulesetModels.get(row);
			if (col == 0) {
				return rulesetModel.getRulesetId();
			} else if (col == 1) {
				return rulesetModel.getRulesetName();
			} else if (col == 2) {
				return (rulesetModel.getRulesetState() == 1 ? Boolean.TRUE : Boolean.FALSE);
			} else if (col == 3) {
				return (rulesetModel.getRulesetDescr());
			} else if  (col == 4) {
				return String.format("%s ~ %s", 
						DateUtil.getSimpleMinStr(rulesetModel.getRulesetStartTime()),
						DateUtil.getSimpleMinStr(rulesetModel.getRulesetEndTime()));
			} else if  (col == 5) {
				return rulesetModel.getRulesetPatternModelCount();
			} else if  (col == 6) {
				return rulesetModel.getCreateTime();
			} else if (col == 7) {
				return rulesetModel.getUserId();
			} else {
				return "";
			}
		}
		
		public boolean isCellEditable(int row, int col) {
			if (col == 2) {
				return false;
			}
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return Integer.class;
			} else if (col == 1) {
				return String.class;
			} else if (col == 2) {
				return Boolean.class;
			} else if (col == 3) {
				return String.class;
			} else if (col == 4) {
				return String.class;
			} else if (col == 5) {
				return String.class;
			} else if (col == 6) {
				return String.class;
			} else if (col == 7) {
				return String.class;
			} else {
				return String.class;
			}
			
		}
		
	} // end of class
	

} // end of class
