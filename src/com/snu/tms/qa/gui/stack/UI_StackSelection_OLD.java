/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.stack;

import com.snu.tms.qa.model.FactoryInfo;
import com.snu.tms.qa.model.InventoryDataManager;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.StackInfo;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;
import javax.swing.table.AbstractTableModel;

/**
 * Left Panel for stack selection
 * 
 */
public class UI_StackSelection_OLD extends JPanel implements ActionListener 
{
	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private JRadioButton  viewByFactory;
	private JRadioButton  viewByClassification;
	
	private JTextField     searchField;
	
	private JTable  class1ListTable;
	private JTable  class2ListTable;
	
	private JTable  stackInfoTable;
	private StackInfo selectedStackInfo;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * constructor
	 */
	public UI_StackSelection_OLD() {
		
		// Set layout
		this.setPreferredSize(new Dimension(400, 700));
		this.setLayout(new BorderLayout());
		
		// Set Classification Selection Panel
		JPanel basePanel_10 = createClassificationPanel();
		JPanel basePanel_11 = createSearchPanel();
		
		// Set stack selection panel
		JSplitPane basePanel_20 = createStackSelectionPanel();
		
		// Set Border and Layout
		JPanel basePanelTop = new JPanel(new BorderLayout());
		
		
		TitledBorder titledBorder_10 = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "select classification type");
		titledBorder_10.setTitleJustification(TitledBorder.LEFT);
		basePanel_10.setBorder(titledBorder_10);

		TitledBorder titledBorder_11 = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "search factory");
		titledBorder_11.setTitleJustification(TitledBorder.LEFT);
		basePanel_11.setBorder(titledBorder_11);

		basePanelTop.add(basePanel_10, BorderLayout.CENTER);
		basePanelTop.add(basePanel_11, BorderLayout.EAST);
		
		TitledBorder titledBorder_20 = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "select analysis target stack");
		titledBorder_20.setTitleJustification(TitledBorder.LEFT);
		basePanel_20.setBorder(titledBorder_20);
		
		this.add(basePanelTop, BorderLayout.NORTH);
		this.add(basePanel_20, BorderLayout.CENTER);
		
		// Set default Select
		this.viewByFactory.setSelected(true);
		updateClass1Table();
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
		String actionCommand = e.getActionCommand();
		if (actionCommand.equals("SortByFactory")) {

			// init textField
			this.searchField.setText("");
			this.searchField.updateUI();
			
			// update table
			updateClass1Table();
			
		} else if (actionCommand.equals("SortByClass")) {

			// init textField
			this.searchField.setText("");
			this.searchField.updateUI();
			
			// update table
			updateClass1Table();
		}
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Set layout for classification rule selection
	 */
	private JPanel createClassificationPanel() {
		
		JPanel basePanel = new JPanel(new GridLayout(0, 1));
		basePanel.setPreferredSize(new Dimension(250, 70));
		
		this.viewByFactory = new JRadioButton("Sorting by factory ID");
		this.viewByFactory.setActionCommand("SortByFactory");
		this.viewByFactory.addActionListener(this);

		this.viewByClassification = new JRadioButton("Sorting by classification");
		this.viewByClassification.setActionCommand("SortByClass");
		this.viewByClassification.addActionListener(this);
		
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(this.viewByFactory);
		buttonGroup.add(this.viewByClassification);
		
		basePanel.add(this.viewByFactory);
		basePanel.add(this.viewByClassification);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * Set layout for classification rule selection
	 */
	private JPanel createSearchPanel() {
		
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(150, 70));
		
		this.searchField = new JTextField();
		searchField.setBounds(10, 20, 130, 20);
		
		JButton button = new JButton("search");
		button.setBounds(50, 42, 90, 18);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateClass1Table(searchField.getText().trim());
			}
		});
		
		basePanel.add(searchField);
		basePanel.add(button);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * Set layout for stack selection
	 */
	private JSplitPane createStackSelectionPanel() {
		
		JSplitPane basePanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		basePanel.setDividerLocation(450);
		basePanel.setDividerSize(5);
		
		// Set Upper Panel (for stack selection)
		JSplitPane upperBasePanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		upperBasePanel.setDividerLocation(200);
		upperBasePanel.setDividerSize(5);
		
		// Set layout for upperPanel
		//
		// Set sorting level 1
		this.class1ListTable = new JTable();
		JScrollPane  class1ListTableScrollPane = new JScrollPane(this.class1ListTable);
		//class1ListTableScrollPane.setPreferredSize(new Dimension(600, 300));
		upperBasePanel.add(class1ListTableScrollPane);
		
		// Set sorting level 1
		this.class2ListTable = new JTable();
		JScrollPane  class2ListTableScrollPane = new JScrollPane(this.class2ListTable);
		//class2ListTableScrollPane.setPreferredSize(new Dimension(600, 100));
		upperBasePanel.add(class2ListTableScrollPane);
		
		// Set selected Stack information table
		this.stackInfoTable = new JTable();
		JScrollPane  stackInfoTableScrollPane = new JScrollPane(this.stackInfoTable);
		//stackInfoTableScrollPane.setPreferredSize(new Dimension(600, 200));
		
		// set layout
		basePanel.add(upperBasePanel);
		basePanel.add(stackInfoTableScrollPane);
		
		return basePanel;
		
	} // end of method
	
	
	/**
	 * Update Class_01 table
	 */
	private void updateClass1Table() {
	
		// Set TableModel
		if (this.viewByFactory.isSelected()) {
			
			// Get all factory info
			FactoryInfo[] factories = InventoryDataManager.getInstance().getAllFactoryInfo();
			if (factories != null) {
				AbstractTableModel tableModel = new FactoryClassTableModel(factories);
				this.class1ListTable.setModel(tableModel);
				this.class1ListTable.getColumnModel().getColumn(1).setPreferredWidth(400);
				this.class1ListTable.setRowHeight(20);
				this.class1ListTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				this.class1ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent  e) {
						updateClass2Table();
					}
				});
			}
			
		} else if (this.viewByClassification.isSelected()) {
			
			// Get all stack classification
			String[] stackClasses = InventoryDataManager.getInstance().getAllStackClass();
			if (stackClasses != null) {
				AbstractTableModel tableModel = new StackClassTableModel(stackClasses);
				this.class1ListTable.setModel(tableModel);
				this.class1ListTable.setRowHeight(20);
				this.class1ListTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				this.class1ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent  e) {
						updateClass2Table();
					}
				});
			}
		}
		
		// updateUI
		this.class1ListTable.updateUI();
		
	} // end of method
	
	/**
	 * Update Class_01 table
	 */
	private void updateClass1Table(String searchText) {
	
		// Set TableModel
		// Get all factory info
		FactoryInfo[] factories = InventoryDataManager.getInstance().getAllFactoryInfo(searchText);
		if (factories != null) {
			AbstractTableModel tableModel = new FactoryClassTableModel(factories);
			this.class1ListTable.setModel(tableModel);
			this.class1ListTable.getColumnModel().getColumn(1).setPreferredWidth(400);
			this.class1ListTable.setRowHeight(20);
			this.class1ListTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			this.class1ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent  e) {
					updateClass2Table();
				}
			});
		}

		// updateUI
		this.class1ListTable.updateUI();
		
	} // end of method
	
	/**
	 * Update Class_02 table
	 */
	private void updateClass2Table() {
	
		// Set TableModel
		if (this.viewByFactory.isSelected()) {
			
			// Get selected Factory code
			int selectedRow = this.class1ListTable.getSelectedRow();
			String factoryCode = (String)this.class1ListTable.getValueAt(selectedRow, 0);
			
			// Get all Stack info
			StackInfo[] stacks = InventoryDataManager.getInstance().getStackInfo(factoryCode);
			if (stacks != null) {
				AbstractTableModel tableModel = new StackTableModel(stacks);
				this.class2ListTable.setModel(tableModel);
				this.class2ListTable.getColumnModel().getColumn(0).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(1).setPreferredWidth(200);
				this.class2ListTable.getColumnModel().getColumn(2).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(3).setPreferredWidth(130);
				this.class2ListTable.setRowHeight(20);
				this.class2ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent  e) {
						updateStackInfoTable();
					}
				});
			} else {
				AbstractTableModel tableModel = new StackTableModel(null);
				this.class2ListTable.setModel(tableModel);
				this.class2ListTable.getColumnModel().getColumn(0).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(1).setPreferredWidth(200);
				this.class2ListTable.getColumnModel().getColumn(2).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(3).setPreferredWidth(130);
				this.class2ListTable.setRowHeight(20);
			}
			
		} else {

			// Get selected Factory code
			int selectedRow = this.class1ListTable.getSelectedRow();
			String stackClassName = (String)this.class1ListTable.getValueAt(selectedRow, 0);
			
			// Get all Stack info
			StackInfo[] stacks = InventoryDataManager.getInstance().getAllStackInfo(stackClassName);
			if (stacks != null) {
				AbstractTableModel tableModel = new StackTableModel(stacks);
				this.class2ListTable.setModel(tableModel);
				this.class2ListTable.getColumnModel().getColumn(0).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(1).setPreferredWidth(200);
				this.class2ListTable.getColumnModel().getColumn(2).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(3).setPreferredWidth(130);
				this.class2ListTable.setRowHeight(20);
				this.class2ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent  e) {
						updateStackInfoTable();
					}
				});
			} else {
				AbstractTableModel tableModel = new StackTableModel(stacks);
				this.class2ListTable.setModel(tableModel);
				this.class2ListTable.getColumnModel().getColumn(0).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(1).setPreferredWidth(200);
				this.class2ListTable.getColumnModel().getColumn(2).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(3).setPreferredWidth(130);
				this.class2ListTable.setRowHeight(20);
			}
		}
		
		// updateUI
		this.class2ListTable.updateUI();
		
		// Update StackInfoTable
		updateStackInfoTable();
		this.stackInfoTable.updateUI();
		
	} // end of method
	
	
	/**
	 * Update StackInfo table
	 */
	private void updateStackInfoTable() {
		
		// Get selected StackInfo
		InventoryDataManager inventoryDataManager = InventoryDataManager.getInstance();
		int selectedRow = this.class2ListTable.getSelectedRow();
		if (selectedRow >= 0) {
			
			// get selected stackInfo
			String factoryCode = (String)this.class2ListTable.getValueAt(selectedRow, 0);
			int    stackCode = (Integer)this.class2ListTable.getValueAt(selectedRow, 2);
			
			// set selected StackInfo
			this.selectedStackInfo = inventoryDataManager.getStackInfo(factoryCode, stackCode);
			inventoryDataManager.setSelectedStackInfo(this.selectedStackInfo);
			
		} else {
			inventoryDataManager.setSelectedStackInfo(null);
		}
		
		// Set TableModel
		AbstractTableModel tableModel = new StackInfoTableModel(this.selectedStackInfo);
		this.stackInfoTable.setModel(tableModel);
		this.stackInfoTable.getColumnModel().getColumn(0).setPreferredWidth(150);
		this.stackInfoTable.getColumnModel().getColumn(1).setPreferredWidth(350);
		this.stackInfoTable.setRowHeight(20);
		
	} // end of method

	//==========================================================================
	// Inner Class
	//==========================================================================
	class FactoryClassTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames4Factory = {
			"fact_code", "fact_name"
		};
		
		private  FactoryInfo[] factories = null;
		
		public FactoryClassTableModel(FactoryInfo[] factories) {
			this.factories = factories;
		}
		
		public String getColumnName(int col) {
			return columnNames4Factory[col];
		}
		
		public int getRowCount() { 
			if (factories == null) {
				return 0;
			}
			return factories.length; 
		}
		
		public int getColumnCount() { 
			return columnNames4Factory.length; 
		}
		
		public Object getValueAt(int row, int col) {
			if (row < 0) {
				return "";
			}
			
			if (col == 0) {
				return factories[row].getCode();
			} else {
				return factories[row].getFullName();
			}
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class StackClassTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames4StackClass = {
			"facility_type"
		};
		
		private  String[] stackClasses = null;
		
		public StackClassTableModel(String[] stackClasses) {
			this.stackClasses = stackClasses;
		}
		
		public String getColumnName(int col) {
			return columnNames4StackClass[col];
		}
		
		public int getRowCount() { 
			return stackClasses.length; 
			}
		
		public int getColumnCount() { 
			return columnNames4StackClass.length; 
		}
		
		public Object getValueAt(int row, int col) {
			if (row < 0) {
				return "";
			}
			return stackClasses[row];
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class StackTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames4Stack = {
			"fact_code", "fact_name", "stack_code", "stack_name"};
		
		private  StackInfo[] stacks = null;
		
		public StackTableModel(StackInfo[] stacks) {
			this.stacks = stacks;
		}
		
		public String getColumnName(int col) {
			return columnNames4Stack[col];
		}
		
		public int getRowCount() {
			if (stacks == null) {
				return 0;
			}
			return stacks.length; 
			}
		
		public int getColumnCount() { 
			return columnNames4Stack.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return stacks[row].getFactoryInfo().getCode();
			} else if (col == 1) {
				return stacks[row].getFactoryInfo().getFullName();
			} else if (col == 2) {
				return stacks[row].getStackNumber();
			} else if  (col == 3) {
				return stacks[row].getStackName();
			} else {
				return "";
			}
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class StackInfoTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames4StackInfo = {
			"item", "value"
		};
		
		private final String[] rowNames4StackInfo = {
			"factoryCode", "factoryName", "stackNumber", "stackName", "stackClass1", "stackClass2",
			"standardOxygen", "standardMoisture",
			"correction(TSP)", "correction(SOX)", "correction(NOX)", "correction(HCL)", "correction(HF)",
			"correction(NH3)", "correction(CO)", "correction(O2)", "correction(FL1)", "correction(TMP)",
			"correction(TM1)", "correction(TM2)", "correction(TM3)"
		};
		
		private  StackInfo stackInfo = null;
		
		public StackInfoTableModel(StackInfo stackInfo) {
			this.stackInfo = stackInfo;
		}
		
		public String getColumnName(int col) {
			return columnNames4StackInfo[col];
		}
		
		public int getRowCount() {
			if (stackInfo == null) {
				return 0;
			}
			return 21; 
			}
		
		public int getColumnCount() { 
			return columnNames4StackInfo.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return rowNames4StackInfo[row];
			} else {
				switch(row) {
				case 0:
					return stackInfo.getFactoryInfo().getCode();
				case 1:
					return stackInfo.getFactoryInfo().getFullName();
				case 2:
					return stackInfo.getStackNumber();
				case 3:
					return stackInfo.getStackName();
				case 4:
					return stackInfo.getFacilityClass1();
				case 5:
					return stackInfo.getFacilityClass2();
				case 6:
					return "NOT_DEFINE";
				case 7:
					if (stackInfo.getStandardMoisture() == StackInfo.NOT_DEFINE_VALUE) {
						return "NOT_DEFINE";
					} else {
						return String.format("%-5.3f\n", stackInfo.getStandardMoisture());
					}
				case 8:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(ItemCodeDefine.TSP).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.TSP).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.TSP).isMoistureApplied() ? ("ON") : ("OFF") );
				case 9:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(ItemCodeDefine.SOX).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.SOX).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.SOX).isMoistureApplied() ? ("ON") : ("OFF") );
				case 10:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(ItemCodeDefine.NOX).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.NOX).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.NOX).isMoistureApplied() ? ("ON") : ("OFF") );
				case 11:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(ItemCodeDefine.HCL).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.HCL).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.HCL).isMoistureApplied() ? ("ON") : ("OFF") );
				case 12:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(ItemCodeDefine.HF).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.HF).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.HF).isMoistureApplied() ? ("ON") : ("OFF") );
				case 13:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(ItemCodeDefine.NH3).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.NH3).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.NH3).isMoistureApplied() ? ("ON") : ("OFF") );
				case 14:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(ItemCodeDefine.CO).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.CO).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.CO).isMoistureApplied() ? ("ON") : ("OFF") );
				case 15:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(ItemCodeDefine.O2).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.O2).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.O2).isMoistureApplied() ? ("ON") : ("OFF") );
				case 16:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(ItemCodeDefine.FL1).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.FL1).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.FL1).isMoistureApplied() ? ("ON") : ("OFF") );
				case 17:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(ItemCodeDefine.TMP).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.TMP).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.TMP).isMoistureApplied() ? ("ON") : ("OFF") );
				case 18:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(ItemCodeDefine.TM1).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.TM1).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.TM1).isMoistureApplied() ? ("ON") : ("OFF") );
				case 19:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(ItemCodeDefine.TM2).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.TM2).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.TM2).isMoistureApplied() ? ("ON") : ("OFF") );
				case 20:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(ItemCodeDefine.TM3).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.TM3).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(ItemCodeDefine.TM3).isMoistureApplied() ? ("ON") : ("OFF") );
				default:
					return "";
				}
			}
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
} // end of class
