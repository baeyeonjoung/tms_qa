/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.stack;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import com.snu.tms.qa.soap.QueryTmsRecord;
import com.snu.tms.qa.fileio.QueryTmsRecordFromFile;
import com.snu.tms.qa.fileio.TmsDataWriter;
import com.snu.tms.qa.gui.AnalysisBasePanel;
import com.snu.tms.qa.model.FactoryInfo;
import com.snu.tms.qa.model.InventoryDataManager;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.DatasetManager;
import com.snu.tms.qa.util.DateUtil;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.Dialog.ModalityType;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;
import javax.swing.table.AbstractTableModel;


/**
 * Left Panel for stack selection
 * 
 */
public class UI_StackSelectionPanel_OLD extends JPanel implements ActionListener 
{
	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(UI_StackSelectionPanel_OLD.class);
	private static final long serialVersionUID = 1L;
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private JRadioButton  viewByFactory;
	private JRadioButton  viewByClassification;
	
	private JTextField     searchField;
	
	private JTable  class1ListTable;
	private JTable  class2ListTable;
	
	private JTable  stackInfoTable;
	private StackInfo selectedStackInfo;
	
	private JDatePickerImpl startDatePicker;
	private JDatePickerImpl endDatePicker;
	
	private AnalysisBasePanel  analysisBasePanel;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * 
	 */
	public UI_StackSelectionPanel_OLD(AnalysisBasePanel analysisBasePanel) {
		
		this.analysisBasePanel = analysisBasePanel;
		
		// Set layout
		this.setPreferredSize(new Dimension(400, 700));
		this.setLayout(new BorderLayout());
		
		// Set Classification Selection Panel
		JPanel basePanel_10 = createClassificationPanel();
		JPanel basePanel_11 = createSearchPanel();
		
		// Set stack selection panel
		JSplitPane basePanel_20 = createStackSelectionPanel();
		
		// Set Control Panel
		JPanel basePanel_30 = createControlPanel();
		
		// Set Border and Layout
		JPanel basePanelTop = new JPanel(new BorderLayout());
		
		
		TitledBorder titledBorder_10 = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "select classification type");
		titledBorder_10.setTitleJustification(TitledBorder.LEFT);
		basePanel_10.setBorder(titledBorder_10);

		TitledBorder titledBorder_11 = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "search factory");
		titledBorder_11.setTitleJustification(TitledBorder.LEFT);
		basePanel_11.setBorder(titledBorder_11);

		basePanelTop.add(basePanel_10, BorderLayout.CENTER);
		basePanelTop.add(basePanel_11, BorderLayout.EAST);
		
		TitledBorder titledBorder_20 = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "select analysis target stack");
		titledBorder_20.setTitleJustification(TitledBorder.LEFT);
		basePanel_20.setBorder(titledBorder_20);
		
		TitledBorder titledBorder_30 = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "select date and data loading type");
		titledBorder_30.setTitleJustification(TitledBorder.LEFT);
		basePanel_30.setBorder(titledBorder_30);
		
		this.add(basePanelTop, BorderLayout.NORTH);
		this.add(basePanel_20, BorderLayout.CENTER);
		this.add(basePanel_30, BorderLayout.SOUTH);
		
		// Set default Select
		this.viewByFactory.setSelected(true);
		updateClass1Table();
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
		String actionCommand = e.getActionCommand();
		if (actionCommand.equals("SortByFactory")) {

			// init textField
			this.searchField.setText("");
			this.searchField.updateUI();
			
			// update table
			updateClass1Table();
			
		} else if (actionCommand.equals("SortByClass")) {

			// init textField
			this.searchField.setText("");
			this.searchField.updateUI();
			
			// update table
			updateClass1Table();

		} else if (actionCommand.equals("applyAndSave")) {
			
			// Get Selected StackInfo
			StackInfo stackInfo = this.selectedStackInfo;
			
			// Get Start Time
			int sYear  = this.startDatePicker.getModel().getYear();
			int sMonth = this.startDatePicker.getModel().getMonth();
			int sDay   = this.startDatePicker.getModel().getDay();
			String sDateStr = DateUtil.getDateStr(sYear, sMonth, sDay);
			
			// Get Start Time
			int eYear  = this.endDatePicker.getModel().getYear();
			int eMonth = this.endDatePicker.getModel().getMonth();
			int eDay   = this.endDatePicker.getModel().getDay();
			String eDateStr = DateUtil.getDateStr(eYear, eMonth, eDay);
			
			// Check StackInfo and TimeSet
			if (stackInfo == null) {
				JOptionPane.showMessageDialog(
						SwingUtilities.getWindowAncestor(this), 
						"Analysis target stack is not valid.");
			} else if (eDateStr.compareTo(sDateStr) <= 0) {
				JOptionPane.showMessageDialog(
						SwingUtilities.getWindowAncestor(this), 
						"Analysis target time is not valid.");
			} else {
				// Show Dialog and query start
				showWaitMessageDialogAndDoQueryWork(stackInfo, sDateStr, eDateStr, true);
				// show AnalysisPanel
				this.analysisBasePanel.initUI();
			}	
			
		} else if (actionCommand.equals("apply")) {
			
			// Get Selected StackInfo
			StackInfo stackInfo = this.selectedStackInfo;
			
			// Get Start Time
			int sYear  = this.startDatePicker.getModel().getYear();
			int sMonth = this.startDatePicker.getModel().getMonth();
			int sDay   = this.startDatePicker.getModel().getDay();
			String sDateStr = DateUtil.getDateStr(sYear, sMonth, sDay);
			
			// Get Start Time
			int eYear  = this.endDatePicker.getModel().getYear();
			int eMonth = this.endDatePicker.getModel().getMonth();
			int eDay   = this.endDatePicker.getModel().getDay();
			String eDateStr = DateUtil.getDateStr(eYear, eMonth, eDay);
			
			// Check StackInfo and TimeSet
			if (stackInfo == null) {
				JOptionPane.showMessageDialog(
						SwingUtilities.getWindowAncestor(this), 
						"Analysis target stack is not valid.");
			} else if (eDateStr.compareTo(sDateStr) <= 0) {
				JOptionPane.showMessageDialog(
						SwingUtilities.getWindowAncestor(this), 
						"Analysis target time is not valid.");
			} else {
				// Show Dialog and query start
				showWaitMessageDialogAndDoQueryWork(stackInfo, sDateStr, eDateStr, false);
				// show AnalysisPanel
				this.analysisBasePanel.initUI();
			}
			
			// logging
			if (stackInfo != null) {
				logger.debug("Selected stack changed : \n" + stackInfo.toString());
			}
			
		}
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Set layout for classification rule selection
	 */
	private JPanel createClassificationPanel() {
		
		JPanel basePanel = new JPanel(new GridLayout(0, 1));
		basePanel.setPreferredSize(new Dimension(250, 70));
		
		this.viewByFactory = new JRadioButton("Sorting by factory ID");
		this.viewByFactory.setActionCommand("SortByFactory");
		this.viewByFactory.addActionListener(this);

		this.viewByClassification = new JRadioButton("Sorting by classification");
		this.viewByClassification.setActionCommand("SortByClass");
		this.viewByClassification.addActionListener(this);
		
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(this.viewByFactory);
		buttonGroup.add(this.viewByClassification);
		
		basePanel.add(this.viewByFactory);
		basePanel.add(this.viewByClassification);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * Set layout for classification rule selection
	 */
	private JPanel createSearchPanel() {
		
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(150, 70));
		
		this.searchField = new JTextField();
		searchField.setBounds(10, 20, 130, 20);
		
		JButton button = new JButton("search");
		button.setBounds(50, 42, 90, 18);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateClass1Table(searchField.getText().trim());
			}
		});
		
		basePanel.add(searchField);
		basePanel.add(button);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * Set layout for stack selection
	 */
	private JSplitPane createStackSelectionPanel() {
		
		JSplitPane basePanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		basePanel.setDividerLocation(450);
		basePanel.setDividerSize(5);
		
		// Set Upper Panel (for stack selection)
		JSplitPane upperBasePanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		upperBasePanel.setDividerLocation(200);
		upperBasePanel.setDividerSize(5);
		
		// Set layout for upperPanel
		//
		// Set sorting level 1
		this.class1ListTable = new JTable();
		JScrollPane  class1ListTableScrollPane = new JScrollPane(this.class1ListTable);
		//class1ListTableScrollPane.setPreferredSize(new Dimension(600, 300));
		upperBasePanel.add(class1ListTableScrollPane);
		
		// Set sorting level 1
		this.class2ListTable = new JTable();
		JScrollPane  class2ListTableScrollPane = new JScrollPane(this.class2ListTable);
		//class2ListTableScrollPane.setPreferredSize(new Dimension(600, 100));
		upperBasePanel.add(class2ListTableScrollPane);
		
		// Set selected Stack information table
		this.stackInfoTable = new JTable();
		JScrollPane  stackInfoTableScrollPane = new JScrollPane(this.stackInfoTable);
		//stackInfoTableScrollPane.setPreferredSize(new Dimension(600, 200));
		
		// set layout
		basePanel.add(upperBasePanel);
		basePanel.add(stackInfoTableScrollPane);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * Set layout for control button
	 */
	private JPanel createControlPanel() {
		
		// Set target date
		JPanel dateSetPanel = new JPanel(new GridLayout(1, 0));

		// Set start Date
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");

		UtilDateModel startDateModel = new UtilDateModel();
		startDateModel.setDate(2014, 0, 1);
		startDateModel.setSelected(true);
		JDatePanelImpl startDatePanel = new JDatePanelImpl(startDateModel, p);
		startDatePanel.updateUI();
		startDatePanel.setShowYearButtons(true);
		this.startDatePicker = new JDatePickerImpl(startDatePanel, new DateLabelFormatter());
		dateSetPanel.add(this.startDatePicker);

		// Set end Date
		UtilDateModel endDateModel = new UtilDateModel();
		endDateModel.setDate(2015, 0, 1);
		endDateModel.setSelected(true);
		JDatePanelImpl endDatePanel = new JDatePanelImpl(endDateModel, p);
		endDatePanel.setShowYearButtons(true);
		this.endDatePicker = new JDatePickerImpl(endDatePanel, new DateLabelFormatter());
		dateSetPanel.add(this.endDatePicker);
		
		
		// Set Apply Button
		JPanel controlButtonPanel = new JPanel(new GridLayout(1, 0));
		//controlButtonPanel.setPreferredSize(new Dimension(400, 25));
		controlButtonPanel.setBorder(BorderFactory.createEtchedBorder());
		
		
		JButton applyAndSaveButton = new JButton("apply & save");
		applyAndSaveButton.setActionCommand("applyAndSave");
		applyAndSaveButton.addActionListener(this);
		controlButtonPanel.add(applyAndSaveButton);
		
		JButton applyButton = new JButton("apply");
		applyButton.setActionCommand("apply");
		applyButton.addActionListener(this);
		controlButtonPanel.add(applyButton);
		
		// Set layout
		JPanel basePanel = new JPanel(new GridLayout(0, 1));
		basePanel.setPreferredSize(new Dimension(400, 100));
		
		basePanel.add(dateSetPanel);
		basePanel.add(controlButtonPanel);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * Update Class_01 table
	 */
	private void updateClass1Table() {
	
		// Set TableModel
		if (this.viewByFactory.isSelected()) {
			
			// Get all factory info
			FactoryInfo[] factories = InventoryDataManager.getInstance().getAllFactoryInfo();
			if (factories != null) {
				AbstractTableModel tableModel = new FactoryClassTableModel(factories);
				this.class1ListTable.setModel(tableModel);
				this.class1ListTable.getColumnModel().getColumn(1).setPreferredWidth(400);
				this.class1ListTable.setRowHeight(20);
				this.class1ListTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				this.class1ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent  e) {
						updateClass2Table();
					}
				});
			}
			
		} else if (this.viewByClassification.isSelected()) {
			
			// Get all stack classification
			String[] stackClasses = InventoryDataManager.getInstance().getAllStackClass();
			if (stackClasses != null) {
				AbstractTableModel tableModel = new StackClassTableModel(stackClasses);
				this.class1ListTable.setModel(tableModel);
				this.class1ListTable.setRowHeight(20);
				this.class1ListTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				this.class1ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent  e) {
						updateClass2Table();
					}
				});
			}
		}
		
		// updateUI
		this.class1ListTable.updateUI();
		
	} // end of method
	
	/**
	 * Update Class_01 table
	 */
	private void updateClass1Table(String searchText) {
	
		// Set TableModel
		// Get all factory info
		FactoryInfo[] factories = InventoryDataManager.getInstance().getAllFactoryInfo(searchText);
		if (factories != null) {
			AbstractTableModel tableModel = new FactoryClassTableModel(factories);
			this.class1ListTable.setModel(tableModel);
			this.class1ListTable.getColumnModel().getColumn(1).setPreferredWidth(400);
			this.class1ListTable.setRowHeight(20);
			this.class1ListTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			this.class1ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent  e) {
					updateClass2Table();
				}
			});
		}

		// updateUI
		this.class1ListTable.updateUI();
		
	} // end of method
	
	/**
	 * Update Class_02 table
	 */
	private void updateClass2Table() {
	
		// Set TableModel
		if (this.viewByFactory.isSelected()) {
			
			// Get selected Factory code
			int selectedRow = this.class1ListTable.getSelectedRow();
			String factoryCode = (String)this.class1ListTable.getValueAt(selectedRow, 0);
			
			// Get all Stack info
			StackInfo[] stacks = InventoryDataManager.getInstance().getStackInfo(factoryCode);
			if (stacks != null) {
				AbstractTableModel tableModel = new StackTableModel(stacks);
				this.class2ListTable.setModel(tableModel);
				this.class2ListTable.getColumnModel().getColumn(0).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(1).setPreferredWidth(200);
				this.class2ListTable.getColumnModel().getColumn(2).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(3).setPreferredWidth(130);
				this.class2ListTable.getColumnModel().getColumn(4).setPreferredWidth(130);
				this.class2ListTable.setRowHeight(20);
				this.class2ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent  e) {
						updateStackInfoTable();
					}
				});
			} else {
				AbstractTableModel tableModel = new StackTableModel(null);
				this.class2ListTable.setModel(tableModel);
				this.class2ListTable.getColumnModel().getColumn(0).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(1).setPreferredWidth(200);
				this.class2ListTable.getColumnModel().getColumn(2).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(3).setPreferredWidth(130);
				this.class2ListTable.getColumnModel().getColumn(4).setPreferredWidth(130);
				this.class2ListTable.setRowHeight(20);
			}
			
		} else {

			// Get selected Factory code
			int selectedRow = this.class1ListTable.getSelectedRow();
			String stackClassName = (String)this.class1ListTable.getValueAt(selectedRow, 0);
			
			// Get all Stack info
			StackInfo[] stacks = InventoryDataManager.getInstance().getAllStackInfo(stackClassName);
			if (stacks != null) {
				AbstractTableModel tableModel = new StackTableModel(stacks);
				this.class2ListTable.setModel(tableModel);
				this.class2ListTable.getColumnModel().getColumn(0).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(1).setPreferredWidth(200);
				this.class2ListTable.getColumnModel().getColumn(2).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(3).setPreferredWidth(130);
				this.class2ListTable.getColumnModel().getColumn(4).setPreferredWidth(130);
				this.class2ListTable.setRowHeight(20);
				this.class2ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent  e) {
						updateStackInfoTable();
					}
				});
			} else {
				AbstractTableModel tableModel = new StackTableModel(stacks);
				this.class2ListTable.setModel(tableModel);
				this.class2ListTable.getColumnModel().getColumn(0).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(1).setPreferredWidth(200);
				this.class2ListTable.getColumnModel().getColumn(2).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(3).setPreferredWidth(130);
				this.class2ListTable.getColumnModel().getColumn(4).setPreferredWidth(130);
				this.class2ListTable.setRowHeight(20);
			}
		}
		
		// updateUI
		this.class2ListTable.updateUI();
		
		// Update StackInfoTable
		updateStackInfoTable();
		this.stackInfoTable.updateUI();
		
	} // end of method
	
	
	/**
	 * Update StackInfo table
	 */
	private void updateStackInfoTable() {
		
		// Get selected StackInfo
		int selectedRow = this.class2ListTable.getSelectedRow();
		if (selectedRow >= 0) {
			String factoryCode = (String)this.class2ListTable.getValueAt(selectedRow, 0);
			int    stackCode = (Integer)this.class2ListTable.getValueAt(selectedRow, 2);
			this.selectedStackInfo = InventoryDataManager.getInstance().getStackInfo(factoryCode, stackCode);
		} else {
			this.selectedStackInfo = null;
		}
		
		// Set TableModel
		AbstractTableModel tableModel = new StackInfoTableModel(this.selectedStackInfo);
		this.stackInfoTable.setModel(tableModel);
		this.stackInfoTable.getColumnModel().getColumn(0).setPreferredWidth(150);
		this.stackInfoTable.getColumnModel().getColumn(1).setPreferredWidth(350);
		this.stackInfoTable.setRowHeight(20);
		
	} // end of method
	
	
	/**
	 *
	 */
	private void showWaitMessageDialogAndDoQueryWork(
			final StackInfo stackInfo, 
			final String sTimeStr, 
			final String eTimeStr, 
			final boolean fileSave) {
		
		// Make SwingWorker
		SwingWorker<Void, Void> threadSwingWorker = new SwingWorker<Void, Void>() {

			@Override
			protected Void doInBackground() throws Exception {
				
				// Prepare DataSetInstnace
				DatasetManager tmsDataSetInstance = DatasetManager.getInstance();
				tmsDataSetInstance.clear();
				
				// prepare TmsDataSet
				TmsDataset tmsDataSet = null;
				
				// search file
				String targetFilePath = searchFile(
						"./clone_data", 
						stackInfo.getFactoryInfo().getCode(), 
						stackInfo.getStackNumber(), 
						sTimeStr.substring(0, 10), 
						eTimeStr.substring(0, 10));
				if (targetFilePath == null) {
					targetFilePath = "*";
				}
				
				// Firstly, Search data file from "./clone_data/"
				File dataFile = new File(targetFilePath);
				if (dataFile.exists()) {
					logger.debug("Target file exist. [" + targetFilePath + "]");
					tmsDataSet = QueryTmsRecordFromFile.readTmsRecord(
							targetFilePath, 
							stackInfo.getFactoryInfo().getCode(), 
							stackInfo.getStackNumber(), 
							sTimeStr, 
							eTimeStr);
					tmsDataSet.getTmsDataHeader().setFactoryInfo(stackInfo.getFactoryInfo());
					tmsDataSet.getTmsDataHeader().setStackInfo(stackInfo);
				} 
				// Secondly, query from database
				else {
					logger.debug("Target file doesn't exist. So we try db query.");
					tmsDataSet = QueryTmsRecord.queryTmsRecord(
							stackInfo.getFactoryInfo().getCode(), 
							stackInfo.getStackNumber(), 
							sTimeStr, 
							eTimeStr);
					tmsDataSet.getTmsDataHeader().setFactoryInfo(stackInfo.getFactoryInfo());
					tmsDataSet.getTmsDataHeader().setStackInfo(stackInfo);
				}
				
				// Set Data
				tmsDataSetInstance.setTmsDataset(tmsDataSet);
				
				// File Save
				if (!dataFile.exists() && fileSave) {
					String fileName = "./clone_data/tms_data_" + 
							stackInfo.getFactoryInfo().getCode() + "_" + 
							stackInfo.getStackNumber() + "_" + 
							sTimeStr.substring(0, 10) + "_" + eTimeStr.substring(0, 10) + ".txt";
					TmsDataWriter.writeDataFile(fileName, tmsDataSet);
				}

				// return
				return null;
			}
		};
		
		// Create JDialog for show please wait message
		Window win = SwingUtilities.getWindowAncestor(this);
		final JDialog dialog = new JDialog(win, "Information message", ModalityType.APPLICATION_MODAL);
		
		// Add SwingWorker Listener
		threadSwingWorker.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state")) {
					if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
						dialog.dispose();
					}
				}
			}
		});
		
		// Execute SwingWorker
		threadSwingWorker.execute();
		
		// Show ProgressBar
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		JPanel panel = new JPanel(new BorderLayout());
		panel.setPreferredSize(new Dimension(400, 80));
		panel.add(progressBar, BorderLayout.CENTER);
		JLabel messageLabel = new JLabel("  Database query job take serveral minutes.");
		messageLabel.setPreferredSize(new Dimension(300, 40));
		panel.add(messageLabel, BorderLayout.PAGE_START);
		dialog.add(panel);
		dialog.pack();
		dialog.setLocationRelativeTo(win);
		dialog.setVisible(true);
		
	} // end of method
	
	/**
	 * search file
	 */
	private String searchFile(
			String   folderPath,
			String   factCode,
			int      stckCode,
			String   sTimeStr,
			String   eTimeStr
			) {
		
		try {
			// set target file name
			String targetFileName = String.format("tms_data_%s_%d_", factCode, stckCode);
			
			// get file name list
			File folder = new File(folderPath);
			File[] fileList = folder.listFiles();
			for (File file : fileList) {
				
				// check file name
				String fileName = file.getName();
				String mFileName = fileName.substring(0, fileName.length()-4);
				if (mFileName.startsWith(targetFileName)) {
					
					// separate with "_"
					String[] nameParts = mFileName.split("_");
					if (nameParts.length < 6) {
						continue;
					}
					
					//System.out.println(nameParts[4] + "  " + sTimeStr + "  " + nameParts[4].compareTo(sTimeStr));
					//System.out.println(nameParts[5] + "  " + eTimeStr + "  " + nameParts[5].compareTo(eTimeStr));
					// check startTime
					if (nameParts[4].compareTo(sTimeStr) > 0) {
						continue;
					}
					
					// check endTime
					if (nameParts[5].compareTo(eTimeStr) < 0) {
						continue;
					}
					return folderPath + "/" + fileName;
					
				}
				
			} // end of for-loop
			
			// return
			return null;
			
		} catch (Exception e) {
			return null;
		}
		
	} // end of method
	

	//==========================================================================
	// Inner Class
	//==========================================================================
	class FactoryClassTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames4Factory = {
			"fact_code", "fact_name"
		};
		
		private  FactoryInfo[] factories = null;
		
		public FactoryClassTableModel(FactoryInfo[] factories) {
			this.factories = factories;
		}
		
		public String getColumnName(int col) {
			return columnNames4Factory[col];
		}
		
		public int getRowCount() { 
			if (factories == null) {
				return 0;
			}
			return factories.length; 
		}
		
		public int getColumnCount() { 
			return columnNames4Factory.length; 
		}
		
		public Object getValueAt(int row, int col) {
			if (row < 0) {
				return "";
			}
			
			if (col == 0) {
				return factories[row].getCode();
			} else {
				return factories[row].getFullName();
			}
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class StackClassTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames4StackClass = {
			"facility_type"
		};
		
		private  String[] stackClasses = null;
		
		public StackClassTableModel(String[] stackClasses) {
			this.stackClasses = stackClasses;
		}
		
		public String getColumnName(int col) {
			return columnNames4StackClass[col];
		}
		
		public int getRowCount() { 
			return stackClasses.length; 
			}
		
		public int getColumnCount() { 
			return columnNames4StackClass.length; 
		}
		
		public Object getValueAt(int row, int col) {
			if (row < 0) {
				return "";
			}
			return stackClasses[row];
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class StackTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames4Stack = {
			"fact_code", "fact_name", "stack_code", "stack_name", "stack_class"
		};
		
		private  StackInfo[] stacks = null;
		
		public StackTableModel(StackInfo[] stacks) {
			this.stacks = stacks;
		}
		
		public String getColumnName(int col) {
			return columnNames4Stack[col];
		}
		
		public int getRowCount() {
			if (stacks == null) {
				return 0;
			}
			return stacks.length; 
			}
		
		public int getColumnCount() { 
			return columnNames4Stack.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return stacks[row].getFactoryInfo().getCode();
			} else if (col == 1) {
				return stacks[row].getFactoryInfo().getFullName();
			} else if (col == 2) {
				return stacks[row].getStackNumber();
			} else if  (col == 3) {
				return stacks[row].getStackName();
			} else {
				return stacks[row].getFacilityClass2();
			}
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class StackInfoTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames4StackInfo = {
			"item", "value"
		};
		
		private final String[] rowNames4StackInfo = {
			"factoryCode", "factoryName", "stackNumber", "stackName", "stackClass1", "stackClass2",
			"standardOxygen", "standardMoisture",
			"correction(TSP)", "correction(SO2)", "correction(NOX)", "correction(HCL)", "correction(HF)",
			"correction(NH3)", "correction(CO)", "correction(O2)", "correction(FLW)", "correction(TMP)",
			"correction(TM1)", "correction(TM2)", "correction(TM3)"
		};
		
		private  StackInfo stackInfo = null;
		
		public StackInfoTableModel(StackInfo stackInfo) {
			this.stackInfo = stackInfo;
		}
		
		public String getColumnName(int col) {
			return columnNames4StackInfo[col];
		}
		
		public int getRowCount() {
			if (stackInfo == null) {
				return 0;
			}
			return 21; 
			}
		
		public int getColumnCount() { 
			return columnNames4StackInfo.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return rowNames4StackInfo[row];
			} else {
				switch(row) {
				case 0:
					return stackInfo.getFactoryInfo().getCode();
				case 1:
					return stackInfo.getFactoryInfo().getFullName();
				case 2:
					return stackInfo.getStackNumber();
				case 3:
					return stackInfo.getStackName();
				case 4:
					return stackInfo.getFacilityClass1();
				case 5:
					return stackInfo.getFacilityClass2();
				case 6:
					/**
					if (stackInfo.getStandardOxygen() == StackInfo.NOT_DEFINE_VALUE) {
						return "NOT_DEFINE";
					} else {
						return String.format("%-5.3f\n", stackInfo.getStandardOxygen());
					}
					*/
					return "NOT_DEFINE";
				case 7:
					if (stackInfo.getStandardMoisture() == StackInfo.NOT_DEFINE_VALUE) {
						return "NOT_DEFINE";
					} else {
						return String.format("%-5.3f\n", stackInfo.getStandardMoisture());
					}
				case 8:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(1).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(1).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(1).isMoistureApplied() ? ("ON") : ("OFF") );
				case 9:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(2).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(2).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(2).isMoistureApplied() ? ("ON") : ("OFF") );
				case 10:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(3).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(3).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(3).isMoistureApplied() ? ("ON") : ("OFF") );
				case 11:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(4).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(4).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(4).isMoistureApplied() ? ("ON") : ("OFF") );
				case 12:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(5).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(5).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(5).isMoistureApplied() ? ("ON") : ("OFF") );
				case 13:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(6).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(6).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(6).isMoistureApplied() ? ("ON") : ("OFF") );
				case 14:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(7).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(7).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(7).isMoistureApplied() ? ("ON") : ("OFF") );
				case 15:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(8).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(8).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(8).isMoistureApplied() ? ("ON") : ("OFF") );
				case 16:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(9).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(9).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(9).isMoistureApplied() ? ("ON") : ("OFF") );
				case 17:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(10).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(10).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(10).isMoistureApplied() ? ("ON") : ("OFF") );
				case 18:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(11).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(11).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(11).isMoistureApplied() ? ("ON") : ("OFF") );
				case 19:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(12).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(12).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(12).isMoistureApplied() ? ("ON") : ("OFF") );
				case 20:
					return String.format("oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
							stackInfo.getCorrectionInfo(13).isOxygenApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(13).isTemperatureApplied() ? ("ON") : ("OFF"),
							stackInfo.getCorrectionInfo(13).isMoistureApplied() ? ("ON") : ("OFF") );
				default:
					return "";
				}
			}
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	public class DateLabelFormatter extends AbstractFormatter {

		private static final long serialVersionUID = 1L;
		private String datePattern = "yyyy-MM-dd HH:mm:ss";
		private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

		public Object stringToValue(String text) throws ParseException {
			return dateFormatter.parseObject(text);
		}

		public String valueToString(Object value) throws ParseException {
			if (value != null) {
				Calendar cal = (Calendar) value;
				return dateFormatter.format(cal.getTime());
			}
			return "";
		}
	} // end of class

} // end of class
