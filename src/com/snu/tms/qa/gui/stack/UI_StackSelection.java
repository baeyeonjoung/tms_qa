/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.stack;

import com.snu.tms.qa.model.CorrectionItemInfo;
import com.snu.tms.qa.model.ExhaustInfo;
import com.snu.tms.qa.model.FactoryInfo;
import com.snu.tms.qa.model.InventoryDataManager;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.PreventionInfo;
import com.snu.tms.qa.model.StackInfo;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Left Panel for stack selection
 * 
 */
public class UI_StackSelection extends JPanel implements ActionListener 
{
	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private StackInfo selectedStackInfo;

	private JRadioButton  viewByFactory;
	private JRadioButton  viewByClassification;
	
	private JTextField     searchField;
	
	private JTable  class1ListTable;
	private JTable  class2ListTable;
	
	//private JTable  stackInfoTable;
	private JTable  exhaustListTable;
	private JTable  preventionListTable;
	private JTable  correctionInfoTable;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * constructor
	 */
	public UI_StackSelection() {
		
		// Set layout
		this.setPreferredSize(new Dimension(400, 700));
		this.setLayout(new BorderLayout());
		
		// Set Classification Selection Panel
		JPanel basePanel_10 = createClassificationPanel();
		JPanel basePanel_11 = createSearchPanel();
		
		// Set stack selection panel
		JSplitPane basePanel_20 = createStackSelectionPanel();
		
		// Set Border and Layout
		JPanel basePanelTop = new JPanel(new BorderLayout());
		
		
		TitledBorder titledBorder_10 = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "select classification type");
		titledBorder_10.setTitleJustification(TitledBorder.LEFT);
		basePanel_10.setBorder(titledBorder_10);

		TitledBorder titledBorder_11 = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "search factory");
		titledBorder_11.setTitleJustification(TitledBorder.LEFT);
		basePanel_11.setBorder(titledBorder_11);

		basePanelTop.add(basePanel_10, BorderLayout.CENTER);
		basePanelTop.add(basePanel_11, BorderLayout.EAST);
		
		TitledBorder titledBorder_20 = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "select analysis target stack");
		titledBorder_20.setTitleJustification(TitledBorder.LEFT);
		basePanel_20.setBorder(titledBorder_20);
		
		this.add(basePanelTop, BorderLayout.NORTH);
		this.add(basePanel_20, BorderLayout.CENTER);
		
		// Set default Select
		this.viewByFactory.setSelected(true);
		updateClass1Table();
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
		String actionCommand = e.getActionCommand();
		if (actionCommand.equals("SortByFactory")) {

			// init textField
			this.searchField.setText("");
			this.searchField.updateUI();
			
			// update table
			updateClass1Table();
			
		} else if (actionCommand.equals("SortByClass")) {

			// init textField
			this.searchField.setText("");
			this.searchField.updateUI();
			
			// update table
			updateClass1Table();
		}
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Set layout for classification rule selection
	 */
	private JPanel createClassificationPanel() {
		
		JPanel basePanel = new JPanel(new GridLayout(0, 1));
		basePanel.setPreferredSize(new Dimension(250, 70));
		
		this.viewByFactory = new JRadioButton("Sorting by factory ID");
		this.viewByFactory.setActionCommand("SortByFactory");
		this.viewByFactory.addActionListener(this);

		this.viewByClassification = new JRadioButton("Sorting by classification");
		this.viewByClassification.setActionCommand("SortByClass");
		this.viewByClassification.addActionListener(this);
		
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(this.viewByFactory);
		buttonGroup.add(this.viewByClassification);
		
		basePanel.add(this.viewByFactory);
		basePanel.add(this.viewByClassification);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * Set layout for classification rule selection
	 */
	private JPanel createSearchPanel() {
		
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(150, 70));
		
		this.searchField = new JTextField();
		searchField.setBounds(10, 20, 130, 20);
		
		// add key listener
		searchField.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent event) {
                int keyCode=event.getKeyCode();
                if (keyCode == KeyEvent.VK_ENTER) {
                	updateClass1Table(searchField.getText().trim());
                }
            }
		});
		
		// set search button
		JButton button = new JButton("search");
		button.setBounds(50, 42, 90, 18);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateClass1Table(searchField.getText().trim());
			}
		});
		
		basePanel.add(searchField);
		basePanel.add(button);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * Set layout for stack selection
	 */
	private JSplitPane createStackSelectionPanel() {
		
		JSplitPane basePanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		basePanel.setDividerLocation(350);
		basePanel.setDividerSize(5);
		
		// Set Upper Panel (for stack selection)
		JSplitPane upperBasePanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		upperBasePanel.setDividerLocation(170);
		upperBasePanel.setDividerSize(5);
		
		// Set layout for upperPanel
		//
		// Set sorting level 1
		this.class1ListTable = new JTable();
		JScrollPane  class1ListTableScrollPane = new JScrollPane(this.class1ListTable);
		//class1ListTableScrollPane.setPreferredSize(new Dimension(600, 300));
		upperBasePanel.add(class1ListTableScrollPane);
		
		// Set sorting level 1
		this.class2ListTable = new JTable();
		JScrollPane  class2ListTableScrollPane = new JScrollPane(this.class2ListTable);
		//class2ListTableScrollPane.setPreferredSize(new Dimension(600, 100));
		upperBasePanel.add(class2ListTableScrollPane);
		
		// Set selected Stack information table
		//this.stackInfoTable = new JTable();
		//JScrollPane  stackInfoTableScrollPane = new JScrollPane(this.stackInfoTable);
		JPanel selectedInfoPanel = new JPanel(new BorderLayout());
		JPanel selectedInfoUpperPanel = new JPanel(new BorderLayout());
		JPanel selectedInfoLowerPanel = new JPanel(new BorderLayout());
		
		this.exhaustListTable = new JTable();
		this.preventionListTable = new JTable();
		this.correctionInfoTable = new JTable();
		initStackInfoTable();
		
		JScrollPane ehaustInfoPanel     = new JScrollPane(this.exhaustListTable);
		JScrollPane preventionInfoPanel = new JScrollPane(this.preventionListTable);
		ehaustInfoPanel.setPreferredSize(new Dimension(600, 100));
		preventionInfoPanel.setPreferredSize(new Dimension(600, 100));
		
		selectedInfoUpperPanel.add(ehaustInfoPanel, BorderLayout.NORTH);
		selectedInfoUpperPanel.add(preventionInfoPanel, BorderLayout.CENTER);
		selectedInfoLowerPanel.add(new JScrollPane(this.correctionInfoTable), BorderLayout.CENTER);
		
		selectedInfoUpperPanel.setPreferredSize(new Dimension(600, 200));
		selectedInfoLowerPanel.setPreferredSize(new Dimension(600, 250));
		selectedInfoPanel.add(selectedInfoUpperPanel, BorderLayout.NORTH);
		selectedInfoPanel.add(selectedInfoLowerPanel, BorderLayout.CENTER);
		
		// set layout
		basePanel.add(upperBasePanel);
		basePanel.add(selectedInfoPanel);
		
		return basePanel;
		
	} // end of method
	
	
	/**
	 * Update Class_01 table
	 */
	private void updateClass1Table() {
	
		// Set TableModel
		if (this.viewByFactory.isSelected()) {
			
			// Get all factory info
			FactoryInfo[] factories = InventoryDataManager.getInstance().getAllFactoryInfo();
			if (factories != null) {
				AbstractTableModel tableModel = new FactoryClassTableModel(factories);
				this.class1ListTable.setModel(tableModel);
				this.class1ListTable.getColumnModel().getColumn(1).setPreferredWidth(400);
				this.class1ListTable.setRowHeight(20);
				this.class1ListTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				this.class1ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent  e) {
						updateClass2Table();
					}
				});
			}
			
		} else if (this.viewByClassification.isSelected()) {
			
			// Get all stack classification
			String[] stackClasses = InventoryDataManager.getInstance().getAllStackClass();
			if (stackClasses != null) {
				AbstractTableModel tableModel = new StackClassTableModel(stackClasses);
				this.class1ListTable.setModel(tableModel);
				this.class1ListTable.setRowHeight(20);
				this.class1ListTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				this.class1ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent  e) {
						updateClass2Table();
					}
				});
			}
		}
		
		// updateUI
		this.class1ListTable.updateUI();
		
	} // end of method
	
	/**
	 * Update Class_01 table
	 */
	private void updateClass1Table(String searchText) {
	
		// Set TableModel
		// Get all factory info
		FactoryInfo[] factories = InventoryDataManager.getInstance().getAllFactoryInfo(searchText);
		if (factories != null) {
			AbstractTableModel tableModel = new FactoryClassTableModel(factories);
			this.class1ListTable.setModel(tableModel);
			this.class1ListTable.getColumnModel().getColumn(1).setPreferredWidth(400);
			this.class1ListTable.setRowHeight(20);
			this.class1ListTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			this.class1ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent  e) {
					updateClass2Table();
				}
			});
		}

		// updateUI
		this.class1ListTable.updateUI();
		
	} // end of method
	
	/**
	 * Update Class_02 table
	 */
	private void updateClass2Table() {
	
		// Set TableModel
		if (this.viewByFactory.isSelected()) {
			
			// Get selected Factory code
			int selectedRow = this.class1ListTable.getSelectedRow();
			String factoryCode = (String)this.class1ListTable.getValueAt(selectedRow, 0);
			
			// Get all Stack info
			StackInfo[] stacks = InventoryDataManager.getInstance().getStackInfo(factoryCode);
			if (stacks != null) {
				AbstractTableModel tableModel = new StackTableModel(stacks);
				this.class2ListTable.setModel(tableModel);
				this.class2ListTable.getColumnModel().getColumn(0).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(1).setPreferredWidth(200);
				this.class2ListTable.getColumnModel().getColumn(2).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(3).setPreferredWidth(130);
				this.class2ListTable.setRowHeight(20);
				this.class2ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent  e) {
						updateStackInfoTable();
					}
				});
			} else {
				AbstractTableModel tableModel = new StackTableModel(null);
				this.class2ListTable.setModel(tableModel);
				this.class2ListTable.getColumnModel().getColumn(0).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(1).setPreferredWidth(200);
				this.class2ListTable.getColumnModel().getColumn(2).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(3).setPreferredWidth(130);
				this.class2ListTable.setRowHeight(20);
			}
			
		} else {

			// Get selected Factory code
			int selectedRow = this.class1ListTable.getSelectedRow();
			String stackClassName = (String)this.class1ListTable.getValueAt(selectedRow, 0);
			
			// Get all Stack info
			StackInfo[] stacks = InventoryDataManager.getInstance().getAllStackInfo(stackClassName);
			if (stacks != null) {
				AbstractTableModel tableModel = new StackTableModel(stacks);
				this.class2ListTable.setModel(tableModel);
				this.class2ListTable.getColumnModel().getColumn(0).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(1).setPreferredWidth(200);
				this.class2ListTable.getColumnModel().getColumn(2).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(3).setPreferredWidth(130);
				this.class2ListTable.setRowHeight(20);
				this.class2ListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent  e) {
						updateStackInfoTable();
					}
				});
			} else {
				AbstractTableModel tableModel = new StackTableModel(stacks);
				this.class2ListTable.setModel(tableModel);
				this.class2ListTable.getColumnModel().getColumn(0).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(1).setPreferredWidth(200);
				this.class2ListTable.getColumnModel().getColumn(2).setPreferredWidth(70);
				this.class2ListTable.getColumnModel().getColumn(3).setPreferredWidth(130);
				this.class2ListTable.setRowHeight(20);
			}
		}
		
		// updateUI
		this.class2ListTable.updateUI();
		
		// Update StackInfoTable
		updateStackInfoTable();
		
	} // end of method
	
	
	/**
	 * init. stack info tables
	 */
	private void initStackInfoTable() {
		
		// prepare cell renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		// set exhaust info table
		this.exhaustListTable = new JTable(new ExhaustListTableModel(null));
		this.exhaustListTable.getColumnModel().getColumn(0).setPreferredWidth(100);
		this.exhaustListTable.getColumnModel().getColumn(1).setPreferredWidth(250);
		this.exhaustListTable.getColumnModel().getColumn(2).setPreferredWidth(250);
		this.exhaustListTable.setRowHeight(20);
		
		this.exhaustListTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.exhaustListTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.exhaustListTable.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		
		// set exhaust info table
		this.preventionListTable = new JTable(new PreventionListTableModel(null));
		this.preventionListTable.getColumnModel().getColumn(0).setPreferredWidth(100);
		this.preventionListTable.getColumnModel().getColumn(1).setPreferredWidth(250);
		this.preventionListTable.getColumnModel().getColumn(2).setPreferredWidth(250);
		this.preventionListTable.setRowHeight(20);

		this.preventionListTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.preventionListTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.preventionListTable.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		
		// set correction info table
		this.correctionInfoTable = new JTable(new CorrectionInfoTableModel(null));
		this.correctionInfoTable.getColumnModel().getColumn(0).setPreferredWidth(100);
		this.correctionInfoTable.getColumnModel().getColumn(1).setPreferredWidth(100);
		this.correctionInfoTable.getColumnModel().getColumn(2).setPreferredWidth(100);
		this.correctionInfoTable.getColumnModel().getColumn(3).setPreferredWidth(100);
		this.correctionInfoTable.getColumnModel().getColumn(4).setPreferredWidth(100);
		this.correctionInfoTable.getColumnModel().getColumn(5).setPreferredWidth(100);
		this.correctionInfoTable.setRowHeight(20);
		
		this.correctionInfoTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.correctionInfoTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.correctionInfoTable.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		
	} // end of method
	
	/**
	 * Update StackInfo table
	 */
	private void updateStackInfoTable() {
		
		// Get selected StackInfo
		InventoryDataManager inventoryDataManager = InventoryDataManager.getInstance();
		int selectedRow = this.class2ListTable.getSelectedRow();
		if (selectedRow >= 0) {
			
			// get selected stackInfo
			String factoryCode = (String)this.class2ListTable.getValueAt(selectedRow, 0);
			int    stackCode = (Integer)this.class2ListTable.getValueAt(selectedRow, 2);
			
			// set selected StackInfo
			this.selectedStackInfo = inventoryDataManager.getStackInfo(factoryCode, stackCode);
			inventoryDataManager.setSelectedStackInfo(this.selectedStackInfo);
			
		} else {
			inventoryDataManager.setSelectedStackInfo(null);
		}
		
		// set exhaust info table
		this.exhaustListTable.setModel(new ExhaustListTableModel(this.selectedStackInfo));
		this.preventionListTable.setModel(new PreventionListTableModel(this.selectedStackInfo));
		this.correctionInfoTable.setModel(new CorrectionInfoTableModel(this.selectedStackInfo));
		
		// prepare cell renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		// set exhaust info table
		this.exhaustListTable.getColumnModel().getColumn(0).setPreferredWidth(100);
		this.exhaustListTable.getColumnModel().getColumn(1).setPreferredWidth(250);
		this.exhaustListTable.getColumnModel().getColumn(2).setPreferredWidth(250);
		this.exhaustListTable.setRowHeight(20);
		
		this.exhaustListTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.exhaustListTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.exhaustListTable.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		
		// set exhaust info table
		this.preventionListTable.getColumnModel().getColumn(0).setPreferredWidth(100);
		this.preventionListTable.getColumnModel().getColumn(1).setPreferredWidth(250);
		this.preventionListTable.getColumnModel().getColumn(2).setPreferredWidth(250);
		this.preventionListTable.setRowHeight(20);

		this.preventionListTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.preventionListTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.preventionListTable.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		
		// set correction info table
		this.correctionInfoTable.getColumnModel().getColumn(0).setPreferredWidth(100);
		this.correctionInfoTable.getColumnModel().getColumn(1).setPreferredWidth(100);
		this.correctionInfoTable.getColumnModel().getColumn(2).setPreferredWidth(100);
		this.correctionInfoTable.getColumnModel().getColumn(3).setPreferredWidth(100);
		this.correctionInfoTable.getColumnModel().getColumn(4).setPreferredWidth(100);
		this.correctionInfoTable.getColumnModel().getColumn(5).setPreferredWidth(100);
		this.correctionInfoTable.setRowHeight(20);
		
		this.correctionInfoTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.correctionInfoTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.correctionInfoTable.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		
		// update UI
		this.exhaustListTable.updateUI();
		this.preventionListTable.updateUI();
		this.correctionInfoTable.updateUI();
		
	} // end of method

	//==========================================================================
	// Inner Class
	//==========================================================================
	class FactoryClassTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames4Factory = {
			"fact_code", "fact_name"
		};
		
		private  FactoryInfo[] factories = null;
		
		public FactoryClassTableModel(FactoryInfo[] factories) {
			this.factories = factories;
		}
		
		public String getColumnName(int col) {
			return columnNames4Factory[col];
		}
		
		public int getRowCount() { 
			if (factories == null) {
				return 0;
			}
			return factories.length; 
		}
		
		public int getColumnCount() { 
			return columnNames4Factory.length; 
		}
		
		public Object getValueAt(int row, int col) {
			if (row < 0) {
				return "";
			}
			
			if (col == 0) {
				return factories[row].getCode();
			} else {
				return factories[row].getFullName();
			}
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class StackClassTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames4StackClass = {
			"facility_type"
		};
		
		private  String[] stackClasses = null;
		
		public StackClassTableModel(String[] stackClasses) {
			this.stackClasses = stackClasses;
		}
		
		public String getColumnName(int col) {
			return columnNames4StackClass[col];
		}
		
		public int getRowCount() { 
			return stackClasses.length; 
			}
		
		public int getColumnCount() { 
			return columnNames4StackClass.length; 
		}
		
		public Object getValueAt(int row, int col) {
			if (row < 0) {
				return "";
			}
			return stackClasses[row];
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class StackTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames4Stack = {
			"fact_code", "fact_name", "stack_code", "stack_name"};
		
		private  StackInfo[] stacks = null;
		
		public StackTableModel(StackInfo[] stacks) {
			this.stacks = stacks;
		}
		
		public String getColumnName(int col) {
			return columnNames4Stack[col];
		}
		
		public int getRowCount() {
			if (stacks == null) {
				return 0;
			}
			return stacks.length; 
			}
		
		public int getColumnCount() { 
			return columnNames4Stack.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return stacks[row].getFactoryInfo().getCode();
			} else if (col == 1) {
				return stacks[row].getFactoryInfo().getFullName();
			} else if (col == 2) {
				return stacks[row].getStackNumber();
			} else if  (col == 3) {
				return stacks[row].getStackName();
			} else {
				return "";
			}
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class ExhaustListTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames = {
			"exhst.code", "exhst.name", "capacity"
		};
		
		private  StackInfo      stackInfo       = null;
		private  ExhaustInfo[]  exhaustInfoList = null;
		
		public ExhaustListTableModel(StackInfo stackInfo) {
			
			this.stackInfo = stackInfo;
			if (this.stackInfo != null) {
				this.exhaustInfoList = this.stackInfo.getAllExhaustInfo();
			} else {
				this.exhaustInfoList = null;
			}
		} // end of method
		
		public String getColumnName(int col) {
			return columnNames[col];
		} // end of method
		
		public int getRowCount() {

			if (this.exhaustInfoList == null) {
				return 0;
			} else {
				return this.exhaustInfoList.length;
			}

		} // end of method
		
		public int getColumnCount() { 
			return columnNames.length; 
		} // end of method
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return exhaustInfoList[row].getExhstCode();
			} else if (col == 1) {
				return exhaustInfoList[row].getExhstName();
			} else if (col == 2) {
				return exhaustInfoList[row].getExhstCapacity();
			} else {
				return "";
			}
			
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class PreventionListTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames = {
			"exhst.code", "prvn.name", "capacity"
		};
		
		private  StackInfo         stackInfo          = null;
		private  PreventionInfo[]  preventionInfoList = null;
		
		public PreventionListTableModel(StackInfo stackInfo) {
			
			this.stackInfo = stackInfo;
			if (this.stackInfo != null) {
				this.preventionInfoList = this.stackInfo.getAllPreventionInfo();
			} else {
				this.preventionInfoList = null;
			}
		} // end of method
		
		public String getColumnName(int col) {
			return columnNames[col];
		} // end of method
		
		public int getRowCount() {

			if (this.preventionInfoList == null) {
				return 0;
			} else {
				return this.preventionInfoList.length;
			}

		} // end of method
		
		public int getColumnCount() { 
			return columnNames.length; 
		} // end of method
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return preventionInfoList[row].getExhstCode();
			} else if (col == 1) {
				return preventionInfoList[row].getPrvnName();
			} else if (col == 2) {
				return preventionInfoList[row].getPrvnCapacity();
			} else {
				return "";
			}
			
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class CorrectionInfoTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames = {
			"item", "std.oxgn", "std.mois", "oxgn", "temp", "mois"
		};
		
		private  StackInfo         stackInfo          = null;
		private  CorrectionItemInfo[]  correctionInfoList = null;
		
		public CorrectionInfoTableModel(StackInfo stackInfo) {
			
			this.stackInfo = stackInfo;
			if (this.stackInfo != null) {
				this.correctionInfoList = this.stackInfo.getAllCorrectionInfo();
			} else {
				this.correctionInfoList = null;
			}
		} // end of method
		
		public String getColumnName(int col) {
			return columnNames[col];
		} // end of method
		
		public int getRowCount() {

			if (this.correctionInfoList == null) {
				return 0;
			} else {
				return this.correctionInfoList.length;
			}

		} // end of method
		
		public int getColumnCount() { 
			return columnNames.length; 
		} // end of method
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return ItemCodeDefine.getItemName(correctionInfoList[row].getItemCode());
			} else if (col == 1) {
				if (correctionInfoList[row].getStdOxygen() == -1.0D) {
					return "-";
				} else {
					return String.format("%3.0f", correctionInfoList[row].getStdOxygen());
				}
			} else if (col == 2) {
				if (stackInfo.getStandardMoisture() == -1.0D) {
					return "-";
				} else {
					return String.format("%3.2f", stackInfo.getStandardMoisture());
				}
			} else if (col == 3) {
				return correctionInfoList[row].isOxygenApplied();
			} else if (col == 4) {
				return correctionInfoList[row].isTemperatureApplied();
			} else if (col == 5) {
				return correctionInfoList[row].isMoistureApplied();
			} else {
				return "";
			}
			
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return String.class;
			} else if (col == 1) {
				return String.class;
			} else if (col == 2) {
				return String.class;
			} else {
				return Boolean.class;
			}
			
		}

	} // end of class
	
	
} // end of class
