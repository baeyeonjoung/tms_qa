/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.old;

import com.snu.tms.qa.gui.outlier.UI_Outlier_CORR;
import com.snu.tms.qa.gui.outlier.UI_Outlier_PCA;
import com.snu.tms.qa.gui.outlier.UI_Outlier_PD;
import com.snu.tms.qa.gui.outlier.UI_Outlier_SPC;
import com.snu.tms.qa.gui.outlier.dialog.DialogEditCorrection;
import com.snu.tms.qa.gui.outlier.dialog.DialogPattern;
import com.snu.tms.qa.gui.outlier.dialog.DialogPatternResult;
import com.snu.tms.qa.gui.outlier.dialog.DialogRawDataCsvExporter;
import com.snu.tms.qa.gui.outlier.dialog.DialogRawDataGraph;
import com.snu.tms.qa.gui.outlier.dialog.DialogRawDataViewer;
import com.snu.tms.qa.gui.outlier.dialog.DialogRulesetSave;
import com.snu.tms.qa.gui.outlier.dialog.DialogOutlierDetectionResult;
import com.snu.tms.qa.gui.outlier.dialog.DialogOutlierDetectionThreshold;
import com.snu.tms.qa.gui.outlier.event.ActionInvoker;
import com.snu.tms.qa.model.PatternDataSet;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.DatasetManager;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.*;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;


/**
 * Outlier detection panel
 * 
 */
public class AnalysisPanel_OutlierDetection_OLD extends JPanel implements ActionListener
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private DatasetManager datasetManager = DatasetManager.getInstance();
	
	private JPanel          basePanel = null;
	private JLabel          selectedPatternLabel = null;
	private JTabbedPane     outlierDetectionPanel = null;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public AnalysisPanel_OutlierDetection_OLD() {
		
		// Set parents actionListener
		ActionInvoker.getinstnace().addActionListener(this);
		
		// Set statControlBase
		JPanel statControlBase = createStatControlPanel();
		
		// Set control Button Panel
		JPanel patternControlBase = createPatternControlPanel();
		
		// Create Control Panel
		JPanel controlBase = new JPanel(new BorderLayout());
		controlBase.setPreferredSize(new Dimension(500, 135));
		controlBase.add(statControlBase, BorderLayout.NORTH);
		controlBase.add(patternControlBase, BorderLayout.CENTER);
		
		// set outlier detection panel
		this.basePanel = new JPanel(new BorderLayout());
		setOutlierPanel();
		
		// set Layout
		this.setLayout(new BorderLayout());
		this.add(controlBase, BorderLayout.NORTH);
		this.add(this.basePanel, BorderLayout.CENTER);
		
	} // end of constructor
	
	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
		if (e.getActionCommand().equals("apply_pattern")) {
			try {
				// getPatternDataSet
				PatternDataSet patternDataSet = (PatternDataSet)e.getSource();
				
				// clear all previous patternData
				this.datasetManager.clearPatternData();
				
				// set data
				this.datasetManager.initDetectionPatternDataset(patternDataSet);
				
				// Set GUI
				setOutlierPanel();
				
			} catch (Exception ee) {
				
			}
		}
		
	} // end of method
	
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Set general statistical analysis
	 */
	private JPanel createStatControlPanel() {
		
		// Set ancestor
		final JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		
		// Set Base Panel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(500, 60));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("show raw data statistics"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// Set ControlButton
		JButton viewDataButton = new JButton("show raw data");
		viewDataButton.setBounds(30, 25, 150, 25);
		viewDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogRawDataViewer(
						rootFrame, 
						stackInfoStr,
						dataSet);
			}
		});
		basePanel.add(viewDataButton);

		JButton exportDataButton = new JButton("export as a csv");
		exportDataButton.setBounds(210, 25, 150, 25);
		exportDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("%s_%d",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber());
				DialogRawDataCsvExporter.exportRawDataAsCsvFormat(rootFrame, stackInfoStr, dataSet);
			}
		});
		basePanel.add(exportDataButton);
			
		JButton showGraphButton = new JButton("show graph");
		showGraphButton.setBounds(390, 25, 150, 25);
		showGraphButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogRawDataGraph(
						rootFrame, 
						stackInfoStr,
						dataSet);
			}
		});
		basePanel.add(showGraphButton);

		JButton showEditCorrectionButton = new JButton("edit correction");
		showEditCorrectionButton.setBounds(570, 25, 150, 25);
		showEditCorrectionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogEditCorrection(
						rootFrame, 
						stackInfoStr,
						dataSet);
			}
		});
		basePanel.add(showEditCorrectionButton);

		return basePanel;
		
	} // end of method
	
	/**
	 * set Pattern analysis
	 */
	private JPanel createPatternControlPanel() {
		
		// Set Base Panel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(500, 75));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("Pattern Analysis (Principal Analysis & Clustering"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// Set TimeWindows Configuration
		JLabel timeWindowLabel = new JLabel("Time Window (minutes)");
		timeWindowLabel.setBounds(30, 15, 150, 25);
		basePanel.add(timeWindowLabel);
		
		Integer[] timeWindowList = {5, 10, 15, 30, 60, 90, 120};
		final JComboBox<Integer> timeWindowComboBox = new JComboBox<Integer>(timeWindowList);
		timeWindowComboBox.setSelectedIndex(0);
		timeWindowComboBox.setBounds(30, 40, 100, 25);
		basePanel.add(timeWindowComboBox);
		
		// Set Max Pattern Number Configuration
		JLabel patternNumLabel = new JLabel("Max Pattern Number");
		patternNumLabel.setBounds(180, 15, 150, 25);
		basePanel.add(patternNumLabel);
		
		Integer[] patternNumList = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
		final JComboBox<Integer> patternNumComboBox = new JComboBox<Integer>(patternNumList);
		patternNumComboBox.setSelectedIndex(1);
		patternNumComboBox.setBounds(180, 40, 100, 25);
		basePanel.add(patternNumComboBox);
		
		// do reverse correction control button
		JButton patternAnalysisButton = new JButton("Do pattern analysis");
		final JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		patternAnalysisButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				TmsDataset tmsDataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						tmsDataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						tmsDataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						tmsDataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						tmsDataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				int selectedTimeWindow = ((Integer)timeWindowComboBox.getSelectedItem()).intValue();
				int selectedPatternNum = ((Integer)patternNumComboBox.getSelectedItem()).intValue();
				new DialogPattern(
						rootFrame, 
						stackInfoStr,
						tmsDataSet,
						selectedTimeWindow,
						selectedPatternNum);
			}
		});
		patternAnalysisButton.setBounds(330, 40, 150, 25);
		basePanel.add(patternAnalysisButton);
		
		return basePanel;
		
	} // end of method

	/**
	 * Outlier Detection Panel
	 */
	private void setOutlierPanel() {
		
		// clear basePanel
		this.basePanel.removeAll();
		if (this.datasetManager.getPatternCount() == 0) {
			this.basePanel.updateUI();
			return;
		}
		
		// create Button control panel
		JPanel controlBasePanel = setOutlierDetectionControlPanel();
		
		// create pattern selection panel
		JScrollPane patternSelectionBasePanel = 
				new JScrollPane(setOutlierDetectionPatternSelectionPanel());
		
		// create detection Panel
		JPanel patternLabelPanel = new JPanel(null);
		patternLabelPanel.setPreferredSize(new Dimension(400, 30));
		
		JLabel patternLabel = new JLabel("Selected Pattern : ");
		patternLabel.setBounds(10, 5, 120, 20);
		patternLabelPanel.add(patternLabel);
		
		this.selectedPatternLabel = new JLabel();
		//this.selectedPatternLabel.setHorizontalAlignment(SwingConstants.LEFT);
		this.selectedPatternLabel.setBounds(130, 5, 300, 20);
		patternLabelPanel.add(this.selectedPatternLabel);
		
		this.outlierDetectionPanel = new JTabbedPane();

		JPanel detectionBasePanel = new JPanel(new BorderLayout());
		detectionBasePanel.add(patternLabelPanel, BorderLayout.NORTH);
		detectionBasePanel.add(this.outlierDetectionPanel, BorderLayout.CENTER);
		
		// Create basePanel
		JSplitPane outlierDetectionBasePanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		outlierDetectionBasePanel.setDividerLocation(165);
		outlierDetectionBasePanel.setDividerSize(5);
		
		outlierDetectionBasePanel.add(patternSelectionBasePanel);
		outlierDetectionBasePanel.add(detectionBasePanel);

		// Set Layout
		this.basePanel.add(controlBasePanel, BorderLayout.NORTH);
		this.basePanel.add(outlierDetectionBasePanel, BorderLayout.CENTER);
		this.basePanel.updateUI();
		
	} // end of method
	
	/**
	 * Set Outlier Detection Control Panel
	 */
	private JPanel setOutlierDetectionControlPanel() {
		
		// create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(400, 60));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("outlier ruleset control"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));

		
		// Add Control Button
		JButton saveButton = new JButton("save ruleset");
		saveButton.setBounds(30, 25, 150, 25);
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveDetectionRuleset();
			}
		});
		basePanel.add(saveButton);
		
		JButton showPatternButton = new JButton("show Pattern result");
		showPatternButton.setBounds(200, 25, 150, 25);
		showPatternButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showPatternResult();
			}
		});
		basePanel.add(showPatternButton);
		
		JButton setDetionThresholdButton = new JButton("set weighting factor");
		setDetionThresholdButton.setBounds(370, 25, 150, 25);
		setDetionThresholdButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setWeightingFactor();
			}
		});
		basePanel.add(setDetionThresholdButton);

		JButton showResultButton = new JButton("show outlier result");
		showResultButton.setBounds(540, 25, 150, 25);
		showResultButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showOutlierDetectionResult();
			}
		});
		basePanel.add(showResultButton);

		//return
		return basePanel;
		
		
	} // end of method
	
	/**
	 * set Outlier Detection Pattern Selection Panel
	 */
	private void setOutlierDetectionPanel(DetectionPatternDataset patternData) {
		
		// remove all previous component
		this.outlierDetectionPanel.removeAll();
		
		// set Pattern Label
		String patternName = String.format(
				"Pattern ID [ %d ]  Pattern Name [ %s ]", 
				patternData.getPatternId(), 
				patternData.getPatternName());
		this.selectedPatternLabel.setText(patternName);
		
		// Add outlier detection panel
		this.outlierDetectionPanel.add("Multivariate checking",  new UI_Outlier_PCA(patternData));
		this.outlierDetectionPanel.add("Bivariate checking",     new UI_Outlier_CORR(patternData));
		this.outlierDetectionPanel.add("Univariate checking",    new UI_Outlier_SPC(patternData));
		this.outlierDetectionPanel.add("Correction checking",    new UI_Outlier_PD(patternData));
		//this.outlierDetectionPanel.add("Undulation",           new AnalysisPanel_Outlier_UD(patternData));
		//this.outlierDetectionPanel.add("Constant",             new AnalysisPanel_Outlier_CONST(patternData));
		
	} // end of method
	

	/**
	 * set Outliser Detection Panel
	 */
	private JPanel setOutlierDetectionPatternSelectionPanel() {
		
		// Get PatternData
		DetectionPatternDataset[] patternDataArray = this.datasetManager.getAllPatternData();
		
		// Get Pattern name
		if (patternDataArray.length == 0) {
			return new JPanel();
		}
		
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(160, 135*patternDataArray.length));
		
		// Add PatternSelection Box
		JPanel patternSelectionBox;
		for (int i=0 ; i<patternDataArray.length ; i++) {
			patternSelectionBox = setPatternSelectionBox(patternDataArray[i]);
			patternSelectionBox.setBounds(0,  155*i, 160, 155);
			basePanel.add(patternSelectionBox);
		}
		
		// return
		return basePanel;
		
	} // end of method
	
	/**
	 * Create Pattern Selection Box
	 */
	private JPanel setPatternSelectionBox(final DetectionPatternDataset patternData) {
		
		// Create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(155, 155));
		basePanel.setBorder(BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("Pattern ID [" + patternData.getPatternId() + " ]"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// create PatternIndex Label
		JLabel label = new JLabel("Pattern Name");
		label.setBounds(10, 20, 140, 20);
		basePanel.add(label);
		
		JTextField textField = new JTextField(patternData.getPatternName());
		textField.setBounds(10, 40, 140, 20);
		basePanel.add(textField);
		
		JCheckBox enableDetectionCheck = new JCheckBox("enable detection");
		enableDetectionCheck.setSelected(patternData.isEnabled());
		enableDetectionCheck.setBounds(10, 60, 140, 20);
		basePanel.add(enableDetectionCheck);
		
		enableDetectionCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				patternData.setEnabled(((JCheckBox)e.getSource()).isSelected());
			}
		});
		
		final JCheckBox rulesetCheck = new JCheckBox("ruleset created");
		rulesetCheck.setSelected(patternData.isRulesetCreated());
		//rulesetCheck.setEnabled(false);
		rulesetCheck.setBounds(10, 80, 140, 20);
		basePanel.add(rulesetCheck);
		
		rulesetCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rulesetCheck.setSelected(patternData.isRulesetCreated());
			}
		});

		final JCheckBox operateStopCheck = new JCheckBox("operate stop state");
		operateStopCheck.setSelected(patternData.isOperateStop());
		//rulesetCheck.setEnabled(false);
		operateStopCheck.setBounds(10, 100, 140, 20);
		basePanel.add(operateStopCheck);
		
		operateStopCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				patternData.setOperateStop(((JCheckBox)e.getSource()).isSelected());
			}
		});

		JButton button = new JButton("execute detection");
		button.setBounds(10, 125, 140, 20);
		basePanel.add(button);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOutlierDetectionPanel(patternData);
				patternData.setRulesetCreated(true);
				rulesetCheck.setSelected(true);
			}
		});
		
		//this.patternNameInputHash.put(patternData.getPatternIndex(), textField);
		//this.patternEnableInputHash.put(patternData.getPatternIndex(), enableDetectionCheck);
		//this.patternRulesetInputHash.put(patternData.getPatternIndex(), rulesetCheck);
		
		// return
		return basePanel;
		
	} // end of method
	
	/**
	 * save Outlier detection ruleset
	 */
	private void saveDetectionRuleset() {
		
		JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		new DialogRulesetSave(rootFrame);
		
	} // end of method
	
	/**
	 * show Pattern analysis result of outlier detection
	 */
	private void showPatternResult() {
		
		JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
		String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
				dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
				dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
				dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
				dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
		new DialogPatternResult(rootFrame, stackInfoStr, DatasetManager.getInstance().getPatternDataSet());
		
	} // end of method
	
	/**
	 * set outlier detection weighting factor and threshold
	 */
	private void setWeightingFactor() {
		
		JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		new DialogOutlierDetectionThreshold(rootFrame);
		
	} // end of method

	/**
	 * show result of outlier detection
	 */
	private void showOutlierDetectionResult() {
		
		JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		new DialogOutlierDetectionResult(rootFrame);
		
	} // end of method
	

} // end of class
