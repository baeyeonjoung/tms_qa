/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.old;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.snu.tms.qa.analysis.outlier.OutlierDetectionUsingCorrelation;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.OutlierCorrData_OLD;
import com.snu.tms.qa.model.ruleset.OutlierCorrRuleset_OLD;
import com.snu.tms.qa.util.NumberUtil;

/**
 * Panel for outlier(Correlation) detection
 * 
 */
public class AnalysisPanel_Outlier_Correlation_OLD extends JPanel
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	private final static String[] PercentConfidenceLevels = {"90%", "95%", "99%", "99.9%"};
	private final static double[] PercentConfidenceValues = {0.90D, 0.95D, 0.99D, 0.999D};
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private DetectionPatternDataset      patternData;
	private OutlierCorrData_OLD  outlierCorrData;
	
	private JComboBox<String>  confidenceLevelCombo;
	private JTextField         thresholdTextField;

	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Show analysis result for outlier detection using Correlation analysis
	 */
	public AnalysisPanel_Outlier_Correlation_OLD(DetectionPatternDataset patternData) {
		
		/**
		// set data
		this.patternData = patternData;
		
		// compute t-square statistics
		this.outlierCorrData = 
				OutlierDetectionUsingCorrelation.doOutlierDetectionWithPatternData(
						patternData, 0.95D);
				
		// set layout
		this.setLayout(new BorderLayout());
		
		// create control panel
		JPanel controlBase = createControlPanel();
		this.add(controlBase, BorderLayout.NORTH);
		
		// Make base panel
		JSplitPane basePanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		basePanel.setDividerLocation(300);
		basePanel.setDividerSize(5);
		//basePanel.setPreferredSize(new Dimension(400, 600));
		this.add(basePanel, BorderLayout.CENTER);
		
		// make correlation chart panel
		JPanel upperPanel = makeCorrChartPanel();
		JScrollPane upperBasePanel = new JScrollPane(upperPanel);
		upperBasePanel.setPreferredSize(new Dimension(400, 250));
		basePanel.add(upperBasePanel);
		
		// make outlier table panel
		JPanel lowerBasePanel = new JPanel(new BorderLayout());
		lowerBasePanel.setPreferredSize(new Dimension(400, 200));
		basePanel.add(lowerBasePanel);
		
		// make table control panel
		JPanel tableControlPanel = createOutlierTableControlPanel();
		lowerBasePanel.add(tableControlPanel, BorderLayout.NORTH);
		
		// make outlier table panel
		JScrollPane lowerTablePanel = new JScrollPane();
		lowerBasePanel.add(lowerTablePanel, BorderLayout.CENTER);
		*/
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create Control Panel
	 */
	public JPanel createControlPanel() {

		// Set Base Panel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(400, 35));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder(""),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
				
		// Set apply button
		JButton applyButton = new JButton("apply");
		applyButton.setBounds(10, 5, 100, 20);
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		basePanel.add(applyButton);
		
		// set ruleset enable
		JCheckBox rulesetEnableCheck = new JCheckBox("enable ruleset");
		rulesetEnableCheck.setBounds(120, 5, 150, 20);
		rulesetEnableCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		basePanel.add(rulesetEnableCheck);
		
		// return
		return basePanel;
		
	} // end of method
	
	/**
	 * Make Correlation Chart Panel
	 */
	private JPanel makeCorrChartPanel() {
		
		// get all ruleset
		OutlierCorrRuleset_OLD[] rulesets = this.outlierCorrData.getAllRuleset();
		int col = 3;
		int row = rulesets.length / col;
		if ((((double)rulesets.length) % ((double)col)) != 0.0D) {
			row++;
		}
		
		// set JPanel size
		int panelWidth  = 200;
		int panelHeight = 170;
		JPanel basePanel = new JPanel(new GridLayout(0, col));
		basePanel.setPreferredSize(new Dimension(panelWidth * col, panelHeight * row));
		
		// add chartComponent
		JPanel chartPanel;
		for (int i=0 ; i<rulesets.length ; i++) {
			chartPanel = makeChartPanel(rulesets[i], panelWidth, panelHeight);
			basePanel.add(chartPanel);
		}

		// return
		return basePanel;
		
	} // end of method
	
	/**
	 * Make Chart Panel
	 */
	private JPanel makeChartPanel(final OutlierCorrRuleset_OLD ruleset, int width, int height) {
		
		// set base panel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(width, height));
		
		// set checkbox
		String item1Name = ruleset.getItem1Name();
		String item2Name = ruleset.getItem2Name();
		double rsquare   = ruleset.getrSquare();
		String ruleSetName = item1Name + " - " + item2Name + " (" + NumberUtil.getNumberStr(rsquare) + ")";
		JCheckBox rulesetCheck = new JCheckBox(ruleSetName);
		rulesetCheck.setSelected(ruleset.isEnabled());
		rulesetCheck.setBounds(5, 5, 190, 20);
		rulesetCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JCheckBox check = (JCheckBox)e.getSource();
				ruleset.setEnabled(check.isSelected());
			}
		});
		
		// set Chart
		int item1 = ruleset.getItem1();
		int item2 = ruleset.getItem2();
		DetectionRecord[] rawDataRecords = this.patternData.getAllDetectionRecords();
		double[][] rawData = new double[rawDataRecords.clone().length][2];
		for (int i=0 ; i<rawData.length ; i++) {
			rawData[i][0] = rawDataRecords[i].getRawDataCell(item1).getData_value();
			rawData[i][1] = rawDataRecords[i].getRawDataCell(item2).getData_value();
		}
		
		// Create Chart
		JFreeChart chart = ChartFactory.createScatterPlot(
				"", 
				item1Name, 
				item2Name, 
				createChartData(rawData),
				PlotOrientation.VERTICAL, true, true, false);
		
		chart.getLegend().setVisible(false);
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setNoDataMessage("NO DATA");
		
		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		plot.setRangeCrosshairVisible(true);
		plot.setRangeCrosshairLockedOnData(true);
		plot.setDomainZeroBaselineVisible(true);
		plot.setRangeZeroBaselineVisible(true);
		XYDotRenderer renderer = new XYDotRenderer();
		renderer.setDotWidth(3);
		renderer.setDotHeight(3);
		plot.setRenderer(renderer);
		NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
		domainAxis.setAutoRangeIncludesZero(false);
		domainAxis.setTickLabelsVisible(false);
		//domainAxis.setVisible(false);
		
		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setTickLabelsVisible(false);
		//rangeAxis.setVisible(false);
		
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setBounds(0, 20, width, height-25);
		
		// setLayout
		basePanel.add(rulesetCheck);
		basePanel.add(chartPanel);
		
		// return
		return basePanel;
		
	} // end of method
	
	/**
	 * Create Chart Data
	 */
	private XYDataset createChartData(double[][] data) {
		
		XYSeries series = new XYSeries("correlation");
		for (int i=0 ; i<data.length ; i++) {
			series.add(data[i][0], data[i][1]);
		}
		return new XYSeriesCollection(series);
		
	} // end of method
	
	/**
	 * create Table Control Panel
	 */
	public JPanel createOutlierTableControlPanel() {

		// Set Base Panel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(400, 35));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder(""),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
				
		// Set Threshold button
		JLabel label = new JLabel("confidence level");
		label.setBounds(5, 5, 110, 20);
		basePanel.add(label);
		
		this.confidenceLevelCombo = new JComboBox<String>(PercentConfidenceLevels);
		this.confidenceLevelCombo.setBounds(115, 5, 80, 20);
		this.confidenceLevelCombo.setSelectedIndex(1);
		basePanel.add(this.confidenceLevelCombo);
		
		// set threshold textfield
		label = new JLabel("threshold");
		label.setBounds(210, 5, 110, 20);
		basePanel.add(label);
		
		this.thresholdTextField = new JTextField();
		this.thresholdTextField.setEditable(false);
		this.thresholdTextField.setHorizontalAlignment(JTextField.RIGHT);
		this.thresholdTextField.setBounds(320, 5, 70, 20);
		basePanel.add(this.thresholdTextField);
		
		// set apply button
		JButton updateButton = new JButton("apply");
		updateButton.setBounds(400, 5, 90, 20);
		updateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		basePanel.add(updateButton);

		// return
		return basePanel;
		
	} // end of method

	//==========================================================================
	// Inner Class
	//==========================================================================


} // end of class
