/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.old;

import com.snu.tms.qa.gui.outlier.dialog.DialogCorrelationAnalysis;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.DatasetManager;

import java.awt.event.*;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


/**
 * Left Panel for stack selection
 * 
 */
public class AnalysisPanel_Covariance_OLD
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * set general statistical analysis
	 */
	public static void setControl(JPanel rootBasePanel) {
		
		
		// set rootFrame
		final JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(rootBasePanel);

		// Set Base Panel
		JPanel basePanel = new JPanel(null);
		basePanel.setBounds(10, 340, 620, 65);
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("covariance with each tms item"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// do reverse correction control button
		JButton correlationAfterValue = new JButton("correlation(after correction)");
		correlationAfterValue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				TmsDataset tmsDataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						tmsDataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						tmsDataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						tmsDataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						tmsDataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogCorrelationAnalysis(
						rootFrame, 
						stackInfoStr,
						tmsDataSet,
						true);
			}
		});
		correlationAfterValue.setBounds(30, 25, 200, 25);
		basePanel.add(correlationAfterValue);
		
		JButton correlationBeforeValue = new JButton("correlation(before correction)");
		correlationBeforeValue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				TmsDataset tmsDataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						tmsDataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						tmsDataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						tmsDataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						tmsDataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogCorrelationAnalysis(
						rootFrame, 
						stackInfoStr,
						tmsDataSet,
						false);
			}
		});
		correlationBeforeValue.setBounds(250, 25, 200, 25);
		basePanel.add(correlationBeforeValue);
		
		// add rootBasePanel
		rootBasePanel.add(basePanel);
		
	} // end of method

	

} // end of class
