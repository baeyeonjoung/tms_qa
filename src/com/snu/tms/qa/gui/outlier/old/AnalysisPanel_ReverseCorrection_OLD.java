/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.old;

import com.snu.tms.qa.analysis.correction.DoReverseCorrection_OLD;
import com.snu.tms.qa.gui.outlier.dialog.DialogReverseCorrectionAnalysis;
import com.snu.tms.qa.model.CorrectionItemInfo;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.TmsDataCell;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.DatasetManager;

import java.awt.event.*;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


/**
 * Left Panel for stack selection
 * 
 */
public class AnalysisPanel_ReverseCorrection_OLD
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * set general statistical analysis
	 */
	public static void setControl(JPanel rootBasePanel) {
		
		// set rootFrame
		final JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(rootBasePanel);

		// Set Base Panel
		JPanel basePanel = new JPanel(null);
		basePanel.setBounds(10, 200, 620, 140);
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("reverse correction(temperture/moisture/oxgyen)"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// do reverse correction control button
		JButton executeReverseCorrection = new JButton("execute reverse correction");
		executeReverseCorrection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				if (!DatasetManager.getInstance().isApplyReverseCorrection()) {
					// get TmsDataSetInstance
					DatasetManager tmsDataSetInstance = DatasetManager.getInstance();
					TmsDataset tmsDataSet = tmsDataSetInstance.getTmsDataSet();
					
					// do reverse correction
					DoReverseCorrection.doReverseCorrection(tmsDataSet);
					
					// apply
					DatasetManager.getInstance().setApplyReverseCorrection(true);
				}
				*/
			}
		});
		executeReverseCorrection.setBounds(30, 25, 200, 25);
		basePanel.add(executeReverseCorrection);
		
		
		// do analysis button
		JButton executeAnalysis = new JButton("TSP");
		executeAnalysis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				if (!DatasetManager.getInstance().isApplyReverseCorrection()) {
					JOptionPane.showMessageDialog(
							rootFrame, 
							"Prior to to this job, please execute a reverse correction analysis.");
					return;
				}
				*/
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("item(%s), fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						"TSP",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogReverseCorrectionAnalysis(
						rootFrame, 
						stackInfoStr,
						ItemCodeDefine.TSP,
						dataSet);
			}
		});
		executeAnalysis.setBounds(30, 60, 60, 25);
		basePanel.add(executeAnalysis);
		
		executeAnalysis = new JButton("SO2");
		executeAnalysis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				if (!DatasetManager.getInstance().isApplyReverseCorrection()) {
					JOptionPane.showMessageDialog(rootFrame, "Prior to to this job, please execute a reverse correction analysis.");
					return;
				}
				*/
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("item(%s), fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						"SO2",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogReverseCorrectionAnalysis(
						rootFrame, 
						stackInfoStr,
						ItemCodeDefine.SOX,
						dataSet);
			}
		});
		executeAnalysis.setBounds(110, 60, 60, 25);
		basePanel.add(executeAnalysis);
		
		executeAnalysis = new JButton("NOx");
		executeAnalysis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				if (!DatasetManager.getInstance().isApplyReverseCorrection()) {
					JOptionPane.showMessageDialog(rootFrame, "Prior to to this job, please execute a reverse correction analysis.");
					return;
				}
				*/
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("item(%s), fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						"NOx",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogReverseCorrectionAnalysis(
						rootFrame, 
						stackInfoStr,
						ItemCodeDefine.NOX,
						dataSet);
			}
		});
		executeAnalysis.setBounds(190, 60, 60, 25);
		basePanel.add(executeAnalysis);
		
		executeAnalysis = new JButton("HCl");
		executeAnalysis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				if (!DatasetManager.getInstance().isApplyReverseCorrection()) {
					JOptionPane.showMessageDialog(rootFrame, "Prior to to this job, please execute a reverse correction analysis.");
					return;
				}
				*/
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("item(%s), fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						"HCl",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogReverseCorrectionAnalysis(
						rootFrame, 
						stackInfoStr,
						ItemCodeDefine.HCL,
						dataSet);
			}
		});
		executeAnalysis.setBounds(270, 60, 60, 25);
		basePanel.add(executeAnalysis);
		
		executeAnalysis = new JButton("HF");
		executeAnalysis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				if (!DatasetManager.getInstance().isApplyReverseCorrection()) {
					JOptionPane.showMessageDialog(rootFrame, "Prior to to this job, please execute a reverse correction analysis.");
					return;
				}
				*/
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("item(%s), fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						"HF",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogReverseCorrectionAnalysis(
						rootFrame, 
						stackInfoStr,
						ItemCodeDefine.HF,
						dataSet);
			}
		});
		executeAnalysis.setBounds(350, 60, 60, 25);
		basePanel.add(executeAnalysis);
		
		executeAnalysis = new JButton("NH3");
		executeAnalysis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				if (!DatasetManager.getInstance().isApplyReverseCorrection()) {
					JOptionPane.showMessageDialog(rootFrame, "Prior to to this job, please execute a reverse correction analysis.");
					return;
				}
				*/
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("item(%s), fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						"NH3",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogReverseCorrectionAnalysis(
						rootFrame, 
						stackInfoStr,
						ItemCodeDefine.NH3,
						dataSet);
			}
		});
		executeAnalysis.setBounds(430, 60, 60, 25);
		basePanel.add(executeAnalysis);
		
		executeAnalysis = new JButton("CO");
		executeAnalysis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				if (!DatasetManager.getInstance().isApplyReverseCorrection()) {
					JOptionPane.showMessageDialog(rootFrame, "Prior to to this job, please execute a reverse correction analysis.");
					return;
				}
				*/
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("item(%s), fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						"CO",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogReverseCorrectionAnalysis(
						rootFrame, 
						stackInfoStr,
						ItemCodeDefine.CO,
						dataSet);
			}
		});
		executeAnalysis.setBounds(510, 60, 60, 25);
		basePanel.add(executeAnalysis);
		
		executeAnalysis = new JButton("O2");
		executeAnalysis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				if (!DatasetManager.getInstance().isApplyReverseCorrection()) {
					JOptionPane.showMessageDialog(rootFrame, "Prior to to this job, please execute a reverse correction analysis.");
					return;
				}
				*/
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("item(%s), fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						"O2",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogReverseCorrectionAnalysis(
						rootFrame, 
						stackInfoStr,
						ItemCodeDefine.O2,
						dataSet);
			}
		});
		executeAnalysis.setBounds(30, 95, 60, 25);
		basePanel.add(executeAnalysis);
		
		executeAnalysis = new JButton("FLW");
		executeAnalysis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/**
				if (!DatasetManager.getInstance().isApplyReverseCorrection()) {
					JOptionPane.showMessageDialog(rootFrame, "Prior to to this job, please execute a reverse correction analysis.");
					return;
				}
				*/
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("item(%s), fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						"FLW",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogReverseCorrectionAnalysis(
						rootFrame, 
						stackInfoStr,
						ItemCodeDefine.FL1,
						dataSet);
			}
		});
		executeAnalysis.setBounds(110, 95, 60, 25);
		basePanel.add(executeAnalysis);
		
		// add rootBasePanel
		rootBasePanel.add(basePanel);
		
	} // end of method	
	
	
	

} // end of class
