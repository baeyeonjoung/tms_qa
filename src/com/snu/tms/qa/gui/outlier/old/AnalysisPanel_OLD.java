/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.old;

import com.snu.tms.qa.gui.outlier.UI_Pattern;
import com.snu.tms.qa.gui.outlier.UI_RawDataControl;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.DatasetManager;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


/**
 * Left Panel for stack selection
 * 
 */
public class AnalysisPanel_OLD extends JScrollPane
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * 
	 */
	public AnalysisPanel_OLD() {
		
		// Set layout
		this.setPreferredSize(new Dimension(600, 1024));
		//this.setLayout(new BorderLayout());
		
		
		// SetEnalbled
		//setEnabled();
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * CleanUI
	 */
	public void cleanUI() {
		this.getViewport().removeAll();
		this.updateUI();
	} // end of method

	/**
	 * Initialize
	 */
	public void initUI() {
		
		// Add control Button Base Panel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(600, 2000));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("Data management and control"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// Add data analysis control
		UI_RawDataControl.setControl(basePanel);
		AnalysisPanel_GeneralStatAnalysis_OLD.setControl(basePanel);
		AnalysisPanel_ReverseCorrection_OLD.setControl(basePanel);
		AnalysisPanel_Covariance_OLD.setControl(basePanel);
		UI_Pattern.setControl(basePanel);
		
		// add sub_components
		this.getViewport().add(basePanel);
		//this.add(basePanel, BorderLayout.CENTER);
		
		// updateUI
		this.updateUI();
		
	} // end of method
	
	
	/**
	 * 
	 */
	public void setEnabled() {
		
		// Get TmsDataSetInstance
		DatasetManager tmsDataSetInstance = DatasetManager.getInstance();
		TmsDataset tmsDataSet = tmsDataSetInstance.getTmsDataset();
		
		// Check dataSet is loaded
		if (tmsDataSet == null) {
			setSubComponentEnabled(this, false);
		} else {
			setSubComponentEnabled(this, true);
		}
		this.updateUI();
		
	} // end of method
	
	/**
	 * iterate subcomponents
	 */
	private void setSubComponentEnabled(Container parent, boolean enabled) {
		
		if (parent == null) {
			return;
		}
		
		// Get all children
		Component[] children = parent.getComponents();
		if (children != null) {
			for (Component child : children) {
				if (child instanceof Container) {
					setSubComponentEnabled((Container)child, enabled);
				}
				child.setEnabled(enabled);
			}
		}
		
	} // end of method
	
	

} // end of class
