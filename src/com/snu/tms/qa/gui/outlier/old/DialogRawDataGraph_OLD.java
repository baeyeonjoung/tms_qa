/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.old;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;


/**
 * Show Graph
 * 
 */
public class DialogRawDataGraph_OLD extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  TmsDataset dataSet;
	private  boolean[]  itemSelectionCheck;
	private  JFreeChart chart;
	private  boolean  lineVisible = true;
	private  boolean  shapeVisible = true;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogRawDataGraph_OLD(
			JFrame parent, 
			String stackInfoStr,
			TmsDataset dataSet) {
		
		// set dialog
		super(parent, "chart viewer : " + stackInfoStr, ModalityType.MODELESS);
		
		// set icon
		try {
			setIconImage(ImageIO.read(new File("image/smart.png")));
		} catch (IOException exc) {}
		
		// Set Raw Data
		this.dataSet = dataSet;
		
		// create controlPane
		JPanel controlPanel = createControlPanel();
		
		// create graphPanel
		JPanel chartPanel = createChartPanel();
		
		// Add basePanel
		this.setPreferredSize(new Dimension(960, 600));
		this.setLayout(new BorderLayout());
		this.add(controlPanel, BorderLayout.NORTH);
		this.add(chartPanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create control Panel
	 */
	private JPanel createControlPanel() {
		
		// Set basePanel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(960, 95));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("Item selection control"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// Set Item selection checkbox
		int[][] itemRecordCounts = this.dataSet.getItemValueCount();
		JCheckBox[] itemSelectionCheckBox = new JCheckBox[itemRecordCounts.length];
		this.itemSelectionCheck = new boolean[itemRecordCounts.length];
		for (int i=0 ; i<itemRecordCounts.length ; i++) {
			
			itemSelectionCheckBox[i] = new JCheckBox(ItemCodeDefine.getItemName(itemRecordCounts[i][0]));
			if (itemRecordCounts[i][1] <= 0 ) {
				itemSelectionCheck[i] = false;
				itemSelectionCheckBox[i].setSelected(false);
				itemSelectionCheckBox[i].setEnabled(false);
			} else {
				itemSelectionCheck[i] = true;
				itemSelectionCheckBox[i].setSelected(true);
				itemSelectionCheckBox[i].setEnabled(true);
			}
			
			final int index = i;
			itemSelectionCheckBox[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setItemSelectionCheck(index, ((JCheckBox)e.getSource()).isSelected());
				}
			});
			
			itemSelectionCheckBox[i].setBounds(30 + i*70, 25, 60, 20);
			basePanel.add(itemSelectionCheckBox[i]);
		}
		
		JCheckBox lineVisibleCheck = new JCheckBox("line visible");
		lineVisibleCheck.setSelected(true);
		lineVisibleCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lineVisible = ((JCheckBox)e.getSource()).isSelected();
			}
		});
		lineVisibleCheck.setBounds(30, 55, 120, 25);
		basePanel.add(lineVisibleCheck);
		
		JCheckBox shapeVisibleCheck = new JCheckBox("shape visible");
		shapeVisibleCheck.setSelected(true);
		shapeVisibleCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				shapeVisible = ((JCheckBox)e.getSource()).isSelected();
			}
		});
		shapeVisibleCheck.setBounds(170, 55, 120, 25);
		basePanel.add(shapeVisibleCheck);

		
		JButton applyButton = new JButton("apply selection");
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateItemSelection();
			}
		});
		applyButton.setBounds(810, 55, 120, 25);
		basePanel.add(applyButton);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * change item selection check
	 */
	public void setItemSelectionCheck(int i, boolean isCheck) {
		this.itemSelectionCheck[i] = isCheck;
	} // end of method
	
	
	/**
	 * create chart Panel
	 */
	private JPanel createChartPanel() {
		
		// create XYDataSet
		XYDataset chartDataSet = createDataSet();
		
		// create chart
		chart = ChartFactory.createTimeSeriesChart(
				"Raw data analysis",  // title
				"date",             // x-axis label
				"tms_value",   // y-axis label
				chartDataSet,            // data
				true,               // create legend?
				true,               // generate tooltips?
				false               // generate URLs?
		);
		
		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setDomainPannable(true);
		plot.setRangePannable(false);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);
		
		// set chart shape properties
		XYItemRenderer r = plot.getRenderer();
		if (r instanceof XYLineAndShapeRenderer) {
			XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
			renderer.setBaseShapesVisible(true);
		}
		
		// set x-axis properties
		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));

		// return
		return new ChartPanel(chart);
		
	} // end of method
	
	
	/**
	 * 
	 */
	private void updateItemSelection() {
		
		// Get chart renderer
		XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)((XYPlot) chart.getPlot()).getRenderer();
		
		for (int i=0 ; i<itemSelectionCheck.length ; i++) {
			renderer.setSeriesVisible(i, itemSelectionCheck[i]);
			renderer.setSeriesLinesVisible(i, lineVisible);
			renderer.setSeriesShapesVisible(i, shapeVisible);
		}
		
	} // end of method
	
	/**
	 * Create graph data set
	 */
	private XYDataset createDataSet() {
		
		// create TimeSeries
		int[][] itemValueCount = this.dataSet.getItemValueCount();
		TimeSeries[] tmsDatasets = new TimeSeries[itemValueCount.length];
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		for (int i=0 ; i<tmsDatasets.length ; i++) {
			tmsDatasets[i] = new TimeSeries(ItemCodeDefine.getItemName(itemValueCount[i][0]));
			dataset.addSeries(tmsDatasets[i]);
		}
		
		// Set data
		Minute time = null;
		double value = 0.0D;
		TmsDataRecord[] records = this.dataSet.getTmsDataRecords();
		for (TmsDataRecord record : records) {
			
			time = new Minute(new java.util.Date(record.getTime()));
			for (int j=0 ; j<itemValueCount.length ; j++) {
				value = record.getDataCell(itemValueCount[j][0]).getData_value();
				if (value != -1.0D) {
					tmsDatasets[j].add(time, value);
				}
			}
		}

		// return
		return dataset;
		
	} // end of method
	
	//==========================================================================
	// Inner Class
	//==========================================================================

	
	
} // end of class
