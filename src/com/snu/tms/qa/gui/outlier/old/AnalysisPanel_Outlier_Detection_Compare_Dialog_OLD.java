/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.old;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import com.snu.tms.qa.soap.QueryTmsRecord;
import com.snu.tms.qa.fileio.QueryTms30minRecordFromFile;
import com.snu.tms.qa.fileio.Tms30minDataWriter;
import com.snu.tms.qa.model.DatasetManager;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.Tms30minDataCell;
import com.snu.tms.qa.model.Tms30minDataRecord;
import com.snu.tms.qa.model.Tms30minDataset;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.ruleset.OdrCell;
import com.snu.tms.qa.model.ruleset.OdrRecord;
import com.snu.tms.qa.util.DateUtil;


/**
 * Show outlier detection reuslts
 * 
 */
public class AnalysisPanel_Outlier_Detection_Compare_Dialog_OLD extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	private static final String[] OUTLIER_CODE = 
		{"", "WARNING", "MINOR", "MAJOR", "CRITICAL"};
	private static final String[] OUTLIER_CODE_STR = 
		{"정상", "WARNING", "MINOR", "MAJOR", "CRITICAL"};
	private static final String[] HALF_OVER_CODE = 
		{"0", "1", "2"};
	private static final String[] HALF_OVER_CODE_STR = 
		{"0 (정상)", "1 (초과)", "2 (비율초과)"};
	private static final String[] HALF_STAT_CODE = 
		{"0", "1", "2", "4", "8"};
	private static final String[] HALF_STAT_CODE_STR = 
		{"0 (정상)", "1 (교정중)", "2 (동작불량)", "4 (전원단절)", "8 (보정중)"};
	private static final String[] HALF_DLST_CODE = 
		{"0", "1", "4"};
	private static final String[] HALF_DLST_CODE_STR = 
		{"0 (정상)", "1 (비정상)", "4 (전원단절)"};
	private static final String[] HALF_EXCH_CODE = 
		{"0", 
		"10", "11", "12", "13", "14",
		"15", "16", "18", "19", "20", 
		"21", "22", "24", "30", "40", 
		"50", "60", "61", "62", "70", 
		"90", "91", "R1", "R2", "R3"};
	private static final String[] HALF_EXCH_CODE_STR = 
		{"정상", 
		"10", "11", "12", "13", "14",
		"15", "16", "18", "19", "20", 
		"21", "22", "24", "30", "40", 
		"50", "60", "61", "62", "70", 
		"90", "91", "R1", "R2", "R3"};
	private static final String[] HALF_EXCH_DESC = {
		"정상",
		"[10] 미수신 자료 : 정상 마감된 자료 중 최근 30분 평균자료)",
		"[11] 조치명령 받은 측정기기 개선(개선계획서 미제출) : 정상 마감된 전월의 최근 3개월간 30분 평균자료 중 최대값",
		"[12] 조치명령 받은 측정기기 개선(개선계획서 제출) : 정상 마감된 전월의 최근 3개월간 30분 평균자료",
		"[13] 조치명령 받지 않은 측정기기 개선 (자체 개선계획) : 정상 마감된 전월의 최근 3개월간 30분 평균자료",
		"[14] 정도검사 기간 및 불합격 또는 미수검 : 정상 마감된 전월의 최근 1개월간 30분 평균자료",
		"[15] 비정상 측정자료 : 정상 마감된 자료 중 최근 30분 평균자료",
		"[16] 장비점검(통합시험 등) : 정상 마감된 자료 중 최근 30분 평균자료",
		"[18] 관할 행정기관 인정(24시간 미만) : 정상 자료 중 최근 30분 평균자료",
		"[19] 관할 행정기관 인정(24시간 이상) : 정상 마감된 전월의 최근 1개월간 30분 평균자료",
		"[20] 배출시설 기준초과 인정시간 : 해당 30분 평균자료를 그대로 적용",
		"[21] 배출시설 가동중지 : 해당기간의 자료는 0으로 처리",
		"[22] 대체자료 : 사용자가 직접 입력한 값",
		"[24] 행정기관 인정 미전송(최초 설치 후 가동개시 전) : 해당기간 자료는 0으로 처리",
		"[30] 배출및방지시설(개선계획서제출) : 해당 30분 평균자료를 그대로 적용",
		"[40] 배출및방지시설(행정처분후개선) : 해당 30분 평균자료를 그대로 적용",
		"[50] 상대정확도 부적합 : 정상 마감된 전월의 최근 1개월간 30분 평균자료",
		"[60] 공기비 3배 이상인 기간 : 해당 30분 평균자료를 그대로 적용",
		"[61] 산소항목 비정상 : 해당 30분 평균자료를 그대로 사용",
		"[62] 공기비 3배 기간 중 상태표시 발생 : 공기비 3배 이상인 자료 중 최근 30분 평균자료",
		"[70] 상태표시 발생 : 정상 마감된 자료 중 최근 30분 평균자료",
		"[90] 유량계 정도검사 : 정상 가동된 최근 1일간 평균자료",
		"[91] 소각시설 폐기물 투입이전 CO초과 : 해당 30분 평균자료를 그대로 사용",
		"[R1] 오염항목 상태표시, 산소정상 ,온도정상 : 최근 오염항목,산소,온도가 모두 정상측정된 30분 자료",
		"[R2] 오염항목 정상, 산소 상태표시 ,온도 상태표시 : 최근 오염항목,산소,온도가 모두 정상측정된 30분 자료",
		"[R3] 오염항목 상태표시, 산소 상태표시 ,온도 상태표시 : 최근 오염항목,산소,온도가 모두 정상측정된 30분 자료"
	};
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  DatasetManager             datasetManager;
	private  Tms30minDataRecord[]       dataRecords30min;
	private  Tms30minDataRecord[]       dataRecordsFiltered30min;
	private  OdrRecord[]                odrModel;
	
	private  Hashtable<Long, OdrRecord> odrRecordHash;
	private  Hashtable<Long, Tms30minDataRecord> tms30minRecordHash;
	
	private  DataRecordFilter        filter = new DataRecordFilter();
	
	private  JCheckBox[]   outlierFilter  = new JCheckBox[OUTLIER_CODE_STR.length];
	private  JCheckBox[]   halfOverFilter = new JCheckBox[HALF_OVER_CODE_STR.length];
	private  JCheckBox[]   halfStatFilter = new JCheckBox[HALF_STAT_CODE_STR.length];
	private  JCheckBox[]   halfDlstFilter = new JCheckBox[HALF_DLST_CODE_STR.length];
	private  JCheckBox[]   halfExchFilter = new JCheckBox[HALF_EXCH_CODE_STR.length];
	
	private  JTable  dataTable30min;
	private  JTable  dataTable30minDetail;
	private  JTable  dataTable5min;
	
	private  long    selectedTime = 0L;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public AnalysisPanel_Outlier_Detection_Compare_Dialog_OLD(
			JDialog parent,
			OdrRecord[] odrModel) {
		
		// set dialog
		super(parent, "", ModalityType.MODELESS);
		this.setTitle("Outlier detection result (compare with 30-min data)");
		
		// set parameters
		this.datasetManager = DatasetManager.getInstance();
		this.dataRecords30min = new Tms30minDataRecord[0];
		this.dataRecordsFiltered30min = new Tms30minDataRecord[0];
		this.odrModel = odrModel;
		
		// set OdrRecord Hashtable
		this.odrRecordHash = new Hashtable<Long, OdrRecord>();
		for (OdrRecord odrRecord : this.odrModel) {
			this.odrRecordHash.put(odrRecord.time, odrRecord);
		}
		
		// set Tms30minRecord hash
		this.tms30minRecordHash = new Hashtable<Long, Tms30minDataRecord>();
		
		// set filtered dataRecords30min
		setFilteredDataRecords30min();
		
		// set layout
		JPanel      controlPanel     = createControlPanel();
		JPanel      filterPanel      = createFilterPanel();
		JScrollPane tablePanel30min  = create30minTablePanel();
		JScrollPane tablePaneldetail = create30minDetailTablePanel();
		JScrollPane tablePanel5min   = create5minTablePanel();
		
		JPanel upperBasePanel = new JPanel(new BorderLayout());
		controlPanel.setPreferredSize(new Dimension(1000, 50));
		filterPanel.setPreferredSize(new Dimension(1000, 180));
		upperBasePanel.add(controlPanel, BorderLayout.NORTH);
		upperBasePanel.add(filterPanel, BorderLayout.CENTER);
		
		JSplitPane lowerBasePanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		lowerBasePanel.setDividerLocation(600);
		lowerBasePanel.setDividerSize(5);
		
		lowerBasePanel.add(tablePanel30min);
		lowerBasePanel.add(tablePaneldetail);
		
		JSplitPane centerBasePanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		centerBasePanel.setDividerLocation(350);
		centerBasePanel.setDividerSize(5);
		
		centerBasePanel.add(lowerBasePanel);
		centerBasePanel.add(tablePanel5min);
		
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(1000, 800));
		this.add(upperBasePanel, BorderLayout.NORTH);
		this.add(centerBasePanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Set DataRecords
	 */
	private void init() {
		
		// get all records
		this.dataRecords30min = this.datasetManager.get30minTmsDataset().getTmsDataRecords();
		
		// set timeHash
		for (Tms30minDataRecord record : this.dataRecords30min) {
			this.tms30minRecordHash.put(record.getTime(), record);
		}
		
		// set outlier level
		long timeInterval = 5L*60L*1000L;
		long baseTime = 0L;
		int score;
		for (Tms30minDataRecord record : this.dataRecords30min) {
			
			baseTime = record.getTime();
			OdrRecord odrRecord;
			int outlierScore = 0;
			for (int i=0 ; i<6 ; i++) {
				odrRecord = this.odrRecordHash.get(baseTime + timeInterval * (long)i);
				if (odrRecord != null) {
					score = odrRecord.getOutlierLevel();
					if (outlierScore < score) {
						outlierScore = score;
					}
				}
			}
			record.setOutlierLevel(outlierScore);
			
		}
		
		// set filtered records
		setFilteredDataRecords30min();
		// table update
		dataTable30min.updateUI();

	} // end of method
	
	/**
	 * set filtered data records 30min
	 */
	private void setFilteredDataRecords30min() {
		
		// crreate records array
		Vector<Tms30minDataRecord> records = new Vector<Tms30minDataRecord>();
		
		// set filter
		int[] cellItems;
		Tms30minDataCell cell;
		for (Tms30minDataRecord record : this.dataRecords30min) {
			
			cellItems = record.getValidItems();
			
			// search outlier
			boolean satisfyFilter = filter.outlierFilter.get(record.getOutlierLevel());
			if (!satisfyFilter) {
				continue;
			}

			// search half_over
			satisfyFilter = false;
			for (int i=0 ; i<cellItems.length ; i++) {
				cell = record.getDataCell(cellItems[i]);
				if (filter.overFilter.get(cell.getHalf_over()) == Boolean.TRUE) {
					satisfyFilter = true;
					break;
				}
			}
			if (!satisfyFilter) {
				continue;
			}
				
			// search half_stat
			satisfyFilter = false;
			for (int i=0 ; i<cellItems.length ; i++) {
				cell = record.getDataCell(cellItems[i]);
				if (filter.sensorFilter.get(cell.getHalf_stat()) == Boolean.TRUE) {
					satisfyFilter = true;
					break;
				}
			}
			if (!satisfyFilter) {
				continue;
			}
			
			// search half_dlst
			satisfyFilter = false;
			for (int i=0 ; i<cellItems.length ; i++) {
				cell = record.getDataCell(cellItems[i]);
				if (filter.loggerFilter.get(cell.getHalf_dlst()) == Boolean.TRUE) {
					satisfyFilter = true;
					break;
				}
			}
			if (!satisfyFilter) {
				continue;
			}
			
			// search stat_code
			satisfyFilter = false;
			for (int i=0 ; i<cellItems.length ; i++) {
				cell = record.getDataCell(cellItems[i]);
				if (filter.statFilter.get(cell.getStat_code()) == Boolean.TRUE) {
					satisfyFilter = true;
					break;
				}
			}
			if (!satisfyFilter) {
				continue;
			}
			
			// add Record
			records.add(record);
			
		}
		
		// return
		this.dataRecordsFiltered30min = records.toArray(new Tms30minDataRecord[records.size()]);
		
	} // end of method
	
	/**
	 * create control panel
	 */
	private JPanel createControlPanel() {
		
		// create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("  outlier detection result control  "),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// Add Control Button
		JButton queryDataButton = new JButton("query 30-min data");
		queryDataButton.setBounds(20, 20, 200, 20);
		queryDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showWaitMessageDialogAndDoQueryWork();
			}
		});
		basePanel.add(queryDataButton);
		
		// Add Control Button
		JButton saveButton = new JButton("save result");
		saveButton.setBounds(250, 20, 200, 20);
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		basePanel.add(saveButton);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * Create Filter Panel
	 */
	private JPanel createFilterPanel() {
		
		// create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("  30-min data filter configuration  "),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// Set outlier filter
		JCheckBox outlierFilterAllCheck = new JCheckBox("OUTLIER");
		outlierFilterAllCheck.setSelected(false);
		outlierFilterAllCheck.setBounds(20, 20, 120, 25);
		outlierFilterAllCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i=0 ; i<outlierFilter.length ; i++) {
					outlierFilter[i].setSelected(((JCheckBox)e.getSource()).isSelected());
				}
			}
		});
		basePanel.add(outlierFilterAllCheck);
		
		int x = 160;
		int y = 20;
		for (int i=0 ; i<this.outlierFilter.length ; i++) {
		
			this.outlierFilter[i] = new JCheckBox(OUTLIER_CODE_STR[i]);
			this.outlierFilter[i].setSelected(true);
			this.outlierFilter[i].setBounds(x+100*i, y, 100, 25);
			final int codeIndex = i;
			this.outlierFilter[i].addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					filter.outlierFilter.put(OUTLIER_CODE[codeIndex], 
							((JCheckBox)e.getSource()).isSelected());
				}
			});
			basePanel.add(this.outlierFilter[i]);
		}

		// Set half_over filter
		JCheckBox halfOverFilterAllCheck = new JCheckBox("HALF_OVER");
		halfOverFilterAllCheck.setSelected(false);
		halfOverFilterAllCheck.setBounds(20, 45, 120, 25);
		halfOverFilterAllCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i=0 ; i<halfOverFilter.length ; i++) {
					halfOverFilter[i].setSelected(((JCheckBox)e.getSource()).isSelected());
				}
			}
		});
		basePanel.add(halfOverFilterAllCheck);
		
		y = 45;
		for (int i=0 ; i<this.halfOverFilter.length ; i++) {
		
			this.halfOverFilter[i] = new JCheckBox(HALF_OVER_CODE_STR[i]);
			this.halfOverFilter[i].setSelected(true);
			this.halfOverFilter[i].setBounds(x+100*i, y, 100, 25);
			final int codeIndex = i;
			this.halfOverFilter[i].addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					filter.overFilter.put(HALF_OVER_CODE[codeIndex], 
							((JCheckBox)e.getSource()).isSelected());
				}
			});
			basePanel.add(this.halfOverFilter[i]);
		}
		
		// Set half_over filter
		JCheckBox halfStatFilterAllCheck = new JCheckBox("HALF_STAT");
		halfStatFilterAllCheck.setSelected(false);
		halfStatFilterAllCheck.setBounds(20, 70, 120, 25);
		halfStatFilterAllCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i=0 ; i<halfStatFilter.length ; i++) {
					halfStatFilter[i].setSelected(((JCheckBox)e.getSource()).isSelected());
				}
			}
		});
		basePanel.add(halfStatFilterAllCheck);
		
		y = 70;
		for (int i=0 ; i<this.halfStatFilter.length ; i++) {
			
			this.halfStatFilter[i] = new JCheckBox(HALF_STAT_CODE_STR[i]);
			this.halfStatFilter[i].setSelected(true);
			this.halfStatFilter[i].setBounds(x+100*i, y, 100, 25);
			final int codeIndex = i;
			this.halfStatFilter[i].addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					filter.sensorFilter.put(HALF_STAT_CODE[codeIndex], 
							((JCheckBox)e.getSource()).isSelected());
				}
			});
			basePanel.add(this.halfStatFilter[i]);
		}

		// Set half_over filter
		JCheckBox halfDlstFilterAllCheck = new JCheckBox("HALF_DLST");
		halfDlstFilterAllCheck.setSelected(false);
		halfDlstFilterAllCheck.setBounds(20, 95, 120, 25);
		halfDlstFilterAllCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i=0 ; i<halfDlstFilter.length ; i++) {
					halfDlstFilter[i].setSelected(((JCheckBox)e.getSource()).isSelected());
				}
			}
		});
		basePanel.add(halfDlstFilterAllCheck);
		
		y = 95;
		for (int i=0 ; i<this.halfDlstFilter.length ; i++) {
			
			this.halfDlstFilter[i] = new JCheckBox(HALF_DLST_CODE_STR[i]);
			this.halfDlstFilter[i].setSelected(true);
			this.halfDlstFilter[i].setBounds(x+100*i, y, 100, 25);
			final int codeIndex = i;
			this.halfDlstFilter[i].addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					filter.loggerFilter.put(HALF_DLST_CODE[codeIndex], 
							((JCheckBox)e.getSource()).isSelected());
				}
			});
			basePanel.add(this.halfDlstFilter[i]);
		}

		// Set half_over filter
		JCheckBox halfExchFilterAllCheck = new JCheckBox("STAT_CODE");
		halfExchFilterAllCheck.setSelected(false);
		halfExchFilterAllCheck.setBounds(20, 120, 120, 25);
		halfExchFilterAllCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i=0 ; i<halfExchFilter.length ; i++) {
					halfExchFilter[i].setSelected(((JCheckBox)e.getSource()).isSelected());
				}
			}
		});
		basePanel.add(halfExchFilterAllCheck);
		
		y = 120;
		int index = 0;
		for (int i=0 ; i<this.halfExchFilter.length ; i++) {
			
			if (i == 13) {
				index = 0;
				y += 25;
			}
			this.halfExchFilter[i] = new JCheckBox(HALF_EXCH_CODE_STR[i]);
			this.halfExchFilter[i].setSelected(true);
			this.halfExchFilter[i].setBounds(x+62*index, y, 62, 25);
			this.halfExchFilter[i].setToolTipText(HALF_EXCH_DESC[i]);
			final int codeIndex = i;
			this.halfExchFilter[i].addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					filter.statFilter.put(HALF_EXCH_CODE[codeIndex], 
							((JCheckBox)e.getSource()).isSelected());
				}
			});
			basePanel.add(this.halfExchFilter[i]);
			index++;
		}
		
		// Add Control Button
		JButton applyButton = new JButton("apply");
		applyButton.setBounds(20, 145, 100, 25);
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setFilteredDataRecords30min();
				dataTable30min.updateUI();
			}
		});
		basePanel.add(applyButton);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * create 30min viewer table
	 */
	private JScrollPane create30minTablePanel() {
		
		// Create Table
		TableModel30min model = new TableModel30min();
		this.dataTable30min = new JTable(model);
		this.dataTable30min.setRowHeight(20);
		this.dataTable30min.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.dataTable30min.getColumnModel().getColumn(0).setPreferredWidth(50);
		this.dataTable30min.getColumnModel().getColumn(1).setPreferredWidth(140);
		this.dataTable30min.getColumnModel().getColumn(2).setPreferredWidth(70);
		for (int i=3 ; i<model.getColumnCount() ; i++) {
			this.dataTable30min.getColumnModel().getColumn(i++).setPreferredWidth(50);
			this.dataTable30min.getColumnModel().getColumn(i).setPreferredWidth(40);
		}
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		this.dataTable30min.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.dataTable30min.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.dataTable30min.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		for (int i=3 ; i<model.getColumnCount() ; i++) {
			this.dataTable30min.getColumnModel().getColumn(i++).setCellRenderer( rightRenderer );
			this.dataTable30min.getColumnModel().getColumn(i).setCellRenderer( centerRenderer );
		}
		
		// add List selection Model
		this.dataTable30min.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent event) {
						
						int  selectedRow = dataTable30min.getSelectedRow();
						String timeStr = (String)dataTable30min.getValueAt(selectedRow, 1);
						long selectedTime = DateUtil.getDate(timeStr);
						changeTableRowSelection(selectedTime);
						
					}
				});
				
		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.dataTable30min);
		return basePane;
		
	} // end of method
	
	private JScrollPane create30minDetailTablePanel() {

		// Create Table
		TableModel30minDetail model = new TableModel30minDetail();
		this.dataTable30minDetail = new JTable(model);
		this.dataTable30minDetail.setRowHeight(20);
		this.dataTable30minDetail.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		this.dataTable30minDetail.getColumnModel().getColumn(0).setPreferredWidth(40);
		this.dataTable30minDetail.getColumnModel().getColumn(1).setPreferredWidth(60);
		this.dataTable30minDetail.getColumnModel().getColumn(2).setPreferredWidth(40);
		this.dataTable30minDetail.getColumnModel().getColumn(3).setPreferredWidth(40);
		this.dataTable30minDetail.getColumnModel().getColumn(4).setPreferredWidth(40);
		this.dataTable30minDetail.getColumnModel().getColumn(5).setPreferredWidth(40);
		this.dataTable30minDetail.getColumnModel().getColumn(6).setPreferredWidth(60);

		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		this.dataTable30minDetail.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.dataTable30minDetail.getColumnModel().getColumn(1).setCellRenderer( rightRenderer );
		this.dataTable30minDetail.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		this.dataTable30minDetail.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
		this.dataTable30minDetail.getColumnModel().getColumn(4).setCellRenderer( centerRenderer );
		this.dataTable30minDetail.getColumnModel().getColumn(5).setCellRenderer( centerRenderer );
		this.dataTable30minDetail.getColumnModel().getColumn(6).setCellRenderer( rightRenderer );
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.dataTable30minDetail);
		return basePane;
		
	} // end of method
	
	private JScrollPane create5minTablePanel() {

		// Create Table
		TableModel5min model = new TableModel5min();
		this.dataTable5min = new JTable(model);
		this.dataTable5min.setRowHeight(20);
		this.dataTable5min.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.dataTable5min.getColumnModel().getColumn(0).setPreferredWidth(140);
		this.dataTable5min.getColumnModel().getColumn(1).setPreferredWidth(50);
		this.dataTable5min.getColumnModel().getColumn(2).setPreferredWidth(100);
		this.dataTable5min.getColumnModel().getColumn(3).setPreferredWidth(50);
		this.dataTable5min.getColumnModel().getColumn(4).setPreferredWidth(50);
		this.dataTable5min.getColumnModel().getColumn(5).setPreferredWidth(80);
		for (int i=6 ; i<this.dataTable5min.getColumnModel().getColumnCount() ; i++) {
			this.dataTable5min.getColumnModel().getColumn(i).setPreferredWidth(100);
		}

		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		for (int i=0 ; i<6 ; i++) {
			this.dataTable5min.getColumnModel().getColumn(i).setCellRenderer( centerRenderer );
		}
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.dataTable5min);
		return basePane;
		
	} // end of method
	
	/**
	 * change row selection
	 */
	private void changeTableRowSelection(long time) {
		
		// set selected time
		this.selectedTime = time;
		
		// update 30min record detail table
		((TableModel30minDetail)this.dataTable30minDetail.getModel()).setSelectedDataCell();
		this.dataTable30minDetail.updateUI();
		
		// update row data table
		((TableModel5min)this.dataTable5min.getModel()).setSelectedDataCell();
		this.dataTable5min.updateUI();
		
	} // end of method
	
	/**
	 * Show Query Data
	 */
	private void showWaitMessageDialogAndDoQueryWork() {
		
		// Get selected StackInfo
		final DatasetManager datasetManager = DatasetManager.getInstance();
		final StackInfo stackInfo = datasetManager.getTmsDataset().getTmsDataHeader().getStackInfo();
		if (stackInfo == null) {
			JOptionPane.showMessageDialog(
					SwingUtilities.getWindowAncestor(this), 
					"Analysis target stack is not valid.");
		}
		final long sTime = datasetManager.getTmsDataset().getTmsDataHeader().getStart_time();
		final long eTime = datasetManager.getTmsDataset().getTmsDataHeader().getEnd_time();
		
		// Make SwingWorker
		final Window win = SwingUtilities.getWindowAncestor(this);
		SwingWorker<Void, Void> threadSwingWorker = new SwingWorker<Void, Void>() {

			@Override
			protected Void doInBackground() throws Exception {

				// set Tms30minDataset
				Tms30minDataset tms30minDataset = null;
				
				// Firstly, Search data file from "./clone_data/"
				String sTimeStr = DateUtil.getDateStr(sTime);
				String eTimeStr = DateUtil.getDateStr(eTime);
				String fileName = "./clone_data/tms_30min_data_" + 
						stackInfo.getFactoryInfo().getCode() + "_" + 
						stackInfo.getStackNumber() + "_" + 
						sTimeStr.substring(0, 10) + "_" + eTimeStr.substring(0, 10) + ".txt";
				File dataFile = new File(fileName);
				if (dataFile.exists()) {
					tms30minDataset = QueryTms30minRecordFromFile.readTmsRecord(
							fileName, 
							stackInfo.getFactoryInfo().getCode(), 
							stackInfo.getStackNumber(), 
							sTime, 
							eTime);
				} 
				// Secondly, query from database
				else {
					tms30minDataset= QueryTmsRecord.queryTms30minRecord(
						stackInfo.getFactoryInfo().getCode(), 
						stackInfo.getStackNumber(), 
						sTime, 
						eTime);
				}
			
				// Set Data
				datasetManager.setTms30minDataset(tms30minDataset);
				
				// File Save
				if (!dataFile.exists()) {
					int answer = JOptionPane.showConfirmDialog(
							win, 
							"Do you want to save the data to a file for use next time analysis?");
					if (answer == JOptionPane.YES_OPTION) {
						Tms30minDataWriter.writeDataFile(fileName, tms30minDataset);
					}
				}
				
				// set Data
				init();
				// return
				return null;
			}
		};
		
		// Create JDialog for show please wait message
		final JDialog dialog = new JDialog(win, "Information message", ModalityType.APPLICATION_MODAL);
		
		// Add SwingWorker Listener
		threadSwingWorker.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state")) {
					if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
						dialog.dispose();
					}
				}
			}
		});
		
		// Execute SwingWorker
		threadSwingWorker.execute();
		
		// Show ProgressBar
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		JPanel panel = new JPanel(new BorderLayout());
		panel.setPreferredSize(new Dimension(400, 80));
		panel.add(progressBar, BorderLayout.CENTER);
		JLabel messageLabel = new JLabel("  Database query job take serveral minutes.");
		messageLabel.setPreferredSize(new Dimension(300, 40));
		panel.add(messageLabel, BorderLayout.PAGE_START);
		dialog.add(panel);
		dialog.pack();
		dialog.setLocationRelativeTo(win);
		dialog.setVisible(true);
		
	} // end of method

	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * 30min data table model
	 */
	class TableModel30min extends AbstractTableModel {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		private static final long serialVersionUID = 1L;
		
		//==========================================================================
		// Fields
		//==========================================================================
		private int[][]  itemsValueCount = datasetManager.getTmsDataset().getItemValueCount();
		private String[] columnNames;
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public TableModel30min() {
			
			this.columnNames = new String[itemsValueCount.length*2 + 3];
			this.columnNames[0] = "No.";
			this.columnNames[1] = "Time";
			this.columnNames[2] = "Outlier";
			for (int i=0 ; i<itemsValueCount.length ; i++) {
				String colName = ItemCodeDefine.getItemName(itemsValueCount[i][0]);
				this.columnNames[i*2+3] = colName;
				this.columnNames[i*2+4] = "Code";
			}
		}
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			return dataRecordsFiltered30min.length;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return row+1;
			} else if (col == 1) {
				return DateUtil.getDateStr(dataRecordsFiltered30min[row].getTime());
			} else if (col == 2) {
				return dataRecordsFiltered30min[row].getOutlierLevel();
			} else {
				int item = itemsValueCount[(col-3)/2][0];
				double half_value = dataRecordsFiltered30min[row].getDataCell(item).getHalf_valu();
				double stat_value = dataRecordsFiltered30min[row].getDataCell(item).getStat_valu();
				String stat_code  = dataRecordsFiltered30min[row].getDataCell(item).getStat_code();
				if ((col-3) % 2 == 0) {
					if (stat_value != -1.0D) {
						return stat_value;
					} else {
						return half_value;
					}
				} else {
					return stat_code;
				}
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * 30min data table model
	 */
	class TableModel30minDetail extends AbstractTableModel {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		private static final long serialVersionUID = 1L;
		
		//==========================================================================
		// Fields
		//==========================================================================
		private String[] columnNames = 
			{"ITEM", "VALUE", "OVER", "STAT", "DLST", "E.Code", "E.Val"};
		
		private Tms30minDataCell[] selectedCells = null;
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public TableModel30minDetail() {
			
		} // end of constructor
		
		//==========================================================================
		// Methods
		//==========================================================================
		/**
		 * set selected Record cell
		 */
		public void setSelectedDataCell() {
			
			// get selected record
			Tms30minDataRecord selectedRecord = tms30minRecordHash.get(selectedTime);
			if (selectedRecord == null) {
				this.selectedCells = null;
			} else {
				int[] items = selectedRecord.getValidItems();
				this.selectedCells = new Tms30minDataCell[items.length];
				for (int i=0 ; i<items.length ; i++) {
					this.selectedCells[i] = selectedRecord.getDataCell(items[i]);
				}
			}
			
		} // end of method
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			
			if (this.selectedCells == null) {
				return 0;
			} else {
				return this.selectedCells.length;
			}
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return ItemCodeDefine.getItemName(this.selectedCells[row].getData_type());
			} else if (col == 1) {
				return this.selectedCells[row].getHalf_valu();
			} else if (col == 2) {
				return this.selectedCells[row].getHalf_over();
			} else if (col == 3) {
				return this.selectedCells[row].getHalf_stat();
			} else if (col == 4) {
				return this.selectedCells[row].getHalf_dlst();
			} else if (col == 5) {
				return this.selectedCells[row].getStat_code();
			} else {
				double stat_value = this.selectedCells[row].getStat_valu();
				if (stat_value == -1.0D) {
					return "";
				} else {
					return stat_value;
				}
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * 5min data table model
	 */
	class TableModel5min extends AbstractTableModel {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		private static final long serialVersionUID = 1L;
		
		//==========================================================================
		// Fields
		//==========================================================================
		private String[] columnNames = null;
		
		private OdrRecord[]     selectedOdrRecords = null;
		private TmsDataRecord[] selectedTmsRecords = null;
		
		private int[][]  itemCounts = null;
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public TableModel5min() {
			
			// Get first odrRecord
			OdrRecord odrRecord = odrModel[0];
			this.itemCounts = datasetManager.getTmsDataset().getItemValueCount();
			
			// get column count
			int columnCount = 6 + itemCounts.length + odrRecord.odrCells.length;
			this.columnNames = new String[columnCount];
			
			int index = 0;
			this.columnNames[index++] = "Time";
			this.columnNames[index++] = "Pattern";
			this.columnNames[index++] = "Pattern Name";
			this.columnNames[index++] = "Count";
			this.columnNames[index++] = "Score";
			this.columnNames[index++] = "Level";
			
			for (int i=0 ; i<this.itemCounts.length ; i++) {
				this.columnNames[index++] = ItemCodeDefine.getItemName(this.itemCounts[i][0]);
			}
			
			for (OdrCell odrCell : odrRecord.odrCells) {
				this.columnNames[index++] = odrCell.ruleTypeName;
			}
			
		} // end of constructor
		
		//==========================================================================
		// Methods
		//==========================================================================
		/**
		 * set selected Record cell
		 */
		public void setSelectedDataCell() {
			
			// get selected record
			OdrRecord[] odrRecords = new OdrRecord[6];
			int index = 0;
			long baseTime = selectedTime;
			long timeInterval = 5L*60L*1000L;
			for (int i=0 ; i<6 ; i++) {
				OdrRecord record = odrRecordHash.get(baseTime + (long)i * timeInterval);
				if (record != null) {
					odrRecords[index++] = record;
				}
			}
			this.selectedOdrRecords = Arrays.copyOf(odrRecords, index);
			
			// set tmsDataRecord
			TmsDataset tmsDataset = datasetManager.getTmsDataset();
			this.selectedTmsRecords = new TmsDataRecord[this.selectedOdrRecords.length];
			for (int i=0 ; i<this.selectedTmsRecords.length ; i++) {
				this.selectedTmsRecords[i] = 
						tmsDataset.getTmsDataRecord(this.selectedOdrRecords[i].time);
			}
			
		} // end of method
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			
			if (this.selectedOdrRecords == null) {
				return 0;
			} else {
				return this.selectedOdrRecords.length;
			}
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return DateUtil.getDateStr(this.selectedOdrRecords[row].time);
			} else if (col == 1) {
				return this.selectedOdrRecords[row].patternId;
			} else if (col == 2) {
				return this.selectedOdrRecords[row].patternName;
			} else if (col == 3) {
				return this.selectedOdrRecords[row].getOutlierCount();
			} else if (col == 4) {
				return this.selectedOdrRecords[row].getOutlierScore();
			} else if (col == 5) {
				return this.selectedOdrRecords[row].getOutlierLevelStr();
			} else if (col < (6+this.itemCounts.length)) {
				int item = this.itemCounts[col - 6][0];
				return this.selectedTmsRecords[row].getDataCell(item).getData_value();
			} else {
				int odrIndex = col - 6 - this.itemCounts.length;
				OdrCell odrCell = this.selectedOdrRecords[row].odrCells[odrIndex];
				return new Boolean(odrCell.isOutlier == 1);
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return String.class;
			} else if (col == 1) {
				return Integer.class;
			} else if (col == 2) {
				return String.class;
			} else if (col == 3) {
				return Integer.class;
			} else if (col == 4) {
				return Integer.class;
			} else if (col == 5) {
				return String.class;
			} else if (col < (6+this.itemCounts.length)) {
				return Double.class;
			} else {
				return Boolean.class;
			}
			
		} // end of method

		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * 30min data filter
	 */
	class DataRecordFilter {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		
		//==========================================================================
		// Fields
		//==========================================================================
		public Hashtable<String, Boolean> outlierFilter= new Hashtable<String, Boolean>();
		public Hashtable<String, Boolean> overFilter   = new Hashtable<String, Boolean>();
		public Hashtable<String, Boolean> sensorFilter = new Hashtable<String, Boolean>();
		public Hashtable<String, Boolean> loggerFilter = new Hashtable<String, Boolean>();
		public Hashtable<String, Boolean> statFilter   = new Hashtable<String, Boolean>();
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public DataRecordFilter() {
			
			// outlier
			outlierFilter.put("",         true);
			outlierFilter.put("WARNING",  true);
			outlierFilter.put("MINOR",    true);
			outlierFilter.put("MAJOR",    true);
			outlierFilter.put("CRITICAL", true);

			// half_over
			overFilter.put("0", true);
			overFilter.put("1", true);
			overFilter.put("2", true);
			
			// half_stat
			sensorFilter.put("0", true);
			sensorFilter.put("1", true);
			sensorFilter.put("2", true);
			sensorFilter.put("4", true);
			sensorFilter.put("8", true);
			
			// half_dlst
			loggerFilter.put("0", true);
			loggerFilter.put("1", true);
			loggerFilter.put("4", true);
			
			// stat_code
			statFilter.put("0", true);
			statFilter.put("10", true);
			statFilter.put("11", true);
			statFilter.put("12", true);
			statFilter.put("13", true);
			statFilter.put("14", true);
			statFilter.put("15", true);
			statFilter.put("16", true);
			statFilter.put("18", true);
			statFilter.put("19", true);
			statFilter.put("20", true);
			statFilter.put("21", true);
			statFilter.put("22", true);
			statFilter.put("24", true);
			statFilter.put("30", true);
			statFilter.put("40", true);
			statFilter.put("50", true);
			statFilter.put("60", true);
			statFilter.put("61", true);
			statFilter.put("62", true);
			statFilter.put("70", true);
			statFilter.put("90", true);
			statFilter.put("91", true);
			statFilter.put("R1", true);
			statFilter.put("R2", true);
			statFilter.put("R3", true);
			
		} // end of constructor
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================

		
	} // end of class
	
} // end of class
