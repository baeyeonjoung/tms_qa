/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier;

import com.snu.tms.qa.gui.outlier.dialog.DialogPattern;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.DatasetManager;

import java.awt.event.*;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


/**
 * Left Panel for stack selection
 * 
 */
public class UI_Pattern
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * set general statistical analysis
	 */
	public static void setControl(JPanel rootBasePanel) {
		
		// set rootFrame
		final JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(rootBasePanel);

		// Set Base Panel
		JPanel basePanel = new JPanel(null);
		basePanel.setBounds(10, 405, 620, 90);
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("Pattern Analysis (Principal Analysis & Clustering"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// Set TimeWindows Configuration
		JLabel timeWindowLabel = new JLabel("Time Window (minutes)");
		timeWindowLabel.setBounds(30, 25, 200, 25);
		basePanel.add(timeWindowLabel);
		
		Integer[] timeWindowList = {5, 10, 15, 30, 60, 90, 120};
		final JComboBox<Integer> timeWindowComboBox = new JComboBox<Integer>(timeWindowList);
		timeWindowComboBox.setSelectedIndex(0);
		timeWindowComboBox.setBounds(230, 25, 100, 25);
		basePanel.add(timeWindowComboBox);
		
		// Set Max Pattern Number Configuration
		JLabel patternNumLabel = new JLabel("Max Pattern Num.");
		patternNumLabel.setBounds(30, 55, 200, 25);
		basePanel.add(patternNumLabel);
		
		Integer[] patternNumList = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
		final JComboBox<Integer> patternNumComboBox = new JComboBox<Integer>(patternNumList);
		patternNumComboBox.setSelectedIndex(2);
		patternNumComboBox.setBounds(230, 55, 100, 25);
		basePanel.add(patternNumComboBox);
		
		// do reverse correction control button
		JButton patternAnalysisButton = new JButton("Pattern Analysis");
		patternAnalysisButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				TmsDataset tmsDataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						tmsDataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						tmsDataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						tmsDataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						tmsDataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				int selectedTimeWindow = ((Integer)timeWindowComboBox.getSelectedItem()).intValue();
				int selectedPatternNum = ((Integer)patternNumComboBox.getSelectedItem()).intValue();
				new DialogPattern(
						rootFrame, 
						stackInfoStr,
						tmsDataSet,
						selectedTimeWindow,
						selectedPatternNum);
			}
		});
		patternAnalysisButton.setBounds(370, 25, 150, 25);
		basePanel.add(patternAnalysisButton);
		
		// add rootBasePanel
		rootBasePanel.add(basePanel);
		
	} // end of method

	

} // end of class
