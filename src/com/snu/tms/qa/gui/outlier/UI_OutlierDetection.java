/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier;

import com.snu.tms.qa.soap.QueryTmsRecord;
import com.snu.tms.qa.fileio.QueryTmsRecordFromFile;
import com.snu.tms.qa.fileio.TmsDataReader;
import com.snu.tms.qa.fileio.TmsDataWriter;
import com.snu.tms.qa.gui.evaluation.dialog.DialogCloneDataFileManager;
import com.snu.tms.qa.gui.outlier.dialog.DialogEditCorrection;
import com.snu.tms.qa.gui.outlier.dialog.DialogPattern;
import com.snu.tms.qa.gui.outlier.dialog.DialogPatternResult;
import com.snu.tms.qa.gui.outlier.dialog.DialogRawDataCsvExporter;
import com.snu.tms.qa.gui.outlier.dialog.DialogRawDataGraph;
import com.snu.tms.qa.gui.outlier.dialog.DialogRawDataViewer;
import com.snu.tms.qa.gui.outlier.dialog.DialogRulesetSave;
import com.snu.tms.qa.gui.outlier.dialog.DialogOutlierDetectionResult;
import com.snu.tms.qa.gui.outlier.dialog.DialogOutlierDetectionThreshold;
import com.snu.tms.qa.gui.outlier.event.ActionInvoker;
import com.snu.tms.qa.model.InventoryDataManager;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.PatternDataSet;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.DatasetManager;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.util.DateUtil;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.Dialog.ModalityType;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.JFormattedTextField.AbstractFormatter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;


/**
 * Outlier detection panel
 * 
 */
public class UI_OutlierDetection extends JPanel implements ActionListener
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;

	private static Log logger = LogFactory.getLog(UI_OutlierDetection.class);

	//==========================================================================
	// Local Fields
	//==========================================================================
	private DatasetManager datasetManager = DatasetManager.getInstance();
	
	private  JPanel           overallBasePanel      = null;
	private  JPanel           contentsBasePanel     = null;
	
	private  JLabel           selectedPatternLabel  = null;
	private  JTabbedPane      outlierDetectionPanel = null;
	
	private  JDatePickerImpl  startDatePicker;
	private  JDatePickerImpl  endDatePicker;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public UI_OutlierDetection() {
		
		// Set parents actionListener
		ActionInvoker.getinstnace().addActionListener(this);
		
		// add data loading control panel
		JPanel dataControlBase = createDataControlPanel();
		this.overallBasePanel = new JPanel(new BorderLayout());
		
		// set Layout
		this.setLayout(new BorderLayout());
		this.add(dataControlBase, BorderLayout.NORTH);
		this.add(this.overallBasePanel, BorderLayout.CENTER);
		
	} // end of constructor
	
	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
		if (e.getActionCommand().equals("apply_pattern")) {
			try {
				// getPatternDataSet
				PatternDataSet patternDataSet = (PatternDataSet)e.getSource();
				
				// clear all previous patternData
				this.datasetManager.clearPatternData();
				
				// set data
				this.datasetManager.initDetectionPatternDataset(patternDataSet);
				
				// Set GUI
				setOutlierPanel();
				
			} catch (Exception ee) {
				
			}
		}
		
	} // end of method
	
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create data loading control panel
	 */
	private JPanel createDataControlPanel() {

		// Set target date
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(600, 60));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("  data loading control  "),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));

		// Set start Date
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year",  "Year");

		UtilDateModel startDateModel = new UtilDateModel();
		startDateModel.setDate(2014, 0, 1);
		startDateModel.setSelected(true);
		JDatePanelImpl startDatePanel = new JDatePanelImpl(startDateModel, p);
		startDatePanel.updateUI();
		startDatePanel.setShowYearButtons(true);
		this.startDatePicker = new JDatePickerImpl(startDatePanel, new DateLabelFormatter());
		this.startDatePicker.setBounds(20, 20, 170, 30);
		basePanel.add(this.startDatePicker);

		// Set end Date
		UtilDateModel endDateModel = new UtilDateModel();
		endDateModel.setDate(2015, 0, 1);
		endDateModel.setSelected(true);
		JDatePanelImpl endDatePanel = new JDatePanelImpl(endDateModel, p);
		endDatePanel.setShowYearButtons(true);
		this.endDatePicker = new JDatePickerImpl(endDatePanel, new DateLabelFormatter());
		this.endDatePicker.setBounds(210, 20, 170, 30);
		basePanel.add(this.endDatePicker);
		
		// Set Apply Button
		JButton dataLoadingButton = new JButton("apply");
		dataLoadingButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadData();
			}
		});
		dataLoadingButton.setBounds(400, 20, 80, 25);
		basePanel.add(dataLoadingButton);

		// Set pattern loading Button
		JButton patternLoadingButton = new JButton("load pattern");
		patternLoadingButton.setEnabled(false);
		patternLoadingButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		patternLoadingButton.setBounds(520, 20, 110, 25);
		basePanel.add(patternLoadingButton);

		// Set data management Button
		JButton dataManagerButton = new JButton("file manager");
		dataManagerButton.setEnabled(true);
		final JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		dataManagerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new DialogCloneDataFileManager(rootFrame);
			}
		});
		dataManagerButton.setBounds(640, 20, 110, 25);
		basePanel.add(dataManagerButton);
		return basePanel;
		
	} // end of method
	
	/**
	 * load (from db query or file loading) file
	 */
	private void loadData() {
		
		// Get Selected StackInfo
		InventoryDataManager inventoryDataManager = InventoryDataManager.getInstance();
		StackInfo stackInfo = inventoryDataManager.getSelectedStackInfo();
		
		// Get Start Time
		int sYear  = this.startDatePicker.getModel().getYear();
		int sMonth = this.startDatePicker.getModel().getMonth();
		int sDay   = this.startDatePicker.getModel().getDay();
		String sDateStr = DateUtil.getDateStr(sYear, sMonth, sDay);
		
		// Get Start Time
		int eYear  = this.endDatePicker.getModel().getYear();
		int eMonth = this.endDatePicker.getModel().getMonth();
		int eDay   = this.endDatePicker.getModel().getDay();
		String eDateStr = DateUtil.getDateStr(eYear, eMonth, eDay);
		
		// Check StackInfo and TimeSet
		if (stackInfo == null) {
			JOptionPane.showMessageDialog(
					SwingUtilities.getWindowAncestor(this), 
					"Analysis target stack is not valid.");
		} else if (eDateStr.compareTo(sDateStr) <= 0) {
			JOptionPane.showMessageDialog(
					SwingUtilities.getWindowAncestor(this), 
					"Analysis target time is not valid.");
		} else {
			// Show Dialog and query start
			showWaitMessageDialogAndDoQueryWork(stackInfo, sDateStr, eDateStr, true);
		}
		
		// show data query result with JOptionPane
		TmsDataset queryDataset = this.datasetManager.getTmsDataset();
		String  factName  = queryDataset.getTmsDataHeader().getFactoryInfo().getFullName();
		String  factCode  = queryDataset.getTmsDataHeader().getFactoryInfo().getCode();
		int     stackCode = queryDataset.getTmsDataHeader().getStackInfo().getStackNumber();
		int[][] itemCount = queryDataset.getItemValueCount();
		String  itemResultStr = "";
		for (int i=0 ; i<itemCount.length ; i++) {
			if (itemCount[i][1] != 0) {
				itemResultStr += String.format("%s[%d]  ", ItemCodeDefine.getItemName(itemCount[i][0]), itemCount[i][1]);
			}
		}
		String resultStr = String.format(
				"%s\n  - Factory : %s [%s]\n  - Stack   : %d\n  - Data     : %s", 
				"TMS data query results",
				factCode, factName,
				stackCode,
				itemResultStr);
		JOptionPane.showMessageDialog(
				SwingUtilities.getWindowAncestor(this), 
				resultStr);
		
		// update panel
		showGeneralControlPanel();
		
	} // end of method
	
	/**
	 * Data loading waiting dialog
	 */
	private void showWaitMessageDialogAndDoQueryWork(
			final StackInfo stackInfo, 
			final String sTimeStr, 
			final String eTimeStr, 
			final boolean fileSave) {
		
		// Make SwingWorker
		SwingWorker<Void, Void> threadSwingWorker = new SwingWorker<Void, Void>() {

			@Override
			protected Void doInBackground() throws Exception {
				
				// Prepare DataSetInstnace
				DatasetManager tmsDataSetInstance = DatasetManager.getInstance();
				tmsDataSetInstance.clear();
				
				// prepare TmsDataSet
				TmsDataset tmsDataSet = null;
				
				// search file
				String targetFilePath = TmsDataReader.searchFile(
						"./clone_data", 
						stackInfo.getFactoryInfo().getCode(), 
						stackInfo.getStackNumber(), 
						sTimeStr.substring(0, 10), 
						eTimeStr.substring(0, 10));
				if (targetFilePath == null) {
					targetFilePath = "*";
				}
				
				// Firstly, Search data file from "./clone_data/"
				File dataFile = new File(targetFilePath);
				if (dataFile.exists()) {
					logger.debug("Target file exist. [" + targetFilePath + "]");
					tmsDataSet = QueryTmsRecordFromFile.readTmsRecord(
							targetFilePath, 
							stackInfo.getFactoryInfo().getCode(), 
							stackInfo.getStackNumber(), 
							sTimeStr, 
							eTimeStr);
					tmsDataSet.getTmsDataHeader().setFactoryInfo(stackInfo.getFactoryInfo());
					tmsDataSet.getTmsDataHeader().setStackInfo(stackInfo);
				} 
				// Secondly, query from database
				else {
					logger.debug("Target file doesn't exist. So try db query.");
					tmsDataSet = QueryTmsRecord.queryTmsRecord(
							stackInfo.getFactoryInfo().getCode(), 
							stackInfo.getStackNumber(), 
							sTimeStr, 
							eTimeStr);
					tmsDataSet.getTmsDataHeader().setFactoryInfo(stackInfo.getFactoryInfo());
					tmsDataSet.getTmsDataHeader().setStackInfo(stackInfo);
				}

				// Set Data
				tmsDataSetInstance.setTmsDataset(tmsDataSet);
				
				// data validation check
				boolean isValidData = false;
				int[][] itemCount = tmsDataSet.getItemValueCount();
				for (int i=0 ; i<itemCount.length ; i++) {
					if (itemCount[i][1] != 0) {
						isValidData = true;
						break;
					}
				}
				
				// File Save
				if (isValidData && !dataFile.exists() && fileSave) {
					String fileName = "./clone_data/tms_data_" + 
							stackInfo.getFactoryInfo().getCode() + "_" + 
							stackInfo.getStackNumber() + "_" + 
							sTimeStr.substring(0, 10) + "_" + eTimeStr.substring(0, 10) + ".txt";
					TmsDataWriter.writeDataFile(fileName, tmsDataSet);
				}

				// return
				return null;
			}
		};
		
		// Create JDialog for show please wait message
		Window win = SwingUtilities.getWindowAncestor(this);
		final JDialog dialog = new JDialog(win, "Information message", ModalityType.APPLICATION_MODAL);
		
		// Add SwingWorker Listener
		threadSwingWorker.addPropertyChangeListener(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state")) {
					if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
						dialog.dispose();
					}
				}
			}
		});
		
		// Execute SwingWorker
		threadSwingWorker.execute();
		
		// Show ProgressBar
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		JPanel panel = new JPanel(new BorderLayout());
		panel.setPreferredSize(new Dimension(400, 80));
		panel.add(progressBar, BorderLayout.CENTER);
		JLabel messageLabel = new JLabel("  Database query job take serveral minutes.");
		messageLabel.setPreferredSize(new Dimension(300, 40));
		panel.add(messageLabel, BorderLayout.PAGE_START);
		dialog.add(panel);
		dialog.pack();
		dialog.setLocationRelativeTo(win);
		dialog.setVisible(true);
		
	} // end of method
	

	/**
	 * Set general statistical analysis
	 */
	private void showGeneralControlPanel() {
		
		// remove all sub-contents components
		if (this.overallBasePanel != null) {
			this.overallBasePanel.removeAll();
		}
		
		if (this.datasetManager.getTmsDataset() == null) {
			return;
		}
		
		// create contents panel
		JPanel controlPanel = createStatisticControlPanel();
		JPanel patternPanel = createPatternControlPanel();
		this.contentsBasePanel = new JPanel(new BorderLayout());
		
		JPanel basePanel = new JPanel(new BorderLayout());
		basePanel.setPreferredSize(new Dimension(600, 135));
		basePanel.add(controlPanel, BorderLayout.NORTH);
		basePanel.add(patternPanel, BorderLayout.CENTER);
		
		// add panel
		this.overallBasePanel.add(basePanel, BorderLayout.NORTH);
		this.overallBasePanel.add(this.contentsBasePanel, BorderLayout.CENTER);
		
		// update UI
		this.overallBasePanel.updateUI();
		
	} // end of method
		
	/**
	 * Create general statisics control panel
	 */
	private JPanel createStatisticControlPanel() {
		
		// Set ancestor
		final JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		
		// Set Base Panel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(500, 60));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("show raw data statistics"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// Set ControlButton
		JButton viewDataButton = new JButton("show raw data");
		viewDataButton.setBounds(30, 25, 150, 25);
		viewDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogRawDataViewer(
						rootFrame, 
						stackInfoStr,
						dataSet);
			}
		});
		basePanel.add(viewDataButton);

		JButton exportDataButton = new JButton("export as a csv");
		exportDataButton.setBounds(210, 25, 150, 25);
		exportDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String fileName = String.format("%s(Stack-%d)_%s-%s",
						dataSet.getTmsDataHeader().getFactoryInfo().getSimpleName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						DateUtil.getTmsDateStr(dataSet.getTmsDataHeader().getStart_time()),
						DateUtil.getTmsDateStr(dataSet.getTmsDataHeader().getEnd_time()));
				DialogRawDataCsvExporter.exportRawDataAsCsvFormat(rootFrame, fileName, dataSet);
			}
		});
		basePanel.add(exportDataButton);
			
		JButton showGraphButton = new JButton("show graph");
		showGraphButton.setBounds(390, 25, 150, 25);
		showGraphButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogRawDataGraph(
						rootFrame, 
						stackInfoStr,
						dataSet);
			}
		});
		basePanel.add(showGraphButton);

		JButton showEditCorrectionButton = new JButton("edit correction");
		showEditCorrectionButton.setBounds(570, 25, 150, 25);
		showEditCorrectionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogEditCorrection(
						rootFrame, 
						stackInfoStr,
						dataSet);
			}
		});
		basePanel.add(showEditCorrectionButton);

		return basePanel;
		
	} // end of method
	
	/**
	 * set Pattern analysis
	 */
	private JPanel createPatternControlPanel() {
		
		// Set Base Panel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(500, 75));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("Pattern Analysis (Principal Analysis & Clustering"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// Set TimeWindows Configuration
		JLabel timeWindowLabel = new JLabel("Time Window (minutes)");
		timeWindowLabel.setBounds(30, 15, 150, 25);
		basePanel.add(timeWindowLabel);
		
		Integer[] timeWindowList = {5, 10, 15, 30, 60, 90, 120};
		final JComboBox<Integer> timeWindowComboBox = new JComboBox<Integer>(timeWindowList);
		timeWindowComboBox.setSelectedIndex(0);
		timeWindowComboBox.setBounds(30, 40, 100, 25);
		basePanel.add(timeWindowComboBox);
		
		// Set Max Pattern Number Configuration
		JLabel patternNumLabel = new JLabel("Max Pattern Number");
		patternNumLabel.setBounds(180, 15, 150, 25);
		basePanel.add(patternNumLabel);
		
		Integer[] patternNumList = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
		final JComboBox<Integer> patternNumComboBox = new JComboBox<Integer>(patternNumList);
		patternNumComboBox.setSelectedIndex(1);
		patternNumComboBox.setBounds(180, 40, 100, 25);
		basePanel.add(patternNumComboBox);
		
		// do reverse correction control button
		JButton patternAnalysisButton = new JButton("Do pattern analysis");
		final JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		patternAnalysisButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				TmsDataset tmsDataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						tmsDataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						tmsDataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						tmsDataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						tmsDataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				int selectedTimeWindow = ((Integer)timeWindowComboBox.getSelectedItem()).intValue();
				int selectedPatternNum = ((Integer)patternNumComboBox.getSelectedItem()).intValue();
				new DialogPattern(
						rootFrame, 
						stackInfoStr,
						tmsDataSet,
						selectedTimeWindow,
						selectedPatternNum);
			}
		});
		patternAnalysisButton.setBounds(330, 40, 150, 25);
		basePanel.add(patternAnalysisButton);
		
		return basePanel;
		
	} // end of method

	/**
	 * Outlier Detection Panel
	 */
	private void setOutlierPanel() {
		
		// clear basePanel
		this.contentsBasePanel.removeAll();
		if (this.datasetManager.getPatternCount() == 0) {
			this.contentsBasePanel.updateUI();
			return;
		}
		
		// create Button control panel
		JPanel controlBasePanel = setOutlierDetectionControlPanel();
		
		// create pattern selection panel
		JScrollPane patternSelectionBasePanel = 
				new JScrollPane(setOutlierDetectionPatternSelectionPanel());
		
		// create detection Panel
		JPanel patternLabelPanel = new JPanel(null);
		patternLabelPanel.setPreferredSize(new Dimension(400, 30));
		
		JLabel patternLabel = new JLabel("Selected Pattern : ");
		patternLabel.setBounds(10, 5, 120, 20);
		patternLabelPanel.add(patternLabel);
		
		this.selectedPatternLabel = new JLabel();
		//this.selectedPatternLabel.setHorizontalAlignment(SwingConstants.LEFT);
		this.selectedPatternLabel.setBounds(130, 5, 300, 20);
		patternLabelPanel.add(this.selectedPatternLabel);
		
		this.outlierDetectionPanel = new JTabbedPane();

		JPanel detectionBasePanel = new JPanel(new BorderLayout());
		detectionBasePanel.add(patternLabelPanel, BorderLayout.NORTH);
		detectionBasePanel.add(this.outlierDetectionPanel, BorderLayout.CENTER);
		
		// Create basePanel
		JSplitPane outlierDetectionBasePanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		outlierDetectionBasePanel.setDividerLocation(180);
		outlierDetectionBasePanel.setDividerSize(5);
		
		outlierDetectionBasePanel.add(patternSelectionBasePanel);
		outlierDetectionBasePanel.add(detectionBasePanel);

		// Set Layout
		this.contentsBasePanel.add(controlBasePanel, BorderLayout.NORTH);
		this.contentsBasePanel.add(outlierDetectionBasePanel, BorderLayout.CENTER);
		this.contentsBasePanel.updateUI();
		
	} // end of method
	
	/**
	 * Set Outlier Detection Control Panel
	 */
	private JPanel setOutlierDetectionControlPanel() {
		
		// create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(400, 60));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("outlier ruleset control"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));

		
		// Add Control Button
		JButton saveButton = new JButton("save ruleset");
		saveButton.setBounds(30, 25, 150, 25);
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveDetectionRuleset();
			}
		});
		basePanel.add(saveButton);
		
		JButton showPatternButton = new JButton("show Pattern result");
		showPatternButton.setBounds(200, 25, 150, 25);
		showPatternButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showPatternResult();
			}
		});
		basePanel.add(showPatternButton);
		
		JButton setDetionThresholdButton = new JButton("set weighting factor");
		setDetionThresholdButton.setBounds(370, 25, 150, 25);
		setDetionThresholdButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setWeightingFactor();
			}
		});
		basePanel.add(setDetionThresholdButton);

		JButton showResultButton = new JButton("show outlier result");
		showResultButton.setBounds(540, 25, 150, 25);
		showResultButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showOutlierDetectionResult();
			}
		});
		basePanel.add(showResultButton);

		//return
		return basePanel;
		
		
	} // end of method
	
	/**
	 * set Outlier Detection Pattern Selection Panel
	 */
	private void setOutlierDetectionPanel(DetectionPatternDataset patternData) {
		
		// remove all previous component
		this.outlierDetectionPanel.removeAll();
		
		// set Pattern Label
		String patternName = String.format(
				"Pattern ID [ %d ]  Pattern Name [ %s ]", 
				patternData.getPatternId(), 
				patternData.getPatternName());
		this.selectedPatternLabel.setText(patternName);
		
		// Add outlier detection panel
		this.outlierDetectionPanel.add("Multivariate checking",  new UI_Outlier_PCA(patternData));
		this.outlierDetectionPanel.add("Bivariate checking",     new UI_Outlier_CORR(patternData));
		this.outlierDetectionPanel.add("Univariate checking",    new UI_Outlier_SPC(patternData));
		this.outlierDetectionPanel.add("Correction checking",    new UI_Outlier_PD(patternData));
		//this.outlierDetectionPanel.add("Undulation",           new AnalysisPanel_Outlier_UD(patternData));
		//this.outlierDetectionPanel.add("Constant",             new AnalysisPanel_Outlier_CONST(patternData));
		
	} // end of method
	

	/**
	 * set Outliser Detection Panel
	 */
	private JPanel setOutlierDetectionPatternSelectionPanel() {
		
		// Get PatternData
		DetectionPatternDataset[] patternDataArray = this.datasetManager.getAllPatternData();
		
		// Get Pattern name
		if (patternDataArray.length == 0) {
			return new JPanel();
		}
		
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(160, 155*patternDataArray.length));
		
		// Add PatternSelection Box
		JPanel patternSelectionBox;
		for (int i=0 ; i<patternDataArray.length ; i++) {
			patternSelectionBox = setPatternSelectionBox(patternDataArray[i]);
			patternSelectionBox.setBounds(0,  155*i, 160, 155);
			basePanel.add(patternSelectionBox);
		}
		
		// return
		return basePanel;
		
	} // end of method
	
	/**
	 * Create Pattern Selection Box
	 */
	private JPanel setPatternSelectionBox(final DetectionPatternDataset patternData) {
		
		// Create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(155, 155));
		basePanel.setBorder(BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("Pattern ID [" + patternData.getPatternId() + " ]"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// create PatternIndex Label
		JLabel label = new JLabel("Pattern Name");
		label.setBounds(10, 20, 140, 20);
		basePanel.add(label);
		
		JTextField textField = new JTextField(patternData.getPatternName());
		textField.setBounds(10, 40, 140, 20);
		basePanel.add(textField);
		
		JCheckBox enableDetectionCheck = new JCheckBox("enable detection");
		enableDetectionCheck.setSelected(patternData.isEnabled());
		enableDetectionCheck.setBounds(10, 60, 140, 20);
		basePanel.add(enableDetectionCheck);
		
		enableDetectionCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				patternData.setEnabled(((JCheckBox)e.getSource()).isSelected());
			}
		});
		
		final JCheckBox rulesetCheck = new JCheckBox("ruleset created");
		rulesetCheck.setSelected(patternData.isRulesetCreated());
		//rulesetCheck.setEnabled(false);
		rulesetCheck.setBounds(10, 80, 140, 20);
		basePanel.add(rulesetCheck);
		
		rulesetCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rulesetCheck.setSelected(patternData.isRulesetCreated());
			}
		});

		final JCheckBox operateStopCheck = new JCheckBox("operate stop state");
		operateStopCheck.setSelected(patternData.isOperateStop());
		//rulesetCheck.setEnabled(false);
		operateStopCheck.setBounds(10, 100, 140, 20);
		basePanel.add(operateStopCheck);
		
		operateStopCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				patternData.setOperateStop(((JCheckBox)e.getSource()).isSelected());
			}
		});

		JButton button = new JButton("execute detection");
		button.setBounds(10, 125, 140, 20);
		basePanel.add(button);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setOutlierDetectionPanel(patternData);
				patternData.setRulesetCreated(true);
				rulesetCheck.setSelected(true);
			}
		});
		
		//this.patternNameInputHash.put(patternData.getPatternIndex(), textField);
		//this.patternEnableInputHash.put(patternData.getPatternIndex(), enableDetectionCheck);
		//this.patternRulesetInputHash.put(patternData.getPatternIndex(), rulesetCheck);
		
		// return
		return basePanel;
		
	} // end of method
	
	/**
	 * save Outlier detection ruleset
	 */
	private void saveDetectionRuleset() {
		
		JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		new DialogRulesetSave(rootFrame);
		
	} // end of method
	
	/**
	 * show Pattern analysis result of outlier detection
	 */
	private void showPatternResult() {
		
		JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
		String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
				dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
				dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
				dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
				dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
		new DialogPatternResult(rootFrame, stackInfoStr, DatasetManager.getInstance().getPatternDataSet());
		
	} // end of method
	
	/**
	 * set outlier detection weighting factor and threshold
	 */
	private void setWeightingFactor() {
		
		JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		new DialogOutlierDetectionThreshold(rootFrame);
		
	} // end of method

	/**
	 * show result of outlier detection
	 */
	private void showOutlierDetectionResult() {
		
		JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(this);
		new DialogOutlierDetectionResult(rootFrame);
		
	} // end of method
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	public class DateLabelFormatter extends AbstractFormatter {

		private static final long serialVersionUID = 1L;
		private String datePattern = "yyyy-MM-dd HH:mm:ss";
		private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

		public Object stringToValue(String text) throws ParseException {
			return dateFormatter.parseObject(text);
		}

		public String valueToString(Object value) throws ParseException {
			if (value != null) {
				Calendar cal = (Calendar) value;
				return dateFormatter.format(cal.getTime());
			}
			return "";
		}
	} // end of class
	

} // end of class
