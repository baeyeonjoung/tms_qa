/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier;

import javax.swing.JTabbedPane;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;


/**
 * Panel for outlier(Correlation analysis) detection
 * 
 */
public class UI_Outlier_CONST extends JTabbedPane
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Show analysis result for outlier detection using SPC Analysis
	 */
	public UI_Outlier_CONST(DetectionPatternDataset patternData) {
		
		// get all items
		int[] items = patternData.getItems();
		
		// filter with r-sqaure and p-value
		for (int i=0 ; i<items.length ; i++) {
			this.add(ItemCodeDefine.getItemName(items[i]), 
					new UI_Outlier_SPC_ITEM(	patternData, items[i]));
		}
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
