/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier;

import javax.swing.JTabbedPane;

import com.snu.tms.qa.analysis.outlier.OutlierDetectionUsingCorrelation;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.ruleset.CorrelationModel;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;


/**
 * Panel for outlier(Correlation analysis) detection
 * 
 */
public class UI_Outlier_CORR extends JTabbedPane
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	private static final double  CORR_RSQUARE_LOWER_LIMIT = 0.40D;
	private static final double  CORR_PVALUE_UPPER_LIMIT  = 0.05D;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Show analysis result for outlier detection using Correction Analysis
	 */
	public UI_Outlier_CORR(DetectionPatternDataset patternData) {
		
		// compute r-square for each two-variables
		CorrelationModel[] corrModels = 
				OutlierDetectionUsingCorrelation.getCorrelationModels(patternData);
		
		// filter with r-sqaure and p-value
		for (int i=0 ; i<corrModels.length ; i++) {
			
			// do filtering
			if (corrModels[i].getrSquare() >= CORR_RSQUARE_LOWER_LIMIT &&
					corrModels[i].getpValue() <= CORR_PVALUE_UPPER_LIMIT) {
				String name = String.format(
						"CORR [%s - %s]", 
						ItemCodeDefine.getItemName(corrModels[i].getItem1()),
						ItemCodeDefine.getItemName(corrModels[i].getItem2()));
				this.add(name, 
						new UI_Outlier_CORR_ITEM(
								patternData, 
								corrModels[i].getItem1(), 
								corrModels[i].getItem2(),
								corrModels[i].getrSquare()));
			}
		}
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
