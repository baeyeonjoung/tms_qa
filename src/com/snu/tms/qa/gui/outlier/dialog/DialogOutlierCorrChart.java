/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.ruleset.DetectionRecord;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.*;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;


/**
 * Show Graph
 * 
 */
public class DialogOutlierCorrChart extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	private  int[]               items;
	private  DetectionRecord[]   dataRecords;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogOutlierCorrChart(
			JFrame parent, 
			String chartName,
			int[] items,
			DetectionRecord[] dataRecords) {
		
		// set dialog
		super(parent, "", ModalityType.MODELESS);
		this.setTitle(chartName);
		
		// set parameters
		this.items = items;
		this.dataRecords = dataRecords;
		
		// create chart
		JPanel chartPanel = createChartPanel();
		chartPanel.setPreferredSize(new Dimension(800, 500));
		
		this.setLayout(new BorderLayout());
		this.add(chartPanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create chart Panel
	 */
	private JPanel createChartPanel() {
	
		// create Chart Data
		XYDataset dataset = createDataSet(this.items[0], this.items[1]);
		
		// Create Chart
		JFreeChart chart = ChartFactory.createScatterPlot(
				"",
				ItemCodeDefine.getItemName(this.items[0]),
				ItemCodeDefine.getItemName(this.items[1]), 
				dataset,
				PlotOrientation.VERTICAL, true, true, false);
		
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setNoDataMessage("NO DATA");
		
		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		plot.setRangeCrosshairVisible(true);
		plot.setRangeCrosshairLockedOnData(true);
		plot.setDomainZeroBaselineVisible(true);
		plot.setRangeZeroBaselineVisible(true);
		XYDotRenderer renderer = new XYDotRenderer();
		renderer.setDotWidth(3);
		renderer.setDotHeight(3);
		plot.setRenderer(renderer);
		NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
		domainAxis.setAutoRangeIncludesZero(false);
		domainAxis.setTickLabelsVisible(true);
		domainAxis.setVisible(true);
		
		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setTickLabelsVisible(true);
		rangeAxis.setVisible(true);
		
		chart.getLegend().setVisible(true);
		
		// create chart Panel
		ChartPanel basePanel = new ChartPanel(chart);
		basePanel.setPreferredSize(new Dimension(800, 500));

		// Add ActionListener
		
		return basePanel;
		
	} // end of method

	/**
	 * Create graph data set
	 */
	private XYDataset createDataSet(int item_x, int item_y) {
		
		// Set DataSet
		XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
		
		// Add XYSeries
		XYSeries xySeries = new XYSeries("");
		double x, y;
		for (int i=0 ; i<this.dataRecords.length ; i++) {
			
			x = this.dataRecords[i].getRawDataCell(item_x).getData_value();
			y = this.dataRecords[i].getRawDataCell(item_y).getData_value();
			xySeries.add(x, y);
			
		} // end of for-loop
		xySeriesCollection.addSeries(xySeries);
		return xySeriesCollection;
		
	} // end of method
	
	//==========================================================================
	// Inner Class
	//==========================================================================

	
} // end of class
