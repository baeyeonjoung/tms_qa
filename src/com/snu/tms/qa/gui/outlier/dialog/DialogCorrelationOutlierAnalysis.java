/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.snu.tms.qa.analysis.correlation.DoComputeMahalanobisDistance;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.correlation.TmsDataCell4Mahalanobis;
import com.snu.tms.qa.util.DateUtil;
import com.snu.tms.qa.util.NumberUtil;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * Show Graph
 * 
 */
public class DialogCorrelationOutlierAnalysis extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogCorrelationOutlierAnalysis(
			JFrame parent, 
			String stackInfoStr,
			TmsDataset dataSet,
			int itemCode1,
			int itemCode2,
			boolean isCorrectedValue) {
		
		// set dialog
		super(parent, "", ModalityType.MODELESS);
		if (isCorrectedValue) {
			this.setTitle("outlier with correlation : " + stackInfoStr);
		} else {
			this.setTitle("outlier with correlation (before corrected) : " + stackInfoStr);
		}
		
		// set base panel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(800, 1000));
		
		// add correlation chart panel
		JPanel correlationChartPanel = addChartPanel(
				itemCode1, itemCode2, dataSet, isCorrectedValue);
		correlationChartPanel.setBounds(0, 0, 300, 300);
		basePanel.add(correlationChartPanel);
		
		// compute mahalanobis distance
		TmsDataCell4Mahalanobis[] md_dataset = 
				DoComputeMahalanobisDistance.doAnalysis(itemCode1, itemCode2, dataSet, isCorrectedValue);
		
		// add QQ-plot chart panel
		JPanel qqChartPanel = addQQPlotChartPanel(
				itemCode1, itemCode2, md_dataset);
		qqChartPanel.setBounds(300, 0, 500, 300);
		basePanel.add(qqChartPanel);
		
		// add time-series chart panel
		JPanel timeseriesChartPanel = addTimeSeriesChartPanel(
				itemCode1, itemCode2, md_dataset);
		timeseriesChartPanel.setBounds(0, 300, 800, 300);
		basePanel.add(timeseriesChartPanel);
		
		// add raw data table
		JScrollPane tablePanel = createTablePanel(md_dataset);
		tablePanel.setBounds(0, 600, 800, 400);
		basePanel.add(tablePanel);
		
		// add base panel to dialog
		this.setLayout(new BorderLayout());
		this.add(basePanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Add Chart Panel
	 */
	private JPanel addChartPanel(
			final int itemCode1, 
			final int itemCode2, 
			final TmsDataset dataSet, 
			final boolean isCorrectedValue) {
		
		// create Chart Data
		XYDataset dataset = createChartData(itemCode1, itemCode2, dataSet, isCorrectedValue);
		
		// Compute pearsons correlation
		double[] pearsonsCorrelationValue = computePearsonCorrelation(itemCode1, itemCode2, dataSet, isCorrectedValue);
		String correlationInfoStr = String.format("corr = %.2f, pValue = %.2f", 
				pearsonsCorrelationValue[0], pearsonsCorrelationValue[1]);
		
		// Create Chart
		JFreeChart chart = ChartFactory.createScatterPlot(
				correlationInfoStr, 
				ItemCodeDefine.getItemName(itemCode1), 
				ItemCodeDefine.getItemName(itemCode2), 
				dataset,
				PlotOrientation.VERTICAL, true, true, false);
		
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setNoDataMessage("NO DATA");
		
		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		plot.setRangeCrosshairVisible(true);
		plot.setRangeCrosshairLockedOnData(true);
		plot.setDomainZeroBaselineVisible(true);
		plot.setRangeZeroBaselineVisible(true);
		XYDotRenderer renderer = new XYDotRenderer();
		renderer.setDotWidth(4);
		renderer.setDotHeight(4);
		plot.setRenderer(renderer);
		NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
		domainAxis.setAutoRangeIncludesZero(false);
		//domainAxis.setTickLabelsVisible(false);
		//domainAxis.setVisible(false);
		
		//NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		//rangeAxis.setTickLabelsVisible(false);
		//rangeAxis.setVisible(false);
		
		chart.getLegend().setVisible(false);
		
		// create chart Panel
		ChartPanel basePanel = new ChartPanel(chart);
		return basePanel;
		
	} // end of method
	
	
	/**
	 * Add Chart Panel
	 */
	private JPanel addQQPlotChartPanel(
			final int itemCode1, 
			final int itemCode2, 
			final TmsDataCell4Mahalanobis[] md_dataset) {
		
		// create Chart Data
		XYDataset dataset = createChartData(md_dataset);
		
		// Create Chart
		JFreeChart chart = ChartFactory.createScatterPlot(
				"Mahalanobis Distance QQ-Plot", 
				"Mahalanobis distance", 
				"Quantiles of Chi_p^2", 
				dataset,
				PlotOrientation.VERTICAL, true, true, false);
		
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setNoDataMessage("NO DATA");
		
		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		plot.setRangeCrosshairVisible(true);
		plot.setRangeCrosshairLockedOnData(true);
		plot.setDomainZeroBaselineVisible(true);
		plot.setRangeZeroBaselineVisible(true);
		//XYDotRenderer renderer = new XYDotRenderer();
		//renderer.setDotWidth(2);
		//renderer.setDotHeight(2);
		XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
		renderer.setSeriesOutlinePaint(0, Color.blue);
		renderer.setSeriesFillPaint(0, Color.white);
		renderer.setUseFillPaint(true);
		renderer.setUseOutlinePaint(true);
		
		plot.setRenderer(renderer);
		NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
		domainAxis.setAutoRangeIncludesZero(false);
		//domainAxis.setTickLabelsVisible(false);
		//domainAxis.setVisible(false);
		
		//NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		//rangeAxis.setTickLabelsVisible(false);
		//rangeAxis.setVisible(false);
		
		chart.getLegend().setVisible(false);
		
		// create chart Panel
		ChartPanel basePanel = new ChartPanel(chart);
		return basePanel;
		
	} // end of method
	
	/**
	 * Add Chart Panel
	 */
	private JPanel addTimeSeriesChartPanel(
			final int itemCode1, 
			final int itemCode2, 
			final TmsDataCell4Mahalanobis[] md_dataset) {
		
		// create Chart Data (first series)
		XYDataset dataset1 = createChartData1(md_dataset);
		
		// Create Chart
		JFreeChart chart = ChartFactory.createTimeSeriesChart(
				"", 
				"time", 
				"Axis for " + ItemCodeDefine.getItemName(itemCode1), 
				dataset1,
				true, true, false);
		
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setOrientation(PlotOrientation.VERTICAL);
		plot.setDomainPannable(true);
		plot.setRangePannable(true);
		
		// set x-axis properties
		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));
		
		// for AXIS 2
		NumberAxis axis2 = new NumberAxis("Axis for " + ItemCodeDefine.getItemName(itemCode2));
		axis2.setAutoRangeIncludesZero(false);
		plot.setRangeAxis(1, axis2);
		plot.setRangeAxisLocation(1, AxisLocation.BOTTOM_OR_LEFT);

		XYDataset dataset2 = createChartData2(md_dataset);
		plot.setDataset(1, dataset2);
		plot.mapDatasetToRangeAxis(1, 1);
		XYItemRenderer renderer2 = new StandardXYItemRenderer();
		plot.setRenderer(1, renderer2);
		
		// for AXIS 3
		NumberAxis axis3 = new NumberAxis("Mahalanobis distance");
		plot.setRangeAxis(2, axis3);

		XYDataset dataset3 = createChartData3(md_dataset);
		plot.setDataset(2, dataset3);
		plot.mapDatasetToRangeAxis(2, 2);
		XYItemRenderer renderer3 = new StandardXYItemRenderer();
		plot.setRenderer(2, renderer3);
		
		ChartUtilities.applyCurrentTheme(chart);
		
		// change the series and axis colours after the theme has been applied...
		plot.getRenderer().setSeriesPaint(0, Color.red);
		renderer2.setSeriesPaint(0, Color.green);
		axis2.setLabelPaint(Color.green);
		axis2.setTickLabelPaint(Color.green);
		renderer3.setSeriesPaint(0, Color.blue);
		axis3.setLabelPaint(Color.blue);
		axis3.setTickLabelPaint(Color.blue);
		
		// set line visible
		//XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)((XYPlot) chart.getPlot()).getRenderer();
		//renderer.setSeriesLinesVisible(0, false);
		//renderer.setSeriesLinesVisible(1, false);
		//renderer.setSeriesLinesVisible(2, false);
		//renderer.setBaseShapesVisible(false);
		
		// create chart Panel
		ChartPanel basePanel = new ChartPanel(chart);
		return basePanel;
		
	} // end of method
	
	
	/**
	 * Create Chart Data
	 */
	private XYDataset createChartData(int itemCode1, int itemCode2, TmsDataset dataSet, boolean isCorrectedValue) {
		
		XYSeries series = new XYSeries("correlation");
		TmsDataRecord[] records = dataSet.getTmsDataRecords();
		double x, y;
		for (TmsDataRecord record : records) {
			if (isCorrectedValue) {
				x = record.getDataCell(itemCode1).getData_value();
				y = record.getDataCell(itemCode2).getData_value();
			} else {
				x = record.getDataCell(itemCode1).getOrigin_value();
				y = record.getDataCell(itemCode2).getOrigin_value();
			}
			if (x == -1.0D || y == -1.0D) {
				continue;
			} else {
				series.add(x, y);
			}
		}
		return new XYSeriesCollection(series);
		
	} // end of method

	/**
	 * Create Chart Data
	 */
	private XYDataset createChartData(TmsDataCell4Mahalanobis[] md_dataset) {
		
		XYSeries series = new XYSeries("correlation");
		double x, y;
		for (TmsDataCell4Mahalanobis record : md_dataset) {
			x = record.getMahalanobisDistance();
			y = record.getImverseChiSquare();
			series.add(x, y);
		}
		return new XYSeriesCollection(series);
		
	} // end of method
	
	/**
	 * Create Chart Data
	 */
	private XYDataset createChartData1(TmsDataCell4Mahalanobis[] md_dataset) {
		
		TimeSeries series = new TimeSeries(ItemCodeDefine.getItemName(md_dataset[0].getData1_item()));

		Minute time = null;
		for (int i=0; i<md_dataset.length ; i++) {
			time = new Minute(new java.util.Date(md_dataset[i].getTime()));
			series.add(time, md_dataset[i].getData1_value());
		}

		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(series);
		return dataset;
		
	} // end of method
	
	/**
	 * Create Chart Data
	 */
	private XYDataset createChartData2(TmsDataCell4Mahalanobis[] md_dataset) {
		
		TimeSeries series = new TimeSeries(ItemCodeDefine.getItemName(md_dataset[0].getData2_item()));

		Minute time = null;
		for (int i=0; i<md_dataset.length ; i++) {
			time = new Minute(new java.util.Date(md_dataset[i].getTime()));
			series.add(time, md_dataset[i].getData2_value());
		}

		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(series);
		return dataset;
		
	} // end of method
	
	/**
	 * Create Chart Data
	 */
	private XYDataset createChartData3(TmsDataCell4Mahalanobis[] md_dataset) {
		
		TimeSeries series = new TimeSeries("Mahalanobis distance");

		Minute time = null;
		for (int i=0; i<md_dataset.length ; i++) {
			time = new Minute(new java.util.Date(md_dataset[i].getTime()));
			series.add(time, md_dataset[i].getMahalanobisDistance());
		}

		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(series);
		return dataset;
		
	} // end of method
	
	
	/**
	 * computePearsonCorrelation
	 */
	private double[] computePearsonCorrelation(int itemCode1, int itemCode2, TmsDataset dataSet, boolean isCorrectedValue) {
		
		// allocate enough memory
		double[][] data = new double[dataSet.getTmsDataRecordCount()][2];
		// set data
		double x, y;
		int index = 0;
		TmsDataRecord[] records = dataSet.getTmsDataRecords();
		for (TmsDataRecord record : records) {
			if (isCorrectedValue) {
				x = record.getDataCell(itemCode1).getData_value();
				y = record.getDataCell(itemCode2).getData_value();
			} else {
				x = record.getDataCell(itemCode1).getOrigin_value();
				y = record.getDataCell(itemCode2).getOrigin_value();
			}
			if (x == -1.0D || y == -1.0D) {
				continue;
			} else {
				data[index][0] = x;
				data[index][1] = y;
				index++;
			}
		}
		// trim
		double[][] trimedData = Arrays.copyOf(data, index);
		// compute pearson-correlation
		PearsonsCorrelation pearsonsCorrelation = new PearsonsCorrelation(trimedData);
		double[] correlationValue = new double[2];
		correlationValue[0] = pearsonsCorrelation.getCorrelationMatrix().getEntry(0, 1);
		correlationValue[1] = pearsonsCorrelation.getCorrelationPValues().getEntry(0, 1);;
		
		return correlationValue;
		
	} // end of method
	
	
	/**
	 * create statistic result table
	 */
	private JScrollPane createTablePanel(TmsDataCell4Mahalanobis[] md_dataset) {
		
		// create table
		JTable dataTable = new JTable(new TableModel(md_dataset));
		dataTable.setRowHeight(20);
		dataTable.getColumnModel().getColumn(0).setPreferredWidth(100);
		dataTable.getColumnModel().getColumn(1).setPreferredWidth(200);
		dataTable.getColumnModel().getColumn(2).setPreferredWidth(100);
		dataTable.getColumnModel().getColumn(3).setPreferredWidth(100);
		dataTable.getColumnModel().getColumn(4).setPreferredWidth(100);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );

		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		dataTable.getColumnModel().getColumn(0).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		dataTable.getColumnModel().getColumn(2).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(3).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(4).setCellRenderer( rightRenderer );
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(dataTable);

		return basePane;
		
	} // end of method
	

	//==========================================================================
	// Inner Class
	//==========================================================================
	class TableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private String[] columnNames = {
			"no.", "time", "item1", "item2", "md"
		};
		
		private TmsDataCell4Mahalanobis[] dataset = null;

		public TableModel(TmsDataCell4Mahalanobis[] dataset) {
			this.dataset = dataset;
			this.columnNames[2] = ItemCodeDefine.getItemName(dataset[0].getData1_item());
			this.columnNames[3] = ItemCodeDefine.getItemName(dataset[0].getData2_item());
		}
		
		public String getColumnName(int col) {
			return columnNames[col];
		}
		
		public int getRowCount() { 
			return dataset.length;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			switch(col) {
			case 0:  return  row;
			case 1:  return  DateUtil.getDateStr(dataset[row].getTime());
			case 2:  return  NumberUtil.getNumberStr(dataset[row].getData1_value());
			case 3:  return  NumberUtil.getNumberStr(dataset[row].getData2_value());
			case 4:  return  NumberUtil.getNumberStr(dataset[row].getMahalanobisDistance());
			default: return  "";
			}

		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class

	
} // end of class
