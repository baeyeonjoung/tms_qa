/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.PatternDataSet;
import com.snu.tms.qa.model.ruleset.PatternDataRecord;
import com.snu.tms.qa.util.NumberUtil;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * Show Graph
 * 
 */
public class DialogPatternDataView extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private PatternDataSet patternDataSet = null;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogPatternDataView(
			JFrame parent, 
			String stackInfoStr,
			PatternDataSet dataSet) {
		
		// set dialog
		super(parent, "Pattern Analysis(Data View) : " + stackInfoStr, ModalityType.MODELESS);

		// set icon
		try {
			setIconImage(ImageIO.read(new File("image/smart.png")));
		} catch (IOException exc) {}

		// set
		this.patternDataSet = dataSet;
		
		// create table
		JTable dataTable = new JTable(new TableModel(this.patternDataSet));
		dataTable.setRowHeight(20);
		//dataTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		dataTable.getColumnModel().getColumn(0).setPreferredWidth(130);
		dataTable.getColumnModel().getColumn(1).setPreferredWidth(60);
		dataTable.getColumnModel().getColumn(2).setPreferredWidth(150);
		int[] items = dataSet.getItems();
		for (int i=0 ; i<items.length ; i++) {
			if (items[i] == ItemCodeDefine.FL1) {
				dataTable.getColumnModel().getColumn(i+1).setPreferredWidth(100);
			} else {
				dataTable.getColumnModel().getColumn(i+1).setPreferredWidth(60);
			}
		}
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );

		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		dataTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		dataTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		dataTable.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		for (int i=0 ; i<items.length ; i++) {
			dataTable.getColumnModel().getColumn(i+3).setCellRenderer( rightRenderer );
		}
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(dataTable);
		this.setPreferredSize(new Dimension(930, 870));
		this.setLayout(new BorderLayout());
		this.add(basePane, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class TableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		private String[] columnNames;
		
		private PatternDataRecord[] records;
		private PatternDataSet      dataset;
		
		public TableModel(PatternDataSet dataset) {
			
			// set dataset
			this.dataset = dataset;
			this.records = dataset.getAllSortedPatternDataRecord();
			
			// set column names
			int[] items = dataset.getItems();
			this.columnNames = new String[items.length + 3];
			columnNames[0] = "Time";
			columnNames[1] = "Pattern";
			columnNames[2] = "PatternName";
			for (int i=0 ; i<items.length ; i++) {
				this.columnNames[i+3] = ItemCodeDefine.getItemName(items[i]);
			}
		}
		
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			if (records == null) {
				return 0;
			}
			return records.length;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return records[row].getTimestr();
			} else if (col == 1) {
				return records[row].getPatternIndex();
			} else if (col == 2) {
				return this.dataset.getPatternName(records[row].getPatternIndex());
			} else {
				return NumberUtil.getNumberStr(records[row].getValue(col-3));
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	
	//==========================================================================
	// Inner Class
	//==========================================================================

	
} // end of class
