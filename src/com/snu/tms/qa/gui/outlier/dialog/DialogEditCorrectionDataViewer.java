/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import com.snu.tms.qa.analysis.outlier.DoAnalysisReverseCorrection;
import com.snu.tms.qa.model.CorrectionItemInfo;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.TmsDataCell;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.util.DateUtil;

import java.awt.Dimension;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * Left Panel for stack selection
 * 
 */
public class DialogEditCorrectionDataViewer extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	private static final DecimalFormat valueFormatter = new DecimalFormat("#,###,###,##0.00");
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  TmsDataset     dataSet;
	private  StackInfo      stackInfo;
	private  CorrectionItemInfo correctionInfo;
	private  int            item      = ItemCodeDefine.UNDEFINED;
	private  double[][]     values    = null;
	private  long[]         times     = null;

	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogEditCorrectionDataViewer(
			JFrame     parent, 
			StackInfo  stackInfo,
			TmsDataset dataSet,
			int        selectedItem) {
		
		// set dialog
		super(parent, 
				"correction before value vs. user defined correction value", 
				ModalityType.MODELESS);
		
		// set icon
		try {
			setIconImage(ImageIO.read(new File("image/smart.png")));
		} catch (IOException exc) {}

		// Set parameters 
		this.dataSet   = dataSet;
		this.stackInfo = stackInfo;
		this.item      = selectedItem;
		
		// get correctionInfo
		this.correctionInfo = this.stackInfo.getCorrectionInfo(this.item);
		if (this.correctionInfo == null) {
			this.correctionInfo = new CorrectionItemInfo(this.item, -1.0D, -1.0D, 0, 0, 0);
		}

		// set data
		TmsDataRecord[] dataRecords = this.dataSet.getTmsDataRecords();
		TmsDataCell     dataCell, o2DataCell, tmpDataCell;

		this.times  = this.dataSet.getTmsDataRecordTimes();
		this.values = new double[dataRecords.length][5]; 
		// o2_value, temp_value, af_value, bf_value, computed_value
		for (int i=0 ; i<this.values.length ; i++) {
			
			// set value
			dataCell = dataRecords[i].getDataCell(item);
			this.values[i][2] = dataCell.getData_value();
			this.values[i][3] = dataCell.getOrigin_value();
			
			// do reverse correction
			o2DataCell = dataRecords[i].getDataCell(ItemCodeDefine.O2);
			if (o2DataCell != null) {
				this.values[i][0] = o2DataCell.getData_value();
			} else {
				this.values[i][0] = -1.0D;
			}
			tmpDataCell = dataRecords[i].getDataCell(ItemCodeDefine.TMP);
			if (tmpDataCell != null) {
				this.values[i][1] = tmpDataCell.getData_value();
			} else {
				this.values[i][1] = -1.0D;
			}
			
			this.values[i][4] = DoAnalysisReverseCorrection.doReverseCorrection(
					this.item, this.values[i][2],
					correctionInfo.getStdOxygen(), correctionInfo.getStdMoisture(),
					this.values[i][0], this.values[i][1],
					correctionInfo.isOxygenApplied(),
					correctionInfo.isTemperatureApplied(),
					correctionInfo.isMoistureApplied() );
			
		} // end of for-loop
		
		// Set JTable
		TableModel tableModel = new TableModel();
		JTable dataTable = new JTable(tableModel);
		dataTable.setRowHeight(20);
		//dataTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		dataTable.getColumnModel().getColumn(0).setPreferredWidth(145);
		dataTable.getColumnModel().getColumn(1).setPreferredWidth(70);
		dataTable.getColumnModel().getColumn(2).setPreferredWidth(70);
		dataTable.getColumnModel().getColumn(3).setPreferredWidth(50);
		dataTable.getColumnModel().getColumn(4).setPreferredWidth(50);
		dataTable.getColumnModel().getColumn(5).setPreferredWidth(50);
		dataTable.getColumnModel().getColumn(6).setPreferredWidth(90);
		dataTable.getColumnModel().getColumn(7).setPreferredWidth(90);
		dataTable.getColumnModel().getColumn(8).setPreferredWidth(90);
		dataTable.getColumnModel().getColumn(9).setPreferredWidth(90);
		dataTable.getColumnModel().getColumn(10).setPreferredWidth(90);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );

		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		dataTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		dataTable.getColumnModel().getColumn(1).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(2).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(6).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(7).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(8).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(9).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(10).setCellRenderer( rightRenderer );
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(dataTable);
		basePane.setPreferredSize(new Dimension(900, 700));
		this.add(basePane);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================

	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class TableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		private final String[] columnNames = {
				"Time", "Std.Oxgn", "Std.Mois", 
				"Oxgn", "Temp", "Mois",
				"value(O2)", "value(Temp)", 
				"af_value", "bf_value", "corr_value" };

		public TableModel() { } // end of constructor
		
		public String getColumnName(int col) {
			
			if (col < 8) {
				return columnNames[col];
			} else {
				return columnNames[col] + "(" + ItemCodeDefine.getItemName(item) + ")";
			}
		}
		
		public int getRowCount() { 
			return values.length;
		}
		
		public int getColumnCount() { 
			return columnNames.length;
		}
		
		public Object getValueAt(int row, int col) {
			
			double value = 0.0D;
			if (col == 0) {
				return DateUtil.getDateStr(times[row]);
			} else if (col == 1) {
				value = correctionInfo.getStdOxygen();
				if (value == -1.0D) {
					return "";
				} else {
					return valueFormatter.format(value);
				}
			} else if (col == 2) {
				value = correctionInfo.getStdMoisture();
				if (value == -1.0D) {
					return "";
				} else {
					return valueFormatter.format(value);
				}
			} else if (col == 3) {
				return new Boolean(correctionInfo.isOxygenApplied());
			} else if (col == 4) {
				return new Boolean(correctionInfo.isTemperatureApplied());
			} else if (col == 5) {
				return new Boolean(correctionInfo.isMoistureApplied());
			} else if (col == 6) {
				value = values[row][0];
				if (value == -1.0D) {
					return "";
				} else {
					return valueFormatter.format(value);
				}
			} else if (col == 7) {
				value = values[row][1];
				if (value == -1.0D) {
					return "";
				} else {
					return valueFormatter.format(value);
				}
			} else if (col == 8) {
				value = values[row][2];
				if (value == -1.0D) {
					return "";
				} else {
					return valueFormatter.format(value);
				}
			} else if (col == 9) {
				value = values[row][3];
				if (value == -1.0D) {
					return "";
				} else {
					return valueFormatter.format(value);
				}
			} else if (col == 10) {
				value = values[row][4];
				if (value == -1.0D) {
					return "";
				} else {
					return valueFormatter.format(value);
				}
			} else {
				return "";
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 3 || col == 4 || col == 5) {
				return Boolean.class;
			} else {
				return String.class;
			}
			
		}
		
	} // end of class
	
	
} // end of class
