/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.event.ChartProgressEvent;
import org.jfree.chart.event.ChartProgressListener;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import com.snu.tms.qa.fileio.OutlierDetectionResultWriterAsCsvFormat;
import com.snu.tms.qa.model.DatasetManager;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.PatternDataHistogram;
import com.snu.tms.qa.model.ruleset.DetectionDataCell;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.DetectionRuleset;
import com.snu.tms.qa.model.ruleset.OdrRecord;
import com.snu.tms.qa.model.ruleset.RulesetModel;
import com.snu.tms.qa.model.ruleset.RulesetWeightingModel;
import com.snu.tms.qa.util.DateUtil;


/**
 * Show outlier detection reuslts
 * 
 */
public class DialogOutlierDetectionResult extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  DatasetManager          datasetManager;
	private  RulesetWeightingModel  rulesetWeighingModel;
	
	private  int[]                   items;
	
	private  DetectionRecord[]       detectionRecords;
	private  OdrRecord[]             odrRecords;
	
	private  int                     selectedItem = 0;
	
	private  JPanel                  timeseriesBasePanel;

	private  JTable                  summaryTable;
	private  JTable                  detailTable;
	private  JTable                  rawDataTable;
	
	private  JPanel                  histogramBasePanel;
	
	private  Hashtable<Long, Integer> detectionRecordTimeHash;
	private  Hashtable<Long, Integer> odrRecordTimeHash;
	
	private  DetectionRecord          selectedDetectionRecord = null;
	private  OdrRecord                selectedOdrRecord = null;
	
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogOutlierDetectionResult(JFrame parent) {
		
		// set dialog
		super(parent, "", ModalityType.MODELESS);
		this.setTitle("Outlier detection result");
		
		// set parameters
		this.datasetManager = DatasetManager.getInstance();
		this.rulesetWeighingModel = this.datasetManager.getRulesetWeighingModel();
		this.items = this.datasetManager.getItems();
		
		// create OdrModel
		this.odrRecordTimeHash = new Hashtable<Long, Integer>();
		this.odrRecords = createOdrModel();
		
		// create detection records
		this.detectionRecordTimeHash = new Hashtable<Long, Integer>();
		this.detectionRecords = createDetectionModel();
		
		// set layout
		JPanel       controlPanel        = createControlPanel();
		this.timeseriesBasePanel         = new JPanel(new BorderLayout());
		this.timeseriesBasePanel.add(createTimeseriesGraphPanel(), BorderLayout.CENTER);
		JScrollPane  summaryTablePanel   = createSummaryTablePanel();
		JScrollPane  rawDataTablePanel   = createRawDataTablePanel();
		JScrollPane  detailTablePanel    = createDetailTablePanel();
		this.histogramBasePanel          = new JPanel(new BorderLayout());
		
		JPanel       upperBasePanel = new JPanel(new BorderLayout());
		JSplitPane   lowerBasePanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		lowerBasePanel.setDividerLocation(550);
		lowerBasePanel.setDividerSize(5);
		
		JSplitPane   lowerLeftBasePanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		lowerLeftBasePanel.setDividerLocation(180);
		lowerLeftBasePanel.setDividerSize(5);
		
		JPanel       lowerLeftContentsPanel = new JPanel(new BorderLayout());
		lowerLeftContentsPanel.add(summaryTablePanel, BorderLayout.NORTH);
		lowerLeftContentsPanel.add(rawDataTablePanel, BorderLayout.CENTER);
		summaryTablePanel.setPreferredSize(new Dimension(180, 125));
		
		lowerLeftBasePanel.add(lowerLeftContentsPanel);
		lowerLeftBasePanel.add(detailTablePanel);
		
		lowerBasePanel.add(lowerLeftBasePanel);
		lowerBasePanel.add(this.histogramBasePanel);
		
		upperBasePanel.add(controlPanel, BorderLayout.NORTH);
		upperBasePanel.add(this.timeseriesBasePanel, BorderLayout.CENTER);
		
		upperBasePanel.setPreferredSize(new Dimension(1100, 350));
		//lowerBasePanel.setPreferredSize(new Dimension(1000, 450));
		
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(1100, 800));
		this.add(upperBasePanel, BorderLayout.NORTH);
		this.add(lowerBasePanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create time series graph panel
	 */
	private JPanel createTimeseriesGraphPanel() {
		
		// create XYDataSet
		XYDataset chartDataSet = createTimeseriesDataSet();
		
		// create chart
		final JFreeChart chart = ChartFactory.createTimeSeriesChart(
				"",           // title
				"",           // x-axis label
				ItemCodeDefine.getItemName(this.selectedItem),           // y-axis label
				chartDataSet, // data
				true,        // create legend?
				true,         // generate tooltips?
				false         // generate URLs?
		);

		// set Font
		chart.getTitle().setFont(new Font("����", Font.BOLD, 14));
		chart.getLegend().setItemFont(new Font("����", Font.BOLD, 12));

		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setNoDataMessage("NO DATA");
		plot.setDomainPannable(true);
		plot.setRangePannable(false);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);
		
		// set chart shape properties
		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		plot.setRangeCrosshairVisible(true);
		plot.setRangeCrosshairLockedOnData(true);
		plot.setDomainZeroBaselineVisible(true);
		plot.setRangeZeroBaselineVisible(true);
		
		XYDotRenderer renderer = new XYDotRenderer();
		renderer.setDotWidth(3);
		renderer.setDotHeight(3);
		plot.setRenderer(renderer);
		Color[] paintColors = {Color.GRAY, Color.GREEN, Color.BLUE, Color.YELLOW, Color.RED};
		for (int i=0 ; i<5 ; i++) {
			renderer.setSeriesPaint(i, paintColors[i]);
		}
		
		// set x-axis properties
		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("yy/MM/dd HH:mm"));
		
		// Add chart listener
		chart.addProgressListener( new ChartProgressListener() {
			public void chartProgress(ChartProgressEvent event) {
				
				if (event.getType() != ChartProgressEvent.DRAWING_FINISHED) {
					return;
				}
				
				// get mouse click domain axis point
				XYPlot plot = (XYPlot) chart.getPlot();
				double xx = plot.getDomainCrosshairValue();
				
				// change table selection
				changeTableSelection((long)xx);
				
			} // end of method
		});

		// return
		return new ChartPanel(chart);	
		
	} // end of method
	
	/**
	 * create Control panel
	 */
	private JPanel createControlPanel() {
		
		// create base panel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(1000, 30));
		
		// create JCombo
		int[] items = this.datasetManager.getPatternDataSet().getItems();
		String[] itemNames = new String[items.length];
		for (int i=0 ; i<items.length ; i++) {
			itemNames[i] = ItemCodeDefine.getItemName(items[i]);
		}
		
		final JComboBox<String> itemSelectionCombo = new JComboBox<String>(itemNames);
		itemSelectionCombo.setBounds(20, 5, 100, 20);
		basePanel.add(itemSelectionCombo);
		
		// set selected item
		this.selectedItem = items[0];
		itemSelectionCombo.setSelectedIndex(0);
		
		itemSelectionCombo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				
				@SuppressWarnings("rawtypes")
				int selectedIndex = ((JComboBox)e.getSource()).getSelectedIndex();
				selectedItem = datasetManager.getPatternDataSet().getItems()[selectedIndex];
				updateTimeseriesGraph();
				
			}
		});
		
		// add 30-min data evaluation button
		JButton evaluationButton = new JButton("compare 30-min data");
		evaluationButton.setBounds(150, 5, 200, 20);
		evaluationButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				compareWith30minData();
			}
		});
		basePanel.add(evaluationButton);
		
		// add save button
		JButton exportCsvButton = new JButton("export csv format");
		exportCsvButton.setBounds(380, 5, 200, 20);
		exportCsvButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exportCsvFormat();
			}
		});
		basePanel.add(exportCsvButton);
		
		// return
		return basePanel;
		
	} // end of method
	
	/**
	 * create window for compare with 30-min data
	 */
	private void compareWith30minData() {
		new DialogOutlierDetectionCompare(this, this.odrRecords);
	} // end of method
	
	/**
	 * export reult data as a CSV format
	 */
	private void exportCsvFormat() {
		
		// get fact_code and stck_code
		RulesetModel rulesetModel = this.datasetManager.getRulesetModel();
		if (rulesetModel == null || this.detectionRecords == null || this.detectionRecords.length == 0) {
			JOptionPane.showMessageDialog(
					SwingUtilities.getWindowAncestor(this), 
					"Outlier detection model doesn't estabilished yet.");
		}
		String fact_code = rulesetModel.getFactCode();
		int    stck_code = rulesetModel.getStckCode();
		String fileName = String.format("OutlierDetectionResult-%s-%d", fact_code, stck_code);
		
		// Show JFileChooser
		JFileChooser fc = new JFileChooser();
		fc.setDialogType(JFileChooser.SAVE_DIALOG);
		fc.setSelectedFile(new File(fileName + ".csv"));
		fc.setFileFilter(new FileNameExtensionFilter("csv file","csv"));
		
		int returnVal = fc.showSaveDialog((JFrame)this.getParent());
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			OutlierDetectionResultWriterAsCsvFormat.writeDataFile(file.getAbsolutePath(), items, this.detectionRecords, odrRecords);
		}
		
	} // end of method
	
	/**
	 * update chart panel
	 */
	private void updateTimeseriesGraph() {
		
		// chage chart panel
		this.timeseriesBasePanel.removeAll();
		this.timeseriesBasePanel.add(createTimeseriesGraphPanel(), BorderLayout.CENTER);
		this.timeseriesBasePanel.updateUI();
		
	} // end of method
	
	/**
	 * update histogram base panel
	 */
	private void updateHistogramBasePanel() {
		
		// chage chart panel
		this.histogramBasePanel.removeAll();
		this.histogramBasePanel.add(createHistogramPanel(), BorderLayout.CENTER);
		this.histogramBasePanel.updateUI();
		
	} // end of method
	
	/**
	 * Create graph data set
	 */
	private XYDataset createTimeseriesDataSet() {
		
		// create TimeSeries
		String[] levelStr = {"NORMAL", "WARNING", "MINOR", "MAJOR", "CRITICAL"};
		TimeSeries[] timeseries = new TimeSeries[levelStr.length];
		for (int i=0 ; i<timeseries.length ; i++) {
			timeseries[i] = new TimeSeries(levelStr[i]);
		}
		
		// Set data
		Minute time = null;
		int    level;
		double value;
		for (int i=0 ; i<this.odrRecords.length ; i++) {
			
			level = this.odrRecords[i].getOutlierLevel();
			value = this.detectionRecords[i].getRawDataCell(this.selectedItem).getData_value();
			
			time  = new Minute(new java.util.Date(this.odrRecords[i].time));
			timeseries[level].add(time, value);
		
		} // end of for-loop
		
		// Create TimeSeriesCollection
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		for (int i=0 ; i<timeseries.length ; i++) {
			dataset.addSeries(timeseries[i]);
		}
		
		// return
		return dataset;
		
	} // end of method
	
	/**
	 * create summary table pane
	 */
	private JScrollPane createSummaryTablePanel() {
		
		// Create Table
		SummaryTableModel tableModel = new SummaryTableModel();
		this.summaryTable = new JTable(tableModel);
		this.summaryTable.setRowHeight(20);
		//this.summaryTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		this.summaryTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.summaryTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );

		this.summaryTable.getColumnModel().getColumn(0).setPreferredWidth(90);
		this.summaryTable.getColumnModel().getColumn(1).setPreferredWidth(90);

		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.summaryTable);
		return basePane;
		
		
	} // end of method
	
	/**
	 * create raw data table pane
	 */
	private JScrollPane createRawDataTablePanel() {
		
		// Create Table
		RawDataTableModel tableModel = new RawDataTableModel();
		this.rawDataTable = new JTable(tableModel);
		this.rawDataTable.setRowHeight(20);
		//this.rawDataTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		this.rawDataTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.rawDataTable.getColumnModel().getColumn(1).setCellRenderer( rightRenderer );

		this.rawDataTable.getColumnModel().getColumn(0).setPreferredWidth(90);
		this.rawDataTable.getColumnModel().getColumn(1).setPreferredWidth(90);

		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.rawDataTable);
		return basePane;
		
	} // end of method
	
	/**
	 * create outlier detail table pane
	 */
	private JScrollPane createDetailTablePanel() {
		
		// Create Table
		DetailTableModel tableModel = new DetailTableModel();
		this.detailTable = new JTable(tableModel);
		this.detailTable.setRowHeight(20);
		//this.summaryTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		this.detailTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.detailTable.getColumnModel().getColumn(2).setCellRenderer( rightRenderer );
		this.detailTable.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
		
		this.detailTable.getColumnModel().getColumn(0).setPreferredWidth(130);
		this.detailTable.getColumnModel().getColumn(1).setPreferredWidth(50);
		this.detailTable.getColumnModel().getColumn(2).setPreferredWidth(60);
		this.detailTable.getColumnModel().getColumn(3).setPreferredWidth(130);
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.detailTable);
		return basePane;
		
		
	} // end of method
	
	/**
	 * create Histogram Panel
	 */
	private JScrollPane createHistogramPanel() {
		
		// check selected data
		if (this.selectedDetectionRecord == null) {
			return new JScrollPane();
		}
		
		// get selected pattern and histogram data
		int patternIndex = this.selectedDetectionRecord.getPatternId();
		PatternDataHistogram[] patternHistograms = 
				this.datasetManager.getPatternData(patternIndex).getAllPatternDataHistogram();
		if (patternHistograms == null) {
			System.out.println("selected pattern histogram doesn't exist.");
			return new JScrollPane();
		}
		int size = (int) Math.ceil( ((double)patternHistograms.length) / 2.0D);
		
		// create basePanel
		JPanel basePanel = new JPanel(new GridLayout(0, 2));
		basePanel.setPreferredSize(new Dimension(450, 180 * size));
		
		// create histogram and add basePanel
		JPanel chartPanel;
		for (int i=0 ; i<patternHistograms.length ; i++) {
			chartPanel = createPatternHistogramGraph(patternHistograms[i]);
			chartPanel.setPreferredSize(new Dimension(200, 180));
			basePanel.add(chartPanel);
		}
		return new JScrollPane(basePanel);
		
	} // end of method
	
	/**
	 * create PatternHistogram graph
	 */
	private JPanel createPatternHistogramGraph(PatternDataHistogram histogramData) {
		
		// get parameters
		double[][] densities = histogramData.getDensities();
		int        item      = histogramData.getItem();
		String     itemStr   = ItemCodeDefine.getItemName(item);
		
		// prepare HistogramDataset
		HistogramDataset dataset = new HistogramDataset();
		int dataCount = 2000;
		double densitySum = 0.0D;
		int maxN = 0;
		for (int i=0 ; i<densities.length ; i++) {
			densitySum += densities[i][1];
		}

		double densityMultiplier = 0.0D;
		if (densitySum != 0.0D) {
			densityMultiplier = ((double)dataCount) / densitySum;
		}
		
		double[] values = new double[dataCount];
		int count = 0;
		int N     = 0;
		for (int i=0 ; i<densities.length ; i++) {
			N = (int) (densities[i][1] * densityMultiplier);
			for (int j=0 ; j<N ; j++) {
				if (count >= dataCount) {
					break;
				}
				values[count++] = densities[i][0];
			}
			if (N > maxN) {
				maxN = N;
			}
		}

		values = Arrays.copyOf(values, count);
		if (histogramData.getBinNo() <= 1) {
			dataset.addSeries(itemStr, values, 1);
		} else {
			dataset.addSeries(itemStr, values, histogramData.getBinNo()-1);
		}
		
		// add target record
		double targetValue = this.selectedDetectionRecord.getRawDataCell(item).getData_value();
		double[] targetValues = new double[maxN];
		Arrays.fill(targetValues, targetValue);
		dataset.addSeries("target record", targetValues, 1);
		
		// create histogram chart
		JFreeChart chart = ChartFactory.createHistogram(
				"",
				null,
				null,
				dataset,
				PlotOrientation.VERTICAL,
				true,
				true,
				false);
		
		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setDomainPannable(true);
		plot.setRangePannable(true);
		plot.setForegroundAlpha(0.85f);
		NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
		yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
		renderer.setDrawBarOutline(false);
		renderer.setBarPainter(new StandardXYBarPainter());
		renderer.setShadowVisible(false);
		
		/**
		// add a category marker
		CategoryMarker marker = new CategoryMarker(new Double(targetValue), Color.blue, new BasicStroke(5.0f));
		marker.setDrawAsLine(true);
		marker.setLabel("Target Record");
		marker.setLabelFont(new Font("Dialog", Font.PLAIN, 11));
		marker.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
		marker.setLabelOffset(new RectangleInsets(2, 5, 2, 5));
		plot.addDomainMarker(marker, Layer.BACKGROUND);
		*/

		// return
		return new ChartPanel(chart);

	} // end of method
	
	/**
	 * create OdrModel
	 */
	private OdrRecord[] createOdrModel() {
		
		// get Pattern Data model
		HashSet<String> multiRuleHashSet = new HashSet<String>();
		HashSet<String> biRuleHashSet    = new HashSet<String>();
		HashSet<String> uniRuleHashSet   = new HashSet<String>();
		HashSet<String> corrRuleHashSet  = new HashSet<String>();

		DetectionPatternDataset[] patternDatasetArray = this.datasetManager.getAllPatternData();
		DetectionRuleset[] rulesets;
		for (int i=0 ; i<patternDatasetArray.length ; i++) {
			
			rulesets = patternDatasetArray[i].getAllRuleset();
			for (int j=0 ; j<rulesets.length ; j++) {
				
				switch(rulesets[j].getRulesetType()) {
				case DetectionRuleset.RULESET_PCA_TSS:
					multiRuleHashSet.add(rulesets[j].getKeyName());
					break;
				case DetectionRuleset.RULESET_PCA_SPE:
					multiRuleHashSet.add(rulesets[j].getKeyName());
					break;
				case DetectionRuleset.RULESET_CORR:
					biRuleHashSet.add(rulesets[j].getKeyName());
					break;
				case DetectionRuleset.RULESET_SPC:
					uniRuleHashSet.add(rulesets[j].getKeyName());
					break;
				case DetectionRuleset.RULESET_PD:
					corrRuleHashSet.add(rulesets[j].getKeyName());
					break;
				
				}
			}
		}
		
		Hashtable<String, Integer> rulesetOrderHash = new Hashtable<String, Integer>();
		int index = 0;
		String key = "";
		for (Iterator<String> e=multiRuleHashSet.iterator() ; e.hasNext() ; ) {
			key = e.next();
			rulesetOrderHash.put(key, index++);
		}
		for (Iterator<String> e=biRuleHashSet.iterator() ; e.hasNext() ; ) {
			key = e.next();
			rulesetOrderHash.put(key, index++);
		}
		for (Iterator<String> e=uniRuleHashSet.iterator() ; e.hasNext() ; ) {
			key = e.next();
			rulesetOrderHash.put(key, index++);
		}
		for (Iterator<String> e=corrRuleHashSet.iterator() ; e.hasNext() ; ) {
			key = e.next();
			rulesetOrderHash.put(key, index++);
		}
		// add record
		Vector<OdrRecord> odrRecordArray = new Vector<OdrRecord>();
		DetectionRecord[] detectionRecords;
		String factCode, factName, patternName;
		int    stckCode, patternId;
		long   time;
		OdrRecord  odrRecord;
		for (int i=0 ; i<patternDatasetArray.length ; i++) {
			
			factCode    = patternDatasetArray[i].getFactoryId();
			factName    = patternDatasetArray[i].getFactoryName();
			stckCode    = patternDatasetArray[i].getStackId();
			patternName = patternDatasetArray[i].getPatternName();
			patternId   = patternDatasetArray[i].getPatternId();
			detectionRecords = patternDatasetArray[i].getAllDetectionRecords();
			for (int j=0 ; j<detectionRecords.length ; j++) {
				
				// create odrRecord
				time = detectionRecords[j].getTime();
				odrRecord = new OdrRecord(
						factCode, factName, stckCode, 
						time, patternId, patternName, rulesetOrderHash,
						rulesetWeighingModel);
				odrRecordArray.add(odrRecord);
				
				// set odrCell
				if (!patternDatasetArray[i].isEnabled()) {
					continue;
				}
				DetectionDataCell[] outlierCells = detectionRecords[j].getAllDetectionDataCell();
				int  odrCellIndex;
				for (int k=0 ; k<outlierCells.length ; k++) {
					if (!outlierCells[k].getRuleset().isRulesetEnabled()) {
						continue;
					}
					odrCellIndex = rulesetOrderHash.get(outlierCells[k].getRuleset().getKeyName());
					odrRecord.odrCells[odrCellIndex].setIsOutlier(outlierCells[k].isOutlier());
				}
			}
		}
		
		// create return value
		OdrRecord[] odrRecords = odrRecordArray.toArray(new OdrRecord[odrRecordArray.size()]);
		Arrays.sort(odrRecords);
		
		// create timeHash
		createOdrRecordTimeHashtable(odrRecords);
		
		return odrRecords;
		
	} // end of method
	
	/**
	 * create detection decords
	 */
	private DetectionRecord[] createDetectionModel() {
		
		// get detectionPatternDataset
		DetectionPatternDataset[] patternDatasets = datasetManager.getAllPatternData();
		// set data
		Vector<DetectionRecord> detectionRecordArray = new Vector<DetectionRecord>();
		DetectionRecord[] subRecords;
		for (int i=0 ; i<patternDatasets.length ; i++) {
			subRecords = patternDatasets[i].getAllDetectionRecords();
			for (int j=0 ; j<subRecords.length ; j++) {
				detectionRecordArray.add(subRecords[j]);
			}
		}
		
		DetectionRecord[] detectionRecords = detectionRecordArray.toArray(new DetectionRecord[detectionRecordArray.size()]);
		Arrays.sort(detectionRecords);
		createDetectionRecordTimeHashtable(detectionRecords);
		
		return detectionRecords;
		
	} // end of method
	
	/**
	 * Create time hashtable
	 */
	private void createOdrRecordTimeHashtable(OdrRecord[] records) {
		
		// set data
		for (int i=0 ; i<records.length ; i++) {
			this.odrRecordTimeHash.put(records[i].time, i);
		}
		
	} // end of method

	
	/**
	 * Create time hashtable
	 */
	private void createDetectionRecordTimeHashtable(DetectionRecord[] records) {
		
		// set data
		for (int i=0 ; i<records.length ; i++) {
			this.detectionRecordTimeHash.put(records[i].getTime(), i);
		}
		
	} // end of method
	
	/**
	 * Table Selection
	 */
	private void changeTableSelection(long selectedTime) {

		// get selected detectionRecord
		Integer row = this.detectionRecordTimeHash.get(selectedTime);
		if (row == null) {
			this.selectedDetectionRecord = null;
			return;
		} else {
			this.selectedDetectionRecord = this.detectionRecords[row.intValue()];
		}
		
		// get selected detectionRecord
		row = this.odrRecordTimeHash.get(selectedTime);
		if (row == null) {
			this.selectedOdrRecord = null;
			return;
		} else {
			this.selectedOdrRecord = this.odrRecords[row.intValue()];
		}
		
		// update UI
		this.summaryTable.updateUI();
		this.rawDataTable.updateUI();
		this.detailTable.updateUI();
		updateHistogramBasePanel();
		
	} // end of method
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * SummaryTableModel
	 */
	class SummaryTableModel extends AbstractTableModel {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		private static final long serialVersionUID = 1L;
		
		//==========================================================================
		// Fields
		//==========================================================================
		private String[] columnNames = {"Name", "Value"};
		private String[] rowNames = {"Time", "PatternId", "PatternName", "Score", "Level"};
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public SummaryTableModel() {}
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			return 5;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return rowNames[row];
			} else if (col == 1) {
				
				if (row == 0) {
					if (selectedDetectionRecord == null) {
						return "";
					} else {
						return DateUtil.getSimpleMinStr(selectedDetectionRecord.getTime());
					}
				} else if (row == 1) {
					if (selectedDetectionRecord == null) {
						return "";
					} else {
						return selectedDetectionRecord.getPatternId() + "";
					}
				} else if (row == 2) {
					if (selectedDetectionRecord == null) {
						return "";
					} else {
						return selectedDetectionRecord.getPatternName();
					}
				} else if (row == 3) {
					if (selectedOdrRecord == null) {
						return "";
					} else {
						return selectedOdrRecord.getOutlierScore() + "";
					}
				} else if (row == 4) {
					if (selectedOdrRecord == null) {
						return "";
					} else {
						return selectedOdrRecord.getOutlierLevelStr();
					}
				} else {
					return "";
				}
			} else {
				return "";
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * RawDataTableModel
	 */
	class RawDataTableModel extends AbstractTableModel {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		private static final long serialVersionUID = 1L;
		
		//==========================================================================
		// Fields
		//==========================================================================
		private String[] columnNames = {"Item", "Value"};
		private String[] rowNames;
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public RawDataTableModel() {
			
			this.rowNames = new String[items.length];
			for (int i=0 ; i<items.length ; i++) {
				rowNames[i] = ItemCodeDefine.getItemName(items[i]);
			}
		}
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			return rowNames.length;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {

			if (col == 0) {
				return rowNames[row];
			} else if (col == 1) {
				
				if (selectedDetectionRecord == null) {
						return "";
				} else {
					return String.format(
							"%.1f", 
							selectedDetectionRecord.getRawDataCell(items[row]).getData_value());
				}
			} else {
				return "";
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class

	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * DeatilTableModel
	 */
	class DetailTableModel extends AbstractTableModel {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		private static final long serialVersionUID = 1L;
		
		//==========================================================================
		// Fields
		//==========================================================================
		private String[] columnNames = {"Ruleset", "Outlier", "Error", "Threshold"};
		private String[] rowNames = null;
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public DetailTableModel() {
			
			// get all ruleset
			if (detectionRecords == null || detectionRecords.length ==0) {
				return;
			}
			
			for (int i=0 ; i<detectionRecords.length ; i++) {
				
				if (detectionRecords[i].getAllDetectionDataCell().length == 0) {
					continue;
				}
				DetectionDataCell[] detectionDataCells = detectionRecords[i].getAllDetectionDataCell();
				rowNames = new String[detectionDataCells.length];
				for (int j=0 ; j<detectionDataCells.length ; j++) {
					rowNames[j] = detectionDataCells[j].getRuleset().getKeyName();
				}
				break;
			}

		} // end of constructor
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			
			if (this.rowNames == null) {
				return 0;
			} else {
				return rowNames.length;
			}
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return rowNames[row];
			} else if (col ==1) {
				if (rowNames == null) {
					return false;
				}
				if (selectedDetectionRecord == null) {
					return false;
				} else {
					
					// get detectionDataCell
					DetectionDataCell detectionDataCell = selectedDetectionRecord.getDetectionDataCell(rowNames[row]);
					if (detectionDataCell == null) {
						return false;
					}
					return detectionDataCell.isOutlier();
				}				
			} else if (col ==2) {
				if (rowNames == null) {
					return "";
				}
				
				if (selectedDetectionRecord == null) {
					return "";
				} else {
					
					// get detectionDataCell
					DetectionDataCell detectionDataCell = selectedDetectionRecord.getDetectionDataCell(rowNames[row]);
					if (detectionDataCell == null) {
						return "";
					}
					return String.format("%.1f", detectionDataCell.getResultValue());
				}
			} else {
				if (rowNames == null) {
					return "";
				}
				
				if (selectedDetectionRecord == null) {
					return "";
				} else {
					// get detectionDataCell
					DetectionDataCell detectionDataCell = selectedDetectionRecord.getDetectionDataCell(rowNames[row]);
					if (detectionDataCell == null) {
						return "";
					}
					return String.format("%.1f ~ %.1f", 
							detectionDataCell.getRuleset().getThresholds()[1],
							detectionDataCell.getRuleset().getThresholds()[0]);
				}
			}
		
		} // end of method
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return String.class;
			} else if (col == 1) {
				return Boolean.class;
			} else if (col == 2) {
				return String.class;
			} else if (col == 3) {
				return String.class;
			} else {
				return String.class;
			}
			
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class

} // end of class
