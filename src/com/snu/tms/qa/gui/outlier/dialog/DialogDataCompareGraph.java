/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import com.snu.tms.qa.analysis.outlier.DoAnalysisReverseCorrection;
import com.snu.tms.qa.model.CorrectionItemInfo;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.TmsDataCell;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;


/**
 * Show Graph
 * 
 */
public class DialogDataCompareGraph extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long  serialVersionUID = 1L;
	
	private static final Color[] itemColors = {
		Color.GREEN, Color.RED, Color.BLUE, 
		Color.CYAN, Color.MAGENTA, Color.YELLOW, Color.DARK_GRAY};
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int         item = ItemCodeDefine.UNDEFINED;
	private  TmsDataset  dataSet;
	private  StackInfo   stackInfo;

	private  JFreeChart  chart;
	private  JPanel      chartBasePanel;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogDataCompareGraph(
			JFrame     parent, 
			String     stackInfoStr,
			StackInfo  stackInfo,
			TmsDataset dataSet,
			int        selectedItem) {
		
		// set dialog
		super(parent, "Data compare(correction) : " + stackInfoStr, ModalityType.MODELESS);
		
		// set icon
		try {
			setIconImage(ImageIO.read(new File("image/smart.png")));
		} catch (IOException exc) {}
		
		// Set Raw Data
		this.dataSet = dataSet;
		this.stackInfo = stackInfo;
		this.item    = selectedItem;
		
		// create graphPanel
		this.chartBasePanel = createChartPanel();
		
		// Add basePanel
		this.setPreferredSize(new Dimension(900, 550));
		this.setLayout(new BorderLayout());
		this.add(this.chartBasePanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create chart Panel
	 */
	private JPanel createChartPanel() {
		
		// create XYDataSet
		TimeSeriesCollection chartDataSet = createDataSet();
		String seriesName = 
				ItemCodeDefine.getItemName(item) + 
				"(" +  ItemCodeDefine.getItemUnit(item) + ")";
		
		// create chart
		chart = ChartFactory.createTimeSeriesChart(
				"",  // title
				"date",             // x-axis label
				seriesName,   // y-axis label
				chartDataSet,            // data
				true,               // create legend?
				true,               // generate tooltips?
				false               // generate URLs?
		);

		// set Font
		chart.getTitle().setFont(new Font("����", Font.BOLD, 14));
		chart.getLegend().setItemFont(new Font("����", Font.BOLD, 12));

		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setOrientation(PlotOrientation.VERTICAL);
		plot.setDomainPannable(true);
		plot.setRangePannable(false);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		// set x-axis properties
		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));

		// set range axis
		XYDotRenderer renderer = new XYDotRenderer();
		renderer.setDotWidth(1);
		renderer.setDotHeight(1);
		plot.setRenderer(renderer);
		renderer.setSeriesPaint(0, itemColors[0]);
		renderer.setSeriesPaint(1, itemColors[1]);
		renderer.setSeriesPaint(2, itemColors[2]);
		
		// return
		return new ChartPanel(chart);
		
	} // end of method
	
	/**
	 * Create graph data set
	 */
	private TimeSeriesCollection createDataSet() {
		
		// create timeseries data
		TimeSeries series1 = new TimeSeries("after");
		TimeSeries series2 = new TimeSeries("before");
		TimeSeries series3 = new TimeSeries("corrected");
		
		// set data
		TmsDataRecord[] dataRecords = this.dataSet.getTmsDataRecords();
		TmsDataCell     dataCell, o2DataCell, tmpDataCell;
		CorrectionItemInfo  correctionInfo = this.stackInfo.getCorrectionInfo(this.item);

		long[]     times  = this.dataSet.getTmsDataRecordTimes();
		double[][] values = new double[dataRecords.length][5]; 
		// o2_value, temp_value, af_value, bf_value, computed_value
		for (int i=0 ; i<values.length ; i++) {
			
			// set value
			dataCell = dataRecords[i].getDataCell(item);
			values[i][2] = dataCell.getData_value();
			values[i][3] = dataCell.getOrigin_value();
			
			// do reverse correction
			o2DataCell = dataRecords[i].getDataCell(ItemCodeDefine.O2);
			if (o2DataCell != null) {
				values[i][0] = o2DataCell.getData_value();
			} else {
				values[i][0] = -1.0D;
			}
			tmpDataCell = dataRecords[i].getDataCell(ItemCodeDefine.TMP);
			if (tmpDataCell != null) {
				values[i][1] = tmpDataCell.getData_value();
			} else {
				values[i][1] = -1.0D;
			}
			
			values[i][4] = DoAnalysisReverseCorrection.doReverseCorrection(
					this.item, values[i][2],
					correctionInfo.getStdOxygen(), correctionInfo.getStdMoisture(),
					values[i][0], values[i][1],
					correctionInfo.isOxygenApplied(),
					correctionInfo.isTemperatureApplied(),
					correctionInfo.isMoistureApplied() );
			
		} // end of for-loop
		
		Minute time = null;
		for (int i=0 ; i<values.length ; i++) {
			
			time = new Minute(new java.util.Date(times[i]));
			if (values[i][2] != -1.0D) {
				series1.add(time, values[i][2]);
			}
			if (values[i][3] != -1.0D) {
				series2.add(time, values[i][3]);
			}
			if (values[i][4] != -1.0D) {
				series3.add(time, values[i][4]);
			}
		}
		
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(series1);
		dataset.addSeries(series2);
		dataset.addSeries(series3);
		
		// release memory
		times = null;
		values = null;
		
		return dataset;
		
	} // end of method
	
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	
	
} // end of class
