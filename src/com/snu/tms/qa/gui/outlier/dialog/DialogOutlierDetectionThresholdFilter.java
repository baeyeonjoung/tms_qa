/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.snu.tms.qa.model.ruleset.RulesetResultFilterModel;


/**
 * Show filtering rule for outlier detection reuslts
 * 
 */
public class DialogOutlierDetectionThresholdFilter extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  RulesetResultFilterModel  filterModel = null;
	private  RulesetResultFilterModel  originFilterModel = null;
	
	private  JCheckBox[] patternFilterChecks = null;
	private  JCheckBox[] levelFilterChecks = null;
	private  JCheckBox[] rulesetFilterChecks = null;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogOutlierDetectionThresholdFilter(
			JDialog parent,
			RulesetResultFilterModel filterModel) {
		
		// set dialog
		super(parent, "", ModalityType.APPLICATION_MODAL);
		this.setTitle("Set filtering for outlier detection result");
		
		// set parameters
		this.originFilterModel = filterModel;
		this.filterModel = new RulesetResultFilterModel(filterModel);
		
		// set layout
		JPanel  controlPanel  = createControlPanel();
		JScrollPane  filterPanel = new JScrollPane(createFilterPanel());
		
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(660, 500));
		this.add(controlPanel, BorderLayout.NORTH);
		this.add(filterPanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create control panel
	 */
	private JPanel createControlPanel() {
		
		// create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(660, 50));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("  outlier filtering control  "),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));

		
		// Add Control Button
		JButton applyButton = new JButton("apply");
		applyButton.setBounds(20, 20, 100, 20);
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doApplyAction();
			}
		});
		basePanel.add(applyButton);

		JButton cancelButton = new JButton("cancel");
		cancelButton.setBounds(150, 20, 100, 20);
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doCancelAction();
			}
		});
		basePanel.add(cancelButton);

		return basePanel;
		
	} // end of method
	
	/**
	 * create filter panel
	 */
	private JPanel createFilterPanel() {
		
		// create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("  set outlier filter  "),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// set Dimension
		int width = 600;
		int height = 0;
		
		// set layout
		JCheckBox patternCheck = new JCheckBox("Pattern Filter");
		patternCheck.setSelected(false);
		patternCheck.setBounds(30, 25, 200, 25);
		patternCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i=0 ; i<patternFilterChecks.length ; i++) {
					patternFilterChecks[i].setSelected(((JCheckBox)e.getSource()).isSelected());
				}
			}
		});
		basePanel.add(patternCheck);

		JCheckBox levelCheck = new JCheckBox("Level Filter");
		levelCheck.setSelected(false);
		levelCheck.setBounds(230, 25, 150, 25);
		levelCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i=0 ; i<levelFilterChecks.length ; i++) {
					levelFilterChecks[i].setSelected(((JCheckBox)e.getSource()).isSelected());
				}
			}
		});
		basePanel.add(levelCheck);

		JCheckBox rulesetCheck = new JCheckBox("Ruleset Filter");
		rulesetCheck.setSelected(false);
		rulesetCheck.setBounds(380, 25, 150, 25);
		rulesetCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i=0 ; i<rulesetFilterChecks.length ; i++) {
					rulesetFilterChecks[i].setSelected(((JCheckBox)e.getSource()).isSelected());
				}
			}
		});
		basePanel.add(rulesetCheck);

		// Set Pattern Filter
		RulesetResultFilterModel.FilteringModelPattern[] patternFilters = 
				this.filterModel.getPatternFilters();
		this.patternFilterChecks = new JCheckBox[patternFilters.length];
		for (int i=0 ; i<patternFilters.length ; i++) {
			
			this.patternFilterChecks[i] = new JCheckBox(String.format("[%2d] %s", 
					patternFilters[i].patternDataset.getPatternId(),
					patternFilters[i].patternDataset.getPatternName()));
			this.patternFilterChecks[i].setSelected(patternFilters[i].isShowEnable);
			this.patternFilterChecks[i].setBounds(30, 60+30*i, 200, 25);
			final int patternId = patternFilters[i].patternDataset.getPatternId();
			this.patternFilterChecks[i].addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					filterModel.setPatternFilter(patternId, ((JCheckBox)e.getSource()).isSelected());
				}
			});
			basePanel.add(this.patternFilterChecks[i]);
			
		}
		if (height < 60+30*patternFilters.length) {
			height = 60+30*patternFilters.length;
		}

		// Set Level Filter
		RulesetResultFilterModel.FilteringModelDetectionLevel[] levelFilters = 
				this.filterModel.getLevelFilters();
		this.levelFilterChecks = new JCheckBox[levelFilters.length];
		for (int i=0 ; i<levelFilters.length ; i++) {
			
			this.levelFilterChecks[i] = new JCheckBox(levelFilters[i].getLevelName());
			this.levelFilterChecks[i].setSelected(levelFilters[i].isShowEnable);
			this.levelFilterChecks[i].setBounds(230, 60+30*i, 150, 25);
			final int level = levelFilters[i].level;
			this.levelFilterChecks[i].addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					filterModel.setLevelFilter(level, ((JCheckBox)e.getSource()).isSelected());
				}
			});
			basePanel.add(this.levelFilterChecks[i]);
			
		}
		if (height < 60+30*levelFilters.length) {
			height = 60+30*levelFilters.length;
		}

		// Set Ruleset Filter
		RulesetResultFilterModel.FilteringModelRuleset[] rulesetFilters = 
				this.filterModel.getRulesetFilters();
		this.rulesetFilterChecks = new JCheckBox[rulesetFilters.length];
		for (int i=0 ; i<rulesetFilters.length ; i++) {
			
			this.rulesetFilterChecks[i] = new JCheckBox(rulesetFilters[i].rulesetName);
			this.rulesetFilterChecks[i].setSelected(rulesetFilters[i].isShowEnable);
			this.rulesetFilterChecks[i].setBounds(380, 60+30*i, 150, 25);
			final String rulesetName = rulesetFilters[i].rulesetName;
			this.rulesetFilterChecks[i].addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					filterModel.setRulesetFilter(rulesetName, ((JCheckBox)e.getSource()).isSelected());
				}
			});
			basePanel.add(this.rulesetFilterChecks[i]);
			
		}
		if (height < 60+30*rulesetFilters.length) {
			height = 60+30*rulesetFilters.length;
		}
		
		// return
		basePanel.setPreferredSize(new Dimension(width, height));
		return basePanel;
		
	} // end of method
	
	/**
	 * do apply action
	 */
	private void doApplyAction() {
		
		// apply change
		this.originFilterModel.setFilterModel(this.filterModel);
		// close dialog
		this.setVisible(false);
		
	} // end of method
	
	/**
	 * do cancel action
	 */
	private void doCancelAction() {
		
		// close dialog
		this.setVisible(false);
				
	} // end of method
	
	
	
} // end of class
