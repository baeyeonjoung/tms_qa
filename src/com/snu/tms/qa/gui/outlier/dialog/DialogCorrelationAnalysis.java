/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.*;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


/**
 * Show Graph
 * 
 */
public class DialogCorrelationAnalysis extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogCorrelationAnalysis(
			JFrame parent, 
			String stackInfoStr,
			TmsDataset dataSet,
			boolean isCorrectedValue) {
		
		// set dialog
		super(parent, "", ModalityType.MODELESS);
		if (isCorrectedValue) {
			this.setTitle("correlation analysis : " + stackInfoStr);
		} else {
			this.setTitle("correlation analysis(before corrected) : " + stackInfoStr);
		}
		
		// Determine how many items is contain valid item records
		int[][] itemValueCounts = dataSet.getItemValueCount();
		Vector<Integer> targetItems = new Vector<Integer>();
		for (int i=0 ; i<itemValueCounts.length ; i++) {
			if (itemValueCounts[i][0] == ItemCodeDefine.UNDEFINED) {
				continue;
			} else if (itemValueCounts[i][0] == ItemCodeDefine.TM3) {
				continue;
			} else if (itemValueCounts[i][1] == 0) {
				continue;
			} else {
				targetItems.add(itemValueCounts[i][0]);
			}
		}
		
		// create basePanel
		int itemCount = targetItems.size();
		int windowSize = 200 * itemCount;
		JPanel basePanel = null;
		if (itemCount == 0) {
			basePanel = new JPanel(null);
		} else {
			basePanel = new JPanel(new GridLayout(itemCount, itemCount));
		}
		
		// add child chart components
		for (int i=0 ; i<itemCount ; i++) {
			for (int j=0 ; j<itemCount ; j++) {
				if (i == j) {
					basePanel.add(addLabelPanel(targetItems.get(i).intValue()));
				} else if (i< j) {
					//basePanel.add(addNullPanel());
					basePanel.add(
							addChartPanel(
									targetItems.get(i).intValue(), 
									targetItems.get(j).intValue(), 
									dataSet, 
									isCorrectedValue,
									parent,
									stackInfoStr));
				} else {
					//basePanel.add(addNullPanel());
					basePanel.add(
							addChartPanel(
									targetItems.get(i).intValue(), 
									targetItems.get(j).intValue(), 
									dataSet, 
									isCorrectedValue,
									parent,
									stackInfoStr));
				}
			}
		}
		
		// set ui
		JScrollPane scrollPane = new JScrollPane(basePanel);
		if (windowSize >= 800) {
			this.setPreferredSize(new Dimension(830, 870));
		} else if (windowSize <= 200) {
			this.setPreferredSize(new Dimension(830, 270));
		} else {
			this.setPreferredSize(new Dimension(windowSize+30, windowSize+70));
		}
		this.setLayout(new BorderLayout());
		this.add(scrollPane, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Add Label Panel
	 */
	private JPanel addLabelPanel(int itemCode) {
		
		JPanel basePanel = new JPanel(new BorderLayout());
		basePanel.setPreferredSize(new Dimension(200, 200));
		basePanel.setBorder(BorderFactory.createEtchedBorder());
		
		JLabel label = new JLabel(ItemCodeDefine.getItemName(itemCode));
		label.setHorizontalAlignment(JLabel.CENTER);
		label.setVerticalAlignment(JLabel.CENTER);
		basePanel.add(label, BorderLayout.CENTER);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * Add Null Panel
	 */
	@SuppressWarnings("unused")
	private JPanel addNullPanel() {
		
		JPanel basePanel = new JPanel(new BorderLayout());
		basePanel.setPreferredSize(new Dimension(200, 200));
		basePanel.setBorder(BorderFactory.createEtchedBorder());
		
		return basePanel;
		
	} // end of method
	
	/**
	 * Add Chart Panel
	 */
	private JPanel addChartPanel(
			final int itemCode1, final int itemCode2, 
			final TmsDataset dataSet, final boolean isCorrectedValue,
			final JFrame parent, final String stackInfoStr) {
		
		// create Chart Data
		XYDataset dataset = createChartData(itemCode1, itemCode2, dataSet, isCorrectedValue);
		
		// Compute pearsons correlation
		double[] pearsonsCorrelationValue = computePearsonCorrelation(itemCode1, itemCode2, dataSet, isCorrectedValue);
		String correlationInfoStr = String.format("corr = %.2f, pValue = %.2f", 
				pearsonsCorrelationValue[0], pearsonsCorrelationValue[1]);
		
		// Create Chart
		JFreeChart chart = ChartFactory.createScatterPlot(
				correlationInfoStr, 
				ItemCodeDefine.getItemName(itemCode1), 
				ItemCodeDefine.getItemName(itemCode2), 
				dataset,
				PlotOrientation.VERTICAL, true, true, false);
		
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setNoDataMessage("NO DATA");
		
		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		plot.setRangeCrosshairVisible(true);
		plot.setRangeCrosshairLockedOnData(true);
		plot.setDomainZeroBaselineVisible(true);
		plot.setRangeZeroBaselineVisible(true);
		XYDotRenderer renderer = new XYDotRenderer();
		renderer.setDotWidth(4);
		renderer.setDotHeight(4);
		plot.setRenderer(renderer);
		NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
		domainAxis.setAutoRangeIncludesZero(false);
		domainAxis.setTickLabelsVisible(false);
		domainAxis.setVisible(false);
		
		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setTickLabelsVisible(false);
		rangeAxis.setVisible(false);
		
		chart.getLegend().setVisible(false);
		
		// create chart Panel
		@SuppressWarnings("serial")
		ChartPanel basePanel = new ChartPanel(chart) {
			public void mouseClicked(MouseEvent e) {
				new DialogCorrelationOutlierAnalysis(
						parent, 
						stackInfoStr,
						dataSet,
						itemCode1, itemCode2, isCorrectedValue);
			}
		};
		basePanel.setPreferredSize(new Dimension(200, 200));

		// Add ActionListener
		
		return basePanel;
		
	} // end of method
	
	
	/**
	 * Create Chart Data
	 */
	private XYDataset createChartData(int itemCode1, int itemCode2, TmsDataset dataSet, boolean isCorrectedValue) {
		
		XYSeries series = new XYSeries("correlation");
		TmsDataRecord[] records = dataSet.getTmsDataRecords();
		double x, y;
		for (TmsDataRecord record : records) {
			if (isCorrectedValue) {
				x = record.getDataCell(itemCode1).getData_value();
				y = record.getDataCell(itemCode2).getData_value();
			} else {
				x = record.getDataCell(itemCode1).getOrigin_value();
				y = record.getDataCell(itemCode2).getOrigin_value();
			}
			if (x == -1.0D || y == -1.0D) {
				continue;
			} else {
				series.add(x, y);
			}
		}
		return new XYSeriesCollection(series);
		
	} // end of method
	
	/**
	 * computePearsonCorrelation
	 */
	private double[] computePearsonCorrelation(int itemCode1, int itemCode2, TmsDataset dataSet, boolean isCorrectedValue) {
		
		// allocate enough memory
		double[][] data = new double[dataSet.getTmsDataRecordCount()][2];
		// set data
		double x, y;
		int index = 0;
		TmsDataRecord[] records = dataSet.getTmsDataRecords();
		for (TmsDataRecord record : records) {
			if (isCorrectedValue) {
				x = record.getDataCell(itemCode1).getData_value();
				y = record.getDataCell(itemCode2).getData_value();
			} else {
				x = record.getDataCell(itemCode1).getOrigin_value();
				y = record.getDataCell(itemCode2).getOrigin_value();
			}
			if (x == -1.0D || y == -1.0D) {
				continue;
			} else {
				data[index][0] = x;
				data[index][1] = y;
				index++;
			}
		}
		// trim
		double[][] trimedData = Arrays.copyOf(data, index);
		// compute pearson-correlation
		PearsonsCorrelation pearsonsCorrelation = new PearsonsCorrelation(trimedData);
		double[] correlationValue = new double[2];
		correlationValue[0] = pearsonsCorrelation.getCorrelationMatrix().getEntry(0, 1);
		correlationValue[1] = pearsonsCorrelation.getCorrelationPValues().getEntry(0, 1);;
		
		return correlationValue;
		
	} // end of method
	
	//==========================================================================
	// Inner Class
	//==========================================================================

	
} // end of class
