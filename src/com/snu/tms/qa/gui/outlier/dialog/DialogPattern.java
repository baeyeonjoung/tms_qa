/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.snu.tms.qa.analysis.pca.DoComputePCA;
import com.snu.tms.qa.fileio.PatternDataWriter;
import com.snu.tms.qa.gui.outlier.event.ActionInvoker;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.PatternDataSet;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.ruleset.PatternDataRecord;
import com.snu.tms.qa.util.NumberUtil;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * Show Graph
 * 
 */
public class DialogPattern extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private PatternDataSet patternDataSet = null;
	private JTabbedPane    patternBasePanel = null;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogPattern(
			final JFrame parent, 
			final String stackInfoStr,
			TmsDataset dataSet,
			int timeWindow,
			int patternNum) {
		
		// set dialog
		super(parent, "Pattern Analysis : " + stackInfoStr, ModalityType.MODELESS);

		// set icon
		try {
			setIconImage(ImageIO.read(new File("image/smart.png")));
		} catch (IOException exc) {}
		
		// Determine how many items is contain valid item records
		int[][] itemValueCounts = dataSet.getItemValueCount();
		Vector<Integer> targetItems = new Vector<Integer>();
		for (int i=0 ; i<itemValueCounts.length ; i++) {
			if (itemValueCounts[i][0] == ItemCodeDefine.UNDEFINED) {
				continue;
			} else if (itemValueCounts[i][0] == ItemCodeDefine.TM3) {
				continue;
			} else if (itemValueCounts[i][1] == 0) {
				continue;
			} else {
				targetItems.add(itemValueCounts[i][0]);
			}
		}
		
		// compute PCA
		this.patternDataSet = DoComputePCA.doAnalysis(targetItems, dataSet, timeWindow, patternNum);
		if (this.patternDataSet == null) {
			JOptionPane.showMessageDialog(
					parent, 
					"[" + stackInfoStr + "] has not any TMS data");
			return;
		}
		
		// create control panel
		JPanel upperPanel = new JPanel(null);
		upperPanel.setPreferredSize(new Dimension(930, 35));
		
		JButton showDataButton = new JButton("show data");
		showDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new DialogPatternDataView(
						parent, 
						stackInfoStr,
						patternDataSet);
			}
		});
		showDataButton.setBounds(10, 5, 150, 25);
		upperPanel.add(showDataButton);
		
		JButton showGraphButton = new JButton("show graph");
		showGraphButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new DialogPatternGraphView(
						parent, 
						stackInfoStr,
						patternDataSet);
			}
		});
		showGraphButton.setBounds(170, 5, 150, 25);
		upperPanel.add(showGraphButton);
		
		JButton applyButton = new JButton("apply");
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				applyPatternAnalysisResult();
				closeWindow();
			}
		});
		applyButton.setBounds(400, 5, 100, 25);
		upperPanel.add(applyButton);
		
		JButton cancleButton = new JButton("cancle");
		cancleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeWindow();
			}
		});
		cancleButton.setBounds(520, 5, 100, 25);
		upperPanel.add(cancleButton);

		
		JButton savePatternButton = new JButton("save pattern");
		savePatternButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				savePattern();
			}
		});
		savePatternButton.setBounds(700, 5, 150, 25);
		upperPanel.add(savePatternButton);

		// Pattern analysis panel
		this.patternBasePanel = new JTabbedPane();
		setPatternBasePanel(-1);
		
		// BasePanel
		JPanel basePanel = new JPanel(new BorderLayout());
		basePanel.add(upperPanel, BorderLayout.NORTH);
		basePanel.add(this.patternBasePanel, BorderLayout.CENTER);
		
		// set ui
		this.setPreferredSize(new Dimension(1100, 800));
		this.setLayout(new BorderLayout());
		this.add(basePanel, BorderLayout.CENTER);
		
		this.addWindowListener(new WindowListener() {
			
			public void windowClosing(WindowEvent e) {
				patternDataSet = null;
				patternBasePanel.removeAll();
			}
			public void windowOpened(WindowEvent e) {}
			public void windowDeactivated(WindowEvent e) {}
			public void windowDeiconified(WindowEvent e) {}
			public void windowActivated(WindowEvent e) {}
			public void windowIconified(WindowEvent e) {}
			public void windowClosed(WindowEvent e) {
				System.gc();
			}
			
		});
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Close this window
	 */
	public void closeWindow() {
		this.dispose();
	} // end of method
	
	/**
	 * Apply Pattern Analysis Result
	 */
	public void applyPatternAnalysisResult() {
		
		ActionEvent e = new ActionEvent(this.patternDataSet, 0, "apply_pattern");
		ActionInvoker.getinstnace().invokeActionListener(e);
		
	} // end of method
	
	
	/**
	 * set Pattern BasePanel
	 */
	private void setPatternBasePanel(int selectedPatternIndex) {
		
		// remove all tab
		int selectedTabIndex = 0;
		this.patternBasePanel.removeAll();
		
		// Set PCA result panel
		this.patternBasePanel.addTab("Pattern Analysis result", createPatternAnalysisResultPanel());

		// set pattern tab
		int[] patternIndices = this.patternDataSet.getPatternIndexArray();
		JPanel patternBasePanel;
		String tabName;
		int tabIndex = 1;
		for (int patternIndex : patternIndices) {
			patternBasePanel = createPatternAnalysisPanel(patternIndex);
			tabName = String.format("Pattern [ %d ]", patternIndex);
			this.patternBasePanel.addTab(tabName, patternBasePanel);
			if (selectedPatternIndex == patternIndex) {
				selectedTabIndex = tabIndex;
			}
			tabIndex++;
		}
		
		// Set tab
		this.patternBasePanel.setSelectedIndex(selectedTabIndex);
		
	} // end of method
	
	
	/**
	 * Create PCA rssult Panel
	 */
	private JPanel createPatternAnalysisResultPanel() {
		
		// make basePanel
		JPanel basePanel = new JPanel(new BorderLayout());
		
		// create Pattern Analysis result table
		JScrollPane tablePanel = createPatternAnalysisResultTablePanel();
		int patternCount = this.patternDataSet.getPatternIndexArray().length;
		tablePanel.setPreferredSize(new Dimension(600, 5+20*(patternCount+1)));
		basePanel.add(tablePanel, BorderLayout.NORTH);
		
		// create data scattor plot
		int[] items = this.patternDataSet.getItems();
		PatternDataRecord[] patternRecords = this.patternDataSet.getAllSortedPatternDataRecord();
		int[] patternIndices = this.patternDataSet.getPatternIndexArray();
		JScrollPane chartBasePanel = createPatternPlotChartPanel(
				items, patternRecords, patternIndices);
		basePanel.add(chartBasePanel, BorderLayout.CENTER);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * Create Pattern analysis panel
	 */
	private JPanel createPatternAnalysisPanel(final int patternIndex) {
		
		// Set Pattern Info Panel
		JPanel patternInfoPanel = new JPanel(null);
		patternInfoPanel.setPreferredSize(new Dimension(800, 120));
		patternInfoPanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("pattern information"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								patternInfoPanel.getBorder()));
		
		// Set TimeWindows Configuration
		JLabel infoLabel = new JLabel("Pattern Index");
		infoLabel.setBounds(20, 20, 150, 25);
		patternInfoPanel.add(infoLabel);
		
		infoLabel = new JLabel("Data Count");
		infoLabel.setBounds(400, 20, 100, 25);
		patternInfoPanel.add(infoLabel);
		
		infoLabel = new JLabel("Pattern Name");
		infoLabel.setBounds(20, 50, 150, 25);
		patternInfoPanel.add(infoLabel);
		
		infoLabel = new JLabel("Move Pattern To");
		infoLabel.setBounds(20, 80, 150, 25);
		patternInfoPanel.add(infoLabel);
		
		JTextField patternIndexTextField = new JTextField(patternIndex + "");
		patternIndexTextField.setBounds(170, 20, 50, 25);
		patternIndexTextField.setEditable(false);
		patternInfoPanel.add(patternIndexTextField);
		
		int patternDataCount = this.patternDataSet.getPatternDataRecordCount(patternIndex);
		int dataCount = this.patternDataSet.getDataRecordCount();
		JTextField dataCountTextField = new JTextField(
				String.format("%s / %s", 
						NumberUtil.getNumberStr(patternDataCount),
						NumberUtil.getNumberStr(dataCount)));
		dataCountTextField.setBounds(500, 20, 150, 25);
		dataCountTextField.setEditable(false);
		patternInfoPanel.add(dataCountTextField);

		final JTextField patternNameTextField = 
				new JTextField(this.patternDataSet.getPatternName(patternIndex));
		patternNameTextField.setBounds(170, 50, 200, 25);
		patternNameTextField.setEditable(true);
		patternInfoPanel.add(patternNameTextField);
		
		int[] patternIndices = this.patternDataSet.getPatternIndexArray();
		Integer[] patterns = new Integer[patternIndices.length];
		for (int i=0 ; i<patternIndices.length ; i++) {
			patterns[i] = new Integer(patternIndices[i]);
		}
		final JComboBox<Integer> patternNumComboBox = new JComboBox<Integer>(patterns);
		patternNumComboBox.setSelectedItem(new Integer(patternIndex));
		patternNumComboBox.setBounds(170, 80, 80, 25);
		patternInfoPanel.add(patternNumComboBox);
		
		JButton applyButton = new JButton("apply");
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// set pattern user-defined name
				String userPatternName = patternNameTextField.getText();
				if (userPatternName != null && !userPatternName.trim().equals("")) {
					patternDataSet.setPatternName(patternIndex, userPatternName);
				}
				
				// move pattern
				int selectedPattern = ((Integer)patternNumComboBox.getSelectedItem()).intValue();
				if (selectedPattern != patternIndex) {
					patternDataSet.setPatternName(patternIndex, "");
					patternDataSet.updatePatternData(patternIndex, selectedPattern);
					
					// re-establish TabbedPane
					setPatternBasePanel(selectedPattern);
				}
			}
		});
		applyButton.setBounds(270, 80, 100, 25);
		patternInfoPanel.add(applyButton);
		
		infoLabel = new JLabel("Pattern Split");
		infoLabel.setBounds(400, 50, 150, 25);
		patternInfoPanel.add(infoLabel);
		
		Integer[] patternSplitNumbers = {2, 3, 4, 5, 6, 7, 8, 9, 10};
		final JComboBox<Integer> patternSplitComboBox = new JComboBox<Integer>(patternSplitNumbers);
		patternSplitComboBox.setSelectedItem(0);
		patternSplitComboBox.setBounds(400, 80, 80, 25);
		patternInfoPanel.add(patternSplitComboBox);

		JButton splitPatternButton = new JButton("split");
		splitPatternButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// get pattern split number
				int splitNumber = ((Integer)patternSplitComboBox.getSelectedItem()).intValue();
				
				// pattern split
				int[] newPatternIndices = DoComputePCA.splitPattern(
						patternDataSet, patternIndex, splitNumber);
				
				// re-establish TabbedPane
				setPatternBasePanel(newPatternIndices[0]);
			}
		});
		splitPatternButton.setBounds(500, 80, 90, 25);
		patternInfoPanel.add(splitPatternButton);

		// Analysis Base Panel
		JSplitPane resultBasePanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		resultBasePanel.setDividerLocation(500);
		resultBasePanel.setDividerSize(5);
		resultBasePanel.setPreferredSize(new Dimension(900, 640));
		
		JScrollPane tablePanel = createDataTablePanel(patternIndex);
		resultBasePanel.add(tablePanel);
		
		JScrollPane graphPanel = createGraphPanel(patternIndex);
		resultBasePanel.add(graphPanel);
		
		// Base Panel
		JPanel basePanel = new JPanel(new BorderLayout());
		basePanel.add(patternInfoPanel, BorderLayout.NORTH);
		basePanel.add(resultBasePanel, BorderLayout.CENTER);
		
		return basePanel;
		
	} // end of method
	
	
	/**
	 * Set data table
	 */
	private JScrollPane createDataTablePanel(int patternIndex) {
		
		int[] item = this.patternDataSet.getItems();
		
		JTable dataTable = new JTable(new DataTableModel(this.patternDataSet, patternIndex));
		dataTable.setRowHeight(20);
		//dataTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		dataTable.getColumnModel().getColumn(0).setPreferredWidth(130);
		/**
		dataTable.getColumnModel().getColumn(0).setPreferredWidth(130);
		for (int i=0 ; i<item.length ; i++) {
			if (item[i] == ItemCodeDefine.FLW) {
				dataTable.getColumnModel().getColumn(i+1).setPreferredWidth(100);
			} else {
				dataTable.getColumnModel().getColumn(i+1).setPreferredWidth(50);
			}
		}
		*/
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );

		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		dataTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		for (int i=1 ; i<item.length+1 ; i++) {
			dataTable.getColumnModel().getColumn(i).setCellRenderer( rightRenderer );
		}
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(dataTable);
		return basePane;
		
	} // end of method
	
	
	/**
	 * Set data table
	 */
	private JScrollPane createGraphPanel(int patternIndex) {
		
		// Get items
		int[] items = this.patternDataSet.getItems();
		PatternDataRecord[] records = this.patternDataSet.getSortedPatternDataRecord(patternIndex);
		
		// create chart
		JPanel chartBasePanel = new JPanel(new GridLayout(0, 2));
		chartBasePanel.setPreferredSize(new Dimension(300, 150*(items.length / 2)));
		
		JPanel chartPanel;
		for (int i=0 ; i<items.length ; i++) {
			chartPanel = createHistogram(
					records, patternIndex, i, items[i], this.patternDataSet.getConfidenceInterval(i));
			chartPanel.setPreferredSize(new Dimension(150, 150));
			chartBasePanel.add(chartPanel);
		}
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(chartBasePanel);
		return basePane;
		
	} // end of method
	
	
	/**
	 * create Histogram
	 */
	private JPanel createHistogram(
			PatternDataRecord[] records, int patternIndex, int itemIndex, int itemType, double[] range) {
		
		// prepare HistogramDataset
		HistogramDataset dataset = new HistogramDataset();
		double[] values = new double[records.length];
		for (int i=0 ; i<records.length ; i++) {
			values[i] = records[i].getValue(itemIndex);
		}
		dataset.addSeries(ItemCodeDefine.getItemName(itemType), values, 20);
		
		// create histogram chart
		JFreeChart chart = ChartFactory.createHistogram(
				"",
				null,
				null,
				dataset,
				PlotOrientation.VERTICAL,
				true,
				true,
				false);
		
		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setDomainPannable(true);
		plot.setRangePannable(true);
		plot.setForegroundAlpha(0.85f);
		NumberAxis xAxis = (NumberAxis) plot.getDomainAxis();
		xAxis.setRange(range[0], range[1]);
		NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
		yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
		renderer.setDrawBarOutline(false);
		renderer.setBarPainter(new StandardXYBarPainter());
		renderer.setShadowVisible(false);

		// return
		return new ChartPanel(chart);
		
	} // end of method
	
	

	/**
	 * Set Pattern Analysis Result Table Panel
	 */
	private JScrollPane createPatternAnalysisResultTablePanel() {
		
		JTable patternResultTable = new JTable(new PatternAnalysisResultTableModel());
		patternResultTable.setRowHeight(20);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );

		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		patternResultTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		patternResultTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		patternResultTable.getColumnModel().getColumn(2).setCellRenderer( rightRenderer );
		patternResultTable.getColumnModel().getColumn(3).setCellRenderer( rightRenderer );
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(patternResultTable);
		return basePane;
		
	} // end of method
	
	
	/**
	 * create Pattern data scattor plot
	 */
	private JScrollPane createPatternPlotChartPanel(
			int[] items,
			PatternDataRecord[] patternRecords,
			int[] patternIndices) {
		
		/**
		// create chart base panel
		int N = items.length;
		JPanel basePanel = new JPanel(new GridLayout(N-1, N-1));
		
		// add child chart components
		for (int i=0 ; i<N-1 ; i++) {
			for (int j=0 ; j<N ; j++) {
				if (i == j) {
					basePanel.add(addEmptyPanel());
				} else {
					basePanel.add(addPcPlotChartPanel(items, i, j, patternRecords, patternIndices));
				}
			}
		}
		
		// set UI
		JScrollPane scrollPane = new JScrollPane(basePanel);
		return scrollPane;
		*/
		// create chart base panel
		int N = items.length;
		JPanel basePanel = new JPanel(new GridLayout(0, 3));
		
		// add child chart components
		for (int i=0 ; i<N ; i++) {
			for (int j=0 ; j<N ; j++) {
				if (i == j) {
					continue;
				} else {
					basePanel.add(addPcPlotChartPanel(items, i, j, patternRecords, patternIndices));
				}
			}
		}
		
		// set UI
		JScrollPane scrollPane = new JScrollPane(basePanel);
		return scrollPane;
		
	} // end of method
	
	/**
	 * Add PC-Plot Chart Panel
	 */
	private JPanel addPcPlotChartPanel(
			int[] items,
			int pcXIndex, 
			int pcYIndex, 
			PatternDataRecord[] pcRecords,
			int[] patternIndices) {
		
		// create Chart Data
		XYDataset dataset = createChartData(pcXIndex, pcYIndex, pcRecords, patternIndices);
		
		// Create Chart
		JFreeChart chart = ChartFactory.createScatterPlot(
				"",
				ItemCodeDefine.getItemName(items[pcXIndex]),
				ItemCodeDefine.getItemName(items[pcYIndex]), 
				dataset,
				PlotOrientation.VERTICAL, true, true, false);
		
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setNoDataMessage("NO DATA");
		
		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		plot.setRangeCrosshairVisible(true);
		plot.setRangeCrosshairLockedOnData(true);
		plot.setDomainZeroBaselineVisible(true);
		plot.setRangeZeroBaselineVisible(true);
		XYDotRenderer renderer = new XYDotRenderer();
		renderer.setDotWidth(2);
		renderer.setDotHeight(2);
		plot.setRenderer(renderer);
		NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
		domainAxis.setAutoRangeIncludesZero(false);
		domainAxis.setTickLabelsVisible(false);
		domainAxis.setVisible(true);
		
		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setTickLabelsVisible(false);
		rangeAxis.setVisible(true);
		
		chart.getLegend().setVisible(true);
		
		// create chart Panel
		ChartPanel basePanel = new ChartPanel(chart);
		basePanel.setPreferredSize(new Dimension(300, 300));

		// Add ActionListener
		
		return basePanel;
		
	} // end of method
	
	
	/**
	 * create chart data
	 */
	private XYDataset createChartData(int item_x, int item_y, PatternDataRecord[] records, int[] patternIndices) {
		
		// Set DataSet
		XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
		
		// Add XYSeries
		XYSeries[] xySeries = new XYSeries[patternIndices.length];
		for (int i=0 ; i<xySeries.length ; i++) {
			xySeries[i] = new XYSeries("pattern[" + patternIndices[i] + "]");
		}
		int pattern = 0;
		double x, y;
		for (PatternDataRecord record : records) {
			pattern = record.getPatternIndex();
			x = record.getValue(item_x);
			y = record.getValue(item_y);
			for (int i=0 ; i<patternIndices.length ; i++) {
				if (patternIndices[i] == pattern) {
					xySeries[i].add(x, y);
					continue;
				}
			}
		}
		
		// add XYSeries to Collection
		for (int i=0 ; i<xySeries.length ; i++) {
			xySeriesCollection.addSeries(xySeries[i]);
		}
		return xySeriesCollection;
		
	} // end of method
	
	
	/**
	 * Add Null Panel
	 */
	private JPanel addEmptyPanel() {
		
		JPanel basePanel = new JPanel(new BorderLayout());
		basePanel.setPreferredSize(new Dimension(200, 200));
		//basePanel.setBorder(BorderFactory.createEtchedBorder());
		
		return basePanel;
		
	} // end of method
	
	/** 
	 * save pattern
	 */
	private void savePattern() {
		PatternDataWriter.writeDataFile();
	} // end of method
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class DataTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		private String[] columnNames;
		
		private int                 patternIndex;
		private PatternDataRecord[] records;
		
		public DataTableModel(PatternDataSet dataset, int patternIndex) {
			
			// set dataset
			this.patternIndex = patternIndex;
			this.records = dataset.getSortedPatternDataRecord(this.patternIndex);
			
			// set column names
			int[] items = dataset.getItems();
			this.columnNames = new String[items.length + 1];
			columnNames[0] = "Time";
			for (int i=0 ; i<items.length ; i++) {
				this.columnNames[i+1] = ItemCodeDefine.getItemName(items[i]);
			}
			
		}
		
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			if (records == null) {
				return 0;
			}
			return records.length;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return records[row].getTimestr();
			} else {
				return NumberUtil.getNumberStr(records[row].getValue(col-1));
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class PatternAnalysisResultTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		private String[] columnNames = {"Pattern Index", "Pattern Name", "Data Count", "% Data Count"};
		
		public PatternAnalysisResultTableModel() {
			
		} // end of constructor
			
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() {
			
			int[] patternIndices = patternDataSet.getPatternIndexArray();
			return patternIndices.length;
		
		} // end of method
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			int[] patternIndices = patternDataSet.getPatternIndexArray();
			int selectedPatternIndex = patternIndices[row];
			if (col == 0) {
				return "Pattern[" + selectedPatternIndex + "]";
			} else if (col == 1) {
				return patternDataSet.getPatternName(selectedPatternIndex);
			} else if (col == 2) {
				int dataCount = patternDataSet.getPatternDataRecordCount(selectedPatternIndex);
				return NumberUtil.getNumberStr(dataCount);
			} else if (col == 3) {
				int allDataCount = patternDataSet.getDataRecordCount();
				int targetDataCount = patternDataSet.getPatternDataRecordCount(selectedPatternIndex);
				if (allDataCount == 0) {
					return "0.00 %";
				} else {
					return String.format("%.2f", ((double)targetDataCount / (double)allDataCount) * 100.0D) + " %";
				}
			} else {
				return "";
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	
	//==========================================================================
	// Inner Class
	//==========================================================================

	
} // end of class
