/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.statistics.HistogramDataset;

import com.snu.tms.qa.analysis.generalstats.DoAnalysisGeneralStats;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.TmsDataCell;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.TmsDataSingleItemRecord;
import com.snu.tms.qa.util.NumberUtil;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.*;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * Show Graph
 * 
 */
public class DialogGeneralAnalysis extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(DialogGeneralAnalysis.class);
	private static final long serialVersionUID = 1L;
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  TmsDataset dataSet;
	private  int        itemCode;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogGeneralAnalysis(
			JFrame parent, 
			String stackInfoStr,
			int itemCode,
			TmsDataset dataSet) {
		
		// set dialog
		super(parent, "general statistical analysis : " + stackInfoStr, ModalityType.MODELESS);
		
		// Set Raw Data
		this.dataSet = dataSet;
		this.itemCode = itemCode;
		
		// create statistic result table
		JScrollPane controlPanel = createResultTablePanel();
		
		// create histogram chart
		JPanel chartPanel = createChartPanel();
		
		// Add basePanel
		this.setPreferredSize(new Dimension(960, 530));
		this.setLayout(new BorderLayout());
		this.add(controlPanel, BorderLayout.WEST);
		this.add(chartPanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create statistic result table
	 */
	private JScrollPane createResultTablePanel() {
		
		// Prepare analysis
		TmsDataSingleItemRecord[] records = null;
		TmsDataCell[] tmsDataCell = null;
		DoAnalysisGeneralStats result = null;
				
		// analysis
		records = dataSet.getValidTmsDataRecords(itemCode);
		if (records.length != 0) {
			tmsDataCell = new TmsDataCell[records.length];
			for (int i=0 ; i<tmsDataCell.length ; i++) {
				tmsDataCell[i] = records[i].getTmsDataCell();
			}
			result = new DoAnalysisGeneralStats(tmsDataCell);
		}
		
		// create table
		JTable dataTable = new JTable(new TableModel(result));
		dataTable.setRowHeight(30);
		dataTable.getColumnModel().getColumn(0).setPreferredWidth(200);
		dataTable.getColumnModel().getColumn(1).setPreferredWidth(100);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );

		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		dataTable.getColumnModel().getColumn(0).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(1).setCellRenderer( rightRenderer );
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(dataTable);
		basePane.setPreferredSize(new Dimension(300, 600));
		basePane.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("analysis result"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePane.getBorder()));

		return basePane;
		
	} // end of method
	
	
	/**
	 * create chart Panel
	 */
	private JPanel createChartPanel() {
		
		// create DataSet
		HistogramDataset chartDataSet = createDataSet();
		
		// create chart
		JFreeChart chart = ChartFactory.createHistogram(
				"",
				null,
				null,
				chartDataSet,
				PlotOrientation.VERTICAL,
				true,
				true,
				false);
		
		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setDomainPannable(true);
		plot.setRangePannable(true);
		plot.setForegroundAlpha(0.85f);
		NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
		yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
		renderer.setDrawBarOutline(false);
		renderer.setBarPainter(new StandardXYBarPainter());
		renderer.setShadowVisible(false);

		// return
		return new ChartPanel(chart);
		
	} // end of method
	
	/**
	 * create dataset for histogram chart
	 */
	private HistogramDataset createDataSet() {
		
		// Prepare analysis
		TmsDataSingleItemRecord[] records = null;
		TmsDataCell[] tmsDataCell = null;
				
		records = dataSet.getValidTmsDataRecords(itemCode);
		if (records.length != 0) {
			tmsDataCell = new TmsDataCell[records.length];
			for (int i=0 ; i<tmsDataCell.length ; i++) {
				tmsDataCell[i] = records[i].getTmsDataCell();
			}
		}
		
		// prepare HistogramDataset
		HistogramDataset dataset = new HistogramDataset();
		if (tmsDataCell != null) {
			double[] values = new double[tmsDataCell.length];
			for (int i=0 ; i<tmsDataCell.length ; i++) {
				values[i] = tmsDataCell[i].getData_value();
			}
			dataset.addSeries(ItemCodeDefine.getItemName(itemCode), values, 50);
		}
		
		// return
		return dataset;
		
	} // end of method
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class TableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames = {
			"properties", "value"
		};
		
		private DoAnalysisGeneralStats result = null;

		public TableModel(DoAnalysisGeneralStats result) {
			this.result = result;
		}
		
		public String getColumnName(int col) {
			return columnNames[col];
		}
		
		public int getRowCount() { 
			return 14;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			// return name
			if (col == 0) {
				switch(row) {
				case 0:   return "item code";
				case 1:   return "data count";
				case 2:   return "mean value";
				case 3:   return "variance";
				case 4:   return "standard deviation";
				case 5:   return "skewness";
				case 6:   return "kurtosis";
				case 7:   return "min value";
				case 8:   return "max value";
				case 9:   return "median value";
				case 10:  return "percentile(25%)";
				case 11:  return "percentile(75%)";
				case 12:  return "95% confidence (lower)";
				case 13:  return "95% confidence (upper)";
				default:  return "";
				}
			}
			
			else {
				if (result == null) {
					return "";
				}
				switch(row) {
				case 0:   return   ItemCodeDefine.getItemName(itemCode);
				case 1:   return   NumberUtil.getNumberStr(result.getDataCount());
				case 2:   return   NumberUtil.getNumberStr(result.getMaxValue());
				case 3:   return   NumberUtil.getNumberStr(result.getVariance());
				case 4:   return   NumberUtil.getNumberStr(result.getStandardDeviation());
				case 5:   return   NumberUtil.getNumberStr(result.getSkewness());
				case 6:   return   NumberUtil.getNumberStr(result.getKurtosis());
				case 7:   return   NumberUtil.getNumberStr(result.getMInValue());
				case 8:   return   NumberUtil.getNumberStr(result.getMaxValue());
				case 9:   return   NumberUtil.getNumberStr(result.getPercentile050());
				case 10:  return   NumberUtil.getNumberStr(result.getPercentile025());
				case 11:  return   NumberUtil.getNumberStr(result.getPercentile075());
				case 12:  return   NumberUtil.getNumberStr(result.getConfidence95_lower());
				case 13:  return   NumberUtil.getNumberStr(result.getConfidence95_upper());
				default:  return "";
				}
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	
} // end of class
