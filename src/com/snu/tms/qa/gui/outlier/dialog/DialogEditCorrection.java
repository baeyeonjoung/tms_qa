/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import com.snu.tms.qa.analysis.outlier.DoAnalysisReverseCorrection;
import com.snu.tms.qa.model.CorrectionItemInfo;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.TmsDataset;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;


/**
 * Edit correction factor
 * 
 */
public class DialogEditCorrection extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  TmsDataset dataSet;
	
	private  StackInfo  stackInfo;
	
	private  JFrame     parent;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogEditCorrection(
			JFrame parent, 
			String stackInfoStr,
			TmsDataset dataSet) {
		
		// set dialog
		super(parent, "Raw data viewer : " + stackInfoStr, ModalityType.MODELESS);
		this.parent = parent;

		// set icon
		try {
			setIconImage(ImageIO.read(new File("image/smart.png")));
		} catch (IOException exc) {}
		
		// Set Raw Data
		this.dataSet = dataSet;
		this.stackInfo = new StackInfo(dataSet.getTmsDataHeader().getStackInfo());
		//System.out.println(this.stackInfo);
		
		// create controlPane
		JPanel controlPanel = createControlPanel();
		
		// create Table base Panel
		JScrollPane tablePanel = createTablePanel();
		
		// add basePanel
		JPanel basePanel = new JPanel(new BorderLayout());
		basePanel.add(controlPanel, BorderLayout.NORTH);
		basePanel.add(tablePanel, BorderLayout.CENTER);
		this.add(basePanel);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create control panel
	 */
	private JPanel createControlPanel() {
		
		// Set basePanel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(600, 40));
		
		// create button
		JButton applyButton = new JButton("apply");
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// user confirm
				int confirm = JOptionPane.showConfirmDialog(
						parent, 
						"Are you sure to apply a correction factor to change?");
				if (confirm == JOptionPane.OK_OPTION) {
					applyChange();
					closeWindow();
				}
			}
		});
		
		applyButton.setBounds(20, 10, 120, 20);
		basePanel.add(applyButton);
		
		return basePanel;
		
	} // end of method
	
	
	/**
	 * create table panel
	 */
	private JScrollPane createTablePanel() {
		
		// Set JTable
		TableModel tableModel = new TableModel();
		JTable dataTable = new JTable(tableModel);

		dataTable.setRowHeight(25);
		dataTable.getColumnModel().getColumn(0).setPreferredWidth(80);
		dataTable.getColumnModel().getColumn(1).setPreferredWidth(130);
		dataTable.getColumnModel().getColumn(2).setPreferredWidth(130);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		dataTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		dataTable.getColumnModel().getColumn(3).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(4).setCellRenderer( rightRenderer );
		
		dataTable.getColumnModel().getColumn(1).setCellRenderer( new ButtonRenderer() );
		dataTable.getColumnModel().getColumn(1).setCellEditor( new ButtonEditor(new JCheckBox()) );

		dataTable.getColumnModel().getColumn(2).setCellRenderer( new ButtonRenderer() );
		dataTable.getColumnModel().getColumn(2).setCellEditor( new ButtonEditor(new JCheckBox()) );

		// Add basePanel
		JScrollPane tablePanel = new JScrollPane(dataTable);
		tablePanel.setPreferredSize(new Dimension(700, 300));
		
		return tablePanel;
		
	} // end of method
	
	/**
	 * Close this window
	 */
	public void closeWindow() {
		this.dispose();
	} // end of method
	
	/**
	 * Apply Pattern Analysis Result
	 */
	public void applyChange() {
		
		// set correction factor
		this.dataSet.getTmsDataHeader().getStackInfo().setCorrectionInfo(this.stackInfo);
		
		// do reverse correction
		DoAnalysisReverseCorrection.doReverseCorrection(this.dataSet);
		
	} // end of method

	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class TableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		public int[]      items     = null;

		public TableModel() {

			// get items
			this.items = dataSet.getItems();
		
		} // end of constructor
		
		public String getColumnName(int col) {
			
			if (col == 0) {
				return "Item";
			} else if (col ==1) {
				return "check data";
			} else if (col ==2) {
				return "show graph";
			} else if (col ==3) {
				return "Std.Oxgn";
			} else if (col ==4) {
				return "Std.Moi";
			} else if (col ==5) {
				return "Oxgn Corr.";
			} else if (col ==6) {
				return "Temp Corr.";
			} else if (col ==7) {
				return "Mois Corr.";
			} else {
				return "";
			}
		}
		
		public int getRowCount() { 
			if (dataSet == null) {
				return 0;
			}
			return this.items.length;
		}
		
		public int getColumnCount() { 
			return 8;
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return ItemCodeDefine.getItemName(this.items[row]);
			} else if (col == 1) {
				return "table-" + this.items[row];
			} else if (col == 2) {
				return "graph-" + this.items[row];
			} else if (col == 3) {
				return String.format("%3.0f", stackInfo.getCorrectionInfo(this.items[row]).getStdOxygen());
			} else if (col == 4) {
				return String.format("%3.2f", stackInfo.getStandardMoisture());
			} else if (col == 5) {
				return new Boolean(stackInfo.getCorrectionInfo(this.items[row]).isOxygenApplied());
			} else if (col == 6) {
				return new Boolean(stackInfo.getCorrectionInfo(this.items[row]).isTemperatureApplied());
			} else if (col == 7) {
				return new Boolean(stackInfo.getCorrectionInfo(this.items[row]).isMoistureApplied());
			} else {
				return "";
			}
		
		} // end of method
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return String.class;
			} else if (col == 1) {
				return Object.class;
			} else if (col == 2) {
				return Object.class;
			} else if (col == 3) {
				return String.class;
			} else if (col == 4) {
				return String.class;
			} else {
				return Boolean.class;
			}
			
		}

		public boolean isCellEditable(int row, int col) {
			
			if (col == 0 || col == 4) {
				return false;
			} else {
				return true;
			}
		}
		
		public void setValueAt(Object value, int row, int col) {
			
			if (col == 0) {
				return;
			} 
			
			// get target item
			int item = this.items[row];
			CorrectionItemInfo corrInfo = stackInfo.getCorrectionInfo(item);
			
			if (col == 3) {
				double numValue = 0.0D;
				try {
					numValue = Double.parseDouble(((String) value));
				} catch (Exception e) {}
				corrInfo.setStdOxygen(numValue);
			} else if (col == 5) {
				boolean isChecked = ((Boolean) value).booleanValue();
				corrInfo.setOxygenApplied(isChecked);
			} else if (col == 6) {
				boolean isChecked = ((Boolean) value).booleanValue();
				corrInfo.setTemperatureApplied(isChecked);
			} else if (col == 7) {
				boolean isChecked = ((Boolean) value).booleanValue();
				corrInfo.setMoistureApplied(isChecked);
			} else {
				return;
			}
		}
		
		
	} // end of class
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class ButtonRenderer extends JButton implements TableCellRenderer {

		private static final long serialVersionUID = 1L;

		public ButtonRenderer() {
			setOpaque(true);
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value,
				boolean isSelected, boolean hasFocus, int row, int column) {
			
			if (isSelected) {
				setForeground(table.getSelectionForeground());
				setBackground(table.getSelectionBackground());
			} else {
				setForeground(table.getForeground());
				setBackground(UIManager.getColor("Button.background"));
			}
			
			String valueStr = (String)value;
			if (valueStr.startsWith("table")) {
				setText("check data");
			} else {
				setText("show graph");
			}
			return this;
		
		} // end of method

	} // end of inner class

	//==========================================================================
	// Inner Class
	//==========================================================================
	class ButtonEditor extends DefaultCellEditor {

		private static final long serialVersionUID = 1L;

		protected JButton button;
		private   boolean isPushed;

		private   int     selectedItem = ItemCodeDefine.UNDEFINED;
		private   String  selectedAction = "";
		
		public ButtonEditor(JCheckBox checkBox) {
			super(checkBox);
			button = new JButton();
			button.setOpaque(true);
			button.addActionListener(new ActionListener() {
			    @Override
			    public void actionPerformed(ActionEvent e) {
					fireEditingStopped();
				}
			});
		}

		@Override
	    public Component getTableCellEditorComponent(JTable table, Object value,
				boolean isSelected, int row, int column) {
			if (isSelected) {
				button.setForeground(table.getSelectionForeground());
				button.setBackground(table.getSelectionBackground());
			} else {
				button.setForeground(table.getForeground());
				button.setBackground(table.getBackground());
			}
			
			//button.setText("compare data");
			isPushed = true;

			String valueStr = (String)value;
			selectedItem = Integer.parseInt(valueStr.substring(6, valueStr.length()));
			if (valueStr.startsWith("table")) {
				selectedAction = "table";
			} else {
				selectedAction = "graph";
			}
			return button;
		}

		@Override
		public Object getCellEditorValue() {
			if (isPushed) {
				
				if (selectedAction.equals("table")) {
					new DialogEditCorrectionDataViewer(parent, stackInfo, dataSet, selectedItem);
				} else {
					String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d)",
							dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
							dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
							dataSet.getTmsDataHeader().getStackInfo().getStackNumber());
					new DialogDataCompareGraph(parent, stackInfoStr, stackInfo, dataSet, selectedItem);
				}
			}
			isPushed = false;
			return "";
		}

		@Override
		public boolean stopCellEditing() {
			isPushed = false;
			return super.stopCellEditing();
		}

		@Override
		protected void fireEditingStopped() {
			super.fireEditingStopped();
		}
		
	} // end of inner class
	
	
} // end of class
