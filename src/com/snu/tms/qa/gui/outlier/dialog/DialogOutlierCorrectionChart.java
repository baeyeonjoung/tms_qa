/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.RawDataCell;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.*;
import java.text.SimpleDateFormat;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;


/**
 * Show Graph
 * 
 */
public class DialogOutlierCorrectionChart extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	private  int                 item;
	private  DetectionRecord[]   dataRecords;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogOutlierCorrectionChart(
			JFrame parent, 
			String chartName,
			int    item,
			DetectionRecord[] dataRecords) {
		
		// set dialog
		super(parent, "", ModalityType.MODELESS);
		this.setTitle(chartName);
		
		// set parameters
		this.item = item;
		this.dataRecords = dataRecords;
		
		// create chart
		JPanel chartPanel = createChartPanel();
		chartPanel.setPreferredSize(new Dimension(800, 500));
		
		this.setLayout(new BorderLayout());
		this.add(chartPanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * Implemented ActionListener
	 */
	public void actionPerformed(ActionEvent e) {
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create chart Panel
	 */
	private JPanel createChartPanel() {
	
		// create XYDataSet
		TimeSeriesCollection chartDataSet = createDataSet();
		
		// create chart
		JFreeChart chart = ChartFactory.createTimeSeriesChart(
				"",  // title
				"date",             // x-axis label
				ItemCodeDefine.getItemName(this.item),   // y-axis label
				chartDataSet,            // data
				true,               // create legend?
				true,               // generate tooltips?
				false               // generate URLs?
		);

		// set Font
		chart.getTitle().setFont(new Font("����", Font.BOLD, 14));
		chart.getLegend().setItemFont(new Font("����", Font.BOLD, 12));

		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setOrientation(PlotOrientation.VERTICAL);
		plot.setDomainPannable(true);
		plot.setRangePannable(false);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		// set x-axis properties
		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("yy/MM/dd"));

		// set range axis
		XYItemRenderer renderer = new StandardXYItemRenderer();
		plot.setRenderer(renderer);
		
		// create chart Panel
		ChartPanel basePanel = new ChartPanel(chart);
		basePanel.setPreferredSize(new Dimension(800, 500));

		// Add ActionListener
		
		return basePanel;
		
	} // end of method

	/**
	 * Create graph data set
	 */
	private TimeSeriesCollection createDataSet() {
		
		// create timeseries data
		TimeSeries[] series = new TimeSeries[2];
		series[0] = new TimeSeries("after corr.");
		series[1] = new TimeSeries("beofre corr.");
		
		Minute time = null;
		double value1, value2;
		RawDataCell dataCell;
		for (DetectionRecord record : this.dataRecords) {
			
			time = new Minute(new java.util.Date(record.getTime()));
			dataCell = record.getRawDataCell(this.item);
			
			value1 = dataCell.getData_value();
			value2 = dataCell.getOrigin_value();

			if (value1 != -1.0D) {
				series[0].add(time, value1);
			}
			if (value2 != -1.0D) {
				series[1].add(time, value2);
			}
			
		}
		
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(series[0]);
		dataset.addSeries(series[1]);
		
		return dataset;
		
	} // end of method
	
	//==========================================================================
	// Inner Class
	//==========================================================================

	
} // end of class
