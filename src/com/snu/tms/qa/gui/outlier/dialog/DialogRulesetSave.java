/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import com.snu.tms.qa.model.DatasetManager;
import com.snu.tms.qa.model.LoginUserManager;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.RulesetModel;
import com.snu.tms.qa.soap.RulesetModelUpdateAgent;
import com.snu.tms.qa.util.DateUtil;

/**
 * set detection weighting factor and threshold
 * 
 */
public class DialogRulesetSave extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  DatasetManager  datasetManager;
	private  RulesetModel    rulesetModel;
	
	private  JTextField  rulesetIdInput;
	private  JTextField  factCodeInput;
	private  JTextField  stckCodeInput;
	private  JTextField  rulesetNameInput;
	private  JTextArea   rulesetDescInput;
	private  JTextField  rulesetBaseTimeInput;
	
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogRulesetSave(JFrame parent) {
		
		// set dialog
		super(parent, "", ModalityType.APPLICATION_MODAL);
		this.setTitle("Save Ruleset");
		
		// set parameters
		this.datasetManager = DatasetManager.getInstance();
		this.rulesetModel = this.datasetManager.getRulesetModel();
		
		// set layout
		JPanel       controlPanel    = createControlPanel();
		JPanel       basePanel       = createContentsPanel();
		
		controlPanel.setPreferredSize(new Dimension(800, 50));
		basePanel.setPreferredSize(new Dimension(800, 500));
		
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(800, 550));
		this.add(controlPanel, BorderLayout.NORTH);
		this.add(basePanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create control panel
	 */
	private JPanel createControlPanel() {
		
		// create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("  ruleset save control  "),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));

		// add save threshold button
		JButton applyButton = new JButton("apply");
		applyButton.setBounds(20, 20, 100, 20);
		applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doSaveRuleset();
				closeWindow();
			}
		});
		basePanel.add(applyButton);
		
		JButton cancleButton = new JButton("cancle");
		cancleButton.setBounds(150, 20, 100, 20);
		cancleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeWindow();
			}
		});
		basePanel.add(cancleButton);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * create contents panel
	 */
	private JPanel createContentsPanel() {
		
		// create JPanel
		JPanel basePanel = new JPanel(null);
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("  set ruleset informations  "),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// set layout
		JLabel label = null;
		label = new JLabel("Ruleset Id");
		label.setBounds(20, 25, 150, 25);
		basePanel.add(label);

		label = null;
		label = new JLabel("Factory Code");
		label.setBounds(20, 55, 150, 25);
		basePanel.add(label);
		
		label = new JLabel("Stack Code");
		label.setBounds(20, 85, 150, 25);
		basePanel.add(label);
		
		label = new JLabel("Ruleset Name");
		label.setBounds(20, 115, 150, 25);
		basePanel.add(label);
		
		label = new JLabel("Ruleset Description");
		label.setBounds(20, 145, 150, 25);
		basePanel.add(label);
		
		label = new JLabel("Ruleset Base Time");
		label.setBounds(20, 255, 150, 25);
		basePanel.add(label);
		
		label = new JLabel("Pattern results");
		label.setBounds(20, 285, 150, 25);
		basePanel.add(label);
		
		String rulesetIdStr = "";
		if (rulesetModel.getRulesetId() != -1) {
			rulesetIdStr = rulesetModel.getRulesetId() + "";
		}
		this.rulesetIdInput = new JTextField(rulesetIdStr);
		this.rulesetIdInput.setBounds(180, 25, 100, 25);
		this.rulesetIdInput.setEditable(false);
		basePanel.add(this.rulesetIdInput);

		this.factCodeInput = new JTextField(rulesetModel.getFactCode());
		this.factCodeInput.setBounds(180, 55, 100, 25);
		this.factCodeInput.setEditable(false);
		basePanel.add(this.factCodeInput);
		
		this.stckCodeInput = new JTextField(rulesetModel.getStckCode() + "");
		this.stckCodeInput.setBounds(180, 85, 100, 25);
		this.stckCodeInput.setEditable(false);
		basePanel.add(this.stckCodeInput);
		
		this.rulesetNameInput = new JTextField(rulesetModel.getRulesetName());
		this.rulesetNameInput.setBounds(180, 115, 360, 25);
		this.rulesetNameInput.setEditable(true);
		basePanel.add(this.rulesetNameInput);
		
		this.rulesetDescInput = new JTextArea(rulesetModel.getRulesetDescr());
		this.rulesetDescInput.setPreferredSize(new Dimension(500, 200));
		this.rulesetDescInput.setEditable(true);
		JScrollPane rulesetDescBasePane = new JScrollPane(this.rulesetDescInput);
		rulesetDescBasePane.setBounds(180, 145, 590, 105);
		basePanel.add(rulesetDescBasePane);
		
		String rulesetBaseTime = String.format("%s  ~  %s", 
				DateUtil.getDateStr(rulesetModel.getRulesetStartTime()),
				DateUtil.getDateStr(rulesetModel.getRulesetEndTime()));
		this.rulesetBaseTimeInput = new JTextField(rulesetBaseTime);
		this.rulesetBaseTimeInput.setBounds(180, 255, 300, 25);
		this.rulesetBaseTimeInput.setEditable(false);
		basePanel.add(this.rulesetBaseTimeInput);
		
		JScrollPane tableBasePanel = createPatternInfoTable();
		tableBasePanel.setBounds(180, 285, 560, 170);
		basePanel.add(tableBasePanel);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * create detection reuslt table
	 */
	private JScrollPane createPatternInfoTable() {
		
		// Create Table
		PatternInfoTableModel patternInfoTableModel = new PatternInfoTableModel();
		JTable patternInfoTable = new JTable(patternInfoTableModel);
		patternInfoTable.setRowHeight(20);
		patternInfoTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		
		patternInfoTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		patternInfoTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		
		patternInfoTable.getColumnModel().getColumn(0).setPreferredWidth(70);
		patternInfoTable.getColumnModel().getColumn(1).setPreferredWidth(150);
		patternInfoTable.getColumnModel().getColumn(2).setPreferredWidth(70);
		patternInfoTable.getColumnModel().getColumn(3).setPreferredWidth(70);
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(patternInfoTable);
		return basePane;
		
	} // end of method
	
	/**
	 * do save ruleset
	 */
	private void doSaveRuleset() {
		
		// set parameters
		String name  = this.rulesetNameInput.getText().trim();
		String descr = this.rulesetDescInput.getText().trim();
		this.rulesetModel.setRulesetName(name);
		this.rulesetModel.setRulesetDescr(descr);
		this.rulesetModel.setUserId(LoginUserManager.getInstance().getUserId());
		
		// set rulesetModel sub-components
		this.datasetManager.filloutRulesetModel(this.rulesetModel);
		
		// do insert or update query
		int rulesetId = RulesetModelUpdateAgent.updateRulesetModel(this.rulesetModel);
		if (rulesetId != -1) {
			this.rulesetModel.setRulesetId(rulesetId);
			this.rulesetIdInput.updateUI();
		}
		
	} // end of method
	
	/**
	 * Close this window
	 */
	public void closeWindow() {
		this.dispose();
	} // end of method

	
	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * Outlier Check Table Model
	 */
	class PatternInfoTableModel extends AbstractTableModel {
		
		//==========================================================================
		// Static Fields
		//==========================================================================
		private static final long serialVersionUID = 1L;
		
		//==========================================================================
		// Fields
		//==========================================================================
		private String[] columnNames = {"Pattern Id", "Pattern Name", "Ruleset Created", "Ruleset Enabled"};
		private DetectionPatternDataset[]  patternDatasetArray;
		
		//==========================================================================
		// Constructors
		//==========================================================================
		public PatternInfoTableModel() { 
			
			// set parameters
			this.patternDatasetArray = datasetManager.getAllPatternData();
			
		} // end of constructor
		
		//==========================================================================
		// Implemented Methods
		//==========================================================================
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			
			if (this.patternDatasetArray == null) {
				return 0;
			} else {
				return this.patternDatasetArray.length;
			}
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return this.patternDatasetArray[row].getPatternId();
			} else if (col == 1) {
				return this.patternDatasetArray[row].getPatternName();
			} else if (col == 2) {
				return this.patternDatasetArray[row].isRulesetCreated();
			} else if (col == 3) {
				return this.patternDatasetArray[row].isEnabled();
			} else {
				return "";
			}
		
		} // end of method
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 0) {
				return String.class;
			} else if (col == 1) {
				return String.class;
			} else if (col == 2) {
				return Boolean.class;
			} else if (col == 3) {
				return Boolean.class;
			} else {
				return String.class;
			}
			
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		} // end of method
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class

} // end of class
