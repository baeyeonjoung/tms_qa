/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import com.snu.tms.qa.analysis.confidenceinterval.DoAnlaysisWithPercentOfDifference;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.correction.TmsDataCorrectionModel;
import com.snu.tms.qa.util.NumberUtil;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.text.SimpleDateFormat;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * 
 * 
 */
public class DialogReverseCorrectionAnalysis extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  TmsDataset dataSet;
	private  int        itemCode;
	private  TmsDataCorrectionModel model;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogReverseCorrectionAnalysis(
			JFrame parent, 
			String stackInfoStr,
			int itemCode,
			TmsDataset dataSet) {
		
		// set dialog
		super(parent, "reverse correction analysis : " + stackInfoStr, ModalityType.MODELESS);
		
		// Set Raw Data
		this.dataSet = dataSet;
		this.itemCode = itemCode;
		
		// execute analysis
		this.model = DoAnlaysisWithPercentOfDifference.doAnlaysisWithPercentOfDifference(
				itemCode, dataSet, 95.0D);
		
		// Create Information panel
		JPanel infoPanel       = createInfoPanel();
		JScrollPane tablePanel = createTablePanel();
		JPanel upperChartPanel = createHistogramPanel();
		JPanel lowerChartPanel = createTimeseriesPanel();
		
		infoPanel.setPreferredSize(new Dimension(450, 90));
		tablePanel.setPreferredSize(new Dimension(450, 600));
		upperChartPanel.setPreferredSize(new Dimension(500, 250));
		lowerChartPanel.setPreferredSize(new Dimension(500, 450));
		
		// Add basePanel
		JSplitPane basePanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		basePanel.setDividerLocation(450);
		
		JPanel LeftPanel  = new JPanel(new BorderLayout());
		LeftPanel.add(infoPanel, BorderLayout.NORTH);
		LeftPanel.add(tablePanel, BorderLayout.CENTER);
		
		JPanel rightPanel = new JPanel(new BorderLayout());
		rightPanel.add(upperChartPanel, BorderLayout.NORTH);
		rightPanel.add(lowerChartPanel, BorderLayout.CENTER);
		
		basePanel.add(LeftPanel);
		basePanel.add(rightPanel);
		
		this.setPreferredSize(new Dimension(960, 700));
		this.setLayout(new BorderLayout());
		this.add(basePanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create statistic result table
	 */
	private JPanel createInfoPanel() {
		
		// Add basePanel
		JPanel basePanel = new JPanel(null);
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("percent of difference limits"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// Add textfield
		JLabel label = new JLabel("upper limit of percent of difference");
		label.setBounds(30, 25, 220, 25);
		basePanel.add(label);
		
		label = new JLabel("lower limit of percent of difference");
		label.setBounds(30, 55, 220, 25);
		basePanel.add(label);
		
		JTextField limit = new JTextField(NumberUtil.getNumberStr(model.getPercentOfDiffUpperLimit()));
		limit.setEditable(false);
		limit.setHorizontalAlignment(JTextField.RIGHT);
		limit.setBounds(250, 25, 150, 25);
		basePanel.add(limit);
		
		limit = new JTextField(NumberUtil.getNumberStr(model.getPercentOfDiffLowerLimit()));
		limit.setEditable(false);
		limit.setHorizontalAlignment(JTextField.RIGHT);
		limit.setBounds(250, 55, 150, 25);
		basePanel.add(limit);

		return basePanel;
		
	} // end of method
	
	/**
	 * create statistic result table
	 */
	private JScrollPane createTablePanel() {
		
		// create table
		JTable dataTable = new JTable(new TableModel(model));
		dataTable.setRowHeight(20);
		dataTable.getColumnModel().getColumn(0).setPreferredWidth(50);
		dataTable.getColumnModel().getColumn(1).setPreferredWidth(140);
		dataTable.getColumnModel().getColumn(2).setPreferredWidth(90);
		dataTable.getColumnModel().getColumn(3).setPreferredWidth(90);
		dataTable.getColumnModel().getColumn(4).setPreferredWidth(80);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );

		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		dataTable.getColumnModel().getColumn(0).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		dataTable.getColumnModel().getColumn(2).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(3).setCellRenderer( rightRenderer );
		dataTable.getColumnModel().getColumn(4).setCellRenderer( rightRenderer );
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(dataTable);
		basePane.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("reverse correction result"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePane.getBorder()));

		return basePane;
		
	} // end of method
	
	
	/**
	 * create chart Panel
	 */
	private JPanel createHistogramPanel() {
		
		// create DataSet
		HistogramDataset chartDataSet = createHistogramDataSet();
		
		// create chart
		JFreeChart chart = ChartFactory.createHistogram(
				"",
				null,
				null,
				chartDataSet,
				PlotOrientation.VERTICAL,
				true,
				true,
				false);
		
		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setDomainPannable(true);
		plot.setRangePannable(true);
		plot.setForegroundAlpha(0.85f);
		NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
		yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
		renderer.setDrawBarOutline(false);
		renderer.setBarPainter(new StandardXYBarPainter());
		renderer.setShadowVisible(false);

		// return
		return new ChartPanel(chart);
		
	} // end of method
	
	
	/**
	 * create chart Panel
	 */
	private JPanel createTimeseriesPanel() {
		
		// create XYDataSet
		XYDataset chartDataSet = createTimeseriesDataSet();
		
		// create chart
		JFreeChart chart = ChartFactory.createTimeSeriesChart(
				"",  // title
				"date",             // x-axis label
				"percent of difference(%)",   // y-axis label
				chartDataSet,            // data
				true,               // create legend?
				true,               // generate tooltips?
				false               // generate URLs?
		);
		
		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setDomainPannable(true);
		plot.setRangePannable(false);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);
				
		// set chart shape properties
		XYItemRenderer r = plot.getRenderer();
		if (r instanceof XYLineAndShapeRenderer) {
			XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
			renderer.setBaseShapesVisible(true);
		}
				
		// set x-axis properties
		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("yyyy-MM-dd"));

		// return
		return new ChartPanel(chart);
		
	} // end of method
	
	
	/**
	 * create dataset for histogram chart
	 */
	private HistogramDataset createHistogramDataSet() {
		
		// Prepare analysis
		int count = model.getDataCount();
				
		// prepare HistogramDataset
		HistogramDataset dataset = new HistogramDataset();
		if (count != 0) {
			double[] values = new double[count];
			for (int i=0 ; i<count ; i++) {
				values[i] = model.getDataTmsValue(i);
			}
			dataset.addSeries(ItemCodeDefine.getItemName(itemCode) + "(TMS)", values, 50);
			
			values = new double[count];
			for (int i=0 ; i<count ; i++) {
				values[i] = model.getDataReverseCorrValue(i);
			}
			dataset.addSeries(ItemCodeDefine.getItemName(itemCode) + "(REVERSE CORR.)", values, 50);
		}
		
		// return
		return dataset;
		
	} // end of method
	
	
	/**
	 * create dataset for histogram chart
	 */
	private XYDataset createTimeseriesDataSet() {
		
		// Prepare analysis
		int count = model.getDataCount();
		
		// create TimeSeries
		TimeSeries tmsDataset = new TimeSeries(ItemCodeDefine.getItemName(itemCode) + "(Percent of Difference)");
		
		// prepare TimeSeries
		Minute time = null;
		for (int i=0 ; i<count ; i++) {
			time = new Minute(new java.util.Date(model.getDataTime(i)));
			tmsDataset.add(time, model.getDataPercentOfDiff(i));
		}
		
		// Create TimeSeriesCollection
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(tmsDataset);
		
		// return
		return dataset;
		
	} // end of method
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class TableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;

		private final  String[] columnNames = {
			"no.", "time", "tms_value", "corr_value", "diff(%)"
		};
		
		private TmsDataCorrectionModel model = null;

		public TableModel(TmsDataCorrectionModel model) {
			this.model = model;
		}
		
		public String getColumnName(int col) {
			return columnNames[col];
		}
		
		public int getRowCount() { 
			return model.getDataCount();
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			switch(col) {
			case 0:  return  row;
			case 1:  return  model.getDataTimeStr(row);
			case 2:  return  NumberUtil.getNumberStr(model.getDataTmsValue(row));
			case 3:  return  NumberUtil.getNumberStr(model.getDataReverseCorrValue(row));
			case 4:  return  NumberUtil.getNumberStr(model.getDataPercentOfDiff(row));
			default: return  "";
			}

		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	
} // end of class
