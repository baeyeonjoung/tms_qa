/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.dialog;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.event.ChartProgressEvent;
import org.jfree.chart.event.ChartProgressListener;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.PatternDataSet;
import com.snu.tms.qa.model.ruleset.PatternDataRecord;
import com.snu.tms.qa.util.NumberUtil;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Hashtable;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * Show Graph
 * 
 */
public class DialogPatternResult extends JDialog
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  PatternDataSet patternDataSet = null;
	private  JTabbedPane    patternBasePanel = null;
	
	private  int            selectedItemIndex = 0;
	private  int            selectedItem = ItemCodeDefine.UNDEFINED;
	
	private  JPanel         chartBasePanel;
	
	private  Hashtable<Long, Integer>    timeHash;
	
	private  JTable         rawDataTable;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public DialogPatternResult(
			final JFrame   parent, 
			final String   stackInfoStr,
			PatternDataSet dataSet) {
		
		// set dialog
		super(parent, "Pattern Result : " + stackInfoStr, ModalityType.MODELESS);

		// set icon
		try {
			setIconImage(ImageIO.read(new File("image/smart.png")));
		} catch (IOException exc) {}
		
		// set parameters
		this.patternDataSet = dataSet;
		
		// create timeHash
		createTimeHashtable();
		
		// Pattern analysis panel
		this.patternBasePanel = new JTabbedPane();
		setPatternBasePanel();
		
		// set ui
		this.setPreferredSize(new Dimension(1100, 800));
		this.setLayout(new BorderLayout());
		this.add(this.patternBasePanel, BorderLayout.CENTER);
		
		// Set view
		this.pack();
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * set Pattern BasePanel
	 */
	private void setPatternBasePanel() {
		
		// Set Pattern analysis result
		this.patternBasePanel.addTab("Item value histogram", createPatternAnalysisResultPanel());
		
		// set pattern data timeseries graph
		this.patternBasePanel.addTab("Timeseries graph", createPatternTimeseriesPanel());
		
	} // end of method
	
	
	/**
	 * Set Pattern analysis result
	 */
	private JPanel createPatternAnalysisResultPanel() {
		
		// make basePanel
		JPanel basePanel = new JPanel(new BorderLayout());
		
		// create Pattern Analysis result table
		JScrollPane tablePanel = createPatternAnalysisResultTablePanel();
		int patternCount = this.patternDataSet.getPatternIndexArray().length;
		tablePanel.setPreferredSize(new Dimension(600, 5+20*(patternCount+1)));
		basePanel.add(tablePanel, BorderLayout.NORTH);
		
		// create data scattor plot
		JPanel chartBasePanel = new JPanel(new GridLayout(0, 1));
		int[] patternIndices = this.patternDataSet.getPatternIndexArray();
		for (int patternIndex : patternIndices) {
			JPanel chartPanel = createHistogramPanel(patternIndex);
			chartBasePanel.add(chartPanel);
		}
		JScrollPane lowerBasePanel = new JScrollPane(chartBasePanel);
		lowerBasePanel.setPreferredSize(new Dimension(1100, 600));
		
		// return
		basePanel.add(lowerBasePanel, BorderLayout.CENTER);
		return basePanel;
		
	} // end of method
	
	
	/**
	 * createPatternTimeseriesPanel
	 */
	private JPanel createPatternTimeseriesPanel() {
		
		// make basePanel
		JPanel basePanel = new JPanel(new BorderLayout());
		
		// make control panel
		JPanel upperBasePanel = createControlPanel();
		upperBasePanel.setPreferredSize(new Dimension(1100, 35));
		
		// make lower base panel
		JSplitPane lowerBasePanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		lowerBasePanel.setDividerLocation(300);
		lowerBasePanel.setDividerSize(5);
		lowerBasePanel.setPreferredSize(new Dimension(1100, 550));
		
		this.chartBasePanel = new JPanel(new BorderLayout());
		this.chartBasePanel.add(createTimeseriesPanel(), BorderLayout.CENTER);
		lowerBasePanel.add(this.chartBasePanel);

		JScrollPane tablePanel = createRawDataTablePanel();
		lowerBasePanel.add(tablePanel);
		
		// Base Panel
		basePanel.add(upperBasePanel, BorderLayout.NORTH);
		basePanel.add(lowerBasePanel, BorderLayout.CENTER);
		
		// return 
		return basePanel;
		
	} // end of method
	
	/**
	 * create Control panel
	 */
	private JPanel createControlPanel() {
		
		// create base panel
		JPanel basePanel = new JPanel(null);
		
		// create JCombo
		int[] items = this.patternDataSet.getItems();
		String[] itemNames = new String[items.length];
		for (int i=0 ; i<items.length ; i++) {
			itemNames[i] = ItemCodeDefine.getItemName(items[i]);
		}
		final JComboBox<String> itemSelectionCombo = new JComboBox<String>(itemNames);
		itemSelectionCombo.setBounds(20, 5, 100, 20);
		basePanel.add(itemSelectionCombo);
		
		// set selected item
		this.selectedItemIndex = 0;
		this.selectedItem = items[0];
		itemSelectionCombo.setSelectedIndex(0);
		
		itemSelectionCombo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				
				// get item seleteion
				int selectedIndex = itemSelectionCombo.getSelectedIndex();
				
				// update timeseries graph
				updateChart(selectedIndex);
				
			}
		});
		
		// return
		return basePanel;
		
	} // end of method
	
	
	/**
	 * create timeseries graph
	 */
	private JPanel createTimeseriesPanel() {
		
		// create XYDataSet
		XYDataset chartDataSet = createTimeseriesDataSet();
		
		// create chart
		final JFreeChart chart = ChartFactory.createTimeSeriesChart(
				"",           // title
				"",           // x-axis label
				"",           // y-axis label
				chartDataSet, // data
				true,        // create legend?
				true,         // generate tooltips?
				false         // generate URLs?
		);

		// set Font
		chart.getTitle().setFont(new Font("����", Font.BOLD, 14));
		chart.getLegend().setItemFont(new Font("����", Font.BOLD, 12));

		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setNoDataMessage("NO DATA");
		plot.setDomainPannable(true);
		plot.setRangePannable(false);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);
		
		// set chart shape properties
		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		plot.setRangeCrosshairVisible(true);
		plot.setRangeCrosshairLockedOnData(true);
		plot.setDomainZeroBaselineVisible(true);
		plot.setRangeZeroBaselineVisible(true);
		XYDotRenderer renderer = new XYDotRenderer();
		renderer.setDotWidth(3);
		renderer.setDotHeight(3);
		plot.setRenderer(renderer);
		
		// set x-axis properties
		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("yy/MM/dd"));
		
		// Add chart listener
		chart.addProgressListener( new ChartProgressListener() {
			public void chartProgress(ChartProgressEvent event) {
				
				if (event.getType() != ChartProgressEvent.DRAWING_FINISHED) {
					return;
				}
				
				// get mouse click domain axis point
				XYPlot plot = (XYPlot) chart.getPlot();
				double xx = plot.getDomainCrosshairValue();
				
				// change table selection
				changeTableRowSelection((long)xx);
				
			} // end of method
		});

		// return
		return new ChartPanel(chart);	
		
	} // end of method
	
	
	/**
	 * create table panel
	 */
	private JScrollPane createRawDataTablePanel() {

		// Create Table
		this.rawDataTable = new JTable(new RawDataTableModel());
		this.rawDataTable.setRowHeight(20);
		this.rawDataTable.getColumnModel().getColumn(0).setPreferredWidth(170);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		this.rawDataTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		this.rawDataTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		this.rawDataTable.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		for (int i=0 ; i<this.patternDataSet.getItems().length ; i++) {
			this.rawDataTable.getColumnModel().getColumn(i+3).setCellRenderer( rightRenderer );
		}
				
		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.rawDataTable);
		basePane.setPreferredSize(new Dimension(1100, 2500));
		return basePane;
		
	} // end of method
	
	
	/**
	 * Create graph data set
	 */
	private XYDataset createTimeseriesDataSet() {
		
		// create TimeSeries
		int[] patternIndices = this.patternDataSet.getPatternIndexArray();
		TimeSeries[] timeseries = new TimeSeries[patternIndices.length];
		for (int i=0 ; i<timeseries.length ; i++) {
			timeseries[i] = new TimeSeries(String.format(" %d [ %s ]", 
					patternIndices[i],
					this.patternDataSet.getPatternName(patternIndices[i])));
		}
		
		// prepare parameters
		Minute time = null;
		int itemIndex = this.selectedItemIndex;
		
		// Set data
		double value;
		for (int i=0 ; i<patternIndices.length ; i++) {
			
			PatternDataRecord[] records = this.patternDataSet.getSortedPatternDataRecord(patternIndices[i]);
			for (int j=0 ; j<records.length ; j++) {
				
				value = records[j].getValue(itemIndex);
				time  = new Minute(new java.util.Date(records[j].getTime()));
				timeseries[i].add(time, value);
			}
		
		} // end of for-loop
		
		// Create TimeSeriesCollection
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		for (int i=0 ; i<timeseries.length ; i++) {
			dataset.addSeries(timeseries[i]);
		}
		
		// return
		return dataset;
		
	} // end of method
	
	
	/**
	 * Create time hashtable
	 */
	private void createTimeHashtable() {
		
		// create time hash
		this.timeHash = new Hashtable<Long, Integer>();
		
		// set data
		PatternDataRecord[] records = this.patternDataSet.getAllSortedPatternDataRecord();
		for (int i=0 ; i<records.length ; i++) {
			this.timeHash.put(records[i].getTime(), i);
		}
		
	} // end of method
	
	/**
	 * Table Selection
	 */
	private void changeTableRowSelection(long selectedTime) {
		
		// get table row index from time hash
		Integer row = this.timeHash.get(selectedTime);
		if (row == null) {
			return;
		}
		
		// table selection
		this.rawDataTable.setRowSelectionInterval(row, row);
		JViewport viewport = (JViewport)this.rawDataTable.getParent();
		Rectangle rect = this.rawDataTable.getCellRect(row, 0, true);
		Rectangle viewRect = viewport.getViewRect();
		
		int y = viewRect.y;
		if (rect.y >= viewRect.y && rect.y <= (viewRect.y + viewRect.height - rect.height)) {
			
		} else if (rect.y < viewRect.y) {
			y = rect.y;
		} else if (rect.y > (viewRect.y + viewRect.height - rect.height)) {
			 y = rect.y - viewRect.height + rect.height;
		}
		viewport.setViewPosition(new Point(0, y));
		
	} // end of method
	

	/**
	 * Set data table
	 */
	private JPanel createHistogramPanel(int patternIndex) {
		
		// Get items
		int[] items = this.patternDataSet.getItems();
		PatternDataRecord[] records = this.patternDataSet.getSortedPatternDataRecord(patternIndex);
		
		// create chart
		JPanel chartBasePanel = new JPanel(new GridLayout(0, 3));
		chartBasePanel.setPreferredSize(new Dimension(1000, 150*(items.length / 3)));
		
		JPanel chartPanel;
		for (int i=0 ; i<items.length ; i++) {
			chartPanel = createHistogram(
					records, patternIndex, i, items[i], this.patternDataSet.getConfidenceInterval(i));
			chartPanel.setPreferredSize(new Dimension(200, 150));
			chartBasePanel.add(chartPanel);
		}
		
		// set border
		String titile = String.format("Pattern_%d [ %s ]", 
				patternIndex, 
				this.patternDataSet.getPatternName(patternIndex));
		chartBasePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder(titile),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								chartBasePanel.getBorder()));

		
		// return
		return chartBasePanel;
		
	} // end of method
	
	
	/**
	 * create Histogram
	 */
	private JPanel createHistogram(
			PatternDataRecord[] records, int patternIndex, int itemIndex, int itemType, double[] range) {
		
		// prepare HistogramDataset
		HistogramDataset dataset = new HistogramDataset();
		double[] values = new double[records.length];
		for (int i=0 ; i<records.length ; i++) {
			values[i] = records[i].getValue(itemIndex);
		}
		dataset.addSeries(ItemCodeDefine.getItemName(itemType), values, 20);
		
		// create histogram chart
		JFreeChart chart = ChartFactory.createHistogram(
				"",
				null,
				null,
				dataset,
				PlotOrientation.VERTICAL,
				true,
				true,
				false);

		// set Font
		chart.getTitle().setFont(new Font("����", Font.BOLD, 14));
		chart.getLegend().setItemFont(new Font("����", Font.BOLD, 12));

		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setDomainPannable(true);
		plot.setRangePannable(true);
		plot.setForegroundAlpha(0.85f);
		NumberAxis xAxis = (NumberAxis) plot.getDomainAxis();
		//xAxis.setRange(range[0], range[1]);
		NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
		yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
		renderer.setDrawBarOutline(false);
		renderer.setBarPainter(new StandardXYBarPainter());
		renderer.setShadowVisible(false);

		// return
		return new ChartPanel(chart);
		
	} // end of method
	
	

	/**
	 * Set Pattern Analysis Result Table Panel
	 */
	private JScrollPane createPatternAnalysisResultTablePanel() {
		
		JTable patternResultTable = new JTable(new PatternAnalysisResultTableModel());
		patternResultTable.setRowHeight(20);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );

		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		patternResultTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		patternResultTable.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		patternResultTable.getColumnModel().getColumn(2).setCellRenderer( rightRenderer );
		patternResultTable.getColumnModel().getColumn(3).setCellRenderer( rightRenderer );
		
		// Add basePanel
		JScrollPane basePane = new JScrollPane(patternResultTable);
		return basePane;
		
	} // end of method
	
	/**
	 * update chart panel
	 */
	private void updateChart(int selectedItemIndex) {
		
		// set parameters
		this.selectedItemIndex = selectedItemIndex;
		this.selectedItem = this.patternDataSet.getItems()[selectedItemIndex];
		
		// chage chart panel
		this.chartBasePanel.removeAll();
		this.chartBasePanel.add(createTimeseriesPanel(), BorderLayout.CENTER);
		this.chartBasePanel.updateUI();
		
	} // end of method
	
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class PatternAnalysisResultTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		private String[] columnNames = {"Pattern Index", "Pattern Name", "Data Count", "% Data Count"};
		
		public PatternAnalysisResultTableModel() {
			
		} // end of constructor
			
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() {
			
			int[] patternIndices = patternDataSet.getPatternIndexArray();
			return patternIndices.length;
		
		} // end of method
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			int[] patternIndices = patternDataSet.getPatternIndexArray();
			int selectedPatternIndex = patternIndices[row];
			if (col == 0) {
				return "Pattern[" + selectedPatternIndex + "]";
			} else if (col == 1) {
				return patternDataSet.getPatternName(selectedPatternIndex);
			} else if (col == 2) {
				int dataCount = patternDataSet.getPatternDataRecordCount(selectedPatternIndex);
				return NumberUtil.getNumberStr(dataCount);
			} else if (col == 3) {
				int allDataCount = patternDataSet.getDataRecordCount();
				int targetDataCount = patternDataSet.getPatternDataRecordCount(selectedPatternIndex);
				if (allDataCount == 0) {
					return "0.00 %";
				} else {
					return String.format("%.2f", ((double)targetDataCount / (double)allDataCount) * 100.0D) + " %";
				}
			} else {
				return "";
			}
		
		} // end of method
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class RawDataTableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		private String[] columnNames;
		private int[]    items;
		
		private PatternDataRecord[] records;
		
		public RawDataTableModel() {
			
			// set dataset
			this.records = patternDataSet.getAllSortedPatternDataRecord();
			this.items   = patternDataSet.getItems();
			
			// set column names
			this.columnNames = new String[items.length + 3];
			columnNames[0] = "Time";
			columnNames[1] = "Id";
			columnNames[2] = "Name";
			for (int i=0 ; i<items.length ; i++) {
				this.columnNames[i+3] = ItemCodeDefine.getItemName(items[i]);
			}
		}
		
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			if (records == null) {
				return 0;
			}
			return records.length;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return records[row].getTimestr();
			} else if (col == 1) {
				return records[row].getPatternIndex();
			} else if (col == 2) {
				return patternDataSet.getPatternName(records[row].getPatternIndex());
			} else {
				return NumberUtil.getNumberStr(records[row].getValue(col-3));
			}
		
		} // end of method
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 1) {
				return Integer.class;
			} else {
				return String.class;
			}
			
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	
	
} // end of class
