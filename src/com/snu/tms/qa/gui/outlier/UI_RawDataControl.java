/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier;


import com.snu.tms.qa.gui.outlier.dialog.DialogRawDataCsvExporter;
import com.snu.tms.qa.gui.outlier.dialog.DialogRawDataGraph;
import com.snu.tms.qa.gui.outlier.dialog.DialogRawDataViewer;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.DatasetManager;

import java.awt.event.*;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


/**
 * Left Panel for stack selection
 * 
 */
public class UI_RawDataControl
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * setRawDataControl
	 */
	public static void setControl(JPanel rootBasePanel) {
		
		// set rootFrame
		final JFrame  rootFrame = (JFrame)SwingUtilities.getWindowAncestor(rootBasePanel);

		// Set Base Panel
		JPanel basePanel = new JPanel(null);
		basePanel.setBounds(10, 30, 620, 70);
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder("raw data analysis"),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		// Set ControlButton
		JButton viewDataButton = new JButton("show raw data");
		viewDataButton.setBounds(30, 25, 170, 30);
		viewDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogRawDataViewer(
						rootFrame, 
						stackInfoStr,
						dataSet);
			}
		});
		basePanel.add(viewDataButton);

		JButton exportDataButton = new JButton("export as a csv format");
		exportDataButton.setBounds(230, 25, 170, 30);
		exportDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("%s_%d",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber());
				DialogRawDataCsvExporter.exportRawDataAsCsvFormat(rootFrame, stackInfoStr, dataSet);
			}
		});
		basePanel.add(exportDataButton);
			
		JButton showGraphButton = new JButton("show graph");
		showGraphButton.setBounds(430, 25, 170, 30);
		showGraphButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TmsDataset dataSet = DatasetManager.getInstance().getTmsDataset();
				String stackInfoStr = String.format("fact_code(%s), fact_name(%s), stck_code(%d), stck_class(%s)",
						dataSet.getTmsDataHeader().getFactoryInfo().getCode(),
						dataSet.getTmsDataHeader().getFactoryInfo().getFullName(),
						dataSet.getTmsDataHeader().getStackInfo().getStackNumber(),
						dataSet.getTmsDataHeader().getStackInfo().getFacilityClass2());
				new DialogRawDataGraph(
						rootFrame, 
						stackInfoStr,
						dataSet);
			}
		});
		basePanel.add(showGraphButton);
		
		// add rootBasePanel
		rootBasePanel.add(basePanel);
		
	} // end of method

} // end of class
