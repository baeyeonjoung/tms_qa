/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Singleton Pattern for action invoker
 * 
 */
public class ActionInvoker 
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(ActionInvoker.class);
	
	private static ActionInvoker instance = new ActionInvoker();
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private Vector<ActionListener> actionListeners = 
			new Vector<ActionListener>();
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	private ActionInvoker() {
		
	} // end of method
	
	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * get Instance
	 */
	public static ActionInvoker getinstnace() {
		return instance;
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Add Action Listener
	 */
	public void addActionListener(ActionListener actionListener) {
		
		if (actionListener != null) {
			this.actionListeners.add(actionListener);
		}
		
	} // end of method
	
	/**
	 * Invoke ActionListener
	 */
	public void invokeActionListener(ActionEvent e) {
		
		ActionListener element;
		for (Iterator<ActionListener> ee = this.actionListeners.iterator(); ee.hasNext();) {
		
			// Get Listener
			try {
				element = ee.next();
				if (element == null) {
					ee.remove();
				}
				element.actionPerformed(e);
			} catch (Exception eee) {
				logger.error(eee);
			}
		}
		
	} // end of method

} // end of class
