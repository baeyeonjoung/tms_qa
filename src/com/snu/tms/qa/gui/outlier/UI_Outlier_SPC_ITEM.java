/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui.outlier;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.event.ChartProgressEvent;
import org.jfree.chart.event.ChartProgressListener;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import com.snu.tms.qa.analysis.outlier.OutlierDetectionUsingSpc;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.DetectionRuleset;
import com.snu.tms.qa.model.ruleset.RulesetSpc;
import com.snu.tms.qa.util.NumberUtil;


/**
 * Panel for outlier(SPC) detection
 * 
 */
public class UI_Outlier_SPC_ITEM extends JPanel
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static final long serialVersionUID = 1L;
	
	private final static String[] PercentConfidenceLevels = {"50%", "60%", "75%", "90%", "95%", "99%", "99.9%"};
	private final static double[] PercentConfidenceValues = {0.50, 0.60, 0.75, 0.90D, 0.95D, 0.99D, 0.999D};
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private DetectionPatternDataset     patternData;
	private RulesetSpc                  ruleset;
	
	private JTextField                  lowerThresholdTextField;
	private JTextField                  upperThresholdTextField;
	
	private JComboBox<String>           confidenceLevelCombo;
	
	private JTable                      outlierCheckTable;
	
	private JPanel                      chartBasePanel;
	
	private Hashtable<Long, Integer>    timeHash;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Show analysis result for outlier detection using SPC(Statictic Process Control)
	 */
	public UI_Outlier_SPC_ITEM(
			DetectionPatternDataset patternData, int  item) {
		
		// set data
		this.patternData = patternData;
		
		// set ruleset
		DetectionRuleset registeredRuleset = patternData.getRuleset(new RulesetSpc(item));
		if (registeredRuleset == null) {
			this.ruleset = OutlierDetectionUsingSpc.doOutlierDetectionWithPatternData(
					patternData, item, 0.99D);
			this.patternData.addRuleset(this.ruleset);
		} else {
			this.ruleset = (RulesetSpc)registeredRuleset;
		}
		
		// create timeHash
		createTimeHashtable();
		
		// create control panel
		JPanel controlBase = createControlPanel();
		
		// Set upper components
		JPanel chartBase = createOutlierChartPanel();
		
		// Set lower components
		JScrollPane outlierCheckBase = createOutlierTablePanel();
		
		// setLayout
		this.setLayout(new BorderLayout());
		// add controlBase
		this.add(controlBase, BorderLayout.NORTH);
		// create basePanel
		JPanel basePanel = new JPanel(new BorderLayout());
		basePanel.add(chartBase, BorderLayout.NORTH);
		basePanel.add(outlierCheckBase, BorderLayout.CENTER);
		this.add(basePanel, BorderLayout.CENTER);
		
	} // end of method
	
	//==========================================================================
	// implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * create Control Panel
	 */
	public JPanel createControlPanel() {

		// Set Base Panel
		JPanel basePanel = new JPanel(null);
		basePanel.setPreferredSize(new Dimension(400, 50));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder(""),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
				
		// set ruleset enable
		JCheckBox rulesetEnableCheck = new JCheckBox("enable ruleset");
		rulesetEnableCheck.setSelected(this.ruleset.isRulesetEnabled());
		rulesetEnableCheck.setBounds(10, 5, 140, 20);
		rulesetEnableCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean isChecked = ((JCheckBox)e.getSource()).isSelected();
				ruleset.setRulesetEnabled(isChecked);
			}
		});
		basePanel.add(rulesetEnableCheck);

		// Set Threshold button
		JLabel label = new JLabel("confidence level");
		label.setBounds(150, 5, 120, 20);
		basePanel.add(label);
		
		this.confidenceLevelCombo = new JComboBox<String>(PercentConfidenceLevels);
		this.confidenceLevelCombo.setBounds(150, 25, 100, 20);
		int selectedConfidenceLevel = 5;
		for (int i=0 ; i<PercentConfidenceValues.length ; i++) {
			if (this.ruleset.getConfidenceLevel() == PercentConfidenceValues[i]) {
				selectedConfidenceLevel = i;
			}
		}
		this.confidenceLevelCombo.setSelectedIndex(selectedConfidenceLevel);
		basePanel.add(this.confidenceLevelCombo);
		
		label = new JLabel("threshold");
		label.setBounds(260, 5, 100, 20);
		basePanel.add(label);
		
		String valueLowerStr = NumberUtil.getNumberStr(this.ruleset.getThresholds()[1]);
		lowerThresholdTextField = new JTextField(valueLowerStr);
		lowerThresholdTextField.setEditable(false);
		lowerThresholdTextField.setHorizontalAlignment(JTextField.LEFT);
		lowerThresholdTextField.setBounds(260, 25, 80, 20);
		basePanel.add(lowerThresholdTextField);
		
		String valueUpperStr = NumberUtil.getNumberStr(this.ruleset.getThresholds()[0]);
		upperThresholdTextField = new JTextField(valueUpperStr);
		upperThresholdTextField.setEditable(false);
		upperThresholdTextField.setHorizontalAlignment(JTextField.LEFT);
		upperThresholdTextField.setBounds(360, 25, 80, 20);
		basePanel.add(upperThresholdTextField);

		JButton updateButton = new JButton("apply");
		updateButton.setBounds(460, 25, 100, 20);
		updateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateThreshold();
			}
		});
		basePanel.add(updateButton);

		// return
		return basePanel;
		
	} // end of method
	
	/**
	 * create SPC panel
	 */
	public JPanel createOutlierChartPanel() {

		// Base panel
		int[] items = this.ruleset.getItems();
		String name = String.format(
				"Statistic Process Control [ %s ] plot", 
				ItemCodeDefine.getItemName(items[0]));
		JPanel basePanel = new JPanel(new BorderLayout());
		basePanel.setPreferredSize(new Dimension(400, 250));
		basePanel.setBorder(
				BorderFactory.createCompoundBorder(
						BorderFactory.createCompoundBorder(
								BorderFactory.createTitledBorder(name),
								BorderFactory.createEmptyBorder(5,5,5,5)),
								basePanel.getBorder()));
		
		this.chartBasePanel = new JPanel(new BorderLayout());
		this.chartBasePanel.add(createChartPanel(), BorderLayout.CENTER);
		basePanel.add(this.chartBasePanel, BorderLayout.CENTER);
		
		return basePanel;
		
	} // end of method
	
	/**
	 * create outlier check table panel
	 */
	private JScrollPane createOutlierTablePanel() {

		// Create Table
		this.outlierCheckTable = new JTable(new TableModel());
		this.outlierCheckTable.setRowHeight(20);
		//dataTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.outlierCheckTable.getColumnModel().getColumn(0).setPreferredWidth(170);
		
		// Set Renderer
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		this.outlierCheckTable.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		for (int i=0 ; i<patternData.getItems().length ; i++) {
			this.outlierCheckTable.getColumnModel().getColumn(i+2).setCellRenderer( rightRenderer );
		}
				
		// Add basePanel
		JScrollPane basePane = new JScrollPane(this.outlierCheckTable);
		basePane.setPreferredSize(new Dimension(400, 200));
		return basePane;
		
	} // end of method
	
	/**
	 * create chart Panel
	 */
	private JPanel createChartPanel() {
		
		// create XYDataSet
		XYDataset chartDataSet = createDataSet();
		
		// create chart
		final JFreeChart chart = ChartFactory.createTimeSeriesChart(
				"",  // title
				"",             // x-axis label
				"",   // y-axis label
				chartDataSet,            // data
				false,               // create legend?
				true,               // generate tooltips?
				false               // generate URLs?
		);
		
		// set chart properties
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setNoDataMessage("NO DATA");
		plot.setDomainPannable(true);
		plot.setRangePannable(false);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);
		
		// set chart shape properties
		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		plot.setRangeCrosshairVisible(true);
		plot.setRangeCrosshairLockedOnData(true);
		plot.setDomainZeroBaselineVisible(true);
		plot.setRangeZeroBaselineVisible(true);
		XYDotRenderer renderer = new XYDotRenderer();
		renderer.setSeriesPaint(0, Color.blue);
		renderer.setSeriesPaint(1, Color.red);
		renderer.setDotWidth(3);
		renderer.setDotHeight(3);
		plot.setRenderer(renderer);
		
		// set x-axis properties
		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("yyMMdd"));
		
		// Add chart listener
		chart.addProgressListener( new ChartProgressListener() {
			public void chartProgress(ChartProgressEvent event) {
				
				// null check
				if (event.getType() != ChartProgressEvent.DRAWING_FINISHED) {
					return;
				}
				// get mouse click domain axis point
				XYPlot plot = (XYPlot) chart.getPlot();
				double xx = plot.getDomainCrosshairValue();
				// change table selection
				changeTableRowSelection((long)xx);
				
			} // end of method
		});

		// return
		return new ChartPanel(chart);
		
	} // end of method

	/**
	 * Create graph data set
	 */
	private XYDataset createDataSet() {
		
		// create TimeSeries
		String[] seriesName = {"normal data", "outlier data"};
		TimeSeries[] tmsDatasets = new TimeSeries[2];
		for (int i=0 ; i<2 ; i++) {
			tmsDatasets[i] = new TimeSeries(seriesName[i]);
		}
		
		// prepare parameters
		Minute time = null;
		DetectionRecord[] dataRecord = this.patternData.getAllDetectionRecords();
		double value = 0.0D;
		double upperThreshold = this.ruleset.getThresholds()[0];
		double lowerThreshold = this.ruleset.getThresholds()[1];
		
		// Set data
		int[] items = this.ruleset.getItems();
		for (int i=0 ; i<dataRecord.length ; i++) {
			value = this.ruleset.applyRuleset(dataRecord[i].getRawDataCellValue(items));
			time = new Minute(new java.util.Date(dataRecord[i].getTime()));
			if (value > upperThreshold) {
				if (value > (upperThreshold * 3.0D)) {
					value = upperThreshold * 3.0D;
				}
				tmsDatasets[1].add(time, value);
			} else if (value < lowerThreshold) {
				tmsDatasets[1].add(time, value);
			} else {
				tmsDatasets[0].add(time, value);
			}
		}
		
		// Create TimeSeriesCollection
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(tmsDatasets[0]);
		dataset.addSeries(tmsDatasets[1]);
		
		// return
		return dataset;
		
	} // end of method
	
	/**
	 * update threshold
	 */
	public void updateThreshold() {
		
		// Set Selected confidence level
		double confidenceValue = 
				PercentConfidenceValues[this.confidenceLevelCombo.getSelectedIndex()];
		this.ruleset.setConfidenceLevel(confidenceValue);
		
		double mean  = this.ruleset.getMean();
		double sigma = this.ruleset.getSigma();
		double[] thresholds = OutlierDetectionUsingSpc.computeThrehold(mean, sigma, confidenceValue);
		this.ruleset.setThresholds(thresholds);
		this.patternData.recomputeOutlier(this.ruleset);
		
		// set 
		this.lowerThresholdTextField.setText(NumberUtil.getNumberStr(thresholds[1]));
		this.upperThresholdTextField.setText(NumberUtil.getNumberStr(thresholds[0]));
		this.chartBasePanel.removeAll();
		this.chartBasePanel.add(createChartPanel(), BorderLayout.CENTER);
		this.chartBasePanel.updateUI();
		
		// Set TablePanel
		this.outlierCheckTable.updateUI();
		
	} // end of method
	
	/**
	 * Create time hashtable
	 */
	private void createTimeHashtable() {
		
		// create time hash
		this.timeHash = new Hashtable<Long, Integer>();
		
		// set data
		DetectionRecord[] records = patternData.getAllDetectionRecords();
		for (int i=0 ; i<records.length ; i++) {
			this.timeHash.put(records[i].getTime(), i);
		}
		
	} // end of method
	
	/**
	 * Table Selection
	 */
	private void changeTableRowSelection(long selectedTime) {
		
		// get table row index from time hash
		Integer row = this.timeHash.get(selectedTime);
		if (row == null) {
			return;
		}
		
		// table selection
		this.outlierCheckTable.setRowSelectionInterval(row, row);
		JViewport viewport = (JViewport)this.outlierCheckTable.getParent();
		Rectangle rect = this.outlierCheckTable.getCellRect(row, 0, true);
		Rectangle viewRect = viewport.getViewRect();
		
		int y = viewRect.y;
		if (rect.y >= viewRect.y && rect.y <= (viewRect.y + viewRect.height - rect.height)) {
			
		} else if (rect.y < viewRect.y) {
			y = rect.y;
		} else if (rect.y > (viewRect.y + viewRect.height - rect.height)) {
			 y = rect.y - viewRect.height + rect.height;
		}
		viewport.setViewPosition(new Point(0, y));
		
	} // end of method

	//==========================================================================
	// Inner Class
	//==========================================================================
	class TableModel extends AbstractTableModel {
		
		private static final long serialVersionUID = 1L;
		
		private String[] columnNames;
		private int[]    items;
		private int[]    targetItems = ruleset.getItems();
		
		private DetectionRecord[] records;
		
		public TableModel() {
			
			// set dataset
			this.records = patternData.getAllDetectionRecords();
			int[] unsortedItems = patternData.getItems();
			this.items = new int[unsortedItems.length];
			this.items[0] = targetItems[0];
			int index = 1;
			for (int i=0 ; i<unsortedItems.length ; i++) {
				if (unsortedItems[i] == targetItems[0]) {
					continue;
				}
				this.items[index++]   = unsortedItems[i];
			}
			
			// set column names
			this.columnNames = new String[items.length + 2];
			columnNames[0] = "Time";
			columnNames[1] = "Outlier";
			for (int i=0 ; i<items.length ; i++) {
				this.columnNames[i+2] = ItemCodeDefine.getItemName(items[i]);
			}
		}
		
		public String getColumnName(int col) {
			return this.columnNames[col];
		}
		
		public int getRowCount() { 
			if (records == null) {
				return 0;
			}
			return records.length;
		}
		
		public int getColumnCount() { 
			return columnNames.length; 
		}
		
		public Object getValueAt(int row, int col) {
			
			if (col == 0) {
				return records[row].getTimeStr();
			} else if (col == 1) {
				return new Boolean(records[row].getDetectionDataCell(ruleset).isOutlier());
			} else {
				return NumberUtil.getNumberStr(records[row].getRawDataCell(items[col-2]).getData_value());
			}
		
		} // end of method
		
		@SuppressWarnings("unchecked")
		public Class getColumnClass(int col) {
			
			if (col == 1) {
				return Boolean.class;
			} else {
				return String.class;
			}
			
		}
		
		public boolean isCellEditable(int row, int col) {
			return false;
		}
		
		public void setValueAt(Object value, int row, int col) {}
		
	} // end of class
	


} // end of class
