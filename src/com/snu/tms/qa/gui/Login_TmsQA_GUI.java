/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.gui;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.snu.tms.qa.model.LoginUserManager;
import com.snu.tms.qa.soap.QueryUserLoginInfo;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;


/**
 * User login check
 * 
 */
@SuppressWarnings("serial")
public class Login_TmsQA_GUI extends JDialog {

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  JTextField userIdInput;
	private  JPasswordField userPwInput;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Main window
	 */
	public Login_TmsQA_GUI() {
		
		// create JFrame
		super((JFrame)null);
		this.setModal(true);
		// show login window
		showLoginWindow();
		
	} // end of method
	
	//==========================================================================
	// Static Method
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * show login window and check user privileges
	 */
	private void showLoginWindow() 
	{
		// Set Frame
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setTitle("Air-DAP(Data quality Analysis Program) (v1.0)");
		
		// get screen size and compute center of screen
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screenSize.width - 600) / 2;
		int y = (screenSize.height - 350) / 2;
		this.setBounds(x, y, 600, 350);
		
		// set icon
		try {
			this.setIconImage(ImageIO.read(new File("image/smart.png")));
		} catch (IOException exc) {}
		
		// create basePanel
		JPanel basePanel = createBasePanel();
		
		// set window layout
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(basePanel, BorderLayout.CENTER);
		this.setResizable(false);
		this.setVisible(true);
		
	} // end of method
	
	
	/**
	 * set BasePanel
	 */
	private JPanel createBasePanel() {
		
		try {
			// set Image
			final ImageIcon backgroundImage = new ImageIcon(new File("image/login.png").toURI().toURL());
			JPanel basePanel = new JPanel(null) {

				private static final long serialVersionUID = 1L;

				@Override
				protected void paintComponent(Graphics g) {
					super.paintComponent(g);
					g.drawImage(backgroundImage.getImage(), 0, 0, getWidth(), getHeight(), this);
				}
				
				@Override
				public Dimension getPreferredSize() {
					Dimension size = super.getPreferredSize();
					size.width = Math.max(backgroundImage.getIconWidth(), size.width);
					size.height = Math.max(backgroundImage.getIconHeight(), size.height);
					return size;
				}
			};
			
			// set user input components
			JLabel label = new JLabel("USER ID");
			label.setFont(new Font("Consolas", Font.BOLD, 12));
			label.setBounds(300, 220, 100, 25);
			basePanel.add(label);
			
			label = new JLabel("PASSWORD");
			label.setBounds(300, 250, 100, 25);
			basePanel.add(label);
			
			userIdInput = new JTextField();
			userIdInput.setFont(new Font("Consolas", Font.BOLD, 13));
			userIdInput.setBounds(400, 220, 150, 25);
			basePanel.add(userIdInput);
			
			userPwInput = new JPasswordField();
			userPwInput.setFont(new Font("Consolas", Font.BOLD, 13));
			userPwInput.setBounds(400, 250, 150, 25);
			basePanel.add(userPwInput);
			
			JButton checkButton = new JButton("  LOGIN  ");
			checkButton.setFont(new Font("Consolas", Font.BOLD, 13));
			checkButton.setBounds(400, 280, 120, 30);
			basePanel.add(checkButton);
			
			checkButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					checkUserIdentify();
				}
			});
			
			// add key listener
			basePanel.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent event) {
	                int keyCode=event.getKeyCode();
	                if (keyCode == KeyEvent.VK_ENTER) {
	                	checkUserIdentify();
	                }
	            }
			});
			
			userIdInput.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent event) {
	                int keyCode=event.getKeyCode();
	                if (keyCode == KeyEvent.VK_ENTER) {
	                	checkUserIdentify();
	                }
	            }
			});
			
			userPwInput.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent event) {
	                int keyCode=event.getKeyCode();
	                if (keyCode == KeyEvent.VK_ENTER) {
	                	checkUserIdentify();
	                }
	            }
			});
			
			checkButton.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent event) {
	                int keyCode=event.getKeyCode();
	                if (keyCode == KeyEvent.VK_ENTER) {
	                	checkUserIdentify();
	                }
	            }
			});
			
			return basePanel;
			
		} catch (Exception e) {
			e.printStackTrace();
			return new JPanel();
		}
		
	} // end of method
	
	/**
	 * check user id and password
	 */
	public void checkUserIdentify() {
		
		// check all component is fulfillment
		if (userIdInput.getText().trim().equals("")) {
			userIdInput.requestFocus();
		} else if (new String(userPwInput.getPassword()).trim().equals("")) {
			userPwInput.requestFocus();
		} else {
			
			String userId = userIdInput.getText().trim();
			String userPw = new String(userPwInput.getPassword()).trim();
			
			// check via SOAP
			int identification = checkUserAuthorization(userId, userPw);
			//System.out.println("User Login Authorization check : [ " + identification + " ]");;
			
			// in case of login success
			if (identification == 1) {
				LoginUserManager loginUserManager = LoginUserManager.getInstance();
				loginUserManager.setUserId(userId);
				loginUserManager.setLoginTime(System.currentTimeMillis());
				doProcessAfterLogin();
			}
			
			// in case of user_id mismatch
			else if (identification == 2) {
				JOptionPane.showMessageDialog(
						SwingUtilities.getWindowAncestor(this), 
						"UserId is not valid.");
				userIdInput.selectAll();
				userIdInput.requestFocus();
			}
			
			// in case of user_pw mismatch
			else if (identification == 3) {
				JOptionPane.showMessageDialog(
						SwingUtilities.getWindowAncestor(this), 
						"Password is not valid.");
				userPwInput.selectAll();
				userPwInput.requestFocus();
			}
			
			// in case of user authority is not valid
			else if (identification == 4) {
				JOptionPane.showMessageDialog(
						SwingUtilities.getWindowAncestor(this), 
						"Your ID is not manager authority.");
				userPwInput.removeAll();
				userIdInput.selectAll();
				userIdInput.requestFocus();
			}
			
			else {
				JOptionPane.showMessageDialog(
						SwingUtilities.getWindowAncestor(this), 
						"Can't connect server. Please check network connection.");
				userPwInput.removeAll();
				userIdInput.selectAll();
				userIdInput.requestFocus();
			}
			
		}
		
	} // end of method
	
	/**
	 * check user authentication and authorization
	 */
	private int checkUserAuthorization(String userId, String userPw) {
		return QueryUserLoginInfo.queryUserInfo(userId, userPw);
	} // end of method
	
	/**
	 * close this window and do process after user authorization
	 */
	private void doProcessAfterLogin() {
		
		// close this window
		this.dispose();
		
	} // end of method

} // end of class
