/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

/**
 * 배출시설 정보
 * 
 */
public class PreventionInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  FactoryInfo  factoryInfo    = null;
	private  StackInfo    stackInfo      = null;
	
	private  String       exhstCode      = "";
	private  String       prvnName       = "";
	private  String       prvnCapacity   = "";
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for StackInfo
	 */
	public PreventionInfo() {
		
	} // end of constructor
	
	public PreventionInfo(
			FactoryInfo  factoryInfo,
			StackInfo    stackInfo,
			String       exhstCode,
			String       prvnName,
			String       prvnCapacity) {

		this.factoryInfo   = factoryInfo;
		this.stackInfo     = stackInfo;
		this.exhstCode     = exhstCode;
		this.prvnName      = prvnName;
		this.prvnCapacity  = prvnCapacity;
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * toString()
	 */
	public String toString() {
		
		return String.format(
				"FACT_CODE[%s],  STACK_CODE[%d], EXHAUST_CODE[%s], PRVN_NAME[%s], PRVN_CAPACITY[%s]",
				factoryInfo.getCode(),
				stackInfo.getStackNumber(),
				exhstCode, prvnName, prvnCapacity);
		
	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the factoryInfo
	 */
	public FactoryInfo getFactoryInfo() {
		return factoryInfo;
	}

	/**
	 * @param factoryInfo the factoryInfo to set
	 */
	public void setFactoryInfo(FactoryInfo factoryInfo) {
		this.factoryInfo = factoryInfo;
	}

	/**
	 * @return the stackInfo
	 */
	public StackInfo getStackInfo() {
		return stackInfo;
	}

	/**
	 * @param stackInfo the stackInfo to set
	 */
	public void setStackInfo(StackInfo stackInfo) {
		this.stackInfo = stackInfo;
	}

	/**
	 * @return the exhstCode
	 */
	public String getExhstCode() {
		return exhstCode;
	}

	/**
	 * @param exhstCode the exhstCode to set
	 */
	public void setExhstCode(String exhstCode) {
		this.exhstCode = exhstCode;
	}

	/**
	 * @return the prvnName
	 */
	public String getPrvnName() {
		return prvnName;
	}

	/**
	 * @param prvnName the prvnName to set
	 */
	public void setPrvnName(String prvnName) {
		this.prvnName = prvnName;
	}

	/**
	 * @return the prvnCapacity
	 */
	public String getPrvnCapacity() {
		return prvnCapacity;
	}

	/**
	 * @param prvnCapacity the prvnCapacity to set
	 */
	public void setPrvnCapacity(String prvnCapacity) {
		this.prvnCapacity = prvnCapacity;
	}


} // end of class
