/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

/**
 * Data Element for TMS Data
 * 
 */
public class ItemCodeDefine {

	//==========================================================================
	// Static Fields
	//==========================================================================
	public  final  static  int  UNDEFINED = 0;
	public  final  static  int  TSP       = 1;
	public  final  static  int  SOX       = 2;
	public  final  static  int  NOX       = 3;
	public  final  static  int  HCL       = 4;
	public  final  static  int  HF        = 5;
	public  final  static  int  NH3       = 6;
	public  final  static  int  CO        = 7;
	public  final  static  int  CO2       = 20;
	public  final  static  int  FL1       = 21;
	public  final  static  int  FL2       = 22;
	public  final  static  int  O2        = 23;
	public  final  static  int  TMP       = 24;
	public  final  static  int  TM1       = 25;
	public  final  static  int  TM2       = 26;
	public  final  static  int  TM3       = 27;
	public  final  static  int  IRS       = 31;
	
	public  final  static  int[] ITEMS = {
			ItemCodeDefine.TSP,
			ItemCodeDefine.SOX,
			ItemCodeDefine.NOX,
			ItemCodeDefine.HCL,
			ItemCodeDefine.HF,
			ItemCodeDefine.NH3,
			ItemCodeDefine.CO,
			ItemCodeDefine.CO2,
			ItemCodeDefine.FL1,
			ItemCodeDefine.FL2,
			ItemCodeDefine.O2,
			ItemCodeDefine.TMP,
			ItemCodeDefine.TM1,
			ItemCodeDefine.TM2,
			ItemCodeDefine.TM3,
			ItemCodeDefine.IRS };
	
	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Get all Items
	 */
	public static int[] getAllItems() {
		return ITEMS;
	} // end of method
	
	/**
	 * get item index
	 */
	public static int getItemCode(String itemName) {
		
		if (itemName.toLowerCase().equals("tsp")) {
			return ItemCodeDefine.TSP;
		} else if (itemName.toLowerCase().equals("sox")) {
			return ItemCodeDefine.SOX;
		} else if (itemName.toLowerCase().equals("nox")) {
			return ItemCodeDefine.NOX;
		} else if (itemName.toLowerCase().equals("hcl")) {
			return ItemCodeDefine.HCL;
		} else if (itemName.toLowerCase().equals("hf")) {
			return ItemCodeDefine.HF;
		} else if (itemName.toLowerCase().equals("nh3")) {
			return ItemCodeDefine.NH3;
		} else if (itemName.toLowerCase().equals("co")) {
			return ItemCodeDefine.CO;
		} else if (itemName.toLowerCase().equals("co2")) {
			return ItemCodeDefine.CO2;
		} else if (itemName.toLowerCase().equals("fl1")) {
			return ItemCodeDefine.FL1;
		} else if (itemName.toLowerCase().equals("fl2")) {
			return ItemCodeDefine.FL2;
		} else if (itemName.toLowerCase().equals("o2")) {
			return ItemCodeDefine.O2;
		} else if (itemName.toLowerCase().equals("tmp")) {
			return ItemCodeDefine.TMP;
		} else if (itemName.toLowerCase().equals("tm1")) {
			return ItemCodeDefine.TM1;
		} else if (itemName.toLowerCase().equals("tm2")) {
			return ItemCodeDefine.TM2;
		} else if (itemName.toLowerCase().equals("tm3")) {
			return ItemCodeDefine.TM3;
		} else if (itemName.toLowerCase().equals("irs")) {
			return ItemCodeDefine.IRS;
		} else {
			return ItemCodeDefine.UNDEFINED;
		}
		
	} // end of method
	
	/**
	 * get item name
	 */
	public static String getItemName(int itemIndex) {

		if (itemIndex == ItemCodeDefine.TSP) {
			return "TSP";
		} else if (itemIndex == ItemCodeDefine.SOX) {
			return "SOX";
		} else if (itemIndex == ItemCodeDefine.NOX) {
			return "NOX";
		} else if (itemIndex == ItemCodeDefine.HCL) {
			return "HCL";
		} else if (itemIndex == ItemCodeDefine.HF) {
			return "HF";
		} else if (itemIndex == ItemCodeDefine.NH3) {
			return "NH3";
		} else if (itemIndex == ItemCodeDefine.CO) {
			return "CO";
		} else if (itemIndex == ItemCodeDefine.CO2) {
			return "CO2";
		} else if (itemIndex == ItemCodeDefine.FL1) {
			return "FL1";
		} else if (itemIndex == ItemCodeDefine.FL2) {
			return "FL2";
		} else if (itemIndex == ItemCodeDefine.O2) {
			return "O2";
		} else if (itemIndex == ItemCodeDefine.TMP) {
			return "TMP";
		} else if (itemIndex == ItemCodeDefine.TM1) {
			return "TM1";
		} else if (itemIndex == ItemCodeDefine.TM2) {
			return "TM2";
		} else if (itemIndex == ItemCodeDefine.TM3) {
			return "TM3";
		} else if (itemIndex == ItemCodeDefine.IRS) {
			return "IRS";
		} else {
			return "UNDEFINED";
		}

	} // end of method
	
	/**
	 * get item hangule name
	 */
	public static String getItemHName(int itemIndex) {

		if (itemIndex == ItemCodeDefine.TSP) {
			return "먼지";
		} else if (itemIndex == ItemCodeDefine.SOX) {
			return "황산화물";
		} else if (itemIndex == ItemCodeDefine.NOX) {
			return "질소산화물";
		} else if (itemIndex == ItemCodeDefine.HCL) {
			return "염화수소";
		} else if (itemIndex == ItemCodeDefine.HF) {
			return "불화수소";
		} else if (itemIndex == ItemCodeDefine.NH3) {
			return "암모니아";
		} else if (itemIndex == ItemCodeDefine.CO) {
			return "일산화탄소";
		} else if (itemIndex == ItemCodeDefine.CO2) {
			return "이산화탄소";
		} else if (itemIndex == ItemCodeDefine.FL1) {
			return "기체유량1";
		} else if (itemIndex == ItemCodeDefine.FL2) {
			return "기체유량2";
		} else if (itemIndex == ItemCodeDefine.O2) {
			return "산소";
		} else if (itemIndex == ItemCodeDefine.TMP) {
			return "온도";
		} else if (itemIndex == ItemCodeDefine.TM1) {
			return "소각노내온도1";
		} else if (itemIndex == ItemCodeDefine.TM2) {
			return "소각노내온도2";
		} else if (itemIndex == ItemCodeDefine.TM3) {
			return "소각노내온도3";
		} else if (itemIndex == ItemCodeDefine.IRS) {
			return "열선감지";
		} else {
			return "미지정물질";
		} 		

	} // end of method
	
	/**
	 * get item hangule name
	 */
	public static String getItemUnit(int itemIndex) {

		if (itemIndex == ItemCodeDefine.TSP) {
			return "mg/m³";
		} else if (itemIndex == ItemCodeDefine.SOX) {
			return "ppm";
		} else if (itemIndex == ItemCodeDefine.NOX) {
			return "ppm";
		} else if (itemIndex == ItemCodeDefine.HCL) {
			return "ppm";
		} else if (itemIndex == ItemCodeDefine.HF) {
			return "ppm";
		} else if (itemIndex == ItemCodeDefine.NH3) {
			return "ppm";
		} else if (itemIndex == ItemCodeDefine.CO) {
			return "ppm";
		} else if (itemIndex == ItemCodeDefine.CO2) {
			return "ppm";
		} else if (itemIndex == ItemCodeDefine.FL1) {
			return "m³";
		} else if (itemIndex == ItemCodeDefine.FL2) {
			return "m³";
		} else if (itemIndex == ItemCodeDefine.O2) {
			return "%";
		} else if (itemIndex == ItemCodeDefine.TMP) {
			return "℃";
		} else if (itemIndex == ItemCodeDefine.TM1) {
			return "℃";
		} else if (itemIndex == ItemCodeDefine.TM2) {
			return "℃";
		} else if (itemIndex == ItemCodeDefine.TM3) {
			return "℃";
		} else if (itemIndex == ItemCodeDefine.IRS) {
			return "";
		} else {
			return "";
		} 		

	} // end of method
	
	/**
	 * get item weighting factor
	 */
	public static double getItemWeighting(int itemIndex) {
		
		if (itemIndex == ItemCodeDefine.TSP) {
			return 1;
		} else if (itemIndex == ItemCodeDefine.SOX) {
			return 1;
		} else if (itemIndex == ItemCodeDefine.NOX) {
			return 1;
		} else if (itemIndex == ItemCodeDefine.HCL) {
			return 1;
		} else if (itemIndex == ItemCodeDefine.HF) {
			return 1;
		} else if (itemIndex == ItemCodeDefine.NH3) {
			return 1;
		} else if (itemIndex == ItemCodeDefine.CO) {
			return 1;
		} else if (itemIndex == ItemCodeDefine.CO2) {
			return 1;
		} else if (itemIndex == ItemCodeDefine.FL1) {
			return 2;
		} else if (itemIndex == ItemCodeDefine.FL2) {
			return 2;
		} else if (itemIndex == ItemCodeDefine.O2) {
			return 2;
		} else if (itemIndex == ItemCodeDefine.TMP) {
			return 1;
		} else if (itemIndex == ItemCodeDefine.TM1) {
			return 1;
		} else if (itemIndex == ItemCodeDefine.TM2) {
			return 1;
		} else if (itemIndex == ItemCodeDefine.TM3) {
			return 1;
		} else if (itemIndex == ItemCodeDefine.IRS) {
			return 0;
		} else {
			return 0;
		}
		
	} // end of method

} // end of class
