/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

/**
 * Data manager for Fact_code, Stck_code, CorrectionInfo (using Singleton Pattern)
 * 
 */
public class LoginUserManager {

	//==========================================================================
	// Static Fields
	//==========================================================================
	// for singleton pattern
	private static LoginUserManager instance = null;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String  userId = "";
	private  long    loginTime = 0L;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	private LoginUserManager() {
		
	} // end of constructor

	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Singleton pattern
	 */
	public static LoginUserManager getInstance() {
		
		if (instance == null) {
			instance = new LoginUserManager();
			return instance;
		} 
		
		// return
		return instance;
		
	}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	//==========================================================================
	// Methods
	//==========================================================================

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public long getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(long loginTime) {
		this.loginTime = loginTime;
	}



	
} // end of class
