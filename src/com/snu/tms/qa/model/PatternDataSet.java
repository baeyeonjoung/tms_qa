/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.snu.tms.qa.model.ruleset.PatternDataRecord;

/**
 * Pattern Data Set
 * 
 */
public class PatternDataSet {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private int[]      items;
	private int        pcCount = 0;
	private double[][] eigenVector;
	private double[]   eigenValues;
	private double[]   neigenValues;
	
	private  Hashtable<Long, PatternDataRecord>            timeHash;
	private  Hashtable<Integer, Vector<PatternDataRecord>> patternHash;
	private  Hashtable<Integer, String>                    patternNameHash;
	
	private  double[][] confidence95Intervals;
	
	private int patternIndex = 100;
	
	private long startTime = Long.MAX_VALUE;
	private long endTime = Long.MIN_VALUE;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public PatternDataSet() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// Append information
		Vector<PatternDataRecord> patternRecords = null;
		int patternIndex;
		String patternName;
		for (Enumeration<Vector<PatternDataRecord>> e = this.patternHash.elements(); e.hasMoreElements();) {
			patternRecords = e.nextElement();
			PatternDataRecord[] records = patternRecords.toArray(new PatternDataRecord[patternRecords.size()]);
			if (records.length <= 0) {
				continue;
			}
			Arrays.sort(records);
			patternIndex = records[0].getPatternIndex();
			patternName = this.patternNameHash.get(patternIndex);
			
			infoStrBuffer.append("--------------------------------------------------\n");
			infoStrBuffer.append(String.format(" Pattern [%d] : Size(%d), Name(%s)\n", 
					patternIndex, records.length, patternName));
			infoStrBuffer.append("--------------------------------------------------\n");
		}
		
		// return
		return infoStrBuffer.toString();

	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Initialize data records
	 */
	public void initDataRecord(TmsDataRecord[] dataRecords, int[] items) {
		
		// memory allocate
		this.timeHash = new Hashtable<Long, PatternDataRecord>();
		this.patternNameHash = new Hashtable<Integer, String>();
		this.patternHash = new Hashtable<Integer, Vector<PatternDataRecord>>();
		
		// Set items
		this.items = Arrays.copyOf(items, items.length);
		this.pcCount = items.length;
		
		// Set Confidence Interval
		this.confidence95Intervals = new double[items.length][2];
		
		// Add TimeHash
		long time;
		PatternDataRecord patternRecord;
		double[] values;
		for (TmsDataRecord tmsDataRecord : dataRecords) {
			time = tmsDataRecord.getTime();
			values = new double[items.length];
			for (int i=0 ; i<items.length ; i++) {
				values[i] = tmsDataRecord.getDataCell(items[i]).getData_value();
			}
			patternRecord = new PatternDataRecord(time, items, values, pcCount);
			this.timeHash.put(time, patternRecord);

			// Set Time
			if (time < this.startTime) {
				this.startTime = time;
			}
			if (time > this.endTime) {
				this.endTime = time;
			}
			
		}
		
	} // end of method
	
	/**
	 * get PatternData Histogram
	 */
	public PatternDataHistogram[] getPatternHistogram(int patternIndex) {

		// create PatternDataHistogram
		int itemCount = this.items.length;
		PatternDataHistogram[] patternDataHistograms = new PatternDataHistogram[itemCount];
		
		// set patternDataHistogram
		PatternDataRecord[] records = getSortedPatternDataRecord(patternIndex);
		for (int i=0 ; i<this.items.length ; i++) {
			
			// set values
			double[] values = new double[records.length];
			for (int j=0 ; j<values.length ; j++) {
				values[j] = records[j].getItemValue(this.items[i]);
			}
			
			// create PatternDataHistogram
			patternDataHistograms[i] = new PatternDataHistogram(
					patternIndex, this.items[i], this.confidence95Intervals[i], values);
				
		} // end of for-loop
		
		// return 
		return patternDataHistograms;

	} // end of method
	
	
	/**
	 * Set PcaResults
	 */
	public void setPcaResults(double[][] eigenVector, double[] eigenValues) {
		
		int pcCount = eigenValues.length;
		this.eigenVector = new double[pcCount][pcCount];
		this.eigenValues = new double[pcCount];
		this.neigenValues = new double[pcCount];
		
		double eigenValueSum = 0.0D;
		for (int i=0 ; i<pcCount ; i++) {
			this.eigenValues[i] = eigenValues[i];
			eigenValueSum += eigenValues[i];
			for (int j=0 ; j<pcCount ; j++) {
				this.eigenVector[i][j] = eigenVector[i][j];
			}
		}
		
		for (int i=0 ; i<pcCount ; i++) {
			this.neigenValues[i] = eigenValues[i] / eigenValueSum;
		}
		
	} // end of method
	
	
	/**
	 * Set PCA result data
	 */
	public void setPcaResultPoint(long time, double[] pcaResultPoint) {
		
		// search target Record
		PatternDataRecord targetRecord = this.timeHash.get(time);
		if (targetRecord == null) {
			return;
		}
		
		// setPcaResults
		if (pcaResultPoint == null || pcaResultPoint.length != this.eigenValues.length) {
			return;
		}
		if (targetRecord.getPcCount() != pcaResultPoint.length) {
			return;
		}
		targetRecord.setValue(pcaResultPoint, this.eigenValues);
		
	} // end of method
	
	
	/**
	 * Clear pattern data
	 */
	public void clearPatternData() {
		
		// Clear pattern hash
		this.patternNameHash.clear();
		this.patternHash.clear();
		
		// remove all PatternDataRecord
		PatternDataRecord patternRecord;
		for (Enumeration<PatternDataRecord> e = this.timeHash.elements(); e.hasMoreElements();) {
			patternRecord = e.nextElement();
			patternRecord.setPatternIndex(-1);
		}
		
	} // end of method
	
	
	/**
	 * Update pattern data
	 */
	public void updatePatternData(int oldPatternIndex, int newPatternIndex) {
		
		// move oldPatternIndex to newPatternIndex
		Vector<PatternDataRecord> oldPatternRecords = this.patternHash.get(oldPatternIndex);
		Vector<PatternDataRecord> newPatternRecords = this.patternHash.get(newPatternIndex);
		PatternDataRecord targetPatternDataRecord;
		for (Enumeration<PatternDataRecord> e = oldPatternRecords.elements(); e.hasMoreElements();) {
			targetPatternDataRecord = e.nextElement();
			targetPatternDataRecord.setPatternIndex(newPatternIndex);
			newPatternRecords.add(targetPatternDataRecord);
		}
		
		// remove oldPantternIndex records
		this.patternHash.remove(oldPatternIndex);

		// remove from PatternNameHash
		this.patternNameHash.remove(oldPatternIndex);
		
	} // end of method
	
	
	/**
	 * Update pattern data
	 */
	public void updatePatternData(PatternDataRecord record, int newPatternIndex) {
		
		// remove from oldPatternIndexHash
		this.patternHash.get(record.getPatternIndex()).remove(record);
		
		// Set new PatternIndex
		record.setPatternIndex(newPatternIndex);
		
		// Add to new PatternIndexHash
		this.patternHash.get(record.getPatternIndex()).add(record);
		
	} // end of method
	
	
	/**
	 * Set Pattern
	 */
	public void setPattern(long[] patternTimes) {
		
		// allocate PatternIndex
		int patternId = this.patternIndex++;
		
		// Set Pattern Name Hash
		this.patternNameHash.put(patternId, "");
		
		// Set PatternHash
		Vector<PatternDataRecord> targetPatternVector = new Vector<PatternDataRecord>(patternTimes.length);

		// Set PatternId
		PatternDataRecord targetRecord;
		for (long time : patternTimes) {
			targetRecord = this.timeHash.get(time);
			if (targetRecord == null) {
				continue;
			}
			targetRecord.setPatternIndex(patternId);
			targetPatternVector.add(targetRecord);
		}
		
		// Set PatternHash
		this.patternHash.put(patternId, targetPatternVector);
		
	} // end of method
	
	/**
	 * Set Pattern
	 */
	public void setPattern(int patternIndex, long[] patternTimes) {
		
		// Set Pattern Name Hash
		this.patternNameHash.put(patternIndex, "");
		
		// Set PatternHash
		Vector<PatternDataRecord> targetPatternVector = new Vector<PatternDataRecord>(patternTimes.length);

		// Set PatternId
		PatternDataRecord targetRecord;
		for (long time : patternTimes) {
			targetRecord = this.timeHash.get(time);
			if (targetRecord == null) {
				continue;
			}
			targetRecord.setPatternIndex(patternIndex);
			targetPatternVector.add(targetRecord);
		}
		
		// Set PatternHash
		this.patternHash.put(patternIndex, targetPatternVector);
		
	} // end of method
	
	/**
	 * remove pattern
	 */
	public void removePattern(int patternIndex) {
		
		patternHash.remove(patternIndex);
		patternNameHash.remove(patternIndex);
		
	} // end of method
	
	
	/**
	 * Set Pattern Name
	 */
	public void setPatternName(int patternIndex, String patternName) {
		
		if (patternName == null || patternName.equals("")) {
			this.patternNameHash.remove(patternIndex);
		} else {
			this.patternNameHash.put(patternIndex, patternName);
		}
		
	} // end of method
	
	/**
	 * Re-Assign pattern index using Distance of centroid
	 */
	public void reAssignPatternIndex() {
		
		// Get patternIndex Array
		int[] patternIndices = getPatternIndexArray();
		PatternDistance[] patternDist = new PatternDistance[patternIndices.length];
		
		// Get average Pattern Distance
		double distanceSum, count;
		for (int i=0 ; i<patternIndices.length ; i++) {
			
			Vector<PatternDataRecord> recordVector = this.patternHash.get(patternIndices[i]);
			count = (double)recordVector.size();
			distanceSum = 0.0D;
			for (PatternDataRecord record : recordVector) {
				distanceSum += record.getDistanceOfCentroid();
			}
			distanceSum = distanceSum / count;
			patternDist[i] = new PatternDistance(patternIndices[i], distanceSum);
		}
		
		// Sort with pattern distance
		Arrays.sort(patternDist);
		
		// re-assign pattern Index from 1 .. N
		int oldPatternIndex = 0;
		int newPatternIndex = 0;
		for (int i=0 ; i<patternDist.length ; i++) {
			
			oldPatternIndex = patternDist[i].patternIndex;
			newPatternIndex++;
			
			// Get all patternRecord
			Vector<PatternDataRecord> records = this.patternHash.get(oldPatternIndex);
			// change patternIndex
			for (PatternDataRecord record : records) {
				record.setPatternIndex(newPatternIndex);
			}
			// set PatternIndexHash
			this.patternHash.remove(oldPatternIndex);
			this.patternHash.put(newPatternIndex, records);
			// set PatternNameHash
			String patternName = this.patternNameHash.remove(oldPatternIndex);
			this.patternNameHash.put(newPatternIndex, patternName);
			
		}
		
	} // end of method
	
	
	/**
	 * Get Items
	 */
	public int[] getItems() {
		return this.items;
	} // end of method
	
	
	/**
	 * Get PcCount
	 */
	public int getPcCount() {
		return this.pcCount;
	} // end of method
	
	
	/**
	 * Get all sorted PatternDataRecord
	 */
	public PatternDataRecord[] getAllSortedPatternDataRecord() {
		
		// Get all patternDataRecord
		PatternDataRecord[] records = new PatternDataRecord[this.timeHash.size()];
		int index = 0;
		for (Enumeration<PatternDataRecord> e = this.timeHash.elements(); e.hasMoreElements();) {
			records[index++] = e.nextElement();
		}
		
		// sort with time
		Arrays.sort(records);
		
		// return
		return records;
		
	} // end of method
	
	
	/**
	 * Get all sorted PatternDataRecord
	 */
	public PatternDataRecord[] getSortedPatternDataRecord(int patternIndex) {
		
		// Get all patternDataRecord
		Vector<PatternDataRecord> recordVector = this.patternHash.get(patternIndex);
		PatternDataRecord[] records = recordVector.toArray(new PatternDataRecord[recordVector.size()]);
		if (records != null && records.length != 0) {
			Arrays.sort(records);
		}
		
		// return
		return records;
		
	} // end of method
	
	
	/**
	 * Get pattern index array
	 */
	public int[] getPatternIndexArray() {
		
		// Get pattern indices
		int[] patternIndices = new int[this.patternHash.size()];
		int index = 0;
		for (Enumeration<Integer> e = this.patternHash.keys(); e.hasMoreElements();) {
			patternIndices[index++] = e.nextElement().intValue();
		}
		
		// sorting (String compare)
		String[] sPatternIndices = new String[patternIndices.length];
		for (int i=0 ; i<sPatternIndices.length ; i++) {
			sPatternIndices[i] = patternIndices[i] + "";
		}
		Arrays.sort(sPatternIndices);
		
		for (int i=0 ; i<sPatternIndices.length ; i++) {
			patternIndices[i] = Integer.parseInt(sPatternIndices[i]);
		}
		
		// return
		return patternIndices;
		
	} // end of method
	
	
	/**
	 * Get Pattern Name
	 */
	public String getPatternName(int patternIndex) {
		
		String patternName = this.patternNameHash.get(patternIndex);
		if (patternName == null) {
			return "";
		} else {
			return patternName;
		}
		
	} // end of method
	
	/**
	 * Set Confidence Interval
	 */
	public void setConfidenceInterval(int itemIndex, double lowerValue, double upperValue) {
		
		this.confidence95Intervals[itemIndex][0] = lowerValue;
		this.confidence95Intervals[itemIndex][1] = upperValue;
	
	} // end of method
	
	/**
	 * Get Confidence Interval
	 */
	public double[][] getConfidenceInterval() {
		return this.confidence95Intervals;
	} // end of method

	/**
	 * Get Confidence Interval
	 */
	public double[] getConfidenceInterval(int itemIndex) {
		
		double[] interval = new double[2];
		interval[0] = this.confidence95Intervals[itemIndex][0];
		interval[1] = this.confidence95Intervals[itemIndex][1];
		return interval;
		
	} // end of method
	
	/**
	 * get eigenValue
	 */
	public double[] getEigenValue() {
		return this.eigenValues;
	} // end of method
	
	/**
	 * get eigenValue
	 */
	public double[] getNeigenValue() {
		return this.neigenValues;
	} // end of method
	
	/**
	 * Get EigenVector
	 */
	public double[][] getEigenVector() {
		return this.eigenVector;
	} // end of method
	
	/**
	 * Get time range
	 */
	public long[] getTimeRange() {
		
		long[] timeRange = new long[2];
		timeRange[0] = this.startTime;
		timeRange[1] = this.endTime;
		return timeRange;
	
	} // end of method
	
	/**
	 * Get all data count
	 */
	public int getDataRecordCount() {
		return this.timeHash.size();
	} // end of method
	
	/**
	 * get pattern data count
	 */
	public int getPatternDataRecordCount(int patternIndex) {
		
		Vector<PatternDataRecord> patternDataRecords = this.patternHash.get(patternIndex);
		if (patternDataRecords == null) {
			return 0;
		}
		return patternDataRecords.size();
		
	} // end of method
	
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class PatternDistance implements Comparable<PatternDistance>
	{
		//==========================================================================
		// Fields
		//==========================================================================
		public int     patternIndex = 0;
		public double  distanceOfCentroid = 0.0D;
		
		//==========================================================================
		// Contructor
		//==========================================================================
		public PatternDistance(int patternIndex, double distanceOfCentroid) {
			this.patternIndex = patternIndex;
			this.distanceOfCentroid = distanceOfCentroid;
		}

		//==========================================================================
		// Implemented Method
		//==========================================================================
		public int compareTo(PatternDistance compare) {
			
			if (this.distanceOfCentroid > compare.distanceOfCentroid) {
				return 1;
			} else if (this.distanceOfCentroid < compare.distanceOfCentroid) {
				return -1;
			} else {
				return 0;
			}
			
		} // end of method
		
		
	} // end of class
	
} // end of class
