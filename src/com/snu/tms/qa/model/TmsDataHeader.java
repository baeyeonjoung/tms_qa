/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

import com.snu.tms.qa.util.DateUtil;

/**
 * Data Element for TMS Data Header
 * 
 */
public class TmsDataHeader {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  FactoryInfo  factoryInfo = null;
	private  StackInfo    stackInfo   = null;
	
	// Date format (yyyy-MM-dd HH:mm:ss)
	private  String      start_time_str = "";
	private  String      end_time_str   = "";
	
	private  int         time_interval = 5;  // time interval by minutes
	
	private  long        start_time = 0L;
	private  long        end_time   = 0L;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public TmsDataHeader() {}
	

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// Append information
		//infoStrBuffer.append( String.format("fact_info    : \n%s\n", (this.factoryInfo == null)?"":this.factoryInfo.toString() ));
		infoStrBuffer.append( String.format("stck_info    : \n%s\n", (this.stackInfo == null)?"":this.stackInfo.toString() ));

		infoStrBuffer.append(String.format("start_time    : %s\n", DateUtil.getDateStr(this.start_time)));
		infoStrBuffer.append(String.format("end_time      : %s",   DateUtil.getDateStr(this.end_time)));
		
		// return
		return infoStrBuffer.toString();

	} // end of method


	//==========================================================================
	// Methods
	//==========================================================================

	/**
	 * @return the factRecord
	 */
	public FactoryInfo getFactoryInfo() {
		return factoryInfo;
	}


	/**
	 * @param factRecord the factRecord to set
	 */
	public void setFactoryInfo(FactoryInfo factoryInfo) {
		this.factoryInfo = factoryInfo;
	}


	/**
	 * @return the stckRecord
	 */
	public StackInfo getStackInfo() {
		return stackInfo;
	}


	/**
	 * @param stckRecord the stckRecord to set
	 */
	public void setStackInfo(StackInfo stackInfo) {
		this.stackInfo = stackInfo;
	}


	/**
	 * @return the start_time_str
	 */
	public String getStart_time_str() {
		return start_time_str;
	}


	/**
	 * @param start_time_str the start_time_str to set
	 */
	public void setStart_time_str(String start_time_str) {
		this.start_time_str = start_time_str;
		this.start_time = DateUtil.getDate(start_time_str);
	}


	/**
	 * @return the end_time_str
	 */
	public String getEnd_time_str() {
		return end_time_str;
	}


	/**
	 * @param end_time_str the end_time_str to set
	 */
	public void setEnd_time_str(String end_time_str) {
		this.end_time_str = end_time_str;
		this.end_time = DateUtil.getDate(end_time_str);
	}


	/**
	 * @return the time_interval
	 */
	public int getTime_interval() {
		return time_interval;
	}


	/**
	 * @param time_interval the time_interval to set
	 */
	public void setTime_interval(int time_interval) {
		this.time_interval = time_interval;
	}


	/**
	 * @return the start_time
	 */
	public long getStart_time() {
		return start_time;
	}


	/**
	 * @param start_time the start_time to set
	 */
	public void setStart_time(long start_time) {
		this.start_time = start_time;
	}


	/**
	 * @return the end_time
	 */
	public long getEnd_time() {
		return end_time;
	}


	/**
	 * @param end_time the end_time to set
	 */
	public void setEnd_time(long end_time) {
		this.end_time = end_time;
	}


} // end of class
