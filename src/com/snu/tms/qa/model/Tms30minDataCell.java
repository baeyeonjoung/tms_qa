/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

/**
 * Data Element for TMS Data
 * 
 */
public class Tms30minDataCell  {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int       data_type = ItemCodeDefine.UNDEFINED;
	private  double    half_valu = -1.0D;
	private  String    half_over = "0";
	private  String    half_stat = "0";
	private  String    half_dlst = "0";
	private  String    stat_code = "0";
	private  double    stat_valu = -1.0D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public Tms30minDataCell() {}
	
	public Tms30minDataCell(int data_type) {
		this.data_type = data_type;
	}

	public Tms30minDataCell(int data_type, double value, String stat_code) {
		this.data_type = data_type;
		this.half_valu = value;
		this.stat_code = stat_code;
	}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {
		return String.format("%10s    %5.3f   %2s   %2s  %2s  %2s  %5.3f", 
				ItemCodeDefine.getItemName(data_type), 
				half_valu, 
				half_over,
				half_stat,
				half_dlst,
				stat_code,
				stat_valu);
	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * set value
	 */
	public void setValue(
			double half_valu,
			String half_over,
			String half_stat,
			String half_dlst,
			String stat_code,
			double stat_valu) {
		
		this.half_valu = half_valu;
		this.half_over = half_over;
		this.half_stat = half_stat;
		this.half_dlst = half_dlst;
		this.stat_code = stat_code;
		this.stat_valu = stat_valu;
		
	} // end of method
	
	/**
	 * @return the data_type
	 */
	public int getData_type() {
		return data_type;
	}

	/**
	 * @param data_type the data_type to set
	 */
	public void setData_type(int data_type) {
		this.data_type = data_type;
	}

	/**
	 * @return the half_valu
	 */
	public double getHalf_valu() {
		return half_valu;
	}

	/**
	 * @param half_valu the half_valu to set
	 */
	public void setHalf_valu(double half_valu) {
		this.half_valu = half_valu;
	}

	/**
	 * @return the half_over
	 */
	public String getHalf_over() {
		return half_over;
	}

	/**
	 * @param half_over the half_over to set
	 */
	public void setHalf_over(String half_over) {
		this.half_over = half_over;
	}

	/**
	 * @return the half_stat
	 */
	public String getHalf_stat() {
		return half_stat;
	}

	/**
	 * @param half_stat the half_stat to set
	 */
	public void setHalf_stat(String half_stat) {
		this.half_stat = half_stat;
	}

	/**
	 * @return the half_dlst
	 */
	public String getHalf_dlst() {
		return half_dlst;
	}

	/**
	 * @param half_dlst the half_dlst to set
	 */
	public void setHalf_dlst(String half_dlst) {
		this.half_dlst = half_dlst;
	}

	/**
	 * @return the stat_code
	 */
	public String getStat_code() {
		return stat_code;
	}

	/**
	 * @param stat_code the stat_code to set
	 */
	public void setStat_code(String stat_code) {
		this.stat_code = stat_code;
	}

	/**
	 * @return the stat_valu
	 */
	public double getStat_valu() {
		return stat_valu;
	}

	/**
	 * @param stat_valu the stat_valu to set
	 */
	public void setStat_valu(double stat_valu) {
		this.stat_valu = stat_valu;
	}



} // end of class
