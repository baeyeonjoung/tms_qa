/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Factory information
 * 
 */
public class StackInfo implements Comparable<StackInfo> {

	//==========================================================================
	// Static Fields
	//==========================================================================
	public  final  static  double  NOT_DEFINE_VALUE = -1.0D;
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  FactoryInfo  factoryInfo      = null;
	
	private  int          stackNumber      = 0;
	private  String       stackName        = "";
	private  double       standardMoisture = NOT_DEFINE_VALUE;
	
	private  String       stackDescr       = "";
	private  String       facilityDescr    = "";
	private  String       facilityClass1   = "";
	private  String       facilityClass2   = "";
	private  String       fuelName         = "";
	
	//private  CorrectionInfo[]  correctionApplied = null;
	private  Hashtable<Integer, CorrectionItemInfo> correctionApplied = 
			new Hashtable<Integer, CorrectionItemInfo>();
	
	private Vector<ExhaustInfo> exhaustInfoArray = 
			new Vector<ExhaustInfo>();
	
	private Vector<PreventionInfo> preventionInfoArray = 
			new Vector<PreventionInfo>();
	
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for StackInfo
	 */
	public StackInfo() {
		
		int[] itemCodes = ItemCodeDefine.getAllItems();
		for (int i=0 ; i<itemCodes.length ; i++) {
			this.correctionApplied.put(
					itemCodes[i], 
					new CorrectionItemInfo(itemCodes[i], -1.0D, -1.0D, 0, 0, 0));
		}
	
	} // end of constructor
	
	public StackInfo(
			int     stackNumber,
			String  stackName,
			double  standardMoisture) {

		this();
		this.stackNumber = stackNumber;
		this.stackName = stackName;
		this.standardMoisture = standardMoisture;
		
	} // end of constructor
	
	public StackInfo(
			FactoryInfo  factoryInfo,
			int     stackNumber,
			String  stackName,
			double  standardMoisture) {

		this();
		this.factoryInfo = factoryInfo;
		this.stackNumber = stackNumber;
		this.stackName = stackName;
		this.standardMoisture = standardMoisture;
		
	} // end of constructor
	
	public StackInfo(StackInfo clone) {
		
		this.factoryInfo = clone.factoryInfo;
		this.stackNumber = clone.stackNumber;
		this.stackName = clone.stackName;
		this.stackDescr = clone.stackDescr;
		this.facilityDescr = clone.facilityDescr;
		this.facilityClass1 = clone.facilityClass1;
		this.facilityClass2 = clone.facilityClass2;
		this.fuelName = clone.fuelName;
		this.standardMoisture = clone.standardMoisture;
		
		this.correctionApplied.clear();
		int[] itemCodes = ItemCodeDefine.getAllItems();
		for (int i=0 ; i<itemCodes.length ; i++) {
			this.correctionApplied.put(
					itemCodes[i], 
					new CorrectionItemInfo(clone.correctionApplied.get(itemCodes[i])));
		}
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implements Comparable
	 */
	public int compareTo(StackInfo compare) {
		
		if (this.stackNumber > compare.stackNumber) {
			return 1;
		} else if (this.stackNumber < compare.stackNumber) {
			return -1;
		} else {
			return 0;
		}
		
	} // end of method
	
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {
		
		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// Append information
		infoStrBuffer.append(String.format("factoryCode     : %s\n",     this.factoryInfo.getCode()));
		infoStrBuffer.append(String.format("factoryName     : %s\n",     this.factoryInfo.getFullName()));
		infoStrBuffer.append(String.format("stackNumber     : %d\n",     this.stackNumber));
		infoStrBuffer.append(String.format("stackName       : %s\n",     this.stackName));
		infoStrBuffer.append(String.format("stackClass1     : %s\n",     this.facilityClass1));
		infoStrBuffer.append(String.format("stackClass2     : %s\n",     this.facilityClass2));
		
		if (this.standardMoisture == NOT_DEFINE_VALUE) {
			infoStrBuffer.append("standardMoisture: NOT_DEFINE\n");
		} else {
			infoStrBuffer.append(String.format("standardMoisture: %-5.3f\n",   this.standardMoisture));
		}
		
		// print CorrectionInfo
		int[] itemCodes = ItemCodeDefine.getAllItems();
		CorrectionItemInfo corrInfo = null;
		for (int i=0 ; i<itemCodes.length ; i++) {
			
			corrInfo = this.correctionApplied.get(itemCodes[i]);
			if (corrInfo == null) {
				continue;
			}
			infoStrBuffer.append(String.format("correction(%-3s) : oxyg(%-3s), temp(%-3s), mois(%-3s)\n", 
					ItemCodeDefine.getItemName(itemCodes[i]),
					corrInfo.isOxygenApplied() ? ("ON") : ("OFF"),
					corrInfo.isTemperatureApplied() ? ("ON") : ("OFF"),
					corrInfo.isMoistureApplied() ? ("ON") : ("OFF") ));
		} // end of for-loop

		// return
		return infoStrBuffer.toString();

	} // end of method


	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * 
	 */
	public void setStackDescr(
			String stackDescr, 
			String facilityDescr, 
			String facilityClass1, 
			String facilityClass2, 
			String fuelName) {
		
		this.stackDescr = stackDescr;
		this.facilityDescr = facilityDescr;
		this.facilityClass1 = facilityClass1;
		this.facilityClass2 = facilityClass2;
		this.fuelName = fuelName;
	
	}
	
	/**
	 * @return the factoryInfo
	 */
	public FactoryInfo getFactoryInfo() {
		return factoryInfo;
	}

	/**
	 * @param factoryInfo the factoryInfo to set
	 */
	public void setFactoryInfo(FactoryInfo factoryInfo) {
		this.factoryInfo = factoryInfo;
	}

	/**
	 * @return the stackNumber
	 */
	public int getStackNumber() {
		return stackNumber;
	}

	/**
	 * @param stackNumber the stackNumber to set
	 */
	public void setStackNumber(int stackNumber) {
		this.stackNumber = stackNumber;
	}

	/**
	 * @return the stackName
	 */
	public String getStackName() {
		return stackName;
	}

	/**
	 * @param stackName the stackName to set
	 */
	public void setStackName(String stackName) {
		this.stackName = stackName;
	}

	/**
	 * @return the standardMoisture
	 */
	public double getStandardMoisture() {
		return standardMoisture;
	}

	/**
	 * @param standardMoisture the standardMoisture to set
	 */
	public void setStandardMoisture(double standardMoisture) {
		this.standardMoisture = standardMoisture;
	}

	
	/**
	 * 
	 */
	public CorrectionItemInfo getCorrectionInfo(int itemCode) {
		return this.correctionApplied.get(itemCode);
	}
	
	/**
	 * 
	 */
	public CorrectionItemInfo[] getAllCorrectionInfo() {
		
		CorrectionItemInfo[] correctionInfoList = 
				this.correctionApplied.values().toArray(new CorrectionItemInfo[this.correctionApplied.size()]);
		Arrays.sort(correctionInfoList);
		return correctionInfoList;
		
	} // end of method

	/**
	 * 
	 */
	public void setCorrectionInfo(int itemCode, double stdOxyg, int oxyg, int temp, int mois) {
		
		CorrectionItemInfo corrInfo = this.correctionApplied.get(itemCode);
		if (corrInfo == null) {
			return;
		}
		corrInfo.setStdOxygen(stdOxyg);
		corrInfo.setStdMoisture(this.standardMoisture);
		corrInfo.setOxygenApplied(oxyg != 0);
		corrInfo.setTemperatureApplied(temp != 0);
		corrInfo.setMoistureApplied(mois != 0);
		
	} // end of method
	
	/**
	 * 
	 */
	public void setCorrectionInfo(StackInfo clone) {
		
		this.correctionApplied.clear();
		int[] itemCodes = ItemCodeDefine.getAllItems();
		for (int i=0 ; i<itemCodes.length ; i++) {
			this.correctionApplied.put(
					itemCodes[i], 
					new CorrectionItemInfo(clone.correctionApplied.get(itemCodes[i])));
		}
		
	} // end of method
	
	/**
	 * 
	 */
	public double getStandardOxygen(int itemCode) {
		
		CorrectionItemInfo corrInfo = this.correctionApplied.get(itemCode);
		if (corrInfo == null) {
			return -1.0D;
		}
		return corrInfo.getStdOxygen();
		
	} // end of method

	/**
	 * 
	 */
	public boolean isCorrectionAppliedOxygen(int itemCode) {
		
		CorrectionItemInfo corrInfo = this.correctionApplied.get(itemCode);
		if (corrInfo == null) {
			return false;
		}
		return corrInfo.isOxygenApplied();
		
	} // end of method
	
	/**
	 * 
	 */
	public boolean isCorrectionAppliedTemperture(int itemCode) {
		
		CorrectionItemInfo corrInfo = this.correctionApplied.get(itemCode);
		if (corrInfo == null) {
			return false;
		}
		return corrInfo.isTemperatureApplied();
		
	} // end of method
	
	/**
	 * 
	 */
	public boolean isCorrectionAppliedMoisture(int itemCode) {
		
		CorrectionItemInfo corrInfo = this.correctionApplied.get(itemCode);
		if (corrInfo == null) {
			return false;
		}
		return corrInfo.isMoistureApplied();
		
	} // end of method

	/**
	 * @return the stackDescr
	 */
	public String getStackDescr() {
		return stackDescr;
	}

	/**
	 * @param stackDescr the stackDescr to set
	 */
	public void setStackDescr(String stackDescr) {
		this.stackDescr = stackDescr;
	}

	/**
	 * @return the facilityDescr
	 */
	public String getFacilityDescr() {
		return facilityDescr;
	}

	/**
	 * @param facilityDescr the facilityDescr to set
	 */
	public void setFacilityDescr(String facilityDescr) {
		this.facilityDescr = facilityDescr;
	}

	/**
	 * @return the facilityClass1
	 */
	public String getFacilityClass1() {
		return facilityClass1;
	}

	/**
	 * @param facilityClass1 the facilityClass1 to set
	 */
	public void setFacilityClass1(String facilityClass1) {
		this.facilityClass1 = facilityClass1;
	}

	/**
	 * @return the facilityClass2
	 */
	public String getFacilityClass2() {
		return facilityClass2;
	}

	/**
	 * @param facilityClass2 the facilityClass2 to set
	 */
	public void setFacilityClass2(String facilityClass2) {
		this.facilityClass2 = facilityClass2;
	}

	/**
	 * @return the fuelName
	 */
	public String getFuelName() {
		return fuelName;
	}

	/**
	 * @param fuelName the fuelName to set
	 */
	public void setFuelName(String fuelName) {
		this.fuelName = fuelName;
	}

	/**
	 * add exhaustInfo
	 */
	public void addExhaustInfo(ExhaustInfo exhaustInfo) {
		this.exhaustInfoArray.add(exhaustInfo);
	} // end of method

	/**
	 * add preventionInfo
	 */
	public void addPreventionInfo(PreventionInfo preventionInfo) {
		this.preventionInfoArray.add(preventionInfo);
	} // end of method
	
	/**
	 * get all ExhaustInfo
	 */
	public ExhaustInfo[] getAllExhaustInfo() {
		return this.exhaustInfoArray.toArray(new ExhaustInfo[this.exhaustInfoArray.size()]);
	} // end of method

	/**
	 * get all PreventionInfo
	 */
	public PreventionInfo[] getAllPreventionInfo() {
		return this.preventionInfoArray.toArray(new PreventionInfo[this.preventionInfoArray.size()]);
	} // end of method

} // end of class
