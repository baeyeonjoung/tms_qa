/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

import kr.or.webservice.server.model.CorrectionInfo;
import kr.or.webservice.server.model.FactInfo;
import kr.or.webservice.server.model.StckInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.db.CorrectionRecord;
import com.snu.tms.qa.soap.QueryCorrectionInfo;
import com.snu.tms.qa.soap.QueryExhaustInfo;
import com.snu.tms.qa.soap.QueryFactInfo;
import com.snu.tms.qa.soap.QueryPreventionInfo;
import com.snu.tms.qa.soap.QueryStckInfo;

/**
 * Data manager for Fact_code, Stck_code, CorrectionInfo (using Singleton Pattern)
 * 
 */
public class InventoryDataManager {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(InventoryDataManager.class);

	// for singleton pattern
	private static InventoryDataManager instance = null;
	
	public static  Comparator<FactoryInfo> factoryInfoComparator = new Comparator<FactoryInfo>() {
		public int compare(FactoryInfo data1, FactoryInfo data2) {
			if (data1.getCode().length() == 5 && data2.getCode().length() == 6) {
				return -1;
			} else if (data1.getCode().length() == 6 && data2.getCode().length() == 5) {
				return 1;
			} else {
				return data1.getCode().compareTo(data2.getCode());
			}
		}
	};
	
	// Selected Stack
	private StackInfo  selectedStackInfo = null;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	// Factory Hashtable
	private  Hashtable<String, FactoryInfo>  factoryHash = new Hashtable<String, FactoryInfo>();
	
	//==========================================================================
	// Constructors
	//==========================================================================
	private InventoryDataManager() {
		
		//  prepare data-set and query inventory data from database
		init();
		
	} // end of constructor
	

	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Singleton pattern
	 */
	public static InventoryDataManager getInstance() {
		
		if (instance == null) {
			instance = new InventoryDataManager();
			return instance;
		} 
		
		// return
		return instance;
		
	}
	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {
		return "";
	} // end of method

	/**
	 * @return the selectedStackInfo
	 */
	public StackInfo getSelectedStackInfo() {
		return selectedStackInfo;
	}

	/**
	 * @param selectedStackInfo the selectedStackInfo to set
	 */
	public void setSelectedStackInfo(StackInfo selectedStackInfo) {
		this.selectedStackInfo = selectedStackInfo;
	}


	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * prepare data-set and query inventory data from database
	 */
	private void init() {
		
		int  factoryCount = 0;
		int  stackCount = 0;
		
		// Get all factoryInfo
		FactoryInfo factoryInfo = null;
		FactInfo[] factories = QueryFactInfo.queryAllFactInfo();
		for (FactInfo factRecord : factories) {
			factoryInfo = new FactoryInfo(
					factRecord.fact_code,
					factRecord.fact_nams,
					factRecord.fact_naml,
					factRecord.fact_namt,
					factRecord.fact_addr,
					factRecord.fact_prod,
					factRecord.fact_numb,
					factRecord.area_code2,
					"");
			this.factoryHash.put(factoryInfo.getCode(), factoryInfo);
			factoryCount++;
			//System.out.println(factoryInfo + "\n");
		}
		
		// Logging
		logger.debug("Query factoryInfo table and setup InventoryDataManager finished. (Factory : " + factoryCount + ")");

		// Get all StackInfo
		// 신규 STACK 프로그램에서는 STACK에 대한 배출시설 분류체계를 적용하지 않는다.
		// 따라서 STACK NAME을 그대로 분류체계로 설정한다. (천대균 실장 논의 결과 - 20161013)
		StckInfo[] stacks = QueryStckInfo.queryAllStckInfo();
		StackInfo stackInfo = null;
		for (StckInfo stckRecord : stacks) {

			// Add StackInfo
			factoryInfo = this.factoryHash.get(stckRecord.fact_code);
			if (factoryInfo == null) {
				continue;
			}
			try {
				stackInfo = factoryInfo.addStackInfo(
						Integer.parseInt(stckRecord.stck_code), 
						stckRecord.stck_name, 
						Double.parseDouble(stckRecord.stck_oxyg), 
						Double.parseDouble(stckRecord.stck_mois));
				stackCount++;
				
				// Set Stack classification
				stackInfo.setStackDescr(
						stckRecord.stck_name, 
						stckRecord.stck_name, 
						stckRecord.stck_name, 
						stckRecord.stck_name, 
						"");
			} catch (Exception e) {
				logger.error(e);
			}
			
		} // end of for-loop

		// Logging
		logger.debug("Query StackInfo table and setup InventoryDataManager finished. (Stacks : " + stackCount + ")");
		
		
		// Setup CorrectionFactor
		int correctionItemCount = 0;
		CorrectionInfo[] correctionInfos = QueryCorrectionInfo.queryAllCorrectionInfo();
		for (CorrectionInfo corrRecord : correctionInfos) {
			
			try {
				// Get StackInfo
				stackInfo = this.getStackInfo(corrRecord.fact_code, Integer.parseInt(corrRecord.stck_code));
				if (stackInfo == null) {
					continue;
				} else {
					stackInfo.setCorrectionInfo(
							Integer.parseInt(corrRecord.item_code), 
							Double.parseDouble(corrRecord.std_oxgy),
							(corrRecord.oxyg_corr.equals("N") ? 0 : 1), 
							(corrRecord.temp_corr.equals("N") ? 0 : 1), 
							(corrRecord.mois_corr.equals("N") ? 0 : 1));
					correctionItemCount++;
					//System.out.println(stackInfo);
				}
			} catch (Exception e) {
				logger.error(e);
			}
		}
		/**
		int correctionItemCount = 0;
		Vector<CorrectionRecord> records = com.snu.tms.qa.database.QueryCorrectionInfo.queryAllCorrectionInfo();
		for (CorrectionRecord corrRecord : records) {
			
			try {
				// Get StackInfo
				stackInfo = this.getStackInfo(corrRecord.getFact_code(), corrRecord.getStck_code());
				if (stackInfo == null) {
					continue;
				} else {
					stackInfo.setCorrectionInfo(
							corrRecord.getItem_code(), 
							corrRecord.getStd_oxgy(),
							(corrRecord.getOxyg_corr().equals("N") ? 0 : 1), 
							(corrRecord.getTemp_corr().equals("N") ? 0 : 1), 
							(corrRecord.getMois_corr().equals("N") ? 0 : 1));
					correctionItemCount++;
					//System.out.println(stackInfo);
				}
			} catch (Exception e) {
				logger.error(e);
			}
		}
		*/
		
		// Logging
		logger.debug("Query correctionItem table and setup InventoryDataManager finished. (CorrectionItem : " + correctionItemCount + ")");
		
		/**
		// Setup Stack Description
		int stackDescrCount = 0;
		Vector<StckClassRecord> stckClassRecords = QueryStckClassInfo.queryAllStckClassInfo();
		for (StckClassRecord stckClassRecord : stckClassRecords) {
			// Get StackInfo
			stackInfo = this.getStackInfo(stckClassRecord.getFact_code(), stckClassRecord.getStck_code());
			if (stackInfo == null) {
				continue;
			} else {
				stackInfo.setStackDescr(
						stckClassRecord.getStck_desc(), 
						stckClassRecord.getProc_desc(), 
						stckClassRecord.getProc_class1(), 
						stckClassRecord.getProc_class2(), 
						stckClassRecord.getFuel_name());
				stackDescrCount++;
				//System.out.println(stackInfo);
			}
		}
		
		// Logging
		logger.debug("Query StckDescrption table and setup InventoryDataManager finished. (stackDescrCount : " + stackDescrCount + ")");
		*/
		
		// Set up ExhaustInfo
		kr.or.webservice.server.model.ExhaustInfo[] exhaustInfoArray = QueryExhaustInfo.queryAllExhaustInfo();
		ExhaustInfo exhaustInfo = null;
		String factCode = "";
		int    stackCode = 0;
		FactoryInfo factInfo  = null;
		for (kr.or.webservice.server.model.ExhaustInfo record : exhaustInfoArray) {
			
			try {
				// get FactoryInfo and StackInfo
				factCode = record.fact_code;
				stackCode = Integer.parseInt(record.stck_code);
				factInfo = this.getFactoryInfo(factCode);
				stackInfo = this.getStackInfo(factCode, stackCode);
				if (factInfo == null || stackInfo == null) {
					continue;
				}
				
				// Get ExhaustInfo
				exhaustInfo = new ExhaustInfo(
						factInfo, stackInfo, 
						record.exhstCode, 
						record.exhstName,
						record.exhstCapacity);
				stackInfo.addExhaustInfo(exhaustInfo);
				//System.out.println(exhaustInfo);
			} catch (Exception e) {
				logger.error(e);
			}
		
		} // end of for-loop
		logger.debug("Query ExhaustInfo table and setup InventoryDataManager finished. (Exhaust facilities count : " + exhaustInfoArray.length + ")");
		
		// Set up PreventionInfo
		kr.or.webservice.server.model.PreventionInfo[] preventionInfoArray = QueryPreventionInfo.queryAllPreventionInfo();
		PreventionInfo preventionInfo = null;
		for (kr.or.webservice.server.model.PreventionInfo record : preventionInfoArray) {
			
			try {
				// get FactoryInfo and StackInfo
				factCode = record.fact_code;
				stackCode = Integer.parseInt(record.stck_code);
				factInfo = this.getFactoryInfo(factCode);
				stackInfo = this.getStackInfo(factCode, stackCode);
				if (factInfo == null || stackInfo == null) {
					continue;
				}
				
				// Get ExhaustInfo
				preventionInfo = new PreventionInfo(
						factInfo, stackInfo, 
						record.exhstCode, 
						record.prvnName,
						record.prvnCapacity);
				stackInfo.addPreventionInfo(preventionInfo);
				//System.out.println(preventionInfo);
			} catch (Exception e) {
				logger.error(e);
			}
		
		} // end of for-loop
		logger.debug("Query PreventionInfo table and setup InventoryDataManager finished. (Prevention facilities count : " + preventionInfoArray.length + ")");
		
	} // end of method

	/**
	 * 
	 */
	public String[] getAllStackClass() {
		
		HashSet<String> classHash = new HashSet<String>();
		// Get all stackInfo
		FactoryInfo factInfo = null;
		for (Enumeration<FactoryInfo> e = this.factoryHash.elements(); e.hasMoreElements();) {
			factInfo = e.nextElement();
			StackInfo[] stackArray = factInfo.getAllStackInfo();
			if (stackArray != null) {
				for (StackInfo stack : stackArray) {
					classHash.add(stack.getFacilityClass2());
				}
			}
		}
		
		// Sorting
		String[] classStrArray = classHash.toArray(new String[classHash.size()]);
		Arrays.sort(classStrArray);
		
		// return 
		return classStrArray;
		
	}
	
	/**
	 * 
	 */
	public FactoryInfo[] getAllFactoryInfo() {
		
		// init
		Vector<FactoryInfo> factoryArray = new Vector<FactoryInfo>();
		
		// Get all stackInfo
		for (Enumeration<FactoryInfo> e = this.factoryHash.elements(); e.hasMoreElements();) {
			factoryArray.add(e.nextElement());
		}
		FactoryInfo[] factories = factoryArray.toArray(new FactoryInfo[factoryArray.size()]);
		Arrays.sort(factories, factoryInfoComparator);
		
		//return
		return factories;
		
	}
	
	/**
	 * get factoryInfo
	 */
	public FactoryInfo getFactoryInfo(String factoryCode) {
		
		// search factInfo
		FactoryInfo factInfo = this.factoryHash.get(factoryCode);
		return factInfo;
		
	} // end of method
	
	/**
	 * 
	 */
	public FactoryInfo[] getAllFactoryInfo(String searchText) {
		
		// init
		Vector<FactoryInfo> factoryArray = new Vector<FactoryInfo>();
		
		// Get all stackInfo
		FactoryInfo factoryInfo;
		for (Enumeration<FactoryInfo> e = this.factoryHash.elements(); e.hasMoreElements();) {

			factoryInfo = e.nextElement();
			if (factoryInfo.getFullName().contains(searchText)) {
				factoryArray.add(factoryInfo);
			} else if (factoryInfo.getCode().contains(searchText)) {
				factoryArray.add(factoryInfo);
			}
		}
		FactoryInfo[] factories = factoryArray.toArray(new FactoryInfo[factoryArray.size()]);
		Arrays.sort(factories, factoryInfoComparator);
		
		//return
		return factories;
		
	}

	/**
	 * 
	 */
	public StackInfo[] getAllStackInfo() {
		
		// init
		Vector<StackInfo> stacks = new Vector<StackInfo>();
		
		// Get all stackInfo
		FactoryInfo factInfo = null;
		for (Enumeration<FactoryInfo> e = this.factoryHash.elements(); e.hasMoreElements();) {
			factInfo = e.nextElement();
			StackInfo[] stackArray = factInfo.getAllStackInfo();
			if (stackArray != null) {
				for (StackInfo stack : stackArray) {
					stacks.add(stack);
				}
			}
		}
		
		//return
		return stacks.toArray(new StackInfo[stacks.size()]);
		
	}
	
	/**
	 * 
	 */
	public StackInfo[] getStackInfo(String factoryCode) {
		
		// Search FactoryInfo
		FactoryInfo factoryInfo = this.factoryHash.get(factoryCode);
		if (factoryInfo == null) {
			return null;
		}
			
		// Search StackInfo
		StackInfo[] stackInfoArray = factoryInfo.getAllStackInfo();
		Arrays.sort(stackInfoArray);
		return stackInfoArray;
		
	}
	
	/**
	 * 
	 */
	public StackInfo[] getAllStackInfo(String stackClassStr) {
		
		// init
		Vector<StackInfo> stacks = new Vector<StackInfo>();
		
		// Get all stackInfo
		FactoryInfo factInfo = null;
		for (Enumeration<FactoryInfo> e = this.factoryHash.elements(); e.hasMoreElements();) {
			factInfo = e.nextElement();
			StackInfo[] stackArray = factInfo.getAllStackInfo();
			if (stackArray != null) {
				for (StackInfo stack : stackArray) {
					if (stack.getFacilityClass2().equals(stackClassStr)) {
						stacks.add(stack);
					}
				}
			}
		}
		
		//return
		return stacks.toArray(new StackInfo[stacks.size()]);
		
	}
	
	/**
	 * 
	 */
	public StackInfo getStackInfo(String factCode, int stackNumb) {
		
		// Search FactoryInfo
		FactoryInfo factoryInfo = this.factoryHash.get(factCode);
		if (factoryInfo == null) {
			return null;
		}
		
		// Search StackInfo
		StackInfo stackInfo = factoryInfo.getStackInfo(stackNumb);
		return stackInfo;
		
	}
	
	
} // end of class
