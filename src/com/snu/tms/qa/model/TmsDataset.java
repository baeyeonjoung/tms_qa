/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Data Element for TMS Data
 * 
 */
public class TmsDataset {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(TmsDataset.class);
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  TmsDataHeader  tmsDataHeader = null;
	
	private  Hashtable<Long, TmsDataRecord>  timeHash = new Hashtable<Long, TmsDataRecord>();
	
	private  Vector<TmsDataRecord> tmsDataRecords = new Vector<TmsDataRecord>();
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public TmsDataset() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// Append information
		infoStrBuffer.append("--------------------------------------------------\n");
		infoStrBuffer.append("Header\n");
		infoStrBuffer.append("--------------------------------------------------\n");
		infoStrBuffer.append( (this.tmsDataHeader == null) ? "":this.tmsDataHeader.toString() + "\n" );
		infoStrBuffer.append("--------------------------------------------------\n");
		infoStrBuffer.append("Data\n");
		infoStrBuffer.append("--------------------------------------------------\n");
		int[][] itemCount = getItemValueCount();
		for (int i=0 ; i<itemCount.length ; i++) {
			infoStrBuffer.append(String.format("%-10s : %6d\n", 
					ItemCodeDefine.getItemName(itemCount[i][0]), itemCount[i][1]));
		}
		infoStrBuffer.append("--------------------------------------------------\n");
		
		// return
		return infoStrBuffer.toString();

	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Clear all data
	 */
	public void clear() {
		
		// Delete all data 
		this.tmsDataHeader = null;
		this.timeHash.clear();
		this.tmsDataRecords.clear();;
		
		System.gc();
		
	} // end of method
	
	/**
	 * @return the tmsDataHeader
	 */
	public TmsDataHeader getTmsDataHeader() {
		return tmsDataHeader;
	}

	/**
	 * @param tmsDataHeader the tmsDataHeader to set
	 */
	public void setTmsDataHeader(TmsDataHeader tmsDataHeader) {
		
		// Set data
		this.tmsDataHeader = tmsDataHeader;
		
		// Init TimeHash
		this.timeHash.clear();
		
		// Count time devision
		long endTime = tmsDataHeader.getEnd_time();
		long startTime = tmsDataHeader.getStart_time();
		long timeInterval = (long)tmsDataHeader.getTime_interval() * 60L * 1000L;
		long timeRange = endTime - startTime;
		
		int dataCount = (int) Math.ceil(timeRange/ timeInterval);
		
		// Init tmsDataRecords
		long time = startTime;
		TmsDataRecord record;
		for (int i=0 ; i<dataCount ; i++) {
			record = new TmsDataRecord(time, tmsDataHeader);
			this.tmsDataRecords.add(record);
			this.timeHash.put(time, record);
			time += timeInterval;
		}
		
	} // end of method
	
	
	/**
	 * Do data validation check
	 *   : If some of data are missing, then use the average value of the last 30min 
	 */
	public void doDataValidationCheck() {

		/**
		// get item value count
		int[] itemValueCounts = getItemValueCount();
		
		// do correcting
		for (int i=0 ; i<itemValueCounts.length ; i++) {
			
			if (itemValueCounts[i] == 0) {
				continue;
			}
			
			// get all values for target item
			TmsDataSingleItemRecord[] records = getTmsDataRecords(i);
			for (int j=0 ; j<records.length ; j++) {
				if (records[j].getTmsDataCell().getData_value() == -1.0D) {
					double avgValue = computeAverageValue(records, j, 6);
					records[j].getTmsDataCell().setData_value(avgValue);
				}
			}
		}
		*/
		
		// get item value count
		// If some item's valid data count is less than 50% of other's count,
		// we remove that item
		int[][] itemValueCounts = getItemValueCount();
		int maxDataCount = 0;
		for (int i=0 ; i<itemValueCounts.length ; i++) {
			if (maxDataCount < itemValueCounts[i][1]) {
				maxDataCount = itemValueCounts[i][1];
			}
		}
		int lowerDataCountLimit = (int)Math.rint((double)maxDataCount * 0.5D);
		for (int i=0 ; i<itemValueCounts.length ; i++) {
			if (itemValueCounts[i][1] < lowerDataCountLimit) {
				removeDataColumn(itemValueCounts[i][0]);
			}
		}
		
		// find record which one or more item value is not valid
		// and remove record
		Vector<TmsDataRecord> removeTarget = new Vector<TmsDataRecord>();
		itemValueCounts = getItemValueCount();
		for (TmsDataRecord record : this.tmsDataRecords) {

			for (int i=0 ; i<itemValueCounts.length ; i++) {
				
				// TM3의 상태가 비정상이라도 자료 포함
				if (itemValueCounts[i][0] == ItemCodeDefine.TM3) {
					continue;
				}
				if (record.getDataCell(itemValueCounts[i][0]).getData_value() < 0.0D) {
					removeTarget.add(record);
					break;
				}
				
			} // end of for-loop
			
		} // end of for-loop
		
		// remove
		for (TmsDataRecord record : removeTarget) {
			this.tmsDataRecords.remove(record);
			this.timeHash.remove(record.getTime());
		}
		logger.debug("Some record have been removed (invalid value) : " + removeTarget.size() + " records");
		
	} // end of method
	
	/**
	 * remove data column
	 */
	public void removeDataColumn(int item) {
		
		for (TmsDataRecord record : this.tmsDataRecords) {
			record.getDataCell(item).setData_value(-1.0D);
		}
		
	} // end of method
	
	/**
	 * Get average value
	 */
	public double computeAverageValue(TmsDataSingleItemRecord[] records, int index, int lookupRange) {
		
		double sumValue = 0.0D;
		int valueCount = 0;
		double value = 0.0D;
		for (int i=index ; i<(index-lookupRange) && i>=0 ; i--) {
			value = records[i].getTmsDataCell().getData_value();
			if (value != -1.0D) {
				valueCount++;
				sumValue += value;
			}
		}
		
		if (valueCount == 0) {
			return 0;
		} else {
			return (sumValue / (double)valueCount);
		}
		
	} // end of method
	
	
	/**
	 * Get item value count
	 */
	public int[][] getItemValueCount() {
		
		// prepare value count
		int[] items = ItemCodeDefine.getAllItems();
		int[] itemValueCount = new int[items.length];
		Arrays.fill(itemValueCount, 0);
		
		// count
		if (this.tmsDataRecords == null) {
			return new int[0][0];
		}
		for (TmsDataRecord record : this.tmsDataRecords) {
			for (int i=0 ; i<items.length ; i++) {
				if (record.getDataCell(items[i]).getData_value() > 0.0D) {
					itemValueCount[i]++;
				}
			}
		}
		
		// return
		int itemCount = 0;
		for (int i=0 ; i<itemValueCount.length ; i++) {
			if (itemValueCount[i] > 0) {
				itemCount++;
			}
		}
		int index = 0;
		int[][] returnValues = new int[itemCount][2];
		for (int i=0 ; i<items.length ; i++) {
			if (itemValueCount[i] > 0) {
				returnValues[index][0] = items[i];
				returnValues[index][1] = itemValueCount[i];
				index++;
			}
		}
		return returnValues;
		
	} // end of method
	
	/**
	 * Get item value count
	 */
	public int[] getItems() {
		
		// prepare value count
		int[][] itemValueCount = getItemValueCount();
		int[] items = new int[itemValueCount.length];
		
		// search valid items
		int itemCount = 0;
		for (int i=0 ; i<itemValueCount.length ; i++) {
			
			if (itemValueCount[i][1] <= 0) {
				continue;
			}
			items[itemCount++] = itemValueCount[i][0];
			
		} // end of for-loop
		
		// return
		return Arrays.copyOf(items, itemCount);
		
	} // end of method

	/**
	 * @return the tmsDataRecords
	 */
	public TmsDataRecord[] getTmsDataRecords() {
		return this.tmsDataRecords.toArray(new TmsDataRecord[this.tmsDataRecords.size()]);
	}
	
	
	/**
	 * Get TmsDataRecord count
	 */
	public int getTmsDataRecordCount() {
		return tmsDataRecords.size();
	}// end of method
	
	
	/**
	 * Get TmsRecords for selected item_code
	 */
	public TmsDataSingleItemRecord[] getTmsDataRecords(int item) {
		
		// memory allocation
		TmsDataSingleItemRecord[] singleItemRecords = new TmsDataSingleItemRecord[this.tmsDataRecords.size()];
		
		// copy data
		for (int i=0 ; i<singleItemRecords.length ; i++) {
			singleItemRecords[i] = new TmsDataSingleItemRecord(item, this.tmsDataRecords.get(i));
		}
		
		// return
		return singleItemRecords;
		
	} // end of records
	
	/**
	 * Get TmsRecords for selected item_code
	 */
	public TmsDataSingleItemRecord[] getValidTmsDataRecords(int item) {
		
		// prepare temporary data saving vector
		Vector<TmsDataSingleItemRecord> singleItemRecords = new Vector<TmsDataSingleItemRecord>();
		
		// copy data
		TmsDataCell dataCell = null;
		int N = this.tmsDataRecords.size();
		for (int i=0 ; i<N ; i++) {
			dataCell = this.tmsDataRecords.get(i).getDataCell(item);
			if (dataCell != null && dataCell.getData_value() != -1.0D) {
				singleItemRecords.add(new TmsDataSingleItemRecord(item, this.tmsDataRecords.get(i)));
			}
		}
		
		// return
		return singleItemRecords.toArray(new TmsDataSingleItemRecord[singleItemRecords.size()]);
		
	} // end of records

	
	/**
	 * 
	 */
	public TmsDataRecord getTmsDataRecord(long time) {
		return this.timeHash.get(time);
	}
	
	/**
	 * Get data values
	 */
	public double[][] getTmsDataRecordValues() {
		
		int[][] itemValueCount = getItemValueCount();
		int N = getTmsDataRecordCount();
		double[][] values = new double[N][itemValueCount.length];
		TmsDataRecord record;
		for (int i=0 ; i<values.length ; i++) {
			record = this.tmsDataRecords.get(i);
			for (int j=0 ; j<itemValueCount.length ; j++) {
				values[i][j] = record.getDataCell(itemValueCount[j][0]).getData_value();
			}
		}
		return values;
		
	} // end of method
	
	/**
	 * Get data values
	 */
	public double[][] getTmsDataRecordBeforeValues() {
		
		int[][] itemValueCount = getItemValueCount();
		int N = getTmsDataRecordCount();
		double[][] values = new double[N][itemValueCount.length];
		TmsDataRecord record;
		for (int i=0 ; i<values.length ; i++) {
			record = this.tmsDataRecords.get(i);
			for (int j=0 ; j<itemValueCount.length ; j++) {
				values[i][j] = record.getDataCell(itemValueCount[j][0]).getOrigin_value();
			}
		}
		return values;
		
	} // end of method
	
	/**
	 * Get data values
	 */
	public long[] getTmsDataRecordTimes() {
		
		int N = getTmsDataRecordCount();
		long[] times = new long[N];
		TmsDataRecord record;
		for (int i=0 ; i<times.length ; i++) {
			record = this.tmsDataRecords.get(i);
			times[i] = record.getTime();
		}
		return times;
		
	} // end of method

	
} // end of class
