/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.analysis.outlier.DoAnalysisReverseCorrection;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.DetectionRuleset;
import com.snu.tms.qa.model.ruleset.RawDataCell;
import com.snu.tms.qa.model.ruleset.RulesetModel;
import com.snu.tms.qa.model.ruleset.RulesetPatternModel;
import com.snu.tms.qa.model.ruleset.RulesetWeightingModel;

/**
 * Data Element for TMS Data
 * 
 */
public class EvalDatasetManager {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(EvalDatasetManager.class);

	private static EvalDatasetManager instance = new EvalDatasetManager();
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	// TMS raw data
	private  TmsDataset  tmsDataset = null;
	
	// items
	private  int[]       items = null;
	
	// TMS 30min Data
	private  Tms30minDataset  tms30minDataset = null;
	
	private Vector<DetectionPatternDataset> detectionPatternDataset = 
			new Vector<DetectionPatternDataset>();
	
	// weighting factor model
	private RulesetWeightingModel rulesetWeighingModel = null;
	
	private RulesetModel  rulesetModel = null;
	
	// ruleset base date 
	private  long      rulsetBaseStartTime = 0L;
	private  long      rulsetBaseEndTime   = 0L;

	private Hashtable<Integer, Vector<TmsDataRecord>> patternTmsDataRecordHash = 
			new Hashtable<Integer, Vector<TmsDataRecord>>();
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	private EvalDatasetManager() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================


	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * getInstance()
	 */
	public static EvalDatasetManager getInstance() {
		return instance;
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @param tmsDataSet the tmsDataSet to set
	 */
	public void setTmsDataset(TmsDataset tmsDataset) {

		// set TmsDataset
		this.tmsDataset = tmsDataset;
		logger.debug(this.tmsDataset.toString());
		
		// set time
		this.rulsetBaseStartTime = tmsDataset.getTmsDataHeader().getStart_time();
		this.rulsetBaseEndTime   = tmsDataset.getTmsDataHeader().getEnd_time();
		
		// 측정기기 상태정보가 교정중(1), 동잗불량(2), 전원단절(4), 보수중(8) 등에 대해서
		// 일부 항목이 다른 항목에 비해 유효한 데이터가 50% 미만일 경우에는
		// 해당 Item을 자료에서 삭제한다.
		this.tmsDataset.doDataValidationCheck();
		//logger.debug(this.tmsDataset.toString());
		
		// compute before correction value
		// 사용자가 Manualy 설정하도록 변경 (2016-10-22)
		//DoAnalysisReverseCorrection.doReverseCorrection(this.tmsDataset);
		
	} // end of method

	/**
	 * @param tmsDataSet the tmsDataSet to set
	 */
	public void setTms30minDataset(Tms30minDataset tms30minDataset) {
		
		// set TmsDataset
		this.tms30minDataset = tms30minDataset;
		logger.debug(this.tms30minDataset.toString());
		
	} // end of method

	/**
	 * init data model
	 */
	public void initDataModel(RulesetModel rulesetModel) {
		
		// clear all previous patternData
		clearPatternData();
		
		// set ruleset model
		this.rulesetModel = rulesetModel;
		this.rulsetBaseStartTime = rulesetModel.getRulesetStartTime();
		this.rulsetBaseEndTime   = rulesetModel.getRulesetEndTime();
		
		// set weighting factor
		this.rulesetWeighingModel = rulesetModel.getRulesetWeightingModel();
		
		// get all pattern model
		RulesetPatternModel[] rulesetPatternModels = this.rulesetModel.getAllRulesetPatternModel();
		if (rulesetPatternModels == null) {
			return;
		}
		
		// assign pattern for all TmsDataRecords
		for (RulesetPatternModel model : rulesetPatternModels) {
			patternTmsDataRecordHash.put(model.getPatternId(), new Vector<TmsDataRecord>());
		} // end of for-loop
		
		TmsDataRecord[] tmsDataRecords = this.tmsDataset.getTmsDataRecords();
		this.items = rulesetPatternModels[0].getItems();
		for (TmsDataRecord tmsDataRecord : tmsDataRecords) {
			
			// 보정전 자료가 입력되지 않은 경우에는 Ruleset에 설정되어 있는 CorrectionInfo로 값을 치환
			DoAnalysisReverseCorrection.doReverseCorrectionForEmptyCell(tmsDataRecord, this.rulesetModel);
			
			// get conformity value for each Pattern
			double[] values = tmsDataRecord.getValues(items);
			PatternConformity[] patternConformities = new PatternConformity[rulesetPatternModels.length];
			for (int i=0 ; i<rulesetPatternModels.length ; i++) {
				
				patternConformities[i] = new PatternConformity(
						rulesetPatternModels[i].getPatternId(),
						rulesetPatternModels[i].getRulesetPatternConformity(items, values));
				//System.out.println(String.format(
				//		"PatternID[%3d] - Conformity[%6.30f", 
				//		patternConformities[i].patternId, 
				//		patternConformities[i].conformityValue));
			
			} // end of for-loop
			
			// sorting (descending)
			Arrays.sort(patternConformities);
			
			// add record
			patternTmsDataRecordHash.get(patternConformities[0].patternId).add(tmsDataRecord);
			
		} // end of for-loop
		
		
		// set detection pattern dataset
		for (RulesetPatternModel model : rulesetPatternModels) {
			
			// create DetectionPatternDataset
			int     patternId   = model.getPatternId();
			String  patternName = model.getPatternName();
			PatternDataHistogram[] patternDataHistograms = model.getPatternDataHistograms();
			double[][] itemDataRange = new double[items.length][2];
			for (int i=0 ; i<items.length ; i++) {
				itemDataRange[i][0] = patternDataHistograms[i].getMinRange();
				itemDataRange[i][1] = patternDataHistograms[i].getMaxRange();
			}
			
			DetectionPatternDataset dataset = new DetectionPatternDataset(
					this.tmsDataset.getTmsDataHeader().getFactoryInfo(),
					this.tmsDataset.getTmsDataHeader().getStackInfo(),
					patternId, 
					patternName,
					items,
					rulsetBaseStartTime,
					rulsetBaseEndTime,
					itemDataRange,
					patternDataHistograms);
			dataset.setEnabled(model.isEnabled());
			dataset.setRulesetCreated(model.isRulesetCreated());
			dataset.setOperateStop(model.isOperateStop());
			dataset.setPcaResultModel(model.getPcaResultModel());
			
			// add data records
			Vector<TmsDataRecord> tmsDataRecordArray = patternTmsDataRecordHash.get(patternId);
			long time;
			TmsDataRecord tmsDataRecord;
			for (int j=0 ; j<tmsDataRecordArray.size() ; j++) {
				
				tmsDataRecord = tmsDataRecordArray.get(j);
				time = tmsDataRecord.getTime();
				TmsDataCell tmsDataCell;
				RawDataCell[] rawDataCells = new RawDataCell[items.length];
				
				for (int k=0 ; k<items.length ; k++) {
					
					tmsDataCell = tmsDataRecord.getDataCell(items[k]);
					rawDataCells[k] = new RawDataCell(
							tmsDataCell.getData_type(), 
							tmsDataCell.getData_value(), 
							tmsDataCell.getOrigin_value());
				}
				
				// add dataRecord
				dataset.addDetectionRecord(new DetectionRecord(dataset, time, patternId, patternName, rawDataCells));
			}
			
			// add ruleset
			DetectionRuleset[] rulesets = model.getDetectionRulesets();
			for (DetectionRuleset ruleset : rulesets) {
				dataset.addRuleset(ruleset);
			}
			
			// add pattern dataset
			this.detectionPatternDataset.add(dataset);
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * Initialize data
	 */
	public void clear() {
		
		this.tmsDataset = null;
		this.tms30minDataset = null;
		this.detectionPatternDataset.clear();
		this.rulesetWeighingModel = null;
		
		System.gc();
		
	} // end of method
	
	/**
	 * clear pattern data
	 */
	public void clearPatternData() {
		this.detectionPatternDataset.clear();
	} // end of method
	

	/**
	 * @return the tmsDataSet
	 */
	public TmsDataset getTmsDataset() {
		return tmsDataset;
	} // end of method
	
	/**
	 * @return the tmsDataSet
	 */
	public Tms30minDataset get30minTmsDataset() {
		return tms30minDataset;
	} // end of method

	/**
	 * Add patternData
	 */
	public void addPatternData(DetectionPatternDataset patternData) {
		this.detectionPatternDataset.add(patternData);
	} // end of method
	
	/**
	 * @return the items
	 */
	public int[] getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(int[] items) {
		this.items = items;
	}

	/**
	 * Get Pattern Count
	 */
	public int getPatternCount() {
		return this.detectionPatternDataset.size();
	} // end of method
	
	/**
	
	/**
	 * Get patternData
	 */
	public DetectionPatternDataset[] getAllPatternData() {
		return this.detectionPatternDataset.toArray(
				new DetectionPatternDataset[this.detectionPatternDataset.size()]);
	} // end of method
	
	/**
	 * Get PatternData
	 */
	public DetectionPatternDataset getPatternData(int patternId) {
		
		// search patternData and return
		DetectionPatternDataset patternData;
		for (Enumeration<DetectionPatternDataset> e=this.detectionPatternDataset.elements(); e.hasMoreElements();) {
			patternData = e.nextElement();
			if (patternData.getPatternId() == patternId) {
				return patternData;
			}
		}
		
		// if matched patternData is not exist,
		return null;

	} // end of method

	/**
	 * @return the rulesetWeighingModel
	 */
	public RulesetWeightingModel getRulesetWeighingModel() {
		return rulesetWeighingModel;
	}
	
	/**
	 * get RulesetModel
	 */
	public RulesetModel getRulesetModel() {
		return this.rulesetModel;
	}

	/**
	 * @param rulesetWeighingModel the rulesetWeighingModel to set
	 */
	public void setRulesetWeighingModel(RulesetWeightingModel rulesetWeighingModel) {
		this.rulesetWeighingModel = rulesetWeighingModel;
	}

	/**
	 * @return the rulsetBaseStartTime
	 */
	public long getRulsetBaseStartTime() {
		return rulsetBaseStartTime;
	}

	/**
	 * @param rulsetBaseStartTime the rulsetBaseStartTime to set
	 */
	public void setRulsetBaseStartTime(long rulsetBaseStartTime) {
		this.rulsetBaseStartTime = rulsetBaseStartTime;
	}

	/**
	 * @return the rulsetBaseEndTime
	 */
	public long getRulsetBaseEndTime() {
		return rulsetBaseEndTime;
	}

	/**
	 * @param rulsetBaseEndTime the rulsetBaseEndTime to set
	 */
	public void setRulsetBaseEndTime(long rulsetBaseEndTime) {
		this.rulsetBaseEndTime = rulsetBaseEndTime;
	}
	
	/**
	 * get tms data records
	 */
	public TmsDataRecord[] getPatternTmsDataRecords(int patternId) {
		
		Vector<TmsDataRecord> patternTmsDataRecords = this.patternTmsDataRecordHash.get(patternId);
		if (patternTmsDataRecords == null) {
			return null;
		}
		return patternTmsDataRecords.toArray(new TmsDataRecord[patternTmsDataRecords.size()]);
		
	} // end of method
	
	/**
	 * get tms data records
	 */
	public int getPatternTmsDataRecordCount(int patternId) {
		
		Vector<TmsDataRecord> patternTmsDataRecords = this.patternTmsDataRecordHash.get(patternId);
		if (patternTmsDataRecords == null) {
			return 0;
		}
		return patternTmsDataRecords.size();
		
	} // end of method
	
	//==========================================================================
	// Inner Class
	//==========================================================================
	class PatternConformity implements Comparable<PatternConformity> {
		
		//----------------------------------------------------------------------
		// Fields
		//----------------------------------------------------------------------
		public  int     patternId = 0;
		public  double  conformityValue = 0.0D;
		
		//----------------------------------------------------------------------
		// Constructor
		//----------------------------------------------------------------------
		public PatternConformity(int patternId, double conformityValue) {
			
			this.patternId = patternId;
			this.conformityValue = conformityValue;
			
		} // end of method
		
		//----------------------------------------------------------------------
		// Implemented method
		//----------------------------------------------------------------------
		/**
		 * compareTo
		 */
		public int compareTo(PatternConformity compare) {
			
			if (this.conformityValue > compare.conformityValue) {
				return -1;
			} else if (this.conformityValue < compare.conformityValue) {
				return 1;
			} else {
				return 0;
			}
			
		} // end of method
		
	} // end of inner class

	
} // end of class
