/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

/**
 * 배출시설 정보
 * 
 */
public class ExhaustInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  FactoryInfo  factoryInfo    = null;
	private  StackInfo    stackInfo      = null;
	
	private  String       exhstCode      = "";
	private  String       exhstName      = "";
	private  String       exhstCapacity  = "";
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for StackInfo
	 */
	public ExhaustInfo() {
		
	} // end of constructor
	
	public ExhaustInfo(
			FactoryInfo  factoryInfo,
			StackInfo    stackInfo,
			String       exhstCode,
			String       exhstName,
			String       exhstCapacity) {

		this.factoryInfo   = factoryInfo;
		this.stackInfo     = stackInfo;
		this.exhstCode     = exhstCode;
		this.exhstName     = exhstName;
		this.exhstCapacity = exhstCapacity;
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * toString()
	 */
	public String toString() {
		
		return String.format(
				"FACT_CODE[%s],  STACK_CODE[%d], EXHAUST_CODE[%s], EXHAUST_NAME[%s], EXHAUST_CAPACITY[%s]",
				factoryInfo.getCode(),
				stackInfo.getStackNumber(),
				exhstCode, exhstName, exhstCapacity);
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the factoryInfo
	 */
	public FactoryInfo getFactoryInfo() {
		return factoryInfo;
	}

	/**
	 * @param factoryInfo the factoryInfo to set
	 */
	public void setFactoryInfo(FactoryInfo factoryInfo) {
		this.factoryInfo = factoryInfo;
	}

	/**
	 * @return the stackInfo
	 */
	public StackInfo getStackInfo() {
		return stackInfo;
	}

	/**
	 * @param stackInfo the stackInfo to set
	 */
	public void setStackInfo(StackInfo stackInfo) {
		this.stackInfo = stackInfo;
	}

	/**
	 * @return the exhstCode
	 */
	public String getExhstCode() {
		return exhstCode;
	}

	/**
	 * @param exhstCode the exhstCode to set
	 */
	public void setExhstCode(String exhstCode) {
		this.exhstCode = exhstCode;
	}

	/**
	 * @return the exhstName
	 */
	public String getExhstName() {
		return exhstName;
	}

	/**
	 * @param exhstName the exhstName to set
	 */
	public void setExhstName(String exhstName) {
		this.exhstName = exhstName;
	}

	/**
	 * @return the exhstCapacity
	 */
	public String getExhstCapacity() {
		return exhstCapacity;
	}

	/**
	 * @param exhstCapacity the exhstCapacity to set
	 */
	public void setExhstCapacity(String exhstCapacity) {
		this.exhstCapacity = exhstCapacity;
	}


} // end of class
