/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

/**
 * Data for each item have to corrected for each fact_code+stck_code
 * 
 */
public class RulesetWeightingRecord {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int     ruleset_id          = 0;

	private  int     multi_weight        = 1;
	private  int     bi_weight           = 1;
	private  int     uni_weight          = 1;
	private  int     corr_weight         = 1;
	
	private  int     warn_threshold      = 2;
	private  int     minor_threshold     = 4;
	private  int     major_threshold     = 6;
	private  int     critical_threshold  = 8;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for CorrectionItem
	 */
	public RulesetWeightingRecord() {}
	
	public RulesetWeightingRecord(
			int     ruleset_id,
			int     multi_weight,
			int     bi_weight,
			int     uni_weight,
			int     corr_weight,
			int     warn_threshold,
			int     minor_threshold,
			int     major_threshold,
			int     critical_threshold) {
		
		this.ruleset_id         = ruleset_id;
		this.multi_weight       = multi_weight;
		this.bi_weight          = bi_weight;
		this.uni_weight         = uni_weight;
		this.corr_weight        = corr_weight;
		this.warn_threshold     = warn_threshold;
		this.minor_threshold    = minor_threshold;
		this.major_threshold    = major_threshold;
		this.critical_threshold = critical_threshold;
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the ruleset_id
	 */
	public int getRuleset_id() {
		return ruleset_id;
	}

	/**
	 * @param ruleset_id the ruleset_id to set
	 */
	public void setRuleset_id(int ruleset_id) {
		this.ruleset_id = ruleset_id;
	}

	/**
	 * @return the multi_weight
	 */
	public int getMulti_weight() {
		return multi_weight;
	}

	/**
	 * @param multi_weight the multi_weight to set
	 */
	public void setMulti_weight(int multi_weight) {
		this.multi_weight = multi_weight;
	}

	/**
	 * @return the bi_weight
	 */
	public int getBi_weight() {
		return bi_weight;
	}

	/**
	 * @param bi_weight the bi_weight to set
	 */
	public void setBi_weight(int bi_weight) {
		this.bi_weight = bi_weight;
	}

	/**
	 * @return the uni_weight
	 */
	public int getUni_weight() {
		return uni_weight;
	}

	/**
	 * @param uni_weight the uni_weight to set
	 */
	public void setUni_weight(int uni_weight) {
		this.uni_weight = uni_weight;
	}

	/**
	 * @return the corr_weight
	 */
	public int getCorr_weight() {
		return corr_weight;
	}

	/**
	 * @param corr_weight the corr_weight to set
	 */
	public void setCorr_weight(int corr_weight) {
		this.corr_weight = corr_weight;
	}

	/**
	 * @return the warn_threshold
	 */
	public int getWarn_threshold() {
		return warn_threshold;
	}

	/**
	 * @param warn_threshold the warn_threshold to set
	 */
	public void setWarn_threshold(int warn_threshold) {
		this.warn_threshold = warn_threshold;
	}

	/**
	 * @return the minor_threshold
	 */
	public int getMinor_threshold() {
		return minor_threshold;
	}

	/**
	 * @param minor_threshold the minor_threshold to set
	 */
	public void setMinor_threshold(int minor_threshold) {
		this.minor_threshold = minor_threshold;
	}

	/**
	 * @return the major_threshold
	 */
	public int getMajor_threshold() {
		return major_threshold;
	}

	/**
	 * @param major_threshold the major_threshold to set
	 */
	public void setMajor_threshold(int major_threshold) {
		this.major_threshold = major_threshold;
	}

	/**
	 * @return the critical_threshold
	 */
	public int getCritical_threshold() {
		return critical_threshold;
	}

	/**
	 * @param critical_threshold the critical_threshold to set
	 */
	public void setCritical_threshold(int critical_threshold) {
		this.critical_threshold = critical_threshold;
	}



} // end of class
