/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

/**
 * Ruleset Model
 * 
 */
public class PatternRulesetPdRecord
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int      ruleset_id          = 0;
	private  int      pattern_id          = 0;
	private  int      ruleset_enable      = 0;
	private  double   threshold_lower     = 0.0D;
	private  double   threshold_upper     = 0.0D;
	private  int      item                = 0;
	private  double   mean_value          = 0.0D;
	private  double   sigma_value         = 0.0D;
	private  double   confi_level         = 0.0D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * constructor for RulesetModel
	 */
	public PatternRulesetPdRecord() {}
	
	public PatternRulesetPdRecord(
			int      ruleset_id,
			int      pattern_id,
			int      ruleset_enable,
			double   threshold_lower,
			double   threshold_upper,
			int      item,
			double   mean_value,
			double   sigma_value,
			double   confi_level) {
		
		this.ruleset_id      = ruleset_id;
		this.pattern_id      = pattern_id;
		this.ruleset_enable  = ruleset_enable;
		this.threshold_lower = threshold_lower;
		this.threshold_upper = threshold_upper;
		this.item            = item;
		this.mean_value      = mean_value;
		this.sigma_value     = sigma_value;
		this.confi_level     = confi_level;
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the ruleset_id
	 */
	public int getRuleset_id() {
		return ruleset_id;
	}

	/**
	 * @param ruleset_id the ruleset_id to set
	 */
	public void setRuleset_id(int ruleset_id) {
		this.ruleset_id = ruleset_id;
	}

	/**
	 * @return the pattern_id
	 */
	public int getPattern_id() {
		return pattern_id;
	}

	/**
	 * @param pattern_id the pattern_id to set
	 */
	public void setPattern_id(int pattern_id) {
		this.pattern_id = pattern_id;
	}

	/**
	 * @return the ruleset_enable
	 */
	public int getRuleset_enable() {
		return ruleset_enable;
	}

	/**
	 * @param ruleset_enable the ruleset_enable to set
	 */
	public void setRuleset_enable(int ruleset_enable) {
		this.ruleset_enable = ruleset_enable;
	}

	/**
	 * @return the threshold_lower
	 */
	public double getThreshold_lower() {
		return threshold_lower;
	}

	/**
	 * @param threshold_lower the threshold_lower to set
	 */
	public void setThreshold_lower(double threshold_lower) {
		this.threshold_lower = threshold_lower;
	}

	/**
	 * @return the threshold_upper
	 */
	public double getThreshold_upper() {
		return threshold_upper;
	}

	/**
	 * @param threshold_upper the threshold_upper to set
	 */
	public void setThreshold_upper(double threshold_upper) {
		this.threshold_upper = threshold_upper;
	}

	/**
	 * @return the item
	 */
	public int getItem() {
		return item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(int item) {
		this.item = item;
	}

	/**
	 * @return the mean_value
	 */
	public double getMean_value() {
		return mean_value;
	}

	/**
	 * @param mean_value the mean_value to set
	 */
	public void setMean_value(double mean_value) {
		this.mean_value = mean_value;
	}

	/**
	 * @return the sigma_value
	 */
	public double getSigma_value() {
		return sigma_value;
	}

	/**
	 * @param sigma_value the sigma_value to set
	 */
	public void setSigma_value(double sigma_value) {
		this.sigma_value = sigma_value;
	}

	/**
	 * @return the confi_level
	 */
	public double getConfi_level() {
		return confi_level;
	}

	/**
	 * @param confi_level the confi_level to set
	 */
	public void setConfi_level(double confi_level) {
		this.confi_level = confi_level;
	}

} // end of class
