/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

/**
 * Ruleset Model
 * 
 */
public class PatternPcaRecord
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int      ruleset_id          = 0;
	private  int      pattern_id          = 0;
	private  String   items               = "";
	private  int      pc_count            = 0;
	private  String   eigen_vector        = "";
	private  String   eigen_value         = "";
	private  String   neigen_value        = "";
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * constructor for RulesetModel
	 */
	public PatternPcaRecord() {}
	
	public PatternPcaRecord(
			int      ruleset_id,
			int      pattern_id,
			String   items,
			int      pc_count,
			String   eigen_vector,
			String   eigen_value,
			String   neigen_value) {
		
		this.ruleset_id   = ruleset_id;
		this.pattern_id   = pattern_id;
		this.items        = items;
		this.pc_count     = pc_count;
		this.eigen_vector = eigen_vector;
		this.eigen_value  = eigen_value;
		this.neigen_value = neigen_value;
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the ruleset_id
	 */
	public int getRuleset_id() {
		return ruleset_id;
	}

	/**
	 * @param ruleset_id the ruleset_id to set
	 */
	public void setRuleset_id(int ruleset_id) {
		this.ruleset_id = ruleset_id;
	}

	/**
	 * @return the pattern_id
	 */
	public int getPattern_id() {
		return pattern_id;
	}

	/**
	 * @param pattern_id the pattern_id to set
	 */
	public void setPattern_id(int pattern_id) {
		this.pattern_id = pattern_id;
	}

	/**
	 * @return the items
	 */
	public String getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(String items) {
		this.items = items;
	}

	/**
	 * @return the pc_count
	 */
	public int getPc_count() {
		return pc_count;
	}

	/**
	 * @param pc_count the pc_count to set
	 */
	public void setPc_count(int pc_count) {
		this.pc_count = pc_count;
	}

	/**
	 * @return the eigen_vector
	 */
	public String getEigen_vector() {
		return eigen_vector;
	}

	/**
	 * @param eigen_vector the eigen_vector to set
	 */
	public void setEigen_vector(String eigen_vector) {
		this.eigen_vector = eigen_vector;
	}

	/**
	 * @return the eigen_value
	 */
	public String getEigen_value() {
		return eigen_value;
	}

	/**
	 * @param eigen_value the eigen_value to set
	 */
	public void setEigen_value(String eigen_value) {
		this.eigen_value = eigen_value;
	}

	/**
	 * @return the neigen_value
	 */
	public String getNeigen_value() {
		return neigen_value;
	}

	/**
	 * @param neigen_value the neigen_value to set
	 */
	public void setNeigen_value(String neigen_value) {
		this.neigen_value = neigen_value;
	}



} // end of class
