/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

/**
 * Factory information
 * 
 */
public class StckRecord {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String  fact_code = "";
	private  int     stck_code = 0;
	private  String  stck_name = "";
	private  int     stck_oxyg = -1;
	private  double  stck_mois = -1.0D;
		
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public StckRecord() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// Append information
		infoStrBuffer.append(String.format("fact_code      : %s\n",  this.fact_code));
		infoStrBuffer.append(String.format("stck_code      : %s\n",  this.stck_code));
		infoStrBuffer.append(String.format("stck_name      : %s\n",  this.stck_name));
		infoStrBuffer.append(String.format("stck_oxyg      : %d\n",  this.stck_oxyg));
		infoStrBuffer.append(String.format("stck_mois      : %5.3f", this.stck_mois));
		
		// return
		return infoStrBuffer.toString();

	} // end of method


	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the fact_code
	 */
	public String getFact_code() {
		return fact_code;
	}

	/**
	 * @param fact_code the fact_code to set
	 */
	public void setFact_code(String fact_code) {
		this.fact_code = fact_code;
	}

	/**
	 * @return the stck_code
	 */
	public int getStck_code() {
		return stck_code;
	}

	/**
	 * @param stck_code the stck_code to set
	 */
	public void setStck_code(int stck_code) {
		this.stck_code = stck_code;
	}

	/**
	 * @return the stck_name
	 */
	public String getStck_name() {
		return stck_name;
	}

	/**
	 * @param stck_name the stck_name to set
	 */
	public void setStck_name(String stck_name) {
		this.stck_name = stck_name;
	}

	/**
	 * @return the stck_oxyg
	 */
	public int getStck_oxyg() {
		return stck_oxyg;
	}

	/**
	 * @param stck_oxyg the stck_oxyg to set
	 */
	public void setStck_oxyg(int stck_oxyg) {
		this.stck_oxyg = stck_oxyg;
	}

	/**
	 * @return the stck_mois
	 */
	public double getStck_mois() {
		return stck_mois;
	}

	/**
	 * @param stck_mois the stck_mois to set
	 */
	public void setStck_mois(double stck_mois) {
		this.stck_mois = stck_mois;
	}
	

} // end of class
