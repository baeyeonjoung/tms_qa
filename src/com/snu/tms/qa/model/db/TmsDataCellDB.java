/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

import com.snu.tms.qa.model.ItemCodeDefine;

/**
 * Data Element for TMS Data
 * 
 */
public class TmsDataCellDB {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int       item_code   = ItemCodeDefine.UNDEFINED;
	private  String    min_time    = "";
	private  double    min_valu    = -1.0D;
	private  double    min_valu_bf = -1.0D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public TmsDataCellDB() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {
		return String.format("%10s    %s    %5.3f    %5.3f", 
				ItemCodeDefine.getItemName(item_code), min_time, min_valu_bf);
	} // end of method


	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the item_code
	 */
	public int getItem_code() {
		return item_code;
	}

	/**
	 * @param item_code the item_code to set
	 */
	public void setItem_code(int item_code) {
		this.item_code = item_code;
	}

	/**
	 * @return the min_time
	 */
	public String getMin_time() {
		return min_time;
	}

	/**
	 * @param min_time the min_time to set
	 */
	public void setMin_time(String min_time) {
		this.min_time = min_time;
	}

	/**
	 * @return the data_value
	 */
	public double getMin_valu() {
		return min_valu;
	}

	/**
	 * @param data_value the data_value to set
	 */
	public void setMin_valu(double min_valu) {
		this.min_valu = min_valu;
	}

	/**
	 * @return the min_valu_bf
	 */
	public double getMin_valu_bf() {
		return min_valu_bf;
	}

	/**
	 * @param min_valu_bf the min_valu_bf to set
	 */
	public void setMin_valu_bf(double min_valu_bf) {
		this.min_valu_bf = min_valu_bf;
	}


} // end of class
