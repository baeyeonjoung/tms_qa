/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

/**
 * Ruleset Model
 * 
 */
public class PatternRecord
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int      ruleset_id          = 0;
	private  int      pattern_id          = 0;
	private  String   pattern_name        = "";
	private  int      pattern_enable      = 0;
	private  int      ruleset_enable      = 0;
	private  int      operate_stop_state  = 0;
	private  int      item_count          = 0;
	private  String   items               = "";
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * constructor for RulesetModel
	 */
	public PatternRecord() {}
	
	public PatternRecord(
			int      ruleset_id,
			int      pattern_id,
			String   pattern_name,
			int      pattern_enable,
			int      ruleset_enable,
			int      operate_stop_state,
			int      item_count,
			String   items) {
		
		this.ruleset_id = ruleset_id;
		this.pattern_id = pattern_id;
		this.pattern_name = pattern_name;
		this.pattern_enable = pattern_enable;
		this.ruleset_enable = ruleset_enable;
		this.operate_stop_state = operate_stop_state;
		this.item_count = item_count;
		this.items = items;
		
	} // end of constructor
	

	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the ruleset_id
	 */
	public int getRuleset_id() {
		return ruleset_id;
	}

	/**
	 * @param ruleset_id the ruleset_id to set
	 */
	public void setRuleset_id(int ruleset_id) {
		this.ruleset_id = ruleset_id;
	}

	/**
	 * @return the pattern_id
	 */
	public int getPattern_id() {
		return pattern_id;
	}

	/**
	 * @param pattern_id the pattern_id to set
	 */
	public void setPattern_id(int pattern_id) {
		this.pattern_id = pattern_id;
	}

	/**
	 * @return the pattern_name
	 */
	public String getPattern_name() {
		return pattern_name;
	}

	/**
	 * @param pattern_name the pattern_name to set
	 */
	public void setPattern_name(String pattern_name) {
		this.pattern_name = pattern_name;
	}

	/**
	 * @return the pattern_enable
	 */
	public int getPattern_enable() {
		return pattern_enable;
	}

	/**
	 * @param pattern_enable the pattern_enable to set
	 */
	public void setPattern_enable(int pattern_enable) {
		this.pattern_enable = pattern_enable;
	}

	/**
	 * @return the ruleset_enable
	 */
	public int getRuleset_enable() {
		return ruleset_enable;
	}

	/**
	 * @param ruleset_enable the ruleset_enable to set
	 */
	public void setRuleset_enable(int ruleset_enable) {
		this.ruleset_enable = ruleset_enable;
	}

	/**
	 * @return the operate_stop_state
	 */
	public int getOperate_stop_state() {
		return operate_stop_state;
	}

	/**
	 * @param operate_stop_state the operate_stop_state to set
	 */
	public void setOperate_stop_state(int operate_stop_state) {
		this.operate_stop_state = operate_stop_state;
	}

	/**
	 * @return the item_count
	 */
	public int getItem_count() {
		return item_count;
	}

	/**
	 * @param item_count the item_count to set
	 */
	public void setItem_count(int item_count) {
		this.item_count = item_count;
	}

	/**
	 * @return the items
	 */
	public String getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(String items) {
		this.items = items;
	}


} // end of class
