/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

/**
 * Ruleset Model
 * 
 */
public class RulesetRecord
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int      ruleset_id     = 0;
	private  String   fact_code      = "";
	private  int      stck_code      = 0;
	private  int      ruleset_state  = 0;
	private  String   ruleset_name   = "";
	private  String   ruleset_descr  = "";
	private  String   user_id        = "";
	private  String   create_time    = "";
	private  String   start_time     = "";
	private  String   end_time       = "";
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * constructor for RulesetModel
	 */
	public RulesetRecord() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the ruleset_id
	 */
	public int getRuleset_id() {
		return ruleset_id;
	}

	/**
	 * @param ruleset_id the ruleset_id to set
	 */
	public void setRuleset_id(int ruleset_id) {
		this.ruleset_id = ruleset_id;
	}

	/**
	 * @return the fact_code
	 */
	public String getFact_code() {
		return fact_code;
	}

	/**
	 * @param fact_code the fact_code to set
	 */
	public void setFact_code(String fact_code) {
		this.fact_code = fact_code;
	}

	/**
	 * @return the stck_code
	 */
	public int getStck_code() {
		return stck_code;
	}

	/**
	 * @param stck_code the stck_code to set
	 */
	public void setStck_code(int stck_code) {
		this.stck_code = stck_code;
	}

	/**
	 * @return the ruleset_state
	 */
	public int getRuleset_state() {
		return ruleset_state;
	}

	/**
	 * @param ruleset_state the ruleset_state to set
	 */
	public void setRuleset_state(int ruleset_state) {
		this.ruleset_state = ruleset_state;
	}

	/**
	 * @return the ruleset_name
	 */
	public String getRuleset_name() {
		return ruleset_name;
	}

	/**
	 * @param ruleset_name the ruleset_name to set
	 */
	public void setRuleset_name(String ruleset_name) {
		this.ruleset_name = ruleset_name;
	}

	/**
	 * @return the ruleset_descr
	 */
	public String getRuleset_descr() {
		return ruleset_descr;
	}

	/**
	 * @param ruleset_descr the ruleset_descr to set
	 */
	public void setRuleset_descr(String ruleset_descr) {
		this.ruleset_descr = ruleset_descr;
	}

	/**
	 * @return the user_id
	 */
	public String getUser_id() {
		return user_id;
	}

	/**
	 * @param user_id the user_id to set
	 */
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	/**
	 * @return the create_time
	 */
	public String getCreate_time() {
		return create_time;
	}

	/**
	 * @param create_time the create_time to set
	 */
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	/**
	 * @return the start_time
	 */
	public String getStart_time() {
		return start_time;
	}

	/**
	 * @param start_time the start_time to set
	 */
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	/**
	 * @return the end_time
	 */
	public String getEnd_time() {
		return end_time;
	}

	/**
	 * @param end_time the end_time to set
	 */
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}


} // end of class
