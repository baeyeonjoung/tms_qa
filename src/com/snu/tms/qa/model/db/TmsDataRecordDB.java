/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

import com.snu.tms.qa.model.ItemCodeDefine;

/**
 * Data Element for TMS Data
 * 
 */
public class TmsDataRecordDB {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String  fact_code  = "";
	private  int     stck_code  = -1;
	private  int     item_code = ItemCodeDefine.UNDEFINED;
	private  String  start_time_str = "";  // yyyyMMddHHmm
	private  String  end_time_str = "";  // yyyyMMddHHmm
			
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public TmsDataRecordDB() {}


	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the fact_code
	 */
	public String getFact_code() {
		return fact_code;
	}

	/**
	 * @param fact_code the fact_code to set
	 */
	public void setFact_code(String fact_code) {
		this.fact_code = fact_code;
	}

	/**
	 * @return the stck_code
	 */
	public int getStck_code() {
		return stck_code;
	}

	/**
	 * @param stck_code the stck_code to set
	 */
	public void setStck_code(int stck_code) {
		this.stck_code = stck_code;
	}

	/**
	 * @return the item_code
	 */
	public int getItem_code() {
		return item_code;
	}

	/**
	 * @param item_code the item_code to set
	 */
	public void setItem_code(int item_code) {
		this.item_code = item_code;
	}


	/**
	 * @return the start_time_str
	 */
	public String getStart_time_str() {
		return start_time_str;
	}


	/**
	 * @param start_time_str the start_time_str to set
	 */
	public void setStart_time_str(String start_time_str) {
		this.start_time_str = start_time_str;
	}


	/**
	 * @return the end_time_str
	 */
	public String getEnd_time_str() {
		return end_time_str;
	}


	/**
	 * @param end_time_str the end_time_str to set
	 */
	public void setEnd_time_str(String end_time_str) {
		this.end_time_str = end_time_str;
	}


} // end of class
