/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

/**
 * Factory information
 * 
 */
public class StckClassRecord {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String  fact_code   = "";
	private  int     stck_code   = 0;
	private  String  stck_desc   = "";
	private  String  proc_desc   = "";
	private  String  proc_class1 = "";
	private  String  proc_class2 = "";
	private  String  fuel_name   = "";
		
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for StckClassRecord
	 */
	public StckClassRecord() {}


	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the fact_code
	 */
	public String getFact_code() {
		return fact_code;
	}

	/**
	 * @param fact_code the fact_code to set
	 */
	public void setFact_code(String fact_code) {
		this.fact_code = fact_code;
	}

	/**
	 * @return the stck_code
	 */
	public int getStck_code() {
		return stck_code;
	}

	/**
	 * @param stck_code the stck_code to set
	 */
	public void setStck_code(int stck_code) {
		this.stck_code = stck_code;
	}

	/**
	 * @return the stck_desc
	 */
	public String getStck_desc() {
		return stck_desc;
	}

	/**
	 * @param stck_desc the stck_desc to set
	 */
	public void setStck_desc(String stck_desc) {
		this.stck_desc = stck_desc;
	}

	/**
	 * @return the proc_desc
	 */
	public String getProc_desc() {
		return proc_desc;
	}

	/**
	 * @param proc_desc the proc_desc to set
	 */
	public void setProc_desc(String proc_desc) {
		this.proc_desc = proc_desc;
	}

	/**
	 * @return the proc_class1
	 */
	public String getProc_class1() {
		return proc_class1;
	}

	/**
	 * @param proc_class1 the proc_class1 to set
	 */
	public void setProc_class1(String proc_class1) {
		this.proc_class1 = proc_class1;
	}

	/**
	 * @return the proc_class2
	 */
	public String getProc_class2() {
		return proc_class2;
	}

	/**
	 * @param proc_class2 the proc_class2 to set
	 */
	public void setProc_class2(String proc_class2) {
		this.proc_class2 = proc_class2;
	}

	/**
	 * @return the fuel_name
	 */
	public String getFuel_name() {
		return fuel_name;
	}

	/**
	 * @param fuel_name the fuel_name to set
	 */
	public void setFuel_name(String fuel_name) {
		this.fuel_name = fuel_name;
	}


} // end of class
