/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

/**
 * Ruleset Model
 * 
 */
public class PatternItemRecord
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int      ruleset_id          = 0;
	private  int      pattern_id          = 0;
	private  int      item_code           = 0;
	private  String   item_name           = "";
	private  double   min_range           = 0.0D;
	private  double   max_range           = 0.0D;
	private  int      bin_count           = 0;
	private  int      bin_interval        = 0;
	private  String   density             = "";
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * constructor for RulesetModel
	 */
	public PatternItemRecord() {}
	
	public PatternItemRecord(
			int      ruleset_id,
			int      pattern_id,
			int      item_code,
			String   item_name,
			double   min_range,
			double   max_range,
			int      bin_count,
			int      bin_interval,
			String   density) {
		
		this.ruleset_id   = ruleset_id;
		this.pattern_id   = pattern_id;
		this.item_code    = item_code;
		this.item_name    = item_name;
		this.min_range    = min_range;
		this.max_range    = max_range;
		this.bin_count    = bin_count;
		this.bin_interval = bin_interval;
		this.density      = density;
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the ruleset_id
	 */
	public int getRuleset_id() {
		return ruleset_id;
	}

	/**
	 * @param ruleset_id the ruleset_id to set
	 */
	public void setRuleset_id(int ruleset_id) {
		this.ruleset_id = ruleset_id;
	}

	/**
	 * @return the pattern_id
	 */
	public int getPattern_id() {
		return pattern_id;
	}

	/**
	 * @param pattern_id the pattern_id to set
	 */
	public void setPattern_id(int pattern_id) {
		this.pattern_id = pattern_id;
	}

	/**
	 * @return the item_code
	 */
	public int getItem_code() {
		return item_code;
	}

	/**
	 * @param item_code the item_code to set
	 */
	public void setItem_code(int item_code) {
		this.item_code = item_code;
	}

	/**
	 * @return the item_name
	 */
	public String getItem_name() {
		return item_name;
	}

	/**
	 * @param item_name the item_name to set
	 */
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	/**
	 * @return the min_range
	 */
	public double getMin_range() {
		return min_range;
	}

	/**
	 * @param min_range the min_range to set
	 */
	public void setMin_range(double min_range) {
		this.min_range = min_range;
	}

	/**
	 * @return the max_range
	 */
	public double getMax_range() {
		return max_range;
	}

	/**
	 * @param max_range the max_range to set
	 */
	public void setMax_range(double max_range) {
		this.max_range = max_range;
	}

	/**
	 * @return the bin_count
	 */
	public int getBin_count() {
		return bin_count;
	}

	/**
	 * @param bin_count the bin_count to set
	 */
	public void setBin_count(int bin_count) {
		this.bin_count = bin_count;
	}

	/**
	 * @return the bin_interval
	 */
	public int getBin_interval() {
		return bin_interval;
	}

	/**
	 * @param bin_interval the bin_interval to set
	 */
	public void setBin_interval(int bin_interval) {
		this.bin_interval = bin_interval;
	}

	/**
	 * @return the density
	 */
	public String getDensity() {
		return density;
	}

	/**
	 * @param density the density to set
	 */
	public void setDensity(String density) {
		this.density = density;
	}
	


} // end of class
