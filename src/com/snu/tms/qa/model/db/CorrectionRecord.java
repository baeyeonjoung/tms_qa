/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

/**
 * Data for each item have to corrected for each fact_code+stck_code
 * 
 */
public class CorrectionRecord {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String    fact_code = "";
	private  int       stck_code = -1;
	private  int       item_code = 0;
	private  double    std_oxgy  = 0.0D;
	private  String    oxyg_corr = "N";
	private  String    temp_corr = "N";
	private  String    mois_corr = "N";
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for CorrectionItem
	 */
	public CorrectionRecord() {}
	

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		String info = String.format(
				  "fact_code(%5s), stck_code(%3d), item_code(%2d), item_name(%4s), "
				+ "oxyg_corr(%3s), temp_corr(%3s), mois_corr(%3s)", 
				this.fact_code, 
				this.stck_code, 
				this.item_code, 
				this.item_code,
				this.std_oxgy,
				(this.oxyg_corr.equals("N"))?"OFF":"ON", 
				(this.temp_corr.equals("N"))?"OFF":"ON", 
				(this.mois_corr.equals("N"))?"OFF":"ON");
		return info;

	} // end of method


	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the fact_code
	 */
	public String getFact_code() {
		return fact_code;
	}


	/**
	 * @param fact_code the fact_code to set
	 */
	public void setFact_code(String fact_code) {
		this.fact_code = fact_code;
	}


	/**
	 * @return the stck_code
	 */
	public int getStck_code() {
		return stck_code;
	}


	/**
	 * @param stck_code the stck_code to set
	 */
	public void setStck_code(int stck_code) {
		this.stck_code = stck_code;
	}


	/**
	 * @return the item_code
	 */
	public int getItem_code() {
		return item_code;
	}


	/**
	 * @param item_code the item_code to set
	 */
	public void setItem_code(int item_code) {
		this.item_code = item_code;
	}


	/**
	 * @return the oxyg_corr
	 */
	public String getOxyg_corr() {
		return oxyg_corr;
	}


	/**
	 * @param oxyg_corr the oxyg_corr to set
	 */
	public void setOxyg_corr(String oxyg_corr) {
		this.oxyg_corr = oxyg_corr;
	}


	/**
	 * @return the temp_corr
	 */
	public String getTemp_corr() {
		return temp_corr;
	}


	/**
	 * @param temp_corr the temp_corr to set
	 */
	public void setTemp_corr(String temp_corr) {
		this.temp_corr = temp_corr;
	}


	/**
	 * @return the mois_corr
	 */
	public String getMois_corr() {
		return mois_corr;
	}


	/**
	 * @param mois_corr the mois_corr to set
	 */
	public void setMois_corr(String mois_corr) {
		this.mois_corr = mois_corr;
	}


	/**
	 * @return the std_oxgy
	 */
	public double getStd_oxgy() {
		return std_oxgy;
	}


	/**
	 * @param std_oxgy the std_oxgy to set
	 */
	public void setStd_oxgy(double std_oxgy) {
		this.std_oxgy = std_oxgy;
	}



} // end of class
