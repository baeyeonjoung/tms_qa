/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

/**
 * Stack Key information
 * 
 */
public class StackKeyRecord {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String  fact_code = "";
	private  int     stck_code = 0;
		
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for StackKeyRecord
	 */
	public StackKeyRecord(String factCode, int stckCode) {
		
		this.fact_code = factCode;
		this.stck_code = stckCode;
		
	} // end of constructor

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the fact_code
	 */
	public String getFact_code() {
		return fact_code;
	}

	/**
	 * @param fact_code the fact_code to set
	 */
	public void setFact_code(String fact_code) {
		this.fact_code = fact_code;
	}

	/**
	 * @return the stck_code
	 */
	public int getStck_code() {
		return stck_code;
	}

	/**
	 * @param stck_code the stck_code to set
	 */
	public void setStck_code(int stck_code) {
		this.stck_code = stck_code;
	}

} // end of class
