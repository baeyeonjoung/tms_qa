/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

/**
 * 방지시설 데이터베이스 정보
 * 
 */
public class PreventionRecord {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String   fact_code     = "";
	private  int      stck_code     = 0;
	private  String   exhstCode     = "";
	private  String   prvnName      = "";
	private  String   prvnCapacity  = "";
		
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for ExhaustRecord
	 */
	public PreventionRecord() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the fact_code
	 */
	public String getFact_code() {
		return fact_code;
	}

	/**
	 * @param fact_code the fact_code to set
	 */
	public void setFact_code(String fact_code) {
		this.fact_code = fact_code;
	}

	/**
	 * @return the stck_code
	 */
	public int getStck_code() {
		return stck_code;
	}

	/**
	 * @param stck_code the stck_code to set
	 */
	public void setStck_code(int stck_code) {
		this.stck_code = stck_code;
	}

	/**
	 * @return the exhstCode
	 */
	public String getExhstCode() {
		return exhstCode;
	}

	/**
	 * @param exhstCode the exhstCode to set
	 */
	public void setExhstCode(String exhstCode) {
		this.exhstCode = exhstCode;
	}

	/**
	 * @return the prvnName
	 */
	public String getPrvnName() {
		return prvnName;
	}

	/**
	 * @param prvnName the prvnName to set
	 */
	public void setPrvnName(String prvnName) {
		this.prvnName = prvnName;
	}

	/**
	 * @return the prvnCapacity
	 */
	public String getPrvnCapacity() {
		return prvnCapacity;
	}

	/**
	 * @param prvnCapacity the prvnCapacity to set
	 */
	public void setPrvnCapacity(String prvnCapacity) {
		this.prvnCapacity = prvnCapacity;
	}




} // end of class
