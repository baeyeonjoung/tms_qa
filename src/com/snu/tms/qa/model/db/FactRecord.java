/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

/**
 * Factory information
 * 
 */
public class FactRecord {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String  fact_code = "";
	private  String  fact_nams = "";
	private  String  fact_naml = "";
	private  String  fact_namt = "";
	private  String  fact_addr = "";
	private  String  fact_prod = "";
	private  String  fact_numb = "";
	private  String  area_cod2 = "";
		
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public FactRecord() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// Append information
		infoStrBuffer.append(String.format("fact_code      : %s\n", this.fact_code));
		infoStrBuffer.append(String.format("fact_nams      : %s\n", this.fact_nams));
		infoStrBuffer.append(String.format("fact_naml      : %s\n", this.fact_naml));
		infoStrBuffer.append(String.format("fact_namt      : %s\n", this.fact_namt));
		infoStrBuffer.append(String.format("fact_addr      : %s\n", this.fact_addr));
		infoStrBuffer.append(String.format("fact_prod      : %s\n", this.fact_prod));
		infoStrBuffer.append(String.format("fact_numb      : %s\n", this.fact_numb));
		infoStrBuffer.append(String.format("area_cod2      : %s",   this.area_cod2));
		
		// return
		return infoStrBuffer.toString();

	} // end of method


	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the fact_code
	 */
	public String getFact_code() {
		return fact_code;
	}

	/**
	 * @param fact_code the fact_code to set
	 */
	public void setFact_code(String fact_code) {
		this.fact_code = fact_code;
	}

	/**
	 * @return the fact_nama
	 */
	public String getFact_nams() {
		return fact_nams;
	}

	/**
	 * @param fact_nama the fact_nama to set
	 */
	public void setFact_nams(String fact_nams) {
		this.fact_nams = fact_nams;
	}

	/**
	 * @return the fact_naml
	 */
	public String getFact_naml() {
		return fact_naml;
	}

	/**
	 * @param fact_naml the fact_naml to set
	 */
	public void setFact_naml(String fact_naml) {
		this.fact_naml = fact_naml;
	}

	/**
	 * @return the fact_namt
	 */
	public String getFact_namt() {
		return fact_namt;
	}

	/**
	 * @param fact_namt the fact_namt to set
	 */
	public void setFact_namt(String fact_namt) {
		this.fact_namt = fact_namt;
	}

	/**
	 * @return the fact_addr
	 */
	public String getFact_addr() {
		return fact_addr;
	}

	/**
	 * @param fact_addr the fact_addr to set
	 */
	public void setFact_addr(String fact_addr) {
		this.fact_addr = fact_addr;
	}

	/**
	 * @return the fact_prod
	 */
	public String getFact_prod() {
		return fact_prod;
	}

	/**
	 * @param fact_prod the fact_prod to set
	 */
	public void setFact_prod(String fact_prod) {
		this.fact_prod = fact_prod;
	}

	/**
	 * @return the fact_numb
	 */
	public String getFact_numb() {
		return fact_numb;
	}

	/**
	 * @param fact_numb the fact_numb to set
	 */
	public void setFact_numb(String fact_numb) {
		this.fact_numb = fact_numb;
	}

	/**
	 * @return the area_cod2
	 */
	public String getArea_cod2() {
		return area_cod2;
	}

	/**
	 * @param area_cod2 the area_cod2 to set
	 */
	public void setArea_cod2(String area_cod2) {
		this.area_cod2 = area_cod2;
	}


} // end of class
