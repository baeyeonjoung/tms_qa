/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

/**
 * 배출시설 데이터베이스 정보
 * 
 */
public class ExhaustRecord {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String   fact_code     = "";
	private  int      stck_code     = 0;
	private  String   exhstCode     = "";
	private  String   exhstName     = "";
	private  String   exhstCapacity = "";
		
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for ExhaustRecord
	 */
	public ExhaustRecord() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the fact_code
	 */
	public String getFact_code() {
		return fact_code;
	}

	/**
	 * @param fact_code the fact_code to set
	 */
	public void setFact_code(String fact_code) {
		this.fact_code = fact_code;
	}

	/**
	 * @return the stck_code
	 */
	public int getStck_code() {
		return stck_code;
	}

	/**
	 * @param stck_code the stck_code to set
	 */
	public void setStck_code(int stck_code) {
		this.stck_code = stck_code;
	}

	/**
	 * @return the exhstCode
	 */
	public String getExhstCode() {
		return exhstCode;
	}

	/**
	 * @param exhstCode the exhstCode to set
	 */
	public void setExhstCode(String exhstCode) {
		this.exhstCode = exhstCode;
	}

	/**
	 * @return the exhstName
	 */
	public String getExhstName() {
		return exhstName;
	}

	/**
	 * @param exhstName the exhstName to set
	 */
	public void setExhstName(String exhstName) {
		this.exhstName = exhstName;
	}

	/**
	 * @return the exhstCapacity
	 */
	public String getExhstCapacity() {
		return exhstCapacity;
	}

	/**
	 * @param exhstCapacity the exhstCapacity to set
	 */
	public void setExhstCapacity(String exhstCapacity) {
		this.exhstCapacity = exhstCapacity;
	}


} // end of class
