/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.db;

/**
 * Data for each item have to corrected for each fact_code+stck_code
 * 
 */
public class RulesetCorrectionRecord {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int       ruleset_id  = 0;
	private  int       item_code   = 0;
	private  double    std_oxgy    = 0.0D;
	private  double    std_mois    = 0.0D;
	
	private  int       oxyg_corr   = 0;
	private  int       temp_corr   = 0;
	private  int       mois_corr   = 0;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for CorrectionItem
	 */
	public RulesetCorrectionRecord() {}
	
	public RulesetCorrectionRecord(
			int     ruleset_id,
			int     item_code,
			double  std_oxgy,
			double  std_mois,
			int     oxyg_corr,
			int     temp_corr,
			int     mois_corr) {
		
		this.ruleset_id = ruleset_id;
		this.item_code  = item_code;
		this.std_oxgy   = std_oxgy;
		this.std_mois   = std_mois;
		this.oxyg_corr  = oxyg_corr;
		this.temp_corr  = temp_corr;
		this.mois_corr  = mois_corr;
		
	} // end of constructor
	
	public RulesetCorrectionRecord(
			int     ruleset_id,
			int     item_code,
			double  std_oxgy,
			double  std_mois,
			boolean oxyg_corr,
			boolean temp_corr,
			boolean mois_corr) {
		
		this.ruleset_id = ruleset_id;
		this.item_code  = item_code;
		this.std_oxgy   = std_oxgy;
		this.std_mois   = std_mois;
		if (oxyg_corr) {
			this.oxyg_corr  = 1;
		} else {
			this.oxyg_corr  = 0;
		}
		if (temp_corr) {
			this.temp_corr  = 1;
		} else {
			this.temp_corr  = 0;
		}
		if (mois_corr) {
			this.mois_corr  = 1;
		} else {
			this.mois_corr  = 0;
		}
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the ruleset_id
	 */
	public int getRuleset_id() {
		return ruleset_id;
	}

	/**
	 * @param ruleset_id the ruleset_id to set
	 */
	public void setRuleset_id(int ruleset_id) {
		this.ruleset_id = ruleset_id;
	}

	/**
	 * @return the item_code
	 */
	public int getItem_code() {
		return item_code;
	}

	/**
	 * @param item_code the item_code to set
	 */
	public void setItem_code(int item_code) {
		this.item_code = item_code;
	}

	/**
	 * @return the std_oxgy
	 */
	public double getStd_oxgy() {
		return std_oxgy;
	}

	/**
	 * @param std_oxgy the std_oxgy to set
	 */
	public void setStd_oxgy(double std_oxgy) {
		this.std_oxgy = std_oxgy;
	}

	/**
	 * @return the std_mois
	 */
	public double getStd_mois() {
		return std_mois;
	}

	/**
	 * @param std_mois the std_mois to set
	 */
	public void setStd_mois(double std_mois) {
		this.std_mois = std_mois;
	}

	/**
	 * @return the oxyg_corr
	 */
	public int getOxyg_corr() {
		return oxyg_corr;
	}

	/**
	 * @param oxyg_corr the oxyg_corr to set
	 */
	public void setOxyg_corr(int oxyg_corr) {
		this.oxyg_corr = oxyg_corr;
	}

	/**
	 * @return the temp_corr
	 */
	public int getTemp_corr() {
		return temp_corr;
	}

	/**
	 * @param temp_corr the temp_corr to set
	 */
	public void setTemp_corr(int temp_corr) {
		this.temp_corr = temp_corr;
	}

	/**
	 * @return the mois_corr
	 */
	public int getMois_corr() {
		return mois_corr;
	}

	/**
	 * @param mois_corr the mois_corr to set
	 */
	public void setMois_corr(int mois_corr) {
		this.mois_corr = mois_corr;
	}


} // end of class
