/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

/**
 * Data Element for TMS Data
 * 
 */
public class TmsDataSingleItemRecord {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String    timestr = "";
	private  long      time = 0L;
	private  int       item = ItemCodeDefine.UNDEFINED;
	
	private  TmsDataHeader   tmsDataHeader = null;
	
	private  TmsDataCell  tmsDataCell  = null;
		
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public TmsDataSingleItemRecord(int item, TmsDataRecord tmsDataRecord) {
		this.item = item;
		this.timestr = tmsDataRecord.getTimestr();
		this.time = tmsDataRecord.getTime();
		this.tmsDataHeader = tmsDataRecord.getTmsDataHeader();
		this.tmsDataCell = tmsDataRecord.getDataCell(item);
	}
	
	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the timestr
	 */
	public String getTimestr() {
		return timestr;
	}

	/**
	 * @param timestr the timestr to set
	 */
	public void setTimestr(String timestr) {
		this.timestr = timestr;
	}

	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}

	/**
	 * @return the item
	 */
	public int getItem() {
		return item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(int item) {
		this.item = item;
	}

	/**
	 * @return the tmsDataHeader
	 */
	public TmsDataHeader getTmsDataHeader() {
		return tmsDataHeader;
	}

	/**
	 * @param tmsDataHeader the tmsDataHeader to set
	 */
	public void setTmsDataHeader(TmsDataHeader tmsDataHeader) {
		this.tmsDataHeader = tmsDataHeader;
	}

	/**
	 * @return the tmsDataCell
	 */
	public TmsDataCell getTmsDataCell() {
		return tmsDataCell;
	}

	/**
	 * @param tmsDataCell the tmsDataCell to set
	 */
	public void setTmsDataCell(TmsDataCell tmsDataCell) {
		this.tmsDataCell = tmsDataCell;
	}


} // end of class
