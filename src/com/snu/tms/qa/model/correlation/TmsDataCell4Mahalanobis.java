/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.correlation;

import com.snu.tms.qa.model.ItemCodeDefine;

/**
 * Data Element for TMS Data
 * 
 */
public class TmsDataCell4Mahalanobis  {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  long      time = 0L;
	
	private  int       data1_item = ItemCodeDefine.UNDEFINED;
	private  double    data1_value = -1.0D;
	
	private  int       data2_item = ItemCodeDefine.UNDEFINED;
	private  double    data2_value = -1.0D;
	
	private  double    mahalanobisDistance = 0.0D;
	private  double    imverseChiSquare = 0.0D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public TmsDataCell4Mahalanobis(
			long    time,
			int     data1_item,
			double  data1_value,
			int     data2_item,
			double  data2_value) {
		
		this.time = time;
		this.data1_item = data1_item;
		this.data1_value = data1_value;
		this.data2_item = data2_item;
		this.data2_value = data2_value;
		
	} // end of constructor

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}

	/**
	 * @return the data1_item
	 */
	public int getData1_item() {
		return data1_item;
	}

	/**
	 * @param data1_item the data1_item to set
	 */
	public void setData1_item(int data1_item) {
		this.data1_item = data1_item;
	}

	/**
	 * @return the data1_value
	 */
	public double getData1_value() {
		return data1_value;
	}

	/**
	 * @param data1_value the data1_value to set
	 */
	public void setData1_value(double data1_value) {
		this.data1_value = data1_value;
	}

	/**
	 * @return the data2_item
	 */
	public int getData2_item() {
		return data2_item;
	}

	/**
	 * @param data2_item the data2_item to set
	 */
	public void setData2_item(int data2_item) {
		this.data2_item = data2_item;
	}

	/**
	 * @return the data2_value
	 */
	public double getData2_value() {
		return data2_value;
	}

	/**
	 * @param data2_value the data2_value to set
	 */
	public void setData2_value(double data2_value) {
		this.data2_value = data2_value;
	}

	/**
	 * @return the mahalanobisDistance
	 */
	public double getMahalanobisDistance() {
		return mahalanobisDistance;
	}

	/**
	 * @param mahalanobisDistance the mahalanobisDistance to set
	 */
	public void setMahalanobisDistance(double mahalanobisDistance) {
		this.mahalanobisDistance = mahalanobisDistance;
	}

	/**
	 * @return the imverseChiSquare
	 */
	public double getImverseChiSquare() {
		return imverseChiSquare;
	}

	/**
	 * @param imverseChiSquare the imverseChiSquare to set
	 */
	public void setImverseChiSquare(double imverseChiSquare) {
		this.imverseChiSquare = imverseChiSquare;
	}


} // end of class
