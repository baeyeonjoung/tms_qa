/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

/**
 * Pattern Data Set
 * 
 */
public class OdrCell {

	//==========================================================================
	// Static Fields
	//==========================================================================
	public final static int MULTIVARIATE  = 0;
	public final static int BIVARIATE     = 1;
	public final static int UNIVARIATE    = 2;
	public final static int CORRECTION    = 3;
	
	//==========================================================================
	// Fields
	//==========================================================================
	public  String  ruleTypeName = "";
	public  int     ruleType     = MULTIVARIATE;
	public  int     isOutlier    = 0;
	
	public  RulesetWeightingModel rulesetWeighingModel;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	public OdrCell(String ruleTypeName, RulesetWeightingModel rulesetWeighingModel) {
		
		this.rulesetWeighingModel = rulesetWeighingModel;
		
		this.ruleTypeName = ruleTypeName;
		if (ruleTypeName.startsWith("TSS")) {
			this.ruleType = MULTIVARIATE;
		} else if (ruleTypeName.startsWith("TSS")) {
			this.ruleType = MULTIVARIATE;
		} else if (ruleTypeName.startsWith("SPE")) {
			this.ruleType = MULTIVARIATE;
		} else if (ruleTypeName.startsWith("CORR")) {
			this.ruleType = BIVARIATE;
		} else if (ruleTypeName.startsWith("SPC")) {
			this.ruleType = UNIVARIATE;
		} else if (ruleTypeName.startsWith("PD")) {
			this.ruleType = CORRECTION;
		} 
		
	} // end of constructor

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * set isOutlier
	 */
	public void setIsOutlier(boolean isOutlier) {
		
		if (isOutlier) {
			this.isOutlier = 1;
		} else {
			this.isOutlier = 0;
		}
	
	} // end of method
	
	/**
	 * get outlier Score
	 */
	public int getScore() {
		
		int score = 0;
		if (this.ruleType == MULTIVARIATE) {
			score = this.isOutlier * rulesetWeighingModel.getMultivariateRulesetWeighting();
		} else if (this.ruleType == BIVARIATE) {
			score = this.isOutlier * rulesetWeighingModel.getBivariateRulesetWeighting();
		} else if (this.ruleType == UNIVARIATE) {
			score = this.isOutlier * rulesetWeighingModel.getUnivariateRulesetWeighting();
		} else if (this.ruleType == CORRECTION) {
			score = this.isOutlier * rulesetWeighingModel.getCorrectionRulesetWeighting();
		} else {
			score = this.isOutlier;
		}
		return score;
		
	} // end of method
	
} // end of class
