/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import java.util.Vector;

/**
 * Save Outlier detection result in PCA analysis
 * 
 */
public class OutlierPcaData {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private int      dataCount = 0;
	
	private double[] tssValues;
	private double   tssThreshold = 0.0D;
	private double   tssConfidenceLevel = 0.95D;
	
	private double[] speValues;
	private double   speThreshold = 0.0D;
	private double   speConfidenceLevel = 0.95D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Save Outlier detection result in PCA analysis
	 */
	public OutlierPcaData() {}
	
	public OutlierPcaData(
			int      dataCount,
			double[] tssValues,
			double   tssThreshold,
			double   tssConfidenceLevel,
			double[] speValues,
			double   speThreshold,
			double   speConfidenceLevel) {
		
		this.dataCount = dataCount;
		this.tssValues = tssValues;
		this.tssThreshold = tssThreshold;
		this.tssConfidenceLevel = tssConfidenceLevel;
		this.speValues = speValues;
		this.speThreshold = speThreshold;
		this.speConfidenceLevel = speConfidenceLevel;
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// return
		return infoStrBuffer.toString();

	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Is Outlier
	 */
	public boolean isOutlier(int index) {
		
		if (index < 0 || index >= this.tssValues.length || index >= this.speValues.length) {
			return false;
		} 
		if (this.tssValues[index] > this.tssThreshold || this.speValues[index] > this.speThreshold) {
			return true;
		}
		return false;
		
	} // end of method
	
	/**
	 * @return the dataCount
	 */
	public int getDataCount() {
		return dataCount;
	}

	/**
	 * @param dataCount the dataCount to set
	 */
	public void setDataCount(int dataCount) {
		this.dataCount = dataCount;
	}

	/**
	 * @return the tssValues
	 */
	public double[] getTssValues() {
		return tssValues;
	}
	
	/**
	 * @return the tssValue
	 */
	public double getTssValue(int index) {
		return tssValues[index];
	}

	/**
	 * @param tssValues the tssValues to set
	 */
	public void setTssValues(double[] tssValues) {
		this.tssValues = tssValues;
	}

	/**
	 * @return the tssThreshold
	 */
	public double getTssThreshold() {
		return tssThreshold;
	}

	/**
	 * @param tssThreshold the tssThreshold to set
	 */
	public void setTssThreshold(double tssThreshold) {
		this.tssThreshold = tssThreshold;
	}

	/**
	 * @return the tssConfidenceLevel
	 */
	public double getTssConfidenceLevel() {
		return tssConfidenceLevel;
	}

	/**
	 * @param tssConfidenceLevel the tssConfidenceLevel to set
	 */
	public void setTssConfidenceLevel(double tssConfidenceLevel) {
		this.tssConfidenceLevel = tssConfidenceLevel;
	}

	/**
	 * @return the speValues
	 */
	public double[] getSpeValues() {
		return speValues;
	}

	/**
	 * @return the speValue
	 */
	public double getSpeValue(int index) {
		return speValues[index];
	}

	/**
	 * @param speValues the speValues to set
	 */
	public void setSpeValues(double[] speValues) {
		this.speValues = speValues;
	}

	/**
	 * @return the speThreshold
	 */
	public double getSpeThreshold() {
		return speThreshold;
	}
	
	/**
	 * @param speThreshold the speThreshold to set
	 */
	public void setSpeThreshold(double speThreshold) {
		this.speThreshold = speThreshold;
	}

	/**
	 * @return the speConfidenceLevel
	 */
	public double getSpeConfidenceLevel() {
		return speConfidenceLevel;
	}

	/**
	 * @param speConfidenceLevel the speConfidenceLevel to set
	 */
	public void setSpeConfidenceLevel(double speConfidenceLevel) {
		this.speConfidenceLevel = speConfidenceLevel;
	}

	
} // end of class
