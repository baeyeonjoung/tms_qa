/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : Apil 3, 2016
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.snu.tms.qa.model.PatternDataHistogram;


/**
 * Management for outlier ruleset and outlier detection data
 * 
 */
public class DetectionPatternDataset_OLD {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	// pattern Id
	private  int       patternId = 0;
	private  String    patternName = "";
	private  boolean   isEnabled = true;
	private  boolean   isRulesetCreated = false;

	// TMS monitoring items
	private  int[]     items;
	
	// target factory and stack
	private  String    factoryId = "";
	private  int       stackId = 0;
	private  String    factoryName = "";
	private  String    stackName = "";
	
	// result of PC analysis
	private  PcaResultModel  pcaResultModel;
	
	// Pattern data histogram
	private  PatternDataHistogram[]     patternDataHistograms = null;
	private  Hashtable<Integer, PatternDataHistogram>  patternDataHistogramHash = null;

	
	// Outlier ruleset management
	private Vector<DetectionRuleset> rulesetRepository = 
			new Vector<DetectionRuleset>();
	
	// outlier detection result
	private Vector<DetectionRecord> detectionRecords = 
			new Vector<DetectionRecord>();
	private Hashtable<Long, DetectionRecord> detectionRecordTimeHash = 
			new Hashtable<Long, DetectionRecord>();
	
	// ruleset base date 
	private  long      rulsetBaseStartTime = 0L;
	private  long      rulsetBaseEndTime   = 0L;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Management for outlier ruleset and outlier detection data
	 */
	public DetectionPatternDataset_OLD(
			int     patternId,
			String  patternName) {
		
		// set parameters
		this.patternId = patternId;
		this.patternName = patternName;
	
	} // end of constructor
	
	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Set Ruleset
	 */
	public void addRuleset(DetectionRuleset ruleset) {
		
		// add ruleset
		this.rulesetRepository.add(ruleset);
		
		// set DetectionDataCell
		DetectionRecord record;
		DetectionDataCell dataCell;
		for (Enumeration<DetectionRecord> e=this.detectionRecords.elements() ; e.hasMoreElements() ;) {
			record = e.nextElement();
			dataCell = record.addDetectionDataCell(ruleset);
			if (ruleset instanceof RulesetPd) {
				RawDataCell rawDataCell = record.getRawDataCell(ruleset.getItems())[0];
				dataCell.setResultValue(ruleset.applyRuleset(
						new double[] {
								rawDataCell.getData_value(),
								rawDataCell.getOrigin_value()}));
			} else {
				dataCell.setResultValue(ruleset.applyRuleset(record.getRawDataCellValue(ruleset.getItems())));
			}
		}
		
	} // end of method
	
	/**
	 * re-compute outlier with new threshold
	 */
	public void recomputeOutlier(DetectionRuleset ruleset) {
		
		DetectionRecord record;
		DetectionDataCell dataCell;
		for (Enumeration<DetectionRecord> e=this.detectionRecords.elements() ; e.hasMoreElements() ;) {
			record = e.nextElement();
			dataCell = record.getDetectionDataCell(ruleset);
			if (dataCell != null) {
				dataCell.checkOutlier();
			}
		}
		
	} // end of method
	
	/**
	 * @return the pcaResultModel
	 */
	public PcaResultModel getPcaResultModel() {
		return pcaResultModel;
	}

	/**
	 * @param pcaResultModel the pcaResultModel to set
	 */
	public void setPcaResultModel(PcaResultModel pcaResultModel) {
		this.pcaResultModel = pcaResultModel;
	}

	/**
	 * Get ruleset
	 */
	public DetectionRuleset getRuleset(DetectionRuleset ruleset) {
		
		DetectionRuleset rulesetItem;
		for (Enumeration<DetectionRuleset> e=this.rulesetRepository.elements() ; e.hasMoreElements() ;) {
			rulesetItem = e.nextElement();
			if (rulesetItem.isSameRuleset(ruleset)) {
				return rulesetItem;
			} else {
				continue;
			}
		}
		return null;
		
	} // end of method
	
	/**
	 * Get ruleset
	 */
	public DetectionRuleset[] getAllRuleset() {
		return this.rulesetRepository.toArray(new DetectionRuleset[this.rulesetRepository.size()]);		
	} // end of method
	
	/**
	 * add DetectionRecord
	 */
	public void addDetectionRecord(DetectionRecord record) {
		
		this.detectionRecords.add(record);
		this.detectionRecordTimeHash.put(record.getTime(), record);
		
	} // end of method
	
	/**
	 * Get record count
	 */
	public int getDetectionRecordCount() {
		return this.detectionRecords.size();
	} // end of method

	/**
	 * Get all detection records
	 */
	public DetectionRecord[] getAllDetectionRecords() {
		return this.detectionRecords.toArray(new DetectionRecord[this.detectionRecords.size()]);
	} // end of method
	
	/**
	 * @return the items
	 */
	public int[] getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(int[] items) {
		this.items = items;
	}

	/**
	 * @return the patternId
	 */
	public int getPatternId() {
		return patternId;
	}

	/**
	 * @param patternId the patternId to set
	 */
	public void setPatternId(int patternId) {
		this.patternId = patternId;
	}

	/**
	 * @return the patternName
	 */
	public String getPatternName() {
		return patternName;
	}

	/**
	 * @param patternName the patternName to set
	 */
	public void setPatternName(String patternName) {
		this.patternName = patternName;
	}

	/**
	 * @return the rulsetBaseStartTime
	 */
	public long getRulsetBaseStartTime() {
		return rulsetBaseStartTime;
	}

	/**
	 * @param rulsetBaseStartTime the rulsetBaseStartTime to set
	 */
	public void setRulsetBaseStartTime(long rulsetBaseStartTime) {
		this.rulsetBaseStartTime = rulsetBaseStartTime;
	}

	/**
	 * @return the rulsetBaseEndTime
	 */
	public long getRulsetBaseEndTime() {
		return rulsetBaseEndTime;
	}

	/**
	 * @param rulsetBaseEndTime the rulsetBaseEndTime to set
	 */
	public void setRulsetBaseEndTime(long rulsetBaseEndTime) {
		this.rulsetBaseEndTime = rulsetBaseEndTime;
	}

	/**
	 * @return the factoryId
	 */
	public String getFactoryId() {
		return factoryId;
	}

	/**
	 * @param factoryId the factoryId to set
	 */
	public void setFactoryId(String factoryId) {
		this.factoryId = factoryId;
	}

	/**
	 * @return the stackId
	 */
	public int getStackId() {
		return stackId;
	}

	/**
	 * @param stackId the stackId to set
	 */
	public void setStackId(int stackId) {
		this.stackId = stackId;
	}

	/**
	 * @return the factoryName
	 */
	public String getFactoryName() {
		return factoryName;
	}

	/**
	 * @param factoryName the factoryName to set
	 */
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}

	/**
	 * @return the stackName
	 */
	public String getStackName() {
		return stackName;
	}

	/**
	 * @param stackName the stackName to set
	 */
	public void setStackName(String stackName) {
		this.stackName = stackName;
	}

	/**
	 * @return the isEnabled
	 */
	public boolean isEnabled() {
		return isEnabled;
	}

	/**
	 * @param isEnabled the isEnabled to set
	 */
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	/**
	 * @return the isRulesetCreated
	 */
	public boolean isRulesetCreated() {
		return isRulesetCreated;
	}

	/**
	 * @param isRulesetCreated the isRulesetCreated to set
	 */
	public void setRulesetCreated(boolean isRulesetCreated) {
		this.isRulesetCreated = isRulesetCreated;
	}

	
} // end of class
