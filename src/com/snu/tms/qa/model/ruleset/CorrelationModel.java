/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import com.snu.tms.qa.model.ItemCodeDefine;

/**
 * Save Outlier detection result in PCA analysis
 * 
 */
public class CorrelationModel {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private int      item1 = ItemCodeDefine.UNDEFINED;
	private int      item2 = ItemCodeDefine.UNDEFINED;
	
	private double   rSquare = 0.0D;
	private double   pValue  = 0.0D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Result of correlation analysis
	 */
	public CorrelationModel(
			int     item1,
			int     item2,
			double  rSquare,
			double  pValue) {
		
		this.item1 = item1;
		this.item2 = item2;
		this.rSquare = rSquare;
		this.pValue = pValue;
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * toString()
	 */
	public String toString() {
		
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append(String.format("ITEM_01 : %-6s\n",  ItemCodeDefine.getItemName(item1)));
		strBuffer.append(String.format("ITEM_02 : %-6s\n",  ItemCodeDefine.getItemName(item2)));
		strBuffer.append(String.format("R-Square: %5.3f\n", rSquare));
		strBuffer.append(String.format("P-Value : %5.3f\n", pValue));
		return strBuffer.toString();
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the item1
	 */
	public int getItem1() {
		return item1;
	}

	/**
	 * @param item1 the item1 to set
	 */
	public void setItem1(int item1) {
		this.item1 = item1;
	}

	/**
	 * @return the item2
	 */
	public int getItem2() {
		return item2;
	}

	/**
	 * @param item2 the item2 to set
	 */
	public void setItem2(int item2) {
		this.item2 = item2;
	}

	/**
	 * @return the rSquare
	 */
	public double getrSquare() {
		return rSquare;
	}

	/**
	 * @param rSquare the rSquare to set
	 */
	public void setrSquare(double rSquare) {
		this.rSquare = rSquare;
	}

	/**
	 * @return the pValue
	 */
	public double getpValue() {
		return pValue;
	}

	/**
	 * @param pValue the pValue to set
	 */
	public void setpValue(double pValue) {
		this.pValue = pValue;
	}

	
} // end of class
