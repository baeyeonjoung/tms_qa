/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import java.util.Arrays;

import com.snu.tms.qa.util.DateUtil;

/**
 * Principal analysis data record
 * 
 */
public class PatternDataRecord implements Comparable<PatternDataRecord> {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String    timestr = "";
	private  long      time = 0L;
	
	private  int[]     items;
	private  double[]  values;
	private  int       pcCount = 0;
	private  double[]  projectedValues;
	
	private  int       patternIndex = -1;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for PatternDataRecord
	 */
	public PatternDataRecord() {}
	
	public PatternDataRecord(long time, int[] items, double[] values, int pcCount) {
		
		this.time = time;
		this.timestr = DateUtil.getDateStr(time);
		
		this.items = Arrays.copyOf(items, items.length);
		this.values = Arrays.copyOf(values, values.length);
		this.pcCount = pcCount;
		this.projectedValues = new double[pcCount];
		Arrays.fill(this.projectedValues, 0.0D);
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods : Comparable
	//==========================================================================
	/**
	 * compareTo()
	 */
	public int compareTo(PatternDataRecord compare) {
		
		if (this.time > compare.time) {
			return 1;
		} else if (this.time < compare.time) {
			return -1;
		} else {
			return 0;
		}
		
	} // end of method
	
	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// Append information
		infoStrBuffer.append(String.format("%s  :  %3d  [", this.timestr, this.patternIndex));
		for (int i=0 ; i<this.values.length ; i++) {
			if (i == 0) {
				infoStrBuffer.append(String.format("%7.2f", this.values[i]));
			} else {
				infoStrBuffer.append(String.format(", %7.2f", this.values[i]));
			}
		}
		infoStrBuffer.append("]  [");
		for (int i=0 ; i<this.projectedValues.length ; i++) {
			if (i == 0) {
				infoStrBuffer.append(String.format("%7.3f", this.projectedValues[i]));
			} else {
				infoStrBuffer.append(String.format(", %7.3f", this.projectedValues[i]));
			}
		}
		infoStrBuffer.append("]");
		
		// return
		return infoStrBuffer.toString();

	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Get distance
	 */
	public double getDistanceOfCentroid() {
		
		// compute distacne
		double distance = 0.0D;
		double N = (double) this.projectedValues.length;
		for (int i=0 ; i<this.projectedValues.length ; i++) {
			distance += Math.pow(this.projectedValues[i], 2.0D);
		}
		distance = Math.sqrt(distance / N);
		
		// return
		return distance;
		
	} // end of method
	
	
	/**
	 * 
	 */
	public void setValue(double[] projectedValue, double[] eigenValue) {
		
		for (int i=0 ; i<projectedValue.length ; i++) {
			this.projectedValues[i] = projectedValue[i];
		}
	
	} // end of method
	
	
	/**
	 * Get Values
	 */
	public double[] getValues() {
		return this.values;
	} // end of method
	

	/**
	 * Get Items
	 */
	public int[] getItems() {
		return this.items;
	} // end of method

	/**
	 * Get Value
	 */
	public double getValue(int index) {
		return this.values[index];
	} // end of method
	
	/**
	 * get value
	 */
	public double getItemValue(int item) {
		
		// search index
		for (int i=0 ; i<this.items.length ; i++) {
			if (this.items[i] == item) {
				return this.values[i];
			}
		}
		return 0.0D;
		
	} // end of method
	
	
	/**
	 * Get ProjectedValues
	 */
	public double[] getProjectedValues() {
		return this.projectedValues;
	} // end of method
	
	
	/**
	 * Set Pattern Index
	 */
	public void setPatternIndex(int patternIndex) {
		this.patternIndex = patternIndex;
	} // end of method
	
	
	/**
	 * Get Pattern Index
	 */
	public int getPatternIndex() {
		return this.patternIndex;
	} // end of method
	
	
	
	/**
	 * @return the timestr
	 */
	public String getTimestr() {
		return timestr;
	}

	/**
	 * @param timestr the timestr to set
	 */
	public void setTimestr(String timestr) {
		this.timestr = timestr;
	}

	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}
	
	/**
	 * Get PcCount (Principal Component Axis Count)
	 */
	public int getPcCount() {
		return this.pcCount;
	} // end of method

} // end of class
