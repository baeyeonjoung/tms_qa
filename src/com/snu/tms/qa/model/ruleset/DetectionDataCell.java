/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;


/**
 * Data Element for Outlier dection result
 * 
 */
public class DetectionDataCell  {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int               rulesetType;
	private  int               rulesetId;
	private  DetectionRuleset  ruleset;

	private  double           resultValue = Double.NaN;
	private  boolean          isOutlier = false;
	
	private  DetectionRecord  detectionRecord;
	private  RawDataCell[]    rawDataCells;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public DetectionDataCell() {}
	
	public DetectionDataCell(DetectionRuleset ruleset, DetectionRecord detectionRecord) {

		// set parameters
		this.ruleset = ruleset;
		this.detectionRecord = detectionRecord;
		this.rulesetType = ruleset.getRulesetType();
		this.rulesetId = ruleset.getRulesetId();
		
		// get raw data
		int[] items = this.ruleset.getItems();
		this.rawDataCells = this.detectionRecord.getRawDataCell(items);
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the rulesetType
	 */
	public int getRulesetType() {
		return rulesetType;
	}

	/**
	 * @param rulesetType the rulesetType to set
	 */
	public void setRulesetType(int rulesetType) {
		this.rulesetType = rulesetType;
	}

	/**
	 * @return the rulesetId
	 */
	public int getRulesetId() {
		return rulesetId;
	}

	/**
	 * @param rulesetId the rulesetId to set
	 */
	public void setRulesetId(int rulesetId) {
		this.rulesetId = rulesetId;
	}

	/**
	 * @return the ruleset
	 */
	public DetectionRuleset getRuleset() {
		return ruleset;
	}

	/**
	 * @param ruleset the ruleset to set
	 */
	public void setRuleset(DetectionRuleset ruleset) {
		
		// set ruleset
		this.ruleset = ruleset;
		
		// set isOutlier
		double[] thresholds = ruleset.getThresholds();
		if (this.resultValue != Double.NaN) {
			if (thresholds[0] < this.resultValue || thresholds[1] > this.resultValue) {
				this.isOutlier = true;
			} else {
				this.isOutlier = false;
			}
		}

	} // end of method
	
	/**
	 * Check outlier
	 */
	public boolean checkOutlier() {
		
		// set isOutlier
		double[] thresholds = ruleset.getThresholds();
		if (this.ruleset != null) {
			if (thresholds[0] < this.resultValue || thresholds[1] > this.resultValue) {
				this.isOutlier = true;
			} else {
				this.isOutlier = false;
			}
		}
		return this.isOutlier;
		
	} // end of method
	

	/**
	 * @return the resultValue
	 */
	public double getResultValue() {
		return resultValue;
	}

	/**
	 * @param resultValue the resultValue to set
	 */
	public void setResultValue(double resultValue) {
		
		// set result value
		this.resultValue = resultValue;
		
		// set isOutlier
		double[] thresholds = ruleset.getThresholds();
		if (this.ruleset != null) {
			if (thresholds[0] < this.resultValue || thresholds[1] > this.resultValue) {
				this.isOutlier = true;
			} else {
				this.isOutlier = false;
			}
		}

	} // end of method

	/**
	 * @return the isOutlier
	 */
	public boolean isOutlier() {
		return isOutlier;
	}

	/**
	 * @param isOutlier the isOutlier to set
	 */
	public void setOutlier(boolean isOutlier) {
		this.isOutlier = isOutlier;
	}

	/**
	 * @return the detectionRecord
	 */
	public DetectionRecord getDetectionRecord() {
		return detectionRecord;
	}

	/**
	 * @param detectionRecord the detectionRecord to set
	 */
	public void setDetectionRecord(DetectionRecord detectionRecord) {
		this.detectionRecord = detectionRecord;
	}

	/**
	 * @return the rawDataCells
	 */
	public RawDataCell[] getRawDataCells() {
		return rawDataCells;
	}


} // end of class
