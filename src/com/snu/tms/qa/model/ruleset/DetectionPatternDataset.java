/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : Apil 3, 2016
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.snu.tms.qa.model.FactoryInfo;
import com.snu.tms.qa.model.PatternDataHistogram;
import com.snu.tms.qa.model.StackInfo;


/**
 * Management for outlier ruleset and outlier detection data
 * 
 */
public class DetectionPatternDataset implements Comparable<DetectionPatternDataset> {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	// pattern Id
	private  int       patternId = 0;
	private  String    patternName = "";
	private  boolean   isEnabled = true;
	private  boolean   isRulesetCreated = false;
	private  boolean   isOperateStop = false;

	// TMS monitoring items
	private  int[]     items;
	
	private  long     patternBaseStartTime = 0L;
	private  long     patternBaseEndTime   = 0L;
	
	// target factory and stack
	private  String    factoryId = "";
	private  int       stackId = 0;
	private  String    factoryName = "";
	private  String    stackName = "";
	
	// result of PC analysis
	private  PcaResultModel  pcaResultModel;
	
	// data range (min-max) for each item
	private  double[][]  itemDataRange;
	
	// Pattern data histogram
	private  PatternDataHistogram[]     patternDataHistograms = null;
	private  Hashtable<Integer, PatternDataHistogram>  patternDataHistogramHash = null;

	// Outlier ruleset management
	private Vector<DetectionRuleset> rulesetRepository = 
			new Vector<DetectionRuleset>();
	
	// outlier detection result
	private Vector<DetectionRecord> detectionRecords = 
			new Vector<DetectionRecord>();
	private Hashtable<Long, DetectionRecord> detectionRecordTimeHash = 
			new Hashtable<Long, DetectionRecord>();
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Management for outlier ruleset and outlier detection data
	 */
	public DetectionPatternDataset(
			FactoryInfo  factoryInfo,
			StackInfo    stackInfo,
			int          patternId,
			String       patternName,
			int[]        items,
			long         patternBaseStartTime,
			long         patternBaseEndTime,
			double[][]   itemDataRange,
			PatternDataHistogram[]  patternDataHistograms) {
		
		// set parameters
		this.factoryId   = factoryInfo.getCode();
		this.factoryName = factoryInfo.getFullName();
		this.stackId     = stackInfo.getStackNumber();
		this.stackName   = stackInfo.getStackName();
		this.items       = items;
		
		this.patternBaseStartTime  = patternBaseStartTime;
		this.patternBaseEndTime    = patternBaseEndTime;
		
		this.patternId   = patternId;
		this.patternName = patternName;
		
		// create PatternDataHistogram
		this.itemDataRange = itemDataRange;
		this.patternDataHistograms = patternDataHistograms;
		
		this.patternDataHistogramHash = new Hashtable<Integer, PatternDataHistogram>();
		for (int i=0 ; i<this.patternDataHistograms.length ; i++) {
			this.patternDataHistogramHash.put(this.items[i], this.patternDataHistograms[i]);
		}
	
	} // end of constructor
	
	/**
	 * Management for outlier ruleset and outlier detection data
	 */
	public DetectionPatternDataset(
			String       factoryId,
			int          stackId,
			int          patternId,
			String       patternName,
			int[]        items,
			long         patternBaseStartTime,
			long         patternBaseEndTime,
			double[][]   itemDataRange,
			PatternDataHistogram[]  patternDataHistograms) {
		
		// set parameters
		this.factoryId   = factoryId;
		this.stackId     = stackId;
		this.items       = items;
		
		this.patternBaseStartTime  = patternBaseStartTime;
		this.patternBaseEndTime    = patternBaseEndTime;
		
		this.patternId   = patternId;
		this.patternName = patternName;
		
		// create PatternDataHistogram
		this.itemDataRange = itemDataRange;
		this.patternDataHistograms = patternDataHistograms;
		
		this.patternDataHistogramHash = new Hashtable<Integer, PatternDataHistogram>();
		for (int i=0 ; i<this.patternDataHistograms.length ; i++) {
			this.patternDataHistogramHash.put(this.items[i], this.patternDataHistograms[i]);
		}
	
	} // end of constructor
	
	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implements CompareTo
	 */
	public int compareTo(DetectionPatternDataset compare) {
		
		if (this.patternId > compare.patternId) {
			return 1;
		} else if (this.patternId < compare.patternId) {
			return -1;
		} else {
			return 0;
		}
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Set Ruleset
	 */
	public void addRuleset(DetectionRuleset ruleset) {
		
		// add ruleset
		this.rulesetRepository.add(ruleset);
		
		// set DetectionDataCell
		DetectionRecord record;
		DetectionDataCell dataCell;
		for (Enumeration<DetectionRecord> e=this.detectionRecords.elements() ; e.hasMoreElements() ;) {
			record = e.nextElement();
			dataCell = record.addDetectionDataCell(ruleset);
			if (ruleset instanceof RulesetPd) {
				RawDataCell rawDataCell = record.getRawDataCell(ruleset.getItems())[0];
				dataCell.setResultValue(ruleset.applyRuleset(
						new double[] {
								rawDataCell.getData_value(),
								rawDataCell.getOrigin_value()}));
			} else {
				dataCell.setResultValue(ruleset.applyRuleset(record.getRawDataCellValue(ruleset.getItems())));
			}
		}
		
	} // end of method
	
	/**
	 * re-compute outlier with new threshold
	 */
	public void recomputeOutlier(DetectionRuleset ruleset) {
		
		DetectionRecord record;
		DetectionDataCell dataCell;
		for (Enumeration<DetectionRecord> e=this.detectionRecords.elements() ; e.hasMoreElements() ;) {
			record = e.nextElement();
			dataCell = record.getDetectionDataCell(ruleset);
			if (dataCell != null) {
				dataCell.checkOutlier();
			}
		}
		
	} // end of method
	
	/**
	 * @return the pcaResultModel
	 */
	public PcaResultModel getPcaResultModel() {
		return pcaResultModel;
	}

	/**
	 * @param pcaResultModel the pcaResultModel to set
	 */
	public void setPcaResultModel(PcaResultModel pcaResultModel) {
		this.pcaResultModel = pcaResultModel;
	}

	/**
	 * Get ruleset
	 */
	public DetectionRuleset getRuleset(DetectionRuleset ruleset) {
		
		DetectionRuleset rulesetItem;
		for (Enumeration<DetectionRuleset> e=this.rulesetRepository.elements() ; e.hasMoreElements() ;) {
			rulesetItem = e.nextElement();
			if (rulesetItem.isSameRuleset(ruleset)) {
				return rulesetItem;
			} else {
				continue;
			}
		}
		return null;
		
	} // end of method
	
	/**
	 * Get ruleset
	 */
	public DetectionRuleset[] getAllRuleset() {
		return this.rulesetRepository.toArray(new DetectionRuleset[this.rulesetRepository.size()]);		
	} // end of method

	/**
	 * Get ruleset
	 */
	public DetectionRuleset[] getAllRuleset(int rulesetType) {
		
		Vector<DetectionRuleset> rulesets = new Vector<DetectionRuleset>();
		for (DetectionRuleset ruleset : this.rulesetRepository) {
			if (ruleset.getRulesetType() == rulesetType) {
				rulesets.add(ruleset);
			}
		}
		return rulesets.toArray(new DetectionRuleset[rulesets.size()]);	
		
	} // end of method

	/**
	 * add DetectionRecord
	 */
	public void addDetectionRecord(DetectionRecord record) {
		
		this.detectionRecords.add(record);
		this.detectionRecordTimeHash.put(record.getTime(), record);
		
	} // end of method
	
	/**
	 * Get record count
	 */
	public int getDetectionRecordCount() {
		return this.detectionRecords.size();
	} // end of method

	/**
	 * Get all detection records
	 */
	public DetectionRecord[] getAllDetectionRecords() {
		return this.detectionRecords.toArray(new DetectionRecord[this.detectionRecords.size()]);
	} // end of method
	
	/**
	 * get all patternDataHistogram
	 */
	public PatternDataHistogram[] getAllPatternDataHistogram() {
		return this.patternDataHistograms;
	} // end of method
	
	/**
	 * get pattern histogram data
	 */
	public PatternDataHistogram getPatternDataHistogram(int item) {
		return this.patternDataHistogramHash.get(item);
	} // end of method

	/**
	 * get pattern conformity
	 */
	public double getPatternConformity(double[] values) {
		
/**
		// get all pattern index
		int[] patternIndices = getPatternIndexArray();
		if (patternIndices == null) {
			return -1;
		}
		
		// get item and patternDataHistogram
		if (items == null || items.length == 0) {
			return -1;
		}
		
		double[] patternConformityScores = new double[patternIndices.length];
		Arrays.fill(patternConformityScores, 0.0D);
		for (int i=0 ; i<patternIndices.length ; i++) {
			
			double conformityScore = 0.0D;
			PatternDataHistogram patternDataHistogram = null;
			for (int j=0 ; j<items.length ; j++) {
				
				patternDataHistogram = getPatternHistograms(patternIndices[i], items[j]);
				if (patternDataHistogram == null) {
					continue;
				}
				conformityScore += patternDataHistogram.getConformityScore(values[j]);
				
			}
			patternConformityScores[i] = conformityScore;
			
		}
		
		// get max conformityScore and patternId
		int patternId = patternIndices[0];
		double patternConformity = 0.0D;
		for (int i=0 ; i<patternConformityScores.length ; i++) {
			
			if (patternConformity < patternConformityScores[i]) {
				patternId = patternIndices[i];
				patternConformity = patternConformityScores[i];
			}
			
		}
		
		// return
		return patternId;
*/
		double conformityScore = 0.0D;
		for (int i=0 ; i<this.items.length ; i++) {
			conformityScore += this.patternDataHistograms[i].getConformityScore(values[i]);
		}
		return conformityScore;
		
	} // end of method
	
	/**
	 * @return the items
	 */
	public int[] getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(int[] items) {
		this.items = items;
	}

	/**
	 * @return the isOperateStop
	 */
	public boolean isOperateStop() {
		return isOperateStop;
	}

	/**
	 * @param isOperateStop the isOperateStop to set
	 */
	public void setOperateStop(boolean isOperateStop) {
		this.isOperateStop = isOperateStop;
	}

	/**
	 * @return the patternId
	 */
	public int getPatternId() {
		return patternId;
	}

	/**
	 * @return the patternBaseStartTime
	 */
	public long getPatternBaseStartTime() {
		return patternBaseStartTime;
	}

	/**
	 * @param patternBaseStartTime the patternBaseStartTime to set
	 */
	public void setPatternBaseStartTime(long patternBaseStartTime) {
		this.patternBaseStartTime = patternBaseStartTime;
	}

	/**
	 * @return the patternBaseEndTime
	 */
	public long getPatternBaseEndTime() {
		return patternBaseEndTime;
	}

	/**
	 * @param patternBaseEndTime the patternBaseEndTime to set
	 */
	public void setPatternBaseEndTime(long patternBaseEndTime) {
		this.patternBaseEndTime = patternBaseEndTime;
	}

	/**
	 * @param patternId the patternId to set
	 */
	public void setPatternId(int patternId) {
		this.patternId = patternId;
	}

	/**
	 * @return the patternName
	 */
	public String getPatternName() {
		return patternName;
	}

	/**
	 * @param patternName the patternName to set
	 */
	public void setPatternName(String patternName) {
		this.patternName = patternName;
	}

	/**
	 * @return the factoryId
	 */
	public String getFactoryId() {
		return factoryId;
	}

	/**
	 * @param factoryId the factoryId to set
	 */
	public void setFactoryId(String factoryId) {
		this.factoryId = factoryId;
	}

	/**
	 * @return the stackId
	 */
	public int getStackId() {
		return stackId;
	}

	/**
	 * @param stackId the stackId to set
	 */
	public void setStackId(int stackId) {
		this.stackId = stackId;
	}

	/**
	 * @return the factoryName
	 */
	public String getFactoryName() {
		return factoryName;
	}

	/**
	 * @return the itemDataRange
	 */
	public double[][] getItemDataRange() {
		return itemDataRange;
	}

	/**
	 * @param itemDataRange the itemDataRange to set
	 */
	public void setItemDataRange(double[][] itemDataRange) {
		this.itemDataRange = itemDataRange;
	}

	/**
	 * @param factoryName the factoryName to set
	 */
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}

	/**
	 * @return the stackName
	 */
	public String getStackName() {
		return stackName;
	}

	/**
	 * @param stackName the stackName to set
	 */
	public void setStackName(String stackName) {
		this.stackName = stackName;
	}

	/**
	 * @return the isEnabled
	 */
	public boolean isEnabled() {
		return isEnabled;
	}

	/**
	 * @param isEnabled the isEnabled to set
	 */
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	/**
	 * @return the isRulesetCreated
	 */
	public boolean isRulesetCreated() {
		return isRulesetCreated;
	}

	/**
	 * @param isRulesetCreated the isRulesetCreated to set
	 */
	public void setRulesetCreated(boolean isRulesetCreated) {
		this.isRulesetCreated = isRulesetCreated;
	}

	
} // end of class
