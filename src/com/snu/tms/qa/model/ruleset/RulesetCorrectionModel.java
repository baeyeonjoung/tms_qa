/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import com.snu.tms.qa.model.ItemCodeDefine;


/**
 * Tms value correction information
 * 
 */
public class RulesetCorrectionModel implements Comparable<RulesetCorrectionModel> {

	//==========================================================================
	// Static Fields
	//==========================================================================
	public  final  static  int  CORRECTION_APPLIED     = 1;
	public  final  static  int  CORRECTION_NOT_APPLIED = 0;
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int   itemCode = ItemCodeDefine.UNDEFINED;
	
	private  double   stdOxygen           = -1.0D;
	private  double   stdMoisture         = -1.0D;
	
	private  boolean  oxygenApplied       = false;
	private  boolean  temperatureApplied  = false;
	private  boolean  moistureApplied     = false;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for CorrectionInfo
	 */
	public RulesetCorrectionModel() {}
	
	public RulesetCorrectionModel(
			int     itemCode,
			double  stdOxygen,
			double  stdMoisture,
			int     oxygenApplied,
			int     temperatureApplied,
			int     moistureApplied) {
		
		this.itemCode           = itemCode;
		this.stdOxygen          = stdOxygen;
		this.stdMoisture        = stdMoisture;
		this.oxygenApplied      = (oxygenApplied == 0)      ? false : true;
		this.temperatureApplied = (temperatureApplied == 0) ? false : true;
		this.moistureApplied    = (moistureApplied == 0)    ? false : true;
		
	} // end of constructor
	
	public RulesetCorrectionModel(
			int     itemCode,
			double  stdOxygen,
			double  stdMoisture,
			boolean oxygenApplied,
			boolean temperatureApplied,
			boolean moistureApplied) {
		
		this.itemCode           = itemCode;
		this.stdOxygen          = stdOxygen;
		this.stdMoisture        = stdMoisture;
		this.oxygenApplied      = oxygenApplied;
		this.temperatureApplied = temperatureApplied;
		this.moistureApplied    = moistureApplied;
		
	} // end of constructor
	
	public RulesetCorrectionModel(RulesetCorrectionModel clone) {
		
		this.itemCode           = clone.itemCode;
		this.stdOxygen          = clone.stdOxygen;
		this.stdMoisture        = clone.stdMoisture;
		this.oxygenApplied      = clone.oxygenApplied;
		this.temperatureApplied = clone.temperatureApplied;
		this.moistureApplied    = clone.moistureApplied;
		
	} // end of constructor

	//==========================================================================
	// Implements Methods
	//==========================================================================
	/**
	 * implements Comparable
	 */
	public int compareTo(RulesetCorrectionModel compare) {
		
		if (this.itemCode > compare.itemCode) {
			return 1;
		} else if (this.itemCode < compare.itemCode) {
			return -1;
		} else {
			return 0;
		}
	
	} // end of method
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * 
	 */
	public void setCorrectionInfo(RulesetCorrectionModel clone) {
		
		this.itemCode           = clone.itemCode;
		this.stdOxygen          = clone.stdOxygen;
		this.stdMoisture        = clone.stdMoisture;
		this.oxygenApplied      = clone.oxygenApplied;
		this.temperatureApplied = clone.temperatureApplied;
		this.moistureApplied    = clone.moistureApplied;

	} // end of method
	
	/**
	 * @return the itemCode
	 */
	public int getItemCode() {
		return itemCode;
	}

	/**
	 * @param itemCode the itemCode to set
	 */
	public void setItemCode(int itemCode) {
		this.itemCode = itemCode;
	}

	/**
	 * @return the stdOxygen
	 */
	public double getStdOxygen() {
		return stdOxygen;
	}

	/**
	 * @param stdOxygen the stdOxygen to set
	 */
	public void setStdOxygen(double stdOxygen) {
		this.stdOxygen = stdOxygen;
	}

	/**
	 * @return the stdMoisture
	 */
	public double getStdMoisture() {
		return stdMoisture;
	}

	/**
	 * @param stdMoisture the stdMoisture to set
	 */
	public void setStdMoisture(double stdMoisture) {
		this.stdMoisture = stdMoisture;
	}

	/**
	 * @return the oxygenApplied
	 */
	public boolean isOxygenApplied() {
		return oxygenApplied;
	}

	/**
	 * @param oxygenApplied the oxygenApplied to set
	 */
	public void setOxygenApplied(boolean oxygenApplied) {
		this.oxygenApplied = oxygenApplied;
	}

	/**
	 * @return the temperatureApplied
	 */
	public boolean isTemperatureApplied() {
		return temperatureApplied;
	}

	/**
	 * @param temperatureApplied the temperatureApplied to set
	 */
	public void setTemperatureApplied(boolean temperatureApplied) {
		this.temperatureApplied = temperatureApplied;
	}

	/**
	 * @return the moistureApplied
	 */
	public boolean isMoistureApplied() {
		return moistureApplied;
	}

	/**
	 * @param moistureApplied the moistureApplied to set
	 */
	public void setMoistureApplied(boolean moistureApplied) {
		this.moistureApplied = moistureApplied;
	}


} // end of class
