/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import com.snu.tms.qa.model.ItemCodeDefine;

/**
 * Data Element for TMS Data
 * 
 */
public class RawDataCell  {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int       data_type    = ItemCodeDefine.UNDEFINED;
	private  double    data_value   = -1.0D;
	private  double    origin_value = -1.0D;
	
	private  boolean   mois_correction = false;
	private  boolean   temp_correction = false;
	private  boolean   oxyg_correction = false;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public RawDataCell() {}
	
	public RawDataCell(int data_type) {
		this.data_type = data_type;
	} // end of constructor

	public RawDataCell(int data_type, double data_value) {
		this.data_type = data_type;
		this.data_value = data_value;
	} // end of constructor
	
	public RawDataCell(int data_type, double data_value, double origin_value) {
		this.data_type = data_type;
		this.data_value = data_value;
		this.origin_value = origin_value;
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the data_type
	 */
	public int getData_type() {
		return data_type;
	}

	/**
	 * @param data_type the data_type to set
	 */
	public void setData_type(int data_type) {
		this.data_type = data_type;
	}

	/**
	 * @return the data_value
	 */
	public double getData_value() {
		return data_value;
	}

	/**
	 * @param data_value the data_value to set
	 */
	public void setData_value(double data_value) {
		this.data_value = data_value;
	}

	/**
	 * @return the origin_value
	 */
	public double getOrigin_value() {
		return origin_value;
	}

	/**
	 * @param origin_value the origin_value to set
	 */
	public void setOrigin_value(double origin_value) {
		this.origin_value = origin_value;
	}

	/**
	 * @return the mois_correction
	 */
	public boolean isMois_correction() {
		return mois_correction;
	}

	/**
	 * @param mois_correction the mois_correction to set
	 */
	public void setMois_correction(boolean mois_correction) {
		this.mois_correction = mois_correction;
	}

	/**
	 * @return the temp_correction
	 */
	public boolean isTemp_correction() {
		return temp_correction;
	}

	/**
	 * @param temp_correction the temp_correction to set
	 */
	public void setTemp_correction(boolean temp_correction) {
		this.temp_correction = temp_correction;
	}

	/**
	 * @return the oxyg_correction
	 */
	public boolean isOxyg_correction() {
		return oxyg_correction;
	}

	/**
	 * @param oxyg_correction the oxyg_correction to set
	 */
	public void setOxyg_correction(boolean oxyg_correction) {
		this.oxyg_correction = oxyg_correction;
	}


} // end of class
