/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import java.util.Vector;

import com.snu.tms.qa.util.DateUtil;


/**
 * Ruleset Model
 * 
 */
public class RulesetModel implements Comparable<RulesetModel>
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int      rulesetId        = -1;
	private  String   factCode         = "";
	private  int      stckCode         = 0;
	private  int      rulesetState     = 0;
	private  String   rulesetName      = "";
	private  String   rulesetDescr     = "";
	private  long     rulesetStartTime = 0L;
	private  long     rulesetEndTime   = 0L;
	private  String   userId           = "";
	private  String   createTime       = "";
	
	private  RulesetWeightingModel  rulesetWeightingModel = 
			new RulesetWeightingModel();
	
	private  Vector<RulesetCorrectionModel> rulesetCorrectionModels = 
			new Vector<RulesetCorrectionModel>();
	
	private  Vector<RulesetPatternModel>  rulesetPatternModels = 
			new Vector<RulesetPatternModel>();
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * constructor for RulesetModel
	 */
	public RulesetModel() {}
	
	public RulesetModel(
			int     rulesetId,
			String  factCode,
			int     stckCode,
			int     rulesetState,
			String  rulesetName,
			String  rulesetDescr,
			String  userId,
			String  createTime) {
		
		this.rulesetId    = rulesetId;
		this.factCode     = factCode;
		this.stckCode     = stckCode;
		this.rulesetState = rulesetState;
		this.rulesetName  = rulesetName;
		this.rulesetDescr = rulesetDescr;
		this.userId       = userId;
		this.createTime   = createTime;
		
	} // end of constructor
	
	public RulesetModel(
			String  factCode,
			int     stckCode,
			String  rulesetName,
			String  rulesetDescr) {
		
		this.factCode     = factCode;
		this.stckCode     = stckCode;
		this.rulesetName  = rulesetName;
		this.rulesetDescr = rulesetDescr;
		this.createTime   = DateUtil.getCurrDateStr();
		
	} // end of constructor
	
	public RulesetModel(RulesetModel rulesetModel) {
		
		this.rulesetId    = rulesetModel.rulesetId;
		this.factCode     = rulesetModel.factCode;
		this.stckCode     = rulesetModel.stckCode;
		this.rulesetState = rulesetModel.rulesetState;
		this.rulesetName  = rulesetModel.rulesetName;
		this.rulesetDescr = rulesetModel.rulesetDescr;
		this.userId       = rulesetModel.userId;
		this.createTime   = rulesetModel.createTime;
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * implements toString()
	 */
	public String toString() {
		
		String infoStr = String.format(
				"RulesetId [%s], FactCode[%s], StckCode[%d], RulesetName[%s], BaseTime[%s], State[%d]", 
				(rulesetId == -1) ? "" : (rulesetId + ""),
				factCode,
				stckCode,
				rulesetName,
				DateUtil.getDateStr(rulesetStartTime) + " ~ " + DateUtil.getDateStr(rulesetEndTime),
				rulesetState);
		return infoStr;
		
	} // end of method
	
	/**
	 * implement comparable
	 */
	public int compareTo(RulesetModel rulesetModel) {
		return  (this.rulesetId - rulesetModel.rulesetId);
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * clear sub-components
	 */
	public void clear() {
		
		// clear rulesetPatternModel
		this.rulesetPatternModels.clear();
		this.rulesetCorrectionModels.clear();
		System.gc();
		
	} // end of method
	
	/**
	 * Add ruleset pattern model
	 */
	public void addRulesetPatternModel(RulesetPatternModel rulesetPatternModel) {
		this.rulesetPatternModels.add(rulesetPatternModel);
	} // end of method
	
	/**
	 * get all ruleset pattern model
	 */
	public RulesetPatternModel[] getAllRulesetPatternModel() {
		return this.rulesetPatternModels.toArray(new RulesetPatternModel[this.rulesetPatternModels.size()]);
	} // end of method
	
	/**
	 * get ruleset pattern model
	 */
	public RulesetPatternModel getRulesetPatternModel(int patternId) {
		
		// search pattern model
		for (RulesetPatternModel model : rulesetPatternModels) {
			
			if (model.getPatternId() == patternId) {
				return model;
			}
		}
		return null;
		
	} // end of method
	
	/**
	 * get ruleset pattern model count
	 */
	public int getRulesetPatternModelCount() {
		return this.rulesetPatternModels.size();
	} // end of method
	
	/**
	 * @return the rulesetId
	 */
	public int getRulesetId() {
		return rulesetId;
	}

	/**
	 * @param rulesetId the rulesetId to set
	 */
	public void setRulesetId(int rulesetId) {
		this.rulesetId = rulesetId;
	}

	/**
	 * @return the factCode
	 */
	public String getFactCode() {
		return factCode;
	}

	/**
	 * @param factCode the factCode to set
	 */
	public void setFactCode(String factCode) {
		this.factCode = factCode;
	}

	/**
	 * @return the stckCode
	 */
	public int getStckCode() {
		return stckCode;
	}

	/**
	 * @param stckCode the stckCode to set
	 */
	public void setStckCode(int stckCode) {
		this.stckCode = stckCode;
	}

	/**
	 * @return the rulesetState
	 */
	public int getRulesetState() {
		return rulesetState;
	}

	/**
	 * @param rulesetState the rulesetState to set
	 */
	public void setRulesetState(int rulesetState) {
		this.rulesetState = rulesetState;
	}

	/**
	 * @return the rulesetName
	 */
	public String getRulesetName() {
		return rulesetName;
	}

	/**
	 * @param rulesetName the rulesetName to set
	 */
	public void setRulesetName(String rulesetName) {
		this.rulesetName = rulesetName;
	}

	/**
	 * @return the rulesetDescr
	 */
	public String getRulesetDescr() {
		return rulesetDescr;
	}

	/**
	 * @param rulesetDescr the rulesetDescr to set
	 */
	public void setRulesetDescr(String rulesetDescr) {
		this.rulesetDescr = rulesetDescr;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the createTime
	 */
	public String getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the rulesetStartTime
	 */
	public long getRulesetStartTime() {
		return rulesetStartTime;
	}

	/**
	 * @param rulesetStartTime the rulesetStartTime to set
	 */
	public void setRulesetStartTime(long rulesetStartTime) {
		this.rulesetStartTime = rulesetStartTime;
	}

	/**
	 * @return the rulesetEndTime
	 */
	public long getRulesetEndTime() {
		return rulesetEndTime;
	}

	/**
	 * @param rulesetEndTime the rulesetEndTime to set
	 */
	public void setRulesetEndTime(long rulesetEndTime) {
		this.rulesetEndTime = rulesetEndTime;
	}

	/**
	 * @return the rulesetWeightingModel
	 */
	public RulesetWeightingModel getRulesetWeightingModel() {
		return rulesetWeightingModel;
	}

	/**
	 * @param rulesetWeightingModel the rulesetWeightingModel to set
	 */
	public void setRulesetWeightingModel(RulesetWeightingModel rulesetWeightingModel) {
		this.rulesetWeightingModel = rulesetWeightingModel;
	}
	
	/**
	 * 
	 */
	public void setRulesetWeightingFactors(
			int multivariateRulesetWeighting,
			int bivariateRulesetWeighting,
			int univariateRulesetWeighting,
			int correctionRulesetWeighting,
			int warningLevel,
			int minorLevel,
			int majorLevel,
			int criticalLevel) {
		
		// set Parameters
		this.rulesetWeightingModel.setMultivariateRulesetWeighting(multivariateRulesetWeighting);
		this.rulesetWeightingModel.setBivariateRulesetWeighting(bivariateRulesetWeighting);
		this.rulesetWeightingModel.setUnivariateRulesetWeighting(univariateRulesetWeighting);
		this.rulesetWeightingModel.setCorrectionRulesetWeighting(correctionRulesetWeighting);
		
		this.rulesetWeightingModel.setWarningLevel(warningLevel);
		this.rulesetWeightingModel.setMinorLevel(minorLevel);
		this.rulesetWeightingModel.setMajorLevel(majorLevel);
		this.rulesetWeightingModel.setCriticalLevel(criticalLevel);
		
	} // end of method


	/**
	 * @return the rulesetCorrectionModel
	 */
	public RulesetCorrectionModel[] getAllRulesetCorrectionModel() {
		return rulesetCorrectionModels.toArray(new RulesetCorrectionModel[rulesetCorrectionModels.size()]);
	}

	/**
	 * @return the rulesetCorrectionModel
	 */
	public RulesetCorrectionModel getRulesetCorrectionModel(int itemCode) {
		
		for (RulesetCorrectionModel correctionModel : rulesetCorrectionModels) {
			if (correctionModel.getItemCode() == itemCode) {
				return correctionModel;
			}
		}
		return null;
	
	} // end of method

	/**
	 * @param rulesetCorrectionModel the rulesetCorrectionModel to set
	 */
	public void setRulesetCorrectionModel(
			RulesetCorrectionModel[] rulesetCorrectionModels) {
		
		for (RulesetCorrectionModel rulesetCorrectionModel : rulesetCorrectionModels) {
			this.rulesetCorrectionModels.add(rulesetCorrectionModel);
		}
	
	} // end of method
	
	/**
	 * @param rulesetCorrectionModel the rulesetCorrectionModel to set
	 */
	public void addRulesetCorrectionModel(RulesetCorrectionModel rulesetCorrectionModel) {
		this.rulesetCorrectionModels.add(rulesetCorrectionModel);
	} // end of method

} // end of class
