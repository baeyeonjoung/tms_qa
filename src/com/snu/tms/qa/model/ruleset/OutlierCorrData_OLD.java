/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Save Outlier detection result in PCA analysis
 * 
 */
public class OutlierCorrData_OLD {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private Vector<OutlierCorrRuleset_OLD> rulesetArray = 
			new Vector<OutlierCorrRuleset_OLD>();
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Save Outlier detection result in Correlation analysis
	 */
	public OutlierCorrData_OLD() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// return
		return infoStrBuffer.toString();

	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * add ruleset
	 */
	public void addRuleset(OutlierCorrRuleset_OLD ruleset) {
		
		// check registered ruleset
		OutlierCorrRuleset_OLD registeredRuleset = findRuleset(ruleset.getItem1(), ruleset.getItem2());
		if (registeredRuleset != null) {
			return;
		}
		
		// add ruleset
		this.rulesetArray.add(ruleset);
		
	} // end of method
	
	
	/**
	 * search ruleset
	 */
	public OutlierCorrRuleset_OLD findRuleset(int item1, int item2) {
		
		OutlierCorrRuleset_OLD ruleset;
		for (Enumeration<OutlierCorrRuleset_OLD> e=this.rulesetArray.elements() ; e.hasMoreElements(); ) {
			ruleset = e.nextElement();
			if (ruleset.getItem1() == item1 && ruleset.getItem2() == item2) {
				return ruleset;
			} else if (ruleset.getItem1() == item2 && ruleset.getItem2() == item1) {
				return ruleset;
			}
		}
		return null;
		
	} // end of method
	
	/**
	 * get all ruleset
	 */
	public OutlierCorrRuleset_OLD[] getAllRuleset() {
		
		// get all ruleset
		OutlierCorrRuleset_OLD[] rulesets = this.rulesetArray.toArray(
				new OutlierCorrRuleset_OLD[this.rulesetArray.size()]);
		
		// sort with r-square
		Arrays.sort(rulesets, new Comparator<OutlierCorrRuleset_OLD>() {
			public int compare(OutlierCorrRuleset_OLD t1, OutlierCorrRuleset_OLD t2) {
				if (t1.getrSquare() > t2.getrSquare()) {
					return 1;
				} else if (t1.getrSquare() < t2.getrSquare()) {
					return -1;
				} else {
					if (t1.getItem1() != t2.getItem1()) {
						return t1.getItem1() - t2.getItem1();
					} else {
						return t1.getItem2() - t2.getItem2();
					}
				}
			}
		});
		
		// return
		return rulesets;
		
	} // end of method
	
} // end of class
