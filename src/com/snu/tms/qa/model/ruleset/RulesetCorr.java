/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import org.apache.commons.math3.linear.BlockRealMatrix;

import com.snu.tms.qa.model.ItemCodeDefine;

/**
 * Save Outlier detection result in Correlation analysis
 * 
 */
public class RulesetCorr implements DetectionRuleset 
{

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int         rulesetType = DetectionRuleset.RULESET_CORR;

	private  int         rulesetId;
	private  int         patternId;
	private  boolean     rulesetEnabled = true;
	private  double[]    thresholds;

	private  int[]        items;
	private double        rSquare = 0.0D;

	private double[][]    covMatrix;
	private double[][]    imvCovMatrix;
	private double[]      mean;
	private double[]      sigma;
	
	private  double       confidenceLevel = 0.95D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Save Outlier detection result in Correlation analysis
	 */
	public RulesetCorr() {}
	
	public RulesetCorr(int[] items) {
		this.items = items;
	}
	
	public RulesetCorr(
			int         rulesetId,
			int         patternId,
			int[]       items,
			double      rSquare,
			double[][]  covMatrix,
			double[][]  imvCovMatrix,
			double[]    mean,
			double[]    sigma) {
		
		this.rulesetId    = rulesetId;
		this.patternId    = patternId;
		this.items        = items;
		this.rSquare      = rSquare;
		this.covMatrix    = covMatrix;
		this.imvCovMatrix = imvCovMatrix;
		this.mean         = mean;
		this.sigma        = sigma;

	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * get ruleset KeyName
	 */
	public String getKeyName() {
		return String.format("CORR[%s-%s]", 
				ItemCodeDefine.getItemName(items[0]), 
				ItemCodeDefine.getItemName(items[1]));
	} // end of method
	
	/**
	 * get ruleset id
	 */
	public int getRulesetId() {
		return rulesetId;
	} // end of method
	
	/**
	 * set ruleset id
	 */
	public void setRulesetId(int rulesetId) {
		this.rulesetId = rulesetId;
	} // end of method
	
	/**
	 * get pattern type
	 */
	public int getRulesetType() {
		return rulesetType;
	} // end of method

	/**
	 * @return the patternId
	 */
	public int getPatternId() {
		return patternId;
	}

	/**
	 * @param patternId the patternId to set
	 */
	public void setPatternId(int patternId) {
		this.patternId = patternId;
	}

	/**
	 * is same ruleset
	 */
	public boolean isSameRuleset(DetectionRuleset ruleset) {
		
		if (this.rulesetType != ruleset.getRulesetType()) {
			return false;
		}
		int[] compareItems = ruleset.getItems();
		if (compareItems.length != 2) {
			return false;
		}
		if (this.items[0] == compareItems[0] && this.items[1] == compareItems[1]) {
			return true;
		} else if (this.items[0] == compareItems[1] && this.items[1] == compareItems[0]) {
			return true;
		} else {
			return false;
		}
		
	} // end of method
	
	/**
	 * get item
	 */
	public int[] getItems() {
		return items;
	} // end of method
	
	/**
	 * apply rule set and get result
	 */
	public double applyRuleset(double[] inputValues) {
		
		// get projected values
		double[][] data = new double[1][2];
		data[0][0] = (inputValues[0] - mean[0]) / sigma[0];
		data[0][1] = (inputValues[1] - mean[1]) / sigma[1];
		
		// calculate covariance matrix and it's imverse
		BlockRealMatrix invCov = new BlockRealMatrix(imvCovMatrix);
		BlockRealMatrix point  = new BlockRealMatrix(data);
		
		// compute mahalanobis distance
		BlockRealMatrix md = point.multiply(invCov).multiply(point.transpose());
		double distance = md.getEntry(0,  0);
		return distance;
		
	} // end of method
	
	/**
	 * get threshold
	 */
	public double[] getThresholds() {
		return thresholds;
	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the rulesetEnabled
	 */
	public boolean isRulesetEnabled() {
		return rulesetEnabled;
	}

	/**
	 * @param rulesetEnabled the rulesetEnabled to set
	 */
	public void setRulesetEnabled(boolean rulesetEnabled) {
		this.rulesetEnabled = rulesetEnabled;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(int[] items) {
		this.items = items;
	}
	
	/**
	 * parse item string
	 */
	public void setItems(String itemStr) {

		// parse
		String[] splitStr = itemStr.trim().split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}

		try {
			// set item
			int N = splitStr.length;
			this.items = new int[N];
			for (int i=0 ; i<N ; i++) {
				this.items[i] = Integer.parseInt(splitStr[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	} // end of method


	/**
	 * @return the mean
	 */
	public double[] getMean() {
		return mean;
	}

	/**
	 * @param mean the mean to set
	 */
	public void setMean(double[] mean) {
		this.mean = mean;
	}
	
	/**
	 * @param densities the densities to set
	 */
	public void setMean(String dataStr) {
		
		// split target string
		String[] splitStr = dataStr.split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}
		
		// memory allocation
		this.mean = new double[splitStr.length];
		
		// parse
		for (int i=0 ; i<splitStr.length ; i++) {
			this.mean[i] = Double.parseDouble(splitStr[i]);
		} // end of for-loop
		
	} // end of method

	/**
	 * @return the sigma
	 */
	public double[] getSigma() {
		return sigma;
	}

	/**
	 * @param sigma the sigma to set
	 */
	public void setSigma(double[] sigma) {
		this.sigma = sigma;
	}
	
	/**
	 * @param densities the densities to set
	 */
	public void setSigma(String dataStr) {
		
		// split target string
		String[] splitStr = dataStr.split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}
		
		// memory allocation
		this.sigma = new double[splitStr.length];
		
		// parse
		for (int i=0 ; i<splitStr.length ; i++) {
			this.sigma[i] = Double.parseDouble(splitStr[i]);
		} // end of for-loop
		
	} // end of method

	/**
	 * @return the confidenceLevel
	 */
	public double getConfidenceLevel() {
		return confidenceLevel;
	}

	/**
	 * @param confidenceLevel the confidenceLevel to set
	 */
	public void setConfidenceLevel(double confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}

	/**
	 * @param rulesetType the rulesetType to set
	 */
	public void setRulesetType(int rulesetType) {
		this.rulesetType = rulesetType;
	}

	/**
	 * @param threshold the threshold to set
	 */
	public void setThresholds(double[] thresholds) {
		this.thresholds = thresholds;
	}

	/**
	 * @return the rSquare
	 */
	public double getrSquare() {
		return rSquare;
	}

	/**
	 * @param rSquare the rSquare to set
	 */
	public void setrSquare(double rSquare) {
		this.rSquare = rSquare;
	}

	/**
	 * @return the covMatrix
	 */
	public double[][] getCovMatrix() {
		return covMatrix;
	}

	/**
	 * @param covMatrix the covMatrix to set
	 */
	public void setCovMatrix(double[][] covMatrix) {
		this.covMatrix = covMatrix;
	}
	
	/**
	 * @param densities the densities to set
	 */
	public void setCovMatrix(String dataStr) {
		
		// split target string
		String[] splitStr = dataStr.split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}
		
		// memory allocation
		this.covMatrix = new double[splitStr.length][];
		
		// parse
		for (int i=0 ; i<splitStr.length ; i++) {
			
			String[] detailStr = splitStr[i].trim().split(",");
			if (detailStr == null || detailStr.length == 0) {
				continue;
			}
			
			this.covMatrix[i] = new double[detailStr.length];
			for (int j=0 ; j<detailStr.length ; j++) {
				this.covMatrix[i][j] = Double.parseDouble(detailStr[j]);
			}
			
		} // end of for-loop
		
	} // end of method

	/**
	 * @return the imvCovMatrix
	 */
	public double[][] getImvCovMatrix() {
		return imvCovMatrix;
	}

	/**
	 * @param imvCovMatrix the imvCovMatrix to set
	 */
	public void setImvCovMatrix(double[][] imvCovMatrix) {
		this.imvCovMatrix = imvCovMatrix;
	}
	
	/**
	 * @param densities the densities to set
	 */
	public void setImvCovMatrix(String dataStr) {
		
		// split target string
		String[] splitStr = dataStr.split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}
		
		// memory allocation
		this.imvCovMatrix = new double[splitStr.length][];
		
		// parse
		for (int i=0 ; i<splitStr.length ; i++) {
			
			String[] detailStr = splitStr[i].trim().split(",");
			if (detailStr == null || detailStr.length == 0) {
				continue;
			}
			
			this.imvCovMatrix[i] = new double[detailStr.length];
			for (int j=0 ; j<detailStr.length ; j++) {
				this.imvCovMatrix[i][j] = Double.parseDouble(detailStr[j]);
			}
			
		} // end of for-loop
		
	} // end of method

	/**
	 * get items string
	 */
	public String getItemStr() {
		
		String infoStr = "";
		int N = this.items.length;
		for (int i=0 ; i<N ; i++) {
			infoStr += String.format("%d", this.items[i]);
			if (i != (N-1)) {
				infoStr += "|";
			}
		}
		return infoStr;
		
	} // end of method
	
	/**
	 * get cov_matrix string
	 */
	public String getCovMatrixStr() {
		
		String infoStr = "";
		int N = this.covMatrix.length;
		for (int i=0 ; i<N ; i++) {
			
			int M = this.covMatrix[i].length;
			for (int j=0 ; j<this.covMatrix[i].length ; j++) {

				infoStr += String.format("%.6f", this.covMatrix[i][j]);
				if (j != (M-1)) {
					infoStr += ",";
				}

			} // end of for-loop
			
			if (i != (N-1)) {
				infoStr += "|";
			}
			
		} // end of for-loop
		
		return infoStr;
		
	} // end of method
	
	/**
	 * get imv_cov_matrix string
	 */
	public String getImvCovMatrixStr() {
		
		String infoStr = "";
		int N = this.imvCovMatrix.length;
		for (int i=0 ; i<N ; i++) {
			
			int M = this.imvCovMatrix[i].length;
			for (int j=0 ; j<this.imvCovMatrix[i].length ; j++) {

				infoStr += String.format("%.6f", this.imvCovMatrix[i][j]);
				if (j != (M-1)) {
					infoStr += ",";
				}

			} // end of for-loop
			
			if (i != (N-1)) {
				infoStr += "|";
			}
			
		} // end of for-loop
		
		return infoStr;
		
	} // end of method
	
	/**
	 * get mean string
	 */
	public String getMeanStr() {
		
		String infoStr = "";
		int N = this.mean.length;
		for (int i=0 ; i<N ; i++) {
			infoStr += String.format("%.6f", this.mean[i]);
			if (i != (N-1)) {
				infoStr += "|";
			}
		}
		return infoStr;
		
	} // end of method
	
	/**
	 * get sigma string
	 */
	public String getSigmaStr() {
		
		String infoStr = "";
		int N = this.sigma.length;
		for (int i=0 ; i<N ; i++) {
			infoStr += String.format("%.6f", this.sigma[i]);
			if (i != (N-1)) {
				infoStr += "|";
			}
		}
		return infoStr;
		
	} // end of method

		
	
} // end of class
