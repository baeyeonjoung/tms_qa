/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

/**
 * Save Outlier detection result in PCA analysis
 * 
 */
public interface DetectionRuleset {

	//==========================================================================
	// Static Fields
	//==========================================================================
	public final static int    RULESET_PCA_TSS     = 0;
	public final static int    RULESET_PCA_SPE     = 1;
	public final static int    RULESET_CORR        = 2;
	public final static int    RULESET_SPC         = 3;
	public final static int    RULESET_PD          = 4;
	public final static int    RULESET_UD          = 5;
	public final static int    RULESET_CONSTANT    = 6;
	
	public final static String[] RULETSET_NAME = {
		"T-Square Stat",
		"Square Predict Error",
		"Correlation", 
		"Statistic Process",
		"Percent of Differ",
		"Undulation",
		"Constant Value"};

	public final static String[] RULETSET_DESC = {
		"PCA: T-Square Statistics",
		"PCA: Square Prediction Error",
		"Correlation Analysis", 
		"Statistic Process Control",
		"Percent of Difference(Correction)",
		"Undulation(Fluctuation)",
		"Constant Value"};
	
	//==========================================================================
	// Abstract Methods
	//==========================================================================
	/**
	 * get ruleset id
	 */
	public int getRulesetId();
	
	/**
	 * set ruleset id
	 */
	public void setRulesetId(int rulesetId);
	
	/**
	 * get pattern type
	 */
	public int getRulesetType();
	
	/**
	 * @return the rulesetEnabled
	 */
	public boolean isRulesetEnabled();

	/**
	 * @param rulesetEnabled the rulesetEnabled to set
	 */
	public void setRulesetEnabled(boolean rulesetEnabled);

	
	/**
	 * is same ruleset
	 */
	public boolean isSameRuleset(DetectionRuleset ruleset);
	
	/**
	 * get item
	 */
	public int[] getItems();
	
	/**
	 * apply rule set and get result
	 */
	public double applyRuleset(double[] srcData);
	
	/**
	 * get threshold
	 */
	public double[] getThresholds();
	
	/**
	 * get ruleset keyName
	 */
	public String getKeyName();
	
	/**
	 * get confidence level
	 */
	public double getConfidenceLevel();
	

} // end of class
