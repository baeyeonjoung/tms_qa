/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : Apil 3, 2016
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Management for outlier ruleset and outlier detection data
 * 
 */
public class DetectionDataset_OLD {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(DetectionDataset_OLD.class);

	//==========================================================================
	// Local Fields
	//==========================================================================
	// Outlier ruleset management
	private Vector<DetectionPatternDataset> patternDatasetRepository = 
			new Vector<DetectionPatternDataset>();
	
	// target factory and stack
	private String factoryId = "";
	private int    stackId = 0;
	private String factoryName = "";
	private String stackName = "";
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Management for outlier ruleset and outlier detection data
	 */
	public DetectionDataset_OLD() {}
	
	public DetectionDataset_OLD(
			String factoryId,
			int    stackId,
			String factoryName,
			String stackName
			) {
		
		this.factoryId = factoryId;
		this.stackId = stackId;
		this.factoryName = factoryName;
		this.stackName = stackName;
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Add pattern
	 */
	public void addPatternDataset(DetectionPatternDataset patternDataset) {
		
		// check registered element
		DetectionPatternDataset registeredPatternDataset = 
				getPatternDataset(patternDataset.getPatternId());
		if (registeredPatternDataset != null) {
			this.patternDatasetRepository.remove(registeredPatternDataset);
		}
		
		// add patterndataset
		this.patternDatasetRepository.add(patternDataset);
		
	} // end of method
	
	/**
	 * get patterndataset
	 */
	public DetectionPatternDataset getPatternDataset(int patternId) {
		
		DetectionPatternDataset patternDataet;
		for (Enumeration<DetectionPatternDataset> e=this.patternDatasetRepository.elements() ; e.hasMoreElements() ; ) {
			patternDataet = e.nextElement();
			if (patternDataet.getPatternId() == patternId) {
				return patternDataet;
			}
		}
		return null;
		
	} // end of method
	
	/**
	 * remove PatternDatset
	 */
	public void removePatternDataset(int patternId) {
		
		// check registered element
		DetectionPatternDataset registeredPatternDataset = 
				getPatternDataset(patternId);
		if (registeredPatternDataset != null) {
			this.patternDatasetRepository.remove(registeredPatternDataset);
		}
	
	} // end of method
	
	/**
	 * Get all patternDataset
	 */
	public DetectionPatternDataset[] getAllPatternDataset() {
		
		// get all dataset
		DetectionPatternDataset[] results = 
				this.patternDatasetRepository.toArray(
						new DetectionPatternDataset[this.patternDatasetRepository.size()]);
		
		// sort with patternId
		Arrays.sort(results, new Comparator<DetectionPatternDataset>() {
			
			public int compare(DetectionPatternDataset t1, DetectionPatternDataset t2) {
				return t1.getPatternId() - t2.getPatternId();
			}
		});
		
		// return
		return results;
		
	} // end of method

	/**
	 * @return the factoryId
	 */
	public String getFactoryId() {
		return factoryId;
	}

	/**
	 * @param factoryId the factoryId to set
	 */
	public void setFactoryId(String factoryId) {
		this.factoryId = factoryId;
	}

	/**
	 * @return the stackId
	 */
	public int getStackId() {
		return stackId;
	}

	/**
	 * @param stackId the stackId to set
	 */
	public void setStackId(int stackId) {
		this.stackId = stackId;
	}

	/**
	 * @return the factoryName
	 */
	public String getFactoryName() {
		return factoryName;
	}

	/**
	 * @param factoryName the factoryName to set
	 */
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}

	/**
	 * @return the stackName
	 */
	public String getStackName() {
		return stackName;
	}

	/**
	 * @param stackName the stackName to set
	 */
	public void setStackName(String stackName) {
		this.stackName = stackName;
	}
	
	
} // end of class
