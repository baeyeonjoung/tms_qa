/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import java.util.Vector;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.TmsDataCell;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.util.DateUtil;

/**
 * Data Element for TMS Data
 * 
 */
public class PatternRawDataRecord {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String          timestr = "";
	private  long            time = 0L;
	
	private  Vector<TmsDataCell> dataCellArray = new Vector<TmsDataCell>();
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public PatternRawDataRecord() {}
	
	public PatternRawDataRecord(TmsDataRecord tmsDataReocrd) {
		
		this.time = tmsDataReocrd.getTime();
		this.timestr = DateUtil.getDateStr(time);
		
		int[] items = ItemCodeDefine.getAllItems();
		TmsDataCell cell;
		for (int i=0 ; i<items.length ; i++) {
			cell = tmsDataReocrd.getDataCell(items[i]);
			if (cell.getData_value() == -1.0D) {
				continue;
			}
			this.dataCellArray.add(cell);
		}
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * get time
	 */
	public long getTime() {
		return this.time;
	} // end of method
	
	/**
	 * get time
	 */
	public String getTimeStr() {
		return this.timestr;
	} // end of method

	
	/**
	 * get all datacell
	 */
	public TmsDataCell[] getAllDataCell() {
		return this.dataCellArray.toArray(new TmsDataCell[this.dataCellArray.size()]);
	} // end of method
	
	/**
	 * get dataCell
	 */
	public TmsDataCell getDataCell(int item) {
		
		TmsDataCell dataCell;
		for (int i=0 ; i<this.dataCellArray.size() ; i++) {
			dataCell = this.dataCellArray.elementAt(i);
			if (dataCell.getData_type() == item) {
				return dataCell;
			}
		}
		return null;
	
	} // end of method
	
} // end of class
