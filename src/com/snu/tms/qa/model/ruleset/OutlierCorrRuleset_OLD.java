/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import com.snu.tms.qa.model.ItemCodeDefine;

/**
 * Save Outlier detection result in PCA analysis
 * 
 */
public class OutlierCorrRuleset_OLD {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private int      item1 = ItemCodeDefine.UNDEFINED;
	private int      item2 = ItemCodeDefine.UNDEFINED;
	
	private String   item1Name = "";
	private String   item2Name = "";
	
	private double   rSquare = 0.0D;
	private double   pValue  = 0.0D;
	
	private boolean  isEnabled = false;
	
	private double[][]   covMatrix;
	private double[][]   imvCovMatrix;
	private double[]     mean;
	private double[]     sigma;
	
	private double   confidenceLevel = 0.95D;
	private double   threshold = 0.0D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Save Outlier detection result in Correlation analysis
	 */
	public OutlierCorrRuleset_OLD() {}
	
	public OutlierCorrRuleset_OLD(
			int         item1,
			int         item2,
			double      rSquare,
			double      pValue,
			boolean     isEnabled,
			double[][]  covMatrix,
			double[][]  imvCovMatrix,
			double      confidenceLevel,
			double      threshold) {
		
		this.item1 = item1;
		this.item2 = item2;
		this.rSquare = rSquare;
		this.pValue = pValue;
		this.isEnabled = isEnabled;
		this.covMatrix = covMatrix;
		this.imvCovMatrix = imvCovMatrix;
		this.confidenceLevel = confidenceLevel;
		this.threshold = threshold;

	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// return
		return infoStrBuffer.toString();

	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the item1
	 */
	public int getItem1() {
		return item1;
	}

	/**
	 * @param item1 the item1 to set
	 */
	public void setItem1(int item1) {
		this.item1 = item1;
	}

	/**
	 * @return the item2
	 */
	public int getItem2() {
		return item2;
	}

	/**
	 * @param item2 the item2 to set
	 */
	public void setItem2(int item2) {
		this.item2 = item2;
	}

	/**
	 * @return the item1Name
	 */
	public String getItem1Name() {
		return item1Name;
	}

	/**
	 * @param item1Name the item1Name to set
	 */
	public void setItem1Name(String item1Name) {
		this.item1Name = item1Name;
	}

	/**
	 * @return the item2Name
	 */
	public String getItem2Name() {
		return item2Name;
	}

	/**
	 * @param item2Name the item2Name to set
	 */
	public void setItem2Name(String item2Name) {
		this.item2Name = item2Name;
	}

	/**
	 * @return the rSquare
	 */
	public double getrSquare() {
		return rSquare;
	}

	/**
	 * @param rSquare the rSquare to set
	 */
	public void setrSquare(double rSquare) {
		this.rSquare = rSquare;
	}

	/**
	 * @return the pValue
	 */
	public double getpValue() {
		return pValue;
	}

	/**
	 * @param pValue the pValue to set
	 */
	public void setpValue(double pValue) {
		this.pValue = pValue;
	}

	/**
	 * @return the mean
	 */
	public double[] getMean() {
		return mean;
	}

	/**
	 * @param mean the mean to set
	 */
	public void setMean(double[] mean) {
		this.mean = mean;
	}

	/**
	 * @return the sigma
	 */
	public double[] getSigma() {
		return sigma;
	}

	/**
	 * @param sigma the sigma to set
	 */
	public void setSigma(double[] sigma) {
		this.sigma = sigma;
	}

	/**
	 * @return the isEnabled
	 */
	public boolean isEnabled() {
		return isEnabled;
	}

	/**
	 * @param isEnabled the isEnabled to set
	 */
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	/**
	 * @return the covMatrix
	 */
	public double[][] getCovMatrix() {
		return covMatrix;
	}

	/**
	 * @param covMatrix the covMatrix to set
	 */
	public void setCovMatrix(double[][] covMatrix) {
		this.covMatrix = covMatrix;
	}

	/**
	 * @return the imvCovMatrix
	 */
	public double[][] getImvCovMatrix() {
		return imvCovMatrix;
	}

	/**
	 * @param imvCovMatrix the imvCovMatrix to set
	 */
	public void setImvCovMatrix(double[][] imvCovMatrix) {
		this.imvCovMatrix = imvCovMatrix;
	}

	/**
	 * @return the confidenceLevel
	 */
	public double getConfidenceLevel() {
		return confidenceLevel;
	}

	/**
	 * @param confidenceLevel the confidenceLevel to set
	 */
	public void setConfidenceLevel(double confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}

	/**
	 * @return the threshold
	 */
	public double getThreshold() {
		return threshold;
	}

	/**
	 * @param threshold the threshold to set
	 */
	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}
	
	
} // end of class
