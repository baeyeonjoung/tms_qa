/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

/**
 * Save Outlier detection result
 * 
 */
public class DetectionRulesetResult
{

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  double      result = 0.0D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Save Outlier detection result in Correlation analysis
	 */
	public DetectionRulesetResult() {}
	
	public DetectionRulesetResult(double result) {
		this.result = result;
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the result
	 */
	public double getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(double result) {
		this.result = result;
	}

	
	
} // end of class
