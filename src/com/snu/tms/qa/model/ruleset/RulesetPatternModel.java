/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import java.util.Vector;

import com.snu.tms.qa.model.PatternDataHistogram;

/**
 * Pattern Data Set
 * 
 */
public class RulesetPatternModel implements Comparable<RulesetPatternModel> {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private int        patternId = 0;
	private String     patternName = "";
	
	private  boolean   isEnabled        = true;
	private  boolean   isRulesetCreated = false;
	private  boolean   isOperateStop    = false;

	private int[]      items;

	private  long       patternBaseStartTime = 0L;
	private  long       patternBaseEndTime   = 0L;

	private  PatternDataHistogram[]    patternDataHistograms = null;
	
	private  PcaResultModel            pcaResultModel;
	
	private  Vector<DetectionRuleset>  detectionRulesets = new Vector<DetectionRuleset>();
	
	//==========================================================================
	// Constructors
	//==========================================================================
	public RulesetPatternModel() {}
	
	public RulesetPatternModel(
			int      patternId,
			String   patternName,
			boolean  isEnabled,
			boolean  isRulesetCreated,
			boolean  isOperateStop,
			int      itemCount,
			String   itemStr,
			long     patternBaseStartTime,
			long     patternBaseEndTime) {
		
		this.patternId             = patternId;
		this.patternName           = patternName;
		this.isEnabled             = isEnabled;
		this.isRulesetCreated      = isRulesetCreated;
		this.isOperateStop         = isOperateStop;
		this.patternBaseStartTime  = patternBaseStartTime;
		this.patternBaseEndTime    = patternBaseEndTime;
		
		setItems(itemStr);
		this.patternDataHistograms = new PatternDataHistogram[this.items.length];
		for (int i=0 ; i<this.items.length ; i++) {
			this.patternDataHistograms[i] = new PatternDataHistogram(
					this.patternId, this.items[i]);
		}
		
	} // end of method
	
	public RulesetPatternModel(
			int      patternId,
			String   patternName,
			boolean  isEnabled,
			boolean  isRulesetCreated,
			boolean  isOperateStop,
			int[]    items,
			long     patternBaseStartTime,
			long     patternBaseEndTime,
			PatternDataHistogram[] patternDataHistograms,
			PcaResultModel pcaResultModel,
			DetectionRuleset[] detectionRulesets) {
		
		this.patternId             = patternId;
		this.patternName           = patternName;
		this.isEnabled             = isEnabled;
		this.isRulesetCreated      = isRulesetCreated;
		this.isOperateStop         = isOperateStop;
		this.items                 = items;
		this.patternBaseStartTime  = patternBaseStartTime;
		this.patternBaseEndTime    = patternBaseEndTime;
		this.patternDataHistograms = patternDataHistograms;
		this.pcaResultModel        = pcaResultModel;
		for (DetectionRuleset detectionRuleset : detectionRulesets) {
			this.detectionRulesets.add(detectionRuleset);
		}
		
	} // end of method

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implements compareTo
	 */
	public int compareTo(RulesetPatternModel compare) {
		
		if (this.patternId > compare.patternId) {
			return 1;
		} else if (this.patternId < compare.patternId) {
			return -1;
		} else {
			return 0;
		}
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the patternId
	 */
	public int getPatternId() {
		return patternId;
	}

	/**
	 * @param patternId the patternId to set
	 */
	public void setPatternId(int patternId) {
		this.patternId = patternId;
	}

	/**
	 * @return the patternName
	 */
	public String getPatternName() {
		return patternName;
	}

	/**
	 * @param patternName the patternName to set
	 */
	public void setPatternName(String patternName) {
		this.patternName = patternName;
	}

	/**
	 * @return the isEnabled
	 */
	public boolean isEnabled() {
		return isEnabled;
	}

	/**
	 * @param isEnabled the isEnabled to set
	 */
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	/**
	 * @return the isRulesetCreated
	 */
	public boolean isRulesetCreated() {
		return isRulesetCreated;
	}

	/**
	 * @param isRulesetCreated the isRulesetCreated to set
	 */
	public void setRulesetCreated(boolean isRulesetCreated) {
		this.isRulesetCreated = isRulesetCreated;
	}

	/**
	 * get item count
	 */
	public int getItemCount() {
		
		if (this.items == null) {
			return 0;
		} else {
			return this.items.length;
		}
	
	} // end of method
	
	/**
	 * @return the items
	 */
	public int[] getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(int[] items) {
		this.items = items;
	}
	
	/**
	 * get item string
	 */
	public String getItemStr() {
		
		if (this.items == null) {
			return "";
		}

		String infoStr = "";
		int N = this.items.length;
		for (int i=0 ; i<N ; i++) {
			infoStr += String.format("%d", this.items[i]);
			if (i != (N-1)) {
				infoStr += "|";
			}
		}
		return infoStr;
		
	} // end of method
	
	/**
	 * parse item string
	 */
	public void setItems(String itemStr) {

		// parse
		String[] splitStr = itemStr.trim().split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}

		try {
			// set item
			int N = splitStr.length;
			this.items = new int[N];
			for (int i=0 ; i<N ; i++) {
				this.items[i] = Integer.parseInt(splitStr[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	} // end of method

	/**
	 * @return the patternBaseStartTime
	 */
	public long getPatternBaseStartTime() {
		return patternBaseStartTime;
	}

	/**
	 * @param patternBaseStartTime the patternBaseStartTime to set
	 */
	public void setPatternBaseStartTime(long patternBaseStartTime) {
		this.patternBaseStartTime = patternBaseStartTime;
	}

	/**
	 * @return the patternBaseEndTime
	 */
	public long getPatternBaseEndTime() {
		return patternBaseEndTime;
	}

	/**
	 * @param patternBaseEndTime the patternBaseEndTime to set
	 */
	public void setPatternBaseEndTime(long patternBaseEndTime) {
		this.patternBaseEndTime = patternBaseEndTime;
	}

	/**
	 * get pattern data histogram
	 */
	public PatternDataHistogram getPatternDataHistogram(int item) {
		
		// check null
		if (this.patternDataHistograms == null) {
			return null;
		}
		
		// search pattern data histogram
		for (PatternDataHistogram patternDataHistogram : this.patternDataHistograms) {
			if (patternDataHistogram.getItem() == item) {
				return patternDataHistogram;
			}
		}
		return null;
		
	} // end of method
	
	/**
	 * @return the patternDataHistograms
	 */
	public PatternDataHistogram[] getPatternDataHistograms() {
		return patternDataHistograms;
	}

	/**
	 * @param patternDataHistograms the patternDataHistograms to set
	 */
	public void setPatternDataHistograms(
			PatternDataHistogram[] patternDataHistograms) {
		this.patternDataHistograms = patternDataHistograms;
	}

	/**
	 * @return the detectionRulesets
	 */
	public DetectionRuleset[] getDetectionRulesets() {
		return detectionRulesets.toArray(new DetectionRuleset[this.detectionRulesets.size()]);
	}

	/**
	 * @param detectionRulesets the detectionRulesets to set
	 */
	public void setDetectionRulesets(DetectionRuleset[] detectionRulesets) {

		for (DetectionRuleset detectionRuleset : detectionRulesets) {
			this.detectionRulesets.add(detectionRuleset);
		}

	} // end of method
	
	/**
	 * add Detection ruleset
	 */
	public void addDetectionRuleset(DetectionRuleset detectionRuleset) {
		this.detectionRulesets.add(detectionRuleset);
	}

	/**
	 * @return the pcaResultModel
	 */
	public PcaResultModel getPcaResultModel() {
		return pcaResultModel;
	}

	/**
	 * @param pcaResultModel the pcaResultModel to set
	 */
	public void setPcaResultModel(PcaResultModel pcaResultModel) {
		this.pcaResultModel = pcaResultModel;
	}

	/**
	 * @return the isOperateStop
	 */
	public boolean isOperateStop() {
		return isOperateStop;
	}

	/**
	 * @param isOperateStop the isOperateStop to set
	 */
	public void setOperateStop(boolean isOperateStop) {
		this.isOperateStop = isOperateStop;
	}
	
	/**
	 * get pattern conformity
	 */
	public double getRulesetPatternConformity(int[] items, double[] values) {
		
		double score = 0.0D;
		PatternDataHistogram histogram;
		for (int i=0 ; i<items.length ; i++) {
			
			histogram = this.getPatternDataHistogram(items[i]);
			score += histogram.getConformityScore(values[i]);
			//System.out.println(String.format("Item[%10s], value[%6.3f], score[%6.3f]", ItemCodeDefine.ITEM_NAME[items[i]], values[i], histogram.getConformityScore(values[i])));
		}
		return score;
		
	} // end of method


} // end of class
