/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

/**
 * Weighting factor for ruleset
 * 
 */
public class RulesetWeightingModel
{
	//==========================================================================
	// Static Fields
	//==========================================================================
	public final static int  DETECTION_LEVEL_NON      = 0;
	public final static int  DETECTION_LEVEL_WARNING  = 1;
	public final static int  DETECTION_LEVEL_MINOR    = 2;
	public final static int  DETECTION_LEVEL_MAJOR    = 3;
	public final static int  DETECTION_LEVEL_CRITICAL = 4;
	
	public final static String[]  DETECTION_LEVEL_NAMES = 
		{"Normal", "Warning", "Minor", "Major", "Critical"};
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int  multivariateRulesetWeighting = 1;
	private  int  bivariateRulesetWeighting    = 1;
	private  int  univariateRulesetWeighting   = 1;
	private  int  correctionRulesetWeighting   = 1;
	
	private  int  warningLevel    = 2;
	private  int  minorLevel      = 4;
	private  int  majorLevel      = 6;
	private  int  criticalLevel   = 8;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Weighting factor for ruleset
	 */
	public RulesetWeightingModel() {}

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the multivariateRulesetWeighting
	 */
	public int getMultivariateRulesetWeighting() {
		return multivariateRulesetWeighting;
	}

	/**
	 * @param multivariateRulesetWeighting the multivariateRulesetWeighting to set
	 */
	public void setMultivariateRulesetWeighting(int multivariateRulesetWeighting) {
		this.multivariateRulesetWeighting = multivariateRulesetWeighting;
	}

	/**
	 * @return the bivariateRulesetWeighting
	 */
	public int getBivariateRulesetWeighting() {
		return bivariateRulesetWeighting;
	}

	/**
	 * @param bivariateRulesetWeighting the bivariateRulesetWeighting to set
	 */
	public void setBivariateRulesetWeighting(int bivariateRulesetWeighting) {
		this.bivariateRulesetWeighting = bivariateRulesetWeighting;
	}

	/**
	 * @return the univariateRulesetWeighting
	 */
	public int getUnivariateRulesetWeighting() {
		return univariateRulesetWeighting;
	}

	/**
	 * @param univariateRulesetWeighting the univariateRulesetWeighting to set
	 */
	public void setUnivariateRulesetWeighting(int univariateRulesetWeighting) {
		this.univariateRulesetWeighting = univariateRulesetWeighting;
	}

	/**
	 * @return the correctionRulesetWeighting
	 */
	public int getCorrectionRulesetWeighting() {
		return correctionRulesetWeighting;
	}

	/**
	 * @param correctionRulesetWeighting the correctionRulesetWeighting to set
	 */
	public void setCorrectionRulesetWeighting(int correctionRulesetWeighting) {
		this.correctionRulesetWeighting = correctionRulesetWeighting;
	}
	
	/**
	 * Get Level
	 */
	public int getLevel(int score) {
		
		if (score < this.warningLevel) {
			return DETECTION_LEVEL_NON;
		} else if (score < this.minorLevel) {
			return DETECTION_LEVEL_WARNING;
		} else if (score < this.majorLevel) {
			return DETECTION_LEVEL_MINOR;
		} else if (score < this.criticalLevel) {
			return DETECTION_LEVEL_MAJOR;
		} else {
			return DETECTION_LEVEL_CRITICAL;
		}
		
	}

	/**
	 * @return the warningLevel
	 */
	public int getWarningLevel() {
		return warningLevel;
	}

	/**
	 * @param warningLevel the warningLevel to set
	 */
	public void setWarningLevel(int warningLevel) {
		this.warningLevel = warningLevel;
	}

	/**
	 * @return the minorLevel
	 */
	public int getMinorLevel() {
		return minorLevel;
	}

	/**
	 * @param minorLevel the minorLevel to set
	 */
	public void setMinorLevel(int minorLevel) {
		this.minorLevel = minorLevel;
	}

	/**
	 * @return the majorLevel
	 */
	public int getMajorLevel() {
		return majorLevel;
	}

	/**
	 * @param majorLevel the majorLevel to set
	 */
	public void setMajorLevel(int majorLevel) {
		this.majorLevel = majorLevel;
	}

	/**
	 * @return the criticalLevel
	 */
	public int getCriticalLevel() {
		return criticalLevel;
	}

	/**
	 * @param criticalLevel the criticalLevel to set
	 */
	public void setCriticalLevel(int criticalLevel) {
		this.criticalLevel = criticalLevel;
	}

} // end of class
