/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

/**
 * The model saving the result of PCA
 * 
 */
public class PcaResultModel {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private int[]      items;
	private int        pcCount = 0;
	private double[][] eigenVector;
	private double[]   eigenValues;
	private double[]   neigenValues;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for PcaResultModel
	 */
	public PcaResultModel() {}
	
	public PcaResultModel(
			int[]      items,
			int        pcCount,
			double[][] eigenVector,
			double[]   eigenValues,
			double[]   neigenValues) {
		
		this.items = items;
		this.pcCount = pcCount;
		this.eigenVector = eigenVector;
		this.eigenValues = eigenValues;
		this.neigenValues = neigenValues;
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Get projection value
	 */
	public double[] getProjectionValues(double[] itemValues) {
		
		// make matrix for item values
		double[][] originValues = new double[1][];
		originValues[0] = itemValues;
		BlockRealMatrix mData = new BlockRealMatrix(originValues);
		
		// project the original dataset to principal component axis
		BlockRealMatrix mPC = new BlockRealMatrix(eigenVector); 
		RealMatrix mSignals = mPC.multiply(mData.transpose()).transpose();
		
		// return
		return mSignals.getRow(0);
		
	} // end of method
	
	/**
	 * get items string
	 */
	public String getItemStr() {
		
		String infoStr = "";
		int N = this.items.length;
		for (int i=0 ; i<N ; i++) {
			infoStr += String.format("%d", this.items[i]);
			if (i != (N-1)) {
				infoStr += "|";
			}
		}
		return infoStr;
		
	} // end of method
	
	/**
	 * get eigen_vector string
	 */
	public String getEigenVectorStr() {
		
		String infoStr = "";
		int N = this.eigenVector.length;
		for (int i=0 ; i<N ; i++) {
			
			int M = this.eigenVector[i].length;
			for (int j=0 ; j<this.eigenVector[i].length ; j++) {

				infoStr += String.format("%.3f", this.eigenVector[i][j]);
				if (j != (M-1)) {
					infoStr += ",";
				}

			} // end of for-loop
			
			if (i != (N-1)) {
				infoStr += "|";
			}
			
		} // end of for-loop
		
		return infoStr;
		
	} // end of method
	
	/**
	 * get items string
	 */
	public String getEigenValueStr() {
		
		String infoStr = "";
		int N = this.eigenValues.length;
		for (int i=0 ; i<N ; i++) {
			infoStr += String.format("%.3f", this.eigenValues[i]);
			if (i != (N-1)) {
				infoStr += "|";
			}
		}
		return infoStr;
		
	} // end of method
	
	/**
	 * get items string
	 */
	public String getNeigenValueStr() {
		
		String infoStr = "";
		int N = this.neigenValues.length;
		for (int i=0 ; i<N ; i++) {
			infoStr += String.format("%.3f", this.neigenValues[i]);
			if (i != (N-1)) {
				infoStr += "|";
			}
		}
		return infoStr;
		
	} // end of method
	
	/**
	 * @return the items
	 */
	public int[] getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(int[] items) {
		this.items = items;
	}

	/**
	 * parse item string
	 */
	public void setItems(String itemStr) {

		// parse
		String[] splitStr = itemStr.trim().split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}

		try {
			// set item
			int N = splitStr.length;
			this.items = new int[N];
			for (int i=0 ; i<N ; i++) {
				this.items[i] = Integer.parseInt(splitStr[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	} // end of method

	/**
	 * @return the pcCount
	 */
	public int getPcCount() {
		return pcCount;
	}

	/**
	 * @param pcCount the pcCount to set
	 */
	public void setPcCount(int pcCount) {
		this.pcCount = pcCount;
	}

	/**
	 * @return the eigenVector
	 */
	public double[][] getEigenVector() {
		return eigenVector;
	}

	/**
	 * @param eigenVector the eigenVector to set
	 */
	public void setEigenVector(double[][] eigenVector) {
		this.eigenVector = eigenVector;
	}
	
	/**
	 * @param densities the densities to set
	 */
	public void setEigenVector(String dataStr) {
		
		// split target string
		String[] splitStr = dataStr.split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}
		
		// memory allocation
		this.eigenVector = new double[splitStr.length][];
		
		// parse
		for (int i=0 ; i<splitStr.length ; i++) {
			
			String[] detailStr = splitStr[i].trim().split(",");
			if (detailStr == null || detailStr.length == 0) {
				continue;
			}
			
			this.eigenVector[i] = new double[detailStr.length];
			for (int j=0 ; j<detailStr.length ; j++) {
				this.eigenVector[i][j] = Double.parseDouble(detailStr[j]);
			}
			
		} // end of for-loop
		
	} // end of method


	/**
	 * @return the eigenValues
	 */
	public double[] getEigenValues() {
		return eigenValues;
	}

	/**
	 * @param eigenValues the eigenValues to set
	 */
	public void setEigenValues(double[] eigenValues) {
		this.eigenValues = eigenValues;
	}
	
	/**
	 * @param densities the densities to set
	 */
	public void setEigenValues(String dataStr) {
		
		// split target string
		String[] splitStr = dataStr.split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}
		
		// memory allocation
		this.eigenValues = new double[splitStr.length];
		
		// parse
		for (int i=0 ; i<splitStr.length ; i++) {
			this.eigenValues[i] = Double.parseDouble(splitStr[i]);
		} // end of for-loop
		
	} // end of method

	/**
	 * @return the neigenValues
	 */
	public double[] getNeigenValues() {
		return neigenValues;
	}

	/**
	 * @param neigenValues the neigenValues to set
	 */
	public void setNeigenValues(double[] neigenValues) {
		this.neigenValues = neigenValues;
	}
	
	/**
	 * @param densities the densities to set
	 */
	public void setNeigenValues(String dataStr) {
		
		// split target string
		String[] splitStr = dataStr.split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}
		
		// memory allocation
		this.neigenValues = new double[splitStr.length];
		
		// parse
		for (int i=0 ; i<splitStr.length ; i++) {
			this.neigenValues[i] = Double.parseDouble(splitStr[i]);
		} // end of for-loop
		
	} // end of method
	
} // end of class
