/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import java.util.Hashtable;

import com.snu.tms.qa.util.DateUtil;

/**
 * Outlier Detection Record
 * 
 */
public class OdrRecord  implements Comparable<OdrRecord>
{
	//==========================================================================
	// Static Fields
	//==========================================================================
	public final static int  OUTLIER_LEVEL_NON      = 0;
	public final static int  OUTLIER_LEVEL_WARNING  = 1;
	public final static int  OUTLIER_LEVEL_MINOR    = 2;
	public final static int  OUTLIER_LEVEL_MAJOR    = 3;
	public final static int  OUTLIER_LEVEL_CRITICAL = 4;
	
	public final static String[]  OUTLIER_LEVEL_STR = 
		{ "", "WARNING", "MINOR", "MAJOR", "CRITICAL"};
	
	//==========================================================================
	// Fields
	//==========================================================================
	public String      factCode;
	public String      factName;
	public int         stckCode;
	public  long       time;
	public  int        patternId;
	public  String     patternName;
	public  OdrCell[]  odrCells;
	
	public  RulesetWeightingModel rulesetWeighingModel;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	public OdrRecord(
			String factCode, 
			String factName, 
			int stckCode, 
			long time, 
			int patternId, 
			String patternName, 
			Hashtable<String, Integer> rulesetOrderHash,
			RulesetWeightingModel rulesetWeighingModel) {
		
		this.factCode = factCode;
		this.factName = factName;
		this.stckCode = stckCode;
		this.time = time;
		this.patternId = patternId;
		this.patternName = patternName;
		this.odrCells = new OdrCell[rulesetOrderHash.size()];
		
		this.rulesetWeighingModel = rulesetWeighingModel;
		
		String[] odrKeys = rulesetOrderHash.keySet().toArray(new String[rulesetOrderHash.size()]);
		int index;
		for (int i=0 ; i<odrKeys.length ; i++) {
			index = rulesetOrderHash.get(odrKeys[i]);
			this.odrCells[index] = new OdrCell(odrKeys[i], this.rulesetWeighingModel);
		}
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * compareTo()
	 */
	public int compareTo(OdrRecord compare) {
		
		if (this.time > compare.time) {
			return 1;
		} else if (this.time < compare.time) {
			return -1;
		} else {
			return 0;
		}
	} // end of method
	
	/**
	 * toString
	 */
	public String toString() {
		
		String infoStr = "";
		infoStr += DateUtil.getDateStr(this.time);
		infoStr += "\t" + String.format("Outlier-Level : %-10s, patternId : %3d, Pattern Name : %s\n", 
				this.getOutlierLevelStr(), this.patternId, this.patternName);
		for (OdrCell odrCell : this.odrCells) {
			infoStr += "\t" + String.format("RuleTypeName : %-15s, Is-Outlier : %s\n", 
					odrCell.ruleTypeName, (odrCell.isOutlier == 1) ? "TRUE" : "FALSE");
		}
		
		return infoStr;
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * get Outlier count
	 */
	public int getOutlierCount() {
		
		int outlierCount = 0;
		for (int i=0 ; i<odrCells.length ; i++) {
			if (odrCells[i].isOutlier == 1) {
				outlierCount++;
			}
		}
		return outlierCount;
		
	} // end of method
	
	/**
	 * get Outlier Score
	 */
	public int getOutlierScore() {
		
		int score = 0;
		for (int i=0 ; i<odrCells.length ; i++) {
			score += odrCells[i].getScore();
		}
		return score;
		
	} // end of method
	
	/**
	 * get outlier level
	 */
	public int getOutlierLevel() {
		
		int score = getOutlierScore();
		if (score < rulesetWeighingModel.getWarningLevel()) {
			return OUTLIER_LEVEL_NON;
		} else if (score < rulesetWeighingModel.getMinorLevel()) {
			return OUTLIER_LEVEL_WARNING;
		} else if (score < rulesetWeighingModel.getMajorLevel()) {
			return OUTLIER_LEVEL_MINOR;
		} else if (score < rulesetWeighingModel.getCriticalLevel()) {
			return OUTLIER_LEVEL_MAJOR;
		} else {
			return OUTLIER_LEVEL_CRITICAL;
		}
		
	} // end of method
	
	/**
	 * get outlier level string
	 */
	public String getOutlierLevelStr() {
		return OUTLIER_LEVEL_STR[getOutlierLevel()];
	} // end of method
	
} // end of class
