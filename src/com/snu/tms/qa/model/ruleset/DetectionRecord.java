/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import java.util.Enumeration;
import java.util.Vector;

import com.snu.tms.qa.util.DateUtil;

/**
 * data and result for applying ruleset
 * 
 */
public class DetectionRecord implements Comparable<DetectionRecord>{

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String          timestr = "";
	private  long            time = 0L;
	private  int             patternId = 0;
	private  String          patternName = "";
	
	private  Vector<RawDataCell> rawDataCellArray = new Vector<RawDataCell>();
	
	private  Vector<DetectionDataCell> detectionDataCellArray = new Vector<DetectionDataCell>();
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public DetectionRecord() {}
	
	public DetectionRecord(
			DetectionPatternDataset detectionPatternDataset, 
			long time, 
			int  patternId,
			String patternName,
			RawDataCell[] rawDataCells) {
		
		// set Time parameter
		this.patternId = patternId;
		this.patternName = patternName;
		this.time = time;
		this.timestr = DateUtil.getDateStr(time);
		
		// set rawdata
		for (int i=0 ; i<rawDataCells.length ; i++) {
			this.rawDataCellArray.add(rawDataCells[i]);
		}
		
		// set detectionCell
		
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * compare
	 */
	public int compareTo(DetectionRecord compare) {
		
		if (this.time > compare.time) {
			return 1;
		} else if (this.time < compare.time) {
			return -1;
		} else {
			return 0;
		}
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * get time
	 */
	public long getTime() {
		return this.time;
	} // end of method
	
	/**
	 * get time
	 */
	public String getTimeStr() {
		return this.timestr;
	} // end of method

	
	/**
	 * get all datacell
	 */
	public RawDataCell[] getAllRawDataCell() {
		return this.rawDataCellArray.toArray(new RawDataCell[this.rawDataCellArray.size()]);
	} // end of method
	
	/**
	 * get dataCell
	 */
	public RawDataCell getRawDataCell(int item) {
		
		RawDataCell dataCell;
		for (Enumeration<RawDataCell> e=this.rawDataCellArray.elements() ; e.hasMoreElements() ; ) {
			dataCell = e.nextElement();
			if (dataCell.getData_type() == item) {
				return dataCell;
			}
		}
		return null;
	
	} // end of method
	
	/**
	 * get datacells
	 */
	public RawDataCell[] getRawDataCell(int[] items) {
		
		RawDataCell[] dataCells = new RawDataCell[items.length];
		for (int i=0 ; i<items.length ; i++) {
			dataCells[i] = getRawDataCell(items[i]);
		}
		return dataCells;
		
	} // end of method
	
	/**
	 * get datacells
	 */
	public double[] getRawDataCellValue(int[] items) {
		
		double[] dataCellValues = new double[items.length];
		for (int i=0 ; i<items.length ; i++) {
			dataCellValues[i] = getRawDataCell(items[i]).getData_value();
		}
		return dataCellValues;
		
	} // end of method
	
	/**
	 * Add DetectionDataCell
	 */
	public DetectionDataCell addDetectionDataCell(DetectionRuleset ruleset) {
		
		DetectionDataCell detectionDataCell = new DetectionDataCell(ruleset, this);
		this.detectionDataCellArray.add(detectionDataCell);
		return detectionDataCell;
		
	} // end of method
	
	/**
	 * get all detectionCell
	 */
	public DetectionDataCell[] getAllDetectionDataCell() {
		return this.detectionDataCellArray.toArray(
				new DetectionDataCell[this.detectionDataCellArray.size()]);
	} // end of method
	
	/**
	 * get detectionCell
	 */
	public DetectionDataCell getDetectionDataCell(DetectionRuleset ruleset) {
		
		DetectionDataCell dataCell;
		for (Enumeration<DetectionDataCell> e=this.detectionDataCellArray.elements() ; e.hasMoreElements() ;) {
			dataCell = e.nextElement();
			if (dataCell.getRuleset().isSameRuleset(ruleset)) {
				return dataCell;
			} else {
				continue;
			}
		}
		return null;
			
	} // end of method
	
	/**
	 * get detectionCell
	 */
	public DetectionDataCell getDetectionDataCell(String keyName) {
		
		DetectionDataCell dataCell;
		for (Enumeration<DetectionDataCell> e=this.detectionDataCellArray.elements() ; e.hasMoreElements() ;) {
			dataCell = e.nextElement();
			if (dataCell.getRuleset().getKeyName().equals(keyName)) {
				return dataCell;
			} else {
				continue;
			}
		}
		return null;
			
	} // end of method

	/**
	 * @return the timestr
	 */
	public String getTimestr() {
		return timestr;
	}

	/**
	 * @param timestr the timestr to set
	 */
	public void setTimestr(String timestr) {
		this.timestr = timestr;
	}

	/**
	 * @return the patternId
	 */
	public int getPatternId() {
		return patternId;
	}

	/**
	 * @param patternId the patternId to set
	 */
	public void setPatternId(int patternId) {
		this.patternId = patternId;
	}

	/**
	 * @return the patternName
	 */
	public String getPatternName() {
		return patternName;
	}

	/**
	 * @param patternName the patternName to set
	 */
	public void setPatternName(String patternName) {
		this.patternName = patternName;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}
	
	
	
} // end of class
