/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import com.snu.tms.qa.model.ItemCodeDefine;

/**
 * Save Outlier detection result in Percent of Difference
 * 
 */
public class RulesetPd implements DetectionRuleset 
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int         rulesetType = DetectionRuleset.RULESET_PD;

	private  int         rulesetId;
	private  int         patternId;
	private  boolean     rulesetEnabled = false;
	private  double[]    thresholds;

	private  int         item;
	private  double      mean = 0.0D;
	private  double      sigma = 0.0D;
	private  double      confidenceLevel = 0.95D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Save Outlier detection result in Percent of Difference
	 */
	public RulesetPd() {}

	public RulesetPd(int item) {
		this.item = item;
	}

	public RulesetPd(
			int         rulesetId,
			int         patternId,
			int         item,
			double      mean,
			double      sigma) {
	
		this.rulesetId    = rulesetId;
		this.patternId    = patternId;
		this.item         = item;
		this.mean         = mean;
		this.sigma        = sigma;
	}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * get ruleset KeyName
	 */
	public String getKeyName() {
		return String.format("PD[%s]", ItemCodeDefine.getItemName(item));
	} // end of method
	
	/**
	 * get ruleset id
	 */
	public int getRulesetId() {
		return rulesetId;
	} // end of method
	
	/**
	 * set ruleset id
	 */
	public void setRulesetId(int rulesetId) {
		this.rulesetId = rulesetId;
	} // end of method
	
	/**
	 * get pattern type
	 */
	public int getRulesetType() {
		return rulesetType;
	} // end of method

	/**
	 * @return the patternId
	 */
	public int getPatternId() {
		return patternId;
	}

	/**
	 * @param patternId the patternId to set
	 */
	public void setPatternId(int patternId) {
		this.patternId = patternId;
	}
	
	public double getMean() {
		return mean;
	}

	public void setMean(double mean) {
		this.mean = mean;
	}

	/**
	 * @return the sigma
	 */
	public double getSigma() {
		return sigma;
	}

	/**
	 * @param sigma the sigma to set
	 */
	public void setSigma(double sigma) {
		this.sigma = sigma;
	}

	/**
	 * is same ruleset
	 */
	public boolean isSameRuleset(DetectionRuleset ruleset) {

		if (this.rulesetType != ruleset.getRulesetType()) {
			return false;
		}
		int[] compareItems = ruleset.getItems();
		if (compareItems.length != 1) {
			return false;
		}
		if (this.item == compareItems[0]) {
			return true;
		} else {
			return false;
		}
		
	} // end of method
	
	/**
	 * get item
	 */
	public int[] getItems() {
		return new int[]{item};
	} // end of method
	
	/**
	 * apply rule set and get result
	 * parameters:
	 *    - inputValues[0] : the value of after correction process
	 *    - inputValues[1] : the original value
	 */
	public double applyRuleset(double[] inputValues) {
		
		double correctionValue, originalValue;
		correctionValue = inputValues[0];
		originalValue   = inputValues[1];
		if (originalValue == 0.0D) {
			if (correctionValue == 0.0D) {
				return 0.0D;
			} else {
				return mean;
			}
		}
		double diffValue = Math.abs(
				(correctionValue - originalValue) / originalValue * 100.0D);
		return diffValue;
		
	} // end of method
	
	/**
	 * get threshold
	 */
	public double[] getThresholds() {
		return thresholds;
	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * get item
	 */
	public int getItem() {
		return this.item;
	} // end of method

	/**
	 * @return the rulesetEnabled
	 */
	public boolean isRulesetEnabled() {
		return rulesetEnabled;
	}

	/**
	 * @param rulesetEnabled the rulesetEnabled to set
	 */
	public void setRulesetEnabled(boolean rulesetEnabled) {
		this.rulesetEnabled = rulesetEnabled;
	}

	/**
	 * @param items the items to set
	 */
	public void setItem(int item) {
		this.item = item;
	}

	/**
	 * @return the confidenceLevel
	 */
	public double getConfidenceLevel() {
		return confidenceLevel;
	}

	/**
	 * @param confidenceLevel the confidenceLevel to set
	 */
	public void setConfidenceLevel(double confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}

	/**
	 * @param rulesetType the rulesetType to set
	 */
	public void setRulesetType(int rulesetType) {
		this.rulesetType = rulesetType;
	}

	/**
	 * @param threshold the threshold to set
	 */
	public void setThresholds(double[] thresholds) {
		this.thresholds = thresholds;
	}

} // end of class
