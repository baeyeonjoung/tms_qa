/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

/**
 * Save Outlier detection result in PCA analysis
 * 
 */
public class RulesetPcaSpe implements DetectionRuleset 
{

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int         rulesetType = DetectionRuleset.RULESET_PCA_SPE;

	private  int         rulesetId;
	private  int         patternId;
	private  boolean     rulesetEnabled = true;
	private  double[]    thresholds;

	private  int[]        items;
	private  int          pcCount = 0;
	private  double[][]   eigenVector;
	private  double[]     eigenValues;
	private  double[]     neigenValues;
	private  double[]     mean;
	private  double[]     sigma;
	
	private  double       confidenceLevel = 0.95D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Save Outlier detection result in Correlation analysis
	 */
	public RulesetPcaSpe() {}
	
	public RulesetPcaSpe(int[] items) {
		this.items = items;
	}
	
	public RulesetPcaSpe(
			int         rulesetId,
			int         patternId,
			int[]       items,
			int         pcCount,
			double[][]  eigenVector,
			double[]    eigenValues,
			double[]    neigenValues) {
		
		this.rulesetId    = rulesetId;
		this.patternId    = patternId;
		this.items        = items;
		this.pcCount      = pcCount;
		this.eigenVector  = eigenVector;
		this.eigenValues  = eigenValues;
		this.neigenValues = neigenValues;

	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * get ruleset KeyName
	 */
	public String getKeyName() {
		return String.format("SPE");
	} // end of method
	
	/**
	 * get ruleset id
	 */
	public int getRulesetId() {
		return rulesetId;
	} // end of method
	
	/**
	 * set ruleset id
	 */
	public void setRulesetId(int rulesetId) {
		this.rulesetId = rulesetId;
	} // end of method
	
	/**
	 * get pattern type
	 */
	public int getRulesetType() {
		return rulesetType;
	} // end of method

	/**
	 * @return the patternId
	 */
	public int getPatternId() {
		return patternId;
	}

	/**
	 * @param patternId the patternId to set
	 */
	public void setPatternId(int patternId) {
		this.patternId = patternId;
	}

	/**
	 * is same ruleset
	 */
	public boolean isSameRuleset(DetectionRuleset ruleset) {
		return (this.rulesetType == ruleset.getRulesetType());
	} // end of method
	
	/**
	 * get item
	 */
	public int[] getItems() {
		return items;
	} // end of method
	
	/**
	 * apply rule set and get result
	 */
	public double applyRuleset(double[] inputValues) {
		
		// get projected values
		double[][] originValues = new double[1][];
		originValues[0] = inputValues;
		BlockRealMatrix mData = new BlockRealMatrix(originValues);
		BlockRealMatrix mPC = new BlockRealMatrix(this.eigenVector); 
		RealMatrix mSignals = mPC.multiply(mData.transpose()).transpose();
		double[] projectedValues = mSignals.getRow(0);
		
		// get normalized value
		for (int i=0 ; i<projectedValues.length ; i++) {
			if (this.sigma[i] != 0.0D) {
				projectedValues[i] = (projectedValues[i] - this.mean[i]) / this.sigma[i];
			}
		}

		// compute mahalanobis distance
		BlockRealMatrix mProjectedValues = new BlockRealMatrix(new double[][] {projectedValues});
		RealMatrix mQProjectedValues = mProjectedValues.multiply(mPC);
		double qDistance = Math.abs(mQProjectedValues.multiply(mProjectedValues.transpose()).getEntry(0, 0));
		return qDistance;
		
	} // end of method
	
	/**
	 * get threshold
	 */
	public double[] getThresholds() {
		return thresholds;
	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the rulesetEnabled
	 */
	public boolean isRulesetEnabled() {
		return rulesetEnabled;
	}

	/**
	 * @param rulesetEnabled the rulesetEnabled to set
	 */
	public void setRulesetEnabled(boolean rulesetEnabled) {
		this.rulesetEnabled = rulesetEnabled;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(int[] items) {
		this.items = items;
	}
	
	/**
	 * parse item string
	 */
	public void setItems(String itemStr) {

		// parse
		String[] splitStr = itemStr.trim().split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}

		try {
			// set item
			int N = splitStr.length;
			this.items = new int[N];
			for (int i=0 ; i<N ; i++) {
				this.items[i] = Integer.parseInt(splitStr[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	} // end of method

	/**
	 * @return the pcCount
	 */
	public int getPcCount() {
		return pcCount;
	}

	/**
	 * @param pcCount the pcCount to set
	 */
	public void setPcCount(int pcCount) {
		this.pcCount = pcCount;
	}

	/**
	 * @return the eigenVector
	 */
	public double[][] getEigenVector() {
		return eigenVector;
	}

	/**
	 * @param eigenVector the eigenVector to set
	 */
	public void setEigenVector(double[][] eigenVector) {
		this.eigenVector = eigenVector;
	}
	
	/**
	 * @param densities the densities to set
	 */
	public void setEigenVector(String dataStr) {
		
		// split target string
		String[] splitStr = dataStr.split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}
		
		// memory allocation
		this.eigenVector = new double[splitStr.length][];
		
		// parse
		for (int i=0 ; i<splitStr.length ; i++) {
			
			String[] detailStr = splitStr[i].trim().split(",");
			if (detailStr == null || detailStr.length == 0) {
				continue;
			}
			
			this.eigenVector[i] = new double[detailStr.length];
			for (int j=0 ; j<detailStr.length ; j++) {
				this.eigenVector[i][j] = Double.parseDouble(detailStr[j]);
			}
			
		} // end of for-loop
		
	} // end of method

	/**
	 * @return the eigenValues
	 */
	public double[] getEigenValues() {
		return eigenValues;
	}

	/**
	 * @param eigenValues the eigenValues to set
	 */
	public void setEigenValues(double[] eigenValues) {
		this.eigenValues = eigenValues;
	}
	
	/**
	 * @param densities the densities to set
	 */
	public void setEigenValues(String dataStr) {
		
		// split target string
		String[] splitStr = dataStr.split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}
		
		// memory allocation
		this.eigenValues = new double[splitStr.length];
		
		// parse
		for (int i=0 ; i<splitStr.length ; i++) {
			this.eigenValues[i] = Double.parseDouble(splitStr[i]);
		} // end of for-loop
		
	} // end of method

	/**
	 * @return the neigenValues
	 */
	public double[] getNeigenValues() {
		return neigenValues;
	}

	/**
	 * @param neigenValues the neigenValues to set
	 */
	public void setNeigenValues(double[] neigenValues) {
		this.neigenValues = neigenValues;
	}
	
	/**
	 * @param densities the densities to set
	 */
	public void setNeigenValues(String dataStr) {
		
		// split target string
		String[] splitStr = dataStr.split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}
		
		// memory allocation
		this.neigenValues = new double[splitStr.length];
		
		// parse
		for (int i=0 ; i<splitStr.length ; i++) {
			this.neigenValues[i] = Double.parseDouble(splitStr[i]);
		} // end of for-loop
		
	} // end of method

	/**
	 * @return the mean
	 */
	public double[] getMean() {
		return mean;
	}

	/**
	 * @param mean the mean to set
	 */
	public void setMean(double[] mean) {
		this.mean = mean;
	}
	
	/**
	 * @param densities the densities to set
	 */
	public void setMean(String dataStr) {
		
		// split target string
		String[] splitStr = dataStr.split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}
		
		// memory allocation
		this.mean = new double[splitStr.length];
		
		// parse
		for (int i=0 ; i<splitStr.length ; i++) {
			this.mean[i] = Double.parseDouble(splitStr[i]);
		} // end of for-loop
		
	} // end of method

	/**
	 * @return the sigma
	 */
	public double[] getSigma() {
		return sigma;
	}

	/**
	 * @param sigma the sigma to set
	 */
	public void setSigma(double[] sigma) {
		this.sigma = sigma;
	}
	
	/**
	 * @param densities the densities to set
	 */
	public void setSigma(String dataStr) {
		
		// split target string
		String[] splitStr = dataStr.split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}
		
		// memory allocation
		this.sigma = new double[splitStr.length];
		
		// parse
		for (int i=0 ; i<splitStr.length ; i++) {
			this.sigma[i] = Double.parseDouble(splitStr[i]);
		} // end of for-loop
		
	} // end of method

	/**
	 * @return the confidenceLevel
	 */
	public double getConfidenceLevel() {
		return confidenceLevel;
	}

	/**
	 * @param confidenceLevel the confidenceLevel to set
	 */
	public void setConfidenceLevel(double confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}

	/**
	 * @param rulesetType the rulesetType to set
	 */
	public void setRulesetType(int rulesetType) {
		this.rulesetType = rulesetType;
	}

	/**
	 * @param threshold the threshold to set
	 */
	public void setThresholds(double[] thresholds) {
		this.thresholds = thresholds;
	}
	
	/**
	 * get items string
	 */
	public String getItemStr() {
		
		String infoStr = "";
		int N = this.items.length;
		for (int i=0 ; i<N ; i++) {
			infoStr += String.format("%d", this.items[i]);
			if (i != (N-1)) {
				infoStr += "|";
			}
		}
		return infoStr;
		
	} // end of method
	
	/**
	 * get eigen_vector string
	 */
	public String getEigenVectorStr() {
		
		String infoStr = "";
		int N = this.eigenVector.length;
		for (int i=0 ; i<N ; i++) {
			
			int M = this.eigenVector[i].length;
			for (int j=0 ; j<this.eigenVector[i].length ; j++) {

				infoStr += String.format("%.6f", this.eigenVector[i][j]);
				if (j != (M-1)) {
					infoStr += ",";
				}

			} // end of for-loop
			
			if (i != (N-1)) {
				infoStr += "|";
			}
			
		} // end of for-loop
		
		return infoStr;
		
	} // end of method
	
	/**
	 * get items string
	 */
	public String getEigenValueStr() {
		
		String infoStr = "";
		int N = this.eigenValues.length;
		for (int i=0 ; i<N ; i++) {
			infoStr += String.format("%.6f", this.eigenValues[i]);
			if (i != (N-1)) {
				infoStr += "|";
			}
		}
		return infoStr;
		
	} // end of method
	
	/**
	 * get items string
	 */
	public String getNeigenValueStr() {
		
		String infoStr = "";
		int N = this.neigenValues.length;
		for (int i=0 ; i<N ; i++) {
			infoStr += String.format("%.6f", this.neigenValues[i]);
			if (i != (N-1)) {
				infoStr += "|";
			}
		}
		return infoStr;
		
	} // end of method

	/**
	 * get mean string
	 */
	public String getMeanStr() {
		
		String infoStr = "";
		int N = this.mean.length;
		for (int i=0 ; i<N ; i++) {
			infoStr += String.format("%.6f", this.mean[i]);
			if (i != (N-1)) {
				infoStr += "|";
			}
		}
		return infoStr;
		
	} // end of method
	
	/**
	 * get sigma string
	 */
	public String getSigmaStr() {
		
		String infoStr = "";
		int N = this.sigma.length;
		for (int i=0 ; i<N ; i++) {
			infoStr += String.format("%.6f", this.sigma[i]);
			if (i != (N-1)) {
				infoStr += "|";
			}
		}
		return infoStr;
		
	} // end of method

	
} // end of class
