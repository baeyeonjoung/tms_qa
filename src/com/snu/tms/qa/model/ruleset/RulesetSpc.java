/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

import com.snu.tms.qa.model.ItemCodeDefine;

/**
 * Save Outlier detection result in Statistic Process Control
 * 
 */
public class RulesetSpc implements DetectionRuleset 
{

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int         rulesetType = DetectionRuleset.RULESET_SPC;

	private  int         rulesetId;
	private  int         patternId;
	private  boolean     rulesetEnabled = true;
	private  double[]    thresholds;

	private  int         item;
	private  double      mean;
	private  double      sigma;
	private  double      confidenceLevel = 0.95D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Save Outlier detection result in Correlation analysis
	 */
	public RulesetSpc() {}
	
	public RulesetSpc(int item) {
		this.item = item;
	}
	
	public RulesetSpc(
			int         rulesetId,
			int         patternId,
			int[]       items,
			double      mean,
			double      sigma) {
		
		this.rulesetId    = rulesetId;
		this.patternId    = patternId;
		this.item        = items[0];
		this.mean         = mean;
		this.sigma        = sigma;
	}
	
	public RulesetSpc(
			int         rulesetId,
			int         patternId,
			int         item,
			double      mean,
			double      sigma) {
		
		this.rulesetId    = rulesetId;
		this.patternId    = patternId;
		this.item         = item;
		this.mean         = mean;
		this.sigma        = sigma;
	}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * get ruleset KeyName
	 */
	public String getKeyName() {
		return String.format("SPC[%s]", ItemCodeDefine.getItemName(item));
	} // end of method
	
	/**
	 * get ruleset id
	 */
	public int getRulesetId() {
		return rulesetId;
	} // end of method
	
	/**
	 * set ruleset id
	 */
	public void setRulesetId(int rulesetId) {
		this.rulesetId = rulesetId;
	} // end of method
	
	/**
	 * get pattern type
	 */
	public int getRulesetType() {
		return rulesetType;
	} // end of method

	/**
	 * @return the patternId
	 */
	public int getPatternId() {
		return patternId;
	}

	/**
	 * @param patternId the patternId to set
	 */
	public void setPatternId(int patternId) {
		this.patternId = patternId;
	}
	
	/**
	 * is same ruleset
	 */
	public boolean isSameRuleset(DetectionRuleset ruleset) {

		if (this.rulesetType != ruleset.getRulesetType()) {
			return false;
		}
		int[] compareItems = ruleset.getItems();
		if (compareItems.length != 1) {
			return false;
		}
		if (this.item == compareItems[0]) {
			return true;
		} else {
			return false;
		}
		
	} // end of method
	
	/**
	 * get item
	 */
	public int[] getItems() {
		return new int[]{item};
	} // end of method
	
	/**
	 * apply rule set and get result
	 */
	public double applyRuleset(double[] inputValues) {
		return inputValues[0];
	} // end of method
	
	/**
	 * get threshold
	 */
	public double[] getThresholds() {
		return thresholds;
	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * get item
	 */
	public int getItem() {
		return this.item;
	} // end of method

	/**
	 * @return the rulesetEnabled
	 */
	public boolean isRulesetEnabled() {
		return rulesetEnabled;
	}

	/**
	 * @param rulesetEnabled the rulesetEnabled to set
	 */
	public void setRulesetEnabled(boolean rulesetEnabled) {
		this.rulesetEnabled = rulesetEnabled;
	}

	/**
	 * @param items the items to set
	 */
	public void setItem(int item) {
		this.item = item;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(int[] items) {
		this.item = items[0];
	}

	/**
	 * @return the mean
	 */
	public double getMean() {
		return mean;
	}

	/**
	 * @param mean the mean to set
	 */
	public void setMean(double mean) {
		this.mean = mean;
	}

	/**
	 * @return the sigma
	 */
	public double getSigma() {
		return sigma;
	}

	/**
	 * @param sigma the sigma to set
	 */
	public void setSigma(double sigma) {
		this.sigma = sigma;
	}

	/**
	 * @return the confidenceLevel
	 */
	public double getConfidenceLevel() {
		return confidenceLevel;
	}

	/**
	 * @param confidenceLevel the confidenceLevel to set
	 */
	public void setConfidenceLevel(double confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}

	/**
	 * @param rulesetType the rulesetType to set
	 */
	public void setRulesetType(int rulesetType) {
		this.rulesetType = rulesetType;
	}

	/**
	 * @param threshold the threshold to set
	 */
	public void setThresholds(double[] thresholds) {
		this.thresholds = thresholds;
	}

} // end of class
