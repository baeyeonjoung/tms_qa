/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.ruleset;

/**
 * detection filtering model
 * 
 */
public class RulesetResultFilterModel
{

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  FilteringModelPattern[]         patternFilters = null;
	private  FilteringModelDetectionLevel[]  levelFilters = null;
	private  FilteringModelRuleset[]         rulesetFilters = null;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * detection filtering model
	 */
	public RulesetResultFilterModel(
			DetectionPatternDataset[] patternDatasets,
			String[] rulesetNames) {
		
		this.patternFilters = new FilteringModelPattern[patternDatasets.length];
		this.levelFilters   = new FilteringModelDetectionLevel[5];
		this.rulesetFilters = new FilteringModelRuleset[rulesetNames.length];
		
		for (int i=0 ; i<this.patternFilters.length ; i++) {
			this.patternFilters[i] = 
					new FilteringModelPattern(patternDatasets[i], true);
		}

		this.levelFilters[0] = new FilteringModelDetectionLevel(
				RulesetWeightingModel.DETECTION_LEVEL_NON, true);
		this.levelFilters[1] = new FilteringModelDetectionLevel(
				RulesetWeightingModel.DETECTION_LEVEL_WARNING, true);
		this.levelFilters[2] = new FilteringModelDetectionLevel(
				RulesetWeightingModel.DETECTION_LEVEL_MINOR, true);
		this.levelFilters[3] = new FilteringModelDetectionLevel(
				RulesetWeightingModel.DETECTION_LEVEL_MAJOR, true);
		this.levelFilters[4] = new FilteringModelDetectionLevel(
				RulesetWeightingModel.DETECTION_LEVEL_CRITICAL, true);
		
		for (int i=0 ; i<this.rulesetFilters.length ; i++) {
			this.rulesetFilters[i] = 
					new FilteringModelRuleset(rulesetNames[i], true);
		}
		
	} // end of constructor
	
	public RulesetResultFilterModel(RulesetResultFilterModel filterModel) {
		
		this.patternFilters = new FilteringModelPattern[filterModel.patternFilters.length];
		this.levelFilters = new FilteringModelDetectionLevel[5];
		this.rulesetFilters = new FilteringModelRuleset[filterModel.rulesetFilters.length];
		
		FilteringModelPattern[] oriPatternFilters = filterModel.patternFilters;
		for (int i=0 ; i<this.patternFilters.length ; i++) {
			this.patternFilters[i] = new FilteringModelPattern(
					oriPatternFilters[i].patternDataset, 
					oriPatternFilters[i].isShowEnable);
		}

		FilteringModelDetectionLevel[] oriLevelFilters = filterModel.levelFilters;
		for (int i=0 ; i<this.levelFilters.length ; i++) {
			this.levelFilters[i] = new FilteringModelDetectionLevel(
					oriLevelFilters[i].level, 
					oriLevelFilters[i].isShowEnable);
		}
		
		FilteringModelRuleset[] oriRulesetFilters = filterModel.rulesetFilters;
		for (int i=0 ; i<this.rulesetFilters.length ; i++) {
			this.rulesetFilters[i] = new FilteringModelRuleset(
					oriRulesetFilters[i].rulesetName, 
					oriRulesetFilters[i].isShowEnable);
		}

	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * toString
	 */
	public String toString() {
		
		StringBuffer strBuffer = new StringBuffer();
		
		for (int i=0 ; i<this.patternFilters.length ; i++) {
			strBuffer.append(String.format("Pattern[%5d] : %b\n", 
					this.patternFilters[i].patternDataset.getPatternId(),
					this.patternFilters[i].isShowEnable));
		}

		for (int i=0 ; i<this.levelFilters.length ; i++) {
			strBuffer.append(String.format("Level[%5d] : %b\n", 
					this.levelFilters[i].level,
					this.levelFilters[i].isShowEnable));
		}
		
		for (int i=0 ; i<this.rulesetFilters.length ; i++) {
			strBuffer.append(String.format("Ruleset[%15s] : %b\n", 
					this.rulesetFilters[i].rulesetName,
					this.rulesetFilters[i].isShowEnable));
		}
		
		return strBuffer.toString();
		
	} // end of method
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Set Filter Model
	 */
	public void setFilterModel(RulesetResultFilterModel filterModel) {
		
		for (int i=0 ; i<filterModel.patternFilters.length ; i++) {
			setPatternFilter(
					filterModel.patternFilters[i].patternDataset.getPatternId(),
					filterModel.patternFilters[i].isShowEnable);
		}

		for (int i=0 ; i<filterModel.levelFilters.length ; i++) {
			setLevelFilter(
					filterModel.levelFilters[i].level,
					filterModel.levelFilters[i].isShowEnable);
		}
		
		for (int i=0 ; i<filterModel.rulesetFilters.length ; i++) {
			setRulesetFilter(
					filterModel.rulesetFilters[i].rulesetName,
					filterModel.rulesetFilters[i].isShowEnable);
		}
		
	} // end of method
	
	/**
	 * set patternFilter
	 */
	public void setPatternFilter(int patternId, boolean isShowEnable) {
		
		for (int i=0 ; i<this.patternFilters.length ; i++) {
			if (this.patternFilters[i].patternDataset.getPatternId() == patternId) {
				this.patternFilters[i].isShowEnable = isShowEnable;
				return;
			}
		}
		
	} // end of method

	/**
	 * set level filter
	 */
	public void setLevelFilter(int level, boolean isShowEnable) {
		
		for (int i=0 ; i<this.levelFilters.length ; i++) {
			if (this.levelFilters[i].level == level) {
				this.levelFilters[i].isShowEnable = isShowEnable;
				return;
			}
		}
		
	} // end of method
	
	/**
	 * set ruleset Filter
	 */
	public void setRulesetFilter(String rulesetKey, boolean isShowEnable) {
		
		for (int i=0 ; i<this.rulesetFilters.length ; i++) {
			if (this.rulesetFilters[i].rulesetName.equals(rulesetKey)) {
				this.rulesetFilters[i].isShowEnable = isShowEnable;
				return;
			}
		}
		
	} // end of method

	/**
	 * get pattern Filter
	 */
	public boolean getPatternFilter(int patternId) {

		for (int i=0 ; i<this.patternFilters.length ; i++) {
			if (this.patternFilters[i].patternDataset.getPatternId() == patternId) {
				return this.patternFilters[i].isShowEnable;
			}
		}
		return false;

	} // end of method
	
	/**
	 * get pattern Filter
	 */
	public boolean getLevelFilter(int level) {

		for (int i=0 ; i<this.levelFilters.length ; i++) {
			if (this.levelFilters[i].level == level) {
				return this.levelFilters[i].isShowEnable;
			}
		}
		return false;

	} // end of method
	
	/**
	 * get ruleset Filter
	 */
	public boolean getRulesetFilter(String rulesetKey) {

		for (int i=0 ; i<this.rulesetFilters.length ; i++) {
			if (this.rulesetFilters[i].rulesetName.equals(rulesetKey)) {
				return this.rulesetFilters[i].isShowEnable;
			}
		}
		return false;

	} // end of method
	
	/**
	 * @return the patternFilters
	 */
	public FilteringModelPattern[] getPatternFilters() {
		return patternFilters;
	}

	/**
	 * @return the levelFilters
	 */
	public FilteringModelDetectionLevel[] getLevelFilters() {
		return levelFilters;
	}

	/**
	 * @return the rulesetFilters
	 */
	public FilteringModelRuleset[] getRulesetFilters() {
		return rulesetFilters;
	}

	//==========================================================================
	// PatternDataset filtering model
	//==========================================================================
	public class FilteringModelPattern {
		
		public DetectionPatternDataset patternDataset = null;
		public boolean isShowEnable = true;
		
		public FilteringModelPattern(
				DetectionPatternDataset patternDataset, 
				boolean isShowEnable) {
			this.patternDataset = patternDataset;
			this.isShowEnable = isShowEnable;
		}
		
	} // end of inner class

	//==========================================================================
	// DetectionLevel filtering model
	//==========================================================================
	public class FilteringModelDetectionLevel {
		
		public int level = RulesetWeightingModel.DETECTION_LEVEL_NON;
		public boolean isShowEnable = true;
		
		public FilteringModelDetectionLevel(
				int level, 
				boolean isShowEnable) {
			this.level = level;
			this.isShowEnable = isShowEnable;
		}
		
		public String getLevelName() {
			return RulesetWeightingModel.DETECTION_LEVEL_NAMES[level];
		}
		
	} // end of inner class
	
	//==========================================================================
	// Ruleset filtering model
	//==========================================================================
	public class FilteringModelRuleset {
		
		public String rulesetName = null;
		public boolean isShowEnable = true;
		
		public FilteringModelRuleset(
				String rulesetName, 
				boolean isShowEnable) {
			this.rulesetName = rulesetName;
			this.isShowEnable = isShowEnable;
		}
		
	} // end of inner class
	
	
} // end of class
