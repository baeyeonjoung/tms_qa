/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

import com.snu.tms.qa.util.DateUtil;

/**
 * Data Element for TMS Data
 * 
 */
public class TmsDataRecord implements Comparable<TmsDataRecord> {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String          timestr = "";
	private  long            time = 0L;
	
	private  TmsDataHeader   tmsDataHeader = null;
	
	private  TmsDataCell     tsp     = new TmsDataCell(ItemCodeDefine.TSP);
	private  TmsDataCell     sox     = new TmsDataCell(ItemCodeDefine.SOX);
	private  TmsDataCell     nox     = new TmsDataCell(ItemCodeDefine.NOX);
	private  TmsDataCell     hcl     = new TmsDataCell(ItemCodeDefine.HCL);
	private  TmsDataCell     hf      = new TmsDataCell(ItemCodeDefine.HF);
	private  TmsDataCell     nh3     = new TmsDataCell(ItemCodeDefine.NH3);
	private  TmsDataCell     co      = new TmsDataCell(ItemCodeDefine.CO);
	private  TmsDataCell     o2      = new TmsDataCell(ItemCodeDefine.O2);
	private  TmsDataCell     fl1     = new TmsDataCell(ItemCodeDefine.FL1);
	private  TmsDataCell     fl2     = new TmsDataCell(ItemCodeDefine.FL2);
	private  TmsDataCell     tmp     = new TmsDataCell(ItemCodeDefine.TMP);
	private  TmsDataCell     tm1     = new TmsDataCell(ItemCodeDefine.TM1);
	private  TmsDataCell     tm2     = new TmsDataCell(ItemCodeDefine.TM2);
	private  TmsDataCell     tm3     = new TmsDataCell(ItemCodeDefine.TM3);
	
		
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public TmsDataRecord() {}
	
	public TmsDataRecord(long time, TmsDataHeader tmsDataHeader) {
		this.time = time;
		this.timestr = DateUtil.getDateStr(time);
		this.tmsDataHeader = tmsDataHeader;
	}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// Append information
		infoStrBuffer.append(this.timestr + "\n");
		if (this.tsp != null) {
			infoStrBuffer.append(this.tsp.toString() + "\n");
		}
		if (this.sox != null) {
			infoStrBuffer.append(this.sox.toString() + "\n");
		}
		if (this.nox != null) {
			infoStrBuffer.append(this.nox.toString() + "\n");
		}
		if (this.hcl != null) {
			infoStrBuffer.append(this.hcl.toString() + "\n");
		}
		if (this.hf != null) {
			infoStrBuffer.append(this.hf.toString() + "\n");
		}
		if (this.nh3 != null) {
			infoStrBuffer.append(this.nh3.toString() + "\n");
		}
		if (this.co != null) {
			infoStrBuffer.append(this.co.toString() + "\n");
		}
		if (this.o2 != null) {
			infoStrBuffer.append(this.o2.toString() + "\n");
		}
		if (this.fl1 != null) {
			infoStrBuffer.append(this.fl1.toString() + "\n");
		}
		if (this.fl2 != null) {
			infoStrBuffer.append(this.fl2.toString() + "\n");
		}
		if (this.tmp != null) {
			infoStrBuffer.append(this.tmp.toString() + "\n");
		}
		if (this.tm1 != null) {
			infoStrBuffer.append(this.tm1.toString() + "\n");
		}
		if (this.tm2 != null) {
			infoStrBuffer.append(this.tm2.toString() + "\n");
		}
		if (this.tm3 != null) {
			infoStrBuffer.append(this.tm3.toString() + "\n");
		}
		// return
		return infoStrBuffer.toString();

	} // end of method
	
	/**
	 * compare
	 */
	public int compareTo(TmsDataRecord compare) {
		
		if (this.time > compare.time) {
			return 1;
		} else if (this.time < compare.time) {
			return -1;
		} else {
			return 0;
		}
		
	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * get values
	 */
	public double[] getValues(int[] items) {
		
		double[] values = new double[items.length];
		for (int i=0 ; i<items.length ; i++) {
			values[i] = getDataCell(items[i]).getData_value();
		}
		return values;
		
	} // end of method
	
	/**
	 * set value
	 */
	public  void  setValue(int item_code, double value, double value_bf) {
		
		switch(item_code) {
		case ItemCodeDefine.TSP:
			this.tsp.setData_value(value);
			this.tsp.setOrigin_value(value_bf);
			this.tsp.setBefore_value(value_bf);
			break;
		case ItemCodeDefine.SOX:
			this.sox.setData_value(value);
			this.sox.setOrigin_value(value_bf);
			this.sox.setBefore_value(value_bf);
			break;
		case ItemCodeDefine.NOX:
			this.nox.setData_value(value);
			this.nox.setOrigin_value(value_bf);
			this.nox.setBefore_value(value_bf);
			break;
		case ItemCodeDefine.HCL:
			this.hcl.setData_value(value);
			this.hcl.setOrigin_value(value_bf);
			this.hcl.setBefore_value(value_bf);
			break;
		case ItemCodeDefine.HF:
			this.hf.setData_value(value);
			this.hf.setOrigin_value(value_bf);
			this.hf.setBefore_value(value_bf);
			break;
		case ItemCodeDefine.NH3:
			this.nh3.setData_value(value);
			this.nh3.setOrigin_value(value_bf);
			this.nh3.setBefore_value(value_bf);
			break;
		case ItemCodeDefine.CO:
			this.co.setData_value(value);
			this.co.setOrigin_value(value_bf);
			this.co.setBefore_value(value_bf);
			break;
		case ItemCodeDefine.O2:
			this.o2.setData_value(value);
			this.o2.setOrigin_value(value_bf);
			this.o2.setBefore_value(value_bf);
			break;
		case ItemCodeDefine.FL1:
			this.fl1.setData_value(value);
			this.fl1.setOrigin_value(value_bf);
			this.fl1.setBefore_value(value_bf);
			break;
		case ItemCodeDefine.TMP:
			this.tmp.setData_value(value);
			this.tmp.setOrigin_value(value_bf);
			this.tmp.setBefore_value(value_bf);
			break;
		case ItemCodeDefine.TM1:
			this.tm1.setData_value(value);
			this.tm1.setOrigin_value(value_bf);
			this.tm1.setBefore_value(value_bf);
			break;
		case ItemCodeDefine.TM2:
			this.tm2.setData_value(value);
			this.tm2.setOrigin_value(value_bf);
			this.tm2.setBefore_value(value_bf);
			break;
		case ItemCodeDefine.TM3:
			this.tm3.setData_value(value);
			this.tm3.setOrigin_value(value_bf);
			this.tm3.setBefore_value(value_bf);
			break;
		case ItemCodeDefine.FL2:
			this.fl2.setData_value(value);
			this.fl2.setOrigin_value(value_bf);
			this.fl2.setBefore_value(value_bf);
			break;
		}
		
	}
	
	
	/**
	 * @return the timestr
	 */
	public String getTimestr() {
		return timestr;
	}

	/**
	 * @param timestr the timestr to set
	 */
	public void setTimestr(String timestr) {
		this.timestr = timestr;
	}

	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}

	/**
	 * @return the tmsDataHeader
	 */
	public TmsDataHeader getTmsDataHeader() {
		return tmsDataHeader;
	}

	/**
	 * @param tmsDataHeader the tmsDataHeader to set
	 */
	public void setTmsDataHeader(TmsDataHeader tmsDataHeader) {
		this.tmsDataHeader = tmsDataHeader;
	}

	/**
	 * 
	 */
	public TmsDataCell getDataCell(int item) {
		
		switch (item) {
		case ItemCodeDefine.TSP:
			return this.tsp;
		case ItemCodeDefine.SOX:
			return this.sox;
		case ItemCodeDefine.NOX:
			return this.nox;
		case ItemCodeDefine.HCL:
			return this.hcl;
		case ItemCodeDefine.HF:
			return this.hf;
		case ItemCodeDefine.NH3:
			return this.nh3;
		case ItemCodeDefine.CO:
			return this.co;
		case ItemCodeDefine.O2:
			return this.o2;
		case ItemCodeDefine.FL1:
			return this.fl1;
		case ItemCodeDefine.TMP:
			return this.tmp;
		case ItemCodeDefine.TM1:
			return this.tm1;
		case ItemCodeDefine.TM2:
			return this.tm2;
		case ItemCodeDefine.TM3:
			return this.tm3;
		case ItemCodeDefine.FL2:
			return this.fl2;
		default:
			return new TmsDataCell();
		}
	}
	
	/**
	 * @return the tsp
	 */
	public TmsDataCell getTsp() {
		return tsp;
	}

	/**
	 * @param tsp the tsp to set
	 */
	public void setTsp(TmsDataCell tsp) {
		this.tsp = tsp;
	}

	/**
	 * @return the so2
	 */
	public TmsDataCell getSox() {
		return sox;
	}

	/**
	 * @param so2 the so2 to set
	 */
	public void setSox(TmsDataCell sox) {
		this.sox = sox;
	}

	/**
	 * @return the nox
	 */
	public TmsDataCell getNox() {
		return nox;
	}

	/**
	 * @param nox the nox to set
	 */
	public void setNox(TmsDataCell nox) {
		this.nox = nox;
	}

	/**
	 * @return the hcl
	 */
	public TmsDataCell getHcl() {
		return hcl;
	}

	/**
	 * @param hcl the hcl to set
	 */
	public void setHcl(TmsDataCell hcl) {
		this.hcl = hcl;
	}

	/**
	 * @return the hf
	 */
	public TmsDataCell getHf() {
		return hf;
	}

	/**
	 * @param hf the hf to set
	 */
	public void setHf(TmsDataCell hf) {
		this.hf = hf;
	}

	/**
	 * @return the nh3
	 */
	public TmsDataCell getNh3() {
		return nh3;
	}

	/**
	 * @param nh3 the nh3 to set
	 */
	public void setNh3(TmsDataCell nh3) {
		this.nh3 = nh3;
	}

	/**
	 * @return the co
	 */
	public TmsDataCell getCo() {
		return co;
	}

	/**
	 * @param co the co to set
	 */
	public void setCo(TmsDataCell co) {
		this.co = co;
	}

	/**
	 * @return the o2
	 */
	public TmsDataCell getO2() {
		return o2;
	}

	/**
	 * @param o2 the o2 to set
	 */
	public void setO2(TmsDataCell o2) {
		this.o2 = o2;
	}

	/**
	 * @return the flw
	 */
	public TmsDataCell getFl1() {
		return fl1;
	}

	/**
	 * @param flw the flw to set
	 */
	public void setFl1(TmsDataCell fl1) {
		this.fl1 = fl1;
	}

	/**
	 * @return the tmp
	 */
	public TmsDataCell getTmp() {
		return tmp;
	}

	/**
	 * @param tmp the tmp to set
	 */
	public void setTmp(TmsDataCell tmp) {
		this.tmp = tmp;
	}

	/**
	 * @return the tm1
	 */
	public TmsDataCell getTm1() {
		return tm1;
	}

	/**
	 * @param tm1 the tm1 to set
	 */
	public void setTm1(TmsDataCell tm1) {
		this.tm1 = tm1;
	}

	/**
	 * @return the tm2
	 */
	public TmsDataCell getTm2() {
		return tm2;
	}

	/**
	 * @param tm2 the tm2 to set
	 */
	public void setTm2(TmsDataCell tm2) {
		this.tm2 = tm2;
	}

	/**
	 * @return the tm3
	 */
	public TmsDataCell getTm3() {
		return tm3;
	}

	/**
	 * @param tm3 the tm3 to set
	 */
	public void setTm3(TmsDataCell tm3) {
		this.tm3 = tm3;
	}

	/**
	 * @return the fl2
	 */
	public TmsDataCell getFl2() {
		return fl2;
	}

	/**
	 * @param fl2 the fl2 to set
	 */
	public void setFl2(TmsDataCell fl2) {
		this.fl2 = fl2;
	}

} // end of class
