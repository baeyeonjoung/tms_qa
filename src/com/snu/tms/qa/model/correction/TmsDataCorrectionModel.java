/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.correction;

import java.util.Vector;

/**
 * Data Element for TMS Data
 * 
 */
public class TmsDataCorrectionModel {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int  itemCode = 0;
	private  double  percentOfDiffUpperLimit = 0.0D;
	private  double  percentOfDiffLowerLimit = 0.0D;
	
	private  Vector<ReverseCorrectionRecord> records = new Vector<ReverseCorrectionRecord>();
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public TmsDataCorrectionModel() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * set data
	 */
	public void setData(
			String timeStr, 
			long time, 
			double tmsValue, 
			double reverseCorrValue, 
			double percentOfDiff) {
		
		this.records.add(new ReverseCorrectionRecord(
				timeStr, time, tmsValue, reverseCorrValue, percentOfDiff));
		
	} // end of method
	
	/**
	 * Get data count
	 */
	public int getDataCount() {
		return this.records.size();
	} // end of method
	
	/**
	 * Get data (TimeStr)
	 */
	public String getDataTimeStr(int index) {
		return this.records.get(index).getTimeStr();
	} // end of method
	

	/**
	 * Get data (Time)
	 */
	public long getDataTime(int index) {
		return this.records.get(index).getTime();
	} // end of method
	
	/**
	 * Get data (tmsValue)
	 */
	public double getDataTmsValue(int index) {
		return this.records.get(index).getTmsValue();
	} // end of method
	
	/**
	 * Get data (reverseCorrValue)
	 */
	public double getDataReverseCorrValue(int index) {
		return this.records.get(index).getReverseCorrValue();
	} // end of method
	
	/**
	 * Get data (percentOfDiff)
	 */
	public double getDataPercentOfDiff(int index) {
		return this.records.get(index).getPercentOfDiff();
	} // end of method
	
	/**
	 * @return the itemCode
	 */
	public int getItemCode() {
		return itemCode;
	}

	/**
	 * @param itemCode the itemCode to set
	 */
	public void setItemCode(int itemCode) {
		this.itemCode = itemCode;
	}

	/**
	 * @return the percentOfDiffUpperLimit
	 */
	public double getPercentOfDiffUpperLimit() {
		return percentOfDiffUpperLimit;
	}

	/**
	 * @param percentOfDiffUpperLimit the percentOfDiffUpperLimit to set
	 */
	public void setPercentOfDiffUpperLimit(double percentOfDiffUpperLimit) {
		this.percentOfDiffUpperLimit = percentOfDiffUpperLimit;
	}

	/**
	 * @return the percentOfDiffLowerLimit
	 */
	public double getPercentOfDiffLowerLimit() {
		return percentOfDiffLowerLimit;
	}

	/**
	 * @param percentOfDiffLowerLimit the percentOfDiffLowerLimit to set
	 */
	public void setPercentOfDiffLowerLimit(double percentOfDiffLowerLimit) {
		this.percentOfDiffLowerLimit = percentOfDiffLowerLimit;
	}



	//==========================================================================
	// Inner Class
	//==========================================================================
	/**
	 * for save reverse correction data
	 */
	class ReverseCorrectionRecord {
		
		// Fields
		public  String  timeStr = "";
		public  long    time = 0L;
		public  double  tmsValue = 0.0D;
		public  double  reverseCorrValue = 0.0D;
		public  double  percentOfDiff = 0.0D;
		
		public ReverseCorrectionRecord(
				String timeStr, 
				long time, 
				double tmsValue, 
				double reverseCorrValue, 
				double percentOfDiff) {
			
			this.timeStr = timeStr;
			this.time = time;
			this.tmsValue = tmsValue;
			this.reverseCorrValue = reverseCorrValue;
			this.percentOfDiff = percentOfDiff;
			
		} // end of constructor

		/**
		 * @return the timeStr
		 */
		public String getTimeStr() {
			return timeStr;
		}

		/**
		 * @param timeStr the timeStr to set
		 */
		public void setTimeStr(String timeStr) {
			this.timeStr = timeStr;
		}

		/**
		 * @return the time
		 */
		public long getTime() {
			return time;
		}

		/**
		 * @param time the time to set
		 */
		public void setTime(long time) {
			this.time = time;
		}

		/**
		 * @return the tmsValue
		 */
		public double getTmsValue() {
			return tmsValue;
		}

		/**
		 * @param tmsValue the tmsValue to set
		 */
		public void setTmsValue(double tmsValue) {
			this.tmsValue = tmsValue;
		}

		/**
		 * @return the reverseCorrValue
		 */
		public double getReverseCorrValue() {
			return reverseCorrValue;
		}

		/**
		 * @param reverseCorrValue the reverseCorrValue to set
		 */
		public void setReverseCorrValue(double reverseCorrValue) {
			this.reverseCorrValue = reverseCorrValue;
		}

		/**
		 * @return the percentOfDiff
		 */
		public double getPercentOfDiff() {
			return percentOfDiff;
		}

		/**
		 * @param percentOfDiff the percentOfDiff to set
		 */
		public void setPercentOfDiff(double percentOfDiff) {
			this.percentOfDiff = percentOfDiff;
		}
		
		
		
		
	} // end of inner class
	
} // end of class
