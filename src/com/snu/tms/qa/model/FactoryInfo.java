/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

import java.util.Vector;

/**
 * Factory information
 * 
 */
public class FactoryInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String  code         = "";
	private  String  simpleName   = "";
	private  String  fullName     = "";
	private  String  typicalName  = "";
	private  String  addr         = "";
	private  String  products     = "";
	private  String  registNumber = "";
	private  String  areaCode     = "";
	
	private  String  dbName       = "";
	
	private  Vector<StackInfo>  stacks = new Vector<StackInfo>();
		
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for FactoryInfo
	 */
	public FactoryInfo() {}
	
	/**
	 * MySQL 버전에서는 테이블스페이스 용량 문제로 각 사업소 별로 데이터를 관리하기 때문에
	 * DbName을 통해 구분하였지만 Tibero 데이터베이스에서는 이러한 기능이 요구되지 않기 때문에
	 * 이를 사용하지 않는다.
	 */
	public FactoryInfo(
			String  code,
			String  simpleName,
			String  fullName,
			String  typicalName,
			String  addr,
			String  products,
			String  registNumber,
			String  areaCode,
			String  dbName) {
		
		this.code = code;
		this.simpleName = simpleName;
		this.fullName = fullName;
		this.typicalName = typicalName;
		this.addr = addr;
		this.products = products;
		this.registNumber = registNumber;
		this.areaCode = areaCode;
		this.dbName = dbName;
		
	} // end of constructor
	
	public FactoryInfo(
			String  code,
			String  simpleName,
			String  fullName,
			String  typicalName,
			String  addr,
			String  products,
			String  registNumber,
			String  areaCode) {
		
		this(code, simpleName, fullName, typicalName, addr, products, registNumber, areaCode, "");
		
	} // end of constructor

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {
		
		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// Append information
		infoStrBuffer.append(String.format("    code        : %s\n", this.code));
		infoStrBuffer.append(String.format("    simpleName  : %s\n", this.simpleName));
		infoStrBuffer.append(String.format("    fullName    : %s\n", this.fullName));
		infoStrBuffer.append(String.format("    typicalName : %s\n", this.typicalName));
		infoStrBuffer.append(String.format("    addr        : %s\n", this.addr));
		infoStrBuffer.append(String.format("    products    : %s\n", this.products));
		infoStrBuffer.append(String.format("    registNumber: %s\n", this.registNumber));
		infoStrBuffer.append(String.format("    areaCode    : %s\n", this.areaCode));
		infoStrBuffer.append(String.format("    dbName      : %s",   this.dbName));
		
		/**
		if (this.stacks.size() > 0) {
			infoStrBuffer.append("\n---------------------------------------------\n");
			for (StackInfo stackInfo : this.stacks) {
				infoStrBuffer.append(stackInfo.toString() + "\n");
			}
		}
		*/
		
		// return
		return infoStrBuffer.toString();

	} // end of method


	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Add StackInfo
	 */
	public StackInfo addStackInfo(
			int     stackNumber,
			String  stackName,
			double  standardOxygen,
			double  standardMoisture) {
		
		StackInfo stackInfo = new StackInfo(this, stackNumber, stackName, standardMoisture);
		this.stacks.add(stackInfo);
		return stackInfo;
	}
	
	/**
	 * Get all stackInfo
	 */
	public StackInfo[] getAllStackInfo() {
		return this.stacks.toArray(new StackInfo[this.stacks.size()]);
	}
	
	/**
	 * Get StackInfo
	 */
	public StackInfo getStackInfo(int stackCode) {
		
		StackInfo stack = null;
		for (int i=0 ; i<this.stacks.size() ; i++) {
			stack = this.stacks.get(i);
			if (stack.getStackNumber() == stackCode) {
				return stack;
			}
		}
		return null;

	} 
	
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the simpleName
	 */
	public String getSimpleName() {
		return simpleName;
	}

	/**
	 * @param simpleName the simpleName to set
	 */
	public void setSimpleName(String simpleName) {
		this.simpleName = simpleName;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the typicalName
	 */
	public String getTypicalName() {
		return typicalName;
	}

	/**
	 * @param typicalName the typicalName to set
	 */
	public void setTypicalName(String typicalName) {
		this.typicalName = typicalName;
	}

	/**
	 * @return the addr
	 */
	public String getAddr() {
		return addr;
	}

	/**
	 * @param addr the addr to set
	 */
	public void setAddr(String addr) {
		this.addr = addr;
	}

	/**
	 * @return the products
	 */
	public String getProducts() {
		return products;
	}

	/**
	 * @param products the products to set
	 */
	public void setProducts(String products) {
		this.products = products;
	}

	/**
	 * @return the registNumber
	 */
	public String getRegistNumber() {
		return registNumber;
	}

	/**
	 * @param registNumber the registNumber to set
	 */
	public void setRegistNumber(String registNumber) {
		this.registNumber = registNumber;
	}

	/**
	 * @return the areaCode
	 */
	public String getAreaCode() {
		return areaCode;
	}

	/**
	 * @param areaCode the areaCode to set
	 */
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	/**
	 * @return the dbName
	 */
	public String getDbName() {
		return dbName;
	}

	/**
	 * @param dbName the dbName to set
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}



} // end of class
