/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

import java.util.Enumeration;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


//import com.snu.tms.qa.analysis.outlier.DoAnalysisReverseCorrection;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.DetectionRuleset;
import com.snu.tms.qa.model.ruleset.PatternDataRecord;
import com.snu.tms.qa.model.ruleset.PcaResultModel;
import com.snu.tms.qa.model.ruleset.RawDataCell;
import com.snu.tms.qa.model.ruleset.RulesetCorrectionModel;
import com.snu.tms.qa.model.ruleset.RulesetModel;
import com.snu.tms.qa.model.ruleset.RulesetPatternModel;
import com.snu.tms.qa.model.ruleset.RulesetWeightingModel;

/**
 * Data Element for TMS Data
 * 
 */
public class DatasetManager {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(DatasetManager.class);

	private static DatasetManager instance = new DatasetManager();
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	// TMS raw data
	private  TmsDataset  tmsDataset = null;
	
	// TMS 30min Data
	private  Tms30minDataset  tms30minDataset = null;
	
	private  PatternDataSet   patternDataSet = null;
	private Vector<DetectionPatternDataset> detectionPatternDataset = 
			new Vector<DetectionPatternDataset>();
	
	private RulesetModel  rulesetModel = null;
	
	// ruleset base date 
	private  long      rulsetBaseStartTime = 0L;
	private  long      rulsetBaseEndTime   = 0L;

	// items
	private  int[]       items = null;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	private DatasetManager() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================


	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * getInstance()
	 */
	public static DatasetManager getInstance() {
		return instance;
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @param tmsDataSet the tmsDataSet to set
	 */
	public void setTmsDataset(TmsDataset tmsDataset) {

		// set TmsDataset
		this.tmsDataset = tmsDataset;
		logger.debug(this.tmsDataset.toString());
		
		// set time
		this.rulsetBaseStartTime = tmsDataset.getTmsDataHeader().getStart_time();
		this.rulsetBaseEndTime   = tmsDataset.getTmsDataHeader().getEnd_time();
		
		// 측정기기 상태정보가 교정중(1), 동잗불량(2), 전원단절(4), 보수중(8) 등에 대해서
		// 일부 항목이 다른 항목에 비해 유효한 데이터가 50% 미만일 경우에는
		// 해당 Item을 자료에서 삭제한다.
		this.tmsDataset.doDataValidationCheck();
		
		// compute before correction value
		// 사용자가 Manualy 설정하도록 변경 (2016-10-22)
		//DoAnalysisReverseCorrection.doReverseCorrection(this.tmsDataset);
		
	} // end of method

	/**
	 * @param tmsDataSet the tmsDataSet to set
	 */
	public void setTms30minDataset(Tms30minDataset tms30minDataset) {
		
		// set TmsDataset
		this.tms30minDataset = tms30minDataset;
		logger.debug(this.tms30minDataset.toString());
		
	} // end of method

	/**
	 * Create DetectionPatternDataset with PCA-Analysis
	 */
	public void initDetectionPatternDataset(PatternDataSet patternDataSet) {
		
		// clear all previous patternData
		clearPatternData();
		this.patternDataSet = patternDataSet;

		// set ruleset model
		TmsDataHeader tmsDataHeader = this.tmsDataset.getTmsDataHeader();
		this.rulesetModel = new RulesetModel();
		this.rulesetModel.setFactCode(tmsDataHeader.getFactoryInfo().getCode());
		this.rulesetModel.setStckCode(tmsDataHeader.getStackInfo().getStackNumber());
		this.rulesetModel.setRulesetStartTime(tmsDataHeader.getStart_time());
		this.rulesetModel.setRulesetEndTime(tmsDataHeader.getEnd_time());

		this.items = patternDataSet.getItems();

		// Set PCA modelt
		int[]       items = patternDataSet.getItems();
		int         pcCount = patternDataSet.getPcCount();
		double[][]  eigenVector = patternDataSet.getEigenVector();
		double[]    eigenValues = patternDataSet.getEigenValue();
		double[]    neigenValues = patternDataSet.getNeigenValue();		
		PcaResultModel pcaResultModel = new PcaResultModel(items, pcCount, eigenVector, eigenValues, neigenValues);
		
		// create DetectionPatternDataset
		int[] patternIndices = patternDataSet.getPatternIndexArray();
		for (int i=0 ; i<patternIndices.length ; i++) {
			
			// create DetectionPatternDataset
			int patternId = patternIndices[i];
			String patternName = patternDataSet.getPatternName(patternId);
			PatternDataHistogram[] patternDataHistograms = patternDataSet.getPatternHistogram(patternId);
			double[][] itemDataRange = patternDataSet.getConfidenceInterval();
			
			DetectionPatternDataset dataset = new DetectionPatternDataset(
					this.tmsDataset.getTmsDataHeader().getFactoryInfo(),
					this.tmsDataset.getTmsDataHeader().getStackInfo(),
					patternId, 
					patternName,
					items,
					rulsetBaseStartTime,
					rulsetBaseEndTime,
					itemDataRange,
					patternDataHistograms);
			
			// add PCA result
			dataset.setPcaResultModel(pcaResultModel);
			
			// Add dataRecords
			PatternDataRecord[] patternDataRecords = patternDataSet.getSortedPatternDataRecord(patternId);
			long time;
			TmsDataRecord tmsDataRecord;
			for (int j=0 ; j<patternDataRecords.length ; j++) {
				
				// get time key
				time = patternDataRecords[j].getTime();
				tmsDataRecord = this.tmsDataset.getTmsDataRecord(time);
				TmsDataCell tmsDataCell;
				RawDataCell[] rawDataCells = new RawDataCell[items.length];
				
				for (int k=0 ; k<items.length ; k++) {
					
					tmsDataCell = tmsDataRecord.getDataCell(items[k]);
					rawDataCells[k] = new RawDataCell(
							tmsDataCell.getData_type(), 
							tmsDataCell.getData_value(), 
							tmsDataCell.getOrigin_value());
				}
				
				// add dataRecord
				dataset.addDetectionRecord(new DetectionRecord(dataset, time, patternId, patternName, rawDataCells));
			}
			
			// add pattern dataset
			this.detectionPatternDataset.add(dataset);
		}
		
	} // end of method
	
	/**
	 * Initialize data
	 */
	public void clear() {
		
		this.tmsDataset = null;
		this.tms30minDataset = null;
		this.patternDataSet = null;
		this.detectionPatternDataset.clear();
		this.rulesetModel = null;
		
		System.gc();
		
	} // end of method
	
	/**
	 * clear pattern data
	 */
	public void clearPatternData() {
		this.detectionPatternDataset.clear();
	} // end of method
	
	/** 
	 * fill out sub-components
	 */
	public void filloutRulesetModel(RulesetModel rulesetModel) {
		
		// clear exist sub-components
		rulesetModel.clear();
		
		// fillout RulesetPatternModel
		for (DetectionPatternDataset detectionPatternDataset : this.detectionPatternDataset) {
			
			// set parameters
			int patternId             = detectionPatternDataset.getPatternId();
			String patternName        = detectionPatternDataset.getPatternName();
			boolean isEnabled         = detectionPatternDataset.isEnabled();
			boolean isRulesetCreated  = detectionPatternDataset.isRulesetCreated();
			boolean isOperateStop     = detectionPatternDataset.isOperateStop();
			int[] items               = detectionPatternDataset.getItems();
			long patternBaseStartTime = detectionPatternDataset.getPatternBaseStartTime();
			long patternBaseEndTime   = detectionPatternDataset.getPatternBaseEndTime();
			PatternDataHistogram[] patternDataHistograms = 
					detectionPatternDataset.getAllPatternDataHistogram();
			PcaResultModel pcaResultModel = 
					detectionPatternDataset.getPcaResultModel();
			DetectionRuleset[] detectionRulesets = detectionPatternDataset.getAllRuleset();
			
			// create RulesetPatternModel
			RulesetPatternModel rulesetPatternModel = new RulesetPatternModel(
					patternId, 
					patternName,
					isEnabled,
					isRulesetCreated,
					isOperateStop,
					items,
					patternBaseStartTime,
					patternBaseEndTime,
					patternDataHistograms,
					pcaResultModel,
					detectionRulesets);
			
			// add rulesetPatternModel
			rulesetModel.addRulesetPatternModel(rulesetPatternModel);
			
		} // end of for-loop
		
		// set RulesetCorrectionInfo
		InventoryDataManager inventoryDataManager = InventoryDataManager.getInstance();
		String factCode = rulesetModel.getFactCode();
		int    stckCode = rulesetModel.getStckCode();
		StackInfo stackInfo = inventoryDataManager.getStackInfo(factCode, stckCode);
		if (stackInfo == null) {
			return;
		}
		
		CorrectionItemInfo[] correctionInfoArray = stackInfo.getAllCorrectionInfo();
		RulesetCorrectionModel rulesetCorrectionModel;
		for (CorrectionItemInfo correctionInfo : correctionInfoArray) {
			
			rulesetCorrectionModel = new RulesetCorrectionModel(
					correctionInfo.getItemCode(),
					correctionInfo.getStdOxygen(),
					correctionInfo.getStdMoisture(),
					correctionInfo.isOxygenApplied(),
					correctionInfo.isTemperatureApplied(),
					correctionInfo.isMoistureApplied());
			rulesetModel.addRulesetCorrectionModel(rulesetCorrectionModel);
		}

	} // end of method

	/**
	 * @return the patternDataSet
	 */
	public PatternDataSet getPatternDataSet() {
		return patternDataSet;
	}

	/**
	 * @return the tmsDataSet
	 */
	public TmsDataset getTmsDataset() {
		return tmsDataset;
	} // end of method
	
	/**
	 * @return the tmsDataSet
	 */
	public Tms30minDataset get30minTmsDataset() {
		return tms30minDataset;
	} // end of method

	/**
	 * Add patternData
	 */
	public void addPatternData(DetectionPatternDataset patternData) {
		this.detectionPatternDataset.add(patternData);
	} // end of method
	
	/**
	 * Get Pattern Count
	 */
	public int getPatternCount() {
		return this.detectionPatternDataset.size();
	} // end of method
	
	/**
	 * Get patternData
	 */
	public DetectionPatternDataset[] getAllPatternData() {
		return this.detectionPatternDataset.toArray(
				new DetectionPatternDataset[this.detectionPatternDataset.size()]);
	} // end of method
	
	/**
	 * Get PatternData
	 */
	public DetectionPatternDataset getPatternData(int patternId) {
		
		// search patternData and return
		DetectionPatternDataset patternData;
		for (Enumeration<DetectionPatternDataset> e=this.detectionPatternDataset.elements(); e.hasMoreElements();) {
			patternData = e.nextElement();
			if (patternData.getPatternId() == patternId) {
				return patternData;
			}
		}
		
		// if matched patternData is not exist,
		return null;

	} // end of method

	/**
	 * @return the rulesetWeighingModel
	 */
	public RulesetWeightingModel getRulesetWeighingModel() {
		return this.rulesetModel.getRulesetWeightingModel();
	}
	
	/**
	 * get RulesetModel
	 */
	public RulesetModel getRulesetModel() {
		return this.rulesetModel;
	}

	/**
	 * @param rulesetWeighingModel the rulesetWeighingModel to set
	 */
	public void setRulesetWeighingModel(RulesetWeightingModel rulesetWeighingModel) {
		this.rulesetModel.setRulesetWeightingModel(rulesetWeighingModel);
	}

	/**
	 * @return the rulsetBaseStartTime
	 */
	public long getRulsetBaseStartTime() {
		return rulsetBaseStartTime;
	}

	/**
	 * @param rulsetBaseStartTime the rulsetBaseStartTime to set
	 */
	public void setRulsetBaseStartTime(long rulsetBaseStartTime) {
		this.rulsetBaseStartTime = rulsetBaseStartTime;
	}

	/**
	 * @return the rulsetBaseEndTime
	 */
	public long getRulsetBaseEndTime() {
		return rulsetBaseEndTime;
	}

	/**
	 * @param rulsetBaseEndTime the rulsetBaseEndTime to set
	 */
	public void setRulsetBaseEndTime(long rulsetBaseEndTime) {
		this.rulsetBaseEndTime = rulsetBaseEndTime;
	}

	/**
	 * @return the items
	 */
	public int[] getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(int[] items) {
		this.items = items;
	}

	
} // end of class
