/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Data Element for TMS Data
 * 
 */
public class Tms30minDataset {

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  Hashtable<Long, Tms30minDataRecord>  timeHash = new Hashtable<Long, Tms30minDataRecord>();
	
	private  Vector<Tms30minDataRecord> tmsDataRecords = new Vector<Tms30minDataRecord>();
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public Tms30minDataset() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// Append information
		infoStrBuffer.append("--------------------------------------------------\n");
		int[][] itemCount = getItemValueCount();
		for (int i=0 ; i<itemCount.length ; i++) {
			infoStrBuffer.append(String.format("%-10s : %6d\n", 
					ItemCodeDefine.getItemName(itemCount[i][0]), 
					itemCount[i][1]));
		}
		infoStrBuffer.append("--------------------------------------------------\n");
		
		// return
		return infoStrBuffer.toString();

	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Clear all data
	 */
	public void clear() {
		
		// Delete all data 
		this.timeHash.clear();
		this.tmsDataRecords.clear();;
		
		System.gc();
		
	} // end of method
	
	/**
	 * @param tmsDataHeader the tmsDataHeader to set
	 */
	public void init(long sTime, long eTime) {
		
		// Count time devision
		long endTime = eTime;
		long startTime = sTime;
		long timeInterval = 30L * 60L * 1000L;
		long timeRange = endTime - startTime;
		
		int dataCount = (int) (timeRange / timeInterval);
		if ((timeRange % timeInterval) != 0L) {
			dataCount++;
		}
		
		// Init tmsDataRecords
		long time = startTime;
		Tms30minDataRecord record;
		for (int i=0 ; i<dataCount ; i++) {
			record = new Tms30minDataRecord(time);
			this.tmsDataRecords.add(record);
			this.timeHash.put(time, record);
			time += timeInterval;
		}
		
	} // end of method
	
	/**
	 * Get item value count
	 */
	public int[][] getItemValueCount() {
		
		// prepare value count
		int[] items = ItemCodeDefine.getAllItems();
		int[] itemValueCount = new int[items.length];
		Arrays.fill(itemValueCount, 0);

		// count
		if (this.tmsDataRecords == null) {
			return new int[0][0];
		}
		for (Tms30minDataRecord record : this.tmsDataRecords) {
			for (int i=0 ; i<itemValueCount.length ; i++) {
				if (record.getDataCell(items[i]).getHalf_valu() > 0.0D) {
					itemValueCount[i]++;
				}
			}
		}

		// return
		int itemCount = 0;
		for (int i=0 ; i<itemValueCount.length ; i++) {
			if (itemValueCount[i] > 0) {
				itemCount++;
			}
		}
		int index = 0;
		int[][] returnValues = new int[itemCount][2];
		for (int i=0 ; i<itemValueCount.length ; i++) {
			if (itemValueCount[i] > 0) {
				returnValues[index][0] = i;
				returnValues[index][1] = itemValueCount[i];
				index++;
			}
		}
		return returnValues;
		
	} // end of method

	/**
	 * @return the tmsDataRecords
	 */
	public Tms30minDataRecord[] getTmsDataRecords() {
		return this.tmsDataRecords.toArray(new Tms30minDataRecord[this.tmsDataRecords.size()]);
	}
	
	
	/**
	 * Get TmsDataRecord count
	 */
	public int getTmsDataRecordCount() {
		return tmsDataRecords.size();
	}// end of method
	
	/**
	 * 
	 */
	public Tms30minDataRecord getTmsDataRecord(long time) {
		return this.timeHash.get(time);
	}
	
	/**
	 * Get data values
	 */
	public double[][] getTmsDataRecordValues() {
		
		int[][] itemValueCount = getItemValueCount();
		int N = getTmsDataRecordCount();
		double[][] values = new double[N][itemValueCount.length];
		Tms30minDataRecord record;
		for (int i=0 ; i<values.length ; i++) {
			record = this.tmsDataRecords.get(i);
			for (int j=0 ; j<itemValueCount.length ; j++) {
				values[i][j] = record.getDataCell(itemValueCount[j][0]).getHalf_valu();
			}
		}
		return values;
		
	} // end of method
	
	/**
	 * Get data values
	 */
	public long[] getTmsDataRecordTimes() {
		
		int N = getTmsDataRecordCount();
		long[] times = new long[N];
		Tms30minDataRecord record;
		for (int i=0 ; i<times.length ; i++) {
			record = this.tmsDataRecords.get(i);
			times[i] = record.getTime();
		}
		return times;
		
	} // end of method

	
} // end of class
