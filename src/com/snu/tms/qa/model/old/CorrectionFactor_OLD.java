/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.old;

/**
 * Data for standard oxygen and stack moisture
 * 
 */
public class CorrectionFactor_OLD {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  double             stck_oxyg = 0D;
	private  double             stck_mois = 0D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for CorrectionFactor
	 */
	public CorrectionFactor_OLD() {}
	
	public CorrectionFactor_OLD(double stck_oxyg, double stck_mois) {
		this.stck_mois = stck_oxyg;
		this.stck_mois = stck_mois;
	}


	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// Append information
		infoStrBuffer.append(String.format("stck_oxyg      : %5.3f\n", this.stck_oxyg));
		infoStrBuffer.append(String.format("stck_mois      : %5.3f"  , this.stck_mois));
		
		// return
		return infoStrBuffer.toString();

	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the stck_oxyg
	 */
	public double getStck_oxyg() {
		return stck_oxyg;
	}

	/**
	 * @param stck_oxyg the stck_oxyg to set
	 */
	public void setStck_oxyg(double stck_oxyg) {
		this.stck_oxyg = stck_oxyg;
	}

	/**
	 * @return the stck_mois
	 */
	public double getStck_mois() {
		return stck_mois;
	}

	/**
	 * @param stck_mois the stck_mois to set
	 */
	public void setStck_mois(double stck_mois) {
		this.stck_mois = stck_mois;
	}



} // end of class
