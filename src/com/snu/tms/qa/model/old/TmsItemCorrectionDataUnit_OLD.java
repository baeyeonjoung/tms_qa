/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.old;

/**
 * Data Element for TMS Data for Single Item
 * 
 */
public class TmsItemCorrectionDataUnit_OLD {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String          timestr = "";
	private  double          beforeCorrectionvalue   = 0D;
	private  double          afterCorrectionvalue    = 0D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsSingleItemDataUnit
	 */
	public TmsItemCorrectionDataUnit_OLD() {}

	public TmsItemCorrectionDataUnit_OLD(
			String  timestr,
			double  beforeCorrectionvalue,
			double  afterCorrectionvalue) {
		
		this.timestr = timestr;
		this.beforeCorrectionvalue = beforeCorrectionvalue;
		this.afterCorrectionvalue  = afterCorrectionvalue;
	}

	public TmsItemCorrectionDataUnit_OLD(
			String  timestr,
			double  afterCorrectionvalue) {
		
		this.timestr = timestr;
		this.afterCorrectionvalue  = afterCorrectionvalue;
	}

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the timestr
	 */
	public String getTimestr() {
		return timestr;
	}

	/**
	 * @param timestr the timestr to set
	 */
	public void setTimestr(String timestr) {
		this.timestr = timestr;
	}

	/**
	 * @return the beforeCorrectionvalue
	 */
	public double getBeforeCorrectionvalue() {
		return beforeCorrectionvalue;
	}

	/**
	 * @param beforeCorrectionvalue the beforeCorrectionvalue to set
	 */
	public void setBeforeCorrectionvalue(double beforeCorrectionvalue) {
		this.beforeCorrectionvalue = beforeCorrectionvalue;
	}

	/**
	 * @return the afterCorrectionvalue
	 */
	public double getAfterCorrectionvalue() {
		return afterCorrectionvalue;
	}

	/**
	 * @param afterCorrectionvalue the afterCorrectionvalue to set
	 */
	public void setAfterCorrectionvalue(double afterCorrectionvalue) {
		this.afterCorrectionvalue = afterCorrectionvalue;
	}




} // end of class
