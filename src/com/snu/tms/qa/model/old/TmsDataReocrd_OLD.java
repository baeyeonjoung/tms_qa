/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.old;

/**
 * Data Element for TMS Data
 * 
 */
public class TmsDataReocrd_OLD {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String          timestr = "";
	private  long            time = 0L;
	private  double          tsp     = 0D;
	private  double          so2     = 0D;
	private  double          nox     = 0D;
	private  double          hcl     = 0D;
	private  double          hf      = 0D;
	private  double          nh3     = 0D;
	private  double          co      = 0D;
	private  double          o2      = 0D;
	private  double          flw     = 0D;
	private  double          tmp     = 0D;
	private  double          tm1     = 0D;
	private  double          tm2     = 0D;
	private  double          tm3     = 0D;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public TmsDataReocrd_OLD() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// Append information
		infoStrBuffer.append(String.format("TimeStr        : %s\n", this.timestr));
		infoStrBuffer.append(String.format("TSP            : %5.3f\n", this.tsp));
		infoStrBuffer.append(String.format("SO2            : %5.3f\n", this.so2));
		infoStrBuffer.append(String.format("NOx            : %5.3f\n", this.nox));
		infoStrBuffer.append(String.format("HCL            : %5.3f\n", this.hcl));
		infoStrBuffer.append(String.format("HF             : %5.3f\n", this.hf));
		infoStrBuffer.append(String.format("NH3            : %5.3f\n", this.nh3));
		infoStrBuffer.append(String.format("CO             : %5.3f\n", this.co));
		infoStrBuffer.append(String.format("O2             : %5.3f\n", this.o2));
		infoStrBuffer.append(String.format("FLW            : %5.3f\n", this.flw));
		infoStrBuffer.append(String.format("TMP            : %5.3f\n", this.tmp));
		infoStrBuffer.append(String.format("TM1            : %5.3f\n", this.tm1));
		infoStrBuffer.append(String.format("TM2            : %5.3f\n", this.tm2));
		infoStrBuffer.append(String.format("TM3            : %5.3f"  , this.tm3));
		
		// return
		return infoStrBuffer.toString();

	} // end of method


	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * @return the timeStr
	 */
	public String getTimestr() {
		return timestr;
	}

	/**
	 * @param timeStr the timeStr to set
	 */
	public void setTimestr(String timestr) {
		this.timestr = timestr;
	}

	/**
	 * @return the tsp
	 */
	public double getTsp() {
		return tsp;
	}

	/**
	 * @param tsp the tsp to set
	 */
	public void setTsp(double tsp) {
		this.tsp = tsp;
	}

	/**
	 * @return the so2
	 */
	public double getSo2() {
		return so2;
	}

	/**
	 * @param so2 the so2 to set
	 */
	public void setSo2(double so2) {
		this.so2 = so2;
	}

	/**
	 * @return the nox
	 */
	public double getNox() {
		return nox;
	}

	/**
	 * @param nox the nox to set
	 */
	public void setNox(double nox) {
		this.nox = nox;
	}

	/**
	 * @return the hcl
	 */
	public double getHcl() {
		return hcl;
	}

	/**
	 * @param hcl the hcl to set
	 */
	public void setHcl(double hcl) {
		this.hcl = hcl;
	}

	/**
	 * @return the hf
	 */
	public double getHf() {
		return hf;
	}

	/**
	 * @param hf the hf to set
	 */
	public void setHf(double hf) {
		this.hf = hf;
	}

	/**
	 * @return the nh3
	 */
	public double getNh3() {
		return nh3;
	}

	/**
	 * @param nh3 the nh3 to set
	 */
	public void setNh3(double nh3) {
		this.nh3 = nh3;
	}

	/**
	 * @return the co
	 */
	public double getCo() {
		return co;
	}

	/**
	 * @param co the co to set
	 */
	public void setCo(double co) {
		this.co = co;
	}

	/**
	 * @return the o2
	 */
	public double getO2() {
		return o2;
	}

	/**
	 * @param o2 the o2 to set
	 */
	public void setO2(double o2) {
		this.o2 = o2;
	}

	/**
	 * @return the flw
	 */
	public double getFlw() {
		return flw;
	}

	/**
	 * @param flw the flw to set
	 */
	public void setFlw(double flw) {
		this.flw = flw;
	}

	/**
	 * @return the tmp
	 */
	public double getTmp() {
		return tmp;
	}

	/**
	 * @param tmp the tmp to set
	 */
	public void setTmp(double tmp) {
		this.tmp = tmp;
	}

	/**
	 * @return the tm1
	 */
	public double getTm1() {
		return tm1;
	}

	/**
	 * @param tm1 the tm1 to set
	 */
	public void setTm1(double tm1) {
		this.tm1 = tm1;
	}

	/**
	 * @return the tm2
	 */
	public double getTm2() {
		return tm2;
	}

	/**
	 * @param tm2 the tm2 to set
	 */
	public void setTm2(double tm2) {
		this.tm2 = tm2;
	}

	/**
	 * @return the tm3
	 */
	public double getTm3() {
		return tm3;
	}

	/**
	 * @param tm3 the tm3 to set
	 */
	public void setTm3(double tm3) {
		this.tm3 = tm3;
	}



} // end of class
