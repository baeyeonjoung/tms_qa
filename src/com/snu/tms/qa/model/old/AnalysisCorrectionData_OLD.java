/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model.old;

import java.util.ArrayList;
import java.util.List;

import com.snu.tms.qa.analysis.correction.DoReverseCorrection_OLD;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.TmsDataHeader;
import com.snu.tms.qa.model.db.CorrectionRecord;

/**
 * Data for analyzing the correction effect
 * 
 */
public class AnalysisCorrectionData_OLD {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int               itemCode         = ItemCodeDefine.UNDEFINED;
	private  TmsDataHeader     dataHeader       = null;
	private  CorrectionFactor_OLD  correctionFactor = null;
	private  CorrectionRecord    correctionItem   = null;
		
	private  ArrayList<TmsItemCorrectionDataUnit_OLD> dataArray = new ArrayList<TmsItemCorrectionDataUnit_OLD>();

	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsSingleItemDataArray
	 */
	public AnalysisCorrectionData_OLD() {}

	public AnalysisCorrectionData_OLD(
			int               itemCode,
			TmsDataHeader     dataHeader,
			CorrectionFactor_OLD  correctionFactor,
			CorrectionRecord    correctionItem,
			List<TmsDataUnit_OLD> dataList) {
		
		this.itemCode   = itemCode;
		this.dataHeader = dataHeader;
		this.correctionFactor = correctionFactor;
		this.correctionItem = correctionItem;
		
		// Set target item data
		init(dataList);
	}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// Append information
		for (TmsItemCorrectionDataUnit_OLD data : this.dataArray) {
			infoStrBuffer.append(String.format("%5.3f  :  %5.3f\n", data.getBeforeCorrectionvalue(), data.getAfterCorrectionvalue()));
		}
		
		// return
		return infoStrBuffer.toString();

	} // end of method
	
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Fill dataArray based on List<TmsDataUnit> from database query
	 */
	public  void  init(List<TmsDataUnit_OLD> dataList) {
		
		// check data validation
		if (dataList == null  ||  dataList.size() == 0) {
			return;
		}
		
		// copy queried data to afterTmsSingleItemData
		for (TmsDataUnit_OLD  tmsDataUnit : dataList) {
			
			switch(itemCode) {
			case ItemCodeDefine.TSP:
				if (tmsDataUnit.getTsp() > 0.0D) {
					double beforeValue = doReverseCorrection(tmsDataUnit.getTsp(), tmsDataUnit.getTmp(), tmsDataUnit.getO2());
					this.dataArray.add(
							new TmsItemCorrectionDataUnit_OLD(tmsDataUnit.getTimestr(), beforeValue, tmsDataUnit.getTsp()));
				}
				break;
			case ItemCodeDefine.SOX:
				if (tmsDataUnit.getSo2() > 0.0D) {
					double beforeValue = doReverseCorrection(tmsDataUnit.getSo2(), tmsDataUnit.getTmp(), tmsDataUnit.getO2());
					this.dataArray.add(
							new TmsItemCorrectionDataUnit_OLD(tmsDataUnit.getTimestr(), beforeValue, tmsDataUnit.getSo2()));
				}
				break;
			case ItemCodeDefine.NOX:
				if (tmsDataUnit.getNox() > 0.0D) {
					double beforeValue = doReverseCorrection(tmsDataUnit.getNox(), tmsDataUnit.getTmp(), tmsDataUnit.getO2());
					this.dataArray.add(
							new TmsItemCorrectionDataUnit_OLD(tmsDataUnit.getTimestr(), beforeValue, tmsDataUnit.getNox()));
				}
				break;
			case ItemCodeDefine.HCL:
				if (tmsDataUnit.getHcl() > 0.0D) {
					double beforeValue = doReverseCorrection(tmsDataUnit.getHcl(), tmsDataUnit.getTmp(), tmsDataUnit.getO2());
					this.dataArray.add(
							new TmsItemCorrectionDataUnit_OLD(tmsDataUnit.getTimestr(), beforeValue, tmsDataUnit.getHcl()));
				}
				break;
			case ItemCodeDefine.HF:
				if (tmsDataUnit.getHf() > 0.0D) {
					double beforeValue = doReverseCorrection(tmsDataUnit.getHf(), tmsDataUnit.getTmp(), tmsDataUnit.getO2());
					this.dataArray.add(
							new TmsItemCorrectionDataUnit_OLD(tmsDataUnit.getTimestr(), beforeValue, tmsDataUnit.getHf()));
				}
				break;
			case ItemCodeDefine.NH3:
				if (tmsDataUnit.getNh3() > 0.0D) {
					double beforeValue = doReverseCorrection(tmsDataUnit.getNh3(), tmsDataUnit.getTmp(), tmsDataUnit.getO2());
					this.dataArray.add(
							new TmsItemCorrectionDataUnit_OLD(tmsDataUnit.getTimestr(), beforeValue, tmsDataUnit.getNh3()));
				}
				break;
			case ItemCodeDefine.CO:
				if (tmsDataUnit.getCo() > 0.0D) {
					double beforeValue = doReverseCorrection(tmsDataUnit.getCo(), tmsDataUnit.getTmp(), tmsDataUnit.getO2());
					this.dataArray.add(
							new TmsItemCorrectionDataUnit_OLD(tmsDataUnit.getTimestr(), beforeValue, tmsDataUnit.getCo()));
				}
				break;
			case ItemCodeDefine.O2:
				if (tmsDataUnit.getO2() > 0.0D) {
					this.dataArray.add(
							new TmsItemCorrectionDataUnit_OLD(tmsDataUnit.getTimestr(), tmsDataUnit.getO2(), tmsDataUnit.getO2()));
				}
				break;
			case ItemCodeDefine.FL1:
				if (tmsDataUnit.getFlw() > 0.0D) {
					double beforeValue = doReverseCorrectionFlow(tmsDataUnit.getFlw(), tmsDataUnit.getTmp(), tmsDataUnit.getO2());
					this.dataArray.add(
							new TmsItemCorrectionDataUnit_OLD(tmsDataUnit.getTimestr(), beforeValue, tmsDataUnit.getFlw()));
				}
				break;
			case ItemCodeDefine.TMP:
				if (tmsDataUnit.getTmp() > 0.0D) {
					this.dataArray.add(
							new TmsItemCorrectionDataUnit_OLD(tmsDataUnit.getTimestr(), tmsDataUnit.getTmp(), tmsDataUnit.getTmp()));
				}
				break;
			case ItemCodeDefine.TM1:
				if (tmsDataUnit.getTm1() > 0.0D) {
					this.dataArray.add(
							new TmsItemCorrectionDataUnit_OLD(tmsDataUnit.getTimestr(), tmsDataUnit.getTm1(), tmsDataUnit.getTm1()));
				}
				break;
			case ItemCodeDefine.TM2:
				if (tmsDataUnit.getTm2() > 0.0D) {
					this.dataArray.add(
							new TmsItemCorrectionDataUnit_OLD(tmsDataUnit.getTimestr(), tmsDataUnit.getTm2(), tmsDataUnit.getTm2()));
				}
				break;
			case ItemCodeDefine.TM3:
				if (tmsDataUnit.getTm3() > 0.0D) {
					this.dataArray.add(
							new TmsItemCorrectionDataUnit_OLD(tmsDataUnit.getTimestr(), tmsDataUnit.getTm3(), tmsDataUnit.getTm3()));
				}
				break;
			}
		}
		
	}
	
	/**
	 * Convert afterCorrectionValue to beforeCorrectionValue (except flow value)
	 */
	public  double  doReverseCorrection(double value, double temp, double o2 ) {
	
		// Set variable
		double beforeValue = value;
		
		// do temperature reverse correction
		if (this.correctionItem.getTemp_corr().equals("Y") && temp != -1.0D) {
			beforeValue = DoReverseCorrection_OLD.doTemperatureReverseCorrection(beforeValue, temp);
		}
		
		// do oxygen reverse correction
		if (this.correctionItem.getOxyg_corr().equals("Y") && o2 != -1.0D && this.correctionFactor.getStck_oxyg() != -1.0D) {
			beforeValue = DoReverseCorrection_OLD.doOxygenReverseCorrection(beforeValue, this.correctionFactor.getStck_oxyg(), o2);
		}
		
		// do moisture reverse correction
		if (this.correctionItem.getMois_corr().equals("Y") && this.correctionFactor.getStck_mois() != -1.0D) {
			beforeValue = DoReverseCorrection_OLD.doMoistureReverseCorrection(beforeValue, this.correctionFactor.getStck_mois());
		}
		
		return beforeValue;
		
	}
	
	
	/**
	 * Convert afterCorrectionValue to beforeCorrectionValue in case of flow
	 */
	public  double  doReverseCorrectionFlow(double value, double temp, double o2 ) {
	
		// Set variable
		double beforeValue = value;
		
		// do temperature reverse correction
		//if (this.correctionItem.getTemp_corr() == 1 && temp != -1.0D) {
		//	beforeValue = DoReverseCorrection.doTemperatureReverseCorrection(beforeValue, temp);
		//}
		
		// do oxygen reverse correction
		//if (this.correctionItem.getOxyg_corr() == 1 && o2 != -1.0D && this.correctionFactor.getStck_oxyg() != -1.0D) {
		//	beforeValue = DoReverseCorrection.doOxygenReverseCorrection(beforeValue, this.correctionFactor.getStck_oxyg(), o2);
		//}
		
		// do moisture reverse correction
		if (this.correctionItem.getMois_corr().equals("Y") && this.correctionFactor.getStck_mois() != -1.0D) {
			beforeValue = DoReverseCorrection_OLD.doMoistureReverseCorrectionFlow(beforeValue, this.correctionFactor.getStck_mois());
		}

		return beforeValue;
		
	}

	/**
	 * @return the itemCode
	 */
	public int getItemCode() {
		return itemCode;
	}

	/**
	 * @param itemCode the itemCode to set
	 */
	public void setItemCode(int itemCode) {
		this.itemCode = itemCode;
	}

	/**
	 * @return the dataHeader
	 */
	public TmsDataHeader getDataHeader() {
		return dataHeader;
	}

	/**
	 * @param dataHeader the dataHeader to set
	 */
	public void setDataHeader(TmsDataHeader dataHeader) {
		this.dataHeader = dataHeader;
	}

	/**
	 * @return the correctionFactor
	 */
	public CorrectionFactor_OLD getCorrectionFactor() {
		return correctionFactor;
	}

	/**
	 * @param correctionFactor the correctionFactor to set
	 */
	public void setCorrectionFactor(CorrectionFactor_OLD correctionFactor) {
		this.correctionFactor = correctionFactor;
	}

	/**
	 * @return the correctionItem
	 */
	public CorrectionRecord getCorrectionItem() {
		return correctionItem;
	}

	/**
	 * @param correctionItem the correctionItem to set
	 */
	public void setCorrectionItem(CorrectionRecord correctionItem) {
		this.correctionItem = correctionItem;
	}

	/**
	 * @return the dataArray
	 */
	public ArrayList<TmsItemCorrectionDataUnit_OLD> getDataArray() {
		return dataArray;
	}

	/**
	 * @param dataArray the dataArray to set
	 */
	public void setDataArray(ArrayList<TmsItemCorrectionDataUnit_OLD> dataArray) {
		this.dataArray = dataArray;
	}
	
	/**
	 * Get beforeDataArray
	 */
	public double[] getBeforeDataArray() {
		
		double[] results = new double[this.dataArray.size()];
		for (int i=0 ; i<results.length ; i++) {
			results[i] = ((TmsItemCorrectionDataUnit_OLD)this.dataArray.get(i)).getBeforeCorrectionvalue();
		}
		
		return results;
	}
	
	/**
	 * Get afterDataArray
	 */
	public double[] getAfterDataArray() {
		
		double[] results = new double[this.dataArray.size()];
		for (int i=0 ; i<results.length ; i++) {
			results[i] = ((TmsItemCorrectionDataUnit_OLD)this.dataArray.get(i)).getAfterCorrectionvalue();
		}
		
		return results;
	}
	
	
	/**
	 * Get TimeStr Array
	 */
	public String[] getTimeStrArray() {
		
		String[] results = new String[this.dataArray.size()];
		for (int i=0 ; i<results.length ; i++) {
			results[i] = ((TmsItemCorrectionDataUnit_OLD)this.dataArray.get(i)).getTimestr();
		}
		
		return results;
	}


} // end of class
