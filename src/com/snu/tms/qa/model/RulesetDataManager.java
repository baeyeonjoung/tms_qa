/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Data Element for TMS Data
 * 
 */
public class RulesetDataManager {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(RulesetDataManager.class);

	private static RulesetDataManager instance = new RulesetDataManager();
	
	//==========================================================================
	// Local Fields
	//==========================================================================

	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for RulesetDataManager
	 */
	private RulesetDataManager() {}

	//==========================================================================
	// Implemented Methods
	//==========================================================================


	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * getInstance()
	 */
	public static RulesetDataManager getInstance() {
		return instance;
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================

	
} // end of class
