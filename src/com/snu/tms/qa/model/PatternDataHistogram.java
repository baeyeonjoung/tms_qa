/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;


/**
 * Pattern Data Set
 * 
 */
public class PatternDataHistogram {

	//==========================================================================
	// Static Fields
	//==========================================================================
	public final static int  MAX_BINS = 20;
	//==========================================================================
	// Local Fields
	//==========================================================================
	private  int      patternId;
	private  int      item;
	
	private  double   minRange = 0.0D;
	private  double   maxRange = 0.0D;
	
	private  int      binNo    = 0;
	private  int      interval = 0;
	
	private  double[][]  densities = null;
	
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for PatternDataHistogram
	 */
	public PatternDataHistogram(
			int patternId, 
			int item) {
		
		// set parameters
		this.patternId = patternId;
		this.item      = item;
		
	} // end of constructor

	public PatternDataHistogram(
			int patternId, 
			int item, 
			double[] range, 
			double[] values) {
		
		// set parameters
		this.patternId = patternId;
		this.item      = item;
		
		// set histogram
		setHistogram(values, range);
		
	} // end of constructor

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * set histogram
	 */
	private void setHistogram(double[] values, double[] range) {
		
		/**
		 * UPDATE : 2016-08-08
		// set min and max range
		this.minRange = Math.floor(range[0]);
		this.maxRange = Math.ceil(range[1]);
		*/
		
		// Set 98%ile range
		DescriptiveStatistics intervalStats = new DescriptiveStatistics();
		for (int j=0 ; j<values.length ; j++) {
			intervalStats.addValue(values[j]);
		}
		double lowerRange = intervalStats.getPercentile( 0.50D);
		double upperRange = intervalStats.getPercentile(99.50D);
		
		// set number of bins
		double inter = (upperRange - lowerRange) / (double)MAX_BINS;
		inter = setIntervalRange(inter);

		this.minRange = Math.floor(lowerRange/inter) * inter;
		this.maxRange = Math.ceil(upperRange/inter) * inter;

		double bins = (this.maxRange - this.minRange) / inter;
		bins = Math.ceil(bins);
		this.maxRange = this.minRange + inter * bins;
		
		this.binNo = (int) bins;
		this.interval = (int) inter;
		
		// set density
		this.densities = new double[this.binNo][2];
		for (int i=0 ; i<this.densities.length ; i++) {
			this.densities[i][0] = this.minRange + ((double)i) * (double)this.interval;
			this.densities[i][1] = 0.0D;
		}
		
		// calculate frequency
		double N = (double) values.length;
		for (int i=0 ; i<values.length ; i++) {

			double position = (values[i] - this.minRange) / (double)this.interval;
			if (position < 0.0D) {
				continue;
			} else if (position >= (double)this.binNo) {
				position = (double) (this.binNo - 1);
			}
			position = Math.floor(position);
			this.densities[(int)position][1] += 1.0D;
		}
		
		// set dendity and smoothing job
		for (int i=0 ; i<this.densities.length ; i++) {
			this.densities[i][1] = this.densities[i][1] / N;
		}
		
		// density interpolate
		//System.out.println("\n=====================");
		for (int i=0 ; i<this.densities.length ; i++) {
			this.densities[i][1] = doInterpolate(this.densities, i);
			//System.out.println(this.densities[i][1]);
		}
		
		/**
		for (int i=0 ; i<this.densities.length ; i++) {

			if (i == 0) {
				this.densities[i][1] = (this.densities[i][1] * 0.6D + this.densities[i+1][1] * 0.2D) / N;
			} else if (i == this.densities.length-1) {
				this.densities[i][1] = (this.densities[i-1][1] * 0.2D + this.densities[i][1] * 0.6D) / N;
			} else {
				this.densities[i][1] = (this.densities[i-1][1] * 0.2D + this.densities[i][1] * 0.6D + this.densities[i+1][1] * 0.2D) / N;
			}
			
		} // end of for-loop
		*/
		
		// logging
		/**
		System.out.println(String.format(
				"item:%d, Min:%.3f, Max:%.3f, Bin:%d, Interval:%d", 
				this.item,
				this.minRange,
				this.maxRange,
				this.binNo,
				this.interval));
		*/
		
	} // end of method
	
	/**
	 * do interpolate the density distribution
	 */
	private double doInterpolate(double[][] pdf, int index) {
		
		if (index == 0 || pdf[index][0] == 0.0D) {
			return pdf[index][1];
		}
		
		if (pdf[index][1] > 0.01D) {
			return pdf[index][1];
		}
		
		int minIndex = Math.max(0, index-5);
		int maxIndex = Math.min(pdf.length-1, index+5);
		
		double minNonZeroValue = 0.0D;
		double maxNonZeroValue = 0.0D;
		for (int i=index ; i>=minIndex ; i--) {
			if (pdf[i][1] > 0.01D) {
				minNonZeroValue = pdf[i][1];
				break;
			}
		}
		for (int i=index ; i<=maxIndex ; i++) {
			if (pdf[i][1] > 0.01D) {
				maxNonZeroValue = pdf[i][1];
				break;
			}
		}
		
		return (minNonZeroValue + maxNonZeroValue) / 2.0D;
		
	} // end of method
	
	/**
	 * set adequate interval value
	 */
	private double setIntervalRange(double inter) {
		
		if (inter <= 5.0D) {
			return Math.ceil(inter);
		} else if (inter < 10.0D) {
			return 5.0D;
		} else if (inter < 20.0D) {
			return 10.0D;
		} else if (inter < 50.0D) {
			return (double)((int)(inter / 10.0D)) * 10.0D;
		} else if (inter < 100.0D) {
			return 50.0D;
		} else if (inter < 500.0D) {
			return (double)((int)(inter / 50.0D)) * 50.0D;
		} else if (inter < 1000.0D) {
			return (double)((int)(inter / 100.0D)) * 100.0D;
		} else if (inter < 10000.0) {
			return (double)((int)(inter / 500.0D)) * 500.0D;
		} else if (inter < 100000.0D) {
			return (double)((int)(inter / 5000.0D)) * 5000.0D;
		} else {
			return 20000.0D;
		}
		
	} // end of method
	
	/**
	 * get pattern conformity score
	 */
	public double getConformityScore(double value) {
		
		// if value is less then min_value
		if (value < this.minRange) {
			return 0.001D;
		} else if (value > this.maxRange) {
			return 0.001D;
		} else {
			// 히스토그램의 bin 수에 따라 확률의 변화 크기가 달라질 수 있으므로 이를 반영
			// 즉, 2개로 분할된 확률과 10개로 분할된 확률 분포의 기대값이 서로 다르기 때문.
			double binNoEffect = (double)binNo / (double)MAX_BINS;
			for (int i=0 ; i<densities.length ; i++) {
				
				if (value >= densities[i][0] && value <= (densities[i][0] + (double)interval)) {
					return densities[i][1] * ItemCodeDefine.getItemWeighting(item) / binNoEffect;
					//return densities[i][1] / binNoEffect;
				}
				
			}
			return 0.0D;
		}
		
	} // end of method

	/**
	 * @return the patternId
	 */
	public int getPatternId() {
		return patternId;
	}

	/**
	 * @param patternId the patternId to set
	 */
	public void setPatternId(int patternId) {
		this.patternId = patternId;
	}

	/**
	 * @return the item
	 */
	public int getItem() {
		return item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(int item) {
		this.item = item;
	}

	/**
	 * @return the minRange
	 */
	public double getMinRange() {
		return minRange;
	}

	/**
	 * @param minRange the minRange to set
	 */
	public void setMinRange(double minRange) {
		this.minRange = minRange;
	}

	/**
	 * @return the maxRange
	 */
	public double getMaxRange() {
		return maxRange;
	}

	/**
	 * @param maxRange the maxRange to set
	 */
	public void setMaxRange(double maxRange) {
		this.maxRange = maxRange;
	}

	/**
	 * @return the binNo
	 */
	public int getBinNo() {
		return binNo;
	}

	/**
	 * @param binNo the binNo to set
	 */
	public void setBinNo(int binNo) {
		this.binNo = binNo;
	}

	/**
	 * @return the interval
	 */
	public int getInterval() {
		return interval;
	}

	/**
	 * @param interval the interval to set
	 */
	public void setInterval(int interval) {
		this.interval = interval;
	}

	/**
	 * @return the densities
	 */
	public double[][] getDensities() {
		return densities;
	}

	/**
	 * @param densities the densities to set
	 */
	public void setDensities(double[][] densities) {
		this.densities = densities;
	}

	/**
	 * @param densities the densities to set
	 */
	public void setDensities(String densityStr) {
		
		// split target string
		String[] splitStr = densityStr.split("\\|");
		if (splitStr == null || splitStr.length == 0) {
			return;
		}
		
		// memory allocation
		this.densities = new double[splitStr.length][2];
		
		// parse
		for (int i=0 ; i<splitStr.length ; i++) {
			
			String[] detailStr = splitStr[i].trim().split(",");
			if (detailStr == null || detailStr.length < 2) {
				continue;
			}
			
			this.densities[i][0] = Double.parseDouble(detailStr[0]);
			this.densities[i][1] = Double.parseDouble(detailStr[1]);
			
			//System.out.println(String.format("%10s - %6.3f - %6.3f", 
			//		ItemCodeDefine.ITEM_NAME[this.item], 
			//		this.densities[i][0], this.densities[i][1]));
			
		} // end of for-loop
		
	} // end of method

	/**
	 * get density string
	 */
	public String getDensitiesString() {
		
		StringBuffer strBuffer = new StringBuffer();
		for (int i=0 ; i<this.densities.length ; i++) {
			
			strBuffer.append(String.format("%.3f,  %.3f", this.densities[i][0], this.densities[i][1]));
			strBuffer.append("|");
			
		}
		return strBuffer.toString();
		
	} // end of method
	
} // end of class
