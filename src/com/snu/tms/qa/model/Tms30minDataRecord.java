/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.model;

import java.util.Arrays;

import com.snu.tms.qa.util.DateUtil;

/**
 * Data Element for TMS Data
 * 
 */
public class Tms30minDataRecord {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Local Fields
	//==========================================================================
	private  String          timestr = "";
	private  long            time = 0L;
	
	private  Tms30minDataCell     tsp     = new Tms30minDataCell(ItemCodeDefine.TSP);
	private  Tms30minDataCell     so2     = new Tms30minDataCell(ItemCodeDefine.SOX);
	private  Tms30minDataCell     nox     = new Tms30minDataCell(ItemCodeDefine.NOX);
	private  Tms30minDataCell     hcl     = new Tms30minDataCell(ItemCodeDefine.HCL);
	private  Tms30minDataCell     hf      = new Tms30minDataCell(ItemCodeDefine.HF);
	private  Tms30minDataCell     nh3     = new Tms30minDataCell(ItemCodeDefine.NH3);
	private  Tms30minDataCell     co      = new Tms30minDataCell(ItemCodeDefine.CO);
	private  Tms30minDataCell     o2      = new Tms30minDataCell(ItemCodeDefine.O2);
	private  Tms30minDataCell     flw     = new Tms30minDataCell(ItemCodeDefine.FL1);
	private  Tms30minDataCell     fl2     = new Tms30minDataCell(ItemCodeDefine.FL2);
	private  Tms30minDataCell     tmp     = new Tms30minDataCell(ItemCodeDefine.TMP);
	private  Tms30minDataCell     tm1     = new Tms30minDataCell(ItemCodeDefine.TM1);
	private  Tms30minDataCell     tm2     = new Tms30minDataCell(ItemCodeDefine.TM2);
	private  Tms30minDataCell     tm3     = new Tms30minDataCell(ItemCodeDefine.TM3);
	
	private  int  outlierLevel = 0;
		
	//==========================================================================
	// Constructors
	//==========================================================================
	/**
	 * Constructor for TmsDataUnit
	 */
	public Tms30minDataRecord() {}
	
	public Tms30minDataRecord(long time) {
		this.time = time;
		this.timestr = DateUtil.getDateStr(time);
	}

	//==========================================================================
	// Implemented Methods
	//==========================================================================
	/**
	 * Implemented Method for toString()
	 */
	@Override
	public String toString() {

		// Set StringBuffer
		StringBuffer infoStrBuffer = new StringBuffer();
		
		// Append information
		infoStrBuffer.append(this.timestr + "\n");
		if (this.tsp != null) {
			infoStrBuffer.append(this.tsp.toString() + "\n");
		}
		if (this.so2 != null) {
			infoStrBuffer.append(this.so2.toString() + "\n");
		}
		if (this.nox != null) {
			infoStrBuffer.append(this.nox.toString() + "\n");
		}
		if (this.hcl != null) {
			infoStrBuffer.append(this.hcl.toString() + "\n");
		}
		if (this.hf != null) {
			infoStrBuffer.append(this.hf.toString() + "\n");
		}
		if (this.nh3 != null) {
			infoStrBuffer.append(this.nh3.toString() + "\n");
		}
		if (this.co != null) {
			infoStrBuffer.append(this.co.toString() + "\n");
		}
		if (this.o2 != null) {
			infoStrBuffer.append(this.o2.toString() + "\n");
		}
		if (this.flw != null) {
			infoStrBuffer.append(this.flw.toString() + "\n");
		}
		if (this.tmp != null) {
			infoStrBuffer.append(this.tmp.toString() + "\n");
		}
		if (this.tm1 != null) {
			infoStrBuffer.append(this.tm1.toString() + "\n");
		}
		if (this.tm2 != null) {
			infoStrBuffer.append(this.tm2.toString() + "\n");
		}
		if (this.tm3 != null) {
			infoStrBuffer.append(this.tm3.toString() + "\n");
		}
		if (this.fl2 != null) {
			infoStrBuffer.append(this.fl2.toString() + "\n");
		}
		// return
		return infoStrBuffer.toString();

	} // end of method

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * 
	 */
	public  void  setValue(
			int    item_code,
			double half_valu,
			String half_over,
			String half_stat,
			String half_dlst,
			String stat_code,
			double stat_valu) {
		
		switch(item_code) {
		case ItemCodeDefine.TSP:
			this.tsp.setValue(half_valu, half_over, half_stat, half_dlst, stat_code, stat_valu);
			break;
		case ItemCodeDefine.SOX:
			this.so2.setValue(half_valu, half_over, half_stat, half_dlst, stat_code, stat_valu);
			break;
		case ItemCodeDefine.NOX:
			this.nox.setValue(half_valu, half_over, half_stat, half_dlst, stat_code, stat_valu);
			break;
		case ItemCodeDefine.HCL:
			this.hcl.setValue(half_valu, half_over, half_stat, half_dlst, stat_code, stat_valu);
			break;
		case ItemCodeDefine.HF:
			this.hf.setValue(half_valu, half_over, half_stat, half_dlst, stat_code, stat_valu);
			break;
		case ItemCodeDefine.NH3:
			this.nh3.setValue(half_valu, half_over, half_stat, half_dlst, stat_code, stat_valu);
			break;
		case ItemCodeDefine.CO:
			this.co.setValue(half_valu, half_over, half_stat, half_dlst, stat_code, stat_valu);
			break;
		case ItemCodeDefine.O2:
			this.o2.setValue(half_valu, half_over, half_stat, half_dlst, stat_code, stat_valu);
			break;
		case ItemCodeDefine.FL1:
			this.flw.setValue(half_valu, half_over, half_stat, half_dlst, stat_code, stat_valu);
			break;
		case ItemCodeDefine.TMP:
			this.tmp.setValue(half_valu, half_over, half_stat, half_dlst, stat_code, stat_valu);
			break;
		case ItemCodeDefine.TM1:
			this.tm1.setValue(half_valu, half_over, half_stat, half_dlst, stat_code, stat_valu);
			break;
		case ItemCodeDefine.TM2:
			this.tm2.setValue(half_valu, half_over, half_stat, half_dlst, stat_code, stat_valu);
			break;
		case ItemCodeDefine.TM3:
			this.tm3.setValue(half_valu, half_over, half_stat, half_dlst, stat_code, stat_valu);
			break;
		case ItemCodeDefine.FL2:
			this.fl2.setValue(half_valu, half_over, half_stat, half_dlst, stat_code, stat_valu);
			break;
		}
		
	} // end of method
	
	/**
	 * @return the timestr
	 */
	public String getTimestr() {
		return timestr;
	}

	/**
	 * @param timestr the timestr to set
	 */
	public void setTimestr(String timestr) {
		this.timestr = timestr;
	}

	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}

	/**
	 * 
	 */
	public Tms30minDataCell getDataCell(int item) {
		
		switch (item) {
		case ItemCodeDefine.TSP:
			return this.tsp;
		case ItemCodeDefine.SOX:
			return this.so2;
		case ItemCodeDefine.NOX:
			return this.nox;
		case ItemCodeDefine.HCL:
			return this.hcl;
		case ItemCodeDefine.HF:
			return this.hf;
		case ItemCodeDefine.NH3:
			return this.nh3;
		case ItemCodeDefine.CO:
			return this.co;
		case ItemCodeDefine.O2:
			return this.o2;
		case ItemCodeDefine.FL1:
			return this.flw;
		case ItemCodeDefine.TMP:
			return this.tmp;
		case ItemCodeDefine.TM1:
			return this.tm1;
		case ItemCodeDefine.TM2:
			return this.tm2;
		case ItemCodeDefine.TM3:
			return this.tm3;
		case ItemCodeDefine.FL2:
			return this.fl2;
		default:
			return new Tms30minDataCell();
		}
	} // end of method
	
	/**
	 * Get valid items
	 */
	public int[] getValidItems() {
		
		int[] items = {ItemCodeDefine.TSP, ItemCodeDefine.SOX, ItemCodeDefine.NOX,
				ItemCodeDefine.HCL, ItemCodeDefine.HF, ItemCodeDefine.NH3,
				ItemCodeDefine.CO, ItemCodeDefine.O2, ItemCodeDefine.FL1,
				ItemCodeDefine.TMP, ItemCodeDefine.TM1, ItemCodeDefine.TM2,
				ItemCodeDefine.TM3, ItemCodeDefine.FL2};
		int[] validItems = new int[items.length];
		int index = 0;
		Tms30minDataCell dataCell;
		for (int i=0 ; i<items.length ; i++) {
			dataCell = getDataCell(items[i]);
			if (dataCell.getHalf_valu() != -1D) {
				validItems[index++] = items[i];
			}
		}
		
		return Arrays.copyOf(validItems, index);
		
	} // end of method
	
	/**
	 * @return the tsp
	 */
	public Tms30minDataCell getTsp() {
		return tsp;
	}

	/**
	 * @param tsp the tsp to set
	 */
	public void setTsp(Tms30minDataCell tsp) {
		this.tsp = tsp;
	}

	/**
	 * @return the so2
	 */
	public Tms30minDataCell getSo2() {
		return so2;
	}

	/**
	 * @param so2 the so2 to set
	 */
	public void setSo2(Tms30minDataCell so2) {
		this.so2 = so2;
	}

	/**
	 * @return the nox
	 */
	public Tms30minDataCell getNox() {
		return nox;
	}

	/**
	 * @param nox the nox to set
	 */
	public void setNox(Tms30minDataCell nox) {
		this.nox = nox;
	}

	/**
	 * @return the hcl
	 */
	public Tms30minDataCell getHcl() {
		return hcl;
	}

	/**
	 * @param hcl the hcl to set
	 */
	public void setHcl(Tms30minDataCell hcl) {
		this.hcl = hcl;
	}

	/**
	 * @return the hf
	 */
	public Tms30minDataCell getHf() {
		return hf;
	}

	/**
	 * @param hf the hf to set
	 */
	public void setHf(Tms30minDataCell hf) {
		this.hf = hf;
	}

	/**
	 * @return the nh3
	 */
	public Tms30minDataCell getNh3() {
		return nh3;
	}

	/**
	 * @param nh3 the nh3 to set
	 */
	public void setNh3(Tms30minDataCell nh3) {
		this.nh3 = nh3;
	}

	/**
	 * @return the co
	 */
	public Tms30minDataCell getCo() {
		return co;
	}

	/**
	 * @param co the co to set
	 */
	public void setCo(Tms30minDataCell co) {
		this.co = co;
	}

	/**
	 * @return the o2
	 */
	public Tms30minDataCell getO2() {
		return o2;
	}

	/**
	 * @param o2 the o2 to set
	 */
	public void setO2(Tms30minDataCell o2) {
		this.o2 = o2;
	}

	/**
	 * @return the flw
	 */
	public Tms30minDataCell getFlw() {
		return flw;
	}

	/**
	 * @param flw the flw to set
	 */
	public void setFlw(Tms30minDataCell flw) {
		this.flw = flw;
	}

	/**
	 * @return the tmp
	 */
	public Tms30minDataCell getTmp() {
		return tmp;
	}

	/**
	 * @return the outlier
	 */
	public int getOutlierLevel() {
		return outlierLevel;
	}

	/**
	 * @param outlier the outlier to set
	 */
	public void setOutlierLevel(int outlierLevel) {
		this.outlierLevel = outlierLevel;
	}

	/**
	 * @param tmp the tmp to set
	 */
	public void setTmp(Tms30minDataCell tmp) {
		this.tmp = tmp;
	}

	/**
	 * @return the tm1
	 */
	public Tms30minDataCell getTm1() {
		return tm1;
	}

	/**
	 * @param tm1 the tm1 to set
	 */
	public void setTm1(Tms30minDataCell tm1) {
		this.tm1 = tm1;
	}

	/**
	 * @return the tm2
	 */
	public Tms30minDataCell getTm2() {
		return tm2;
	}

	/**
	 * @param tm2 the tm2 to set
	 */
	public void setTm2(Tms30minDataCell tm2) {
		this.tm2 = tm2;
	}

	/**
	 * @return the tm3
	 */
	public Tms30minDataCell getTm3() {
		return tm3;
	}

	/**
	 * @param tm3 the tm3 to set
	 */
	public void setTm3(Tms30minDataCell tm3) {
		this.tm3 = tm3;
	}

	/**
	 * @return the fl2
	 */
	public Tms30minDataCell getFl2() {
		return fl2;
	}

	/**
	 * @param fl2 the fl2 to set
	 */
	public void setFl2(Tms30minDataCell fl2) {
		this.fl2 = fl2;
	}

} // end of class
