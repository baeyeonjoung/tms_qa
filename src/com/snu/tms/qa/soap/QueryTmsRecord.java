/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetTms30minRecordService;
import kr.or.webservice.server.interpace.GetTmsRecordService;
import kr.or.webservice.server.model.Tms30minRecord;
import kr.or.webservice.server.model.TmsRecord;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.snu.tms.qa.model.Tms30minDataset;
import com.snu.tms.qa.model.TmsDataHeader;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.util.DateUtil;

/**
 * Query Agent
 * 
 */
public class QueryTmsRecord {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryTmsRecord.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	
	/**
	 * @param start_time_str: time format (yyyy-MM-dd HH:mm:ss)
	 * @param end_time_str: time format (yyyy-MM-dd HH:mm:ss)
	 */
	public static TmsDataset queryTmsRecord(
			String fact_code, int stck_code, 
			String start_time_str, String end_time_str) {
		
		// Query FactInfo
		//FactRecord fact_record = QueryFactInfo.queryFactInfo(db_name, fact_code);
		//StckRecord stck_record = QueryStckInfo.queryStckInfo(db_name, fact_code, stck_code);
		
		// Create TmsDataSet
		TmsDataset tmsDataSet = new TmsDataset();
		TmsDataHeader tmsDataHeader = new TmsDataHeader();
		tmsDataHeader.setStart_time_str(start_time_str);
		tmsDataHeader.setEnd_time_str(end_time_str);
		tmsDataSet.setTmsDataHeader(tmsDataHeader);
		
		// Query TmsData
		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetTmsRecordService client = (GetTmsRecordService)context.getBean("clientTmsRecord");
			
			// set timeout : 20-min
			Client proxy = ClientProxy.getClient(client);
			HTTPConduit conduit = (HTTPConduit) proxy.getConduit();
			HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
			httpClientPolicy.setConnectionTimeout(1800000);
			httpClientPolicy.setReceiveTimeout(1800000);
			conduit.setClient(httpClientPolicy);
			
			// set max-child limits
			proxy.getEndpoint().put("org.apache.cxf.stax.maxChildElements", "3000000");
			
			// Get all data
			long queryStartTime = System.currentTimeMillis();
			logger.debug(String.format(
					"QueryStart (fact_code: %s, stck_code: %d, start_time: %s, end_time: %s)", 
					fact_code, stck_code, start_time_str, end_time_str));
			
			long targetTime = 0L;
			TmsRecord[] resultList = client.getTmsRecordAll(fact_code, ""+stck_code, start_time_str, end_time_str);
			for (TmsRecord result : resultList) {
				
				try {
					targetTime = DateUtil.getTmsDate(result.min_time);
					tmsDataSet.getTmsDataRecord(targetTime).setValue(
							Integer.parseInt(result.item_code), 
							Double.parseDouble(result.min_valu), 
							Double.parseDouble(result.min_valu_bf));
				} catch (Exception ee) {}
			}
			
			long queryFinishTime = System.currentTimeMillis();
			logger.debug(String.format(
					"Query Finished. (query_time: %d seconds, query_records: %d, fact_code: %s, stck_code: %d, start_time :%s, end_time: %s)", 
					((int) (queryFinishTime - queryStartTime)/1000L), 
					resultList.length,
					fact_code, stck_code, start_time_str, end_time_str));
			
			return tmsDataSet;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error( e.toString() );
			return tmsDataSet;
		}

	} // end of method
	
	/**
	 * @param start_time_str: time format (yyyy-MM-dd HH:mm:ss)
	 * @param end_time_str: time format (yyyy-MM-dd HH:mm:ss)
	 */
	public static Tms30minDataset queryTms30minRecord(
			String fact_code, int stck_code, 
			long start_time, long end_time) {
		
		// Create TmsDataSet
		Tms30minDataset tmsDataSet = new Tms30minDataset();
		tmsDataSet.init(start_time, end_time);
		
		// Query TmsData
		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetTms30minRecordService client = (GetTms30minRecordService)context.getBean("clientTms30minRecord");

			// set timeout : 20-min
			Client proxy = ClientProxy.getClient(client);
			HTTPConduit conduit = (HTTPConduit) proxy.getConduit();
			HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
			httpClientPolicy.setConnectionTimeout(1800000);
			httpClientPolicy.setReceiveTimeout(1800000);
			conduit.setClient(httpClientPolicy);
			
			// set max-child limits
			proxy.getEndpoint().put("org.apache.cxf.stax.maxChildElements", "3000000");
			
			long queryStartTime = System.currentTimeMillis();
			logger.debug(String.format(
					"QueryStart (fact_code: %s, stck_code: %d, start_time: %s, end_time: %s)", 
					fact_code, stck_code, DateUtil.getTmsDateStr(start_time), DateUtil.getTmsDateStr(end_time)));
			
			// Get all data
			long targetTime = 0L;
			Tms30minRecord[] resultList = client.getTms30minRecordAll(
					fact_code, 
					""+stck_code, 
					DateUtil.getDateStr(start_time), 
					DateUtil.getDateStr(end_time));
			for (Tms30minRecord result : resultList) {
				
				try {
					targetTime = DateUtil.getTmsDate(result.half_time);
					tmsDataSet.getTmsDataRecord(targetTime).setValue(
							Integer.parseInt(result.item_code), 
							Double.parseDouble(result.half_valu),
							result.half_over,
							result.half_stat,
							result.half_dlst,
							result.stat_code,
							Double.parseDouble(result.stat_valu));
				} catch (Exception ee) {}
			}
			
			long queryFinishTime = System.currentTimeMillis();
			logger.debug(String.format(
					"Query Finished. (query_time: %d seconds, query_records: %d\n fact_code: %s, stck_code: %d, start_time :%s, end_time: %s)", 
					((int) (queryFinishTime - queryStartTime)/1000L), 
					resultList.length,
					fact_code, stck_code, 
					DateUtil.getTmsDateStr(start_time), DateUtil.getTmsDateStr(end_time)));
			
			return tmsDataSet;
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error( e.toString() );
			return tmsDataSet;
		}

	} // end of method
	

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
