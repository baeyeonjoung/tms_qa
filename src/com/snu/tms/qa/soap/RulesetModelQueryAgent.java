/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import java.util.Arrays;
import java.util.Hashtable;

import kr.or.webservice.server.model.PatternInfo;
import kr.or.webservice.server.model.PatternItemInfo;
import kr.or.webservice.server.model.PatternPcaInfo;
import kr.or.webservice.server.model.RulesetCorrectionInfo;
import kr.or.webservice.server.model.RulesetInfo;
import kr.or.webservice.server.model.RulesetWeightingInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.PatternDataHistogram;
import com.snu.tms.qa.model.ruleset.PcaResultModel;
import com.snu.tms.qa.model.ruleset.RulesetCorr;
import com.snu.tms.qa.model.ruleset.RulesetCorrectionModel;
import com.snu.tms.qa.model.ruleset.RulesetModel;
import com.snu.tms.qa.model.ruleset.RulesetPatternModel;
import com.snu.tms.qa.model.ruleset.RulesetPcaSpe;
import com.snu.tms.qa.model.ruleset.RulesetPcaTss;
import com.snu.tms.qa.model.ruleset.RulesetPd;
import com.snu.tms.qa.model.ruleset.RulesetSpc;
import com.snu.tms.qa.util.DateUtil;

/**
 * Query Agent
 * 
 */
public class RulesetModelQueryAgent {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(RulesetModelQueryAgent.class);

	//==========================================================================
	// Static Method
	//==========================================================================
	
	/**
	 * query ruleset model
	 */
	public static RulesetModel[] queryRulesetModel(String factCode, int stckCode) {
		
		// create RulesetModel Hashtable
		Hashtable<Integer, RulesetModel> rulesetModelHash = new Hashtable<Integer, RulesetModel>();
		
		// query all ruleset_mgmt
		RulesetModel[] rulesetModels = querySelect_ruleset_mgmt(factCode, stckCode);
		for (RulesetModel model : rulesetModels) {
			rulesetModelHash.put(model.getRulesetId(), model);
		}
		
		// query all 
		querySelect_ruleset_pattern_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_pattern_item_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_pattern_pca_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_pattern_tss_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_pattern_spe_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_pattern_corr_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_pattern_spc_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_pattern_pd_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_correction_mgmt(factCode, stckCode, rulesetModelHash);
		querySelect_ruleset_weighting_mgmt(factCode, stckCode, rulesetModelHash);

		// return
		Arrays.sort(rulesetModels);
		return rulesetModels;
		
	} // end of method
	
	/**
	 * query for ruleset_mgmt
	 */
	private static RulesetModel[] querySelect_ruleset_mgmt(String factCode, int stckCode) {
		
		// query from database
		RulesetInfo[] records = QueryRulesetInfo.queryAllRulesetInfo(factCode, stckCode);
		
		// convert from database record type to system model type
		RulesetModel[] models = new RulesetModel[records.length];
		for (int i=0 ; i<records.length ; i++) {
			
			try {
				// get database record
				RulesetInfo record = records[i];
				
				// set system model
				models[i] = new RulesetModel();
				models[i].setRulesetId(Integer.parseInt(record.ruleset_id));
				models[i].setFactCode(factCode);
				models[i].setStckCode(stckCode);
				models[i].setRulesetState(Integer.parseInt(record.ruleset_state));
				models[i].setRulesetName(record.ruleset_name);
				models[i].setRulesetDescr(record.ruleset_descr);
				models[i].setRulesetStartTime(DateUtil.getDate(record.start_time));
				models[i].setRulesetEndTime(DateUtil.getDate(record.end_time));
				models[i].setUserId(record.user_id);
				models[i].setCreateTime(record.create_time);
			} catch (Exception e) {
				logger.error(e);
			}
			
		} // end of for-loop
		
		// return
		return models;
		
	} // end of method
	
	/**
	 * query pattern_mgmt
	 */
	private static void querySelect_ruleset_pattern_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		PatternInfo[] records = QueryPatternInfo.queryAllPatternInfo(factCode, stckCode);
				
		// convert from database record type to system model type
		for (PatternInfo record : records) {
			
			try {
				// set parameters
				int      rulesetId        = Integer.parseInt(record.ruleset_id);
				int      patternId        = Integer.parseInt(record.pattern_id);
				String   patternName      = record.pattern_name;
				boolean  isEnabled        = (record.pattern_enable.equals("1") ? true : false);
				boolean  isRulesetCreated = (record.ruleset_enable.equals("1") ? true : false);
				boolean  isOperateStop    = (record.operate_stop_state.equals("1") ? true : false);
				int      itemCount        = Integer.parseInt(record.item_count);
				String   items            = record.items;
				
				// get ruleset model
				RulesetModel rulesetModel = rulesetModelHash.get(rulesetId);
				if (rulesetModel == null) {
					continue;
				}
				
				// set model
				RulesetPatternModel model = new RulesetPatternModel(
						patternId,
						patternName,
						isEnabled,
						isRulesetCreated,
						isOperateStop, 
						itemCount,
						items,
						rulesetModel.getRulesetStartTime(),
						rulesetModel.getRulesetEndTime());
				
				// add model
				rulesetModel.addRulesetPatternModel(model);
				
			} catch (Exception e) {
				logger.error(e);
			}
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query pattern_item_mgmt
	 */
	private static void querySelect_ruleset_pattern_item_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		PatternItemInfo[] records = QueryPatternItemInfo.queryAllPatternItemInfo(factCode, stckCode);
				
		// convert from database record type to system model type
		for (PatternItemInfo record : records) {
			
			try {
				// set parameters
				int      rulesetId     = Integer.parseInt(record.ruleset_id);
				int      patternId     = Integer.parseInt(record.pattern_id);
				int      item_code     = Integer.parseInt(record.item_code);
				double   min_range     = Double.parseDouble(record.min_range);
				double   max_range     = Double.parseDouble(record.max_range);
				int      bin_count     = Integer.parseInt(record.bin_count);
				int      bin_interval  = Integer.parseInt(record.bin_interval);
				String   density       = record.density;
				
				// get ruleset model
				RulesetModel rulesetModel = rulesetModelHash.get(rulesetId);
				if (rulesetModel == null) {
					continue;
				}
				
				// get ruleset histogram
				RulesetPatternModel  rulesetPatternModel = rulesetModel.getRulesetPatternModel(patternId);
				if (rulesetPatternModel == null) {
					continue;
				}
				
				// add model
				PatternDataHistogram patternDataHistogram = 
						rulesetPatternModel.getPatternDataHistogram(item_code);
				if (patternDataHistogram == null) {
					continue;
				}
				patternDataHistogram.setBinNo(bin_count);
				patternDataHistogram.setInterval(bin_interval);
				patternDataHistogram.setMaxRange(max_range);
				patternDataHistogram.setMinRange(min_range);
				patternDataHistogram.setDensities(density);
				
			} catch (Exception e) {
				logger.error(e);
			}
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query pattern_item_mgmt
	 */
	private static void querySelect_ruleset_pattern_pca_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		PatternPcaInfo[] records = QueryPatternPcaInfo.queryAllPatternPcaInfo(factCode, stckCode);
				
		// convert from database record type to system model type
		for (PatternPcaInfo record : records) {
			
			try {
				// set parameters
				int      rulesetId     = Integer.parseInt(record.ruleset_id);
				int      patternId     = Integer.parseInt(record.pattern_id);
				String   items         = record.items;
				int      pc_count      = Integer.parseInt(record.pc_count);
				String   eigen_vector  = record.eigen_vector;
				String   eigen_value   = record.eigen_value;
				String   neigen_value  = record.neigen_value;
				
				// get ruleset model
				RulesetModel rulesetModel = rulesetModelHash.get(rulesetId);
				if (rulesetModel == null) {
					continue;
				}
				
				// get ruleset histogram
				RulesetPatternModel  rulesetPatternModel = rulesetModel.getRulesetPatternModel(patternId);
				if (rulesetPatternModel == null) {
					continue;
				}
				
				// add model
				PcaResultModel pcaResultModel = new PcaResultModel();
				pcaResultModel.setItems(items);
				pcaResultModel.setPcCount(pc_count);
				pcaResultModel.setEigenVector(eigen_vector);
				pcaResultModel.setEigenValues(eigen_value);
				pcaResultModel.setNeigenValues(neigen_value);
				
				rulesetPatternModel.setPcaResultModel(pcaResultModel);
				
			} catch (Exception e) {
				logger.error(e);
			}
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query ruleset_tss_mgmt
	 */
	private static void querySelect_ruleset_pattern_tss_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		kr.or.webservice.server.model.PatternRulesetTssRecord[] records = QueryPatternRulesetTss.queryAllRuleset(factCode, stckCode);
				
		// convert from database record type to system model type
		for (kr.or.webservice.server.model.PatternRulesetTssRecord record : records) {
			
			try {
				// set parameters
				int      ruleset_id      = Integer.parseInt(record.ruleset_id);
				int      pattern_id      = Integer.parseInt(record.pattern_id);
				int      ruleset_enable  = Integer.parseInt(record.ruleset_enable);
				double   threshold_lower = Double.parseDouble(record.threshold_lower);
				double   threshold_upper = Double.parseDouble(record.threshold_upper);
				String   items           = record.items;
				int      pc_count        = Integer.parseInt(record.pc_count);
				String   eigen_vector    = record.eigen_vector;
				String   eigen_value     = record.eigen_value;
				String   neigen_value    = record.neigen_value;
				String   cov_matrix      = record.cov_matrix;
				String   imv_cov_matrix  = record.imv_cov_matrix;
				String   mean_value      = record.mean_value;
				String   sigma_value     = record.sigma_value;
				double   confi_level     = Double.parseDouble(record.confi_level);
				
				// get ruleset model
				RulesetModel rulesetModel = rulesetModelHash.get(ruleset_id);
				if (rulesetModel == null) {
					continue;
				}
				
				// get ruleset histogram
				RulesetPatternModel  rulesetPatternModel = rulesetModel.getRulesetPatternModel(pattern_id);
				if (rulesetPatternModel == null) {
					continue;
				}
				
				// add model
				RulesetPcaTss model = new RulesetPcaTss();
				model.setRulesetId(ruleset_id);
				model.setPatternId(pattern_id);
				model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
				model.setThresholds(new double[] {threshold_upper, threshold_lower});
				model.setItems(items);
				model.setPcCount(pc_count);
				model.setEigenVector(eigen_vector);
				model.setEigenValues(eigen_value);
				model.setNeigenValues(neigen_value);
				model.setCovMatrix(cov_matrix);
				model.setImvCovMatrix(imv_cov_matrix);
				model.setMean(mean_value);
				model.setSigma(sigma_value);
				model.setConfidenceLevel(confi_level);
				
				rulesetPatternModel.addDetectionRuleset(model);
				
			} catch (Exception e) {
				logger.error(e);
			}
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query ruleset_tss_mgmt
	 */
	private static void querySelect_ruleset_pattern_spe_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		kr.or.webservice.server.model.PatternRulesetSpeRecord[] records = QueryPatternRulesetSpe.queryAllRuleset(factCode, stckCode);
				
		// convert from database record type to system model type
		for (kr.or.webservice.server.model.PatternRulesetSpeRecord record : records) {
			
			try {
				// set parameters
				int      ruleset_id      = Integer.parseInt(record.ruleset_id);
				int      pattern_id      = Integer.parseInt(record.pattern_id);
				int      ruleset_enable  = Integer.parseInt(record.ruleset_enable);
				double   threshold_lower = Double.parseDouble(record.threshold_lower);
				double   threshold_upper = Double.parseDouble(record.threshold_upper);
				String   items           = record.items;
				int      pc_count        = Integer.parseInt(record.pc_count);
				String   eigen_vector    = record.eigen_vector;
				String   eigen_value     = record.eigen_value;
				String   neigen_value    = record.neigen_value;
				String   mean_value      = record.mean_value;
				String   sigma_value     = record.sigma_value;
				double   confi_level     = Double.parseDouble(record.confi_level);
				
				// get ruleset model
				RulesetModel rulesetModel = rulesetModelHash.get(ruleset_id);
				if (rulesetModel == null) {
					continue;
				}
				
				// get ruleset histogram
				RulesetPatternModel  rulesetPatternModel = rulesetModel.getRulesetPatternModel(pattern_id);
				if (rulesetPatternModel == null) {
					continue;
				}
				
				// add model
				RulesetPcaSpe model = new RulesetPcaSpe();
				model.setRulesetId(ruleset_id);
				model.setPatternId(pattern_id);
				model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
				model.setThresholds(new double[] {threshold_upper, threshold_lower});
				model.setItems(items);
				model.setPcCount(pc_count);
				model.setEigenVector(eigen_vector);
				model.setEigenValues(eigen_value);
				model.setNeigenValues(neigen_value);
				model.setMean(mean_value);
				model.setSigma(sigma_value);
				model.setConfidenceLevel(confi_level);
				
				rulesetPatternModel.addDetectionRuleset(model);
				
			} catch (Exception e) {
				logger.error(e);
			}
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query ruleset_tss_mgmt
	 */
	private static void querySelect_ruleset_pattern_corr_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		kr.or.webservice.server.model.PatternRulesetCorrRecord[] records = QueryPatternRulesetCorr.queryAllRuleset(factCode, stckCode);
				
		// convert from database record type to system model type
		for (kr.or.webservice.server.model.PatternRulesetCorrRecord record : records) {
			
			try {
				// set parameters
				int      ruleset_id      = Integer.parseInt(record.ruleset_id);
				int      pattern_id      = Integer.parseInt(record.pattern_id);
				int      ruleset_enable  = Integer.parseInt(record.ruleset_enable);
				double   threshold_lower = Double.parseDouble(record.threshold_lower);
				double   threshold_upper = Double.parseDouble(record.threshold_upper);
				String   items           = record.items;
				double   rsquare         = Double.parseDouble(record.rsquare);
				String   cov_matrix      = record.cov_matrix;
				String   imv_cov_matrix  = record.imv_cov_matrix;
				String   mean_value      = record.mean_value;
				String   sigma_value     = record.sigma_value;
				double   confi_level     = Double.parseDouble(record.confi_level);
				
				// get ruleset model
				RulesetModel rulesetModel = rulesetModelHash.get(ruleset_id);
				if (rulesetModel == null) {
					continue;
				}
				
				// get ruleset histogram
				RulesetPatternModel  rulesetPatternModel = rulesetModel.getRulesetPatternModel(pattern_id);
				if (rulesetPatternModel == null) {
					continue;
				}
				
				// add model
				RulesetCorr model = new RulesetCorr();
				model.setRulesetId(ruleset_id);
				model.setPatternId(pattern_id);
				model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
				model.setThresholds(new double[] {threshold_upper, threshold_lower});
				model.setItems(items);
				model.setrSquare(rsquare);
				model.setCovMatrix(cov_matrix);
				model.setImvCovMatrix(imv_cov_matrix);
				model.setMean(mean_value);
				model.setSigma(sigma_value);
				model.setConfidenceLevel(confi_level);
				
				rulesetPatternModel.addDetectionRuleset(model);
				
			} catch (Exception e) {
				logger.error(e);
			}
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query ruleset_tss_mgmt
	 */
	private static void querySelect_ruleset_pattern_spc_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		kr.or.webservice.server.model.PatternRulesetSpcRecord[] records = QueryPatternRulesetSpc.queryAllRuleset(factCode, stckCode);
				
		// convert from database record type to system model type
		for (kr.or.webservice.server.model.PatternRulesetSpcRecord record : records) {
			
			try {
				// set parameters
				int      ruleset_id      = Integer.parseInt(record.ruleset_id);
				int      pattern_id      = Integer.parseInt(record.pattern_id);
				int      ruleset_enable  = Integer.parseInt(record.ruleset_enable);
				double   threshold_lower = Double.parseDouble(record.threshold_lower);
				double   threshold_upper = Double.parseDouble(record.threshold_upper);
				int      item            = Integer.parseInt(record.item);
				double   mean_value      = Double.parseDouble(record.mean_value);
				double   sigma_value     = Double.parseDouble(record.sigma_value);
				double   confi_level     = Double.parseDouble(record.confi_level);
				
				// get ruleset model
				RulesetModel rulesetModel = rulesetModelHash.get(ruleset_id);
				if (rulesetModel == null) {
					continue;
				}
				
				// get ruleset histogram
				RulesetPatternModel  rulesetPatternModel = rulesetModel.getRulesetPatternModel(pattern_id);
				if (rulesetPatternModel == null) {
					continue;
				}
				
				// add model
				RulesetSpc model = new RulesetSpc();
				model.setRulesetId(ruleset_id);
				model.setPatternId(pattern_id);
				model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
				model.setThresholds(new double[] {threshold_upper, threshold_lower});
				model.setItem(item);
				model.setMean(mean_value);
				model.setSigma(sigma_value);
				model.setConfidenceLevel(confi_level);
				
				rulesetPatternModel.addDetectionRuleset(model);
				
			} catch (Exception e) {
				logger.error(e);
			}
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query ruleset_pd_mgmt
	 */
	private static void querySelect_ruleset_pattern_pd_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		kr.or.webservice.server.model.PatternRulesetPdRecord[] records = QueryPatternRulesetPd.queryAllRuleset(factCode, stckCode);
				
		// convert from database record type to system model type
		for (kr.or.webservice.server.model.PatternRulesetPdRecord record : records) {
			
			try {
				// set parameters
				int      ruleset_id      = Integer.parseInt(record.ruleset_id);
				int      pattern_id      = Integer.parseInt(record.pattern_id);
				int      ruleset_enable  = Integer.parseInt(record.ruleset_enable);
				double   threshold_lower = Double.parseDouble(record.threshold_lower);
				double   threshold_upper = Double.parseDouble(record.threshold_upper);
				int      item            = Integer.parseInt(record.item);
				double   mean_value      = Double.parseDouble(record.mean_value);
				double   sigma_value     = Double.parseDouble(record.sigma_value);
				double   confi_level     = Double.parseDouble(record.confi_level);
				
				// get ruleset model
				RulesetModel rulesetModel = rulesetModelHash.get(ruleset_id);
				if (rulesetModel == null) {
					continue;
				}
				
				// get ruleset histogram
				RulesetPatternModel  rulesetPatternModel = rulesetModel.getRulesetPatternModel(pattern_id);
				if (rulesetPatternModel == null) {
					continue;
				}
				
				// add model
				RulesetPd model = new RulesetPd();
				model.setRulesetId(ruleset_id);
				model.setPatternId(pattern_id);
				model.setRulesetEnabled(ruleset_enable == 1 ? Boolean.TRUE : Boolean.FALSE);
				model.setThresholds(new double[] {threshold_upper, threshold_lower});
				model.setItem(item);
				model.setMean(mean_value);
				model.setSigma(sigma_value);
				model.setConfidenceLevel(confi_level);
				
				rulesetPatternModel.addDetectionRuleset(model);
				
			} catch (Exception e) {
				logger.error(e);
			}
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query ruleset_correction_mgmt
	 */
	private static void querySelect_ruleset_correction_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		RulesetCorrectionInfo[] records = 
				QueryRulesetCorrectionInfo.queryAllRulesetCorrectionInfo(factCode, stckCode);
				
		// convert from database record type to system model type
		for (RulesetCorrectionInfo record : records) {
			
			try {
				// set parameters
				int       ruleset_id  = Integer.parseInt(record.ruleset_id);
				int       item_code   = Integer.parseInt(record.item_code);
				double    std_oxgy    = Double.parseDouble(record.std_oxgy);
				double    std_mois    = Double.parseDouble(record.std_mois);
				int       oxyg_corr   = Integer.parseInt(record.oxyg_corr);
				int       temp_corr   = Integer.parseInt(record.temp_corr);
				int       mois_corr   = Integer.parseInt(record.mois_corr);
				
				// get ruleset model
				RulesetModel rulesetModel = rulesetModelHash.get(ruleset_id);
				if (rulesetModel == null) {
					continue;
				}
				
				// add model
				rulesetModel.addRulesetCorrectionModel(new RulesetCorrectionModel(
						item_code, std_oxgy, std_mois,
						oxyg_corr, temp_corr, mois_corr));
				
			} catch (Exception e) {
				logger.error(e);
			}
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * query ruleset_weighting_mgmt
	 */
	private static void querySelect_ruleset_weighting_mgmt(
			String factCode, int stckCode,
			Hashtable<Integer, RulesetModel> rulesetModelHash) {
		
		// query from database
		RulesetWeightingInfo[] records = 
				QueryRulesetWeightingInfo.queryAllRulesetWeightingInfo(factCode, stckCode);
				
		// convert from database record type to system model type
		for (RulesetWeightingInfo record : records) {
			
			try {
				// set parameters
				int  ruleset_id         = Integer.parseInt(record.ruleset_id);
				int  multi_weight       = Integer.parseInt(record.multi_weight);
				int  bi_weight          = Integer.parseInt(record.bi_weight);
				int  uni_weight         = Integer.parseInt(record.uni_weight);
				int  corr_weight        = Integer.parseInt(record.corr_weight);
				int  warn_threshold     = Integer.parseInt(record.warn_threshold);
				int  minor_threshold    = Integer.parseInt(record.minor_threshold);
				int  major_threshold    = Integer.parseInt(record.major_threshold);
				int  critical_threshold = Integer.parseInt(record.critical_threshold);
	
				// get ruleset model
				RulesetModel rulesetModel = rulesetModelHash.get(ruleset_id);
				if (rulesetModel == null) {
					continue;
				}
				
				// add model
				rulesetModel.setRulesetWeightingFactors(
						multi_weight, bi_weight, uni_weight, corr_weight, 
						warn_threshold, minor_threshold, major_threshold, critical_threshold);
				
			} catch (Exception e) {
				logger.error(e);
			}
			
		} // end of for-loop
		
	} // end of method


	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
