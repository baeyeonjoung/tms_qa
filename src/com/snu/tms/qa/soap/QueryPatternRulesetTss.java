/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetPatternRulesetTssService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.snu.tms.qa.model.db.PatternRulesetTssRecord;

/**
 * Query Agent
 * 
 */
public class QueryPatternRulesetTss {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryPatternRulesetTss.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.PatternRulesetTssRecord[] queryAllRuleset(String factCode, int stckCode) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetTssService client = (GetPatternRulesetTssService)context.getBean("clientPatternRulesetTss");
			
			// Get all tableName
			kr.or.webservice.server.model.PatternRulesetTssRecord[] retList = client.getPatternRulesetTssAll(factCode, ""+stckCode);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.PatternRulesetTssRecord[0];
		}
		
	} // end of method
	
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.PatternRulesetTssRecord[] queryAllRuleset(int rulsetId) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetTssService client = (GetPatternRulesetTssService)context.getBean("clientPatternRulesetTss");
			
			// Get all tableName
			kr.or.webservice.server.model.PatternRulesetTssRecord[] retList = client.getPatternRulesetTss(""+rulsetId);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.PatternRulesetTssRecord[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertRuleset(PatternRulesetTssRecord rulesetRecord) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetTssService client = (GetPatternRulesetTssService)context.getBean("clientPatternRulesetTss");
			
			// Get all tableName
			client.setPatternRulesetTss(
					""+rulesetRecord.getRuleset_id(),
					""+rulesetRecord.getPattern_id(),
					""+rulesetRecord.getRuleset_enable(),
					""+rulesetRecord.getThreshold_lower(),
					""+rulesetRecord.getThreshold_upper(),
					""+rulesetRecord.getItems(),
					""+rulesetRecord.getPc_count(),
					rulesetRecord.getEigen_vector(),
					rulesetRecord.getEigen_value(),
					rulesetRecord.getNeigen_value(),
					rulesetRecord.getCov_matrix(),
					rulesetRecord.getImv_cov_matrix(),
					""+rulesetRecord.getMean_value(),
					""+rulesetRecord.getSigma_value(), 
					""+rulesetRecord.getConfi_level());
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}

	} // end of method
	
	/**
	 * delete all pattern related with ruleset id
	 */
	public static void deleteRuleset(int rulesetId) {
		
		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetTssService client = (GetPatternRulesetTssService)context.getBean("clientPatternRulesetTss");
			
			// Get all tableName
			client.delPatternRulesetTss(""+rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
