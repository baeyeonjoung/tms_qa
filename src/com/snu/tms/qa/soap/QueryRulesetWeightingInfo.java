/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import java.util.List;
import java.util.Vector;

import kr.or.webservice.server.interpace.GetRulesetInfoService;
import kr.or.webservice.server.interpace.GetRulesetWeightingInfoService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.snu.tms.qa.model.db.RulesetCorrectionRecord;
import com.snu.tms.qa.model.db.RulesetWeightingRecord;
import com.snu.tms.qa.model.db.StackKeyRecord;

/**
 * Query Agent
 * 
 */
public class QueryRulesetWeightingInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryRulesetWeightingInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.RulesetWeightingInfo[] queryAllRulesetWeightingInfo() {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetWeightingInfoService client = (GetRulesetWeightingInfoService)context.getBean("clientRulesetWeightingInfo");
			
			// Get all tableName
			kr.or.webservice.server.model.RulesetWeightingInfo[] retList = client.getRulesetWeightingInfoAll();
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.RulesetWeightingInfo[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.RulesetWeightingInfo[] queryAllRulesetWeightingInfo(String factCode, int stckCode) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetWeightingInfoService client = (GetRulesetWeightingInfoService)context.getBean("clientRulesetWeightingInfo");
			
			// Get all tableName
			kr.or.webservice.server.model.RulesetWeightingInfo[] retList = client.getRecordAll(factCode, ""+stckCode);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.RulesetWeightingInfo[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.RulesetWeightingInfo queryRulesetWeightingInfo(int rulesetId) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetWeightingInfoService client = (GetRulesetWeightingInfoService)context.getBean("clientRulesetWeightingInfo");
			
			// Get all tableName
			kr.or.webservice.server.model.RulesetWeightingInfo[] retList = client.getRulesetWeightingInfo(""+rulesetId);
			return retList[0];
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return null;
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertRulesetWeightingInfo(RulesetWeightingRecord rulesetRecord) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetWeightingInfoService client = (GetRulesetWeightingInfoService)context.getBean("clientRulesetWeightingInfo");
			
			// Get all tableName
			client.setRulesetWeightingInfo(
					""+rulesetRecord.getRuleset_id(),
					""+rulesetRecord.getMulti_weight(),
					""+rulesetRecord.getBi_weight(),
					""+rulesetRecord.getUni_weight(),
					""+rulesetRecord.getCorr_weight(),
					""+rulesetRecord.getWarn_threshold(),
					""+rulesetRecord.getMinor_threshold(),
					""+rulesetRecord.getMajor_threshold(),
					""+rulesetRecord.getCritical_threshold());
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}

	} // end of method

	/**
	 * delete ruleset recrod
	 */
	public static void deleteRulesetWeightingInfo(int rulesetId) {
		
		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetWeightingInfoService client = (GetRulesetWeightingInfoService)context.getBean("clientRulesetWeightingInfo");
			
			// Get all tableName
			client.delRulesetWeightingInfo(""+rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
