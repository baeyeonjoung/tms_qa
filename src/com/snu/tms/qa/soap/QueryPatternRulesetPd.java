/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetPatternRulesetPdService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.snu.tms.qa.model.db.PatternRulesetPdRecord;

/**
 * Query Agent
 * 
 */
public class QueryPatternRulesetPd {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryPatternRulesetPd.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.PatternRulesetPdRecord[] queryAllRuleset(String factCode, int stckCode) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetPdService client = (GetPatternRulesetPdService)context.getBean("clientPatternRulesetPd");
			
			// Get all tableName
			kr.or.webservice.server.model.PatternRulesetPdRecord[] retList = client.getPatternRulesetPdAll(factCode, ""+stckCode);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.PatternRulesetPdRecord[0];
		}
		
	} // end of method
	
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.PatternRulesetPdRecord[] queryAllRuleset(int rulsetId) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetPdService client = (GetPatternRulesetPdService)context.getBean("clientPatternRulesetPd");
			
			// Get all tableName
			kr.or.webservice.server.model.PatternRulesetPdRecord[] retList = client.getPatternRulesetPd(""+rulsetId);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.PatternRulesetPdRecord[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertRuleset(PatternRulesetPdRecord rulesetRecord) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetPdService client = (GetPatternRulesetPdService)context.getBean("clientPatternRulesetPd");
			
			// check validation
			if (Double.isNaN(rulesetRecord.getThreshold_lower())) {
				rulesetRecord.setThreshold_lower(0.0D);
			}
			if (Double.isNaN(rulesetRecord.getThreshold_upper())) {
				rulesetRecord.setThreshold_upper(0.0D);
			}
			if (Double.isNaN(rulesetRecord.getMean_value())) {
				rulesetRecord.setMean_value(0.0D);
			}
			if (Double.isNaN(rulesetRecord.getSigma_value())) {
				rulesetRecord.setSigma_value(0.0D);
			}
			if (Double.isNaN(rulesetRecord.getConfi_level())) {
				rulesetRecord.setConfi_level(0.0D);
			}
			
			// Get all tableName
			client.setPatternRulesetPd(
					""+rulesetRecord.getRuleset_id(),
					""+rulesetRecord.getPattern_id(),
					""+rulesetRecord.getRuleset_enable(),
					""+rulesetRecord.getThreshold_lower(),
					""+rulesetRecord.getThreshold_upper(),
					""+rulesetRecord.getItem(),
					""+rulesetRecord.getMean_value(),
					""+rulesetRecord.getSigma_value(), 
					""+rulesetRecord.getConfi_level());
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}

	} // end of method
	
	/**
	 * delete all pattern related with ruleset id
	 */
	public static void deleteRuleset(int rulesetId) {
		
		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetPdService client = (GetPatternRulesetPdService)context.getBean("clientPatternRulesetPd");
			
			// Get all tableName
			client.delPatternRulesetPd(""+rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
