/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetPatternInfoService;
import kr.or.webservice.server.model.PatternInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.snu.tms.qa.model.db.PatternRecord;

/**
 * Query Agent
 * 
 */
public class QueryPatternInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryPatternInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static PatternInfo[] queryAllPatternInfo(String factCode, int stckCode) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternInfoService client = (GetPatternInfoService)context.getBean("clientPatternInfo");
			
			// Get all data
			PatternInfo[] resultList = client.getPatternInfoAll(factCode, ""+stckCode);
			return resultList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new PatternInfo[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static PatternInfo[] queryAllPatternInfo(int rulsetId) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternInfoService client = (GetPatternInfoService)context.getBean("clientPatternInfo");
			
			// Get all data
			PatternInfo[] resultList = client.getPatternInfo("" + rulsetId);
			return resultList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new PatternInfo[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertPatternInfo(PatternRecord patternRecord) {
		
		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternInfoService client = (GetPatternInfoService)context.getBean("clientPatternInfo");
			
			// update
			client.setPatternInfo(
					""+patternRecord.getRuleset_id(),
					""+patternRecord.getPattern_id(),
					patternRecord.getPattern_name(),
					""+patternRecord.getPattern_enable(),
					""+patternRecord.getRuleset_enable(),
					""+patternRecord.getOperate_stop_state(),
					""+patternRecord.getItem_count(),
					patternRecord.getItems());
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}

	} // end of method
	
	/**
	 * delete all pattern related with ruleset id
	 */
	public static void deletePatternInfo(int rulesetId) {
		
		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternInfoService client = (GetPatternInfoService)context.getBean("clientPatternInfo");
			
			// update
			client.delPatternInfo(""+rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			e.printStackTrace();
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
