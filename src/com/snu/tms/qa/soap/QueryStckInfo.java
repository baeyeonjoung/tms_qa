/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetStckInfoService;
import kr.or.webservice.server.model.StckInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Query Agent
 * 
 */
public class QueryStckInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryStckInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	
	/**
	 * 
	 */
	public static StckInfo[]  queryAllStckInfo() {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetStckInfoService client = (GetStckInfoService)context.getBean("clientStckInfo");

			// get data
			StckInfo[] resultList = client.getStckInfoAll();
			return resultList;

		} catch (Exception e) {
			logger.error(e);
			return new StckInfo[0];
		}

	} // end of method
	
	
	/**
	 * For Tibero database
	 */
	public static StckInfo queryStckInfo(String fact_code, int stckNo) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetStckInfoService client = (GetStckInfoService)context.getBean("clientStckInfo");
			
			// get data
			StckInfo result = client.getStckInfo(fact_code, "" + stckNo);
			return result;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return null;
		}

	} // end of method
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
