/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetPatternRulesetSpeService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.snu.tms.qa.model.db.PatternRulesetSpeRecord;

/**
 * Query Agent
 * 
 */
public class QueryPatternRulesetSpe {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryPatternRulesetSpe.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.PatternRulesetSpeRecord[] queryAllRuleset(String factCode, int stckCode) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetSpeService client = (GetPatternRulesetSpeService)context.getBean("clientPatternRulesetSpe");
			
			// Get all tableName
			kr.or.webservice.server.model.PatternRulesetSpeRecord[] retList = client.getPatternRulesetSpeAll(factCode, ""+stckCode);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.PatternRulesetSpeRecord[0];
		}
		
	} // end of method
	
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.PatternRulesetSpeRecord[] queryAllRuleset(int rulsetId) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetSpeService client = (GetPatternRulesetSpeService)context.getBean("clientPatternRulesetSpe");
			
			// Get all tableName
			kr.or.webservice.server.model.PatternRulesetSpeRecord[] retList = client.getPatternRulesetSpe(""+rulsetId);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.PatternRulesetSpeRecord[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertRuleset(PatternRulesetSpeRecord rulesetRecord) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetSpeService client = (GetPatternRulesetSpeService)context.getBean("clientPatternRulesetSpe");
			
			// Get all tableName
			client.setPatternRulesetSpe(
					""+rulesetRecord.getRuleset_id(),
					""+rulesetRecord.getPattern_id(),
					""+rulesetRecord.getRuleset_enable(),
					""+rulesetRecord.getThreshold_lower(),
					""+rulesetRecord.getThreshold_upper(),
					""+rulesetRecord.getItems(),
					""+rulesetRecord.getPc_count(),
					rulesetRecord.getEigen_vector(),
					rulesetRecord.getEigen_value(),
					rulesetRecord.getNeigen_value(),
					""+rulesetRecord.getMean_value(),
					""+rulesetRecord.getSigma_value(), 
					""+rulesetRecord.getConfi_level());
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}

	} // end of method
	
	/**
	 * delete all pattern related with ruleset id
	 */
	public static void deleteRuleset(int rulesetId) {
		
		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetSpeService client = (GetPatternRulesetSpeService)context.getBean("clientPatternRulesetSpe");
			
			// Get all tableName
			client.delPatternRulesetSpe(""+rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
