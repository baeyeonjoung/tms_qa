/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * SOAP Client Manager
 *
 */
public class SoapClientManager 
{
	//==========================================================================
	// Fields
	//==========================================================================
	private static final ClassPathXmlApplicationContext context;

	static {
		try {
			context = new ClassPathXmlApplicationContext(
					new String[] {"/kr/or/webservice/server/webservice.xml"});

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor for SqlMapClientManager
	 * (Using Singleton Pattern)
	 * 
	 */
	private SoapClientManager(){}

	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Get SqlMapClient
	 * (Singleton Pattern)
	 */
	public static ClassPathXmlApplicationContext getContext() {
		return context;  
	} // end of method

} // end of class
