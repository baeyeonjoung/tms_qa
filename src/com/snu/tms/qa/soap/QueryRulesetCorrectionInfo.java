/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetRulesetCorrectionInfoService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.snu.tms.qa.model.db.RulesetCorrectionRecord;

/**
 * Query Agent
 * 
 */
public class QueryRulesetCorrectionInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryRulesetCorrectionInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.RulesetCorrectionInfo[] queryAllRulesetCorrectionInfo() {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetCorrectionInfoService client = (GetRulesetCorrectionInfoService)context.getBean("clientRulesetCorrectionInfo");
			
			// Get all tableName
			kr.or.webservice.server.model.RulesetCorrectionInfo[] retList = client.getRulesetCorrectionInfoAll();
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.RulesetCorrectionInfo[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.RulesetCorrectionInfo[] queryAllRulesetCorrectionInfo(String factCode, int stckCode) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetCorrectionInfoService client = (GetRulesetCorrectionInfoService)context.getBean("clientRulesetCorrectionInfo");
			
			// Get all tableName
			kr.or.webservice.server.model.RulesetCorrectionInfo[] retList = client.getRecordAll(factCode, ""+stckCode);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.RulesetCorrectionInfo[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.RulesetCorrectionInfo[] queryRulesetCorrectionInfo(int rulesetId) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetCorrectionInfoService client = (GetRulesetCorrectionInfoService)context.getBean("clientRulesetCorrectionInfo");
			
			// Get all tableName
			kr.or.webservice.server.model.RulesetCorrectionInfo[] retList = client.getRulesetCorrectionInfo(""+rulesetId);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.RulesetCorrectionInfo[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertRulesetCorrectionInfo(RulesetCorrectionRecord rulesetCorrectionRecord) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetCorrectionInfoService client = (GetRulesetCorrectionInfoService)context.getBean("clientRulesetCorrectionInfo");
			
			// Get all tableName
			client.setRulesetCorrectionInfo(
					""+rulesetCorrectionRecord.getRuleset_id(),
					""+rulesetCorrectionRecord.getItem_code(),
					""+rulesetCorrectionRecord.getStd_oxgy(),
					""+rulesetCorrectionRecord.getStd_mois(),
					""+rulesetCorrectionRecord.getOxyg_corr(),
					""+rulesetCorrectionRecord.getTemp_corr(),
					""+rulesetCorrectionRecord.getMois_corr());
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}

	} // end of method

	/**
	 * delete ruleset recrod
	 */
	public static void deleteRulesetCorrectionInfo(int rulesetId) {
		
		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetCorrectionInfoService client = (GetRulesetCorrectionInfoService)context.getBean("clientRulesetCorrectionInfo");
			
			// Get all tableName
			client.delRulesetCorrectionInfo(""+rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
