/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetFactInfoService;
import kr.or.webservice.server.interpace.GetStckInfoService;
import kr.or.webservice.server.model.FactInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Main class for SOAP test
 * 
 */
public class SoapTest {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(SoapTest.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Setup logging and other required library
		try {
			org.apache.log4j.PropertyConfigurator.configure("./conf/log.properties");
			logger.info("TMS Quality Assessment process is stating .............");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("./conf/log.properties file can't found.\n"
					+ "Process halted and system will be exit.");
			System.exit(1);
		}
		
		// Test SOAP
		testSoap();
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * SOAP Test
	 */
	public static void testSoap() {
		
		// create soap context
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				new String[] {"/kr/or/webservice/server/webservice.xml"});
		
		GetFactInfoService client = (GetFactInfoService)context.getBean("clientFactInfo");
		FactInfo result = client.getFactInfo("150061");
		
		GetStckInfoService client1 = (GetStckInfoService)context.getBean("clientStckInfo");
		
	} // end of method

} // end of class
