/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetCorrectionInfoService;
import kr.or.webservice.server.model.CorrectionInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Query Agent
 * 
 */
public class QueryCorrectionInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryCorrectionInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	
	/**
	 * @param args
	 */
	public static CorrectionInfo[] queryAllCorrectionInfo() {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetCorrectionInfoService client = (GetCorrectionInfoService)context.getBean("clientCorrectionInfo");

			CorrectionInfo[] resultList = client.getCorrectionInfoAll();
			return resultList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new CorrectionInfo[0];
		}

	} // end of method
	
	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
