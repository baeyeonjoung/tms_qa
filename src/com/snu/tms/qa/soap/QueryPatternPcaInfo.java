/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetPatternPcaInfoService;
import kr.or.webservice.server.model.PatternPcaInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.snu.tms.qa.model.db.PatternPcaRecord;

/**
 * Query Agent
 * 
 */
public class QueryPatternPcaInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryPatternPcaInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static PatternPcaInfo[] queryAllPatternPcaInfo(String factCode, int stckCode) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternPcaInfoService client = (GetPatternPcaInfoService)context.getBean("clientPatternPcaInfo");
			
			// Get all tableName
			PatternPcaInfo[] retList = client.getPatternPcaInfoAll(factCode, ""+stckCode);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new PatternPcaInfo[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static PatternPcaInfo[] queryAllPatternPcaInfo(int rulsetId) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternPcaInfoService client = (GetPatternPcaInfoService)context.getBean("clientPatternPcaInfo");
			
			// Get all tableName
			PatternPcaInfo[] retList = client.getPatternPcaInfo(""+rulsetId);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new PatternPcaInfo[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertPatternPcaInfo(PatternPcaRecord patternRecord) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternPcaInfoService client = (GetPatternPcaInfoService)context.getBean("clientPatternPcaInfo");
			
			// Get all tableName
			client.setPatternPcaInfo(
					""+patternRecord.getRuleset_id(),
					""+patternRecord.getPattern_id(),
					patternRecord.getItems(),
					""+patternRecord.getPc_count(),
					patternRecord.getEigen_vector(),
					patternRecord.getEigen_value(),
					patternRecord.getNeigen_value());
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}

	} // end of method
	
	/**
	 * delete all pattern related with ruleset id
	 */
	public static void deletePatternPcaInfo(int rulesetId) {
		
		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternPcaInfoService client = (GetPatternPcaInfoService)context.getBean("clientPatternPcaInfo");
			
			// Get all tableName
			client.delPatternPcaInfo(""+rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
