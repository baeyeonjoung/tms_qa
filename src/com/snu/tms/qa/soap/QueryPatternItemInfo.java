/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetPatternItemInfoService;
import kr.or.webservice.server.model.PatternItemInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.snu.tms.qa.model.db.PatternItemRecord;

/**
 * Query Agent
 * 
 */
public class QueryPatternItemInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryPatternItemInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static PatternItemInfo[] queryAllPatternItemInfo(String factCode, int stckCode) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternItemInfoService client = (GetPatternItemInfoService)context.getBean("clientPatternItemInfo");
			
			// Get all tableName
			PatternItemInfo[] retList = client.getPatternItemInfoAll(factCode, ""+stckCode);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new PatternItemInfo[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static PatternItemInfo[] queryAllPatternInfo(int rulsetId) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternItemInfoService client = (GetPatternItemInfoService)context.getBean("clientPatternItemInfo");
			
			// Get all tableName
			PatternItemInfo[] retList = client.getPatternItemInfo(""+rulsetId);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new PatternItemInfo[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertPatternItemInfo(PatternItemRecord patternRecord) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternItemInfoService client = (GetPatternItemInfoService)context.getBean("clientPatternItemInfo");
			
			// Get all tableName
			client.setPatternItemInfo(
					""+patternRecord.getRuleset_id(),
					""+patternRecord.getPattern_id(),
					""+patternRecord.getItem_code(),
					patternRecord.getItem_name(),
					""+patternRecord.getMin_range(),
					""+patternRecord.getMax_range(),
					""+patternRecord.getBin_count(),
					""+patternRecord.getBin_interval(),
					patternRecord.getDensity());
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}

	} // end of method
	
	/**
	 * delete all pattern related with ruleset id
	 */
	public static void deletePatternItemInfo(int rulesetId) {
		
		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternItemInfoService client = (GetPatternItemInfoService)context.getBean("clientPatternItemInfo");
			
			// Get all tableName
			client.delPatternItemInfo(""+rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
