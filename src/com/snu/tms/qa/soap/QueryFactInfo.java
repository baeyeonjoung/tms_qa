/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetFactInfoService;
import kr.or.webservice.server.model.FactInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Query Agent
 * 
 */
public class QueryFactInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryFactInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static FactInfo[] queryAllFactInfo() {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetFactInfoService client = (GetFactInfoService)context.getBean("clientFactInfo");
			
			// get data
			FactInfo[] resultList = client.getFactInfoAll();
			return resultList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new FactInfo[0];
		}

	} // end of method
	
	
	/**
	 * For Tibero database
	 */
	public static FactInfo queryFactInfo(String fact_code) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetFactInfoService client = (GetFactInfoService)context.getBean("clientFactInfo");
			
			// get data
			FactInfo result = client.getFactInfo(fact_code);
			return result;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return null;
		}

	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
