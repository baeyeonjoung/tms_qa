/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetRulesetInfoService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.snu.tms.qa.model.db.RulesetRecord;

/**
 * Query Agent
 * 
 */
public class QueryRulesetInfo {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryRulesetInfo.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.RulesetInfo[] queryAllRulesetInfo() {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetInfoService client = (GetRulesetInfoService)context.getBean("clientRulesetInfo");
			
			// Get all tableName
			kr.or.webservice.server.model.RulesetInfo[] retList = client.getRulesetInfoAll();
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.RulesetInfo[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.RulesetInfo[] queryAllRulesetInfo(String factCode, int stckCode) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetInfoService client = (GetRulesetInfoService)context.getBean("clientRulesetInfo");
			
			// Get all tableName
			kr.or.webservice.server.model.RulesetInfo[] retList = client.getRulesetInfo(factCode, ""+stckCode);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.RulesetInfo[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.RulesetInfo queryDefaultRulesetInfo(String factCode, int stckCode) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetInfoService client = (GetRulesetInfoService)context.getBean("clientRulesetInfo");
			
			// Get all tableName
			kr.or.webservice.server.model.RulesetInfo[] retList = client.getDefaultRulesetInfo(factCode, ""+stckCode);
			return retList[0];
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return null;
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static int insertRulesetInfo(RulesetRecord rulesetRecord) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetInfoService client = (GetRulesetInfoService)context.getBean("clientRulesetInfo");
			
			// Get all tableName
			client.setRulesetInfo(
					""+rulesetRecord.getRuleset_id(),
					rulesetRecord.getFact_code(),
					""+rulesetRecord.getStck_code(),
					""+rulesetRecord.getRuleset_state(),
					rulesetRecord.getRuleset_name(),
					rulesetRecord.getRuleset_descr(),
					rulesetRecord.getUser_id(),
					rulesetRecord.getCreate_time(),
					rulesetRecord.getStart_time(),
					rulesetRecord.getEnd_time());
			
			String lastRulesetId = client.getLastRulesetId(
					rulesetRecord.getFact_code(), ""+rulesetRecord.getStck_code());
			return Integer.parseInt(lastRulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return -1;
		}

	} // end of method
	
	
	/**
	 * @param args
	 */
	public static void updateRulesetInfo(RulesetRecord rulesetRecord) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetInfoService client = (GetRulesetInfoService)context.getBean("clientRulesetInfo");
			
			// Get all tableName
			client.editRulesetInfo(
					""+rulesetRecord.getRuleset_id(),
					""+rulesetRecord.getRuleset_state(),
					rulesetRecord.getRuleset_name(),
					rulesetRecord.getRuleset_descr());
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}

	} // end of method
	
	/**
	 * delete ruleset recrod
	 */
	public static void deleteRulesetInfo(int rulesetId) {
		
		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetRulesetInfoService client = (GetRulesetInfoService)context.getBean("clientRulesetInfo");
			
			// Get all tableName
			client.delRulesetInfo(""+rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
