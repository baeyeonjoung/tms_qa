/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetPatternRulesetSpcService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.snu.tms.qa.model.db.PatternRulesetSpcRecord;

/**
 * Query Agent
 * 
 */
public class QueryPatternRulesetSpc {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryPatternRulesetSpc.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.PatternRulesetSpcRecord[] queryAllRuleset(String factCode, int stckCode) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetSpcService client = (GetPatternRulesetSpcService)context.getBean("clientPatternRulesetSpc");
			
			// Get all tableName
			kr.or.webservice.server.model.PatternRulesetSpcRecord[] retList = client.getPatternRulesetSpcAll(factCode, ""+stckCode);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.PatternRulesetSpcRecord[0];
		}
		
	} // end of method
	
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.PatternRulesetSpcRecord[] queryAllRuleset(int rulsetId) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetSpcService client = (GetPatternRulesetSpcService)context.getBean("clientPatternRulesetSpc");
			
			// Get all tableName
			kr.or.webservice.server.model.PatternRulesetSpcRecord[] retList = client.getPatternRulesetSpc(""+rulsetId);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.PatternRulesetSpcRecord[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertRuleset(PatternRulesetSpcRecord rulesetRecord) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetSpcService client = (GetPatternRulesetSpcService)context.getBean("clientPatternRulesetSpc");
			
			// Get all tableName
			client.setPatternRulesetSpc(
					""+rulesetRecord.getRuleset_id(),
					""+rulesetRecord.getPattern_id(),
					""+rulesetRecord.getRuleset_enable(),
					""+rulesetRecord.getThreshold_lower(),
					""+rulesetRecord.getThreshold_upper(),
					""+rulesetRecord.getItem(),
					""+rulesetRecord.getMean_value(),
					""+rulesetRecord.getSigma_value(), 
					""+rulesetRecord.getConfi_level());
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}

	} // end of method
	
	/**
	 * delete all pattern related with ruleset id
	 */
	public static void deleteRuleset(int rulesetId) {
		
		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetSpcService client = (GetPatternRulesetSpcService)context.getBean("clientPatternRulesetSpc");
			
			// Get all tableName
			client.delPatternRulesetSpc(""+rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
