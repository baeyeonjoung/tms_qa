/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.soap;

import kr.or.webservice.server.interpace.GetPatternRulesetCorrService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.snu.tms.qa.model.db.PatternRulesetCorrRecord;

/**
 * Query Agent
 * 
 */
public class QueryPatternRulesetCorr {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryPatternRulesetCorr.class);
	

	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.PatternRulesetCorrRecord[] queryAllRuleset(String factCode, int stckCode) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetCorrService client = (GetPatternRulesetCorrService)context.getBean("clientPatternRulesetCorr");
			
			// Get all tableName
			kr.or.webservice.server.model.PatternRulesetCorrRecord[] retList = client.getPatternRulesetCorrAll(factCode, ""+stckCode);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.PatternRulesetCorrRecord[0];
		}
		
	} // end of method
	
	/**
	 * @param args
	 */
	public static kr.or.webservice.server.model.PatternRulesetCorrRecord[] queryAllRuleset(int rulsetId) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetCorrService client = (GetPatternRulesetCorrService)context.getBean("clientPatternRulesetCorr");
			
			// Get all tableName
			kr.or.webservice.server.model.PatternRulesetCorrRecord[] retList = client.getPatternRulesetCorr(""+rulsetId);
			return retList;
			
		} catch (Exception e) {
			logger.error( e.toString() );
			return new kr.or.webservice.server.model.PatternRulesetCorrRecord[0];
		}

	} // end of method
	
	/**
	 * @param args
	 */
	public static void insertRuleset(PatternRulesetCorrRecord rulesetRecord) {

		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetCorrService client = (GetPatternRulesetCorrService)context.getBean("clientPatternRulesetCorr");
			
			// Get all tableName
			client.setPatternRulesetCorr(
					""+rulesetRecord.getRuleset_id(),
					""+rulesetRecord.getPattern_id(),
					""+rulesetRecord.getRuleset_enable(),
					""+rulesetRecord.getThreshold_lower(),
					""+rulesetRecord.getThreshold_upper(),
					rulesetRecord.getItems(),
					""+rulesetRecord.getRsquare(),
					rulesetRecord.getCov_matrix(),
					rulesetRecord.getCov_matrix(),
					rulesetRecord.getMean_value(),
					rulesetRecord.getSigma_value(),
					""+rulesetRecord.getConfi_level());
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}

	} // end of method
	
	/**
	 * delete all pattern related with ruleset id
	 */
	public static void deleteRuleset(int rulesetId) {
		
		try {
			// get soap connection
			ClassPathXmlApplicationContext context = SoapClientManager.getContext();
			GetPatternRulesetCorrService client = (GetPatternRulesetCorrService)context.getBean("clientPatternRulesetCorr");
			
			// Get all tableName
			client.delPatternRulesetCorr(""+rulesetId);
			
		} catch (Exception e) {
			logger.error( e.toString() );
		}
		
	} // end of method

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
