/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.covariance;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math3.stat.correlation.Covariance;

import com.snu.tms.qa.model.TmsDataset;


/**
 * Calculate Confidence Interval for paired sample
 * 
 */
public class DoCovarianceAnalysis {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(DoCovarianceAnalysis.class);

	
	//==========================================================================
	// static Methods
	//==========================================================================
	/**
	 * 
	 */
	public  static  void  execute(
			int  itemCode_1,
			int  itemCode_2,
			TmsDataset dataSet) {
	
		// prepare data
		int n = dataSet.getTmsDataRecordCount();
		double[][] itemValues = new double[n][2];
		for (int i=0 ; i<n ; i++) {
			itemValues[i][0] = dataSet.getTmsDataRecord(i).getDataCell(itemCode_1).getOrigin_value();
			itemValues[i][1] = dataSet.getTmsDataRecord(i).getDataCell(itemCode_2).getOrigin_value();
		}
		
		// calculate covariance
		Covariance cv = new Covariance(itemValues);
		//System.out.println(cv.getCovarianceMatrix());
		//System.out.println(new PearsonsCorrelation(cv).getCorrelationMatrix());
		
		
	}

} // end of class
