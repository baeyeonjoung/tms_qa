/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis;

import java.io.BufferedWriter;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.analysis.generalstats.DoAnalysisGeneralStats;
import com.snu.tms.qa.analysis.outlier.DoAnalysisOutlier;
import com.snu.tms.qa.soap.QueryTmsRecord;
import com.snu.tms.qa.model.CorrectionItemInfo;
import com.snu.tms.qa.model.InventoryDataManager;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.TmsDataCell;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.TmsDataSingleItemRecord;
import com.snu.tms.qa.util.DateUtil;
import com.snu.tms.qa.util.FileUtils;


/**
 * Analysis agent for General statistic
 * 
 */
public class Agent4GeneralStatsAnalysis 
{
	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(Agent4GeneralStatsAnalysis.class);
	
	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * 
	 */
	public static void doAnalysis(String outputFilePath, String startTimeStr, String endTimeStr) {
		
		logger.debug("===========================================\n GeneralStatsAnalysis process started.");
		
		// Get InventoryManager
		InventoryDataManager inventoryDataManager = InventoryDataManager.getInstance();
		
		// Get all stackInfo
		StackInfo[] stacks = inventoryDataManager.getAllStackInfo();
		logger.debug(String.format("Analysis target stacks( %d ) selected.", stacks.length));
		
		// Prepare Writer and do Analysis
		BufferedWriter writer = null;
		try {
			// Prepare file
			File resultFile = new File(outputFilePath);
			if (resultFile.exists()) {
				FileUtils.copyFile(outputFilePath, (outputFilePath + "_" + DateUtil.getFileDateStr(System.currentTimeMillis()) + ".backup"));
				FileUtils.deleteFile(outputFilePath);
			}
			
			// Prepare writer
			Charset charset = Charset.forName("KSC5601");
			Path path = FileSystems.getDefault().getPath(outputFilePath);
			writer = Files.newBufferedWriter(path, charset);
			
			// write name records
			writer.write("fact_name, fact_code, stack_name, stack_code, stack_class1, stack_class2, ");
			writer.write("start_time, end_time, ");
			writer.write("item_code, item_name, correct_temp, correct_oxyg, correct_mois, ");
			writer.write("data_count, p_value, lower_limit, upper_limit, mean, standard_deviation, variance, skewness, kurtosis, min, percentile(25%), median, percentile(75%), max, ");
			writer.write("95%_confidence_lower, 95%_confidence_upper ");
			writer.write("\n");
			writer.flush();
			
			// do start analysis
			int processCount = 1;
			for (StackInfo stack : stacks) {
				logger.debug(String.format("Analysis sub-process started. (%d / %d)", processCount++, stacks.length));
				String resultStr = doAnalysis(stack, startTimeStr, endTimeStr);
				if (resultStr != null && !resultStr.equals("")) {
					writer.write(resultStr + "\n");
				}
				writer.flush();
			}
			
		} catch (Exception e) {
			logger.error("File writing error.", e);
		} finally {
			try {
				writer.close();
			} catch (Exception ee) {}
		}
		
	} // end of method
	
	/**
	 * Do analysis for selected StackInfo
	 */
	private static String doAnalysis(
			StackInfo stackInfo, 
			String startTimeStr, 
			String endTimeStr) {
		
		logger.debug(String.format("%s\nGeneral statistics analysis started. (db_name: %s, fact_code: %s, stck_code: %d)\n%s", 
				"---------------------------------------------------------------------",
				stackInfo.getFactoryInfo().getDbName(), stackInfo.getFactoryInfo().getCode(), stackInfo.getStackNumber(),
				"---------------------------------------------------------------------"));
		
		// prepare resultString
		StringBuffer strBuffer = new StringBuffer();
		
		// Query for all tms data
		String dbName = stackInfo.getFactoryInfo().getDbName();
		String factoryCode = stackInfo.getFactoryInfo().getCode();
		int stackNumber = stackInfo.getStackNumber();

		/**
		// test for only
		if (!factoryCode.equals("10021") || stackNumber != 5) {
			return null;
		}
		*/
		
		// do start query
		TmsDataset tmsDataSet = QueryTmsRecord.queryTmsRecord(factoryCode, stackNumber, startTimeStr, endTimeStr);
		tmsDataSet.getTmsDataHeader().setFactoryInfo(stackInfo.getFactoryInfo());
		tmsDataSet.getTmsDataHeader().setStackInfo(stackInfo);
		
		// Check data exist
		if (tmsDataSet.getTmsDataRecordCount() == 0) {
			logger.debug("  ==> No data found.");
			return null;
		}

		// start analysis
		String resultStr = "";
		resultStr = doAnalysis4Item(stackInfo, startTimeStr, endTimeStr, ItemCodeDefine.TSP, tmsDataSet);
		if (resultStr != null) {
			strBuffer.append(resultStr);
		}
		resultStr = doAnalysis4Item(stackInfo, startTimeStr, endTimeStr, ItemCodeDefine.SOX, tmsDataSet);
		if (resultStr != null) {
			strBuffer.append("\n" + resultStr);
		}
		resultStr = doAnalysis4Item(stackInfo, startTimeStr, endTimeStr, ItemCodeDefine.NOX, tmsDataSet);
		if (resultStr != null) {
			strBuffer.append("\n" + resultStr);
		}
		resultStr = doAnalysis4Item(stackInfo, startTimeStr, endTimeStr, ItemCodeDefine.HCL, tmsDataSet);
		if (resultStr != null) {
			strBuffer.append("\n" + resultStr);
		}
		resultStr = doAnalysis4Item(stackInfo, startTimeStr, endTimeStr, ItemCodeDefine.HF, tmsDataSet);
		if (resultStr != null) {
			strBuffer.append("\n" + resultStr);
		}
		resultStr = doAnalysis4Item(stackInfo, startTimeStr, endTimeStr, ItemCodeDefine.NH3, tmsDataSet);
		if (resultStr != null) {
			strBuffer.append("\n" + resultStr);
		}
		resultStr = doAnalysis4Item(stackInfo, startTimeStr, endTimeStr, ItemCodeDefine.CO, tmsDataSet);
		if (resultStr != null) {
			strBuffer.append("\n" + resultStr);
		}
		resultStr = doAnalysis4Item(stackInfo, startTimeStr, endTimeStr, ItemCodeDefine.O2, tmsDataSet);
		if (resultStr != null) {
			strBuffer.append("\n" + resultStr);
		}
		resultStr = doAnalysis4Item(stackInfo, startTimeStr, endTimeStr, ItemCodeDefine.FL1, tmsDataSet);
		if (resultStr != null) {
			strBuffer.append("\n" + resultStr);
		}
		resultStr = doAnalysis4Item(stackInfo, startTimeStr, endTimeStr, ItemCodeDefine.TMP, tmsDataSet);
		if (resultStr != null) {
			strBuffer.append("\n" + resultStr);
		}
		resultStr = doAnalysis4Item(stackInfo, startTimeStr, endTimeStr, ItemCodeDefine.TM1, tmsDataSet);
		if (resultStr != null) {
			strBuffer.append("\n" + resultStr);
		}
		resultStr = doAnalysis4Item(stackInfo, startTimeStr, endTimeStr, ItemCodeDefine.TM2, tmsDataSet);
		if (resultStr != null) {
			strBuffer.append("\n" + resultStr);
		}
		resultStr = doAnalysis4Item(stackInfo, startTimeStr, endTimeStr, ItemCodeDefine.TM3, tmsDataSet);
		if (resultStr != null) {
			strBuffer.append("\n" + resultStr);
		}
		
		// return
		return strBuffer.toString();

	} // end of method
	
	/**
	 * 
	 * @param stackInfo
	 * @param startTimeStr
	 * @param endTimeStr
	 * @param itemCode
	 * @param tmsDataSet
	 * @return
	 */
	private static String doAnalysis4Item(
			StackInfo stackInfo, 
			String startTimeStr, 
			String endTimeStr,
			int itemCode,
			TmsDataset tmsDataSet) {
		
		logger.debug("==> " + ItemCodeDefine.getItemName(itemCode) + " analysis starting.");
		
		// prepare basic information string
		String factoryName = stackInfo.getFactoryInfo().getFullName();
		String stackName   = stackInfo.getStackName();
		String stackClass1 = stackInfo.getFacilityClass1();
		String stackClass2 = stackInfo.getFacilityClass2();

		// Prepare analysis
		TmsDataSingleItemRecord[] records = null;
		CorrectionItemInfo correctionInfo = null;
		TmsDataCell[] tmsDataCell = null;
		String resultStr = "";
		
		// analysis
		records = tmsDataSet.getValidTmsDataRecords(itemCode);
		if (records.length == 0) {
			return null;
		}

		// Get Correction Factor Info
		correctionInfo =  tmsDataSet.getTmsDataHeader().getStackInfo().getCorrectionInfo(itemCode);

		// prepare data-set
		tmsDataCell = new TmsDataCell[records.length];
		for (int i=0 ; i<tmsDataCell.length ; i++) {
			tmsDataCell[i] = records[i].getTmsDataCell();
		}

		// Do outlier filtering
		double[] outlierLimits = DoAnalysisOutlier.doOutlierFiltering(tmsDataCell, false, 0.05D);
		//logger.debug(outlierLimits[0] + ", " + outlierLimits[1] + ", " + outlierLimits[2]);

		// do general stats analysis
		DoAnalysisGeneralStats generalStats = new DoAnalysisGeneralStats(tmsDataCell);
			
		resultStr = String.format(
				  "%s, %s, %s, %s, %s, %s, %s, %s, "
				+ "%d, %s, %s, %s, %s, "
				+ "%d, %.3f, %.3f, %.3f, "
				+ "%.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, "
				+ "%.3f, %.3f",
				
				(factoryName.equals("") ? " " : factoryName),
				(stackInfo.getFactoryInfo().getCode()),
				(stackName.equals("") ? " " : stackName),
				(stackInfo.getStackNumber() + ""),
				(stackClass1.equals("") ? " " : stackClass1),
				(stackClass2.equals("") ? " " : stackClass2),
				(startTimeStr),
				(endTimeStr),
				
				(itemCode),
				(ItemCodeDefine.getItemName(itemCode)),
				(correctionInfo.isTemperatureApplied() ? "ON" : "OFF"),
				(correctionInfo.isOxygenApplied() ? "ON" : "OFF"),
				(correctionInfo.isMoistureApplied() ? "ON" : "OFF"),
				
				(records.length),
				(outlierLimits[0]),
				(outlierLimits[1]),
				(outlierLimits[2]),
				
				(generalStats.getMeanValue()),
				(generalStats.getStandardDeviation()),
				(generalStats.getVariance()),
				(generalStats.getSkewness()),
				(generalStats.getKurtosis()),
				(generalStats.getMInValue()),
				(generalStats.getPercentile025()),
				(generalStats.getPercentile050()),
				(generalStats.getPercentile075()),
				(generalStats.getMaxValue()),
				(generalStats.getConfidence95_lower()),
				(generalStats.getConfidence95_upper()));

		return resultStr;

	} // end of method


	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================

	
	
	

} // end of class
