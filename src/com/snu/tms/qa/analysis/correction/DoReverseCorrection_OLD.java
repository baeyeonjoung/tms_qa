/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.correction;

import com.snu.tms.qa.model.CorrectionItemInfo;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.TmsDataCell;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;

/**
 * Get correction before value (temperature, moisture, oxygen)
 * 
 * 2016-11-07 com.snu.tms.qa.analysis.outlier.DoAnalysisReverseCorrection
 *            에서 처리하도록 수정
 * 
 */
public class DoReverseCorrection_OLD {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// static Methods
	//==========================================================================
	/**
	 * Do reverse correction
	 */
	/**
	public static void doReverseCorrection(
			TmsDataset tmsDataSet) {
		
		// prepare correctionFactor
		StackInfo  stackInfo = tmsDataSet.getTmsDataHeader().getStackInfo();
		CorrectionInfo correctionInfo = null;
		double stdOxyg = tmsDataSet.getTmsDataHeader().getStackInfo().getStandardOxygen();
		double stdMois = tmsDataSet.getTmsDataHeader().getStackInfo().getStandardMoisture();
		double tmsOxyg, tmsTmp;
		
		// do reverse correction
		TmsDataRecord[] tmsDataRecords = tmsDataSet.getTmsDataRecords();
		TmsDataCell dataCell = null;
		for (TmsDataRecord tmsDataRecord : tmsDataRecords) {
			
			// prepare correction value
			tmsOxyg = tmsDataRecord.getDataCell(ItemCodeDefine.O2).getData_value();
			tmsTmp  = tmsDataRecord.getDataCell(ItemCodeDefine.TMP).getData_value();
			
			// do reverse correction for each item
			for (int item=ItemCodeDefine.TSP ; item<=ItemCodeDefine.FL1 ; item++) {
				
				// Check data is valid
				dataCell = tmsDataRecord.getDataCell(item);
				if (dataCell.getData_value() == -1.0D) {
					continue;
				}
				
				// copy dataValue to originValue
				dataCell.setOrigin_value(dataCell.getData_value());
				
				// Get Correction Factor Info
				correctionInfo =  stackInfo.getCorrectionInfo(item);
				
				// Do oxygen correction
				if (correctionInfo.isOxygenApplied() && dataCell.getData_value() != -1.0D && stdOxyg != -1.0D) {
					dataCell.setOrigin_value(DoReverseCorrection_OLD.doOxygenReverseCorrection(
							dataCell.getOrigin_value(), stdOxyg, tmsOxyg));
					dataCell.setOxyg_correction(true);
				}
				
				// Do temperature correction
				if (correctionInfo.isTemperatureApplied() && dataCell.getData_value() != -1.0D) {
					dataCell.setOrigin_value(DoReverseCorrection_OLD.doTemperatureReverseCorrection(
							dataCell.getOrigin_value(), tmsTmp));
					dataCell.setTemp_correction(true);
				}
			
				// Do Moisture correction
				if (correctionInfo.isMoistureApplied() && dataCell.getData_value() != -1.0D && stdMois != -1.0D) {
					if (item == ItemCodeDefine.FL1) {
						dataCell.setOrigin_value(DoReverseCorrection_OLD.doMoistureReverseCorrectionFlow(
								dataCell.getOrigin_value(), stdMois));
						dataCell.setMois_correction(true);
					} else {
						dataCell.setOrigin_value(DoReverseCorrection_OLD.doMoistureReverseCorrection(
								dataCell.getOrigin_value(), stdMois));
						dataCell.setMois_correction(true);
					}
				}
				
				// logging
				//System.out.println(String.format("ItemCode:%s, vlaue:%.3f, corrValue:%.3f", 
				//		ItemCodeDefine.ITEM_NAME[item], dataCell.getData_value(), dataCell.getOrigin_value()));
			}
		}
		
	} // end of method
	*/
			
	/**
	 * Do temperature reverse correction
	 */
	public  static  double  doTemperatureReverseCorrection(
			double  afterValue,
			double  gauge_temp) {
		
		double  beforeValue = 0.0D;
		beforeValue = afterValue * (273.0D)/(273.0D + gauge_temp);
		
		return beforeValue;
	}
	
	/**
	 * Do Moisture reverse correction
	 */
	public  static  double  doMoistureReverseCorrection(
			double  afterValue,
			double  stck_mois) {
		
		double  beforeValue = 0.0D;
		if (stck_mois == 100.0D) {
			beforeValue = afterValue;
		} else {
			beforeValue = afterValue * (100.0D - stck_mois)/(100.0D);
		}
		
		return beforeValue;
	}
	
	/**
	 * Do Moisture reverse correction (flow)
	 */
	public  static  double  doMoistureReverseCorrectionFlow(
			double  afterValue,
			double  stck_mois) {
		
		double  beforeValue = 0.0D;
		if (stck_mois == 100.0D) {
			beforeValue = afterValue;
		} else {
			beforeValue = afterValue * (100.0D)/(100.0D - stck_mois);
		}
		return beforeValue;
	}
	
	/**
	 * Do oxygen reverse correction
	 */
	public  static  double  doOxygenReverseCorrection(
			double  afterValue,
			double  std_oxyg, 
			double  gauge_oxyg) {
		
		double  beforeValue = 0.0D;
		if (gauge_oxyg == 21.0D || std_oxyg == 21.0D) {
			beforeValue = afterValue;
		} else {
			beforeValue = afterValue * (21.0D - gauge_oxyg)/(21.0D - std_oxyg);
		}
		return beforeValue;
	}

} // end of class
