/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.generalstats;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import com.snu.tms.qa.model.TmsDataCell;


/**
 * Calculate Confidence Interval for paired sample
 * 
 */
public class DoAnalysisGeneralStats {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Fields
	//==========================================================================
	private  TmsDataCell[] tmsDataCellArray = null;

	private  int     dataCount = 0;
	private  double  meanValue = 0.0D;
	private  double  variance = 0.0D;
	private  double  standardDeviation = 0.0D;
	private  double  skewness = 0.0D;
	private  double  kurtosis = 0.0D;
	private  double  minValue = 0.0D;
	private  double  maxValue = 0.0D;
	private  double  percentile025 = 0.0D;
	private  double  percentile050 = 0.0D;
	private  double  percentile075 = 0.0D;
	
	private  double  confidence95_lower = 0.0D;
	private  double  confidence95_upper = 0.0D;
	

	//==========================================================================
	// Constructor
	//==========================================================================
	public DoAnalysisGeneralStats(TmsDataCell[] tmsDataCell) {

		this.tmsDataCellArray = tmsDataCell;
		doAnalysis();
	
	}
	
	//==========================================================================
	// static Methods
	//==========================================================================

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * doAnalysis
	 */
	private void doAnalysis() {
		
		// compute dataCount
		this.dataCount = (this.tmsDataCellArray != null) ? this.tmsDataCellArray.length : 0;
		
		// compute mean Value
		// Get average and standard deviations
		DescriptiveStatistics stats = new DescriptiveStatistics();
		for (int i=0 ; i<this.tmsDataCellArray.length ; i++) {
			stats.addValue(this.tmsDataCellArray[i].getData_value());
		}
		
		this.meanValue = stats.getMean();
		this.variance = stats.getVariance();
		this.standardDeviation = stats.getStandardDeviation();
		this.skewness = stats.getSkewness();
		this.kurtosis = stats.getKurtosis();
		this.minValue = stats.getMin();
		this.maxValue = stats.getMax();
		this.percentile025 = stats.getPercentile(25.0D);
		this.percentile050 = stats.getPercentile(50.0D);
		this.percentile075 = stats.getPercentile(75.0D);
		
		// Get Z-score
		double c = (1 + 0.99D) / 2.0D;
		NormalDistribution normalDist = new NormalDistribution();
		double z = normalDist.inverseCumulativeProbability(c);
				
		// Get upper limit and lower limit
		confidence95_lower = Math.max(0.0D, meanValue - (z * standardDeviation));
		confidence95_upper = meanValue + (z * standardDeviation);
		
		stats = null;
		
	} // end of method

	/**
	 * @return the tmsDataCellArray
	 */
	public TmsDataCell[] getTmsDataCellArray() {
		return tmsDataCellArray;
	}

	/**
	 * @param tmsDataCellArray the tmsDataCellArray to set
	 */
	public void setTmsDataCellArray(TmsDataCell[] tmsDataCellArray) {
		this.tmsDataCellArray = tmsDataCellArray;
	}

	/**
	 * @return the dataCount
	 */
	public int getDataCount() {
		return dataCount;
	}

	/**
	 * @return the meanValue
	 */
	public double getMeanValue() {
		return meanValue;
	}

	/**
	 * @return the variance
	 */
	public double getVariance() {
		return variance;
	}

	/**
	 * @return the standardDeviation
	 */
	public double getStandardDeviation() {
		return standardDeviation;
	}

	/**
	 * @return the skewness
	 */
	public double getSkewness() {
		return skewness;
	}

	/**
	 * @return the kurtosis
	 */
	public double getKurtosis() {
		return kurtosis;
	}

	/**
	 * @return the percentile000
	 */
	public double getMInValue() {
		return minValue;
	}

	/**
	 * @return the percentile025
	 */
	public double getPercentile025() {
		return percentile025;
	}

	/**
	 * @return the percentile050
	 */
	public double getPercentile050() {
		return percentile050;
	}

	/**
	 * @return the percentile075
	 */
	public double getPercentile075() {
		return percentile075;
	}

	/**
	 * @return the percentile100
	 */
	public double getMaxValue() {
		return maxValue;
	}

	/**
	 * @return the confidence95_lower
	 */
	public double getConfidence95_lower() {
		return confidence95_lower;
	}

	/**
	 * @param confidence95_lower the confidence95_lower to set
	 */
	public void setConfidence95_lower(double confidence95_lower) {
		this.confidence95_lower = confidence95_lower;
	}

	/**
	 * @return the confidence95_upper
	 */
	public double getConfidence95_upper() {
		return confidence95_upper;
	}

	/**
	 * @param confidence95_upper the confidence95_upper to set
	 */
	public void setConfidence95_upper(double confidence95_upper) {
		this.confidence95_upper = confidence95_upper;
	}

} // end of class
