/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.confidenceinterval;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;


/**
 * Calculate Confidence Interval for paired sample
 * 
 */
public class DoAnalysisConfidenceIntervalOneSample {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(DoAnalysisConfidenceIntervalOneSample.class);

	
	//==========================================================================
	// static Methods
	//==========================================================================
	/**
	 * Calculate Confidence Interval for paired sample (Percent of Difference)
	 * ref. Guideline on the meaning and the use of precision and Bias
	 *      (Data Required by 40 CRF Part 58 Appendix A)
	 */
	public  static  double[]  doAnlaysis(
			double[]  samples_1,
			double    probabilityInPercent) {
		
		// init variables
		// [0] lower limit, [1] upper limit, [2] exceed limit percent
		double[] results = new double[3];
		Arrays.fill(results, 0.0D);
		
		// Check value size
		if (samples_1 == null) {
			return results;
		}
		
		// Set parameters
		int  n  = samples_1.length;
		int  df = n-1;                // degree of freedom
		
		// Get average and standard deviations
		DescriptiveStatistics stats = new DescriptiveStatistics();
		for (int i=0 ; i<samples_1.length ; i++) {
			stats.addValue(samples_1[i]);
		}
		double mean = stats.getMean();
		double std = stats.getStandardDeviation();
		
		// Get Z-score
		double c = (1 + probabilityInPercent/100.0D) / 2.0D;
		NormalDistribution normalDist = new NormalDistribution();
		double z = normalDist.inverseCumulativeProbability(c);
		
		// Get upper limit and lower limit
		results[0] = mean - (z * std);
		results[1] = mean + (z * std);
		
		// Get Exceed Percent
		int exceedCount = 0;
		for (int i=0 ; i<samples_1.length ; i++) {
			//logger.debug(String.format("i=%d, after=%.3f, before=%.3f, value=%.3f, lower=%.3f, upper=%.3f\n", i, samples_1[i], samples_2[i], d[i], results[0], results[1]));
			if (samples_1[i] < results[0] || samples_1[i] > results[1]) {
				exceedCount++;
			}
		}
		results[2] = (double)exceedCount / (double)n * 100.0D;
		
		// Logger
		logger.debug(String.format(
				"n=%d, df=%d, avg=%.4f, std_deviation=%.4f, z=%.4f, lower=%.4f, upper=%.4f, exceed=%.2f percent\n",
				n, df, mean, std, z, results[0], results[1], results[2]));
		
		return results;
		
	}

} // end of class
