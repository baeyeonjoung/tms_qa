/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.confidenceinterval;

import org.apache.commons.math3.distribution.NormalDistribution;

import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.TmsDataSingleItemRecord;
import com.snu.tms.qa.model.correction.TmsDataCorrectionModel;


/**
 * Calculate Confidence Interval for paired sample
 * 
 */
public class DoAnlaysisWithPercentOfDifference {

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// static Methods
	//==========================================================================
	/**
	 * Calculate Confidence Interval for paired sample (Percent of Difference)
	 * ref. Guideline on the meaning and the use of precision and Bias
	 *      (Data Required by 40 CRF Part 58 Appendix A)
	 */
	public  static  TmsDataCorrectionModel  doAnlaysisWithPercentOfDifference(
			int  itemCode,
			TmsDataset dataSet,
			double probabilityInPercent) {
		
		// prepare TmsDataCorrectionModel
		TmsDataCorrectionModel model = new TmsDataCorrectionModel();
		model.setItemCode(itemCode);
		TmsDataSingleItemRecord[] records = dataSet.getValidTmsDataRecords(itemCode);
		
		// Set parameters
		int  n  = records.length;
		int  df = n-1;                // degree of freedom
		
		// Get Percent of Difference and it's average
		double[] d = new double[n];
		double   d_sum = 0.0D;
		double   d_avg = 0.0D;
		for (int i=0 ; i<d.length ; i++) {
			if (records[i].getTmsDataCell().getOrigin_value() == 0.0D) {
				d[i] = 100.0D;
			} else {
				d[i] = (records[i].getTmsDataCell().getData_value() - records[i].getTmsDataCell().getOrigin_value()) / 
						records[i].getTmsDataCell().getOrigin_value() * 100.0D;
			}
			d_sum += d[i];
		}
		d_avg = d_sum / (double)n;
		
		// Get Percent of Difference and it's average
		double   d_square_sum = 0.0D;
		for (int i=0 ; i<d.length ; i++) {
			d_square_sum += Math.pow(d[i] - d_avg, 2);
		}
		
		// Get Standard Deviation
		double sd = Math.sqrt(d_square_sum / (double)df);
		
		// Get Z-score
		double c = (1 + probabilityInPercent/100.0D) / 2.0D;
		NormalDistribution normalDist = new NormalDistribution();
		double z = normalDist.inverseCumulativeProbability(c);
		
		// Get upper limit and lower limit
		model.setPercentOfDiffLowerLimit(d_avg - (z * sd));
		model.setPercentOfDiffUpperLimit(d_avg + (z * sd));
		
		// Set model
		for (int i=0 ; i<records.length ; i++) {
			model.setData(
					records[i].getTimestr(), 
					records[i].getTime(), 
					records[i].getTmsDataCell().getData_value(), 
					records[i].getTmsDataCell().getOrigin_value(), 
					d[i]);
		}
		
		// return
		return model;
		
	} // end of method

} // end of class
