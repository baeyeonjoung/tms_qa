/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.pca;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;
import org.apache.commons.math3.ml.clustering.MultiKMeansPlusPlusClusterer;
import org.apache.commons.math3.ml.distance.EarthMoversDistance;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.PatternDataSet;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.ruleset.PatternDataRecord;

/**
 * Do compute PCA(Principal Component Analysis)
 * 
 */
public class DoComputePCA {

	//==========================================================================
	// Static Fields
	//==========================================================================
	public final static int   CLUSTER_MAX_ITERATION = 50000000;

	private static Log logger = LogFactory.getLog(DoComputePCA.class);

	
	//==========================================================================
	// Test main method
	//==========================================================================

	//==========================================================================
	// static Methods
	//==========================================================================
	/**
	 * Do Principal Component Analysis
	 */
	public static PatternDataSet doAnalysis(
			Vector<Integer> targetItems, 
			TmsDataset tmsDataSet, 
			int timeWindow,
			int patternTypeNum) {
		
		// Get targetItems
		if (targetItems == null || targetItems.size() == 0) {
			return null;
		}
		int itemCount = targetItems.size();
		int[] items = new int[itemCount];
		for (int i=0 ; i<itemCount ; i++) {
			items[i] = targetItems.get(i).intValue();
		}
		
		// get raw data
		TmsDataRecord[] rawRecords = tmsDataSet.getTmsDataRecords();
		TmsDataRecord[] records = new TmsDataRecord[rawRecords.length];
		int validDataCount = 0;
		for (TmsDataRecord record : rawRecords) {
			for (int j=0 ; j<itemCount ; j++) {
				if (record.getDataCell(items[j]).getData_value() == -1.0D) {
					break;
				}
			}
			records[validDataCount++] = record;
		}
		if (validDataCount == 0) {
			return null;
		}
		records = Arrays.copyOf(records, validDataCount);
		
		// create data array
		double[][] dataset = new double[validDataCount][itemCount];
		for (int i=0 ; i<validDataCount ; i++) {
			for (int j=0 ; j<itemCount ; j++) {
				dataset[i][j] = records[i].getDataCell(items[j]).getData_value();
			}
		}
		
		// create PatternDataSet
		PatternDataSet patternDataSet = new PatternDataSet();
		patternDataSet.initDataRecord(records, items);
		
		try {
			// set data size
			int M = itemCount;
			int N = validDataCount;
			
			// calculate mean
			double[] mn    = new double[M];
			double[] sigma = new double[M];
			double[] skew  = new double[M];
			
			DescriptiveStatistics stats = null;
			for (int i=0 ; i<M ; i++) {
				
				stats = new DescriptiveStatistics();
				for (int j=0 ; j<N ; j++) {
					stats.addValue(dataset[j][i]);
				}
				mn[i]    = stats.getMean();
				sigma[i] = stats.getStandardDeviation();
				skew[i]  = stats.getSkewness();
				//System.out.println(String.format("Item[%s], mn[%.3f], sigma[%.3f], skew[%.3f]", 
				//		ItemCodeDefine.getItemName(items[i]), mn[i], sigma[i], skew[i] ));
				if (skew[i] == 0.0D) {
					skew[i] = 1.0D;
				}
				if (skew[i] > 0.0D && skew[i] < 0.1D) {
					skew[i] = 0.1D;
				}
				if (skew[i] < 0.0D && skew[i] > -0.1D) {
					skew[i] = -0.1D;
				}
				
				stats.clear();
			} // end of for-loop
			
			// data normalizing
			double[][] nDataset = new double[validDataCount][itemCount];
			for (int i=0 ; i<N ; i++) {
				for (int j=0 ; j<M ; j++) {
					nDataset[i][j] = (dataset[i][j] - mn[j]) / sigma[j];
				}
			}
			BlockRealMatrix mData = new BlockRealMatrix(nDataset);
			//logger.debug(mData);
			
			// calculate covariance matrix
			Covariance cv = new Covariance(nDataset);
			RealMatrix mCovariance = cv.getCovarianceMatrix();
			
			// find the eigenvectors and eigenvalues
			EigenDecomposition eigenDecomposition = new EigenDecomposition(mCovariance);
			RealMatrix mEigenVector = eigenDecomposition.getV();
			double[] eigenValues = eigenDecomposition.getRealEigenvalues();
			
			// sorting matrix with eigenvalue (decreasing order)
			//
			// create temporary SortIndex Object
			SortIndex[] sortIndices = new SortIndex[eigenValues.length];
			for (int i=0 ; i<eigenValues.length ; i++) {
				sortIndices[i] = new SortIndex(i, eigenValues[i]);
			}
			Arrays.sort(sortIndices);
			int[] rindices = new int[eigenValues.length];
			for (int i=0 ; i<rindices.length ; i++) {
				rindices[i] = sortIndices[i].index;
			}
			// sort eigenValues
			for (int i=0 ; i<eigenValues.length ; i++) {
				eigenValues[i] = sortIndices[i].value;
			}
			
			// sorting eigenVector
			double[][] eigenVector = sort(mEigenVector.getData(), rindices);
			
			// Set PCResults to PatternDataSet
			patternDataSet.setPcaResults(eigenVector, eigenValues);
			
			// project the original dataset to principal component axis
			BlockRealMatrix mPC = new BlockRealMatrix(eigenVector); 
			RealMatrix mSignals = mPC.multiply(mData.transpose()).transpose();
			for (int i=0 ; i<N ; i++) {
				patternDataSet.setPcaResultPoint(records[i].getTime(), mSignals.getRow(i));
			}
			
			// do clustering
			// Pattern 분석을 위해 자료를 Clustering 방법론을 통해 군집화
			List<ClusterWrapper> clusterInput = new ArrayList<ClusterWrapper>(N);
			double[] eigenValueWeightedPoint = new double[itemCount];
			for (int i=0 ; i<N ; i++) {
				for (int j=0 ; j<itemCount ; j++) {
					eigenValueWeightedPoint[j] = nDataset[i][j]  
									* Math.abs(1.0D / skew[j]) 
									* ItemCodeDefine.getItemWeighting(items[j]);
				}
				clusterInput.add(new ClusterWrapper(records[i].getTime(), eigenValueWeightedPoint));
			}
			
			// apply clustering algorithms (KMeans++)
			// 다양한 Distance를 구하는 방법을 시험하였는데, EarthMoversDistancerk 가장 적합하다.
			// 이유로는 변수의 분포도를 측정하기 때문인 것으로 파악된다.
			// 
			//KMeansPlusPlusClusterer<ClusterWrapper> clusterer = new KMeansPlusPlusClusterer<ClusterWrapper>(
			//		patternTypeNum, CLUSTER_MAX_ITERATION, new EarthMoversDistance());
			KMeansPlusPlusClusterer<ClusterWrapper> clusterer = new KMeansPlusPlusClusterer<ClusterWrapper>(
					patternTypeNum, CLUSTER_MAX_ITERATION, new EarthMoversDistance());
			MultiKMeansPlusPlusClusterer<ClusterWrapper> mClusterer = 
					new MultiKMeansPlusPlusClusterer<ClusterWrapper>(clusterer, 5);
			List<CentroidCluster<ClusterWrapper>> clusterResults = mClusterer.cluster(clusterInput);
			
			// Set Pattern
			long[] patternKeys = new long[N];
			for (int i=0; i<clusterResults.size(); i++) {
				int count = 0;
				for (ClusterWrapper locationWrapper : clusterResults.get(i).getPoints()) {
					patternKeys[count++] = locationWrapper.key;
				}
				patternDataSet.setPattern(Arrays.copyOf(patternKeys, count));
			}
	
			// apply time window
			PatternDataRecord[] sortedRecords = patternDataSet.getAllSortedPatternDataRecord();
			long timeWindowSize = ((long)timeWindow) * 60L * 1000L;
			long baseTime = sortedRecords[0].getTime();
			ArrayList<PatternDataRecord> recordsInTimeWindow = new ArrayList<PatternDataRecord>();
			for (int i=0 ; i<sortedRecords.length ; i++) {
				if ((sortedRecords[i].getTime() - baseTime) < timeWindowSize) {
					recordsInTimeWindow.add(sortedRecords[i]);
				} else {
					setPatternInTimeWindow(patternDataSet, recordsInTimeWindow);
					baseTime = sortedRecords[i].getTime();
					recordsInTimeWindow.clear();
					recordsInTimeWindow.add(sortedRecords[i]);
				}
			}
			
			/**
			// Set Confidence 95% Interval
			//
			// calculate NormalDistribution  Z-score
			double c = (1 + 0.95D) / 2.0D;
			NormalDistribution normalDist = new NormalDistribution();
			double z = normalDist.inverseCumulativeProbability(c);
	
			DescriptiveStatistics intervalStats = null;
			double mean = 0.0D;
			double stdev = 0.0D;
			double confidence95_upper = 0.0D;
			for (int i=0 ; i<itemCount ; i++) {
				intervalStats = new DescriptiveStatistics();
				for (int j=0 ; j<sortedRecords.length ; j++) {
					intervalStats.addValue(sortedRecords[j].getValue(i));
				}
				mean = intervalStats.getMean();
				stdev = intervalStats.getStandardDeviation();
				confidence95_upper = mean + (z * stdev);
				patternDataSet.setConfidenceInterval(i, 0.0D, confidence95_upper);
			}
			*/
			// Set 98%ile range
			DescriptiveStatistics intervalStats = null;
			double minRange, maxRange;
			for (int i=0 ; i<itemCount ; i++) {
				intervalStats = new DescriptiveStatistics();
				for (int j=0 ; j<sortedRecords.length ; j++) {
					intervalStats.addValue(sortedRecords[j].getValue(i));
				}
				minRange = intervalStats.getPercentile(1.0D);
				maxRange = intervalStats.getPercentile(99.0D);
				patternDataSet.setConfidenceInterval(i, minRange, maxRange);
			}
			
			// Re-assign PatternIndex
			patternDataSet.reAssignPatternIndex();
			
			// return
			return patternDataSet;
			
		} catch (Exception eee) {
			logger.error(eee.toString(), eee);
			return patternDataSet;
		} 
		
	} // end of method
	
	/**
	 * split pattern
	 */
	public static int[] splitPattern(
			PatternDataSet patternDataSet, 
			int targetPatternIndex,
			int patternNumber) {
		
		// Get target PatternDataRecords
		PatternDataRecord[] records = patternDataSet.getSortedPatternDataRecord(targetPatternIndex);
		int[] items = patternDataSet.getItems();
		
		// set data size
		int M = patternDataSet.getItems().length;
		int N = records.length;
		
		// calculate mean
		double[] mn    = new double[M];
		double[] sigma = new double[M];
		double[] skew  = new double[M];
		
		DescriptiveStatistics stats;
		for (int i=0 ; i<M ; i++) {
			stats = new DescriptiveStatistics();
			for (int j=0 ; j<N ; j++) {
				stats.addValue(records[j].getValue(i));
			}
			mn[i]    = stats.getMean();
			sigma[i] = stats.getStandardDeviation();
			skew[i]  = stats.getSkewness();
			//System.out.println(String.format("Item[%s], mn[%.3f], sigma[%.3f], skew[%.3f]", 
			//		ItemCodeDefine.getItemName(items[i]), mn[i], sigma[i], skew[i] ));
			if (skew[i] == 0.0D) {
				skew[i] = 1.0D;
			}
			if (skew[i] > 0.0D && skew[i] < 0.1D) {
				skew[i] = 0.1D;
			}
			if (skew[i] < 0.0D && skew[i] > -0.1D) {
				skew[i] = -0.1D;
			}
			stats.clear();
		} // end of for-loop
		
		double[][] nDataset = new double[N][M];
		for (int i=0 ; i<N ; i++) {
			for (int j=0 ; j<M ; j++) {
				nDataset[i][j] = (records[i].getValue(j) - mn[j]) / sigma[j];
			}
		}
		
		// do clustering
		// make cluster input
		List<ClusterWrapper> clusterInput = new ArrayList<ClusterWrapper>(N);
		double[] valueWeightedPoint = new double[M];
		for (int i=0 ; i<N ; i++) {
			for (int j=0 ; j<M ; j++) {
				valueWeightedPoint[j] = nDataset[i][j]  
						* Math.abs(1.0D / skew[j]) 
						* ItemCodeDefine.getItemWeighting(items[j]);
			}
			clusterInput.add(new ClusterWrapper(records[i].getTime(), valueWeightedPoint));
		}
		
		// apply clustering algorithms (KMeans++)
		KMeansPlusPlusClusterer<ClusterWrapper> clusterer = new KMeansPlusPlusClusterer<ClusterWrapper>(
				patternNumber, CLUSTER_MAX_ITERATION, new EarthMoversDistance());
		MultiKMeansPlusPlusClusterer<ClusterWrapper> mClusterer = 
				new MultiKMeansPlusPlusClusterer<ClusterWrapper>(clusterer, 5);
		List<CentroidCluster<ClusterWrapper>> clusterResults = mClusterer.cluster(clusterInput);
		
		// Set Pattern
		int[] newPatternIndices = new int[clusterResults.size()];
		int patternIndex = targetPatternIndex * 10;
		long[] patternKeys = new long[N];
		for (int i=0; i<clusterResults.size(); i++) {
			int count = 0;
			for (ClusterWrapper locationWrapper : clusterResults.get(i).getPoints()) {
				patternKeys[count++] = locationWrapper.key;
			}
			newPatternIndices[i] = patternIndex;
			patternDataSet.setPattern(patternIndex++, Arrays.copyOf(patternKeys, count));
		}
		
		// remove old pattern
		patternDataSet.removePattern(targetPatternIndex);
		
		return newPatternIndices;
		
	} // end of method
	

	/**
	 * sorting matrix with eigenvalue (decreasing order)
	 */
	public static double[][] sort(double[][] eigenVector, int[] rindices) {
		
		// Check dimension
		if (eigenVector == null || rindices == null || eigenVector.length != rindices.length) {
			return eigenVector;
		}
		
		// sorted matrix
		double[][] sortedEigenVector = new double[eigenVector.length][eigenVector[0].length];
		for (int i=0 ; i<rindices.length ; i++) {
			for (int j=0 ; j<sortedEigenVector.length ; j++) {
				sortedEigenVector[j][i] = eigenVector[j][rindices[i]];
			}
		}
		
		// return 
		return sortedEigenVector;
		
	} // end of method
	
	
	/**
	 * Set a same PatternIndex in TimeWindow
	 */
	private static void setPatternInTimeWindow(
			PatternDataSet patternDataSet, 
			ArrayList<PatternDataRecord> recordsInTimeWindow) {
		
		// create PatternIndex Hashtable
		Hashtable<Integer, PatternIndexCount> patternIndexHash = new Hashtable<Integer, PatternIndexCount>();
		PatternIndexCount patternIndexCount;
		for (PatternDataRecord record : recordsInTimeWindow) {
			patternIndexCount = patternIndexHash.get(record.getPatternIndex());
			if (patternIndexCount == null) {
				patternIndexHash.put(record.getPatternIndex(), new PatternIndexCount(record.getPatternIndex()));
			} else {
				patternIndexCount.patternIndexCount++;
			}
		}
		
		// Search max count
		PatternIndexCount maxPatternIndexCount = null;
		for (Enumeration<PatternIndexCount> e = patternIndexHash.elements(); e.hasMoreElements();) {
			patternIndexCount = e.nextElement();
			if (maxPatternIndexCount == null) {
				maxPatternIndexCount = patternIndexCount;
			} else if (maxPatternIndexCount.patternIndexCount < patternIndexCount.patternIndexCount) {
				maxPatternIndexCount = patternIndexCount;
			}
		}
		
		// Set PatternIndex
		for (PatternDataRecord record : recordsInTimeWindow) {
			if (record.getPatternIndex() != maxPatternIndexCount.patternIndex) {
				patternDataSet.updatePatternData(record, maxPatternIndexCount.patternIndex);
			}
		}
		
	} // end of method
	
	
	//==========================================================================
	// Static Inner Class
	//==========================================================================
	static class PatternIndexCount {
		
		public int patternIndex = 0;
		public int patternIndexCount = 0;
		
		public PatternIndexCount(int patternIndex) {
			this.patternIndex = patternIndex;
		}
		
	} // end of inner class

} // end of class
