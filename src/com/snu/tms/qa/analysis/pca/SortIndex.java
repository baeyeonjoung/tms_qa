/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.pca;

/**
 * Do compute PCA(Principal Component Analysis)
 * 
 */
public class SortIndex implements Comparable<SortIndex> {

	//==========================================================================
	// Fields
	//==========================================================================
	public  int     index = 0;
	public  double  value = 0.0D;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public SortIndex(int index, double value) {
		this.index = index;
		this.value = value;
	}
	
	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * Reverse sorting (decreasing order)
	 */
	public int compareTo(SortIndex compare) {
		return (int)((compare.value - this.value) * 100.0D);
	}
		
} // end of class
