/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.pca;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;
import org.apache.commons.math3.ml.distance.EuclideanDistance;
import org.apache.commons.math3.stat.correlation.Covariance;

import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;

/**
 * Do compute PCA(Principal Component Analysis)
 * 
 */
public class DoComputePCA_OLD_OLD {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(DoComputePCA_OLD_OLD.class);
	
	//==========================================================================
	// Test main method
	//==========================================================================
	/**
	 * Test main()
	 */
	public static void main(String args[]) {
		
		// Setup logging and other required library
		try {
			org.apache.log4j.PropertyConfigurator.configure("./conf/log.properties");
			logger.info("TMS Quality Assessment process is stating .............");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("./conf/log.properties file can't found.\n"
					+ "Process halted and system will be exit.");
			System.exit(1);
		}
				
		// Set test data
		double[][] data = {
				{10.0D, 3.0D}, 
				{11.0D, 3.0D}, 
				{12.0D, 3.0D}, 
				{10.0D, 4.0D}, 
				{11.0D, 8.0D}, 
				{10.0D, 7.0D}, 
				{ 9.0D, 3.0D}, 
				{10.0D, 3.0D}, 
				{12.0D, 4.0D}, 
				{11.0D, 3.0D} };
		
		// compute PCA
		DoComputePCA_OLD_OLD.doPcaAnalysis(data);
		
		
	} // end of method
	
	//==========================================================================
	// static Methods
	//==========================================================================
	/**
	 * Do Principal Component Analysis
	 */
	public static void doAnalysis(
			Vector<Integer> targetItems, TmsDataset tmsDataSet, boolean isCorrectedValue) {
		
		// get raw data
		TmsDataRecord[] records = tmsDataSet.getTmsDataRecords();
		int recordCount = records.length;
		if (recordCount == 0) {
			return;
		}
		
		if (targetItems == null || targetItems.size() == 0) {
			return;
		}
		
		// Get targetItems
		int itemCount = targetItems.size();
		int[] items = new int[itemCount];
		for (int i=0 ; i<itemCount ; i++) {
			items[i] = targetItems.get(i).intValue();
		}
		
		// create data array
		double[][] datasetBuffer = new double[recordCount][itemCount];
		int validDataCount = 0;
		for (int i=0 ; i<recordCount ; i++) {
			for (int j=0 ; j<itemCount ; j++) {
				if (isCorrectedValue) {
					datasetBuffer[validDataCount][j] = records[i].getDataCell(items[j]).getData_value();
				} else {
					datasetBuffer[validDataCount][j] = records[i].getDataCell(items[j]).getOrigin_value();
				}
				if (datasetBuffer[validDataCount][j] == -1.0D) {
					break;
				}
			}
			validDataCount++;
		}
		
		// set data size
		double[][] dataset = Arrays.copyOf(datasetBuffer, validDataCount);
		int M = itemCount;
		int N = validDataCount;
		
		// subtract off the mean for each dimension
		double[] avgM = new double[M];
		Arrays.fill(avgM, 0.0D);
		for (int i=0 ; i<N ; i++) {
			for (int j=0 ; j<M ; j++) {
				avgM[j] += dataset[i][j];
			}
		}
		for (int j=0 ; j<M ; j++) {
			avgM[j] = avgM[j] / (double)N;
		}
		
		// 
		
		/**
		// create dataset
		TmsDataCell4Mahalanobis[] datasetBuffer = new TmsDataCell4Mahalanobis[recordCount];
		int validDataCount = 0;
		long time;
		double data1_value, data2_value;
		for (int i=0 ; i<recordCount ; i++) {
			time = records[i].getTime();
			if (isCorrectedValue) {
				data1_value = records[i].getDataCell(itemCode1).getData_value();
				data2_value = records[i].getDataCell(itemCode2).getData_value();
			} else {
				data1_value = records[i].getDataCell(itemCode1).getOrigin_value();
				data2_value = records[i].getDataCell(itemCode2).getOrigin_value();
			}
			if (data1_value == -1.0D || data2_value == -1.0D) {
				continue;
			}
			datasetBuffer[validDataCount] = new TmsDataCell4Mahalanobis(time, itemCode1, data1_value, itemCode2, data2_value);
			validDataCount++;
		}
		if (validDataCount == 0) {
			return null;
		}
		
		// trim dataset
		TmsDataCell4Mahalanobis[] dataset = Arrays.copyOf(datasetBuffer, validDataCount);
		
		// compute average
		double sum_x = 0.0D, sum_y = 0.0D;
		for (int i=0 ; i<validDataCount ; i++) {
			sum_x += dataset[i].getData1_value();
			sum_y += dataset[i].getData2_value();
		}
		double[][] avg = new double[2][1];
		avg[0][0] = sum_x / (double)validDataCount;
		avg[1][0] = sum_y / (double)validDataCount;
		
		// compute covariance matrix
		double[][] valueset = new double[validDataCount][2];
		for (int i=0 ; i<validDataCount ; i++) {
			valueset[i][0] = dataset[i].getData1_value();
			valueset[i][1] = dataset[i].getData2_value();
		}
		
		Covariance covariance = new Covariance(valueset);
		RealMatrix covMatrix = covariance.getCovarianceMatrix();
		RealMatrix imverseCovMatrix = null;
		try {
			imverseCovMatrix = MatrixUtils.inverse(covMatrix);
		} catch (Exception e) {
			return null;
		}
		
		// compute mahalanobis distnace
		double[][] datum = new double[2][1];
		RealMatrix distance;
		RealMatrix md_square;
		double md;
		for (int i=0 ; i<validDataCount ; i++) {
			datum[0][0] = valueset[i][0] - avg[0][0];
			datum[1][0] = valueset[i][1] - avg[1][0];
			distance = new BlockRealMatrix(datum);
			md_square = distance.transpose().multiply(imverseCovMatrix).multiply(distance);
			md = Math.sqrt(md_square.getEntry(0, 0));
			dataset[i].setMahalanobisDistance(md);
		}
		
		// sorting with md
		Arrays.sort(dataset, new Comparator<TmsDataCell4Mahalanobis>() {
			public int compare(TmsDataCell4Mahalanobis dataCell1, TmsDataCell4Mahalanobis dataCell2) {
				double x = dataCell1.getMahalanobisDistance();
				double y = dataCell2.getMahalanobisDistance();
				if (x > y) { return 1;}
				else if (x < y) { return -1;}
				else { return 0;}
			}
		});
		
		// compute qq-plot with chi-square distribution
		ChiSquaredDistribution chiSquareDist = new ChiSquaredDistribution((double) (validDataCount-1));
		for (int i=0 ; i<validDataCount ; i++) {
			double prob = (i+0.5D) / (double)validDataCount;
			double chiQ = chiSquareDist.inverseCumulativeProbability(prob);
			dataset[i].setImverseChiSquare(chiQ);
		}
		
		// re-sorting with time
		Arrays.sort(dataset, new Comparator<TmsDataCell4Mahalanobis>() {
			public int compare(TmsDataCell4Mahalanobis dataCell1, TmsDataCell4Mahalanobis dataCell2) {
				long x = dataCell1.getTime();
				long y = dataCell2.getTime();
				if (x > y) { return 1;}
				else if (x < y) { return -1;}
				else { return 0;}
			}
		});
		*/
		
		
	} // end of method
	
	
	/**
	 * Compute Principal Component Analysis(PCA)
	 * data : M * N (M is item count(dimension), N is records count)
	 */
	public static Object doPcaAnalysis(double[][] data) {
		
		// set data size
		if (data == null || data.length <= 0 || data[0].length <= 0) {
			return null;
		}
		int M, N;
		N = data.length;
		M = data[0].length;
		
		// calculate mean
		double[] mn = new double[M];
		Arrays.fill(mn, 0.0D);
		for (int i=0 ; i<M ; i++) {
			for (int j=0 ; j<N ; j++) {
				mn[i] += data[j][i];
			}
			mn[i] = mn[i] / (double)N;
		}
		
		// subtract off the mean for each dimension
		for (int i=0 ; i<N ; i++) {
			for (int j=0 ; j<M ; j++) {
				data[i][j] = data[i][j] - mn[j];
			}
		}
		BlockRealMatrix mData = new BlockRealMatrix(data);
		logger.debug(mData);
		
		// calculate covariance matrix
		Covariance cv = new Covariance(data);
		RealMatrix mCovariance = cv.getCovarianceMatrix();
		logger.debug(mCovariance);
		
		// find the eigenvectors and eigenvalues
		EigenDecomposition eigenDecomposition = new EigenDecomposition(mCovariance);
		RealMatrix mEigenVector = eigenDecomposition.getV();
		logger.debug(mEigenVector);
		RealMatrix mEigenValue = eigenDecomposition.getD();
		logger.debug(mEigenValue);
		double[] eigenValues = eigenDecomposition.getRealEigenvalues();
		
		// sorting matrix with eigenvalue (decreasing order)
		//
		// create temporary SortIndex Object
		SortIndex[] sortIndices = new SortIndex[eigenValues.length];
		for (int i=0 ; i<eigenValues.length ; i++) {
			sortIndices[i] = new SortIndex(i, eigenValues[i]);
		}
		Arrays.sort(sortIndices);
		int[] rindices = new int[eigenValues.length];
		for (int i=0 ; i<rindices.length ; i++) {
			rindices[i] = sortIndices[i].index;
		}
		// sort eigenValues
		for (int i=0 ; i<eigenValues.length ; i++) {
			eigenValues[i] = sortIndices[i].value;
		}
		// sorting eigenVector
		double[][] eigenVector = sort(mEigenVector.getData(), rindices);
		
		// project the original dataset to principal component axis
		BlockRealMatrix mPC = new BlockRealMatrix(eigenVector); 
		RealMatrix mSignals = mPC.multiply(mData.transpose()).transpose();
		//System.out.println(mSignals);
		//System.out.println(mData);
		
		// weighted distance (weight factor is eigenvalue)
		double D;
		for (int i=0 ; i<N ; i++) {
			D = 0.0D;
			for (int j=0 ; j<M ; j++) {
				D += Math.pow(mSignals.getEntry(i, j) * eigenValues[j], 2.0D);
			}
			System.out.println(Math.sqrt(D));
		}
		
		// do clustering
		// make cluster input
		List<ClusterWrapper> clusterInput = new ArrayList<ClusterWrapper>(N);
		double[] eigenValueWeightedPoint = new double[M];
		for (int i=0 ; i<N ; i++) {
			for (int j=0 ; j<M ; j++) {
				eigenValueWeightedPoint[j] = mSignals.getEntry(i,  j) * eigenValues[j];
			}
			clusterInput.add(new ClusterWrapper(i, eigenValueWeightedPoint));
		}
		
		// apply clustering algoritms (KMeans++)
		final int CLUSTER_SIZE = 2;
		final int CLUSTER_MAX_ITERATION = 10000;
		KMeansPlusPlusClusterer<ClusterWrapper> clusterer = new KMeansPlusPlusClusterer<ClusterWrapper>(
				CLUSTER_SIZE, CLUSTER_MAX_ITERATION, new EuclideanDistance());
		List<CentroidCluster<ClusterWrapper>> clusterResults = clusterer.cluster(clusterInput);
		
		// test print
		for (int i=0; i<clusterResults.size(); i++) {
			System.out.println("Cluster " + i);
			for (ClusterWrapper locationWrapper : clusterResults.get(i).getPoints()) {
				System.out.println(locationWrapper.toString());
			}
			System.out.println();
		}
		
		
		
		
		
		
		
		// return
		return null;
		
	} // end of method
	
	
	/**
	 * sorting matrix with eigenvalue (decreasing order)
	 */
	public static double[][] sort(double[][] eigenVector, int[] rindices) {
		
		// Check dimension
		if (eigenVector == null || rindices == null || eigenVector.length != rindices.length) {
			return eigenVector;
		}
		
		// sorted matrix
		double[][] sortedEigenVector = new double[eigenVector.length][eigenVector[0].length];
		for (int i=0 ; i<rindices.length ; i++) {
			for (int j=0 ; j<sortedEigenVector.length ; j++) {
				sortedEigenVector[j][i] = eigenVector[j][rindices[i]];
			}
		}
		
		// return 
		return sortedEigenVector;
		
	} // end of method
	
	

} // end of class
