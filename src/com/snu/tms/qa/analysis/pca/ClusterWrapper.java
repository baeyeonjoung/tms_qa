/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.pca;

import java.util.Arrays;

import org.apache.commons.math3.ml.clustering.Clusterable;

/**
 * Clustering wrapper (computing cluster after PCA)
 * 
 */
public class ClusterWrapper implements Clusterable {

	//==========================================================================
	// Fields
	//==========================================================================
	public  double[]  points;
	public  long      key;
	
	//==========================================================================
	// Constructor
	//==========================================================================
	/**
	 * Constructor
	 */
	public ClusterWrapper(long key, double[] location) {
		
		this.key = key;
		this.points = Arrays.copyOf(location, location.length);

	} // end of constructor
	
	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * implements Clusterable:getPoint()
	 */
	public double[] getPoint() {
		return points;
	} // end of mehtod

	//==========================================================================
	// implemented Methods
	//==========================================================================
	/**
	 * implements Object:toString()
	 */
	public String toString() {
		
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append(String.format("%4d(%7.3f", this.key, points[0]));
		for (int i=1 ; i<this.points.length ; i++) {
			strBuffer.append(String.format(", %7.3f", points[i]));
		}
		strBuffer.append(")");
		return strBuffer.toString();
		
	} // end of mehtod

	
	//==========================================================================
	// Methods
	//==========================================================================
	
		
} // end of class
