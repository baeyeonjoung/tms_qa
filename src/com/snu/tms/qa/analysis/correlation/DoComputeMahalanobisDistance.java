/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.correlation;

import java.util.Arrays;
import java.util.Comparator;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.correlation.Covariance;

import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.correlation.TmsDataCell4Mahalanobis;

/**
 * Do compute mahalanobis distance
 * 
 */
public class DoComputeMahalanobisDistance {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// static Methods
	//==========================================================================
	/**
	 * Do reverse correction
	 */
	public static TmsDataCell4Mahalanobis[] doAnalysis(
			int itemCode1, int itemCode2, TmsDataset tmsDataSet, boolean isCorrectedValue) {
		
		// get raw data
		TmsDataRecord[] records = tmsDataSet.getTmsDataRecords();
		int recordCount = records.length;
		if (recordCount == 0) {
			return null;
		}
		
		// create dataset
		TmsDataCell4Mahalanobis[] datasetBuffer = new TmsDataCell4Mahalanobis[recordCount];
		int validDataCount = 0;
		long time;
		double data1_value, data2_value;
		for (int i=0 ; i<recordCount ; i++) {
			time = records[i].getTime();
			if (isCorrectedValue) {
				data1_value = records[i].getDataCell(itemCode1).getData_value();
				data2_value = records[i].getDataCell(itemCode2).getData_value();
			} else {
				data1_value = records[i].getDataCell(itemCode1).getOrigin_value();
				data2_value = records[i].getDataCell(itemCode2).getOrigin_value();
			}
			if (data1_value == -1.0D || data2_value == -1.0D) {
				continue;
			}
			datasetBuffer[validDataCount] = new TmsDataCell4Mahalanobis(time, itemCode1, data1_value, itemCode2, data2_value);
			validDataCount++;
		}
		if (validDataCount == 0) {
			return null;
		}
		
		// trim dataset
		TmsDataCell4Mahalanobis[] dataset = Arrays.copyOf(datasetBuffer, validDataCount);
		
		// compute average
		double sum_x = 0.0D, sum_y = 0.0D;
		for (int i=0 ; i<validDataCount ; i++) {
			sum_x += dataset[i].getData1_value();
			sum_y += dataset[i].getData2_value();
		}
		double[][] avg = new double[2][1];
		avg[0][0] = sum_x / (double)validDataCount;
		avg[1][0] = sum_y / (double)validDataCount;
		
		// compute covariance matrix
		double[][] valueset = new double[validDataCount][2];
		for (int i=0 ; i<validDataCount ; i++) {
			valueset[i][0] = dataset[i].getData1_value();
			valueset[i][1] = dataset[i].getData2_value();
		}
		
		Covariance covariance = new Covariance(valueset);
		RealMatrix covMatrix = covariance.getCovarianceMatrix();
		RealMatrix imverseCovMatrix = null;
		try {
			imverseCovMatrix = MatrixUtils.inverse(covMatrix);
		} catch (Exception e) {
			return null;
		}
		
		// compute mahalanobis distance
		double[][] datum = new double[2][1];
		RealMatrix distance;
		RealMatrix md_square;
		double md;
		for (int i=0 ; i<validDataCount ; i++) {
			datum[0][0] = valueset[i][0] - avg[0][0];
			datum[1][0] = valueset[i][1] - avg[1][0];
			distance = new BlockRealMatrix(datum);
			md_square = distance.transpose().multiply(imverseCovMatrix).multiply(distance);
			md = Math.sqrt(md_square.getEntry(0, 0));
			dataset[i].setMahalanobisDistance(md);
		}
		
		// sorting with md
		Arrays.sort(dataset, new Comparator<TmsDataCell4Mahalanobis>() {
			public int compare(TmsDataCell4Mahalanobis dataCell1, TmsDataCell4Mahalanobis dataCell2) {
				double x = dataCell1.getMahalanobisDistance();
				double y = dataCell2.getMahalanobisDistance();
				if (x > y) { return 1;}
				else if (x < y) { return -1;}
				else { return 0;}
			}
		});
		
		// compute qq-plot with chi-square distribution
		ChiSquaredDistribution chiSquareDist = new ChiSquaredDistribution((double) (validDataCount-1));
		for (int i=0 ; i<validDataCount ; i++) {
			double prob = (i+0.5D) / (double)validDataCount;
			double chiQ = chiSquareDist.inverseCumulativeProbability(prob);
			dataset[i].setImverseChiSquare(chiQ);
		}
		
		// re-sorting with time
		Arrays.sort(dataset, new Comparator<TmsDataCell4Mahalanobis>() {
			public int compare(TmsDataCell4Mahalanobis dataCell1, TmsDataCell4Mahalanobis dataCell2) {
				long x = dataCell1.getTime();
				long y = dataCell2.getTime();
				if (x > y) { return 1;}
				else if (x < y) { return -1;}
				else { return 0;}
			}
		});
		
		// Logging
		/**
		for (int i=0 ; i<validDataCount ; i++) {
			System.out.println(String.format("%.3f  %.3f", dataset[i].getMahalanobisDistance(), dataset[i].getImverseChiSquare()));
			i += 10;
		}
		*/
		
		// return
		return dataset;
		
	} // end of method
			


} // end of class
