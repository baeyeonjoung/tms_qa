/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier;

import java.util.Arrays;
import java.util.Vector;

import org.apache.commons.math3.distribution.FDistribution;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import com.snu.tms.qa.model.ruleset.CorrelationModel;
import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.RawDataCell;
import com.snu.tms.qa.model.ruleset.RulesetCorr;


/**
 * Outlier detection using Correlation (T-square statistics)
 * 
 */
public class OutlierDetectionUsingCorrelation {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	public OutlierDetectionUsingCorrelation() {}

	//==========================================================================
	// static Methods
	//==========================================================================
	/**
	 * compute bivariate correlation r-square and p-value
	 */
	public static CorrelationModel[] getCorrelationModels(DetectionPatternDataset patternDataset) {
		
		// create corrModel
		Vector<CorrelationModel> models = new Vector<CorrelationModel>();
		
		// compute correlation r-square for each two-items
		int[] items = patternDataset.getItems();
		CorrelationModel model;
		for (int i=0 ; i<items.length ; i++) {
			for (int j=i+1 ; j<items.length ; j++) {
				model = computeCorreation(patternDataset, items[i], items[j]);
				models.add(model);
			}
		}
		
		// return
		return models.toArray(new CorrelationModel[models.size()]);
		
	} // end of method
	
	/**
	 * compute r-square of bi-variate correlation
	 */
	public static CorrelationModel computeCorreation(
			DetectionPatternDataset  patternDataset,
			int  item1,
			int  item2) {
		
		// set data
		DetectionRecord[] dataRecords = patternDataset.getAllDetectionRecords();
		double[][] rawDataset = new double[dataRecords.length][2];
		int index = 0;
		for (int i=0 ; i<rawDataset.length ; i++) {
			rawDataset[index][0] = dataRecords[i].getRawDataCell(item1).getData_value();
			rawDataset[index][1] = dataRecords[i].getRawDataCell(item2).getData_value();
			if (rawDataset[index][0] != -1.0D && rawDataset[index][1] != -1.0D) {
				index++;
			}
		}
		double[][] dataset = Arrays.copyOf(rawDataset, index);
		
		// compute R-square and p-value of correlation
		double rValue, pValue;
		try {
			PearsonsCorrelation pearsonsCorrelation = new PearsonsCorrelation(dataset);
			rValue = pearsonsCorrelation.getCorrelationMatrix().getEntry(0, 1);
			pValue  = pearsonsCorrelation.getCorrelationPValues().getEntry(0, 1);
		} catch(Exception e) {
			rValue = 0.0D;
			pValue = 0.0D;
		}
		
		// create model
		CorrelationModel model = new CorrelationModel(
				item1,
				item2,
				rValue * rValue,  // squared PersonCorrelation coefficient equal to R-Square in regression
				pValue);
		return model;
		
	} // end of method
	
	
	/**
	 * Do Outlier detection using Correction
	 */
	public static RulesetCorr doOutlierDetectionWithPatternData(
			DetectionPatternDataset patternDataset,
			int[]  items,
			double rSquare,
			double confidenceLevel) {
		
		// check null
		if (patternDataset == null) {
			return null;
		}
		
		// get all records
		DetectionRecord[] dataRecords = patternDataset.getAllDetectionRecords();
		double[][] dataset = new double[dataRecords.length][];
		RawDataCell[] rawDataCells;
		double[] data;
		for (int i=0 ; i<dataRecords.length ; i++) {
			rawDataCells = dataRecords[i].getRawDataCell(items);
			data = new double[items.length];
			for (int j=0 ; j<rawDataCells.length ; j++) {
				data[j] = rawDataCells[j].getData_value();
			}
			dataset[i] = data;
		}
		
		// compute mahalanobis distance
		//
		// set data size
		int M = items.length;
		int N = dataset.length;
		
		// calculate mean
		double[] mn = new double[M];
		double[] sigma = new double[M];
		Arrays.fill(mn, 0.0D);
		Arrays.fill(sigma, 0.0D);
		
		DescriptiveStatistics stats;
		for (int i=0 ; i<M ; i++) {
			stats = new DescriptiveStatistics();
			for (int j=0 ; j<N ; j++) {
				stats.addValue(dataset[j][i]);
			}
			mn[i] = stats.getMean();
			sigma[i] = stats.getStandardDeviation();
		}
		
		// subtract off the mean for each dimension
		double[][] nDataset = new double[N][M];
		for (int i=0 ; i<N ; i++) {
			for (int j=0 ; j<M ; j++) {
				nDataset[i][j] = (dataset[i][j] - mn[j]) / sigma[j];
			}
		}
		
		// calculate covariance matrix and it's imverse
		Covariance cv = new Covariance(nDataset);
		RealMatrix mCovariance = cv.getCovarianceMatrix();
		
		RealMatrix imverseCovMatrix = null;
		try {
			imverseCovMatrix = MatrixUtils.inverse(mCovariance);
		} catch (Exception e) {
			return null;
		}
		
		// compute threshold of t-square statistics
		double threhold = computeThrehold(
				dataRecords.length, items.length, confidenceLevel);
		
		// set ruleset
		RulesetCorr ruleset = new RulesetCorr(
				-1,
				patternDataset.getPatternId(),
				items,
				rSquare,
				mCovariance.getData(),
				imverseCovMatrix.getData(),
				mn,
				sigma);
		ruleset.setConfidenceLevel(confidenceLevel);
		ruleset.setThresholds(new double[]{threhold, 0.0D});
		
		// return
		return ruleset;
		
	} // end of method
	
	/**
	 * compute threshold of t-square statistics
	 */
	public static double computeThrehold(int dataCount, int itemCount, double confidenceLevel) {
		
		// set parameters
		double M = (double)itemCount;
		double N = (double) dataCount;
		
		// get F-distribution
		FDistribution fDist = new FDistribution(M, (N-M));
		double f = fDist.inverseCumulativeProbability(confidenceLevel);
		
		// compute threshold
		double threshold = (M * (N - 1.0D) * (N + 1.0D)) / (N * (N - M)) * f;
		
		return threshold;
		
	} // end of method
	

	
	//==========================================================================
	// Methods
	//==========================================================================

} // end of class
