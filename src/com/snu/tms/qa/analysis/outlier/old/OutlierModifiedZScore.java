/**
 * BAT-AEL Analysis
 * 
 * Objectives : Determining the BAT-AEL range with analysis of TMS data
 * Writer	 : Yeonjoung, Bae
 * Edit date  : Setember 10, 2014
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier.old;

import java.util.Arrays;

/**
 * BAT-AEL Analysis Agent
 * 
 */
public class OutlierModifiedZScore {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private  final  static  double  LIMIT_Z_SCORE = 3.5D;
	private  final  static  double  E_MAD = 0.6745D;
	
	
	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Do analysis for computing outlier lower and upper fence
	 */
	public static double[] doComputingOutlierFences(
			double[] sortedValues,
			double   mean,
			double   stdev
			) {
		
		// init
		double[] outlierFence = new double[2];
		Arrays.fill(outlierFence, 0.0D);
		
		// Get median
		double median = Percentile.getPercentile(sortedValues, 50);
		
		// Compute MAD (median of the absolute deviation of the median)
		double[]  madValues = new double[sortedValues.length];
		for (int i=0 ; i<madValues.length ; i++) {
			madValues[i] = Math.abs(sortedValues[i] - median);
		}
		Arrays.sort(madValues);
		double mad = Percentile.getPercentile(madValues, 50);
		
		// compute outlier
		outlierFence[0] = Math.max(median - LIMIT_Z_SCORE / E_MAD * mad, 0);
		outlierFence[1] = median + LIMIT_Z_SCORE / E_MAD * mad;

		return outlierFence;
		
	} // end of method
	
} // end of class
