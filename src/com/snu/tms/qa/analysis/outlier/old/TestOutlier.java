/**
 * BAT-AEL Analysis
 * 
 * Objectives : Determining the BAT-AEL range with analysis of TMS data
 * Writer	 : Yeonjoung, Bae
 * Edit date  : Setember 10, 2014
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier.old;

import org.apache.commons.math.stat.StatUtils;


/**
 * BAT-AEL Analysis Agent
 * 
 */
public class TestOutlier {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Test for Outlier detection library
	 */
	public static void main(String[] args) {
		
		// Set data
		double[] values = { 
				3.2D, 3.4D, 3.7D, 3.7D, 3.8D, 
				3.9D, 4.0D, 4.0D, 4.1D, 4.2D, 
				4.7D, 4.8D, 14.0D, 15.0D };
		
		double mean = StatUtils.mean(values);
		double stdev = Math.sqrt(StatUtils.variance(values));
		
		double u_percentile95 = OutlierPercentile.doComputingOutlierFences(values, 95);
		double u_percentile99 = OutlierPercentile.doComputingOutlierFences(values, 99);
		
		System.out.println(String.format("percentile 95 [U] : [%.2f]", u_percentile95));
		System.out.println(String.format("percentile 99 [U] : [%.2f]", u_percentile99));
		
		
		double[] tDist95 = OutlierModifiedThompsonTau.doComputingOutlierFences(values, mean, stdev, 0.05D);
		double[] tDist99 = OutlierModifiedThompsonTau.doComputingOutlierFences(values, mean, stdev, 0.01D);
		System.out.println(String.format("[L, U] T-DIST 95 : [%.2f, %.2f]", tDist95[0], tDist95[1]));
		System.out.println(String.format("[L, U] T-DIST 99 : [%.2f, %.2f]", tDist99[0], tDist99[1]));
		
		double[] SD95 = OutlierStandardDeviation.doComputingOutlierFences(values, mean, stdev, 2.0D);
		double[] SD99 = OutlierStandardDeviation.doComputingOutlierFences(values, mean, stdev, 3.0D);
		System.out.println(String.format("[L, U] SD 95   : [%.2f, %.2f]", SD95[0], SD95[1]));
		System.out.println(String.format("[L, U] SD 99.7 : [%.2f, %.2f]", SD99[0], SD99[1]));
		
		double[] zScore = OutlierZScore.doComputingOutlierFences(values, mean, stdev);
		System.out.println(String.format("[L, U] Z-Score : [%.2f, %.2f]", zScore[0], zScore[1]));
		
		double[] zMScore = OutlierModifiedZScore.doComputingOutlierFences(values, mean, stdev);
		System.out.println(String.format("[L, U] Modified Z-Score : [%.2f, %.2f]", zMScore[0], zMScore[1]));

		double[] BoxPlot15 = OutlierTukeyBoxPlot.doComputingOutlierFences(values, 1.5D);
		double[] BoxPlot30 = OutlierTukeyBoxPlot.doComputingOutlierFences(values, 3.0D);
		System.out.println(String.format("[L, U] BoxPlot 1.5 : [%.2f, %.2f]", BoxPlot15[0], BoxPlot15[1]));
		System.out.println(String.format("[L, U] BoxPlot 3.0 : [%.2f, %.2f]", BoxPlot30[0], BoxPlot30[1]));

		double[] AdjustBoxPlot = OutlierModifiedBoxPlot.doComputingOutlierFences(values);
		System.out.println(String.format("[L, U] Adjust-BoxPlot 1.5 : [%.2f, %.2f]", AdjustBoxPlot[0], AdjustBoxPlot[1]));

		double[] MADe2 = OutlierMADe.doComputingOutlierFences(values, 2.0D);
		double[] MADe3 = OutlierMADe.doComputingOutlierFences(values, 3.0D);
		System.out.println(String.format("[L, U] MADe 2 : [%.2f, %.2f]", MADe2[0], MADe2[1]));
		System.out.println(String.format("[L, U] MADe 3 : [%.2f, %.2f]", MADe3[0], MADe3[1]));

		double[] MedianRule = OutlierMedianRule.doComputingOutlierFences(values);
		System.out.println(String.format("[L, U] MedianRule : [%.2f, %.2f]", MedianRule[0], MedianRule[1]));

	} // end of method
	
	
	//==========================================================================
	// Methods
	//==========================================================================



	
} // end of class
