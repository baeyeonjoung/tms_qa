/**
 * BAT-AEL Analysis
 * 
 * Objectives : Determining the BAT-AEL range with analysis of TMS data
 * Writer	 : Yeonjoung, Bae
 * Edit date  : Setember 10, 2014
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier.old;

import java.util.Arrays;

/**
 * BAT-AEL Analysis Agent
 * 
 */
public class OutlierModifiedBoxPlot {

	//==========================================================================
	// Static Fields
	//==========================================================================
	public final static double  INNER_FENCE = 1.5D;
	
	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Do analysis for computing outlier lower and upper fence
	 */
	public static double[] doComputingOutlierFences(
			double[] sortedValues
	) {
		
		// init
		double[] outlierFence = new double[2];
		Arrays.fill(outlierFence, 0.0D);
				
		// Get quartile
		double  q1 = Percentile.getPercentile(sortedValues, 25);
		double  q2 = Percentile.getPercentile(sortedValues, 50);
		double  q3 = Percentile.getPercentile(sortedValues, 75);
		
		// Sorting
		//Arrays.sort(sortedValues);
		int medianIndex = sortedValues.length -1;
		for (int i=0 ; i<sortedValues.length ; i++) {
			if (sortedValues[i] > q2) {
				medianIndex = i;
				break;
			}
		}
		
		// Compute MC
		int[] mcValues = new int[2001];
		Arrays.fill(mcValues, 0);
		
		double mcValue = 0.0D;
		int totalSum = 0;
		for (int i=0 ; i<medianIndex ; i++) {
			for (int j=medianIndex ; j<sortedValues.length ; j++) {
				if (sortedValues[i] == q2) {
					continue;
				}
				mcValue = ((sortedValues[j] - q2) - (q2 - sortedValues[i])) / (sortedValues[j] - sortedValues[i]);
				int index = (int) (( mcValue + 1.0D ) * 1000.0D);
				mcValues[index] ++;
				totalSum ++;
			}
		}
		
		// Get half point
		int halfTotal = totalSum / 2;
		int countSum = 0;
		int index = 0;
		for (index=0 ; index<mcValues.length ; index++) {
			countSum += mcValues[index];
			if (countSum > halfTotal) {
				break;
			}
		}
		double mc = (index / 1000.0D) - 1.0D;

		//System.out.println(q1 + ", " + q2 + ", " + q3 + ", " + index + ", " + mc);
		
		// compute outlier
		if (mc >= 0) {
			outlierFence[0] = Math.max( q1 - 1.5D * Math.exp(-3.5D * mc) * (q3-q1), 0);
			outlierFence[1] = q1 + 1.5D * Math.exp(4.0D * mc) * (q3-q1);
		} else {
			outlierFence[0] = Math.max( q1 - 1.5D * Math.exp(-4.0D * mc) * (q3-q1), 0);
			outlierFence[1] = q1 + 1.5D * Math.exp(3.5D * mc) * (q3-q1);

		}
		return outlierFence;	
		
	} // end of method
	
} // end of class
