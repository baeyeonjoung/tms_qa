/**
 * BAT-AEL Analysis
 * 
 * Objectives : Determining the BAT-AEL range with analysis of TMS data
 * Writer	 : Yeonjoung, Bae
 * Edit date  : Setember 10, 2014
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier.old;

import java.util.Arrays;

/**
 * BAT-AEL Analysis Agent
 * 
 */
public class OutlierStandardDeviation {

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Do analysis for computing outlier lower and upper fence
	 */
	public static double[] doComputingOutlierFences(
			double[] sortedValues,
			double   mean,
			double   stdev,
			double   multiplier
			) {
		
		// init
		double[] outlierFence = new double[2];
		Arrays.fill(outlierFence, 0.0D);
		
		// compute outlier
		outlierFence[0] = Math.max(mean - multiplier * stdev, 0);
		outlierFence[1] = mean + multiplier * stdev;

		return outlierFence;
		
	} // end of method
	
} // end of class
