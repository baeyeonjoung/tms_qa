/**
 * BAT-AEL Analysis
 * 
 * Objectives : Determining the BAT-AEL range with analysis of TMS data
 * Writer	 : Yeonjoung, Bae
 * Edit date  : Setember 10, 2014
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier.old;

import java.util.Arrays;


/**
 * BAT-AEL Analysis Agent
 * 
 */
public class OutlierMedianRule {

	//==========================================================================
	// Static Fields
	//==========================================================================
	public final static double  FENCE = 2.3D;
	
	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Do analysis for computing outlier lower and upper fence
	 */
	public static double[] doComputingOutlierFences(
			double[] sortedValues
	) {
		
		// init
		double[] outlierFence = new double[2];
		Arrays.fill(outlierFence, 0.0D);
				
		// Get quartile
		double  q1 = Percentile.getPercentile(sortedValues, 25);
		double  q2 = Percentile.getPercentile(sortedValues, 50);
		double  q3 = Percentile.getPercentile(sortedValues, 75);
		
		// compute outlier
		outlierFence[0] = Math.max(q2 - FENCE * (q3 - q1), 0);
		outlierFence[1] = q2 + FENCE * (q3 - q1);

		return outlierFence;	
		
	} // end of method
	
} // end of class
