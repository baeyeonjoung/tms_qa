/**
 * BAT-AEL Analysis
 * 
 * Objectives : Determining the BAT-AEL range with analysis of TMS data
 * Writer	 : Yeonjoung, Bae
 * Edit date  : Setember 10, 2014
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier.old;

import java.util.Arrays;

/**
 * BAT-AEL Analysis Agent
 * 
 */
public class OutlierZScore {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private  final  static  double  LIMIT_Z_SCORE = 3.0D;
	
	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Do analysis for computing outlier lower and upper fence
	 */
	public static double[] doComputingOutlierFences(
			double[] sortedValues,
			double   mean,
			double   stdev
			) {
		
		// init
		double[] outlierFence = new double[2];
		Arrays.fill(outlierFence, 0.0D);
		
		// compute outlier
		outlierFence[0] = Math.max(mean - LIMIT_Z_SCORE * stdev, 0);
		outlierFence[1] = mean + LIMIT_Z_SCORE * stdev;

		return outlierFence;
		
	} // end of method
	
} // end of class
