/**
 * BAT-AEL Analysis
 * 
 * Objectives : Determining the BAT-AEL range with analysis of TMS data
 * Writer	 : Yeonjoung, Bae
 * Edit date  : Setember 10, 2014
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier.old;

import java.util.Arrays;


/**
 * BAT-AEL Analysis Agent
 * 
 */
public class OutlierTukeyBoxPlot {

	//==========================================================================
	// Static Fields
	//==========================================================================
	public final static double  INNER_FENCE = 1.5D;
	public final static double  OUTER_FENCE = 3.0D;
	
	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Do analysis for computing outlier lower and upper fence
	 */
	public static double[] doComputingOutlierFences(
			double[] sortedValues,
			double   fenceDistance
	) {
		
		// init
		double[] outlierFence = new double[2];
		Arrays.fill(outlierFence, 0.0D);
				
		// Get quartile
		double  q1 = Percentile.getPercentile(sortedValues, 25);
		double  q3 = Percentile.getPercentile(sortedValues, 75);
		
		// compute outlier
		outlierFence[0] = Math.max(q1 - fenceDistance * (q3 - q1), 0);
		outlierFence[1] = q3 + fenceDistance * (q3 - q1);

		return outlierFence;	
		
	} // end of method
	
} // end of class
