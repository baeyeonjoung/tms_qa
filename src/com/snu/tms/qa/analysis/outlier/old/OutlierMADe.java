/**
 * BAT-AEL Analysis
 * 
 * Objectives : Determining the BAT-AEL range with analysis of TMS data
 * Writer	 : Yeonjoung, Bae
 * Edit date  : Setember 10, 2014
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier.old;

import java.util.Arrays;


/**
 * BAT-AEL Analysis Agent
 * 
 */
public class OutlierMADe {

	//==========================================================================
	// Static Fields
	//==========================================================================
	public final static double  INNER_FENCE = 2.0D;
	public final static double  OUTER_FENCE = 3.0D;
	
	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Do analysis for computing outlier lower and upper fence
	 */
	public static double[] doComputingOutlierFences(
			double[] sortedValues,
			double   fenceDistance
	) {
		
		// init
		double[] outlierFence = new double[2];
		Arrays.fill(outlierFence, 0.0D);
				
		// Get quartile
		double  q2 = Percentile.getPercentile(sortedValues, 50);
		
		// Get MAD
		double[] madValues = new double[sortedValues.length];
		for (int i=0 ; i<sortedValues.length ; i++) {
			madValues[i] = Math.abs(sortedValues[i] - q2);
		}
		
		Arrays.sort(madValues);
		double mad_e = Percentile.getPercentile(madValues, 50) * 1.483D;
		
		// compute outlier
		outlierFence[0] = Math.max(q2 - fenceDistance * mad_e, 0);
		outlierFence[1] = q2 + fenceDistance * mad_e;

		return outlierFence;	
		
	} // end of method
	
} // end of class
