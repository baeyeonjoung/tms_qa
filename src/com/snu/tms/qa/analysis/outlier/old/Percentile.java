/**
 * BAT-AEL Analysis
 * 
 * Objectives : Determining the BAT-AEL range with analysis of TMS data
 * Writer	 : Yeonjoung, Bae
 * Edit date  : Setember 10, 2014
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier.old;

/**
 * BAT-AEL Analysis Agent
 * 
 */
public class Percentile {

	//==========================================================================
	// Static Fields
	//==========================================================================
	/**
	 * Test
	 */
	public static void main(String[] args) {
		
		double[] data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		
		double libraryPercentile = org.apache.commons.math.stat.StatUtils.percentile(data, 56);
		double thisCodePercentile = getPercentile(data, 56);
		
		System.out.println(libraryPercentile + ", " + thisCodePercentile);
		
	}
	
	
	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Get Percentile
	 * data must be sorted
	 */
	public static double getPercentile(
			double[] sortedValues,
			double   percentile
	) {
		
		double index = percentile / 100.0D * (sortedValues.length-1);
		int lower = (int)Math.floor(index);
		if(lower < 0) { 
			return sortedValues[0];
		}
		if(lower >= sortedValues.length-1) {
			return sortedValues[sortedValues.length-1];
		}
		double fraction = index-lower;
		
		// linear interpolation
		double result = sortedValues[lower] + fraction*(sortedValues[lower+1]-sortedValues[lower]);
		return result;

	} // end of method
	
} // end of class

