/**
 * BAT-AEL Analysis
 * 
 * Objectives : Determining the BAT-AEL range with analysis of TMS data
 * Writer	 : Yeonjoung, Bae
 * Edit date  : Setember 10, 2014
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier.old;


/**
 * BAT-AEL Analysis Agent
 * 
 */
public class OutlierPercentile {

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Do analysis for computing outlier lower and upper fence
	 */
	public static double doComputingOutlierFences(
			double[] sortedValues,
			double   percentile
			) {
		
		// Percentile
		return  Percentile.getPercentile(sortedValues, percentile);	
		
	} // end of method
	
} // end of class
