/**
 * BAT-AEL Analysis
 * 
 * Objectives : Determining the BAT-AEL range with analysis of TMS data
 * Writer	 : Yeonjoung, Bae
 * Edit date  : Setember 10, 2014
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier.old;

import java.util.Arrays;

import org.apache.commons.math.distribution.TDistributionImpl;


/**
 * BAT-AEL Analysis Agent
 * 
 */
public class OutlierModifiedThompsonTau {

	//==========================================================================
	// Static Fields
	//==========================================================================
	
	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Do analysis for computing outlier lower and upper fence
	 */
	public static double[] doComputingOutlierFences(
			double[] sortedValues,
			double   mean,
			double   stdev,
			double   alpha
			) {
		
		// DatsCount
		int  dataCount = sortedValues.length;
		double tDistance = 0.0D;
		double tau = 0.0D;
		double[] outlierFence = new double[2];
		Arrays.fill(outlierFence, 0.0D);
		
		// Get T-Distance
		try {
			TDistributionImpl tDistribution = new TDistributionImpl(dataCount - 2);
			tDistance = tDistribution.inverseCumulativeProbability(1.0D - alpha/2.0D);
			tau = (tDistance * (dataCount-1)) / (Math.sqrt(dataCount) * Math.sqrt(dataCount-2+tDistance*tDistance));
		} catch (Exception ee) {
			return outlierFence;
		}
		
		// compute outlier
		//System.out.println("mean=" + mean + ", stdev=" + stdev + ", t=" + tDistance);
		outlierFence[0] = Math.max(mean - stdev * tau, 0);
		outlierFence[1] = mean + stdev * tau;

		return outlierFence;
		
	} // end of method
	
} // end of class
