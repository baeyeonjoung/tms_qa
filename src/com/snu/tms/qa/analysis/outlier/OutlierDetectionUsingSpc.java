/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.RulesetSpc;

/**
 * Outlier detection using Correlation (SPC: Statistic Process Control)
 * 
 */
public class OutlierDetectionUsingSpc {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	public OutlierDetectionUsingSpc() {}

	//==========================================================================
	// static Methods
	//==========================================================================
	/**
	 * Do Outlier detection using Correction
	 */
	public static RulesetSpc doOutlierDetectionWithPatternData(
			DetectionPatternDataset patternDataset,
			int item, double confidenceLevel) {
		
		// check null
		if (patternDataset == null) {
			return null;
		}
		
		// get all records
		DetectionRecord[] dataRecords = patternDataset.getAllDetectionRecords();

		// Get Percent of Difference and it's average
		DescriptiveStatistics stats = new DescriptiveStatistics();
		for (int i=0 ; i<dataRecords.length ; i++) {
			stats.addValue(dataRecords[i].getRawDataCell(item).getData_value());
		}
		
		// Get Percent of Difference and it's average
		double mean  = stats.getMean();
		double sigma = stats.getStandardDeviation();
		
		// compute threshold of confidence interval
		double[] threholds = computeThrehold(mean, sigma, confidenceLevel);

		// set ruleset
		RulesetSpc ruleset = new RulesetSpc(item);
		ruleset.setConfidenceLevel(confidenceLevel);
		ruleset.setMean(mean);
		ruleset.setSigma(sigma);
		ruleset.setThresholds(threholds);
		
		// return
		return ruleset;
		
	} // end of method
	
	/**
	 * compute threshold of confidence interval
	 */
	public static double[] computeThrehold(double mean, double sigma, double confidenceLevel) {
		
		// Get Z-score
		double c = (1.0D + confidenceLevel) / 2.0D;
		NormalDistribution normalDist = new NormalDistribution();
		double z = normalDist.inverseCumulativeProbability(c);
		
		// Get upper limit and lower limit
		double[] thresholds = new double[2];
		thresholds[0] = Math.max( (mean + (z * sigma)), 0.0D );
		thresholds[1] = mean - (z * sigma);
		
		return thresholds;
		
	} // end of method
	

	
	//==========================================================================
	// Methods
	//==========================================================================

} // end of class
