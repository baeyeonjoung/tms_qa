/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math3.distribution.TDistribution;
import org.apache.commons.math3.stat.StatUtils;

import com.snu.tms.qa.model.TmsDataCell;


/**
 * Walsh's outlier test
 * 
 */
public class DoAnalysisOutlierRosner {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(DoAnalysisOutlierRosner.class);

	//==========================================================================
	// Fields
	//==========================================================================

	//==========================================================================
	// Constructor
	//==========================================================================
	public DoAnalysisOutlierRosner() {}
	
	//==========================================================================
	// Test main() Method
	//==========================================================================
	/**
	 * Test source
	 */
	public static void main(String[] args) {
		
		TmsDataCell[] data = new TmsDataCell[300];
		for (int i=0 ; i<290 ; i++) {
			data[i] = new TmsDataCell(1, 100D + Math.random()*10D);
		}
		data[290] = new TmsDataCell(1, 2000.1D);
		data[291] = new TmsDataCell(1, 3000.2D);
		data[292] = new TmsDataCell(1, 4000.1D);
		data[293] = new TmsDataCell(1, 5000.1D);
		data[294] = new TmsDataCell(1, 7000.1D);
		data[295] = new TmsDataCell(1, 4000.1D);
		data[296] = new TmsDataCell(1, 7000.1D);
		data[297] = new TmsDataCell(1, 1.1D);
		data[298] = new TmsDataCell(1, 2.1D);
		data[299] = new TmsDataCell(1, 0.1D);
		
		double[] limits = doOutlierFiltering(data, false, 0.05D);
		System.out.println(limits[0] + ",  " + limits[1]);
		
		for (int i=0 ; i<data.length ; i++) {
			System.out.println(String.format("i=%-3d, value=%-6.3f, outlier=%s", 
					i,
					data[i].getData_value(),
					data[i].isOutlier()?"TRUE":"FLASE"));
		}
		
		
	} // end of method
	
	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * Do outlier Filtering in case of data is not normal distributed.
	 * In this method use Rosner's method (in recommended by EPA)
	 */
	public static double[] doOutlierFiltering(
			TmsDataCell[] tmsDataCell, 
			boolean isSorted, 
			double significanceLevel) {
		
		// Check null
		if (tmsDataCell == null) {
			logger.debug("Seleted data set is null.");
			return null;
		}
		
		// Sorting
		if (!isSorted) {
			Arrays.sort(tmsDataCell, DoAnalysisOutlier.TmsDataCellComparator);
		}
		
		// compute general stats
		double[] data = new double[tmsDataCell.length];
		for (int i=0 ; i<tmsDataCell.length ; i++) {
			data[i] = tmsDataCell[i].getData_value();
		}
		double mean  = StatUtils.mean(data);
		double stdev = Math.sqrt(StatUtils.variance(data));
		int n = tmsDataCell.length;
		
		// Get T-Distance
		double degreeOfFreedom = (double) (n - 2);
		TDistribution tDistribution = new TDistribution(degreeOfFreedom);
		double tDistance = tDistribution.inverseCumulativeProbability(1.0D - significanceLevel/2.0D);
		double tau = (tDistance * (double)(n-1)) / (Math.sqrt((double)n) * Math.sqrt((double)n-2.0D+tDistance*tDistance));
		
		// compute outlier
		//System.out.println("mean=" + mean + ", stdev=" + stdev + ", t=" + tDistance);
		double[] limits = new double[2];
		limits[0] = Math.max(mean - stdev * tau, 0);
		limits[1] = mean + stdev * tau;
		
		// Do rosner's iterator for finding outlier
		// TO-DO :: 2015-12-27  by Yeonjouing, Bae
		//
		
		// Filtering
		for (int i=0 ; i<tmsDataCell.length ; i++) {
			
			if (tmsDataCell[i].getData_value() < limits[0] || tmsDataCell[i].getData_value() > limits[1]) {
				tmsDataCell[i].setOutlier(true);
			}
		}

		return limits;
		
	} // end of method
	

} // end of class
