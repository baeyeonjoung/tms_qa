/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier;

import java.util.Arrays;

import org.apache.commons.math3.distribution.FDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.OutlierPcaData;
import com.snu.tms.qa.model.ruleset.PcaResultModel;
import com.snu.tms.qa.model.ruleset.RawDataCell;


/**
 * Outlier detection using PCA (T-square statistics and SPE)
 * 
 */
public class OutlierDetectionUsingPca_OLD {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	public OutlierDetectionUsingPca_OLD() {}

	//==========================================================================
	// static Methods
	//==========================================================================
	/**
	 * Do Outlier detection using PCA (T-square statistics and SPE)
	 */
	public static OutlierPcaData doOutlierDetectionWithPatternData(
			DetectionPatternDataset patternDataset,
			double tssConfidanceLevel,
			double speConfidanceLevel) {
		
		// check null
		if (patternDataset == null) {
			return null;
		}
		
		// get PCA result
		PcaResultModel pcaResultModel = patternDataset.getPcaResultModel();
		int pcCount = pcaResultModel.getPcCount();
		
		// get all records and make projected point array
		int[] items = patternDataset.getItems();
		DetectionRecord[] dataRecords = patternDataset.getAllDetectionRecords();
		double[][] dataset = new double[dataRecords.length][];
		RawDataCell[] rawDataCells;
		double[] data;
		for (int i=0 ; i<dataRecords.length ; i++) {
			rawDataCells = dataRecords[i].getRawDataCell(items);
			data = new double[items.length];
			for (int j=0 ; j<rawDataCells.length ; j++) {
				data[j] = rawDataCells[j].getData_value();
			}
			dataset[i] = pcaResultModel.getProjectionValues(data);
		}
		
		// compute mahalanobis distance
		double[] md = computeMahalanobisDistance(dataset);
		/**
		for (int i=0 ; i<md.length ; i++) {
			System.out.println(md[i]);
		}
		*/
		
		// compute threshold of t-square statistics
		double tssThrehold = computeTssThrehold(
				dataRecords.length, pcCount, tssConfidanceLevel);
		//System.out.println("====  " + tssThrehold);
		
		// compute SPE(squared Prediction Error)
		double[] q = computeSpeDistance(dataset, pcaResultModel.getEigenVector());
		/**
		for (int i=0 ; i<md.length ; i++) {
			System.out.println(q[i]);
		}
		*/
		
		// compute threshold of SPE
		double speThrehold = computeSpeThrehold(
				pcaResultModel.getEigenValues(), speConfidanceLevel);
		//System.out.println("====  " + speThrehold);
		
		// return 
		OutlierPcaData outlierData = new OutlierPcaData(
				dataRecords.length,
				md,
				tssThrehold,
				tssConfidanceLevel,
				q,
				speThrehold,
				speConfidanceLevel);
		
		return outlierData;
		
	} // end of method
	
	/**
	 * Get Mahalanobis distance
	 * Parameters : input [N*M], M: data field count, N: data record count
	 */
	public static double[] computeMahalanobisDistance(double[][] input) {
		
		// check
		if (input == null || input.length == 0) {
			return null;
		}
		
		// set data size
		int M = input[0].length;
		int N = input.length;
		
		// calculate mean
		double[] mn = new double[M];
		double[] sigma = new double[M];
		Arrays.fill(mn, 0.0D);
		Arrays.fill(sigma, 0.0D);
		
		DescriptiveStatistics stats;
		for (int i=0 ; i<M ; i++) {
			stats = new DescriptiveStatistics();
			for (int j=0 ; j<N ; j++) {
				stats.addValue(input[j][i]);
			}
			mn[i] = stats.getMean();
			sigma[i] = stats.getStandardDeviation();
		}
		
		// subtract off the mean for each dimension
		double[][] dataset = new double[N][M];
		for (int i=0 ; i<N ; i++) {
			for (int j=0 ; j<M ; j++) {
				dataset[i][j] = (input[i][j] - mn[j]) / sigma[j];
			}
		}
		
		// calculate covariance matrix and it's imverse
		Covariance cv = new Covariance(dataset);
		RealMatrix mCovariance = cv.getCovarianceMatrix();
		
		RealMatrix imverseCovMatrix = null;
		try {
			imverseCovMatrix = MatrixUtils.inverse(mCovariance);
		} catch (Exception e) {
			return null;
		}
		
		// compute mahalanobis distance
		double[] md = new double[N];
		double[][] datum = new double[1][M];
		RealMatrix point;
		RealMatrix md_square;
		for (int i=0 ; i<N ; i++) {
			for (int j=0 ; j<M ; j++) {
				datum[0][j] = dataset[i][j];
			}
			point = new BlockRealMatrix(datum);
			md_square = point.multiply(imverseCovMatrix).multiply(point.transpose());
			md[i] = md_square.getEntry(0, 0);
		}
		
		// return 
		return md;
		
	} // end of method
	
	/**
	 * compute threshold of t-square statistics
	 */
	public static double computeTssThrehold(int dataCount, int itemCount, double confidenceLevel) {
		
		// set parameters
		double M = (double)itemCount;
		double N = (double) dataCount;
		
		// get F-distribution
		FDistribution fDist = new FDistribution(M, (N-M));
		double f = fDist.inverseCumulativeProbability(confidenceLevel);
		
		// compute threshold
		double threshold = (M * (N - 1.0D) * (N + 1.0D)) / (N * (N - M)) * f;
		
		return threshold;
		
	} // end of method
	
	/**
	 * Get SPE(squared prediction error)
	 * Parameters : input [N*M], M: data field count, N: data record count
	 */
	public static double[] computeSpeDistance(double[][] input, double[][] eigenVector) {
		
		// check
		if (input == null || input.length == 0 || eigenVector == null) {
			return null;
		}
		
		// set parameters
		int M = input[0].length;
		int N = input.length;

		// compute SPE
		double[] D = new double[N];
		double[][] datum = new double[1][M];
		RealMatrix point;
		RealMatrix qPoint;
		BlockRealMatrix mEigenVector = new BlockRealMatrix(eigenVector);
		for (int i=0 ; i<N ; i++) {
			for (int j=0 ; j<M ; j++) {
				datum[0][j] = input[i][j];
			}
			point = new BlockRealMatrix(datum);
			qPoint = point.multiply(mEigenVector);
			D[i] = Math.abs(qPoint.multiply(point.transpose()).getEntry(0, 0));
		}
		
		// return 
		return D;
		
	} // end of method
	
	/**
	 * compute threshold of SPE(squared prediction error)
	 */
	public static double computeSpeThrehold(double[] eigenValue, double confidenceLevel) {
		
		// set parameters
		double N = (double)eigenValue.length;
		double theta1 = 0.0D;
		double theta2 = 0.0D;
		double theta3 = 0.0D;;
		for (int i=0 ; i<N ; i++) {
			theta1 += eigenValue[i];
			theta2 += Math.pow(eigenValue[i], 2.0D);
			theta3 += Math.pow(eigenValue[i], 3.0D);
		}
		
		// get Normal-distribution
		NormalDistribution nDist = new NormalDistribution();
		double ca = nDist.inverseCumulativeProbability(confidenceLevel);
		
		// compute threshold
		double h0 = 1.0D - (2.0D * theta1 * theta3) / (3.0D * theta2 * theta2);
		double q1 =ca * Math.sqrt(2.0D * theta2 * h0 * h0) / theta1;
		double q2 = theta2 * h0 * (h0 - 1.0D) / (theta1 * theta1);
		double q3 = 1.0D;
		double q = theta1 * Math.pow((q1 + q2 + q3), (1.0D / h0));
		
		// return 
		return q;
		
	} // end of method
	
	
	//==========================================================================
	// Methods
	//==========================================================================

} // end of class
