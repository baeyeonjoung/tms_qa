/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.RulesetPd;

/**
 * Outlier detection using Percent of difference 
 *   between original value and after correction value
 * 
 */
public class OutlierDetectionUsingPd {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	public OutlierDetectionUsingPd() {}

	//==========================================================================
	// static Methods
	//==========================================================================
	/**
	 * Do Outlier detection using Percent of difference 
     *   between original value and after correction value
	 */
	public static RulesetPd doOutlierDetectionWithPatternData(
			DetectionPatternDataset patternDataset,
			int item, double confidenceLevel) {
		
		// check null
		if (patternDataset == null) {
			return null;
		}
		
		// get all records
		DetectionRecord[] dataRecords = patternDataset.getAllDetectionRecords();

		// Get Percent of Difference and it's average
		double   originValue, corrValue;
		DescriptiveStatistics stats = new DescriptiveStatistics();
		for (int i=0 ; i<dataRecords.length ; i++) {
			corrValue   = dataRecords[i].getRawDataCell(item).getData_value();
			originValue = dataRecords[i].getRawDataCell(item).getOrigin_value();
			if (originValue == 0.0D) {
				if (corrValue == 0.0D) {
					stats.addValue(0.0D);
				} else {
					continue;
				}
			} else {
				//stats.addValue((corrValue - originValue) / originValue * 100.0D);
				stats.addValue(Math.abs((corrValue - originValue) / originValue * 100.0D));
			}
		}
		
		// Get Percent of Difference and it's average
		double mean  = stats.getMean();
		double sigma = stats.getStandardDeviation();
		
		// compute threshold of confidence interval
		double[] threholds = computeThrehold(mean, sigma, confidenceLevel);
		
		// set ruleset
		RulesetPd ruleset = new RulesetPd(item);
		ruleset.setConfidenceLevel(confidenceLevel);
		ruleset.setMean(mean);
		ruleset.setSigma(sigma);
		ruleset.setThresholds(threholds);
		
		// return
		return ruleset;
		
	} // end of method
	
	/**
	 * compute threshold of confidence interval
	 */
	public static double[] computeThrehold(double mean, double sigma, double confidenceLevel) {
		
		// Get Z-score
		double c = (1.0D + confidenceLevel) / 2.0D;
		NormalDistribution normalDist = new NormalDistribution();
		double z = normalDist.inverseCumulativeProbability(c);
		
		// Get upper limit and lower limit
		double[] thresholds = new double[2];
		thresholds[0] = mean + (z * sigma);
		thresholds[1] = 0.0D;
		
		return thresholds;
		
	} // end of method
	

	
	//==========================================================================
	// Methods
	//==========================================================================

} // end of class
