/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier;

import java.util.Arrays;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.PcaResultModel;
import com.snu.tms.qa.model.ruleset.RawDataCell;
import com.snu.tms.qa.model.ruleset.RulesetPcaSpe;


/**
 * Outlier detection using PCA (SPE: Statistic Prediction Error)
 * 
 */
public class OutlierDetectionUsingPcaSpe {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	public OutlierDetectionUsingPcaSpe() {}

	//==========================================================================
	// static Methods
	//==========================================================================
	/**
	 * Do Outlier detection using PCA (SPE: Statistic Prediction Error)
	 */
	public static RulesetPcaSpe doOutlierDetectionWithPatternData(
			DetectionPatternDataset patternDataset,
			double confidenceLevel) {
		
		// check null
		if (patternDataset == null) {
			return null;
		}
		
		// get PCA result
		PcaResultModel pcaResultModel = patternDataset.getPcaResultModel();
		int pcCount = pcaResultModel.getPcCount();
		
		// get all records and make projected point array
		int[] items = patternDataset.getItems();
		DetectionRecord[] dataRecords = patternDataset.getAllDetectionRecords();
		double[][] dataset = new double[dataRecords.length][];
		RawDataCell[] rawDataCells;
		double[] data;
		for (int i=0 ; i<dataRecords.length ; i++) {
			rawDataCells = dataRecords[i].getRawDataCell(items);
			data = new double[items.length];
			for (int j=0 ; j<rawDataCells.length ; j++) {
				data[j] = rawDataCells[j].getData_value();
			}
			dataset[i] = pcaResultModel.getProjectionValues(data);
		}
		
		// compute mahalanobis distance
		//
		// set data size
		int M = dataset[0].length;
		int N = dataset.length;
		
		// calculate mean
		double[] mn = new double[M];
		double[] sigma = new double[M];
		Arrays.fill(mn, 0.0D);
		Arrays.fill(sigma, 0.0D);
		
		DescriptiveStatistics stats;
		for (int i=0 ; i<M ; i++) {
			stats = new DescriptiveStatistics();
			for (int j=0 ; j<N ; j++) {
				stats.addValue(dataset[j][i]);
			}
			mn[i] = stats.getMean();
			sigma[i] = stats.getStandardDeviation();
		}
		
		// subtract off the mean for each dimension
		double[][] nDataset = new double[N][M];
		for (int i=0 ; i<N ; i++) {
			for (int j=0 ; j<M ; j++) {
				nDataset[i][j] = (dataset[i][j] - mn[j]) / sigma[j];
			}
		}
		
		// compute threshold of SPE statistics
		double[] eigenValues = pcaResultModel.getEigenValues();
		double threhold = computeThrehold(
				eigenValues, confidenceLevel);
		
		// set ruleset
		RulesetPcaSpe ruleset = new RulesetPcaSpe(
				-1,
				patternDataset.getPatternId(),
				items,
				pcCount,
				pcaResultModel.getEigenVector(),
				pcaResultModel.getEigenValues(),
				pcaResultModel.getNeigenValues());
		ruleset.setMean(mn);
		ruleset.setSigma(sigma);
		ruleset.setConfidenceLevel(confidenceLevel);
		ruleset.setThresholds(new double[]{threhold, 0.0D});
		
		// return
		return ruleset;
		
	} // end of method
	
	
	/**
	 * compute threshold of Statistic Prediction Error (SPE)
	 */
	public static double computeThrehold(double[] eigenValue, double confidenceLevel) {
		
		// set parameters
		double N = (double)eigenValue.length;
		double theta1 = 0.0D;
		double theta2 = 0.0D;
		double theta3 = 0.0D;;
		for (int i=0 ; i<N ; i++) {
			theta1 += eigenValue[i];
			theta2 += Math.pow(eigenValue[i], 2.0D);
			theta3 += Math.pow(eigenValue[i], 3.0D);
		}
		
		// get Normal-distribution
		NormalDistribution nDist = new NormalDistribution();
		double ca = nDist.inverseCumulativeProbability(confidenceLevel);
		
		// compute threshold
		double h0 = 1.0D - (2.0D * theta1 * theta3) / (3.0D * theta2 * theta2);
		double q1 =ca * Math.sqrt(2.0D * theta2 * h0 * h0) / theta1;
		double q2 = theta2 * h0 * (h0 - 1.0D) / (theta1 * theta1);
		double q3 = 1.0D;
		double q = theta1 * Math.pow((q1 + q2 + q3), (1.0D / h0));
		
		// return 
		return q;
		
	} // end of method
	

	//==========================================================================
	// Methods
	//==========================================================================

} // end of class
