/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.TmsDataCell;


/**
 * Walsh's outlier test
 * 
 */
public class DoAnalysisOutlierWalsh {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(DoAnalysisOutlierWalsh.class);

	//==========================================================================
	// Fields
	//==========================================================================

	//==========================================================================
	// Constructor
	//==========================================================================
	public DoAnalysisOutlierWalsh() {}
	
	//==========================================================================
	// Test main() Method
	//==========================================================================
	/**
	 * Test source
	 */
	public static void main(String[] args) {
		
		TmsDataCell[] data = new TmsDataCell[300];
		for (int i=0 ; i<290 ; i++) {
			data[i] = new TmsDataCell(1, 100D + Math.random()*10D);
		}
		data[290] = new TmsDataCell(1, 2000.1D);
		data[291] = new TmsDataCell(1, 3000.2D);
		data[292] = new TmsDataCell(1, 4000.1D);
		data[293] = new TmsDataCell(1, 5000.1D);
		data[294] = new TmsDataCell(1, 7000.1D);
		data[295] = new TmsDataCell(1, 4000.1D);
		data[296] = new TmsDataCell(1, 7000.1D);
		data[297] = new TmsDataCell(1, 1.1D);
		data[298] = new TmsDataCell(1, 2.1D);
		data[299] = new TmsDataCell(1, 0.1D);
		
		double[] limits = doOutlierFiltering(data, false, 0.05D);
		System.out.println(limits[0] + ",  " + limits[1]);
		
		for (int i=0 ; i<data.length ; i++) {
			System.out.println(String.format("i=%-3d, value=%-6.3f, outlier=%s", 
					i,
					data[i].getData_value(),
					data[i].isOutlier()?"TRUE":"FLASE"));
		}
		
		
	} // end of method
	
	//==========================================================================
	// Static Method
	//==========================================================================
	/**
	 * Do outlier Filtering in case of data is not normal distributed.
	 * In this method use Walsh's method (in recommended by EPA)
	 */
	public static double[] doOutlierFiltering(
			TmsDataCell[] tmsDataCell, 
			boolean isSorted, 
			double significanceLevel) {
		
		// Check null
		if (tmsDataCell == null) {
			logger.debug("Seleted data set is null.");
			return null;
		}
		
		// Sorting
		if (!isSorted) {
			Arrays.sort(tmsDataCell, DoAnalysisOutlier.TmsDataCellComparator);
		}

		// Check minimum data count
		double minDataCount = 0.5D*Math.pow((1/significanceLevel+1.0D), 2.0D);
		if (((double)tmsDataCell.length) < minDataCount) {
			logger.debug(String.format("Data size is too small (%d < %d)", 
					tmsDataCell.length,
					(int)minDataCount));
			
			double[] limits = new double[2];
			limits[0] = tmsDataCell[0].getData_value();
			limits[1] = tmsDataCell[tmsDataCell.length-1].getData_value();
			
			return limits;
		}
		
		
		// do filtering outlier
		// return value is composed of two double value.
		// first value is lower limit and second value is upper limit value.
		return doAnalysis(tmsDataCell, significanceLevel);
		
	} // end of method
	

	/**
	 * doAnalysis
	 */
	private static double[] doAnalysis(
			TmsDataCell[] tmsDataCell, 
			double significanceLevel) {
		
		// Do Walsh's Test
		int n = tmsDataCell.length;
		double c = (int) Math.round(Math.sqrt(((double)n) * 2.0D));
		double b = Math.sqrt(1.0D / significanceLevel);
		double a = ( 1 + b * Math.sqrt( (c - b*b) / (c - 1.0D) ) ) / (c - b*b - 1);
		
		// Search smallest valid value
		int smallestR = 0;
		int cc = (int) c;
		for (int r=0 ; (r+cc) < n ; r++) {
			double value = tmsDataCell[r].getData_value() - 
					(1.0D + a)*tmsDataCell[r+1].getData_value() + 
					a*tmsDataCell[r+cc].getData_value();
			//System.out.println(String.format("r=%d, tmsDataCell[r]=%6.3f, value=%6.3f", r, tmsDataCell[r].getData_value(), value));
			if (value < 0.0D) {
				smallestR = r;
				break;
			}
		}
		// Set Lower Limits
		if (smallestR < (n-1)) {
			smallestR++;
		} else {
			smallestR = 0;
		}
		
		// Search largest Valid Value
		int largestR = 0;
		for (int r=1 ; r < (n-c) ; r++) {
			double value = tmsDataCell[n-r].getData_value() - 
					(1.0D + a)*tmsDataCell[n-1-r].getData_value() + 
					a*tmsDataCell[n-r-cc].getData_value();
			//System.out.println(String.format("r=%d, tmsDataCell[n-r]=%6.3f, value=%6.3f", r, tmsDataCell[n-r].getData_value(), value));
			if (value > 0.0D) {
				largestR = (n-r);
				break;
			} 
		}
		// Set Upper Limits
		if (largestR > 0) {
			largestR--;
		} else {
			largestR = n-1;
		}
		
		// Do Filtering
		for (int i=0 ; i<tmsDataCell.length ; i++) {
			
			// Check data is less than lower-limit
			if (i < smallestR) {
				tmsDataCell[i].setOutlier(true);
			}
			
			// Check data is larger than uppper-limit
			if (i > largestR) {
				tmsDataCell[i].setOutlier(true);
			}
		}
		
		// return
		double[] limits = new double[2];
		limits[0] = tmsDataCell[smallestR].getData_value();
		limits[1] = tmsDataCell[largestR].getData_value();
		
		return limits;
		
	} // end of method


} // end of class
