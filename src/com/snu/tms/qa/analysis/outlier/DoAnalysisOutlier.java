/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier;

import java.util.Arrays;
import java.util.Comparator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jdistlib.disttest.NormalityTest;

import com.snu.tms.qa.model.TmsDataCell;


/**
 * Do filtering for outlier-detection 
 * (normality test and determine algorithm between rosner's and walsh's method)
 * 
 */
public class DoAnalysisOutlier {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(DoAnalysisOutlier.class);

	//==========================================================================
	// Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	public DoAnalysisOutlier() {}

	//==========================================================================
	// Static Class
	//==========================================================================
	public static Comparator<TmsDataCell> TmsDataCellComparator = 
			new Comparator<TmsDataCell>() {
		
		public int compare(TmsDataCell data1, TmsDataCell data2) {
			
			if (data1.getData_value() > data2.getData_value()) {
				return 1;
			} else if (data1.getData_value() < data2.getData_value()) {
				return -1;
			} else {
				return 0;
			}
		}
	};
	

	//==========================================================================
	// Methods
	//==========================================================================
	/**
	 * 
	 */
	public static double[] doOutlierFiltering(
			TmsDataCell[] tmsDataCell, 
			boolean isSorted, 
			double significanceLevel) {
		
		// check null
		if (tmsDataCell == null) {
			return null;
		}
		
		// sorting
		if (!isSorted) {
			Arrays.sort(tmsDataCell, TmsDataCellComparator);
		}
		
		// test for normality
		// using "Shapiro-Wilk W test" method
		double pValue = doTestNomality(tmsDataCell);
		boolean isNormalDistribution = (pValue >=  significanceLevel);
		logger.debug(String.format("normality test:: p-value(%.3f) ==> %s selected.", 
				pValue, isNormalDistribution ? "Rosner's method":"Walsh's method"));
		
		// When data is normal distribution
		// To-do
		double[] limitValues = null;
		if (isNormalDistribution) {
			limitValues = DoAnalysisOutlierRosner.doOutlierFiltering(
					tmsDataCell, true, significanceLevel);
		} else {
			limitValues = DoAnalysisOutlierWalsh.doOutlierFiltering(
					tmsDataCell, true, significanceLevel);
		}	
		
		double[] resultValue = new double[3];
		resultValue[0] = pValue;
		resultValue[1] = limitValues[0];
		resultValue[2] = limitValues[1];
		return resultValue;
		
	}
	
	/**
	 * Do test normality with "Shapiro-Wilk W test" method
	 * @param tmsDataCell
	 * @return
	 */
	public static double doTestNomality(TmsDataCell[] tmsDataCell) {
		
		// make double array
		double[] data = new double[tmsDataCell.length];
		for (int i=0 ; i<tmsDataCell.length ; i++) {
			data[i] = tmsDataCell[i].getData_value();
		}
		
		int count = tmsDataCell.length;
		double wValue = NormalityTest.shapiro_wilk_statistic(data);
		double pValue = NormalityTest.shapiro_wilk_pvalue(wValue, count);
		return pValue;
		
	} // end of method
	
	//==========================================================================
	// Methods
	//==========================================================================



} // end of class
