/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier;

import java.util.Arrays;

import org.apache.commons.math3.distribution.FDistribution;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import com.snu.tms.qa.model.ruleset.DetectionPatternDataset;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.PcaResultModel;
import com.snu.tms.qa.model.ruleset.RawDataCell;
import com.snu.tms.qa.model.ruleset.RulesetPcaTss;


/**
 * Outlier detection using PCA (T-square statistics)
 * 
 */
public class OutlierDetectionUsingPcaTss {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// Fields
	//==========================================================================
	
	//==========================================================================
	// Constructor
	//==========================================================================
	public OutlierDetectionUsingPcaTss() {}

	//==========================================================================
	// static Methods
	//==========================================================================
	/**
	 * Do Outlier detection using PCA (T-square statistics)
	 */
	public static RulesetPcaTss doOutlierDetectionWithPatternData(
			DetectionPatternDataset patternDataset,
			double confidenceLevel) {
		
		// check null
		if (patternDataset == null) {
			return null;
		}
		
		// get PCA result
		PcaResultModel pcaResultModel = patternDataset.getPcaResultModel();
		int pcCount = pcaResultModel.getPcCount();
		
		// get all records and make projected point array
		int[] items = patternDataset.getItems();
		DetectionRecord[] dataRecords = patternDataset.getAllDetectionRecords();
		double[][] dataset = new double[dataRecords.length][];
		RawDataCell[] rawDataCells;
		double[] data;
		for (int i=0 ; i<dataRecords.length ; i++) {
			rawDataCells = dataRecords[i].getRawDataCell(items);
			data = new double[items.length];
			for (int j=0 ; j<rawDataCells.length ; j++) {
				data[j] = rawDataCells[j].getData_value();
			}
			dataset[i] = pcaResultModel.getProjectionValues(data);
		}
		
		// compute mahalanobis distance
		//
		// set data size
		int M = dataset[0].length;
		int N = dataset.length;
		
		// calculate mean
		double[] mn = new double[M];
		double[] sigma = new double[M];
		Arrays.fill(mn, 0.0D);
		Arrays.fill(sigma, 0.0D);
		
		DescriptiveStatistics stats;
		for (int i=0 ; i<M ; i++) {
			stats = new DescriptiveStatistics();
			for (int j=0 ; j<N ; j++) {
				stats.addValue(dataset[j][i]);
			}
			mn[i] = stats.getMean();
			sigma[i] = stats.getStandardDeviation();
		}
		
		// subtract off the mean for each dimension
		double[][] nDataset = new double[N][M];
		for (int i=0 ; i<N ; i++) {
			for (int j=0 ; j<M ; j++) {
				nDataset[i][j] = (dataset[i][j] - mn[j]) / sigma[j];
			}
		}
		
		// calculate covariance matrix and it's imverse
		Covariance cv = new Covariance(nDataset);
		RealMatrix mCovariance = cv.getCovarianceMatrix();
		
		RealMatrix imverseCovMatrix = null;
		try {
			imverseCovMatrix = MatrixUtils.inverse(mCovariance);
		} catch (Exception e) {
			return null;
		}
		
		// compute threshold of t-square statistics
		double threhold = computeThrehold(
				dataRecords.length, pcCount, confidenceLevel);
		
		// set ruleset
		RulesetPcaTss ruleset = new RulesetPcaTss(
				-1,
				patternDataset.getPatternId(),
				items,
				pcCount,
				pcaResultModel.getEigenVector(),
				pcaResultModel.getEigenValues(),
				pcaResultModel.getNeigenValues());
		ruleset.setCovMatrix(mCovariance.getData());
		ruleset.setImvCovMatrix(imverseCovMatrix.getData());
		ruleset.setMean(mn);
		ruleset.setSigma(sigma);
		ruleset.setConfidenceLevel(confidenceLevel);
		ruleset.setThresholds(new double[]{threhold, 0.0D});
		
		// return
		return ruleset;
		
	} // end of method
	
	
	/**
	 * compute threshold of t-square statistics
	 */
	public static double computeThrehold(int dataCount, int itemCount, double confidenceLevel) {
		
		// set parameters
		double M = (double)itemCount;
		double N = (double) dataCount;
		
		// get F-distribution
		FDistribution fDist = new FDistribution(M, (N-M));
		double f = fDist.inverseCumulativeProbability(confidenceLevel);
		
		// compute threshold
		double threshold = (M * (N - 1.0D) * (N + 1.0D)) / (N * (N - M)) * f;
		
		return threshold;
		
	} // end of method
	

	//==========================================================================
	// Methods
	//==========================================================================

} // end of class
