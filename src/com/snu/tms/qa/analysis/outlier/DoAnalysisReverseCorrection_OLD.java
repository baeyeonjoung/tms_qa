/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.analysis.outlier;

import com.snu.tms.qa.model.CorrectionItemInfo;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.StackInfo;
import com.snu.tms.qa.model.TmsDataCell;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.ruleset.RulesetCorrectionModel;
import com.snu.tms.qa.model.ruleset.RulesetModel;

/**
 * Get correction before value (temperature, moisture, oxygen)
 * 
 */
public class DoAnalysisReverseCorrection_OLD {

	//==========================================================================
	// Static Fields
	//==========================================================================

	//==========================================================================
	// static Methods
	//==========================================================================
	/**
	 * Do reverse correction
	 */
	public static void doReverseCorrection(
			TmsDataset tmsDataSet) {
		
		// prepare correctionFactor
		StackInfo  stackInfo = tmsDataSet.getTmsDataHeader().getStackInfo();
		CorrectionItemInfo correctionInfo = null;
		double stdMois = tmsDataSet.getTmsDataHeader().getStackInfo().getStandardMoisture();
		double tmsOxyg, tmsTmp;
		double stdOxyg;
		
		// do reverse correction
		TmsDataRecord[] tmsDataRecords = tmsDataSet.getTmsDataRecords();
		TmsDataCell dataCell = null;
		for (TmsDataRecord tmsDataRecord : tmsDataRecords) {
			
			// prepare correction value
			tmsOxyg = tmsDataRecord.getDataCell(ItemCodeDefine.O2).getData_value();
			tmsTmp  = tmsDataRecord.getDataCell(ItemCodeDefine.TMP).getData_value();
			
			// do reverse correction for each item
			int[] allItems = ItemCodeDefine.getAllItems();
			int   item;
			for (int i=0 ; i<allItems.length ; i++) {
				
				// set item code
				item = allItems[i];
				
				// Check data is valid
				dataCell = tmsDataRecord.getDataCell(item);
				if (dataCell.getData_value() == -1.0D) {
					continue;
				}
				
				// copy dataValue to originValue
				dataCell.setOrigin_value(dataCell.getData_value());
				
				// Get Correction Factor Info
				correctionInfo =  stackInfo.getCorrectionInfo(item);
				stdOxyg = correctionInfo.getStdOxygen();
				
				
				// Do oxygen correction
				if (correctionInfo.isOxygenApplied() && dataCell.getData_value() != -1.0D && stdOxyg != -1.0D) {
					dataCell.setOrigin_value(DoAnalysisReverseCorrection_OLD.doOxygenReverseCorrection(
							dataCell.getOrigin_value(), stdOxyg, tmsOxyg));
					dataCell.setOxyg_correction(true);
				}
				
				// Do temperature correction
				if (correctionInfo.isTemperatureApplied() && dataCell.getData_value() != -1.0D) {
					dataCell.setOrigin_value(DoAnalysisReverseCorrection_OLD.doTemperatureReverseCorrection(
							dataCell.getOrigin_value(), tmsTmp));
					dataCell.setTemp_correction(true);
				}
			
				// Do Moisture correction
				if (correctionInfo.isMoistureApplied() && dataCell.getData_value() != -1.0D && stdMois != -1.0D) {
					if (item == ItemCodeDefine.FL1) {
						dataCell.setOrigin_value(DoAnalysisReverseCorrection_OLD.doMoistureReverseCorrectionFlow(
								dataCell.getOrigin_value(), stdMois));
						dataCell.setMois_correction(true);
					} else {
						dataCell.setOrigin_value(DoAnalysisReverseCorrection_OLD.doMoistureReverseCorrection(
								dataCell.getOrigin_value(), stdMois));
						dataCell.setMois_correction(true);
					}
				}
				
				// logging
				//System.out.println(String.format("ItemCode:%s, vlaue:%.3f, corrValue:%.3f", 
				//		ItemCodeDefine.ITEM_NAME[item], dataCell.getData_value(), dataCell.getOrigin_value()));
			}
		}
		
	} // end of method
	
	/**
	 * Do reverse correction
	 * 보정전 자료가 설정되지 않은 것에 대해 수식을 통해 설정
	 */
	public static void doReverseCorrectionForEmptyCell(
			TmsDataRecord tmsDataRecord, 
			RulesetModel rulesetModel) {
		
		double tmsOxyg, tmsTmp;
		double stdOxyg, stdMois;
		
		// prepare correction value
		tmsOxyg = tmsDataRecord.getDataCell(ItemCodeDefine.O2).getData_value();
		tmsTmp  = tmsDataRecord.getDataCell(ItemCodeDefine.TMP).getData_value();
		
		// do reverse correction for each item
		int[] allItems = ItemCodeDefine.getAllItems();
		int   item;
		TmsDataCell dataCell = null;
		for (int i=0 ; i<allItems.length ; i++) {
			
			// set item code
			item = allItems[i];
			
			// Check data is valid
			dataCell = tmsDataRecord.getDataCell(item);
			if (dataCell.getData_value() == -1.0D) {
				continue;
			}
			
			// check Origin_Value is empty
			if (dataCell.getOrigin_value() == -1.0D) {
				continue;
			}
			
			// copy dataValue to originValue
			dataCell.setOrigin_value(dataCell.getData_value());
				
			// Get Correction Factor Info
			RulesetCorrectionModel correctionModel = rulesetModel.getRulesetCorrectionModel(item);
			if (correctionModel == null) {
				continue;
			}
			
			stdOxyg = correctionModel.getStdOxygen();
			stdMois = correctionModel.getStdMoisture();
			
			// Do oxygen correction
			if (correctionModel.isOxygenApplied() && dataCell.getData_value() != -1.0D && stdOxyg != -1.0D) {
				if (item == ItemCodeDefine.FL1) {
					dataCell.setOrigin_value(DoAnalysisReverseCorrection_OLD.doOxygenReverseCorrectionFlow(
							dataCell.getOrigin_value(), stdOxyg, tmsOxyg));
					dataCell.setOxyg_correction(true);
				} else {
					dataCell.setOrigin_value(DoAnalysisReverseCorrection_OLD.doOxygenReverseCorrection(
							dataCell.getOrigin_value(), stdOxyg, tmsOxyg));
					dataCell.setOxyg_correction(true);
				}
			}
			
			// Do temperature correction
			if (correctionModel.isTemperatureApplied() && dataCell.getData_value() != -1.0D) {
				if (item == ItemCodeDefine.FL1) {
					dataCell.setOrigin_value(DoAnalysisReverseCorrection_OLD.doTemperatureReverseCorrectionFlow(
							dataCell.getOrigin_value(), tmsTmp));
					dataCell.setTemp_correction(true);
				} else {
					dataCell.setOrigin_value(DoAnalysisReverseCorrection_OLD.doTemperatureReverseCorrection(
							dataCell.getOrigin_value(), tmsTmp));
					dataCell.setTemp_correction(true);
				}
			}
			
			// Do Moisture correction
			if (correctionModel.isMoistureApplied() && dataCell.getData_value() != -1.0D && stdMois != -1.0D) {
				if (item == ItemCodeDefine.FL1) {
					dataCell.setOrigin_value(DoAnalysisReverseCorrection_OLD.doMoistureReverseCorrectionFlow(
							dataCell.getOrigin_value(), stdMois));
					dataCell.setMois_correction(true);
				} else {
					dataCell.setOrigin_value(DoAnalysisReverseCorrection_OLD.doMoistureReverseCorrection(
							dataCell.getOrigin_value(), stdMois));
					dataCell.setMois_correction(true);
				}
			}
			
		} // end of for-loop
		
	} // end of method
	
	/**
	 * Do reverse correction
	 */
	public static double doReverseCorrection(
			int item, double af_value, 
			double stdOxgn, double stdMois, 
			double oxgn, double temp, 
			boolean doOxgn, boolean doTemp, boolean doMois) {
		
		// create return value
		double value = af_value;
		
		// Do oxygen correction
		if (doOxgn && af_value != -1.0D && stdOxgn != -1.0D) {
			value = doOxygenReverseCorrection(af_value, stdOxgn, oxgn);
		}
		
		// Do temperature correction
		if (doTemp && temp != -1.0D) {
			value = doTemperatureReverseCorrection(value, temp);
		}
	
		// Do Moisture correction
		if (doMois && af_value != -1.0D && stdMois != -1.0D) {
			if (item == ItemCodeDefine.FL1) {
				value = doMoistureReverseCorrectionFlow(value, stdMois);
			} else {
				value = doMoistureReverseCorrection(value, stdMois);
			}
		}
		
		// return
		return value;
		
	} // end of method

			
	/**
	 * Do temperature reverse correction
	 */
	public  static  double  doTemperatureReverseCorrection(
			double  afterValue,
			double  gauge_temp) {
		
		double  beforeValue = 0.0D;
		beforeValue = afterValue * (273.0D)/(273.0D + gauge_temp);
		if (beforeValue < 0.0D) {
			beforeValue = 0.0D;
		}
		
		return beforeValue;
	}
	
	/**
	 * Do temperature reverse correction (FLW)
	 */
	public  static  double  doTemperatureReverseCorrectionFlow(
			double  afterValue,
			double  gauge_temp) {
		
		double  beforeValue = 0.0D;
		beforeValue = afterValue * (273.0D + gauge_temp)/(273.0D);
		if (beforeValue < 0.0D) {
			beforeValue = 0.0D;
		}
		
		return beforeValue;
	}
	
	/**
	 * Do Moisture reverse correction
	 */
	public  static  double  doMoistureReverseCorrection(
			double  afterValue,
			double  stck_mois) {
		
		double  beforeValue = 0.0D;
		if (stck_mois == 100.0D) {
			beforeValue = afterValue;
		} else {
			beforeValue = afterValue * (100.0D - stck_mois)/(100.0D);
		}
		if (beforeValue < 0.0D) {
			beforeValue = 0.0D;
		}
		
		return beforeValue;
	}
	
	/**
	 * Do Moisture reverse correction (flow)
	 */
	public  static  double  doMoistureReverseCorrectionFlow(
			double  afterValue,
			double  stck_mois) {
		
		double  beforeValue = 0.0D;
		if (stck_mois == 100.0D) {
			beforeValue = afterValue;
		} else {
			beforeValue = afterValue * (100.0D)/(100.0D - stck_mois);
		}
		if (beforeValue < 0.0D) {
			beforeValue = 0.0D;
		}
		
		return beforeValue;
	}
	
	/**
	 * Do oxygen reverse correction
	 */
	public  static  double  doOxygenReverseCorrection(
			double  afterValue,
			double  std_oxyg, 
			double  gauge_oxyg) {
		
		double  beforeValue = 0.0D;
		if (gauge_oxyg == 21.0D || std_oxyg == 21.0D) {
			beforeValue = afterValue;
		} else {
			beforeValue = afterValue * (21.0D - gauge_oxyg)/(21.0D - std_oxyg);
		}
		if (beforeValue < 0.0D) {
			beforeValue = 0.0D;
		}
		
		return beforeValue;
	}
	
	/**
	 * Do oxygen reverse correction
	 */
	public  static  double  doOxygenReverseCorrectionFlow(
			double  afterValue,
			double  std_oxyg, 
			double  gauge_oxyg) {
		
		double  beforeValue = 0.0D;
		if (gauge_oxyg == 21.0D || std_oxyg == 21.0D) {
			beforeValue = afterValue;
		} else {
			beforeValue = afterValue * (21.0D - std_oxyg)/(21.0D - gauge_oxyg);
		}
		if (beforeValue < 0.0D) {
			beforeValue = 0.0D;
		}
		
		return beforeValue;
	}

} // end of class
