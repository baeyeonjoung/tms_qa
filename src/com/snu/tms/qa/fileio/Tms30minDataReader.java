/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.fileio;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.db.Tms30minDataCellDB;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Vector;

/**
 * Data Element for TMS Data
 * 
 */
public class Tms30minDataReader {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(Tms30minDataReader.class);

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructors
	//==========================================================================

	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Read TmsData from a file
	 */
	public static Vector<Tms30minDataCellDB> readDataFile(
			String inputFilePath) {
		
		// Prepare vector
		Vector<Tms30minDataCellDB> dataArray = new Vector<Tms30minDataCellDB>();
		
		// Open File and parse data
		BufferedReader in = null;
		try {
			// File Open
			in = new BufferedReader(new FileReader(inputFilePath));
			logger.debug("DataFile[" + inputFilePath + "] successfully open .........");
			
			String lineReadStr = "";
			
			while(true) {
				
				// Read Line
				lineReadStr = in.readLine();
				if (lineReadStr == null) {
					break;
				}
				lineReadStr = lineReadStr.trim();
				
				// If targetLineString is comment, skip parsing
				if (lineReadStr.startsWith("%") || lineReadStr.startsWith("#")) {
					continue;
				}
				
				// Parse String
				Tms30minDataCellDB parsedData = parseTmsDataString(lineReadStr);
				if (parsedData == null) {
					continue;
				}
				
				// Add DataArray
				dataArray.add(parsedData);
				
			}
			
		} catch (Exception e) {
			logger.error("Parsing data file error.", e);
		} finally {
			if (in != null) {
				try { in.close(); } catch (Exception ee) {}
			}
		}

		// return
		return dataArray;
		
	} // end of method
	
	
	/**
	 * Parse Data String
	 */
	private static Tms30minDataCellDB parseTmsDataString(String dataString) {
		
		// Memory allocation
		Tms30minDataCellDB dataCell = new Tms30minDataCellDB();
		
		// Parsing dataString
		try {
			
			// divide String to StringArray with "tab"
			String[] dataStrArray = dataString.split("\t");
			if (dataStrArray.length != 8) {
				throw new Exception("Data fields are not exact.");
			}

			// Parse item_code
			dataCell.setItem_code(Integer.parseInt(dataStrArray[0]));

			// Parse time
			dataCell.setHalf_time(dataStrArray[1]);
			
			// Parse value
			dataCell.setHalf_valu(Double.parseDouble(dataStrArray[2]));
			dataCell.setHalf_over(dataStrArray[3].trim());
			dataCell.setHalf_stat(dataStrArray[4].trim());
			dataCell.setHalf_dlst(dataStrArray[5].trim());
			dataCell.setStat_code(dataStrArray[6].trim());
			dataCell.setStat_valu(Double.parseDouble(dataStrArray[7]));
			
		} catch (Exception ee) {
			logger.error("Parsing error in [" + dataString + "]/n", ee);
			dataCell = null;
		}
		
		// return data
		return dataCell;
		
	} // end of method
	

} // end of class
