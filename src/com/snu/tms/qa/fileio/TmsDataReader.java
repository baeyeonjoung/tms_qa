/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.fileio;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.db.TmsDataCellDB;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Vector;

/**
 * Data Element for TMS Data
 * 
 */
public class TmsDataReader {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(TmsDataReader.class);

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructors
	//==========================================================================

	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * search file
	 */
	public static String searchFile(
			String   folderPath,
			String   factCode,
			int      stckCode,
			String   sTimeStr,
			String   eTimeStr) {
		
		try {
			// set target file name
			String targetFileName = String.format("tms_data_%s_%d_", factCode, stckCode);
			
			// get file name list
			File folder = new File(folderPath);
			File[] fileList = folder.listFiles();
			for (File file : fileList) {
				
				// check file name
				String fileName = file.getName();
				String mFileName = fileName.substring(0, fileName.length()-4);
				if (mFileName.startsWith(targetFileName)) {
					
					// separate with "_"
					String[] nameParts = mFileName.split("_");
					if (nameParts.length < 6) {
						continue;
					}
					
					// check startTime
					if (nameParts[4].compareTo(sTimeStr) > 0) {
						continue;
					}
					
					// check endTime
					if (nameParts[5].compareTo(eTimeStr) < 0) {
						continue;
					}
					return folderPath + "/" + fileName;
					
				}
				
			} // end of for-loop
			
			// return
			return null;
			
		} catch (Exception e) {
			return null;
		}
		
	} // end of method
	
	/**
	 * Read TmsData from a file
	 */
	public static Vector<TmsDataCellDB> readDataFile(
			String inputFilePath) {
		
		// Prepare vector
		Vector<TmsDataCellDB> dataArray = new Vector<TmsDataCellDB>();
		
		// Open File and parse data
		BufferedReader in = null;
		try {
			// File Open
			in = new BufferedReader(new FileReader(inputFilePath));
			logger.debug("DataFile[" + inputFilePath + "] successfully open .........");
			
			String lineReadStr = "";
			
			while(true) {
				
				// Read Line
				lineReadStr = in.readLine();
				if (lineReadStr == null) {
					break;
				}
				lineReadStr = lineReadStr.trim();
				
				// If targetLineString is comment, skip parsing
				if (lineReadStr.startsWith("%") || lineReadStr.startsWith("#")) {
					continue;
				}
				
				// Parse String
				TmsDataCellDB parsedData = parseTmsDataString(lineReadStr);
				if (parsedData == null) {
					continue;
				}
				
				// Add DataArray
				dataArray.add(parsedData);
				
			}
			
		} catch (Exception e) {
			logger.error("Parsing data file error.", e);
		} finally {
			if (in != null) {
				try { in.close(); } catch (Exception ee) {}
			}
		}

		// return
		return dataArray;
		
	} // end of method
	
	
	/**
	 * Parse Data String
	 */
	private static TmsDataCellDB parseTmsDataString(String dataString) {
		
		// Memory allocation
		TmsDataCellDB dataCell = new TmsDataCellDB();
		
		// Parsing dataString
		try {
			
			// divide String to StringArray with "tab"
			String[] dataStrArray = dataString.split("\t");
			if (dataStrArray.length != 4) {
				throw new Exception("Data fields are not exact.");
			}

			// Parse item_code
			dataCell.setItem_code(Integer.parseInt(dataStrArray[0]));

			// Parse time
			dataCell.setMin_time(dataStrArray[1]);
			
			// Parse value
			dataCell.setMin_valu(Double.parseDouble(dataStrArray[2]));
			
			// Parse value (before_va;ue)
			dataCell.setMin_valu_bf(Double.parseDouble(dataStrArray[3]));

		} catch (Exception ee) {
			logger.error("Parsing error in [" + dataString + "]/n", ee);
			dataCell = null;
		}
		
		// return data
		return dataCell;
		
	} // end of method
	

} // end of class
