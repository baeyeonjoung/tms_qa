/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.fileio;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.TmsDataCell;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.util.DateUtil;
import com.snu.tms.qa.util.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Data Element for TMS Data
 * 
 */
public class TmsDataWriter {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(TmsDataWriter.class);

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructors
	//==========================================================================

	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Write TmsData as a file
	 */
	public static void writeDataFile(
			String outputFilePath,
			TmsDataset tmsDataSet) {
		
		
		// Prepare file writer
		BufferedWriter writer = null;
		try {
			// Prepare file
			File resultFile = new File(outputFilePath);
			if (resultFile.exists()) {
				FileUtils.deleteFile(outputFilePath);
			}
			
			// Prepare writer
			Charset charset = Charset.forName("KSC5601");
			Path path = FileSystems.getDefault().getPath(outputFilePath);
			writer = Files.newBufferedWriter(path, charset);
			
			// Get TmsRecords
			TmsDataRecord[] records = tmsDataSet.getTmsDataRecords();
			if (records == null  || records.length == 0) {
				throw new Exception("TmsRecords is null or data length is zero.");
			}
			
			TmsDataCell dataCell= null;
			String timeStr = "";
			String formatter = "%d\t%s\t%.3f\t%.3f\n";
			for (TmsDataRecord record : records) {
				
				timeStr = DateUtil.getTmsDateStr(record.getTime());
				
				dataCell = record.getTsp();
				if (dataCell.getData_value() != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.TSP, 
							timeStr, dataCell.getData_value(), dataCell.getOrigin_value()));
				}
				dataCell = record.getSox();
				if (dataCell.getData_value() != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.SOX, 
							timeStr, dataCell.getData_value(), dataCell.getOrigin_value()));
				}
				dataCell = record.getNox();
				if (dataCell.getData_value() != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.NOX, 
							timeStr, dataCell.getData_value(), dataCell.getOrigin_value()));
				}
				dataCell = record.getHcl();
				if (dataCell.getData_value() != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.HCL, 
							timeStr, dataCell.getData_value(), dataCell.getOrigin_value()));
				}
				dataCell = record.getHf();
				if (dataCell.getData_value() != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.HF, 
							timeStr, dataCell.getData_value(), dataCell.getOrigin_value()));
				}
				dataCell = record.getNh3();
				if (dataCell.getData_value() != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.NH3, 
							timeStr, dataCell.getData_value(), dataCell.getOrigin_value()));
				}
				dataCell = record.getCo();
				if (dataCell.getData_value() != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.CO, 
							timeStr, dataCell.getData_value(), dataCell.getOrigin_value()));
				}
				dataCell = record.getO2();
				if (dataCell.getData_value() != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.O2, 
							timeStr, dataCell.getData_value(), dataCell.getOrigin_value()));
				}
				dataCell = record.getFl1();
				if (dataCell.getData_value() != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.FL1, 
							timeStr, dataCell.getData_value(), dataCell.getOrigin_value()));
				}
				dataCell = record.getFl2();
				if (dataCell.getData_value() != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.FL2, 
							timeStr, dataCell.getData_value(), dataCell.getOrigin_value()));
				}
				dataCell = record.getTmp();
				if (dataCell.getData_value() != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.TMP, 
							timeStr, dataCell.getData_value(), dataCell.getOrigin_value()));
				}
				dataCell = record.getTm1();
				if (dataCell.getData_value() != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.TM1, 
							timeStr, dataCell.getData_value(), dataCell.getOrigin_value()));
				}
				dataCell = record.getTm2();
				if (dataCell.getData_value() != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.TM2, 
							timeStr, dataCell.getData_value(), dataCell.getOrigin_value()));
				}
				dataCell = record.getTm3();
				if (dataCell.getData_value() != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.TM3, 
							timeStr, dataCell.getData_value(), dataCell.getOrigin_value()));
				}
			}
			
		} catch (Exception e) {
			logger.error("File writing error.", e);
		} finally {
			try {
				writer.close();
			} catch (Exception ee) {}
		}

		
	} // end of method
	
	

} // end of class
