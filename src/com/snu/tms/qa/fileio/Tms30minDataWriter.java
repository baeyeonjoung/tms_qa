/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.fileio;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.Tms30minDataRecord;
import com.snu.tms.qa.model.Tms30minDataset;
import com.snu.tms.qa.util.DateUtil;
import com.snu.tms.qa.util.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Data Element for TMS Data
 * 
 */
public class Tms30minDataWriter {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(Tms30minDataWriter.class);

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructors
	//==========================================================================

	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Write TmsData as a file
	 */
	public static void writeDataFile(
			String outputFilePath,
			Tms30minDataset tmsDataSet) {
		
		
		// Prepare file writer
		BufferedWriter writer = null;
		try {
			// Prepare file
			File resultFile = new File(outputFilePath);
			if (resultFile.exists()) {
				FileUtils.deleteFile(outputFilePath);
			}
			
			// Prepare writer
			Charset charset = Charset.forName("KSC5601");
			Path path = FileSystems.getDefault().getPath(outputFilePath);
			writer = Files.newBufferedWriter(path, charset);
			
			// Get TmsRecords
			Tms30minDataRecord[] records = tmsDataSet.getTmsDataRecords();
			if (records == null  || records.length == 0) {
				throw new Exception("TmsRecords is null or data length is zero.");
			}
			
			String timeStr = "";
			String formatter = "%d\t%s\t%.3f\t%s\t%s\t%s\t%s\t%.3f\n";

			double    half_valu = -1.0D;
			String    half_over = "0";
			String    half_stat = "0";
			String    half_dlst = "0";
			String    stat_code = "0";
			double    stat_valu = -1.0D;
			for (Tms30minDataRecord record : records) {
				
				timeStr = DateUtil.getTmsDateStr(record.getTime());
				int[] items = record.getValidItems();
				for (int i=0 ; i<items.length ; i++) {
					
					// date data
					half_valu = record.getDataCell(items[i]).getHalf_valu();
					half_over = record.getDataCell(items[i]).getHalf_over();
					half_stat = record.getDataCell(items[i]).getHalf_stat();
					half_dlst = record.getDataCell(items[i]).getHalf_dlst();
					stat_code = record.getDataCell(items[i]).getStat_code();
					stat_valu = record.getDataCell(items[i]).getStat_valu();
					
					// write
					writer.write(String.format(formatter, 
							items[i], timeStr, half_valu, half_over, half_stat, half_dlst, 
							stat_code, stat_valu));
					
				}
			}
			
		} catch (Exception e) {
			logger.error("File writing error.", e);
		} finally {
			try {
				writer.close();
			} catch (Exception ee) {}
		}

		
	} // end of method
	
	

} // end of class
