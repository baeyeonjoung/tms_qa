/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.fileio;

import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.Tms30minDataRecord;
import com.snu.tms.qa.model.Tms30minDataset;
import com.snu.tms.qa.model.db.Tms30minDataCellDB;
import com.snu.tms.qa.util.DateUtil;

/**
 * Query Agent
 * 
 */
public class QueryTms30minRecordFromFile {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryTms30minRecordFromFile.class);
	
	//==========================================================================
	// Static Method
	//==========================================================================
	
	/**
	 * @param start_time_str: time format (yyyy-MM-dd HH:mm:ss)
	 * @param end_time_str: time format (yyyy-MM-dd HH:mm:ss)
	 */
	public static Tms30minDataset readTmsRecord(
			String  inputFilePath, String fact_code, int stck_code, 
			long start_time, long end_time) {
		
		// Create TmsDataSet
		Tms30minDataset tmsDataSet = new Tms30minDataset();
		tmsDataSet.init(start_time, end_time);
		
		// Read data from a file
		Vector<Tms30minDataCellDB> dataArray = Tms30minDataReader.readDataFile(inputFilePath);
		long targetTime = 0L;
		Tms30minDataCellDB dataCell;
		try {
			for (int i=0 ; i<dataArray.size() ; i++) {

				dataCell = dataArray.get(i);
				targetTime = DateUtil.getTmsDate(dataCell.getHalf_time());
				Tms30minDataRecord record = tmsDataSet.getTmsDataRecord(targetTime);
				if (record != null) {
					record.setValue(
						dataCell.getItem_code(), 
						dataCell.getHalf_valu(),
						dataCell.getHalf_over(),
						dataCell.getHalf_stat(),
						dataCell.getHalf_dlst(),
						dataCell.getStat_code(),
						dataCell.getStat_valu());
				}

			} // end of for-loop
			
		} catch (Exception e) {
			logger.debug(e.toString());
		}
		
		return tmsDataSet;
		
	} // end of method
	

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
