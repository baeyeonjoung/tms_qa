/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.fileio;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.DatasetManager;
import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.util.DateUtil;
import com.snu.tms.qa.util.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Write temporary pattern data analysis result
 * 
 */
public class PatternDataWriter {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(PatternDataWriter.class);

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructors
	//==========================================================================

	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Write TmsData as a file
	 */
	public static void writeDataFile() {
		
		// get DataManager instance
		DatasetManager datasetManager = DatasetManager.getInstance();
		
		// set parameters
		String fact_code = datasetManager.getTmsDataset().getTmsDataHeader().getFactoryInfo().getCode();
		int    stck_code = datasetManager.getTmsDataset().getTmsDataHeader().getStackInfo().getStackNumber();
		String sTimeStr  = datasetManager.getTmsDataset().getTmsDataHeader().getStart_time_str();
		String eTimeStr  = datasetManager.getTmsDataset().getTmsDataHeader().getEnd_time_str();
		
		/**
		// prepare data save directory
		String fileName = "./clone_data/"
		
		
		// Prepare file writer
		BufferedWriter writer = null;
		try {
			// Prepare file
			File resultFile = new File(outputFilePath);
			if (resultFile.exists()) {
				FileUtils.deleteFile(outputFilePath);
			}
			
			// Prepare writer
			Charset charset = Charset.forName("KSC5601");
			Path path = FileSystems.getDefault().getPath(outputFilePath);
			writer = Files.newBufferedWriter(path, charset);
			
			// Get TmsRecords
			TmsDataRecord[] records = tmsDataSet.getTmsDataRecords();
			if (records == null  || records.length == 0) {
				throw new Exception("TmsRecords is null or data length is zero.");
			}
			
			double value = 0.0D;
			String timeStr = "";
			String formatter = "%d\t%s\t%.3f\n";
			for (TmsDataRecord record : records) {
				
				timeStr = DateUtil.getTmsDateStr(record.getTime());
				
				value = record.getTsp().getData_value();
				if (value != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.TSP, timeStr, value));
				}
				value = record.getSo2().getData_value();
				if (value != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.SO2, timeStr, value));
				}
				value = record.getNox().getData_value();
				if (value != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.NOX, timeStr, value));
				}
				value = record.getHcl().getData_value();
				if (value != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.HCL, timeStr, value));
				}
				value = record.getHf().getData_value();
				if (value != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.HF, timeStr, value));
				}
				value = record.getNh3().getData_value();
				if (value != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.NH3, timeStr, value));
				}
				value = record.getCo().getData_value();
				if (value != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.CO, timeStr, value));
				}
				value = record.getO2().getData_value();
				if (value != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.O2, timeStr, value));
				}
				value = record.getFlw().getData_value();
				if (value != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.FLW, timeStr, value));
				}
				value = record.getTmp().getData_value();
				if (value != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.TMP, timeStr, value));
				}
				value = record.getTm1().getData_value();
				if (value != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.TM1, timeStr, value));
				}
				value = record.getTm2().getData_value();
				if (value != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.TM2, timeStr, value));
				}
				value = record.getTm3().getData_value();
				if (value != -1.0D) {
					writer.write(String.format(formatter, ItemCodeDefine.TM3, timeStr, value));
				}
			}
			
		} catch (Exception e) {
			logger.error("File writing error.", e);
		} finally {
			try {
				writer.close();
			} catch (Exception ee) {}
		}
		*/

		
	} // end of method
	
	

} // end of class
