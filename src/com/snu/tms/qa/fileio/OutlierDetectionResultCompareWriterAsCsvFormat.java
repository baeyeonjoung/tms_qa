/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.fileio;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.Tms30minDataCell;
import com.snu.tms.qa.model.Tms30minDataRecord;
import com.snu.tms.qa.model.TmsDataRecord;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.ruleset.DetectionDataCell;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.OdrCell;
import com.snu.tms.qa.model.ruleset.OdrRecord;
import com.snu.tms.qa.util.DateUtil;
import com.snu.tms.qa.util.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Hashtable;

/**
 * Data Element for TMS Data
 * 
 */
public class OutlierDetectionResultCompareWriterAsCsvFormat {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(OutlierDetectionResultCompareWriterAsCsvFormat.class);

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructors
	//==========================================================================

	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Write TmsData as a file
	 */
	public static void writeDataFile(
			String     outputFilePath,
			int[]      items,
			long       startTime,
			long       endTime,
			Hashtable<Long, OdrRecord>          odrRecordHash,
			Hashtable<Long, TmsDataRecord>      tmsRecordHash,
			Hashtable<Long, Tms30minDataRecord> tms30minRecordHash) {
		
		// Prepare file writer
		BufferedWriter writer = null;
		try {
			// Prepare file
			File resultFile = new File(outputFilePath);
			if (resultFile.exists()) {
				FileUtils.deleteFile(outputFilePath);
			}
			
			// Prepare writer
			Charset charset = Charset.forName("KSC5601");
			Path path = FileSystems.getDefault().getPath(outputFilePath);
			writer = Files.newBufferedWriter(path, charset);
			
			// set item parameters
			String[] itemStrs = new String[items.length];
			for (int i=0 ; i<items.length ; i++) {
				itemStrs[i] = ItemCodeDefine.getItemName(items[i]);
			}
			
			// set target time interval
			long timeInterval   = 1000L * 60L * 5L;
			long timeInterval30 = 1000L * 60L * 30L;
			
			
			// set ruleset parameters
			long firstElementTime = odrRecordHash.keys().nextElement();
			OdrRecord firstOdrRecord = odrRecordHash.get(firstElementTime);
			if (firstOdrRecord == null) {
				return;
			}
			OdrCell[] firstOdrCells = firstOdrRecord.odrCells;
			String[] odrNames = new String[firstOdrCells.length];
			for (int i=0 ; i<odrNames.length ; i++) {
				odrNames[i] = firstOdrCells[i].ruleTypeName;
			}
			
			// write title columns
			// first tile row
			writer.write("time");
			writer.write(",pattern_id");
			writer.write(",pattern_name");
			writer.write(",5-min data");
			for (int i=0 ; i<itemStrs.length-1 ; i++) {
				writer.write(",");
			}
			writer.write(",30-min data");
			for (int i=0 ; i<itemStrs.length-1 ; i++) {
				writer.write(",,,,,,,");
			}
			writer.write(",outlier detection");
			writer.write(",,");
			for (int i=0 ; i<odrNames.length-1 ; i++) {
				writer.write(",");
			}
			writer.write("\n");
			
			// second tile row
			writer.write(",,");
			for (int i=0 ; i<itemStrs.length ; i++) {
				writer.write("," + itemStrs[i]);
			}
			for (int i=0 ; i<itemStrs.length ; i++) {
				writer.write("," + itemStrs[i] + ",,,,,");
			}
			writer.write(",score,level");
			for (int i=0 ; i<odrNames.length ; i++) {
				writer.write("," + odrNames[i]);
			}
			writer.write("\n");

			// third tile row
			writer.write(",,");
			for (int i=0 ; i<itemStrs.length ; i++) {
				writer.write(",");
			}
			for (int i=0 ; i<itemStrs.length ; i++) {
				writer.write(",value, over_state, state, dl_state, code, set_value");
			}
			writer.write(",,");
			for (int i=0 ; i<odrNames.length-1 ; i++) {
				writer.write(",");
			}
			writer.write("\n");
			
			// write record
			for (long time = startTime ; time < endTime ; time += timeInterval) {
				
				// set time
				writer.write(DateUtil.getDateStr(time));
				
				// get TmsRecord and OdrRecrod
				TmsDataRecord dataRecord = tmsRecordHash.get(time);
				OdrRecord     odrRecord  = odrRecordHash.get(time);
				
				// set pattern info
				if (dataRecord == null || odrRecord == null) {
					writer.write(",,");
				} else {
					writer.write(String.format(",%d,%s", odrRecord.patternId, odrRecord.patternName));
				}
				
				// set 5-min data
				if (dataRecord == null) {
					for (int i=0 ; i<itemStrs.length ; i++) {
						writer.write(",");
					}
				} else {
					for (int i=0 ; i<items.length ; i++) {
						writer.write(String.format(",%.3f", dataRecord.getDataCell(items[i]).getData_value()));
					}
				}
				
				// set 30-min data
				long min30Time = (time / timeInterval30) * timeInterval30;
				Tms30minDataRecord data30minRecord = tms30minRecordHash.get(min30Time);
				if (data30minRecord == null) {
					for (int i=0 ; i<items.length ; i++) {
						writer.write(",,,,,,");
					}
				} else {
					for (int i=0 ; i<items.length ; i++) {
						Tms30minDataCell dataCell = data30minRecord.getDataCell(items[i]);
						writer.write(String.format(",%.3f", dataCell.getHalf_valu()));
						writer.write(String.format(",%s"  , dataCell.getHalf_over()));
						writer.write(String.format(",%s"  , dataCell.getHalf_stat()));
						writer.write(String.format(",%s"  , dataCell.getHalf_dlst()));
						writer.write(String.format(",%s"  , dataCell.getStat_code()));
						double setValue = dataCell.getStat_valu();
						if (setValue == -1.0D) {
							writer.write(",");
						} else {
							writer.write(String.format(",%.3f", setValue));
						}
					}
				}
				
				// set outlier data
				if (odrRecord == null) {
					writer.write(",,");
					for (int i=0 ; i<odrNames.length ; i++) {
						writer.write(",");
					}
				} else {
					writer.write("," + odrRecord.getOutlierScore());
					String odrLevel = odrRecord.getOutlierLevelStr();
					if (odrLevel.equals("")) {
						writer.write(",NORMAL");
					} else {
						writer.write("," + odrLevel);
					}
					OdrCell[] odrCells = odrRecord.odrCells;
					for (int i=0 ; i<odrCells.length ; i++) {
						writer.write("," + (odrCells[i].isOutlier == 1 ? "1" : ""));
					}
				}
				writer.write("\n");
				
			} // end of for-loop
			
		} catch (Exception e) {
			logger.error("File writing error.", e);
		} finally {
			try {
				writer.close();
			} catch (Exception ee) {}
		}
	} // end of method
	
	

} // end of class
