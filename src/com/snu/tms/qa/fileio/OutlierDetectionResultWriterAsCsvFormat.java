/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.fileio;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.ruleset.DetectionDataCell;
import com.snu.tms.qa.model.ruleset.DetectionRecord;
import com.snu.tms.qa.model.ruleset.OdrRecord;
import com.snu.tms.qa.util.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Data Element for TMS Data
 * 
 */
public class OutlierDetectionResultWriterAsCsvFormat {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(OutlierDetectionResultWriterAsCsvFormat.class);

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructors
	//==========================================================================

	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Write TmsData as a file
	 */
	public static void writeDataFile(
			String outputFilePath,
			int[]             items,
			DetectionRecord[] records,
			OdrRecord[]       odrRecords) {
		
		
		// Prepare file writer
		BufferedWriter writer = null;
		try {
			// Prepare file
			File resultFile = new File(outputFilePath);
			if (resultFile.exists()) {
				FileUtils.deleteFile(outputFilePath);
			}
			
			// Prepare writer
			Charset charset = Charset.forName("KSC5601");
			Path path = FileSystems.getDefault().getPath(outputFilePath);
			writer = Files.newBufferedWriter(path, charset);
			
			// set parameters
			String[] itemStrs = new String[items.length];
			for (int i=0 ; i<items.length ; i++) {
				itemStrs[i] = ItemCodeDefine.getItemName(items[i]);
			}
			
			DetectionDataCell[] detectionDataCells = records[0].getAllDetectionDataCell();
			String[] ruleNames = new String[detectionDataCells.length];
			for (int i=0 ; i<ruleNames.length ; i++) {
				ruleNames[i] = detectionDataCells[i].getRuleset().getKeyName();
			}
			
			// write title columns
			writer.write("time");
			writer.write(",pattern_id");
			writer.write(",pattern_name");
			writer.write(",outlier_score");
			writer.write(",outlier_level");
			writer.write(",tms_value");
			for (int i=0 ; i<itemStrs.length-1 ; i++) {
				writer.write(",");
			}
			for (int i=0 ; i<ruleNames.length-1 ; i++) {
				writer.write("," + ruleNames[i] + ",,");
			}
			writer.write("\n");
			
			writer.write(",");
			writer.write(",");
			writer.write(",");
			writer.write(",");
			for (int i=0 ; i<itemStrs.length ; i++) {
				writer.write("," + itemStrs[i]);
			}
			for (int i=0 ; i<ruleNames.length ; i++) {
				writer.write(",outlier");
				writer.write(",detect_value");
				writer.write(",threshold");
			}
			writer.write("\n");
			
			// write data
			for (int i=0 ; i<records.length ; i++) {

				writer.write(records[i].getTimestr());
				writer.write("," + records[i].getPatternId());
				writer.write("," + records[i].getPatternName());
				writer.write("," + odrRecords[i].getOutlierScore());
				String levelName = odrRecords[i].getOutlierLevelStr();
				if (levelName.equals("")) {
					writer.write(",NORMAL");
				} else {
					writer.write("," + odrRecords[i].getOutlierLevelStr());
				}
				for (int j=0 ; j<items.length ; j++) {
					writer.write("," + records[i].getRawDataCell(items[j]).getData_value());
				}

				detectionDataCells = records[i].getAllDetectionDataCell();
				for (int j=0 ; j<detectionDataCells.length ; j++) {
					writer.write("," + (detectionDataCells[j].isOutlier() ? "1" : ""));
					writer.write("," + String.format("%.1f", detectionDataCells[j].getResultValue()));
					writer.write("," + String.format("%.1f ~ %.1f", 
							detectionDataCells[j].getRuleset().getThresholds()[1],
							detectionDataCells[j].getRuleset().getThresholds()[0]));
				}
				writer.write("\n");

			} // end of for-loop
			
		} catch (Exception e) {
			logger.error("File writing error.", e);
		} finally {
			try {
				writer.close();
			} catch (Exception ee) {}
		}

		
	} // end of method
	
	

} // end of class
