/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.fileio;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.ItemCodeDefine;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.util.DateUtil;
import com.snu.tms.qa.util.FileUtils;
import com.snu.tms.qa.util.NumberUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Data Element for TMS Data
 * 
 */
public class TmsDataWriterAsCsvFormat {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(TmsDataWriterAsCsvFormat.class);

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	//==========================================================================
	// Constructors
	//==========================================================================

	//==========================================================================
	// Static Methods
	//==========================================================================
	/**
	 * Write TmsData as a file
	 */
	public static void writeDataFile(
			String outputFilePath,
			TmsDataset tmsDataSet) {
		
		
		// Prepare file writer
		BufferedWriter writer = null;
		try {
			// Prepare file
			File resultFile = new File(outputFilePath);
			if (resultFile.exists()) {
				FileUtils.deleteFile(outputFilePath);
			}
			
			// Prepare writer
			Charset charset = Charset.forName("KSC5601");
			Path path = FileSystems.getDefault().getPath(outputFilePath);
			writer = Files.newBufferedWriter(path, charset);
			
			// Get TmsRecords
			int[][]    itemValues = tmsDataSet.getItemValueCount();
			double[][] values     = tmsDataSet.getTmsDataRecordValues();
			double[][] bfValues   = tmsDataSet.getTmsDataRecordBeforeValues();
			long[]     times      = tmsDataSet.getTmsDataRecordTimes();
			if (values.length == 0) {
				throw new Exception("TmsRecords is null or data length is zero.");
			}
			
			// write column name
			String columnNames = "time";
			for (int i=0 ; i<itemValues.length ; i++) {
				columnNames += "," + ItemCodeDefine.getItemName(itemValues[i][0]) + "(after)";
				columnNames += "," + ItemCodeDefine.getItemName(itemValues[i][0]) + "(before)";
			}
			writer.write(columnNames);
			writer.write("\n");
			
			// write records
			String timeStr = "";
			for (int i=0 ; i<values.length ; i++) {
				
				timeStr = DateUtil.getDateStr(times[i]);
				writer.write(timeStr);
				for (int j=0 ; j<values[i].length ; j++) {
					writer.write(", " + NumberUtil.getNonCommaNumberStr(values[i][j]));
					writer.write(", " + NumberUtil.getNonCommaNumberStr(bfValues[i][j]));
				}
				writer.write("\n");
			}
			
		} catch (Exception e) {
			logger.error("File writing error.", e);
		} finally {
			try {
				writer.close();
			} catch (Exception ee) {}
		}

		
	} // end of method
	
	

} // end of class
