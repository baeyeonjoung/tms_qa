/**
 * TMS QA/QC
 * 
 * Objectives : TMS data quality assessment
 * Writer	 : Yeonjoung, Bae
 * Edit date  : November 25, 2015
 * Edit ver.  : v1.0
 * 
 */
package com.snu.tms.qa.fileio;

import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.snu.tms.qa.model.TmsDataHeader;
import com.snu.tms.qa.model.TmsDataset;
import com.snu.tms.qa.model.db.TmsDataCellDB;
import com.snu.tms.qa.util.DateUtil;

/**
 * Query Agent
 * 
 */
public class QueryTmsRecordFromFile {

	//==========================================================================
	// Static Fields
	//==========================================================================
	private static Log logger = LogFactory.getLog(QueryTmsRecordFromFile.class);
	
	//==========================================================================
	// Static Method
	//==========================================================================
	
	/**
	 * @param start_time_str: time format (yyyy-MM-dd HH:mm:ss)
	 * @param end_time_str: time format (yyyy-MM-dd HH:mm:ss)
	 */
	public static TmsDataset readTmsRecord(
			String  inputFilePath, String fact_code, int stck_code, 
			String start_time_str, String end_time_str) {
		
		// Create TmsDataSet
		TmsDataset tmsDataSet = new TmsDataset();
		TmsDataHeader tmsDataHeader = new TmsDataHeader();
		tmsDataHeader.setStart_time_str(start_time_str);
		tmsDataHeader.setEnd_time_str(end_time_str);
		tmsDataSet.setTmsDataHeader(tmsDataHeader);
		
		// set Time
		long sTime = DateUtil.getDate(start_time_str);
		long eTime = DateUtil.getDate(end_time_str);
		
		// Read data from a file
		Vector<TmsDataCellDB> dataArray = TmsDataReader.readDataFile(inputFilePath);
		long targetTime = 0L;
		TmsDataCellDB dataCell;
		try {
			for (int i=0 ; i<dataArray.size() ; i++) {
				
				dataCell = dataArray.get(i);
				targetTime = DateUtil.getTmsDate(dataCell.getMin_time());
				if (targetTime < sTime || targetTime >= eTime) {
					continue;
				}
				tmsDataSet.getTmsDataRecord(targetTime).setValue(
						dataCell.getItem_code(), dataCell.getMin_valu(), dataCell.getMin_valu_bf());
			
			} // end of for-loop
		} catch (Exception e) {
			logger.debug(e.toString());;
		}
		
		return tmsDataSet;
		
	} // end of method
	

	//==========================================================================
	// Local Fields
	//==========================================================================
	
	
	//==========================================================================
	// Constructors
	//==========================================================================

	
	//==========================================================================
	// Methods
	//==========================================================================


} // end of class
