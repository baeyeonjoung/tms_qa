package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.soap.*;
import javax.xml.ws.ResponseWrapper;

import kr.or.webservice.server.model.CorrectionInfo;

@WebService(name="GetCorrectionInfoService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetCorrectionInfoService
{
	@WebMethod(operationName="getCorrectionInfoAll")	
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="java.util.List<kr.or.webservice.server.model.CorrectionInfo>")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public CorrectionInfo[] getCorrectionInfoAll();
}
