package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.soap.*;
import javax.xml.ws.ResponseWrapper;

import kr.or.webservice.server.model.StckClassInfo;

@WebService(name="GetStckClassInfoService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetStckClassInfoService
{
	@WebMethod(operationName="getStckClassInfoAll")	
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.util.List<kr.or.webservice.server.model.StckClassInfo>")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public StckClassInfo[] getStckClassInfoAll();
}
