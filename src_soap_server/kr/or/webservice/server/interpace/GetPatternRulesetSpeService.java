package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.WebParam.Mode;
import javax.jws.soap.*;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import kr.or.webservice.server.model.PatternRulesetSpeRecord;

@WebService(name="GetPatternRulesetSpeService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetPatternRulesetSpeService
{
	@WebMethod(operationName="getPatternRulesetSpe")
	@RequestWrapper(targetNamespace="http://kr.or.webservice",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://kr.or.webservice",className="kr.or.webservice.server.model.PatternRulesetSpeRecord")
	@WebResult(targetNamespace="http://kr.or.webservice",name="updateQuote")
	public PatternRulesetSpeRecord[] getPatternRulesetSpe(@WebParam(targetNamespace="http://kr.or.webservice",name="ruleset_id",mode=Mode.IN) String rulesetId);
	
	@WebMethod(operationName="getPatternRulesetSpeAll")
	@RequestWrapper(targetNamespace="http://kr.or.webservice",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://kr.or.webservice",className="java.util.List<kr.or.webservice.server.model.PatternRulesetSpeRecord>")
	@WebResult(targetNamespace="http://kr.or.webservice",name="updateQuote")
	public PatternRulesetSpeRecord[] getPatternRulesetSpeAll(@WebParam(targetNamespace="http://kr.or.webservice",name="fact_code",mode=Mode.IN) String factCode
										, @WebParam(name = "stack_code") String stckCode);
	
	@WebMethod(operationName="setPatternRulesetSpe")
	@RequestWrapper(targetNamespace="http://kr.or.webservice",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://kr.or.webservice",className="java.lang.String")
	@WebResult(targetNamespace="http://kr.or.webservice",name="updateQuote")
	public String setPatternRulesetSpe(@WebParam(targetNamespace="http://kr.or.webservice",name="ruleset_id",mode=Mode.IN) String rulesetId
								, @WebParam(name = "pattern_id") String patternId
								, @WebParam(name = "ruleset_enable") String rulesetEnable
								, @WebParam(name = "threshold_lower") String thresholdLower
								, @WebParam(name = "threshold_upper") String thresholdUpper
								, @WebParam(name = "items") String items
								, @WebParam(name = "pc_count") String pcCount
								, @WebParam(name = "eigen_vector") String eigenVector
								, @WebParam(name = "eigen_value") String eigenValue
								, @WebParam(name = "neigen_value") String neigenValue
								, @WebParam(name = "mean_value") String meanValue
								, @WebParam(name = "sigma_value") String sigmaValue
								, @WebParam(name = "confi_level") String confiLevel);
	
	@WebMethod(operationName="delPatternRulesetSpe")
	@RequestWrapper(targetNamespace="http://kr.or.webservice",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://kr.or.webservice",className="java.lang.String")
	@WebResult(targetNamespace="http://kr.or.webservice",name="updateQuote")
	public String delPatternRulesetSpe(@WebParam(targetNamespace="http://kr.or.webservice",name="ruleset_id",mode=Mode.IN) String rulesetId);
}
