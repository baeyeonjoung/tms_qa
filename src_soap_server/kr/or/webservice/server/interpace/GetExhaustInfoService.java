package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.soap.*;
import javax.xml.ws.ResponseWrapper;

import kr.or.webservice.server.model.ExhaustInfo;

@WebService(name="GetExhaustInfoService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetExhaustInfoService
{
	@WebMethod(operationName="getExhaustInfoAll")	
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/types",className="java.util.List<kr.or.webservice.server.model.ExhaustInfo>")
	@WebResult(targetNamespace="http://webservice.or.kr/types",name="updateQuote")
	public ExhaustInfo[] getExhaustInfoAll();
}
