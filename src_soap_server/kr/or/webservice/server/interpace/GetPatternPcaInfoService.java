package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.WebParam.Mode;
import javax.jws.soap.*;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import kr.or.webservice.server.model.PatternPcaInfo;

@WebService(name="GetPatternPcaInfoService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetPatternPcaInfoService
{
	@WebMethod(operationName="getPatternPcaInfo")
	@RequestWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="kr.or.webservice.server.model.PatternPcaInfo")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public PatternPcaInfo[] getPatternPcaInfo(@WebParam(targetNamespace="http://demo.iona.com/types",name="ruleset_id",mode=Mode.IN) String rulesetId);
	
	@WebMethod(operationName="getPatternPcaInfoAll")
	@RequestWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="java.util.List<kr.or.webservice.server.model.PatternPcaInfo>")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public PatternPcaInfo[] getPatternPcaInfoAll(@WebParam(targetNamespace="http://demo.iona.com/types",name="fact_code",mode=Mode.IN) String factCode
										, @WebParam(name = "stack_code") String stckCode);
	
	@WebMethod(operationName="setPatternPcaInfo")
	@RequestWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public String setPatternPcaInfo(@WebParam(targetNamespace="http://demo.iona.com/types",name="ruleset_id",mode=Mode.IN) String rulesetId
								, @WebParam(name = "pattern_id") String patternId
								, @WebParam(name = "items") String items
								, @WebParam(name = "pc_count") String pcCount
								, @WebParam(name = "eigen_vector") String eigenVector
								, @WebParam(name = "eigen_value") String eigenValue
								, @WebParam(name = "neigen_value") String neigenValue);
	
	@WebMethod(operationName="delPatternPcaInfo")
	@RequestWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public String delPatternPcaInfo(@WebParam(targetNamespace="http://demo.iona.com/types",name="ruleset_id",mode=Mode.IN) String rulesetId);
}
