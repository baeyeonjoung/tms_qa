package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.WebParam.Mode;
import javax.jws.soap.*;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import kr.or.webservice.server.model.RulesetCorrectionInfo;

@WebService(name="GetRulesetCorrectionInfoService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetRulesetCorrectionInfoService
{
	@WebMethod(operationName="getRulesetCorrectionInfo")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="kr.or.webservice.server.model.RulesetCorrectionInfo")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public RulesetCorrectionInfo[] getRulesetCorrectionInfo(@WebParam(targetNamespace="http://webservice.or.kr/",name="ruleset_id",mode=Mode.IN) String rulesetId);
	
	@WebMethod(operationName="getRulesetCorrectionInfoAll")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.util.List<kr.or.webservice.server.model.RulesetCorrectionInfo>")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public RulesetCorrectionInfo[] getRulesetCorrectionInfoAll();
	
	@WebMethod(operationName="getRecordAll")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.util.List<kr.or.webservice.server.model.RulesetCorrectionInfo>")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public RulesetCorrectionInfo[] getRecordAll(@WebParam(targetNamespace="http://webservice.or.kr/",name="fact_code",mode=Mode.IN) String factCode
										, @WebParam(name = "stack_code") String stckCode);
	
	@WebMethod(operationName="setRulesetCorrectionInfo")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public String setRulesetCorrectionInfo(@WebParam(targetNamespace="http://webservice.or.kr/",name="ruleset_id",mode=Mode.IN) String rulesetId
								, @WebParam(name = "item_code") String itemCode
								, @WebParam(name = "std_oxgy") String stdOxgy
								, @WebParam(name = "std_mois") String stdMois
								, @WebParam(name = "oxyg_corr") String oxygCorr
								, @WebParam(name = "temp_corr") String tempCorr
								, @WebParam(name = "mois_corr") String moisCorr);
	
	@WebMethod(operationName="delRulesetCorrectionInfo")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public String delRulesetCorrectionInfo(@WebParam(targetNamespace="http://webservice.or.kr/",name="ruleset_id",mode=Mode.IN) String rulesetId);
}
