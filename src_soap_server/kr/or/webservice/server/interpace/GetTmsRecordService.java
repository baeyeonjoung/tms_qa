package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.WebParam.Mode;
import javax.jws.soap.*;
import javax.xml.ws.ResponseWrapper;

import kr.or.webservice.server.model.TmsRecord;

@WebService(name="GetTmsRecordService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetTmsRecordService
{
	@WebMethod(operationName="getTmsRecordAll")	
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/types",className="java.util.List<kr.or.webservice.server.model.TmsRecord>")
	@WebResult(targetNamespace="http://webservice.or.kr/types",name="updateQuote")
	public TmsRecord[] getTmsRecordAll(@WebParam(targetNamespace="http://webservice.or.kr/",name="fact_code",mode=Mode.IN)String factCode
			, @WebParam(targetNamespace="http://webservice.or.kr/",name="stck_code",mode=Mode.IN)String stckCode
			, @WebParam(targetNamespace="http://webservice.or.kr/",name="start_time",mode=Mode.IN)String startTimeStr
			, @WebParam(targetNamespace="http://webservice.or.kr/",name="end_time",mode=Mode.IN)String endTimeStr);
}
