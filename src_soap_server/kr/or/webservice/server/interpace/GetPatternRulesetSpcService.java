package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.WebParam.Mode;
import javax.jws.soap.*;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import kr.or.webservice.server.model.PatternRulesetSpcRecord;

@WebService(name="GetPatternRulesetSpcService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetPatternRulesetSpcService
{
	@WebMethod(operationName="getPatternRulesetSpc")
	@RequestWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="kr.or.webservice.server.model.PatternRulesetSpcRecord")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public PatternRulesetSpcRecord[] getPatternRulesetSpc(@WebParam(targetNamespace="http://demo.iona.com/types",name="ruleset_id",mode=Mode.IN) String rulesetId);
	
	@WebMethod(operationName="getPatternRulesetSpcAll")
	@RequestWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="java.util.List<kr.or.webservice.server.model.PatternRulesetSpcRecord>")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public PatternRulesetSpcRecord[] getPatternRulesetSpcAll(@WebParam(targetNamespace="http://demo.iona.com/types",name="fact_code",mode=Mode.IN) String factCode
										, @WebParam(name = "stack_code") String stckCode);
	
	@WebMethod(operationName="setPatternRulesetSpc")
	@RequestWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public String setPatternRulesetSpc(@WebParam(targetNamespace="http://demo.iona.com/types",name="ruleset_id",mode=Mode.IN) String rulesetId
								, @WebParam(name = "pattern_id") String patternId
								, @WebParam(name = "ruleset_enable") String rulesetEnable
								, @WebParam(name = "threshold_lower") String thresholdLower
								, @WebParam(name = "threshold_upper") String thresholdUpper
								, @WebParam(name = "item") String item
								, @WebParam(name = "mean_value") String meanValue
								, @WebParam(name = "sigma_value") String sigmaValue
								, @WebParam(name = "confi_level") String confiLevel);
	
	@WebMethod(operationName="delPatternRulesetSpc")
	@RequestWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public String delPatternRulesetSpc(@WebParam(targetNamespace="http://demo.iona.com/types",name="ruleset_id",mode=Mode.IN) String rulesetId);
}
