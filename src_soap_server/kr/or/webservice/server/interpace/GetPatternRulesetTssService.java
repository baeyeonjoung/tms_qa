package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.WebParam.Mode;
import javax.jws.soap.*;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import kr.or.webservice.server.model.PatternRulesetTssRecord;

@WebService(name="GetPatternRulesetTssService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetPatternRulesetTssService
{
	@WebMethod(operationName="getPatternRulesetTss")
	@RequestWrapper(targetNamespace="http://kr.or.webservice",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://kr.or.webservice",className="kr.or.webservice.server.model.PatternRulesetTssRecord")
	@WebResult(targetNamespace="http://kr.or.webservice",name="updateQuote")
	public PatternRulesetTssRecord[] getPatternRulesetTss(@WebParam(targetNamespace="http://kr.or.webservice",name="ruleset_id",mode=Mode.IN) String rulesetId);
	
	@WebMethod(operationName="getPatternRulesetTssAll")
	@RequestWrapper(targetNamespace="http://kr.or.webservice",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://kr.or.webservice",className="java.util.List<kr.or.webservice.server.model.PatternRulesetTssRecord>")
	@WebResult(targetNamespace="http://kr.or.webservice",name="updateQuote")
	public PatternRulesetTssRecord[] getPatternRulesetTssAll(@WebParam(targetNamespace="http://kr.or.webservice",name="fact_code",mode=Mode.IN) String factCode
										, @WebParam(name = "stack_code") String stckCode);
	
	@WebMethod(operationName="setPatternRulesetTss")
	@RequestWrapper(targetNamespace="http://kr.or.webservice",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://kr.or.webservice",className="java.lang.String")
	@WebResult(targetNamespace="http://kr.or.webservice",name="updateQuote")
	public String setPatternRulesetTss(@WebParam(targetNamespace="http://kr.or.webservice",name="ruleset_id",mode=Mode.IN) String rulesetId
								, @WebParam(name = "pattern_id") String patternId
								, @WebParam(name = "ruleset_enable") String rulesetEnable
								, @WebParam(name = "threshold_lower") String thresholdLower
								, @WebParam(name = "threshold_upper") String thresholdUpper
								, @WebParam(name = "items") String items
								, @WebParam(name = "pc_count") String pcCount
								, @WebParam(name = "eigen_vector") String eigenVector
								, @WebParam(name = "eigen_value") String eigenValue
								, @WebParam(name = "neigen_value") String neigenValue
								, @WebParam(name = "cov_matrix") String covMatrix
								, @WebParam(name = "imv_cov_matrix") String imvCovMatrix
								, @WebParam(name = "mean_value") String meanValue
								, @WebParam(name = "sigma_value") String sigmaValue
								, @WebParam(name = "confi_level") String confiLevel);
	
	@WebMethod(operationName="delPatternRulesetTss")
	@RequestWrapper(targetNamespace="http://kr.or.webservice",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://kr.or.webservice",className="java.lang.String")
	@WebResult(targetNamespace="http://kr.or.webservice",name="updateQuote")
	public String delPatternRulesetTss(@WebParam(targetNamespace="http://kr.or.webservice",name="ruleset_id",mode=Mode.IN) String rulesetId);
}
