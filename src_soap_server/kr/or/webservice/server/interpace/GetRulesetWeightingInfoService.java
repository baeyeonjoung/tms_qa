package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.WebParam.Mode;
import javax.jws.soap.*;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import kr.or.webservice.server.model.RulesetWeightingInfo;

@WebService(name="GetRulesetWeightingInfoService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetRulesetWeightingInfoService
{
	@WebMethod(operationName="getRulesetWeightingInfo")
	@RequestWrapper(targetNamespace="http://webservice.or.kr",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="kr.or.webservice.server.model.RulesetWeightingInfo")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public RulesetWeightingInfo[] getRulesetWeightingInfo(@WebParam(targetNamespace="http://webservice.or.kr/",name="ruleset_id",mode=Mode.IN) String rulesetId);
	
	@WebMethod(operationName="getRulesetWeightingInfoAll")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.util.List<kr.or.webservice.server.model.RulesetWeightingInfo>")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public RulesetWeightingInfo[] getRulesetWeightingInfoAll();
	
	@WebMethod(operationName="getRecordAll")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.util.List<kr.or.webservice.server.model.RulesetWeightingInfo>")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public RulesetWeightingInfo[] getRecordAll(@WebParam(targetNamespace="http://webservice.or.kr/",name="fact_code",mode=Mode.IN) String factCode
										, @WebParam(name = "stack_code") String stckCode);
	
	@WebMethod(operationName="setRulesetWeightingInfo")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public String setRulesetWeightingInfo(@WebParam(targetNamespace="http://webservice.or.kr/",name="ruleset_id",mode=Mode.IN) String rulesetId
								, @WebParam(name="multi_weight") String multiWeight
								, @WebParam(name="bi_weight") String biWeight
								, @WebParam(name="uni_weight") String uniWeight
								, @WebParam(name="corr_weight") String corrWeight
								, @WebParam(name="warn_threshold") String warnThreshold
								, @WebParam(name="minor_threshold") String minorThreshold
								, @WebParam(name="major_threshold") String majorThreshold
								, @WebParam(name="critical_threshold") String criticalThreshold);
	
	@WebMethod(operationName="delRulesetWeightingInfo")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public String delRulesetWeightingInfo(@WebParam(targetNamespace="http://webservice.or.kr/",name="ruleset_id",mode=Mode.IN) String rulesetId);
}
