package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.WebParam.Mode;
import javax.jws.soap.*;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import kr.or.webservice.server.model.PatternItemInfo;

@WebService(name="GetPatternItemInfoService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetPatternItemInfoService
{
	@WebMethod(operationName="getPatternItemInfo")
	@RequestWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="kr.or.webservice.server.model.PatternItemInfo")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public PatternItemInfo[] getPatternItemInfo(@WebParam(targetNamespace="http://demo.iona.com/types",name="ruleset_id",mode=Mode.IN) String rulesetId);
	
	@WebMethod(operationName="getPatternItemInfoAll")
	@RequestWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="java.util.List<kr.or.webservice.server.model.PatternItemInfo>")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public PatternItemInfo[] getPatternItemInfoAll(@WebParam(targetNamespace="http://demo.iona.com/types",name="fact_code",mode=Mode.IN) String factCode
										, @WebParam(name = "stack_code") String stckCode);
	
	@WebMethod(operationName="setPatternItemInfo")
	@RequestWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public String setPatternItemInfo(@WebParam(targetNamespace="http://demo.iona.com/types",name="ruleset_id",mode=Mode.IN) String rulesetId
								, @WebParam(name = "pattern_id") String patternId
								, @WebParam(name = "item_code") String itemCode
								, @WebParam(name = "item_name") String itemName
								, @WebParam(name = "min_range") String minRange
								, @WebParam(name = "max_range") String maxRange
								, @WebParam(name = "bin_count") String binCount
								, @WebParam(name = "bin_interval") String binInterval
								, @WebParam(name = "density") String density);
	
	@WebMethod(operationName="delPatternItemInfo")
	@RequestWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public String delPatternItemInfo(@WebParam(targetNamespace="http://demo.iona.com/types",name="ruleset_id",mode=Mode.IN) String rulesetId);
}
