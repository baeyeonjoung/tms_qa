package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.WebParam.Mode;
import javax.jws.soap.*;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(name="GetUserInfoService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetUserInfoService
{
	@WebMethod(operationName="getUserInfoCheck")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="kr.or.webservice.server.model.UserInfo")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public String getUserInfoCheck(@WebParam(targetNamespace="http://webservice.or.kr/",name="user_id",mode=Mode.IN) String userId
								, @WebParam(targetNamespace="http://webservice.or.kr/",name="user_pw",mode=Mode.IN) String userPw);
	
}
