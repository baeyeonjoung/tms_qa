package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.WebParam.Mode;
import javax.jws.soap.*;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


import kr.or.webservice.server.model.FactInfo;

@WebService(name="GetFactInfoService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetFactInfoService
{
	@WebMethod(operationName="getFactInfo")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="kr.or.webservice.server.model.FactInfo")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public FactInfo getFactInfo(@WebParam(targetNamespace="http://webservice.or.kr/",name="fact_code",mode=Mode.IN)String factCode);
	
	@WebMethod(operationName="getFactInfoAll")	
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.util.List<kr.or.webservice.server.model.FactInfo>")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public FactInfo[] getFactInfoAll();
}
