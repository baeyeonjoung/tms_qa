package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.WebParam.Mode;
import javax.jws.soap.*;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import kr.or.webservice.server.model.RulesetInfo;

@WebService(name="GetRulesetInfoService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetRulesetInfoService
{
	@WebMethod(operationName="getRulesetInfo")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="kr.or.webservice.server.model.RulesetInfo")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public RulesetInfo[] getRulesetInfo(@WebParam(targetNamespace="http://webservice.or.kr/",name="fact_code",mode=Mode.IN) String factCode
									, @WebParam(name = "stack_code") String stckCode);
	
	@WebMethod(operationName="getRulesetInfoAll")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.util.List<kr.or.webservice.server.model.RulesetInfo>")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public RulesetInfo[] getRulesetInfoAll();
	
	@WebMethod(operationName="getDefaultRulesetInfo")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.util.List<kr.or.webservice.server.model.RulesetInfo>")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public RulesetInfo[] getDefaultRulesetInfo(@WebParam(targetNamespace="http://webservice.or.kr/",name="fact_code",mode=Mode.IN) String factCode
										, @WebParam(name = "stack_code") String stckCode);
	
	@WebMethod(operationName="setRulesetInfo")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public String setRulesetInfo(@WebParam(targetNamespace="http://webservice.or.kr/",name="ruleset_id",mode=Mode.IN) String rulesetId
								, @WebParam(name="fact_code") String factCode
								, @WebParam(name="stck_code") String stckCode
								, @WebParam(name="ruleset_state") String rulesetState
								, @WebParam(name="ruleset_name") String rulesetName
								, @WebParam(name="ruleset_descr") String rulesetDescr
								, @WebParam(name="user_id") String userId
								, @WebParam(name="create_time") String createTime
								, @WebParam(name="start_time") String startTime
								, @WebParam(name="end_time") String endTime);
	
	@WebMethod(operationName="getLastRulesetId")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public String getLastRulesetId(@WebParam(targetNamespace="http://webservice.or.kr/",name="fact_code",mode=Mode.IN) String factCode
										, @WebParam(name = "stack_code") String stckCode);
	
	@WebMethod(operationName="editRulesetInfo")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public String editRulesetInfo(@WebParam(targetNamespace="http://webservice.or.kr/",name="ruleset_id",mode=Mode.IN) String rulesetId
								, @WebParam(name = "ruleset_state") String rulesetState
								, @WebParam(name = "ruleset_name") String rulesetName
								, @WebParam(name = "ruleset_descr") String rulesetDescr);
	
	@WebMethod(operationName="delRulesetInfo")
	@RequestWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://webservice.or.kr/",className="java.lang.String")
	@WebResult(targetNamespace="http://webservice.or.kr/",name="updateQuote")
	public String delRulesetInfo(@WebParam(targetNamespace="http://webservice.or.kr/",name="ruleset_id",mode=Mode.IN) String rulesetId);
}
