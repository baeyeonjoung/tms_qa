package kr.or.webservice.server.interpace;

import javax.jws.*;
import javax.jws.WebParam.Mode;
import javax.jws.soap.*;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import kr.or.webservice.server.model.StckInfo;

@WebService(name="GetStckInfoService")
@SOAPBinding(style=javax.jws.soap.SOAPBinding.Style.RPC, use=javax.jws.soap.SOAPBinding.Use.LITERAL)
public interface GetStckInfoService
{
	@WebMethod(operationName="getStckInfo")
	@RequestWrapper(targetNamespace="http://demo.iona.com/types",className="java.lang.String")
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="kr.or.webservice.server.model.StckInfo")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public StckInfo getStckInfo(@WebParam(targetNamespace="http://demo.iona.com/types",name="fact_code",mode=Mode.IN) String factCode
								, @WebParam(name = "stack_code") String stckCode);
	
	@WebMethod(operationName="getStckInfoAll")	
	@ResponseWrapper(targetNamespace="http://demo.iona.com/types",className="java.util.List<kr.or.webservice.server.model.StckInfo>")
	@WebResult(targetNamespace="http://demo.iona.com/types",name="updateQuote")
	public StckInfo[] getStckInfoAll();
}
