package kr.or.webservice.server.model;

public class UserInfo {
	
	public String user_id = null;
	public String user_pw = null;
	public String user_role_id = null;
	
	String getUser_id() {
		return user_id;
	}
	void setUser_id(String userId) {
		this.user_id = userId;
	}
	String getUser_pw() {
		return user_pw;
	}
	void setUser_pw(String userPw) {
		this.user_pw = userPw;
	}
	String getUser_role_id() {
		return user_role_id;
	}
	void setUser_role_id(String userRoleId) {
		this.user_role_id = userRoleId;
	}
}
