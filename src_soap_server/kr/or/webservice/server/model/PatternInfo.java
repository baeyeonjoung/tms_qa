package kr.or.webservice.server.model;

public class PatternInfo {
	
	public String ruleset_id = null;
	public String pattern_id = null;
	public String pattern_name = null;
	public String pattern_enable = null;
	public String ruleset_enable = null;
	public String operate_stop_state = null;
	public String item_count = null;
	public String items = null;
	
	String getRuleset_id() {
		return ruleset_id;
	}
	void setRuleset_id(String rulesetId) {
		this.ruleset_id = rulesetId;
	}
	String getPattern_id() {
		return pattern_id;
	}
	void setPattern_id(String patternId) {
		this.pattern_id = patternId;
	}
	String getPattern_name() {
		return pattern_name;
	}
	void setPattern_name(String patternName) {
		this.pattern_name = patternName;
	}
	String getPattern_enable() {
		return pattern_enable;
	}
	void setPattern_enable(String patternEnable) {
		this.pattern_enable = patternEnable;
	}
	String getRuleset_enable() {
		return ruleset_enable;
	}
	void setRuleset_enable(String rulesetEnable) {
		this.ruleset_enable = rulesetEnable;
	}
	String getOperate_stop_state() {
		return operate_stop_state;
	}
	void setOperate_stop_state(String operateStopState) {
		this.operate_stop_state = operateStopState;
	}
	String getItem_count() {
		return item_count;
	}
	void setItem_count(String itemCount) {
		this.item_count = itemCount;
	}
	String getItems() {
		return items;
	}
	void setItems(String items) {
		this.items = items;
	}
}
