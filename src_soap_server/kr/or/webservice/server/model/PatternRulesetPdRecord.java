package kr.or.webservice.server.model;

public class PatternRulesetPdRecord {
	
	public String ruleset_id = null;
	public String pattern_id = null;
	public String ruleset_enable = null;
	public String threshold_lower = null;
	public String threshold_upper = null;
	public String item = null;
	public String mean_value = null;
	public String sigma_value = null;
	public String confi_level = null;
	
	String getRuleset_id() {
		return ruleset_id;
	}
	void setRuleset_id(String rulesetId) {
		this.ruleset_id = rulesetId;
	}
	String getPattern_id() {
		return pattern_id;
	}
	void setPattern_id(String patternId) {
		this.pattern_id = patternId;
	}
	String getRuleset_enable() {
		return ruleset_enable;
	}
	void setRuleset_enable(String rulesetEnable) {
		this.ruleset_enable = rulesetEnable;
	}
	String getThreshold_lower() {
		return threshold_lower;
	}
	void setThreshold_lower(String thresholdLower) {
		this.threshold_lower = thresholdLower;
	}
	String getThreshold_upper() {
		return threshold_upper;
	}
	void setThreshold_upper(String thresholdUpper) {
		this.threshold_upper = thresholdUpper;
	}
	String getItem() {
		return item;
	}
	void setItem(String item) {
		this.item = item;
	}
	String getMean_value() {
		return mean_value;
	}
	void setMean_value(String meanValue) {
		this.mean_value = meanValue;
	}
	String getSigma_value() {
		return sigma_value;
	}
	void setSigma_value(String sigmaValue) {
		this.sigma_value = sigmaValue;
	}
	String getconfi_level() {
		return confi_level;
	}
	void setconfi_level(String confiLevel) {
		this.confi_level = confiLevel;
	}
}
