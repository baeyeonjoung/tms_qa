package kr.or.webservice.server.model;

public class TmsRecord {
	
	public String item_code = null;
	public String min_time = null;
	public String min_valu = null;
	public String min_valu_bf = null;
	
	String getItem_code() {
		return item_code;
	}
	void setItem_code(String itemCode) {
		this.item_code = itemCode;
	}
	String getMin_time() {
		return min_time;
	}
	void setMin_time(String minTime) {
		this.min_time = minTime;
	}
	String getMin_valu() {
		return min_valu;
	}
	void setMin_valu(String minValu) {
		this.min_valu = minValu;
	}
	String getMin_valu_bf() {
		return min_valu_bf;
	}
	void setMin_valu_bf(String minValuBf) {
		this.min_valu_bf = minValuBf;
	}
}
