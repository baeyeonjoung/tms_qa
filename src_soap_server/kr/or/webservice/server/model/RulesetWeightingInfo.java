package kr.or.webservice.server.model;

public class RulesetWeightingInfo {
	
	public String ruleset_id = null;
	public String multi_weight = null;
	public String bi_weight = null;
	public String uni_weight = null;
	public String corr_weight = null;
	public String warn_threshold = null;
	public String minor_threshold = null;
	public String major_threshold = null;
	public String critical_threshold = null;
	
	String getRuleset_id() {
		return ruleset_id;
	}
	void setRuleset_id(String rulesetId) {
		this.ruleset_id = rulesetId;
	}
	String getMulti_weight() {
		return multi_weight;
	}
	void setMulti_weight(String multiWeight) {
		this.multi_weight = multiWeight;
	}
	String getBi_weight() {
		return bi_weight;
	}
	void setBi_weight(String biWeight) {
		this.bi_weight = biWeight;
	}
	String getUni_weight() {
		return uni_weight;
	}
	void setUni_weight(String uniWeight) {
		this.uni_weight = uniWeight;
	}
	String getCorr_weight() {
		return corr_weight;
	}
	void setCorr_weight(String corrWeight) {
		this.corr_weight = corrWeight;
	}
	String getWarn_threshold() {
		return warn_threshold;
	}
	void setWarn_threshold(String warnThreshold) {
		this.warn_threshold = warnThreshold;
	}
	String getMinor_threshold() {
		return minor_threshold;
	}
	void setMinor_threshold(String minorThreshold) {
		this.minor_threshold = minorThreshold;
	}
	String getMajor_threshold() {
		return major_threshold;
	}
	void setMajor_threshold(String majorThreshold) {
		this.major_threshold = majorThreshold;
	}
	String getCritical_threshold() {
		return critical_threshold;
	}
	void setCritical_threshold(String criticalThreshold) {
		this.critical_threshold = criticalThreshold;
	}
}
