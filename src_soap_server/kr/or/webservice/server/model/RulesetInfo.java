package kr.or.webservice.server.model;

public class RulesetInfo {
	
	public String ruleset_id = null;
	public String fact_code = null;
	public String stck_code = null;
	public String ruleset_state = null;
	public String ruleset_name = null;
	public String ruleset_descr = null;
	public String user_id = null;
	public String create_time = null;
	public String start_time = null;
	public String end_time = null;
	
	String getRuleset_id() {
		return ruleset_id;
	}
	void setRuleset_id(String rulesetId) {
		this.ruleset_id = rulesetId;
	}
	String getFact_code() {
		return fact_code;
	}
	void setFact_code(String factCode) {
		this.fact_code = factCode;
	}
	String getStck_code() {
		return stck_code;
	}
	void setStck_code(String stackCode) {
		this.stck_code = stackCode;
	}
	String getRuleset_state() {
		return ruleset_state;
	}
	void setRuleset_state(String rulesetState) {
		this.ruleset_state = rulesetState;
	}
	String getRuleset_name() {
		return ruleset_name;
	}
	void setRuleset_name(String rulesetName) {
		this.ruleset_name = rulesetName;
	}
	String getRuleset_descr() {
		return ruleset_descr;
	}
	void setRuleset_descr(String rulesetDescr) {
		this.ruleset_descr = rulesetDescr;
	}
	String getUser_id() {
		return user_id;
	}
	void setUser_id(String userId) {
		this.user_id = userId;
	}
	String getCreate_time() {
		return create_time;
	}
	void setCreate_time(String createTime) {
		this.create_time = createTime;
	}
	String getStart_time() {
		return start_time;
	}
	void setStart_time(String startTime) {
		this.start_time = startTime;
	}
	String getEnd_time() {
		return end_time;
	}
	void setEnd_time(String endTime) {
		this.end_time = endTime;
	}
}
