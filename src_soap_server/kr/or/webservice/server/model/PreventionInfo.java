package kr.or.webservice.server.model;

public class PreventionInfo {
	
	public String fact_code = null;
	public String stck_code = null;
	public String exhstCode = null;
	public String prvnName = null;
	public String prvnCapacity = null;
	
	String getFact_code() {
		return fact_code;
	}
	void setFact_code(String factCode) {
		this.fact_code = factCode;
	}
	String getStck_code() {
		return stck_code;
	}
	void setStck_code(String stackCode) {
		this.stck_code = stackCode;
	}
	String getExhstCode() {
		return exhstCode;
	}
	void setExhstCode(String exhstCode) {
		this.exhstCode = exhstCode;
	}
	String getPrvnName() {
		return prvnName;
	}
	void setPrvnName(String prvnName) {
		this.prvnName = prvnName;
	}
	String getPrvnCapacity() {
		return prvnCapacity;
	}
	void setPrvnCapacity(String prvnCapacity) {
		this.prvnCapacity = prvnCapacity;
	}
}
