package kr.or.webservice.server.model;

public class CorrectionInfo {
	
	public String fact_code = null;
	public String stck_code = null;
	public String item_code = null;
	public String std_oxgy = null;
	public String oxyg_corr = null;
	public String temp_corr = null;
	public String mois_corr = null;
	
	String getFact_code() {
		return fact_code;
	}
	void setFact_code(String factCode) {
		this.fact_code = factCode;
	}
	String getStck_code() {
		return stck_code;
	}
	void setStck_code(String stackCode) {
		this.stck_code = stackCode;
	}
	String getItem_code() {
		return item_code;
	}
	void setItem_code(String itemCode) {
		this.item_code = itemCode;
	}
	String getStd_oxgy() {
		return std_oxgy;
	}
	void setStd_oxgy(String stdOxgy) {
		this.std_oxgy = stdOxgy;
	}
	String getOxyg_corr() {
		return oxyg_corr;
	}
	void setOxyg_corr(String oxygCorr) {
		this.oxyg_corr = oxygCorr;
	}
	String getTemp_corr() {
		return temp_corr;
	}
	void setTemp_corr(String tempCorr) {
		this.temp_corr = tempCorr;
	}
	String getMois_corr() {
		return mois_corr;
	}
	void setMois_corr(String mois_corr) {
		this.mois_corr = mois_corr;
	}
}
