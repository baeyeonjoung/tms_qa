package kr.or.webservice.server.model;

public class ExhaustInfo {
	
	public String fact_code = null;
	public String stck_code = null;
	public String exhstCode = null;
	public String exhstName = null;
	public String exhstCapacity = null;
	
	String getFact_code() {
		return fact_code;
	}
	void setFact_code(String factCode) {
		this.fact_code = factCode;
	}
	String getStck_code() {
		return stck_code;
	}
	void setStck_code(String stackCode) {
		this.stck_code = stackCode;
	}
	String getExhstCode() {
		return exhstCode;
	}
	void setExhstCode(String exhstCode) {
		this.exhstCode = exhstCode;
	}
	String getExhstName() {
		return exhstName;
	}
	void setExhstName(String exhstName) {
		this.exhstName = exhstName;
	}
	String getExhstCapacity() {
		return exhstCapacity;
	}
	void setExhstCapacity(String exhstCapacity) {
		this.exhstCapacity = exhstCapacity;
	}
}
