package kr.or.webservice.server.model;

public class PatternPcaInfo {
	
	public String ruleset_id = null;
	public String pattern_id = null;
	public String items = null;
	public String pc_count = null;
	public String eigen_vector = null;
	public String eigen_value = null;
	public String neigen_value = null;
	
	String getRuleset_id() {
		return ruleset_id;
	}
	void setRuleset_id(String rulesetId) {
		this.ruleset_id = rulesetId;
	}
	String getPattern_id() {
		return pattern_id;
	}
	void setPattern_id(String patternId) {
		this.pattern_id = patternId;
	}
	String getItems() {
		return items;
	}
	void setItems(String items) {
		this.items = items;
	}
	String getPc_count() {
		return pc_count;
	}
	void setPc_count(String pcCount) {
		this.pc_count = pcCount;
	}
	String getEigen_vector() {
		return eigen_vector;
	}
	void setEigen_vector(String eigenVector) {
		this.eigen_vector = eigenVector;
	}
	String getEigen_value() {
		return eigen_value;
	}
	void setEigen_value(String eigenValue) {
		this.eigen_value = eigenValue;
	}
	String getNeigen_value() {
		return neigen_value;
	}
	void setNeigen_value(String neigenValue) {
		this.neigen_value = neigenValue;
	}
}
