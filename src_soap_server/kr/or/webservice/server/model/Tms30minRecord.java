package kr.or.webservice.server.model;

public class Tms30minRecord {
	
	public String item_code = null;
	public String half_time = null;
	public String half_valu = null;
	public String half_over = null;
	public String half_stat = null;
	public String half_dlst = null;
	public String stat_code = null;
	public String stat_valu = null;
	
	String getItem_code() {
		return item_code;
	}
	void setItem_code(String itemCode) {
		this.item_code = itemCode;
	}
	String getHalf_time() {
		return half_time;
	}
	void setHalf_time(String halfTime) {
		this.half_time = halfTime;
	}
	String getHalf_valu() {
		return half_valu;
	}
	void setHalf_valu(String halfValu) {
		this.half_valu = halfValu;
	}
	String getHalf_over() {
		return half_over;
	}
	void setHalf_over(String halfOver) {
		this.half_over = halfOver;
	}
	String getHalf_stat() {
		return half_stat;
	}
	void setHalf_stat(String halfStat) {
		this.half_stat = halfStat;
	}
	String getHalf_dlst() {
		return half_dlst;
	}
	void setHalf_dlst(String halfDlst) {
		this.half_dlst = halfDlst;
	}
	String getStat_code() {
		return stat_code;
	}
	void setStat_code(String statCode) {
		this.stat_code = statCode;
	}
	String getStat_valu() {
		return stat_valu;
	}
	void setStat_valu(String statValu) {
		this.stat_valu = statValu;
	}
}
