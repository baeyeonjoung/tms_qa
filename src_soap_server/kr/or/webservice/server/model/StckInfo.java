package kr.or.webservice.server.model;

public class StckInfo {
	
	public String fact_code = null;
	public String stck_code = null;
	public String stck_name = null;
	public String stck_oxyg = null;
	public String stck_mois = null;
	
	String getFact_code() {
		return fact_code;
	}
	void setFact_code(String factCode) {
		this.fact_code = factCode;
	}
	String getStck_code() {
		return stck_code;
	}
	void setStck_code(String stackCode) {
		this.stck_code = stackCode;
	}
	String getStck_name() {
		return stck_name;
	}
	void setStck_name(String stckName) {
		this.stck_name = stckName;
	}
	String getStck_oxyg() {
		return stck_oxyg;
	}
	void setStck_oxyg(String stckOxyg) {
		this.stck_oxyg = stckOxyg;
	}
	String getStck_mois() {
		return stck_mois;
	}
	void setStck_mois(String stckMois) {
		this.stck_mois = stckMois;
	}
}
