package kr.or.webservice.server.model;

public class FactInfo {
	
	public String fact_code = null;
	public String fact_nams = null;
	public String fact_naml = null;
	public String fact_namt = null;
	public String fact_addr = null;
	public String fact_prod = null;
	public String fact_numb = null;
	public String area_code2 = null;
	
	
	String getFact_code() {
		return fact_code;
	}
	void setFact_code(String factCode) {
		this.fact_code = factCode;
	}
	String getFact_nams() {
		return fact_nams;
	}
	void setFact_nams(String factNams) {
		this.fact_nams = factNams;
	}
	String getFact_naml() {
		return fact_naml;
	}
	void setFact_naml(String factNaml) {
		this.fact_naml = factNaml;
	}
	String getFact_namt() {
		return fact_namt;
	}
	void setFact_namt(String factNamt) {
		this.fact_namt = factNamt;
	}
	String getFact_addr() {
		return fact_addr;
	}
	void setFact_addr(String factAddr) {
		this.fact_addr = factAddr;
	}
	String getfact_prod() {
		return fact_prod;
	}
	void setfact_prod(String factProd) {
		this.fact_prod = factProd;
	}
	String getFact_numb() {
		return fact_numb;
	}
	void setFact_numb(String factNumb) {
		this.fact_numb = factNumb;
	}
	String getArea_code2() {
		return area_code2;
	}
	void setArea_code2(String factArea) {
		this.area_code2 = factArea;
	}
}
