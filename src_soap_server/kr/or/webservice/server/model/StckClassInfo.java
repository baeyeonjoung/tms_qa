package kr.or.webservice.server.model;

public class StckClassInfo {
	
	public String fact_code = null;
	public String stck_code = null;
	public String stck_desc = null;
	public String proc_class1 = null;
	public String proc_class2 = null;
	public String fuel_name = null;
	
	String getFact_code() {
		return fact_code;
	}
	void setFact_code(String factCode) {
		this.fact_code = factCode;
	}
	String getStck_code() {
		return stck_code;
	}
	void setStck_code(String stackCode) {
		this.stck_code = stackCode;
	}
	String getStck_desc() {
		return stck_desc;
	}
	void setStck_desc(String stckDesc) {
		this.stck_desc = stckDesc;
	}
	String getProc_class1() {
		return proc_class1;
	}
	void setProc_class1(String procClass1) {
		this.proc_class1 = procClass1;
	}
	String getProc_class2() {
		return proc_class2;
	}
	void setProc_class2(String procClass2) {
		this.proc_class2 = procClass2;
	}
	String getFuel_name() {
		return fuel_name;
	}
	void setFuel_name(String fuelName) {
		this.fuel_name = fuelName;
	}
}
