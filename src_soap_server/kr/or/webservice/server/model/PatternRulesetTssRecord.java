package kr.or.webservice.server.model;

public class PatternRulesetTssRecord {
	
	public String ruleset_id = null;
	public String pattern_id = null;
	public String ruleset_enable = null;
	public String threshold_lower = null;
	public String threshold_upper = null;
	public String items = null;
	public String pc_count = null;
	public String eigen_vector = null;
	public String eigen_value = null;
	public String neigen_value = null;
	public String cov_matrix = null;
	public String imv_cov_matrix = null;
	public String mean_value = null;
	public String sigma_value = null;
	public String confi_level = null;
	
	String getRuleset_id() {
		return ruleset_id;
	}
	void setRuleset_id(String rulesetId) {
		this.ruleset_id = rulesetId;
	}
	String getPattern_id() {
		return pattern_id;
	}
	void setPattern_id(String patternId) {
		this.pattern_id = patternId;
	}
	String getRuleset_enable() {
		return ruleset_enable;
	}
	void setRuleset_enable(String rulesetEnable) {
		this.ruleset_enable = rulesetEnable;
	}
	String getThreshold_lower() {
		return threshold_lower;
	}
	void setThreshold_lower(String thresholdLower) {
		this.threshold_lower = thresholdLower;
	}
	String getThreshold_upper() {
		return threshold_upper;
	}
	void setThreshold_upper(String thresholdUpper) {
		this.threshold_upper = thresholdUpper;
	}
	String getItems() {
		return items;
	}
	void setItems(String items) {
		this.items = items;
	}
	String getPc_count() {
		return pc_count;
	}
	void setPc_count(String pcCount) {
		this.pc_count = pcCount;
	}
	String getEigen_vector() {
		return eigen_vector;
	}
	void setEigen_vector(String eigenVector) {
		this.eigen_vector = eigenVector;
	}
	String getEigen_value() {
		return eigen_value;
	}
	void setEigen_value(String eigenValue) {
		this.eigen_value = eigenValue;
	}
	String getNeigen_value() {
		return neigen_value;
	}
	void setNeigen_value(String neigenValue) {
		this.neigen_value = neigenValue;
	}
	String getCov_matrix() {
		return cov_matrix;
	}
	void setCov_matrix(String covMatrix) {
		this.cov_matrix = covMatrix;
	}
	String getImv_cov_matrix() {
		return imv_cov_matrix;
	}
	void setImv_cov_matrix(String imvCovMatrix) {
		this.imv_cov_matrix = imvCovMatrix;
	}
	String getMean_value() {
		return mean_value;
	}
	void setMean_value(String meanValue) {
		this.mean_value = meanValue;
	}
	String getSigma_value() {
		return sigma_value;
	}
	void setSigma_value(String sigmaValue) {
		this.sigma_value = sigmaValue;
	}
	String getconfi_level() {
		return confi_level;
	}
	void setconfi_level(String confiLevel) {
		this.confi_level = confiLevel;
	}
}
