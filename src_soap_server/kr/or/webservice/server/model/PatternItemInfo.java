package kr.or.webservice.server.model;

public class PatternItemInfo {
	
	public String ruleset_id = null;
	public String pattern_id = null;
	public String item_code = null;
	public String item_name = null;
	public String min_range = null;
	public String max_range = null;
	public String bin_count = null;
	public String bin_interval = null;
	public String density = null;
	
	String getRuleset_id() {
		return ruleset_id;
	}
	void setRuleset_id(String rulesetId) {
		this.ruleset_id = rulesetId;
	}
	String getPattern_id() {
		return pattern_id;
	}
	void setPattern_id(String patternId) {
		this.pattern_id = patternId;
	}
	String getItem_code() {
		return item_code;
	}
	void setItem_code(String itemCode) {
		this.item_code = itemCode;
	}
	String getItem_name() {
		return item_name;
	}
	void setItem_name(String itemName) {
		this.item_name = itemName;
	}
	String getMin_range() {
		return min_range;
	}
	void setMin_range(String minRange) {
		this.min_range = minRange;
	}
	String getMax_range() {
		return max_range;
	}
	void setMax_range(String maxRange) {
		this.max_range = maxRange;
	}
	String getBin_count() {
		return bin_count;
	}
	void setBin_count(String binCount) {
		this.bin_count = binCount;
	}
	String getBin_interval() {
		return bin_interval;
	}
	void setBin_interval(String binInterval) {
		this.bin_interval = binInterval;
	}
	String getDensity() {
		return density;
	}
	void setDensity(String density) {
		this.density = density;
	}
}
